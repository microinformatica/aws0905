<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Faccompras_model extends CI_Model {
        public $tabla="proveedores";
		public $nrf="nrf";public $conf="con";public $pro="prov";public $tip="tipo";public $fac="fac";public $can="can";
		public $pre="pre";public $med="uni";public $fec="fec";public $fcap="fcap";	
		
		public $tablaCon="conceptos";
		public $nrc="nrc";public $con="des";public $pm="pm";public $ana="analizar";public $cat="categoria";public $proy="proyeccion";
		public $ant="anterior";public $impant="impant";public $dp="dp";public $eximzt="eximzt";public $consumo="consumo";public $exilab="exilab";
		
		public $tablaOrd="orden";
		public $ord="orden";public $proo="nrp";public $feco="fechao";
		
		public $tablaDet="ordendet";
		public $nrd="nrd";public $nro="nro";public $nrcd="nrc";public $cand="canc";public $obs="obsc";
		var $today;
        
		public $tablaOrdD="ordendet";
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getFaccompras($filter,$ciclo){
			//$query=$this->db->query("Select Siglas,fec,fac,con,can,pre from proveedores inner join $ciclo on prov=numero");
			$this->db->select('nrf,Siglas,Razon,fec,fac,des,con,can,pre,uni,tipo,prov');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->order_by($this->fec,'DESC');$this->db->order_by('Razon');$this->db->order_by($this->fac,'DESC');$this->db->order_by($this->con);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*$zonas="Todos";
			if($filter['num']!=0){ 
					//$this->db->where($this->zona,$filter['num']);		
			}	*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$ixx = 0; $fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$row->tot=$row->can*$row->pre;
				/*if($row->SI==0 ){$deudazona=$deudazona+$row->Saldo;}
				$row->Saldo1 = $row->Saldo; //$row->Saldo2 += $row->Saldo;
				$row->Saldo = '$ '.number_format($row->Saldo, 2, '.', ',');		*/		 					
				if($row->fec=="" or $row->fec=="0000-00-00"){$row->fec1="";}else{$row->fec1 = $fec->fecha($row->fec);}	
				if($row->pre!=0){$row->pre1 ="$". number_format($row->pre, 2, '.', ',');$row->pre =number_format($row->pre, 2, '.', ',');}else{$row->pre1="";$row->pre="";}						
				if($row->tot!=0){$row->tot ="$". number_format($row->tot, 2, '.', ',');}else{$row->tot="";};
				 
				$data[$ixx] = $row;	
				$ixx = $ixx+1;			
			endforeach;	
			
					
			return $data;
		}
		
		
		function getNumRows($filter,$ciclo){
			$this->db->select('nrf,Siglas,Razon,fec,fac,des,con,can,pre,uni,tipo,prov');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->order_by($this->fec,'DESC');$this->db->order_by('Razon');$this->db->order_by($this->fac,'DESC');$this->db->order_by($this->con);
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getClientes($filter){
			$this->db->select('Numero,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumClientes($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getProves($filter,$ciclo){
			$this->db->select('Numero,Razon,Siglas');
			$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->group_by('Razon');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumProves($filter,$ciclo){
			$this->db->select('Numero,Razon,Siglas');
			$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->group_by('Razon');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getPS($filter,$ciclo,$pro){
			$this->db->select('des,sum(can) as cant,uni,sum(can*pre) as pret');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->where('prov',$pro);
			$this->db->group_by('des');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($row->cant!=0){$row->cant =number_format($row->cant, 2, '.', ',').' '.$row->uni;}else{$row->cant="";}
				if($row->pret!=0){$row->pret ="$". number_format($row->pret, 2, '.', ',');}else{$row->pret="";}
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumPS($filter,$ciclo,$pro){
			$this->db->select('des,sum(can) as cant,uni,sum(can*pre) as pret');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->where('prov',$pro);
			$this->db->group_by('des');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getDPS($filter,$ciclo,$pro){
			$this->db->select('des,fec,fac,can,uni,pre,(can*pre) as tot');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->where('prov',$pro);
			$this->db->order_by('des');
			$this->db->order_by('fec');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria(); $cli=''; $totgranja=0;$tottot=0;
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$des=$row->des;
				if($cli!=$des){
					if($totgranja>0){ 
						$this->db->select('max(Numero)');
						$resultZ= $this->db->get('clientes');
						foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "";$rowZ->fec1 = "Total:";$rowZ->fac = "";$rowZ->pre = "";
								$rowZ->can = number_format($totgranja, 2, '.', ',');
								$rowZ->tot ="$". number_format($tottot, 2, '.', ',');
								$data[] = $rowZ;	
						endforeach;
						$totgranja=0;$tottot=0;
					}
					$cli=$row->des; 
				}else{ $row->des="";} 
				$totgranja+=$row->can;$tottot+=$row->tot;
				if($row->fec=="" or $row->fec=="0000-00-00"){$row->fec1="";}else{$row->fec1 = $fec->fecha($row->fec);}	
				if($row->can!=0){$row->can =number_format($row->can, 2, '.', ',').' '.$row->uni;}else{$row->can="";}
				if($row->tot!=0){$row->tot ="$".number_format($row->tot, 2, '.', ',');}else{$row->tot="";}
				if($row->pre!=0){$row->pre ="$". number_format($row->pre, 2, '.', ',');}else{$row->pre="";}
				$data[] = $row;	
			endforeach;
			if($totgranja>0){ 
						$this->db->select('max(Numero)');
						$resultZ= $this->db->get('clientes');
						foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "";$rowZ->fec1 = "Total:";$rowZ->fac = "";$rowZ->pre = "";
								$rowZ->can = number_format($totgranja, 2, '.', ',');
								$rowZ->tot ="$". number_format($tottot, 2, '.', ',');
								$data[] = $rowZ;	
						endforeach;
						$totgranja=0;$tottot=0;
					}
			return $data;
		}
		function getNumDPS($filter,$ciclo,$pro){
			$this->db->select('des,sum(can) as cant,uni,sum(can*pre) as pret');
			$this->db->join($this->tablaCon, 'nrc=con','inner');
			$this->db->where('prov',$pro);
			$this->db->group_by('des');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getDMS($filter,$ciclo,$con){
			$this->db->select('Razon,fec,fac,can,uni,pre,(can*pre) as tot');
			$this->db->join($this->tabla, 'Numero=prov','inner');
			$this->db->where('con',$con);
			$this->db->order_by('Razon');
			$this->db->order_by('fec');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria(); $cli=''; $totgranja=0;$tottot=0;
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$row->Razon=strtolower($row->Razon);$row->Razon=ucwords($row->Razon);
				$des=$row->Razon;
				if($cli!=$des){
					if($totgranja>0){ 
						$this->db->select('max(Numero)');
						$resultZ= $this->db->get('clientes');
						foreach ($resultZ->result() as $rowZ):				
								$rowZ->Razon = "";$rowZ->fec1 = "Total:";$rowZ->fac = "";$rowZ->pre = "";
								$rowZ->can = number_format($totgranja, 2, '.', ',');
								$rowZ->tot ="$". number_format($tottot, 2, '.', ',');
								$data[] = $rowZ;	
						endforeach;
						$totgranja=0;$tottot=0;
					}
					$cli=$row->Razon; 
				}else{ $row->Razon="";} 
				$totgranja+=$row->can;$tottot+=$row->tot;
				if($row->fec=="" or $row->fec=="0000-00-00"){$row->fec1="";}else{$row->fec1 = $fec->fecha($row->fec);}	
				if($row->can!=0){$row->can =number_format($row->can, 2, '.', ',').' '.$row->uni;}else{$row->can="";}
				if($row->tot!=0){$row->tot ="$".number_format($row->tot, 2, '.', ',');}else{$row->tot="";}
				if($row->pre!=0){$row->pre ="$". number_format($row->pre, 2, '.', ',');}else{$row->pre="";}
				
				$data[] = $row;	
			endforeach;
			if($totgranja>0){ 
						$this->db->select('max(Numero)');
						$resultZ= $this->db->get('clientes');
						foreach ($resultZ->result() as $rowZ):				
								$rowZ->Razon = "";$rowZ->fec1 = "Total:";$rowZ->fac = "";$rowZ->pre = "";
								$rowZ->can = number_format($totgranja, 2, '.', ',');
								$rowZ->tot ="$". number_format($tottot, 2, '.', ',');
								$data[] = $rowZ;	
						endforeach;
						$totgranja=0;$tottot=0;
					}
			return $data;
		}
		function getNumDMS($filter,$ciclo,$con){
			$this->db->select('Razon,fec,fac,can,uni,pre,(can*pre) as tot');
			$this->db->join($this->tabla, 'Numero=prov','inner');
			$this->db->where('con',$con);
			$this->db->order_by('Razon');
			$this->db->order_by('fec');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getConceptos($filter){
			//$this->db->select('nrcro,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			$this->db->order_by('des');
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaCon,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaCon);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				///if($row->pm==1){$row->tip ="Producto";}else{$row->tip="Material";};
				$row->anterior =number_format($row->anterior, 2, '.', ',');
				$row->proyeccion =number_format($row->proyeccion, 2, '.', ',');
				$row->impant =number_format($row->impant, 2, '.', ',');
				$row->eximzt =number_format($row->eximzt, 2, '.', ',');
				$row->exilab =number_format($row->exilab, 2, '.', ',');
				$row->consumo =number_format($row->consumo, 2, '.', ',');
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumConceptos($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaCon);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getConceptosS($filter,$ciclo){
			//$this->db->select('nrcro,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			$this->db->join($ciclo, 'nrc=con','inner');
			$this->db->group_by('des');
			//$this->db->order_by('des');
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaCon,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaCon);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				///if($row->pm==1){$row->tip ="Producto";}else{$row->tip="Material";};
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumConceptosS($filter,$ciclo){
			$this->db->join($ciclo, 'nrc=con','inner');
			$this->db->group_by('des');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaCon);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getOrden($filter){
			$this->db->select('orden,Razon,fechao,nrp');
			$this->db->join($this->tabla, 'nrp=Numero','inner');
			//$this->db->select('nrcro,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			$this->db->order_by('orden','DESC');
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaOrd,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaOrd);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($row->fechao=="" or $row->fechao=="0000-00-00"){$row->feco="";}else{$row->feco = $fec->fecha($row->fechao);}
				///if($row->pm==1){$row->tip ="Producto";}else{$row->tip="Material";};
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumOrden($filter){
			$this->db->select('orden,Razon,fechao,nrp');
			$this->db->join($this->tabla, 'nrp=Numero','inner');
			$this->db->order_by('orden','DESC');
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaOrd);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getDetalle($filter){
			//$this->db->select('orden,Razon,fechao,nrp');
			$this->db->join($this->tablaOrdD, 'nro=orden','inner');
			//$this->db->select('nrcro,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			//$this->db->order_by('orden','DESC');
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			if($filter['num']!=0)
				$this->db->where('nro',$filter['num']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaOrd,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaOrd);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//if($row->fechao=="" or $row->fechao=="0000-00-00"){$row->feco="";}else{$row->feco = $fec->fecha($row->fechao);}
				///if($row->pm==1){$row->tip ="Producto";}else{$row->tip="Material";};
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumDetalle($filter){
			//$this->db->select('orden,Razon,fechao,nrp');
			$this->db->join($this->tablaOrdD, 'nro=orden','inner');
			//$this->db->order_by('orden','DESC');
			if($filter['num']!=0)
				$this->db->where('nro',$filter['num']);
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaOrd);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getfaccomprasgral($filter,$buscar,$ciclo){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			$this->db->select('des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp,eximzt,consumo,exilab');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'con=nrc','inner');
			$this->db->where('analizar',1);
			if($buscar!=''){
				$this->db->like($this->con,$buscar);
			}
			$this->db->group_by('dp'); $this->db->group_by('des');
			//$this->db->order_by('des');$this->db->order_by('Razon');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*$zonas="Todos";
			if($filter['num']!=0){
					//$this->db->where($this->zona,$filter['num']);		
			}	*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();$totact=0;$totant=0;$totinc=0;$cli=0;$totgranja=0;$tex="";
			if($can>0){
			
			
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$dp=$row->dp;
				if($cli!=$dp){
							
						if($totgranja>0){ $totgranja=0;
							if($cli==1) $tex="Dólares"; else $tex="Pesos"; 
							$this->db->select('max(Numero)');
							$resultZ= $this->db->get('clientes');
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "Total ".$tex.":";$rowZ->cantidad = "";$rowZ->ppact = "";$rowZ->importe ="$".number_format($totact, 2, '.', ',');
								$rowZ->pca = "";$rowZ->inc = "";$rowZ->incp = "";$rowZ->anterior = "";$rowZ->ppant = "";$rowZ->incd="$". number_format($totinc, 2, '.', ',');
								$rowZ->impant ="$". number_format($totant, 2, '.', ',');
								$rowZ->proyeccion = "";$rowZ->pp = "";$rowZ->eximzt = "";$rowZ->exilab = "";$rowZ->consumo = "";
								$data[] = $rowZ;	
							endforeach;
							$totact=0;$totant=0;$totinc=0;
						}
						$cli=$row->dp;
				}
				$uno=0;$dos=0;$tres=0;$pre=0;$totact+=$row->importe;$totant+=$row->impant;$totgranja+=1;
				$row->inc=number_format(($row->cantidad-$row->anterior), 2, '.', ',')." ".$row->uni;
				//$row->incd="$".number_format(($row->ppact-$row->ppant)*$row->cantidad, 2, '.', ',');
				//$row->incd="$".number_format($row->incd, 2, '.', ',');
				$row->incp="$".number_format($row->ppact-$row->ppant, 2, '.', ',');$row->incp1=$row->ppact-$row->ppant;
				$row->incp2=number_format($row->ppact-$row->ppant, 2, '.', ',');$totinc+=$row->cantidad*($row->incp2);
				$row->incd="$".number_format($row->cantidad*$row->incp2, 2, '.', ',');
				$row->ppact="$".number_format($row->ppact, 2, '.', ',');
				if($row->anterior!=0){$uno=$row->anterior;$row->anterior = number_format($row->anterior, 2, '.', ',')." ".$row->uni;}else{$row->anterior="";}
				if($row->proyeccion!=0){$tres=$row->proyeccion;$row->proyeccion = number_format($row->proyeccion, 2, '.', ',')." ".$row->uni;}else{$row->proyeccion="";}
				$dos=$row->cantidad;
				$row->cantidad = number_format($row->cantidad, 2, '.', ',')." ".$row->uni;
				if($row->eximzt>0){$row->eximzt = number_format($row->eximzt, 2, '.', ',')." ".$row->uni;}else{$row->eximzt='';}						
				if($row->exilab>0){$row->exilab = number_format($row->exilab, 2, '.', ',')." ".$row->uni;}else{$row->exilab='';}
				if($row->consumo>0){$row->consumo = number_format($row->consumo, 2, '.', ',')." ".$row->uni;}else{$row->consumo='';}
				$row->importe ="$". number_format($row->importe, 2, '.', ',');
				if($row->impant>0){
					$row->ppant="$".number_format($row->ppant, 2, '.', ',');	
					$row->impant ="$". number_format($row->impant, 2, '.', ',');
				}else{$row->impant ='';$row->ppant="";}
				 
				if($dos>0 && $uno>0){
					$row->pca=number_format(((($dos/$uno)-1)*100), 2, '.', ',')."%";$row->pca1=number_format((($dos/$uno)*100), 2, '.', ',');
				} else {$row->pca='';;$row->pca1='';}
				if($dos>0 && $tres>0){$row->pp=number_format((($dos/$tres)*100), 2, '.', ',')."%";} else {$row->pp='';}
				//elseif($uno==0 && $dos==0)  $row->pca=''; else $row->pca='100.00%'; 
				/*if($dos>0) $row->pp=number_format((($tres-$dos)/$dos*100), 2, '.', ',')."%";
				elseif($tres==0 && $dos==0)  $row->pp=''; else $row->pp='100.00%';*/
				$data[] = $row;	
			endforeach;	
			if($totgranja>0){ $totgranja=0;
							if($cli==1) $tex="Dólares"; else $tex="Pesos"; 
							$this->db->select('max(Numero)');
							$resultZ= $this->db->get('clientes');
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "Total ".$tex.":";$rowZ->cantidad = "";$rowZ->ppact = "";$rowZ->importe ="$".number_format($totact, 2, '.', ',');
								$rowZ->pca = "";$rowZ->inc = "";$rowZ->incp = "";$rowZ->anterior = "";$rowZ->ppant = "";$rowZ->incd="$". number_format($totinc, 2, '.', ',');
								$rowZ->impant ="$". number_format($totant, 2, '.', ',');
								$rowZ->proyeccion = "";$rowZ->pp = "";$rowZ->eximzt = "";$rowZ->exilab = "";$rowZ->consumo = "";
								$data[] = $rowZ;	
							endforeach;
						}
			}
			return $data;
		}
		
		function getNumRowsgral($filter,$buscar,$ciclo){
			$this->db->select('des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,proyeccion,anterior');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'con=nrc','inner');
			$this->db->where('analizar',1);
			if($buscar!=''){
				$this->db->like($this->con,$buscar);
			}
			$this->db->group_by('des'); //$this->db->group_by('Razon');
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function agregarR($con,$tip,$ana,$cat,$pro,$ant,$impant,$dp,$eximzt,$exilab,$consumo){
			$pro = str_replace(",", "", $pro);$ant = str_replace(",", "", $ant);$impant = str_replace(",", "", $impant);$eximzt = str_replace(",", "", $eximzt);$exilab = str_replace(",", "", $exilab);$consumo = str_replace(",", "", $consumo);
			$data=array($this->con=>$con,$this->pm=>$tip,$this->ana=>$ana,$this->cat=>$cat,$this->proy=>$pro,$this->ant=>$ant,$this->impant=>$impant,$this->dp=>$dp,$this->eximzt=>$eximzt,$this->exilab=>$exilab,$this->consumo=>$consumo);
			$this->db->insert($this->tablaCon,$data);
			return $this->db->insert_id();
		}
		function actualizarR($id,$con,$tip,$ana,$cat,$pro,$ant,$impant,$dp,$eximzt,$exilab,$consumo){
			$pro = str_replace(",", "", $pro);$ant = str_replace(",", "", $ant);$impant = str_replace(",", "", $impant);$eximzt = str_replace(",", "", $eximzt);$exilab = str_replace(",", "", $exilab);$consumo = str_replace(",", "", $consumo);
			$data=array($this->con=>$con,$this->pm=>$tip,$this->ana=>$ana,$this->cat=>$cat,$this->proy=>$pro,$this->ant=>$ant,$this->impant=>$impant,$this->dp=>$dp,$this->eximzt=>$eximzt,$this->exilab=>$exilab,$this->consumo=>$consumo);
			$this->db->where($this->nrc,$id);
			$this->db->update($this->tablaCon,$data);
			//$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function agregarF($con,$pro,$tip,$fac,$can,$pre,$med,$fec,$ciclo){
			$can = str_replace(",", "", $can);$pre = str_replace(",", "", $pre);	
			$hoy=date("y-m-d H:i:s");
			$data=array($this->fcap=>$hoy,$this->conf=>$con,$this->pro=>$pro,$this->tip=>$tip,$this->fac=>$fac,$this->can=>$can,$this->pre=>$pre,$this->med=>$med,$this->fec=>$fec);
			$this->db->insert($ciclo,$data);
			return $this->db->insert_id();
		}
		function actualizarF($id,$con,$pro,$tip,$fac,$can,$pre,$med,$fec,$ciclo){
			$can = str_replace(",", "", $can);$pre = str_replace(",", "", $pre);	
			$data=array($this->conf=>$con,$this->pro=>$pro,$this->tip=>$tip,$this->fac=>$fac,$this->can=>$can,$this->pre=>$pre,$this->med=>$med,$this->fec=>$fec);
			$this->db->where($this->nrf,$id);
			$this->db->update($ciclo,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function actualizarO($id,$pro,$fec){
			$data=array($this->proo=>$pro,$this->feco=>$fec);
			$this->db->where($this->ord,$id);
			$this->db->update($this->tablaOrd,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function agregarO($pro,$fec){
			$data=array($this->proo=>$pro,$this->feco=>$fec);
			$this->db->insert($this->tablaOrd,$data);
			return $this->db->insert_id();
		}
		function actualizarD($id,$can,$des,$obs){
			$data=array($this->cand=>$can,$this->nrcd=>$des,$this->obs=>$obs);
			$this->db->where($this->nrd,$id);
			$this->db->update($this->tablaDet,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function agregarD($can,$des,$obs,$ord){
			$data=array($this->cand=>$can,$this->nrcd=>$des,$this->obs=>$obs,$this->nro=>$ord);
			$this->db->insert($this->tablaDet,$data);
			return $this->db->insert_id();
		}
		function borrarD($id){
			$this->db->where($this->nrd,$id);
			$this->db->delete($this->tablaDet);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function borrarF($id,$ciclo){
			$this->db->where($this->nrf,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
    }
    
?>