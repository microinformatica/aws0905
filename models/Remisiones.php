<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Remisiones extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('remisiones_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('remisiones_model');
			$fi=date("Y-m-d");$ff= date("Y-m-d");
			//$data['txtFI']="2012-01-11"; //$fi;
			//$data['txtFF']=$ff;				
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$rem['result'] = $this->remisiones_model->ultimaremision();
			$txtRemision=0;$urr=0;
			foreach ($rem['result'] as $row): 
				$txtRemision=$row->ultimo+1;										
				$urr=$row->ultimoR+1;
			endforeach;
			$data['txtRemision']=$txtRemision;
			$data['urr']=$urr;
			$this->load->view('remisiones/lista',$data);
        }	
		function pdfrepcv() {
            $this->load->model('remisiones_model');
			//$data['result']=$this->coeficiente_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
					
			$this->load->view('remisiones/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('remisiones/listapdfcv', $data, true);  
			pdf ($html,'remisiones/listapdfcv', true);
        	set_paper('letter');
        }		
		function pdfrep() {
            $this->load->model('remisiones_model');
			//$data['result']=$this->remisiones_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
					
			$this->load->view('remisiones/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('remisiones/listapdf', $data, true);  
			pdf ($html,'remisiones/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($cic='',$ori=10,$extra=0,$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ori!=10) $filter['where']['origen =']=$ori;
			$filter['num'] = $extra;
			if($desde!='') $filter['where']['fechar >=']=$desde;
			if($hasta!='') $filter['where']['fechar <=']=$hasta;
			$folio='folios';
			if($cic<16) $folio=$folio.'_'.($cic-1);
			$cic='r'.$cic;
			$data['rows'] = $this->remisiones_model->getRemisiones($filter,$cic,$folio);
        	$data['num_rows'] = $this->remisiones_model->getNumRows($filter,$cic,$folio);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablaesc($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $extra;
			$data['rows'] = $this->remisiones_model->getEscaner($filter);
        	//$data['num_rows'] = $this->remisiones_model->getNumRowsEsc($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaescr($extra=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$cli = $extra;
			$data['rows'] = $this->remisiones_model->getEscanerr($cli,$mes);
        	//$data['num_rows'] = $this->remisiones_model->getNumRowsEsc($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaescrc($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//$cli = $extra;
			$data['rows'] = $this->remisiones_model->getEscanerrc($extra);
        	//$data['num_rows'] = $this->remisiones_model->getNumRowsEsc($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaescrz($zona=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//$cli = $extra;
			if($zona!='null' && $zona!='0'){
        		if($zona=='El%20Dorado') $zona='El Dorado';
				if($zona=='Sur%20Sinaloa') $zona='Sur Sinaloa';
				if($zona=='Obreg%C3%B3n') $zona='Obregón';
				if($zona=='Yucat%C3%A1n') $zona='Yucatán';
        		//$filter['where']['Zona']=$zona;$z=1;
        	
			}
			$data['rows'] = $this->remisiones_model->getEscanerrz($zona);
        	//$data['num_rows'] = $this->remisiones_model->getNumRowsEsc($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra;        	
        	$data['rows'] = $this->remisiones_model->getClientes($filter);
        	$data['num_rows'] = $this->remisiones_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaclientesr($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra;        	
        	$data['rows'] = $this->remisiones_model->getClientes($filter);
        	$data['num_rows'] = $this->remisiones_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablacv($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra;
        	$data['rows'] = $this->remisiones_model->getcv($filter);
        	echo '('.json_encode($data).')';                
    	}
		
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		//$this->load->helper('url');
		$this->load->model('remisiones_model');		
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$cli=$this->input->post('cli');
		$can=$this->input->post('can');
		$pre=$this->input->post('pre');
		$avi=$this->input->post('avi');
		$est=$this->input->post('est');
		$dco=$this->input->post('dco');
		$tipo=$this->input->post('tipo');
		$obs=$this->input->post('obs');
		$fol=$this->input->post('fol');
		$ur=$this->input->post('ur');		
		$ori=$this->input->post('ori');
		$cic=$this->input->post('cic');
		$enc=$this->input->post('enc');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ela=$this->input->post('ela');
		$folio='folios';
		if($cic<15) $folio=$folio.'_'.($cic-1);
		$cic='r'.$cic;
		if($rem!=''){	
			$this->remisiones_model->agregar($id,$fec,$rem,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$fol,$ur,$ori,$cic,$folio,$enc,$cho,$uni,$ela);
			redirect('remisiones');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		
		$this->load->view('remisiones/agregar',$datos);
		}
		
		function actualizar($id=0){
		//$this->load->helper('url');
		$this->load->model('remisiones_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$cli=$this->input->post('cli');
		$can=$this->input->post('can');
		$pre=$this->input->post('pre');
		$avi=$this->input->post('avi');
		$est=$this->input->post('est');
		$dco=$this->input->post('dco');
		$tipo=$this->input->post('tipo');
		$obs=$this->input->post('obs');
		$fol=$this->input->post('fol');
		$ur=$this->input->post('ur');
		$rec=$this->input->post('rec');
		$ori=$this->input->post('ori');
		$cic=$this->input->post('cic');
		$enc=$this->input->post('enc');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ela=$this->input->post('ela');
		$folio='folios';
		if($cic<15) $folio=$folio.'_'.($cic-1);
		$cic='r'.$cic;
		if($id_post!=''){
			$return=$this->remisiones_model->actualizar($id_post,$fec,$rem,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$fol,$ur,$rec,$ori,$cic,$folio,$enc,$cho,$uni,$ela); 			
			redirect('remisiones');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->remisiones_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('remisiones/actualizar',$datos);*/
		}
		
		function actualizacv($id=0){
		//$this->load->helper('url');
		$this->load->model('remisiones_model');
		$id_post=$this->input->post('id'); 
		$m1=$this->input->post('m1');
		$m2=$this->input->post('m2');
		$m3=$this->input->post('m3');
		if($id_post!=''){
			$return=$this->remisiones_model->actualizacv($id_post,$m1,$m2,$m3); 			
			redirect('remisiones');
		}
		}
		function agregacv(){
		//$this->load->helper('url');
		$this->load->model('remisiones_model');		
		$m1=$this->input->post('m1');
		$m2=$this->input->post('m2');
		$m3=$this->input->post('m3');
		$fol=$this->input->post('fol');
		if($fol!=''){	
			$this->remisiones_model->agregacv($m1,$m2,$m3,$fol);
			redirect('remisiones');
		}		
		}
		function quitarDet($id=0){
		$this->load->helper('url');
		$this->load->model('remisiones_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->remisiones_model->quitarDet($id_post); 			
			redirect('remisiones');
		}
		}
		public function tablacvimpcv($extra=0,$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($desde!='') $filter['where']['fechar >=']=$desde;
			if($hasta!='') $filter['where']['fechar <=']=$hasta;
			$cic='r'.$extra;
        	$data['rows'] = $this->remisiones_model->getcvimpcv($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}	
		
		function actualizaresc($id=0){
		$this->load->model('remisiones_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fecesc');
		$uni=$this->input->post('uni');
		$cho=$this->input->post('cho');
		$km=$this->input->post('km');
		$cv=$this->input->post('cv');
		$cp=$this->input->post('cp');if($cp=='') $cp=0;
		$rv=$this->input->post('rv');if($rv=='') $rv=0;
		$rl=$this->input->post('rl');if($rl=='') $rl=0;
		$ren=$this->input->post('renesc');if($ren=='') $ren=0;
		$reme1=$this->input->post('reme1');
		$reme2=$this->input->post('reme2');
		if($id_post!=''){
			$return=$this->remisiones_model->actualizaresc($id_post,$fec,$uni,$cho,$km,$cv,$cp,$rv,$rl,$ren,$reme1,$reme2); 			
			redirect('remisiones');
		}
		}
		function agregaresc(){
		$this->load->model('remisiones_model');		
		$fec=$this->input->post('fecesc');
		$uni=$this->input->post('uni');
		$cho=$this->input->post('cho');
		$km=$this->input->post('km');
		$cv=$this->input->post('cv');
		$cp=$this->input->post('cp');if($cp=='') $cp=0;
		$rv=$this->input->post('rv');if($rv=='') $rv=0;
		$rl=$this->input->post('rl');if($rl=='') $rl=0;
		$ren=$this->input->post('renesc');if($ren=='') $ren=0;
		$reme1=$this->input->post('reme1');
		$reme2=$this->input->post('reme2');
		if($fec!=''){	
			$this->remisiones_model->agregaresc($fec,$uni,$cho,$km,$cv,$cp,$rv,$rl,$ren,$reme1,$reme2);
			redirect('remisiones');
		}		
		}
		public function comboZonUni(){
			$filter['NumUni']=$this->input->post('id_unidad');//EN SU CASO 
			$data = $this->base_model->getElementsZU($filter);//SE MANDA A LLAMAR LA FUNCION DEL MODELO QUE REGRESARA LA CONSULTA. 
			echo '('.json_encode($data).')';//SE ENVIA UN ECHO CON LOS DATOS CODIFICADOS CON JSON_ENCODE 
		}	
    }
    
?>