<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Aqpgranja_model extends CI_Model {
    	
        public $idpis="idpis";public $pisg="pisg";public $hasg="hasg";public $espg="espg";public $orgg="orgg";public $prog="prog";
		public $fecg="fecg";public $plg="plg";public $cicg="cicg";public $fecgc="fecgc";public $biogc="biogc";public $ppgc="ppgc";
		public $obsc="obsc";public $numgra="numgra";
		public $tabla="siegra_21";public $tablasie="siegra_";
		
		public $idcpis="idcpis";public $cpisg="cpisg";public $ccicg="ccicg";public $cfecgc="cfecgc";public $cbiogc="cbiogc";public $cppgc="cppgc";
		public $cobsc="cobsc";
		public $tablacos="cosgra_21";
		
        public $idbio="idbio";public $idpisb="idpisb";public $pesb="pesb";public $fecb="fecb";public $cicb="cicb";public $obsb="obsb";public $grab="numgrab";public $incest="incest";
		public $tablabiogra="biogra_21";
		
		public $idsob="idsob";public $idpiss="idpiss";public $sobp="sobp";public $fecs="fecs";public $cics="cics";public $obss="obss";public $gras="numgras";
		public $camm2="camm2";
		public $tablasobgra="sobgra_21";
		
		public $idali="idali";public $idpisali="idpisali";public $canali="canali";public $fecali="fecali";public $cicali="cicali";public $obsali="obsali";public $graa="numgraa";
		public $tablaaligra="aligra_21";
		
		public $idpfq="idpfq";public $idpisfq="idpisfq";public $fecfq="fecfq";public $t1fq="t1fq";public $o1fq="o1fq";public $salfq="salfq";public $reca="reca";
		public $turfq="turfq";public $t2fq="t2fq";public $o2fq="o2fq";public $phfq="phfq";public $cicfq="cicfq";public $grap="numgrap";public $salu="salud";
		public $tablapargra="pargra_21";
		
		public $idpischa="idpischa";
		public $tablachagra="chagra_21";
		public $tablacha="aligracha_21";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function quitar($id,$tab,$cam,$ciclo){
			$this->db->where($cam,$id);
			$this->db->delete($tab.'_'.$ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function quitarfq($id){
			$this->db->where($this->idpfq,$id);
			$this->db->delete($this->tablapargra);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getSiembraPis($filter){
			//$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);	
			//$this->db->where('Saldo >',0);	
			$this->db->order_by($this->pisg);		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se realiza la consulta con una limitación, en caso de que sea valida
		//	If($filter['limit']!=0)
		//		$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
		//	else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$th=0;$to=0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$th+=$row->hasg;$to+=$row->orgg;
				$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',');
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				$row->hasg = number_format($row->hasg, 3, '.', ',');
				$row->fecg1 = $fec->fecha($row->fecg);
				$row->biogc = number_format($row->biogc, 3, '.', ',');
				$row->ppgc = number_format($row->ppgc, 2, '.', ',');
				if($row->plg==0) $row->plg='';
				$data[] = $row;	
			endforeach;	
			if($th>0){
			$this->db->select('max(idpis)');	
			$result = $this->db->get($this->tabla);		
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):				
				$row->pisg = "Total: "; //$row->Saldo2 += $row->Saldo;
				$row->den=number_format((($to*1000)/$th/10000), 1, '.', ',');			 					
				$row->orgg = number_format($to, 3, '.', ',');
				$row->hasg = number_format($th, 3, '.', ',');
				$row->espg="";$row->prog="";$row->plg="";
				$row->fecg1="";$row->cicg="";
				$data[] = $row;	
							
			endforeach;
			}				
			return $data;
		}
		function getNumRows($filter){					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getProduccion($filter){
			$this->db->select('idpis,pisg,hasg,orgg,prog,fecg,plg,cicg,fecgc,biogc,ppgc,DATEDIFF( fecgc,fecg ) AS dias',NULL,FALSE);	
			//$this->db->where('Saldo >',0);	
			$this->db->order_by($this->idpis);		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$th=0;$to=0;$biot=0;$prot=0;$cont=0;$orgt=0;$orgs=0;$alit=0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):				
				$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',');
				$est=$row->idpis;$bioprod=0;
				if($row->dias>0)$row->diasc=$row->dias; else $row->diasc='';
				$row->fecg1 = $fec->fecha($row->fecg);
				
				if($row->biogc>0){
					$row->sob=number_format(((($row->biogc)/$row->ppgc)/$row->orgg*100), 1, '.', ',').'%';	$bioprod=$row->biogc;$biot+=$row->biogc;
					$row->habio=number_format($row->biogc/$row->hasg, 0, '.', ',');$row->biogc=number_format($row->biogc, 1, '.', ',');
					if($row->ppgc>0){$prot+=$row->ppgc;$cont+=1;$row->ppgc=number_format($row->ppgc, 1, '.', ',');}else{$row->ppgc='';}
					$orgt+=($row->orgg*10)*$row->sob;
					$row->orgsc=number_format(($row->orgg*10)*$row->sob, 0, '.', ',');
					$row->orgsmc=number_format($row->orgsc/($row->hasg*10), 1, '.', ',');
					$row->efi=number_format($row->biogc/$row->orgg*1000, 2, '.', ',');
					//buscar alimento acumulado
					$query=$this->db->query("select sum(canali) as alim from aligra_21 where idpisali=$est");
					foreach($query->result() as $rowAli):
						if($rowAli->alim>0){
							$alit+=$rowAli->alim;
							$row->alihas=number_format(($rowAli->alim/$row->hasg), 0, '.', ',');
							$row->alifca=number_format(($rowAli->alim/$bioprod), 2, '.', ',');
							$row->aliprod=number_format(($rowAli->alim), 0, '.', ',');
						}
						else{$row->aliprod='';$row->alihas='';$row->alifca=''; $alit=0;}					
					endforeach;
				}else{
					$row->biogc='';$row->habio='';$row->ppgc='';$row->orgsc='';$row->orgsmc='';$row->sob='';$row->efi='';
					$row->alihas='';$row->alifca='';$row->aliprod='';
				}
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				$row->hasg = number_format($row->hasg, 3, '.', ',');
				$data[] = $row;	
			endforeach;	
			if($result->num_rows()>0){
			$this->db->select('sum(hasg) as hasg,sum(orgg) as orgg,avg(ppgc) as ppgc');	
			if($filter['where']!=''){
				$this->db->where($filter['where']); }	
			$result = $this->db->get($this->tabla);		
									
			foreach ($result->result() as $row):
				$orgs=$row->orgg;				
				$row->pisg = "Total: "; //$row->Saldo2 += $row->Saldo;
				$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',');
				$row->hasg = number_format($row->hasg, 3, '.', ',');
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				if($biot>0){
					$row->biogc= number_format($biot, 0, '.', ',');
					$row->habio=number_format($biot/$row->hasg, 0, '.', ',');
					$row->ppgc=number_format($prot/$cont, 2, '.', ',');;
					$row->orgsc=number_format($orgt, 0, '.', ',');
					$row->orgsmc=number_format($orgt/($row->hasg*10), 1, '.', ',');
					$row->sob=number_format($orgt/$orgs, 1, '.', ',').'%';
					$row->efi=number_format($biot/$orgs, 1, '.', ',').'%';
					$row->aliprod=number_format($alit, 0, '.', ',');
					$row->alihas=number_format($alit/$row->hasg, 0, '.', ',');
					$row->alifca=number_format($alit/$biot, 2, '.', ',');
				}
				else{
				$row->biogc='';$row->habio='';$row->ppgc='';$row->orgsc='';$row->orgsmc='';$row->sob='';$row->efi='';
				$row->aliprod='';$row->alihas='';$row->alifca='';				
					
				}
				$row->diasc='';$row->prog='';$row->fecg1='';
				$data[] = $row;	
			endforeach;
			}				
			return $data;
		}
		function getNumRowP($filter){					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function actualizar($id,$pis,$has,$esp,$org,$pro,$fec,$est,$cic,$fecc,$bio,$pp,$obs,$numgra){
			$org = str_replace(",", "", $org);$bio = str_replace(",", "", $bio);
			if($fecc='0000-00-00' || $fecc='')
			$data=array($this->pisg=>$pis,$this->hasg=>$has,$this->espg=>$esp,$this->orgg=>$org,$this->prog=>$pro,$this->fecg=>$fec,$this->plg=>$est,$this->cicg=>$cic,$this->biogc=>$bio,$this->ppgc=>$pp,$this->obsc=>$obs,$this->numgra=>$numgra);
			else
			$data=array($this->pisg=>$pis,$this->hasg=>$has,$this->espg=>$esp,$this->orgg=>$org,$this->prog=>$pro,$this->fecg=>$fec,$this->plg=>$est,$this->cicg=>$cic,$this->fecgc=>$fecc,$this->biogc=>$bio,$this->ppgc=>$pp,$this->obsc=>$obs,$this->numgra=>$numgra);
			$this->db->where($this->idpis,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregar($pis,$has,$esp,$org,$pro,$fec,$est,$cic,$numgra){
			$org = str_replace(",", "", $org);
			$data=array($this->pisg=>$pis,$this->hasg=>$has,$this->espg=>$esp,$this->orgg=>$org,$this->prog=>$pro,$this->fecg=>$fec,$this->plg=>$est,$this->cicg=>$cic,$this->numgra=>$numgra);		
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function verPiscina(){
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		public function verFecha(){
			$querycuenta=$this->db->query("select fecb from biogra_21 group by fecb");
			$tot=0;  //en el ciclo 2014-1 tenia el valor de -1
			foreach($querycuenta->result() as $rowcta):
				$tot+=1;
			endforeach;
			$this->db->select('fecb');
			$this->db->group_by($this->fecb);	
			$this->db->order_by($this->fecb,'DESC');
			$query=$this->db->get($this->tablabiogra, $tot);
			
			return $query->result();			
		}
		public function verFechaT(){
			$querycuenta=$this->db->query("select count(*) as sem from biogra_21 group by fecb");
			$tot=0;  
			foreach($querycuenta->result() as $rowcta):
				$tot+=1;
			endforeach;
			return $tot;			
		}
		function getBiometria($filter){
			//$ciclo=15;
			$this->db->select('idbio,idpisb,pesb,fecb,cicb,obsb,pisg,idpis,incest');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisb, 'inner');
			//$this->db->order_by('idpisb','DESC');
			$this->db->order_by('pisg','DESC');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablabiogra,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablabiogra);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$fec=new Libreria();
			foreach($result->result() as $row):
				$row->fecb1 = $fec->fecha($row->fecb);
				$row->pesb=number_format($row->pesb, 3, '.', ',');
				if($row->incest>0) $row->incest=number_format($row->incest, 3, '.', ','); else $row->incest='';
				$data[] = $row;		
			endforeach;
			return $data;
		}
		function getNumRowsB($filter){
			//$ciclo=15;
			//$this->db->select('idc,cfec,clts,cvale,cobs,NomUni');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisb, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablabiogra);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function actualizarbio($id,$pis,$fec,$pes,$cic,$obs,$gra,$inc){
			$pes = str_replace(",", "", $pes);$inc = str_replace(",", "", $inc);
			$data=array($this->idpisb=>$pis,$this->fecb=>$fec,$this->pesb=>$pes,$this->cicb=>$cic,$this->obsb=>$obs,$this->grab=>$gra,$this->incest=>$inc);
			$this->db->where($this->idbio,$id);
			$this->db->update($this->tablabiogra,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarbio($pis,$fec,$pes,$cic,$obs,$gra,$inc){
			$pes = str_replace(",", "", $pes);$inc = str_replace(",", "", $inc);
			$data=array($this->idpisb=>$pis,$this->fecb=>$fec,$this->pesb=>$pes,$this->cicb=>$cic,$this->obsb=>$obs,$this->grab=>$gra,$this->incest=>$inc);		
			$this->db->insert($this->tablabiogra,$data);
			return $this->db->insert_id();
		}
		
		function getSobrevivencia($filter){
			//$ciclo=15;
			//$this->db->select('idbio,idpisb,pesb,fecb,cicb,obsb,pisg,idpis');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpiss, 'inner');
			//$this->db->order_by('idpiss','DESC');
			$this->db->order_by('pisg','DESC');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablabiogra,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablasobgra);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$fec=new Libreria();
			foreach($result->result() as $row):
				$row->fecs1 = $fec->fecha($row->fecs);
				$row->sobp=number_format($row->sobp, 2, '.', ',');
				$data[] = $row;		
			endforeach;
			return $data;
		}
		function getNumRowsS($filter){
			//$ciclo=15;
			//$this->db->select('idc,cfec,clts,cvale,cobs,NomUni');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpiss, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablasobgra);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function actualizarsob($id,$pis,$fec,$sob,$cic,$obs,$gra,$cam,$has,$org){
			if($sob=='' and $cam!=''){
				$sob=(($cam*10000*$has)/($org*1000))*100;	
			}else{
				$sob = str_replace(",", "", $sob);
			}
			$data=array($this->idpiss=>$pis,$this->fecs=>$fec,$this->sobp=>$sob,$this->cics=>$cic,$this->obss=>$obs,$this->gras=>$gra,$this->camm2=>$cam);
			$this->db->where($this->idsob,$id);
			$this->db->update($this->tablasobgra,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarsob($pis,$fec,$sob,$cic,$obs,$gra,$cam,$has,$org){
			if($sob=='' and $cam!=''){
				$sob=(($cam*10000*$has)/($org*1000))*100;	
			}else{
				$sob = str_replace(",", "", $sob);
			}
			$data=array($this->idpiss=>$pis,$this->fecs=>$fec,$this->sobp=>$sob,$this->cics=>$cic,$this->obss=>$obs,$this->gras=>$gra,$this->camm2=>$cam);		
			$this->db->insert($this->tablasobgra,$data);
			return $this->db->insert_id();
		}
		
		public function actualizarali($id,$pis,$fec,$can,$cic,$obs,$gra){
			$can = str_replace(",", "", $can);
			$data=array($this->idpisali=>$pis,$this->fecali=>$fec,$this->canali=>$can,$this->cicali=>$cic,$this->obsali=>$obs,$this->graa=>$gra);
			$this->db->where($this->idali,$id);
			$this->db->update($this->tablaaligra,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarali($pis,$fec,$can,$cic,$obs,$gra){
			$can = str_replace(",", "", $can);
			$data=array($this->idpisali=>$pis,$this->fecali=>$fec,$this->canali=>$can,$this->cicali=>$cic,$this->obsali=>$obs,$this->graa=>$gra);		
			$this->db->insert($this->tablaaligra,$data);
			return $this->db->insert_id();
		}
		
		function getParametrosg($filter){
			//$ciclo=15;
			//$this->db->select('idpfq,idpisfq,fecfq,t1fq,o1fq,salfq,turfq,t2fq,o2fq,phfq,cicfq,salud,reca');
			$this->db->select('idpischa,pisg,feccha,kgd');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpischa, 'inner');
			$this->db->order_by('feccha');
			$this->db->order_by('pisg');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablapargra,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablachagra);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$est=0;$feca='';
			//Se forma el arreglo que sera retornado
			$fec=new Libreria();
			foreach($result->result() as $row):
				$est=$row->idpischa;$feca=$row->feccha;
				$row->fecfq1 = $fec->fecha($row->feccha);
				if($row->kgd>0)$row->kgd=number_format($row->kgd, 2, '.', ','); else $row->kgd='';
				$row->t1fq='';$row->o1fq=''; $row->t2fq='';$row->o2fq='';	
				
				$query=$this->db->query("select	t1fq,o1fq,t2fq,o2fq from pargra_21 where idpisfq='$est' and fecfq='$feca'");
				foreach($query->result() as $rowf):
					if($rowf->t1fq>0)$row->t1fq=number_format($rowf->t1fq, 1, '.', ','); 
					if($rowf->o1fq>0)$row->o1fq=number_format($rowf->o1fq, 1, '.', ','); 
					if($rowf->t2fq>0)$row->t2fq=number_format($rowf->t2fq, 1, '.', ','); 
					if($rowf->o2fq>0)$row->o2fq=number_format($rowf->o2fq, 1, '.', ','); 		
				endforeach;
				/*if($row->t1fq>0)$row->t1fq=number_format($row->t1fq, 1, '.', ','); else $row->t1fq='';
				if($row->o1fq>0)$row->o1fq=number_format($row->o1fq, 1, '.', ','); else $row->o1fq='';
				if($row->t2fq>0)$row->t2fq=number_format($row->t2fq, 1, '.', ','); else $row->t2fq='';
				if($row->o2fq>0)$row->o2fq=number_format($row->o2fq, 1, '.', ','); else $row->o2fq='';
				if($row->reca>0)$row->reca=number_format($row->reca, 1, '.', ','); else $row->reca='';
				if($row->salfq<=0)$row->salfq='';if($row->turfq<=0)$row->turfq='';if($row->phfq<=0)$row->phfq='';
				 * */
				$data[] = $row;		
			endforeach;
			return $data;
		}
		
		function getParametros($filter){
			//$ciclo=15;
			$this->db->select('idpfq,idpisfq,fecfq,t1fq,o1fq,salfq,turfq,t2fq,o2fq,phfq,cicfq,pisg,idpis,salud,reca');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisfq, 'inner');
			//$this->db->order_by('idpisfq','DESC');
			$this->db->order_by('pisg','DESC');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablapargra,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablapargra);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$fec=new Libreria();
			foreach($result->result() as $row):
				$row->fecfq1 = $fec->fecha($row->fecfq);
				if($row->t1fq>0)$row->t1fq=number_format($row->t1fq, 1, '.', ','); else $row->t1fq='';
				if($row->o1fq>0)$row->o1fq=number_format($row->o1fq, 1, '.', ','); else $row->o1fq='';
				if($row->t2fq>0)$row->t2fq=number_format($row->t2fq, 1, '.', ','); else $row->t2fq='';
				if($row->o2fq>0)$row->o2fq=number_format($row->o2fq, 1, '.', ','); else $row->o2fq='';
				if($row->reca>0)$row->reca=number_format($row->reca, 1, '.', ','); else $row->reca='';
				if($row->salfq<=0)$row->salfq='';if($row->turfq<=0)$row->turfq='';if($row->phfq<=0)$row->phfq='';
				$data[] = $row;		
			endforeach;
			return $data;
		}
		function getNumRowsP($filter){
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisfq, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablapargra);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function actualizarpar($id,$pis,$fec,$t1,$o1,$t2,$o2,$sal,$tur,$ph,$cic,$gra,$salu,$reca){
			$data=array($this->idpisfq=>$pis,$this->fecfq=>$fec,$this->t1fq=>$t1,$this->o1fq=>$o1,$this->t2fq=>$t2,$this->o2fq=>$o2,$this->salfq=>$sal,$this->turfq=>$tur,$this->phfq=>$ph,$this->cicfq=>$cic,$this->grap=>$gra,$this->salu=>$salu,$this->reca=>$reca);
			$this->db->where($this->idpfq,$id);
			$this->db->update($this->tablapargra,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarpar($pis,$fec,$t1,$o1,$t2,$o2,$sal,$tur,$ph,$cic,$gra,$salu,$reca){
			$data=array($this->idpisfq=>$pis,$this->fecfq=>$fec,$this->t1fq=>$t1,$this->o1fq=>$o1,$this->t2fq=>$t2,$this->o2fq=>$o2,$this->salfq=>$sal,$this->turfq=>$tur,$this->phfq=>$ph,$this->cicfq=>$cic,$this->grap=>$gra,$this->salu=>$salu,$this->reca=>$reca);		
			$this->db->insert($this->tablapargra,$data);
			return $this->db->insert_id();
		}
		function getDatosEst($filter){			
			$this->db->order_by($this->pisg);		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',').' m2';
				$row->orgg = number_format($row->orgg, 3, '.', ',').' Pl´s';
				$row->hasg = number_format($row->hasg, 2, '.', ',');
				$row->fecg1 = $fec->fecha($row->fecg);
				$row->plg='PL'.$row->plg;
				$data[] = $row;	
			endforeach;	
						
			return $data;
		}
		function getNumRowDE($filter){					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getDatosEstDat($filter){
			//select fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb from biogra_21 inner join siegra_21 on idpis=idpisb where idpisb=2 order by fecb			
			$this->db->select('idpis,fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb,(select pors from sobgra where idsob=dias) as por,orgg,hasg,fecgc,DATEDIFF( fecgc,fecg ) AS dc,biogc,ppgc,obsc,(SELECT sum(canali) from aligra_21 where idpisali=idpisb and (fecali<=fecb))as alic',NULL,FALSE);
			$this->db->join($this->tablabiogra, $this->idpis.'='.$this->idpisb,'inner');
			$this->db->join($this->tablapargra, $this->idpfq.'='.$this->idpisb,'left');
			
			//$this->db->join($this->tablacos, $this->cpisg.'='.$this->idpis,'left');
			$this->db->order_by($this->fecb,'ASC');		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$c=1;$a=1;$ant=0;$dia=0;$antach=0;$biomasa=0;$ac='';$an='';$aliss=0;$cont=0;$inc=0;
			//Se forma el arreglo que sera retornado	
			if($result->num_rows()>0){						
			foreach ($result->result() as $row):
				/*$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',').' m2';*/
				$est=$row->idpis; 
				$cont+=1;
				if($cont==1){ $row->d1=$row->dias;}
				if($cont==2){ $row->d2=$row->dias;}
				if($cont==3){ $row->d3=$row->dias;}
				if($cont==4){ $row->d4=$row->dias;}
				if($cont==5){ $row->d5=$row->dias;}
				if($cont==6){ $row->d6=$row->dias;}
				if($cont==7){ $row->d7=$row->dias;}
				if($cont==8){ $row->d8=$row->dias;}
				if($cont==9){ $row->d9=$row->dias;}
				if($cont==10){ $row->d10=$row->dias;}
				if($cont==11){ $row->d11=$row->dias;}
				if($cont==12){ $row->d12=$row->dias;}
				if($cont==13){ $row->d13=$row->dias;}
				if($cont==14){ $row->d14=$row->dias;}
				$row->pesb = number_format($row->pesb, 3, '.', ',');
				
				//if($c>1 && (($row->pesb-$inc)>0)){$row->incp=number_format($row->pesb-$inc, 2, '.', ',');}else{$row->incp =''; }
				if((($row->pesb-$inc)>0)){$row->incp=number_format($row->pesb-$inc, 3, '.', ',');}else{$row->incp =''; }
				//$c=2;
				$x=0;
				$inc=$row->pesb;
				$row->sem = number_format($row->sem, 0, '.', ',');
				$row->prop= number_format($row->pesb/$row->sem, 2, '.', ',');
				$row->por= number_format($row->por, 2, '.', ',');
				
				//$row->orgs= number_format(($row->orgg*$row->por)/100, 3, '.', ',');
				//$row->orgsm= number_format(($row->orgs/($row->hasg*10)), 1, '.', ',');
				$row->orgs= number_format(($row->orgg*$row->por)/100, 3, '.', ',');
				$row->orgsm= number_format(((($row->orgg*$row->por)/100)/($row->hasg*10)), 1, '.', ',');
				
				
				//if($a<=1){$row->fca='';}else{$x=number_format(($row->fcs/($row->pesb*$row->orgs)), 2, '.', ',');}
				$row->bio= number_format(($row->pesb*$row->orgs), 0, '.', ',');
				$biomasa = str_replace(",", "", $row->bio);
				$row->bioh= number_format((($row->pesb*$row->orgs)/$row->hasg), 0, '.', ',');
				$row->fecb2 = $fec->fecha($row->fecb);	
				$alifinal=$row->alic;
				if($cont==1){ $row->b1=str_replace(",", "", $row->bio);$row->t1=$row->pesb;$row->fe1=$row->fecb2;}
				if($cont==2){ $row->b2=str_replace(",", "", $row->bio);$row->t2=$row->pesb;$row->f2=$row->fecb2;}
				if($cont==3){ $row->b3=str_replace(",", "", $row->bio);$row->t3=$row->pesb;$row->f3=$row->fecb2;}
				if($cont==4){ $row->b4=str_replace(",", "", $row->bio);$row->t4=$row->pesb;$row->f4=$row->fecb2;}
				if($cont==5){ $row->b5=str_replace(",", "", $row->bio);$row->t5=$row->pesb;$row->f5=$row->fecb2;}
				if($cont==6){ $row->b6=str_replace(",", "", $row->bio);$row->t6=$row->pesb;$row->f6=$row->fecb2;}
				if($cont==7){ $row->b7=str_replace(",", "", $row->bio);$row->t7=$row->pesb;$row->f7=$row->fecb2;}
				if($cont==8){ $row->b8=str_replace(",", "", $row->bio);$row->t8=$row->pesb;$row->f8=$row->fecb2;}
				if($cont==9){ $row->b9=str_replace(",", "", $row->bio);$row->t9=$row->pesb;$row->f9=$row->fecb2;}
				if($cont==10){ $row->b10=str_replace(",", "", $row->bio);$row->t10=$row->pesb;$row->f10=$row->fecb2;}
				if($cont==11){ $row->b11=str_replace(",", "", $row->bio);$row->t11=$row->pesb;$row->f11=$row->fecb2;}
				if($cont==12){ $row->b12=str_replace(",", "", $row->bio);$row->t12=$row->pesb;$row->f12=$row->fecb2;}
				if($cont==13){ $row->b13=str_replace(",", "", $row->bio);$row->t13=$row->pesb;$row->f13=$row->fecb2;}
				if($cont==14){ $row->b14=str_replace(",", "", $row->bio);$row->t14=$row->pesb;$row->f14=$row->fecb2;}
				
				if($a<=1){$row->alis=number_format(($row->alic), 2, '.', ',');
				$row->ahd=number_format(($row->alis/$row->hasg/$row->dias), 2, '.', ',');
				$row->f1='';$row->fcs='';$row->fca='';$row->ach=number_format(($row->alis/$row->orgsm), 2, '.', ',');$antach=$row->ach;				
				$an=$row->fecb;
				}
				else{
					$row->alis=number_format(($row->alic-$ant), 2, '.', ',');$aliss=$row->alic-$ant;
					$row->ahd=number_format(($aliss/$row->hasg/($row->dias-$dia)), 2, '.', ',');
					$row->f1=number_format(($row->ahd/$row->orgsm), 2, '.', ',');
					if($row->incp>0)$row->fcs=number_format(($aliss/((($row->incp*($row->orgs*1000))/1000))), 2, '.', ','); else $row->fcs='';
					if($biomasa>0)$row->fca=number_format(($row->alic/$biomasa), 2, '.', ','); else $row->fca='';
					$row->ach=number_format((($aliss/$row->hasg)+$antach), 2, '.', ',');
					$antach=$row->ach;
					$an=$ac;	
				}
				
				if($cont==1){ $row->a1=str_replace(",", "", $row->ahd);}
				if($cont==2){ $row->a2=str_replace(",", "", $row->ahd);}
				if($cont==3){ $row->a3=str_replace(",", "", $row->ahd);}
				if($cont==4){ $row->a4=str_replace(",", "", $row->ahd);}
				if($cont==5){ $row->a5=str_replace(",", "", $row->ahd);}
				if($cont==6){ $row->a6=str_replace(",", "", $row->ahd);}
				if($cont==7){ $row->a7=str_replace(",", "", $row->ahd);}
				if($cont==8){ $row->a8=str_replace(",", "", $row->ahd);}
				if($cont==9){ $row->a9=str_replace(",", "", $row->ahd);}
				if($cont==10){ $row->a10=str_replace(",", "", $row->ahd);}
				if($cont==11){ $row->a11=str_replace(",", "", $row->ahd);}
				if($cont==12){ $row->a12=str_replace(",", "", $row->ahd);}
				if($cont==13){ $row->a13=str_replace(",", "", $row->ahd);}
				if($cont==14){ $row->a14=str_replace(",", "", $row->ahd);}
				$dia=$row->dias;
				$ant=$row->alic;
				$row->alic=number_format(($row->alic), 0, '.', ',');
				
				$ac=$row->fecb;
				if($a<=1){
					$query=$this->db->query("select	min(o1fq)as omin from pargra_21 where o1fq>0 and idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omin>0){$row->omin1=number_format(($rowfqo1->omin), 1, '.', ',');$row->omin=number_format(($rowfqo1->omin), 1, '.', ',');}else{$row->omin='';$row->omin1=0;}					
				endforeach;
				$query=$this->db->query("select	max(o2fq)as omax from pargra_21 where o2fq>0 and idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omax>0){$row->omax1=number_format(($rowfqo1->omax), 1, '.', ',');$row->omax=number_format(($rowfqo1->omax), 1, '.', ',');}else{$row->omax='';$row->omax1=0;}					
				endforeach;
				$query=$this->db->query("select	min(t1fq)as tmin from pargra_21 where idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmin>0){$row->tmin=number_format(($rowfqo1->tmin), 1, '.', ',');}else{$row->tmin='';}					
				endforeach;		
				$query=$this->db->query("select	max(t2fq)as tmax from pargra_21 where idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmax>0){$row->tmax=number_format(($rowfqo1->tmax), 1, '.', ',');}else{$row->tmax='';}					
				endforeach;			 	
				$query=$this->db->query("select	avg(salfq)as sal from pargra_21 where salfq>0 and idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->sal>0){$row->sal=number_format(($rowfqo1->sal), 0, '.', ',');}else{$row->sal='';}					
				endforeach;
				$query=$this->db->query("select	avg(turfq)as tur from pargra_21 where turfq>0 and idpisfq=$est and (fecfq>='$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tur>0){$row->tur=number_format(($rowfqo1->tur), 0, '.', ',');}else{$row->tur='';}					
				endforeach;
				}else{
				$query=$this->db->query("select	min(o1fq)as omin from pargra_21 where o1fq>0 and idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omin>0){$row->omin1=number_format(($rowfqo1->omin), 1, '.', ',');$row->omin=number_format(($rowfqo1->omin), 1, '.', ',');}else{$row->omin='';$row->omin1=0;}					
				endforeach;
				$query=$this->db->query("select	max(o2fq)as omax from pargra_21 where o2fq>0 and idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omax>0){$row->omax1=number_format(($rowfqo1->omax), 1, '.', ',');$row->omax=number_format(($rowfqo1->omax), 1, '.', ',');}else{$row->omax='';$row->omax1=0;}					
				endforeach;
				$query=$this->db->query("select	min(t1fq)as tmin from pargra_21 where idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmin>0){$row->tmin=number_format(($rowfqo1->tmin), 1, '.', ',');}else{$row->tmin='';}					
				endforeach;		
				$query=$this->db->query("select	max(t2fq)as tmax from pargra_21 where idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmax>0){$row->tmax=number_format(($rowfqo1->tmax), 1, '.', ',');}else{$row->tmax='';}					
				endforeach;			 	
				$query=$this->db->query("select	avg(salfq)as sal from pargra_21 where salfq>0 and idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->sal>0){$row->sal=number_format(($rowfqo1->sal), 0, '.', ',');}else{$row->sal='';}					
				endforeach;
				$query=$this->db->query("select	avg(turfq)as tur from pargra_21 where turfq>0 and idpisfq=$est and (fecfq>'$an' and fecfq<='$ac')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tur>0){$row->tur=number_format(($rowfqo1->tur), 0, '.', ',');}else{$row->tur='';}					
				endforeach;		
				}	
				if($cont==1){ $row->tm1=$row->tmin;$row->tx1=$row->tmax;$row->om1=$row->omin1;$row->ox1=$row->omax1;$row->s1=$row->sal;}
				if($cont==2){ $row->tm2=$row->tmin;$row->tx2=$row->tmax;$row->om2=$row->omin1;$row->ox2=$row->omax1;$row->s2=$row->sal;}
				if($cont==3){ $row->tm3=$row->tmin;$row->tx3=$row->tmax;$row->om3=$row->omin1;$row->ox3=$row->omax1;$row->s3=$row->sal;}
				if($cont==4){ $row->tm4=$row->tmin;$row->tx4=$row->tmax;$row->om4=$row->omin1;$row->ox4=$row->omax1;$row->s4=$row->sal;}
				if($cont==5){ $row->tm5=$row->tmin;$row->tx5=$row->tmax;$row->om5=$row->omin1;$row->ox5=$row->omax1;$row->s5=$row->sal;}
				if($cont==6){ $row->tm6=$row->tmin;$row->tx6=$row->tmax;$row->om6=$row->omin1;$row->ox6=$row->omax1;$row->s6=$row->sal;}
				if($cont==7){ $row->tm7=$row->tmin;$row->tx7=$row->tmax;$row->om7=$row->omin1;$row->ox7=$row->omax1;$row->s7=$row->sal;}
				if($cont==8){ $row->tm8=$row->tmin;$row->tx8=$row->tmax;$row->om8=$row->omin1;$row->ox8=$row->omax1;$row->s8=$row->sal;}
				if($cont==9){ $row->tm9=$row->tmin;$row->tx9=$row->tmax;$row->om9=$row->omin1;$row->ox9=$row->omax1;$row->s9=$row->sal;}
				if($cont==10){ $row->tm10=$row->tmin;$row->tx10=$row->tmax;$row->om10=$row->omin1;$row->ox10=$row->omax1;$row->s10=$row->sal;}
				if($cont==11){ $row->tm11=$row->tmin;$row->tx11=$row->tmax;$row->om11=$row->omin1;$row->ox11=$row->omax1;$row->s11=$row->sal;}
				if($cont==12){ $row->tm12=$row->tmin;$row->tx12=$row->tmax;$row->om12=$row->omin1;$row->ox12=$row->omax1;$row->s12=$row->sal;}
				if($cont==13){ $row->tm13=$row->tmin;$row->tx13=$row->tmax;$row->om13=$row->omin1;$row->ox13=$row->omax1;$row->s13=$row->sal;}
				if($cont==14){ $row->tm14=$row->tmin;$row->tx14=$row->tmax;$row->om14=$row->omin1;$row->ox14=$row->omax1;$row->s14=$row->sal;}
				$data[] = $row;
				$a=2;
			endforeach;
			$row->obsc1=$row->obsc;$row->ppgc1=$row->ppgc;$row->biogc1=$row->biogc;$row->fecgc1=$row->fecgc;	
			if($row->ppgc>0 && $row->biogc && $row->fecgc!=0000-00-00){
			$row->fcfin=$fec->fecha($row->fecgc);$row->dcfin=$row->dc;$row->tpfin=number_format(($row->ppgc), 2, '.', ',');
			$row->sobfin=number_format((($row->biogc/$row->ppgc)/$row->orgg)*100, 2, '.', ',').'%';$row->orgsfin=number_format(($row->biogc/$row->ppgc), 3, '.', ',');
			$row->biot=number_format(($row->biogc), 1, '.', ',').' kg.';$row->bioha=number_format(($row->biogc/$row->hasg), 0, '.', ',').' kg.';
			$row->alifin=number_format(($alifinal), 2, '.', ',').' kg.';
			$row->fcafin=number_format(($alifinal/$row->biogc), 2, '.', ',');
			}
			}
			return $data;
		}
		function getNumRowDED($filter){
			$this->db->select('fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb',NULL,FALSE);
			$this->db->join($this->tablabiogra, $this->idpis.'='.$this->idpisb,'inner');					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function buscar($id){
			/*$query=$this->db->get($this->tabla);*/
			$this->db->select($this->pisg);
			$query=$this->db->get_where($this->tabla,array($this->idpis=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->pisg;
			}
			return 0;
		}
		function getAlimentos($filter){
			$this->db->select('idali,idpisali,canali,fecali,cicali,obsali,pisg,idpis');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisali, 'inner');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablaaligra,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaaligra);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$fec=new Libreria();
			foreach($result->result() as $row):
				$row->feca1 = $fec->fecha($row->fecali);
				$row->canali=number_format($row->canali, 2, '.', ',');
				$data[] = $row;		
			endforeach;
			return $data;
		}
		function getNumRowsA($filter){
			//$this->db->select('idc,cfec,clts,cvale,cobs,NomUni');
			$this->db->join($this->tabla,$this->idpis .'='.$this->idpisali, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaaligra);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getDatosEstSem($filter,$fecact,$cic){
			//$fecatc='2019-06-14';
			//$this->db->select('idpis,fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb,(select pors from sobgra where idsob=dias) as por,orgg,hasg,(SELECT sum(canali) from aligra_21 where idpisali=idpisb and (fecali<=fecb))as alic',NULL,FALSE);
			//$this->db->select('idpis,pisg,fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb,(select pors from sobgra where idsob=dias) as por,orgg,hasg,(SELECT sum(canali) from aligra_21 where idpisali=idpisb and (fecali<=fecb))as alic',NULL,FALSE);
			//if(($filter['granja'])==4)$this->db->select('idpis,pisg,fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb,(select pors from sobgra where idsob=dias) as por,orgg,hasg,(SELECT sum(canali) from aligra_21 where idpisali=idpisb and (fecali<=fecb))as alic',NULL,FALSE);
			if(($filter['granja'])==4)$this->db->select("idpis,pisg,fecg,fecb,cicg,,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb,secc,(select pors from sobgra where idsob=dias) as por,orgg,hasg,(SELECT sum(kgd) from aligracha_21 where idpischa=idpisb and (feccha<=fecb)  and ciccha='$cic')as alic,",NULL,FALSE);
			//(select incpp from proygra_21 where diasp=dias and seccp=secc and ciclop=1)as incpp,",NULL,FALSE);
			$this->db->join($this->tablabiogra, $this->idpis.'='.$this->idpisb,'inner');
			$this->db->join($this->tablapargra, $this->idpfq.'='.$this->idpisb,'left');
			//$this->db->order_by($this->idpis,'ASC');	
			$this->db->order_by('pisg','ASC');
			$this->db->order_by($this->fecb,'ASC');	
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$c=1;$a=1;$ant=0;$dia=0;$antach=0;$biomasa=0;$ac='';$an='';$aliss=0;
			$shasgg=0;$sbio=0;$salis=0;$salic=0;
			$cdias=0;$sdias=0;$spesb=0;$spesa=0;$sinc3=0;$sisp=0;$sincp=0;$spor=0;$sorgsm=0;$sbioh=0;$sah=0;$torgact=0;
			$ts=0;$tb=0;$sts=0;$stb=0;$ats=0;$atb=0;$ssb=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$aliant=0;
				$est=$row->pisg;	$fecbus=$fecact;	$shasgg+=$row->hasg;
				$sdias+=$row->dias;	$cdias+=1; $spesb+=$row->pesb;
				/*$row->den=number_format((($row->orgg*1000)/$row->hasg/10000), 1, '.', ',').' m2';*/
				//buscar los datos de la proyeccion de acuerdo a los dias
				if($cic=='2021-2') $ciclopro=2; else $ciclopro=1;
				//$querypro=$this->db->query("select pesbp,incpp,inc3p,porp,orgsmp,fcap from proygra_21 where diasp=$row->dias and seccp=$row->secc and ciclop=$ciclopro");
				$querypro=$this->db->query("select pesbp,incpp,inc3p,porp,orgsmp,fcap from proygra_21 where diasp<=$row->dias and ciclop=$ciclopro");
				foreach($querypro->result() as $rowpro):
					$row->incpp = number_format($rowpro->incpp, 3, '.', ','); 
					$row->inc3p = number_format($rowpro->inc3p, 3, '.', ',');
					$row->porp = number_format($rowpro->porp, 2, '.', ',');
					$row->orgsmp = number_format($rowpro->orgsmp, 1, '.', ',');
					$row->fcap = number_format($rowpro->fcap, 2, '.', ',');
				endforeach;	
				//buscar el porcentaje de sobrevivencia la fecha de busqueda
				$row->iporp ='';$row->pporp ='';$camm2=0;
				$queryinc=$this->db->query("select sobp,camm2 from siegra_21 inner join sobgra_21 on idpiss=idpis where idpiss='$est' and fecs = '$fecbus' and cics = '$cic'");
				foreach($queryinc->result() as $rowsob):
					$spor+=$rowsob->sobp;$camm2=$rowsob->camm2;
					$row->por =number_format(($rowsob->sobp), 0, '.', ','); 
					$row->iporp =number_format(($rowsob->sobp-$rowpro->porp), 2, '.', ',');
					$row->pporp = number_format((($rowsob->sobp-$rowpro->porp)/$rowpro->porp)*100, 2, '.', ',');
				endforeach;	
				
				$row->ipesb = number_format($row->pesb-$rowpro->pesbp, 3, '.', ',');$row->ppesb =number_format((($row->pesb-$rowpro->pesbp)/$rowpro->pesbp)*100, 0, '.', ',');
				$row->pesb = number_format($row->pesb, 3, '.', ',');$row->pesbp = number_format($rowpro->pesbp, 3, '.', ',');
				$row->hasgg=number_format($row->hasg, 1, '.', ',');
				$row->fecg1 = $fec->fecha21($row->fecg);
				$row->orgs1= number_format(($row->orgg), 3, '.', ',');
				$row->orgsm1= number_format(((($row->orgg))/($row->hasg*10)), 0, '.', ',');
				//if($c>1 && (($row->pesb-$inc)>0)){$row->incp=number_format($row->pesb-$inc, 2, '.', ',');;}else{$row->incp =''; }
				//$inc=$row->pesb;
				//select fecb from siegra_21 inner join biogra_21 on idpisb=idpis where fecb<'2015-07-4' group by fecb order by fecb DESC limit 1
				//$entro=0;
				$row->incp =0;$row->pesa =0;$row->bioi =0;$pesoant=0;$orgsant=0;$row->inc3 =0;$row->isp =0;
				$inc1=0;$inc2=0;$inc3=0; $continc=0; $sumax=0;
				
				$row->sem = number_format($row->sem, 0, '.', ',');
				if($row->sem>0) $row->prop= number_format($row->pesb/$row->sem, 2, '.', ','); else $row->prop=number_format($row->pesb/1, 2, '.', ',');
				$row->por= number_format($row->por, 0, '.', ',');
				
				//$row->orgs= number_format(($row->orgg*$row->por)/100, 3, '.', ',');
				//$row->orgsm= number_format(($row->orgs/($row->hasg*10)), 1, '.', ',');
				$orgsbio= ($row->orgg*$row->por)/100;
				//$row->orgs= number_format(($row->orgg*$row->por)/100, 3, '.', ',');$torgact+=($row->orgg*$row->por)/100; 
				$row->orgs= number_format(($row->hasg*$camm2)*10000, 0, '.', ',');
				$torgact+=($row->hasg*$camm2)*10000;
				
				$query=$this->db->query("select fecb from siegra_21 inner join biogra_21 on idpisb=pisg where fecb < '$fecact' group by fecb order by fecb DESC limit 1");
				foreach($query->result() as $rowfqo1):
					//if($rowfqo1->tmax>0){$row->tmax=number_format(($rowfqo1->tmax), 1, '.', ',');}else{$row->tmax='';}	
					$fecbus =$rowfqo1->fecb;
					//busca el peso anterior
					$queryinc=$this->db->query("select (pesb) as inc from siegra_21 inner join biogra_21 on idpisb=idpis where idpisb='$est' and fecb = '$fecbus' and cicb='$cic' ");
					foreach($queryinc->result() as $rowinc):
						$row->incp =number_format(($row->pesb-$rowinc->inc), 3, '.', ','); $inc1=$row->pesb-$rowinc->inc;$continc+=1;$sincp+=$row->pesb-$rowinc->inc;
						$row->iincp = number_format($inc1-$rowpro->incpp, 3, '.', ',');
						$row->pincp =number_format((($inc1-$rowpro->incpp)/$rowpro->incpp)*100, 0, '.', ',');
						$row->isp =number_format((($row->pesb-$rowinc->inc)/$row->sem), 3, '.', ','); $sisp+=$row->isp;
						$row->pesa =number_format(($rowinc->inc), 3, '.', ',');  $spesa+=$rowinc->inc;
						$pesoant=number_format(($rowinc->inc), 2, '.', ',');
						$pesoant = str_replace(",", "", $pesoant);
					endforeach;	
					//busca el peso para obtener el segundo incremento
					//$row->inc3 =number_format(($inc1), 3, '.', ',');
					$ir2=0;
					$fecinc3=date("Y-m-d",strtotime($fecbus."- 1 week"));
					$queryinc=$this->db->query("select (pesb) as inc from siegra_21 inner join biogra_21 on idpisb=idpis where idpisb='$est' and fecb = '$fecinc3' and cicb='$cic' ");
					foreach($queryinc->result() as $rowinc2):
						//$row->inc3 =number_format(($rowinc2->inc-$rowinc->inc), 3, '.', ','); 
						 $inc2=$rowinc->inc-$rowinc2->inc;$continc+=1;$ir2=$rowinc2->inc;
					endforeach;
					
					//$row->inc3 =$inc1.'-'.$inc2;
					//busca el peso para obtener el tercer incremento
					//$row->inc3 =number_format(($inc2), 3, '.', ',');
					$fecinc4=date("Y-m-d",strtotime($fecbus."- 2 week"));
					$queryinc=$this->db->query("select (pesb) as inc from siegra_21 inner join biogra_21 on idpisb=idpis where idpisb='$est' and fecb = '$fecinc4' and cicb='$cic'");
					foreach($queryinc->result() as $rowinc3):
						//$row->inc3 =number_format(($rowinc2->inc-$rowinc->inc), 3, '.', ','); 
						 $inc3=$ir2-$rowinc3->inc;$continc+=1;
					endforeach;
					$sumax=$inc1+$inc2+$inc3;
					//$row->inc3 =$fecbus.' '.$fecinc3.' '.$fecinc4;
					//$row->inc3 =$inc1.'-'.$inc2.'-'.$inc3;
					if($sumax>0){
						$row->inc3 =number_format(($sumax)/$continc, 3, '.', ','); $sinc3+=$row->inc3;
						$row->iinc3 =number_format(($sumax/$continc)-$rowpro->inc3p, 3, '.', ',');
						if($rowpro->inc3p>0) $row->pinc3 =number_format((((($sumax/$continc)-$rowpro->inc3p))/$rowpro->inc3p )*100, 0, '.', ',');
						else $row->pinc3 ='';
					}
					
					//busca el alimento de la semana anterior
					//if(($filter['granja'])==2) $queryali=$this->db->query("SELECT sum(canali) as ant from aligra_21 where idpisali='$est' and fecali>'$fecbus' and fecali<='$fecact'");
					//if(($filter['granja'])==4) 
					$queryali=$this->db->query("SELECT sum(kgd) as ant from aligracha_21 where idpischa='$est' and feccha>'$fecbus' and feccha<='$fecact' and ciccha='$cic'");
					foreach($queryali->result() as $rowali):
						$aliant =$rowali->ant;
					endforeach;
					// para biomasa
					//busca la sobrevivencia para obener los organismos
					$querysob=$this->db->query("select (sobp) as soba from siegra_21 inner join sobgra_21 on idpiss=idpis where idpiss='$est' and fecs = '$fecbus' and cics='$cic' ");
					foreach($querysob->result() as $rowsob):
						$row->bioi =number_format(($row->pesb*$orgsbio)-(($pesoant*($row->orgs1*$rowsob->soba))/100), 0, '.', ',');
					endforeach;	
				endforeach;	
				
				$c=2;$x=0;
				
				//$row->orgsm= number_format(((($row->orgg*$row->por)/100)/($row->hasg*10)), 1, '.', ','); 
				$row->orgsm= number_format($camm2, 1, '.', ',');
				$sorgsm+=(($row->orgg*$row->por)/100)/($row->hasg*10);
				$iorgsm=((($row->orgg*$row->por)/100)/($row->hasg*10)-$rowpro->orgsmp);				
				$row->iorgsm=number_format(((($row->orgg*$row->por)/100)/($row->hasg*10)-$rowpro->orgsmp), 1, '.', ',');
				$row->porgsm=number_format(($iorgsm/$rowpro->orgsmp)*100, 1, '.', ',');
				//$row->bio= number_format(($row->pesb*$orgsbio), 0, '.', ',');$sbio+=$row->pesb*$orgsbio;
				$row->bio= number_format(($row->pesb*(($row->hasg*$camm2)*10000))/1000, 0, '.', ',');$sbio+=($row->pesb*(($row->hasg*$camm2)*10000))/1000;
				$biomasa = str_replace(",", "", $row->bio);
				$row->bioh= number_format((($row->pesb*$orgsbio)/$row->hasg), 0, '.', ','); $sbioh+=(($row->pesb*$orgsbio)/$row->hasg);
				$row->fecb2 = $fec->fecha($row->fecb);	
				
				/*if($a<=1){$row->alis=number_format(($row->alic), 2, '.', ',');
				$row->ahd=number_format(($row->alis/$row->hasg/$row->dias), 2, '.', ',');
				$row->f1='';$row->fcs='';$row->fca='';$row->ach=number_format(($row->alis/$row->orgsm), 2, '.', ',');$antach=$row->ach;
				$an=$row->fecb;
				}
				else{*/
				
					$row->alis=number_format(($aliant), 2, '.', ',');$aliss=$aliant;$salis+=$aliant;
					$row->ah=number_format(($aliant/$row->hasg/7), 2, '.', ','); $sah+=($aliant/$row->hasg);
					$row->adiario=number_format(($aliant/7), 2, '.', ',');
					$row->alicha=number_format(($row->alic/$row->hasg), 2, '.', ',');
					//$row->alis=number_format(($row->alic-$aliant), 2, '.', ',');
					if($aliss>0 )$row->ahd=number_format(($aliss/$row->hasg/7), 2, '.', ','); else $row->ahd=0;
					if($row->ahd>0 and $row->orgsm>0) $row->f1=number_format(($row->ahd/$row->orgsm), 2, '.', ','); else $row->f1='';
					if($row->incp>0 and (($row->orgs*1000)/1000)>0)$row->fcs=number_format(($aliss/((($row->incp*($row->orgs*1000))/1000))), 2, '.', ','); else $row->fcs='';
					if($biomasa>0){
						$row->fca=number_format(($row->alic/$biomasa), 2, '.', ',');
						$row->ifcap=number_format(($row->alic/$biomasa)-$rowpro->fcap, 2, '.', ',');
						$row->pfcap=number_format(($row->ifcap/$rowpro->fcap)*100, 2, '.', ',');
					}
					else {$row->fca='';}
					/*$row->ach=number_format((($row->alis/$row->hasg)+), 2, '.', ',');
					//$row->ach=number_format((($row->alis/$row->hasg)+$antach), 2, '.', ',');
					/*$antach=$row->ach;
					$an=$ac;*/	
				//}
				
				$dia=$row->dias;
				$ant=$row->alic;$salic+=$row->alic;
				$row->alic=number_format(($row->alic), 0, '.', ',');
				$row->alia =number_format(($ant-$aliant), 2, '.', ',');
				if($aliss>0 and $biomasa>0)$row->bw =number_format((($aliss/$biomasa)*100)/7, 1, '.', ','); else $row->bw ='';
				$ac=$row->fecb;
				
				/*if($fecact='2015-05-30'){
					$query=$this->db->query("select	min(o1fq)as omin from pargra_21 where o1fq>0 and idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omin>0){$row->omin=number_format(($rowfqo1->omin), 1, '.', ',');}else{$row->omin='';}					
				endforeach;
				$query=$this->db->query("select	max(o2fq)as omax from pargra_21 where o2fq>0 and idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omax>0){$row->omax=number_format(($rowfqo1->omax), 1, '.', ',');}else{$row->omax='';}					
				endforeach;
				$query=$this->db->query("select	min(t1fq)as tmin from pargra_21 where idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmin>0){$row->tmin=number_format(($rowfqo1->tmin), 1, '.', ',');}else{$row->tmin='';}					
				endforeach;		
				$query=$this->db->query("select	max(t2fq)as tmax from pargra_21 where idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmax>0){$row->tmax=number_format(($rowfqo1->tmax), 1, '.', ',');}else{$row->tmax='';}					
				endforeach;			 	
				$query=$this->db->query("select	avg(salfq)as sal from pargra_21 where salfq>0 and idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->sal>0){$row->sal=number_format(($rowfqo1->sal), 0, '.', ',');}else{$row->sal='';}					
				endforeach;
				$query=$this->db->query("select	avg(turfq)as tur from pargra_21 where turfq>0 and idpisfq=$est and (fecfq>='$fecbus' and fecfq<='$fecact')");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tur>0){$row->tur=number_format(($rowfqo1->tur), 0, '.', ',');}else{$row->tur='';}					
				endforeach;
				}else{*/
				$query=$this->db->query("select	min(o1fq)as omin from pargra_21 where o1fq>0 and idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omin>0){$row->omin=number_format(($rowfqo1->omin), 1, '.', ',');}else{$row->omin='';}					
				endforeach;
				$query=$this->db->query("select	max(o2fq)as omax from pargra_21 where o2fq>0 and idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->omax>0){$row->omax=number_format(($rowfqo1->omax), 1, '.', ',');}else{$row->omax='';}					
				endforeach;
				$query=$this->db->query("select	min(t1fq)as tmin from pargra_21 where idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmin>0){$row->tmin=number_format(($rowfqo1->tmin), 1, '.', ',');}else{$row->tmin='';}					
				endforeach;		
				$query=$this->db->query("select	max(t2fq)as tmax from pargra_21 where idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tmax>0){$row->tmax=number_format(($rowfqo1->tmax), 1, '.', ',');}else{$row->tmax='';}					
				endforeach;			 	
				$query=$this->db->query("select	avg(salfq)as sal from pargra_21 where salfq>0 and idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->sal>0){$row->sal=number_format(($rowfqo1->sal), 0, '.', ',');}else{$row->sal='';}					
				endforeach;
				$query=$this->db->query("select	avg(turfq)as tur from pargra_21 where turfq>0 and idpisfq=$est and (fecfq>'$fecbus' and fecfq<='$fecact') and cicfq='$cic'");
				foreach($query->result() as $rowfqo1):
					if($rowfqo1->tur>0){$row->tur=number_format(($rowfqo1->tur), 0, '.', ',');}else{$row->tur='';}					
				endforeach;		
				//}		
				$row->semsoy='';$row->acusoy='';$row->totsoy='';
				$row->sembal='';$row->acubal='';$row->totbal='';
				$row->soybal='';$row->kgshas='';
				//busca el alimento de
				$this->db->select('pisg,hasg,secc,sum(kgs) as tkgsi,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(porsoy) as tkgs,sum(porbal) as tkgb');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'join');
				$this->db->where('idpischa',$est);   
				$this->db->where('feccha <=',$fecact);
				//$this->db->group_by('idpischa');$this->db->group_by('hasg');$this->db->group_by('secc');
				//if($filter['where']!=''){	$this->db->where($filter['where']); }
				$resultali = $this->db->get($this->tablacha);
			
				//$thas=0;$tsi=0;$stsi=0;$atsi=0;$asb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;
				//$totkgds1s=0;$totkgds1b=0;$totkgds2s=0;$totkgds2b=0;$totkgds3s=0;$totkgds3b=0;$totkgds4s=0;$totkgds4b=0;
				foreach ($resultali->result() as $rowali):
					
					//$row->semsb='';$row->acusb=''
					//$row->semsi='';$row->acusi='';$row->sybsi='';$row->kghasi='';
				
					//$row->hasg=number_format($row->hasg, 1, '.', ',');$thas+=$row->hasg;
				
					if($rowali->tkgs>0){ $row->totsoy=number_format(($rowali->tkgs), 1, '.', ',');$ts+=$row->totsoy;}
					if($rowali->tkgb>0){ $row->totbal=number_format(($rowali->tkgb), 1, '.', ',');$tb+=$row->totbal;}
					$row->soybal=number_format(($rowali->tkgd), 1, '.', ',');
					//if($row->tkgsi>0){ $row->sybsi=number_format(($row->tkgsi), 1, '.', ',');$tsi+=$row->sybsi;}
					
					//suma los kgs de la semana
					//SELECT idpischa,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(kgt*(porsoy/100)) as tkgs,sum(kgt*(porbal/100)) as tkgb 
					//from aligracha_21 where ciccha='2021-1' and feccha>'2021-03-18' and feccha<'2021-03-25' group by idpischa
					$diasem= date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $fecact ) ))));
					$this->db->select('sum(kgs) as tkgsi,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(porsoy) as tkgs,sum(porbal) as tkgb');
					$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'join');
					$this->db->where('feccha >=',$diasem);$this->db->where('feccha <=',$fecact);
					//if($filter['where']!=''){	$this->db->where($filter['where']); }
					$this->db->where('idpischa =',$est);
					$this->db->group_by('idpischa');
					$resultant = $this->db->get($this->tablacha);
					foreach ($resultant->result() as $rowant):
						if($rowant->tkgs>0){$row->semsoy=number_format(($rowant->tkgs), 1, '.', ',');$sts+=$row->semsoy;}
						if(($row->totsoy-$row->semsoy)>0){$row->acusoy=number_format(($row->totsoy-$row->semsoy), 1, '.', ',');$ats+=$row->acusoy;}
						if($rowant->tkgb>0){$row->sembal=number_format(($rowant->tkgb), 1, '.', ',');$stb+=$row->sembal;}
						if(($row->totbal-$row->sembal)>0){$row->acubal=number_format(($row->totbal-$row->sembal), 1, '.', ',');$atb+=$row->acubal;}
						if($row->semsoy+$row->sembal>0){$row->semsb=number_format($row->semsoy+$row->sembal, 1, '.',',');$ssb+=$row->semsb;
						$row->kgshas=number_format(($row->semsb/$row->hasg)/7, 2, '.', ',');
						//	$row->kghasi=number_format(($row->tkgsi/$row->hasg), 2, '.', ',');
						}
						//if(($row->soybal-$row->semsb)>0){$row->acusb=number_format(($row->soybal-$row->semsb), 1, '.', ',');$asb+=$row->acusb;}
						//if($rowant->tkgsi>0){$row->semsi=number_format(($rowant->tkgsi), 1, '.', ',');$stsi+=$row->semsi;}
						//if(($row->sybsi-$row->semsi)>0){$row->acusi=number_format(($row->sybsi-$row->semsi), 1, '.', ',');$atsi+=$row->acusi;}
					endforeach;	
					/*				
					switch ($row->secc) {
						case '41': $totkgds1+=$row->tkgd;$totkgds1s+=$row->semsoy;$totkgds1b+=$row->sembal;break;
						case '42': $totkgds2+=$row->tkgd;$totkgds2s+=$row->semsoy;$totkgds2b+=$row->sembal;break;
						case '43': $totkgds3+=$row->tkgd;$totkgds3s+=$row->semsoy;$totkgds3b+=$row->sembal;break;
						case '44': $totkgds4+=$row->tkgd;$totkgds4s+=$row->semsoy;$totkgds4b+=$row->sembal;break;
					}*/
				endforeach;	
				
				
				$data[] = $row;
				$a=2;
			endforeach;	
			//poner el total 
			$this->db->select("avg(orgg/hasg) as ams");
			$this->db->join($this->tablabiogra, $this->idpis.'='.$this->idpisb,'inner');   
			if($filter['where']!=''){
				$this->db->where($filter['where']); }    
        	$result = $this->db->get($this->tabla);
        	foreach($result->result() as $row):
				$row->pisg='Tot';
				$row->hasgg=number_format($shasgg, 1, '.', ',');
				$row->orgsm1=number_format($row->ams/10, 1, '.', ',');
				$row->fecg1='';
				$row->dias=number_format($sdias/$cdias, 1, '.', ',');
				$row->orgsm=number_format($sorgsm/$cdias, 1, '.', ',');
				$row->orgs= number_format($torgact, 0, '.', ',');
				$row->por=number_format($spor/$cdias, 0, '.', ',');
				$row->pesb=number_format($spesb/$cdias, 1, '.', ',');$row->pesbp ='';$row->ipesb ='';$row->ppesb ='';
				$row->pesa=number_format($spesa/$cdias, 1, '.', ',');
				$row->incp=number_format($sincp/$cdias, 1, '.', ',');$row->incpp ='';$row->iincp ='';$row->pincp ='';
				$row->inc3=number_format($sinc3/$cdias, 3, '.', ',');
				$row->bioh=number_format($sbioh/$cdias, 0, '.', ',');
				$row->bio= number_format($sbio, 0, '.', ',');
				
				$row->bioi='';$row->adiario='';$row->alicha='';$row->bw='';
				$row->inc3p='';$row->iinc3 ='';$row->pinc3 ='';
				$row->porp='';$row->iporp ='';$row->pporp ='';
				$row->orgsmp='';$row->iorgsm ='';$row->porgsm ='';
				$row->fcap='';$row->ifcap ='';$row->pfcap ='';
				//buscar los datos
				$row->ah=number_format($sah/$cdias, 1, '.', ',');
				$row->alis=number_format($salis, 0, '.', ',');
				$row->alic=number_format($salic, 0, '.', ',');
				if($sbio>0) $row->fca=number_format($salic/$sbio, 2, '.', ',');
				$row->isp=number_format($sisp/$cdias, 3, '.', ',');
				
				$row->semsoy='';$row->acusoy='';$row->totsoy='';
				$row->sembal='';$row->acubal='';$row->totbal='';
				$row->soybal='';$row->kgshas='';
				if($ts>0){ $row->totsoy=number_format($ts, 1, '.', ',');}
				if($sts>0){ $row->semsoy=number_format($sts, 1, '.', ',');}
				if($ats>0){ $row->acusoy=number_format($ats, 1, '.', ',');}
				if($tb>0){ $row->totbal=number_format($tb, 1, '.', ',');}
				if($stb>0){ $row->sembal=number_format($stb, 1, '.', ',');}
				if($atb>0){ $row->acubal=number_format($atb, 1, '.', ',');}
				if($tb+$ts>0){ $row->soybal=number_format(($ts+$tb), 1, '.', ',');}
				$row->kgshas=number_format((($sts+$stb)/$row->hasgg)/7, 2, '.', ',');
            	$data[] = $row;
        	endforeach;  
			}			
			return $data;
		}
		function getNumRowDES($filter){
			$this->db->select('fecg,fecb,DATEDIFF( fecb,fecg ) AS dias,(DATEDIFF( fecb,fecg ))/7 AS sem,pesb',NULL,FALSE);
			$this->db->join($this->tablabiogra, $this->idpis.'='.$this->idpisb,'inner');					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getElementsba($where){        
        	$this->db->select("idpis,pisg as val");  
			$this->db->order_by('pisg');     
        	if($where['numgra']!='0')
	            $this->db->where($where);            
			$result = $this->db->get($this->tabla);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementsbSecc1($filter){        
        	$this->db->select("pisg,pisg as val");   
			if($filter['where']!=''){$this->db->where($filter['where']); }
			//$this->db->where('cicg',$ciclo);    
        	//$this->db->where('numgra',4);
			$this->db->order_by('pisg');
			$result = $this->db->get($this->tabla);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
    	function getElementsbSecc($filest){        
			$ano=substr($filest, 2,2);
			$ciclo=substr($filest, 0,6);
			$secc=substr($filest, 6,1);
			$granja=substr($filest, 7,1);
        	$this->db->select("pisg,pisg as val,cicg");     
			$this->db->where('numgra',$granja);
			if($secc!=0){$this->db->where('secc','4'.$secc);}
			if($ciclo!=0){$this->db->where('cicg',$ciclo);}
	        //$this->db->where('secc >',0);
	        $this->db->order_by('cicg');$this->db->order_by('pisg');
			$result = $this->db->get($this->tablasie.$ano);
        	$data = array();        
        	foreach($result->result() as $row):
				//$row->val='C'.substr($row->cicg, -1).'-'.$row->val;
				
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementsb($where,$ciclo){        
        	$this->db->select("pisg,pisg as val");   
			$this->db->where('cicg',$ciclo);    
        	$this->db->where('numgra',4);
			$this->db->order_by('pisg');
			$result = $this->db->get($this->tabla);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementsbs($where){        
        	$this->db->select("secc,secc as val");       
        	if($where['cicg']!=0)
	           $this->db->where($where);
			else {				
				$this->db->where('numgra',4);
			}
			 $this->db->group_by('secc');
			$result = $this->db->get($this->tabla);
        	$data = array();        
        	foreach($result->result() as $row):
				if($row->val==41 || $row->val==432) $row->val='I';
				if($row->val==42 || $row->val==432) $row->val='II';
				if($row->val==43 || $row->val==432 ) $row->val='III';
				if($row->val==44 || $row->val==442 ) $row->val='IV';
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function history($pil){
			$this->db->select($this->hasg." ,".$this->orgg);
			$this->db->from($this->tabla);
			$this->db->where('idpis =', $pil);
			$query=$this->db->get();
			return $query->row();
		}		
    }
    
?>