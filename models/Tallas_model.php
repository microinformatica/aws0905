<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Tallas_model extends CI_Model {
        //private $nombre;
        public $idt="idt"; public $nomt="nomt"; public $prefec="prefec";public $premen="premen";public $premay="premay";
		public $grupo="grupo";public $gpoid="gpoid";public $gpogrl="gpogrl";
		public $tabla="tallas";
        
	    function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function history($gpo){
			$this->db->select($this->gpogrl." ,".$this->grupo);
			$this->db->from($this->tabla);
			$this->db->where('gpoid =', $gpo);
			$query=$this->db->get();
			return $query->row();
		}
		function getTallasg(){
			//SELECT grupo,gpoid,gpogrl from tallas group by grupo,gpoid order by gpogrl
			$this->db->select('grupo,gpoid');
			$this->db->group_by('grupo');$this->db->group_by('gpoid');
			$this->db->order_by('gpogrl');
			$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		public function actualizar($id,$nom,$dia,$pmen,$pmay,$gpot,$gpoidt,$gpogrlt){
			$data=array($this->nomt=>$nom,$this->prefec=>$dia,$this->premen=>$pmen,$this->premay=>$pmay,$this->grupo=>$gpot,$this->gpoid=>$gpoidt,$this->gpogrl=>$gpogrlt);
			$this->db->where($this->idt,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
			
		public function agregar($nom,$dia,$pmen,$pmay,$gpot,$gpoidt,$gpogrlt){
			$data=array($this->nomt=>$nom,$this->prefec=>$dia,$this->premen=>$pmen,$this->premay=>$pmay,$this->grupo=>$gpot,$this->gpoid=>$gpoidt,$this->gpogrl=>$gpogrlt);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		
		function gettallas($filter){
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tabla);
			 $fec=new Libreria();
			foreach ($result->result() as $row):
				if($row->prefec==0) $row->prefec1=''; else $row->prefec1 = $fec->fecha($row->prefec);
				if($row->premen==0) $row->premen='';
				if($row->premay==0) $row->premay='';
				$data[] = $row;	
			endforeach;
			return $data;
		}
    }
?>