<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Estadistica_model extends CI_Model {
        public $numero="Numero";
		public $razon="Razon";
		public $saldo="Saldo";
		public $fecud="FecUD";
		public $impud="ImpUD";
		public $obsud="ObsUD";
		public $si="SI";
		public $zona="Zona";
		public $tabla="clientes";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getEstadistica($filter){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);	
			$this->db->where('Saldo >',0);	
			$this->db->order_by('Saldo','desc');		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*$zonas="Todos";
			if($filter['num']!=0){
					//$this->db->where($this->zona,$filter['num']);		
			}	*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$deudazona=0;$ixx = 0; $fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($row->SI==0 ){$deudazona=$deudazona+$row->Saldo;}
				$row->Saldo1 = $row->Saldo; //$row->Saldo2 += $row->Saldo;
				$row->Saldo = '$ '.number_format($row->Saldo, 2, '.', ',');				 					
				if($row->FecUD=="" or $row->FecUD=="0000-00-00"){$row->FecUD="";$row->FecUD1="";}							
				if($row->ImpUD>0){
					 $row->FecUD1 = $fec->fecha($row->FecUD);	
					 $row->FecUD = $fec->fecha($row->FecUD)." [".$row->diferencia."]";
					 
				//	 $row->FecUD = date("d-m-Y",strtotime($row->FecUD))." [".$row->diferencia."]";
				}
				if($row->FecUD!="" and $row->ImpUD>0){$row->ImpUD = '$ '.number_format($row->ImpUD, 2, '.', ',');}else{$row->ImpUD="";} 
				if($row->ObsUD==""){$row->ObsUD="";}
				//$row->zona = $zonas;
				//$row->Saldo2 = '$ '.number_format($row->Saldo2, 2, '.', ',');
				$data[$ixx] = $row;	
				$ixx = $ixx+1;			
			endforeach;	
			
			$this->db->select('sum(Saldo) as total');	
			$this->db->where('Saldo >',0);				
			$this->db->where($filter['where']);
			$result = $this->db->get($this->tabla);		
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):				
				$row->Razon = "Total: "; //$row->Saldo2 += $row->Saldo;
				$row->Saldo = '$ '.number_format($row->total, 2, '.', ',');				 					
				$row->FecUD="";
				$row->ImpUD="";
				$row->ObsUD="";
				$data[$ixx] = $row;	
				$ixx = $ixx+1;			
			endforeach;					
			return $data;
		}
		
		/*function getEstadistica1($filter){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);	
			$this->db->where('Saldo >',0);	
			$this->db->order_by('Saldo','desc');		
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*$zonas="Todos";
			if($filter['num']!=0){
					if($filter['num']==1){$zonas="Angostura";}
					if($filter['num']==2){$zonas="Campeche";}
					if($filter['num']==3){$zonas="Colima";}
					if($filter['num']==4){$zonas="El Dorado";}			
					if($filter['num']==5){$zonas="Elota";}
					if($filter['num']==6){$zonas="Guasave";}
					if($filter['num']==7){$zonas="Guerrero";}
					if($filter['num']==8){$zonas="Hermosillo";}
					if($filter['num']==9){$zonas="Mochis";}
					if($filter['num']==10){$zonas="Navojoa";}
					if($filter['num']==11){$zonas="Navolato";}
					if($filter['num']==12){$zonas="Nayarit";}
					if($filter['num']==13){$zonas="Obregon";}
					if($filter['num']==14){$zonas="Sur Sinaloa";}
					if($filter['num']==15){$zonas="Tamaulipas";}
					if($filter['num']==16){$zonas="Yucatán";}
					$this->db->where('Zona =',$zonas);			
					//$this->db->where($this->zona,$filter['num']);		
			}*/	
		/*	//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$deudazona=0;$ixx = 0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($row->SI==0 ){$deudazona=$deudazona+$row->Saldo;}
				$row->Saldo1 = $row->Saldo; //$row->Saldo2 += $row->Saldo;
				$row->Saldo = '$ '.number_format($row->Saldo, 2, '.', ',');				 					
				if($row->FecUD=="" or $row->FecUD=="0000-00-00"){$row->FecUD="";}							
				if($row->ImpUD>0){ $row->FecUD = date("d-m-Y",strtotime($row->FecUD))." [".$row->diferencia."]";}
				if($row->FecUD!="" and $row->ImpUD>0){$row->ImpUD = '$ '.number_format($row->ImpUD, 2, '.', ',');}else{$row->ImpUD="";} 
				if($row->ObsUD==""){$row->ObsUD="";}
				//$row->zona = $zonas;
				//$row->Saldo2 = '$ '.number_format($row->Saldo2, 2, '.', ',');
				$data[$ixx] = $row;	
				$ixx = $ixx+1;			
			endforeach;	
								
			return $data;
		}*/
		function getNumRows($filter){
			$this->db->where('Saldo >',0);					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
				
			/*if($filter['num']!=0)
				$this->db->where($this->zona,$filter['num']);
			*/
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function actualizar($id,$obs,$sn){
			$data=array($this->si=>$sn,$this->obsud=>$obs);
			$this->db->where($this->numero,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function zonas(){									
			$this->db->select('Zona, SUM(Saldo) as saldo');
			$this->db->where('Numero >',1);
			$this->db->where('Saldo >',0);
			$this->db->where('SI',0);
			$this->db->where('Zona !=','Varias');	
			$this->db->group_by('Zona');
			$this->db->order_by('SUM(Saldo)','desc');					
			$query = $this->db->get($this->tabla);			
			return $query->result();
		}				
		function zonaseleccionada($zonas){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);	
			$this->db->where('Saldo >',0);
			$this->db->where('Zona =',$zonas);
			$this->db->order_by('Saldo','desc');
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
		public function getClienteEst($id){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);			
			$this->db->where('Numero',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
	
    }
    
?>