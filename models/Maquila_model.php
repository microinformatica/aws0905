<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Maquila_model extends CI_Model {
        public $tablam="maquilas_";
		public $ide="ide";public $fece="fece";public $idae="idae";public $idce="idce";public $idge="idge";public $kgscos="kgscos";	
		
		public $tablamf="maqfco_"; 
		public $idef="idef";public $estf="estf";public $grgf="grgf";public $grpf="grpf";public $ntpf="ntpf";public $kgtf="kgtf";
		public $idfe="idfe";
		
		public $tablamp="maqpro_";
		public $idep="idep";public $lotp="lotp";public $idtp="idtp";public $masp="masp";public $kgmp="kgmp";
		public $kgsp="kgsp";public $idgp="idgp";public $idap="idap";public $prep="prep";public $fecp="fecp";
		public $fac="fac";public $estp="estp";public $idpe="idpe";public $moneda="moneda";public $tcm="tcm";
		public $gpop="gpop";public $fecr="fecr";
		
		public $idfle="idfle";public $cicfle="cicfle";public $fecfle="fecfle";public $folfle="folfle";public $profle="profle";
		public $monfle="monfle";public $tcfle="tcfle";public $impfle="impfle";public $tipfle="tipfle";
		public $tablafst="maqflesegtra";
		
		public $tablae="entradas";	
        public $ident="ident";public $alment="alment";public $graent="graent";public $talent="talent";public $kgsent="kgsent";
		public $exient="exient";public $cicent="cicent";
        
        public $tablag="granjas";	
		public $tablaa="almacenes";
		public $tablat="tallas";
		public $tablapro="proveedores";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function agregarfst($cic,$fec,$fol,$pro,$mon,$tc,$imp,$tip){
			$cic='20'.$cic;
			$imp=str_replace(",", "", $imp);
			$data=array($this->cicfle=>$cic,$this->fecfle=>$fec,$this->folfle=>$fol,$this->profle=>$pro,$this->monfle=>$mon,$this->tcfle=>$tc,$this->impfle=>$imp,$this->tipfle=>$tip);			
			$this->db->insert($this->tablafst,$data);
			return $this->db->insert_id();
		}
		function actualizarfst($id,$fec,$fol,$pro,$mon,$tc,$imp,$tip){
			$imp=str_replace(",", "", $imp);
			$data=array($this->fecfle=>$fec,$this->folfle=>$fol,$this->profle=>$pro,$this->monfle=>$mon,$this->tcfle=>$tc,$this->impfle=>$imp,$this->tipfle=>$tip);
			$this->db->where($this->idfle,$id);
			$this->db->update($this->tablafst,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function getfle($filter,$cic){
				
			$this->db->select('sum(impfle*tcfle) as tot');
			$this->db->join('proveedores', 'Numero=profle','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$resulttf = $this->db->get($this->tablafst);
		    if($resulttf->num_rows()>0){
		     $data = array();		
		     foreach($resulttf->result() as $rowft):
				$rowft->tipfle1 ='Total';$rowft->fecfle1 ='';$rowft->monfle1='';$rowft->impfle1='';$rowft->Numero='';$rowft->Razon='';$rowft->folfle='';
				$rowft->impfle1 ='$ '. number_format(($rowft->tot), 2, '.', ',');
				$data[] = $rowft;
			 endforeach;
			$this->db->select('Numero,Razon,idfle,cicfle,fecfle,folfle,profle,monfle,tcfle,impfle,tipfle');
			$this->db->join('proveedores', 'Numero=profle','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecfle','DESC');
			$result = $this->db->get($this->tablafst);
			 $fec=new Libreria(); 
			 foreach($result->result() as $row):
				 if($row->tipfle==1) $row->tipfle1='Flete'; else $row->tipfle1='Seguro';
				 $row->fecfle1 = $fec->fecha($row->fecfle);
				 if($row->monfle==1) $row->monfle1='MXN'; else $row->monfle1='USD';
				 $row->impfle1 ='$ '. number_format(($row->impfle*$row->tcfle), 2, '.', ',');
				$data[] = $row;				
			 endforeach;
			}
			return $data;
		}
		
		function getProves(){
			$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->group_by('Razon');
			$this->db->group_by('Numero');
			$this->db->group_by('Siglas');
			$result = $this->db->get($this->tablapro);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		
		function getpagosa($filter,$cic,$granja){
			//select noma,sum(kgsp) as kgsp, sum(masp*kgmp*prep*1.16) as cosp,(select sum(masp*kgmp*prep*1.16) from maqpro_19 where fac!=0 and idap=ida) as pagp 
			//from almacenes inner join maqpro_19 on idap=ida where ida!=2 group by noma
			//2da opcion
			//select noma,sum(kgsp) as kgsp,(select sum(masp*kgmp) from maqpro_19 where fac!=0 and idap=ida and moneda=1) as kgspmn,(select sum(masp*kgmp*prep*1.16) from maqpro_19 where idap=ida and moneda=1) as cospmn,(select sum(masp*kgmp*prep*1.16) from maqpro_19 where fac!=0 and idap=ida and moneda=1) as pagpmn,(select sum(masp*kgmp) from maqpro_19 where fac!=0 and idap=ida and moneda=2) as kgspusd,(select sum(masp*kgmp*prep*2.20462*1.16) from maqpro_19 where idap=ida and moneda=2) as cospusd,(select sum(masp*kgmp*2.20462*prep*1.16) from maqpro_19 where fac!=0 and idap=ida and moneda=2) as pagpusd 
			//from almacenes inner join maqpro_19 on idap=ida where ida!=2 group by noma 
			
			if($granja==0)
			$this->db->select('ida,noma,sum(kgsp) as kgsm, (select sum(masp*kgmp*prep*1.16) from maqpro_'.$cic.' where idap=ida and moneda=1) as cospmn,(select sum(masp*kgmp) from maqpro_'.$cic.' where idap=ida and moneda=1 and estp=1) as kgspmnt,(select sum(masp*kgmp) from maqpro_'.$cic.' where fac!=0 and idap=ida and moneda=1) as kgspmn,(select sum(masp*kgmp*prep*1.16) from maqpro_'.$cic.' where fac!=0 and idap=ida and moneda=1) as pagpmn,(select sum(masp*kgmp) from maqpro_'.$cic.' where idap=ida and moneda=2) as kgspusdt,(select sum(masp*kgmp) from maqpro_'.$cic.' where fac!=0 and idap=ida and moneda=2) as kgspusd,(select sum(masp*kgmp*prep*2.20462*1.16) from maqpro_'.$cic.' where idap=ida and moneda=2) as cospusd,(select sum(masp*kgmp*2.20462*prep*1.16) from maqpro_'.$cic.' where fac!=0 and idap=ida and moneda=2) as pagpusd');
			else
			//$this->db->select('ida,noma,sum(kgsp) as kgsm, sum(masp*kgmp*prep*1.16) as cosp,(select sum(masp*kgmp) from maqpro_19 where idgp='.$granja.' and fac!=0 and idap=ida) as kgsp,(select sum(masp*kgmp*prep*1.16) from maqpro_19 where idgp='.$granja.' and fac!=0 and idap=ida) as pagp');
			$this->db->select('ida,noma,sum(kgsp) as kgsm, (select sum(masp*kgmp*prep*1.16) from maqpro_'.$cic.' where idgp='.$granja.' and  idap=ida and moneda=1) as cospmn,(select sum(masp*kgmp) from maqpro_'.$cic.' where idgp='.$granja.' and idap=ida and moneda=1 and estp=1) as kgspmnt,(select sum(masp*kgmp) from maqpro_'.$cic.' where idgp='.$granja.' and  fac!=0 and idap=ida and moneda=1) as kgspmn,(select sum(masp*kgmp*prep*1.16) from maqpro_'.$cic.' where idgp='.$granja.' and fac!=0 and idap=ida and moneda=1) as pagpmn,(select sum(masp*kgmp) from maqpro_'.$cic.' where idgp='.$granja.' and  idap=ida and moneda=2) as kgspusdt,(select sum(masp*kgmp) from maqpro_'.$cic.' where idgp='.$granja.' and fac!=0 and idap=ida and moneda=2) as kgspusd,(select sum(masp*kgmp*prep*2.20462*1.16) from maqpro_'.$cic.' where idgp='.$granja.' and idap=ida and moneda=2) as cospusd,(select sum(masp*kgmp*2.20462*prep*1.16) from maqpro_'.$cic.' where idgp='.$granja.' and fac!=0 and idap=ida and moneda=2) as pagpusd');
			$this->db->join($this->tablaa, 'ida=idap','inner');
			$this->db->where('lotp !=','');
			$this->db->where('estp =','1');
			$this->db->group_by('noma');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$nom='';$totk=0;$totcpmn=0;$totkgspmn=0;$totpagpmn=0;$totdeukmn=0;$totdeupmn=0;
			$totcpusd=0;$totkgspusd=0;$totpagpusd=0;$totdeukusd=0;$totdeupusd=0;
			$totp=0;$totkp=0;$totkd=0;$totf=0;
			if($result->num_rows()>0){
				$feci=new Libreria();
			foreach($result->result() as $row):
				//busca los kilogramos frescos
				//SELECT idae,sum(ntpf*kgtf) as fco from maquilas_19 inner join maqfco_19 on idfe=ide group by idae
				$fco=0;
				$this->db->select('idae,sum(ntpf*kgtf) as fco');
				$this->db->join($this->tablamf.$cic, 'idfe=ide','inner');
				$this->db->where($this->idae,$row->ida);
				//$this->db->group_by('idae');
				if($granja!=0){	$this->db->where('idge',$granja);
				//$this->db->group_by('idge');
				}
				$resultf = $this->db->get($this->tablam.$cic);
				foreach($resultf->result() as $rowf):
					$fco=$rowf->fco;$totf+=$rowf->fco;
				endforeach;	
				$totk+=$row->kgsm;
				$totcpmn+=$row->cospmn;$totkgspmn+=$row->kgspmn;$totpagpmn+=$row->pagpmn;$totdeukmn+=$row->kgspmnt-$row->kgspmn;$totdeupmn+=$row->cospmn-$row->pagpmn;
				$totcpusd+=$row->cospusd;$totkgspusd+=$row->kgspusd;$totpagpusd+=$row->pagpusd;$totdeukusd+=$row->kgspusdt-$row->kgspusd;$totdeupusd+=$row->cospusd-$row->pagpusd;
				//$totp+=$row->pagp;$totkp+=$row->kgsp;
				$row->ren=number_format(($row->kgsm/$fco)*100, 2, '.', ',');
				$row->fco=number_format($fco, 0, '.', ',');
				//mn
				if($row->cospmn-$row->pagpmn>0) $row->deupmn='$ '.number_format($row->cospmn-$row->pagpmn, 2, '.', ','); else $row->deupmn='';
				if($row->cospmn-$row->pagpmn>0) $row->deukmn=number_format($row->kgspmnt-$row->kgspmn, 0, '.', ','); else $row->deukmn='';
				$row->cospmn='$ '.number_format($row->cospmn, 2, '.', ',');
				if($row->pagpmn>0) $row->pagpmn='$ '.number_format($row->pagpmn, 2, '.', ','); else $row->pagpmn=''; 
				if($row->kgspmn>0) $row->kgspmn=number_format($row->kgspmn, 0, '.', ','); else $row->kgspmn='';
				
				//usd
				if($row->cospusd-$row->pagpusd>0) $row->deupusd='$ '.number_format($row->cospusd-$row->pagpusd, 2, '.', ','); else $row->deupusd='';
				if($row->cospusd-$row->pagpusd>0) $row->deukusd=number_format($row->kgspusdt-$row->kgspusd, 0, '.', ','); else $row->deukusd='';
				//$row->kgsm=number_format($row->kgsm, 0, '.', ',');
				if($row->cospusd>0) $row->cospusd='$ '.number_format($row->cospusd, 2, '.', ','); else $row->cospusd='';
				if($row->pagpusd>0) $row->pagpusd='$ '.number_format($row->pagpusd, 2, '.', ','); else $row->pagpusd=''; 
				if($row->kgspusd>0) $row->kgspusd=number_format($row->kgspusd, 0, '.', ','); else $row->kgspusd='';
			
				$row->kgsm=number_format($row->kgsm, 0, '.', ',');
				
				$data[] = $row;
			endforeach;
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->noma = "Totales";
				$row->ren=number_format(($totk/$totf)*100, 2, '.', ',');
				$row->fco=number_format($totf, 0, '.', ',');
				if($totk>0) $row->kgsm=number_format($totk, 0, '.', ','); else $row->kgsm='';
				if($totcpmn>0) $row->cospmn='$ '.number_format($totcpmn, 2, '.', ','); else $row->cospmn='';
				if($totkgspmn>0) $row->kgspmn=number_format($totkgspmn, 0, '.', ','); else $row->kgspmn='';
				if($totpagpmn>0) $row->pagpmn='$ '.number_format($totpagpmn, 2, '.', ','); else $row->pagpmn='';
				if($totdeukmn>0) $row->deukmn=number_format($totdeukmn, 0, '.', ','); else $row->deukmn='';
				if($totdeupmn>0) $row->deupmn='$ '.number_format($totdeupmn, 2, '.', ','); else $row->deupmn='';
				
				if($totcpusd>0) $row->cospusd='$ '.number_format($totcpusd, 2, '.', ','); else $row->cospusd='';
				if($totkgspusd>0) $row->kgspusd=number_format($totkgspusd, 0, '.', ','); else $row->kgspusd='';
				if($totpagpusd>0) $row->pagpusd='$ '.number_format($totpagpusd, 2, '.', ','); else $row->pagpusd='';
				if($totdeukusd>0) $row->deukusd=number_format($totdeukusd, 0, '.', ','); else $row->deukusd='';
				if($totdeupusd>0) $row->deupusd='$ '.number_format($totdeupusd, 2, '.', ','); else $row->deupusd='';
				/*if($totkp>0) $row->kgsp=number_format($totkp, 0, '.', ','); else $row->kgsp='';
				if($totp>0) $row->pagp='$ '.number_format($totp, 2, '.', ','); else $row->pagp='';
				$row->deuk=number_format($totk-$totkp, 0, '.', ',');
				$row->deup='$ '.number_format($totc-$totp, 2, '.', ','); */
				$data[] = $row;	
			endforeach;
			}
			return $data;
		}
		
		
		
		function agregarmaqp($lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec,$cic,$mon,$tcm,$gpop,$fecr){
			$kgsp=$mas*$kgm;
			//busco la talla sino existo la registro
			$this->db->select('ident,kgsent,exient');
			$this->db->where($this->alment,$ida);
			$this->db->where($this->talent,$idt);	
			$this->db->where($this->graent,$idg);
			$this->db->where($this->cicent,$cic);
			$result = $this->db->get($this->tablae);
			if($result->num_rows()==0){
				//ingresa
				$data=array($this->talent=>$idt,$this->alment=>$ida,$this->graent=>$idg,$this->kgsent=>$kgsp,$this->exient=>$kgsp,$this->cicent=>$cic);	
				$this->db->insert($this->tablae,$data);
			}else{
				//extraigo los datos a actualizar
				$kgs=0;$exis=0;
				foreach ($result->result() as $row):
				  $kgs=$row->kgsent+$kgsp;
				  $exi=$row->exient+$kgsp;
				  $id=$row->ident;
				endforeach;
				//realizo actualizacion
				$data=array($this->kgsent=>$kgs,$this->exient=>$exi);	
				$this->db->where($this->ident,$id);
				$this->db->update($this->tablae,$data);
			}		
			
			$data=array($this->lotp=>$lot,$this->idtp=>$idt,$this->masp=>$mas,$this->kgmp=>$kgm,$this->idgp=>$idg,$this->idap=>$ida,$this->idpe=>$idp,$this->kgsp=>$kgsp,$this->prep=>$pre,$this->fecp=>$fec,$this->moneda=>$mon,$this->tcm=>$tcm,$this->gpop=>$gpop,$this->fecr=>$fecr);			
			$this->db->insert($this->tablamp.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizarmaqp($id,$lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec,$cic,$mon,$tcm,$gpop,$fecr){
			$kgsp=$mas*$kgm;
			/* creo esto ya no es necesario aqui
			//busco la talla sino existo la registro
			$this->db->select('ident,kgsent,exient');
			$this->db->where($this->alment,$ida);
			$this->db->where($this->talent,$idt);	
			$this->db->where($this->graent,$idg);
			$result = $this->db->get($this->tablae);
			if($result->num_rows()==0){
				//ingresa
				$data=array($this->talent=>$idt,$this->alment=>$ida,$this->graent=>$idg,$this->kgsent=>$kgsp,$this->exient=>$kgsp,$this->cicent=>$cic);	
				$this->db->insert($this->tablae,$data);
			}else{
			*/
			
			//se debe actualizar las entradas y la existencia
			//y para eso las busco
			$this->db->select('kgsp,idtp,idgp,idap');
			$this->db->where($this->idep,$id);	
			$resultp = $this->db->get($this->tablamp.$cic);
			foreach ($resultp->result() as $row):
			  $kgsqa=$row->kgsp;
			  $idtp=$row->idtp;
			  $idgp=$row->idgp;
			  $idap=$row->idap;
			endforeach;
			if($kgsp!=$kgspa){	
				$this->db->select('ident,kgsent,exient');
				$this->db->where($this->alment,$idap);
				$this->db->where($this->graent,$idgp);
				$this->db->where($this->talent,$idtp);
				$this->db->where($this->cicent,$cic);
				$result = $this->db->get($this->tablae);
				foreach ($result->result() as $row):
				  $kgs=$row->kgsent;
				  $exi=$row->exient;
				  $idq=$row->ident;
				endforeach;
				//defino si se suma o se resta
				if($kgsp>$kgspa){ //suma
					$kgs+=($kgsp-$kgsqa);$exi+=($kgsp-$kgsqa);
				}else{ //resta
					$kgs-=($kgsp-$kgsqa);$exi-=($kgsp-$kgsqa);
				}
				//realizo actualizacion
				$data = array();
				$data=array($this->kgsent=>$kgs,$this->exient=>$exi);	
				$this->db->where($this->ident,$idq);
				$this->db->update($this->tablae,$data);
			}
			
	 		$data=array($this->lotp=>$lot,$this->idtp=>$idt,$this->masp=>$mas,$this->kgmp=>$kgm,$this->idgp=>$idg,$this->idap=>$ida,$this->idpe=>$idp,$this->kgsp=>$kgsp,$this->prep=>$pre,$this->fecp=>$fec,$this->moneda=>$mon,$this->tcm=>$tcm,$this->gpop=>$gpop,$this->fecr=>$fecr);
			$this->db->where($this->idep,$id);
			$this->db->update($this->tablamp.$cic,$data);
			//}
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function getpagos($filter,$cic,$graf){
			//select fece,nomg,kgscos,noma,(select sum(ntpf*kgtf) from maqfco_19 where ide=idfe) as fco 
			//from almacenes inner join( granjas inner join maquilas_19 on idge=idg )on idae=ida order by idge,fece
			$this->db->select('ide,fece,idg,nomg,kgscos,noma,(select sum(ntpf*kgtf) from maqfco_'.$cic.' where ide=idfe) as fco');
			$this->db->join('granjas', 'idge=idg','inner');
			$this->db->join($this->tablaa, 'idae=ida','inner');
			/*$this->db->join($this->tablamp.$cic, 'idpe=ide','inner');
			$this->db->group_by($this->lotp);
			$this->db->group_by($this->moneda);*/
			$this->db->order_by('idge');
			$this->db->order_by('fece');
			//$query=$this->db->query('select fece,nomg,kgscos,noma,(select sum(ntpf*kgtf) from '.$this->tablamf.$cic.' where ide=idfe) as fco from almacenes inner join( granjas inner join '.$this->tablam.$cic.' on idge=idg )on idae=ida order by idge,fece');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablam.$cic);
			$data = array();$nom='';$totk=0;$totp=0;$totdk=0;$totdp=0;$totf=0;$tkgc=0;$totdif=0;$totpi=0;$totdpi=0;
			$dmnk=0;$dmni=0;$dmnsi=0;$pmnk=0;$pmni=0;$pmnsi=0;
			$dusdk=0;$dusdi=0;$dusdsi=0;$pusdk=0;$pusdi=0;$pusdsi=0;
			$dia='';
			if($result->num_rows()>0){
				$feci=new Libreria();
			foreach($result->result() as $row):
				$bus=$row->ide;$totf+=$row->fco;$tkgc+=$row->kgscos;$cos=$row->kgscos;
				if($row->kgscos>0) $row->kgscos=number_format($row->kgscos, 0, '.', ','); else $row->kgscos='';
				if($nom!=$row->nomg){
						$nom=$row->nomg; 
						if($row->idg==2) $row->nomg='OA-Kino'; elseif($row->idg==4) $row->nomg='OA-Ahome'; else $row->nomg='OA';				
				}else{ $row->nomg="";}		
				$row->fec1=$feci->fecha21($row->fece);$dia=$row->fece;$row->moneda=0;
				$row->fec2=$feci->fecha21($row->fece).'<BR>';
				$row->lotp='';$row->lotp1='';$row->reni='';$row->imp='';$row->kgsp='';$row->kgsf='';$row->fac='';$row->impi='';
				$row->kgsf=number_format($row->fco, 0, '.', ',');	
				if($cos-$row->fco!=0) {$row->dif=$row->fco-$cos;$totdif+=$row->fco-$cos;} else {$row->dif='';}
				$this->db->select('fecp,lotp,sum(kgsp) as kgsp, sum(masp*kgmp*prep*1.16) as imp,fac,idpe');
				$this->db->where($this->estp,1);
				$this->db->where($this->idpe,$bus);
				//$this->db->group_by($this->lotp);  esto se quito por los dos lotes en una misma maquila
				
				//$this->db->order_by('nomg','DESC');
				$this->db->order_by('fecp');
				$resultp = $this->db->get($this->tablamp.$cic);
				foreach($resultp->result() as $rowp):
					$row->fac='-';
					if($rowp->fac==0) {$totdk+=$rowp->kgsp;$totdp+=$rowp->imp;$totdpi+=$rowp->imp/1.16;} 
					else { //$row->fac=str_pad($rowp->fac, 4, "0", STR_PAD_LEFT);
						$totk+=$rowp->kgsp;	$totp+=$rowp->imp;$totpi+=($rowp->imp/1.16);
					}
					$row->lotp=$rowp->lotp;$kgsp=$rowp->kgsp;
					if(is_null($rowp->lotp)){ $row->lotp1='';$row->kgsp='';}
					else{$row->lotp1=$rowp->lotp;$row->kgsp=number_format($rowp->kgsp, 0, '.', ',');}	
					if($row->fco>0 && $kgsp>0) $row->reni=number_format(($kgsp/$row->fco)*100, 2, '.', ','); else $row->reni='';
					//$row->impi='$ '.number_format(($rowp->imp/1.16), 2, '.', ',');
					//$row->imp='$ '.number_format($rowp->imp, 2, '.', ',');
				endforeach;
				$row->ren1=71.0;$row->ren2=72.0;
				$data[] = $row;
				if($graf==1){
				// aqui debo ingresar lo que corrresponda a dolar o pesos.
				$this->db->select('fecp,lotp,sum(kgsp) as kgsp, sum(masp*kgmp*prep*1.16) as imp,fac,idpe,moneda');
				$this->db->where($this->estp,1);
				$this->db->where($this->idpe,$bus);
				$this->db->group_by($this->lotp);
				$this->db->group_by('moneda');
				//$this->db->order_by('nomg','DESC');
				$this->db->order_by('fecp');
				$this->db->order_by('moneda');
				$resultt = $this->db->get($this->tablamp.$cic);
				foreach($resultt->result() as $rowt):
					$rowt->fec='';$rowt->nomg='';$rowt->kgscos='';$rowt->noma='';$rowt->kgsf='';$rowt->dif='';
					$rowt->lotp1='';
					//$rowt->impi='';$rowt->imp='';	
					
					$rowt->fec=$dia;$rowt->fec1='';
					
					if($rowt->moneda==1){ $rowt->reni='MN';
						if($rowt->fac==0) {
							$dmnk+=$rowt->kgsp;$dmni+=$rowt->imp;$dmnsi+=$rowt->imp/1.16;$rowt->fac='';
						} 
						else {$rowt->fac=str_pad($rowt->fac, 4, "0", STR_PAD_LEFT);
							$pmnk+=$rowt->kgsp;	$pmni+=$rowt->imp;$pmnsi+=($rowt->imp/1.16);
						}
						$rowt->impi='$ '.number_format($rowt->imp/1.16, 2, '.', ',');
						$rowt->imp='$ '.number_format($rowt->imp, 2, '.', ',');
					} 
					else {$rowt->reni='USD';
						if($rowt->fac==0) {
							$dusdk+=$rowt->kgsp;$dusdi+=$rowt->imp*2.20462;$dusdsi+=$rowt->imp*2.20462/1.16;$rowt->fac='';
						} 
						else {$rowt->fac=str_pad($rowt->fac, 4, "0", STR_PAD_LEFT);
							$pusdk+=$rowt->kgsp;$pusdi+=$rowt->imp*2.20462;$pusdsi+=($rowt->imp*2.20462/1.16);
						}
						$rowt->impi='$ '.number_format(($rowt->imp*2.20462)/1.16, 2, '.', ',');
						$rowt->imp='$ '.number_format($rowt->imp*2.20462, 2, '.', ',');
					}
					$rowt->kgsp=number_format($rowt->kgsp, 0, '.', ',');	
					$data[] = $rowt;
				endforeach;	
			    }
			endforeach;
			//if($graf==1){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				if($graf==1) $row->fec1="Totales"; else $row->fec2='Prom';
				//$row->ren1=71.0;$row->ren2=72.0;
				$row->lotp1="";$row->fac="-";
				$row->nomg = ""; $row->noma = "";
				if($totdif!=0) $row->dif = number_format($totdif, 0, '.', ','); else $row->dif ='';
				if($tkgc>0) $row->kgscos = number_format($tkgc, 0, '.', ','); else $row->kgscos ='';
				if($totf>0) $row->kgsf = number_format($totf, 0, '.', ','); else $row->kgsf ='';$row->imp='';$row->impi='';

				if($totf>0) $row->reni=number_format((($totk+$totdk)/$totf)*100, 2, '.', ','); else $row->reni='';
				if($totk+$totdk>0) $row->kgsp=number_format($totk+$totdk, 0, '.', ','); else $row->kgsp='';
				//if($totp+$totdp>0) $row->imp='$ '.number_format($totp+$totdp, 2, '.', ','); else $row->imp='';
				//if($totpi+$totdpi>0) $row->impi='$ '.number_format($totpi+$totdpi, 2, '.', ','); else $row->impi='';
				$data[] = $row;	
			endforeach;
			//si hay venta en pesos
			if($pmnk>0 || $dmnk>0){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='MN';
				$row->fec1="";$row->fac="-";
				if($pmnk>0 || $dmnk>0) $row->kgsp=number_format($pmnk+$dmnk, 0, '.', ','); else $row->kgsp='';
				if($pmni>0 || $dmni>0) $row->imp='$ '.number_format($pmni+$dmni, 2, '.', ','); else $row->imp='';
				if($pmnsi>0 || $dmnsi>0) $row->impi='$ '.number_format($pmnsi+$dmnsi, 2, '.', ','); else $row->impi='';
				if($pmni==($pmni+$dmni)){ $row->lotp1="PAGADOS";} else{$row->lotp1="";}
				$data[] = $row;	
			endforeach;
			//si aun no se termina de pagar se muestra
			if($pmni!=($pmni+$dmni)){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='';
				$row->fec1="";$row->lotp1="Pagos";$row->fac="-";
				if($pmnk>0) $row->kgsp=number_format($pmnk, 0, '.', ','); else $row->kgsp='';
				if($pmni>0) $row->imp='$ '.number_format($pmni, 2, '.', ','); else $row->imp='';
				if($pmnsi>0) $row->impi='$ '.number_format($pmnsi, 2, '.', ','); else $row->impi='';
				$data[] = $row;	
			endforeach;
			}
			//si se debe muestra
			if($dmnk>0){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='';
				$row->fec1="";$row->lotp1="Se Debe";$row->fac="";
				if($dmnk>0) $row->kgsp=number_format($dmnk, 0, '.', ','); else $row->kgsp='';
				if($dmni>0) $row->imp='$ '.number_format($dmni, 2, '.', ','); else $row->imp='';
				if($dmnsi>0) $row->impi='$ '.number_format($dmnsi, 2, '.', ','); else $row->impi='';
				$data[] = $row;	
			endforeach;
			}
			}
			//si hay venta en dolares
			if($pusdk>0 || $dusdk>0){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='USD';
				$row->fec1="";$row->fac="-";
				if($pusdk>0 || $dusdk>0) $row->kgsp=number_format($pusdk+$dusdk, 0, '.', ','); else $row->kgsp='';
				if($pusdi>0 || $dusdi>0) $row->imp='$ '.number_format($pusdi+$dusdi, 2, '.', ','); else $row->imp='';
				if($pusdsi>0 || $dusdsi>0) $row->impi='$ '.number_format($pusdsi+$dusdsi, 2, '.', ','); else $row->impi='';
				if($pusdi==($pusdi+$dusdi)){ $row->lotp1="PAGADOS";} else{$row->lotp1="";}
				$data[] = $row;	
			endforeach;
			//si aun no se termina de pagar se muestra
			if($pusdi!=($pusdi+$dusdi)){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='';
				$row->fec1="";$row->lotp1="Pagos";$row->fac="-";
				if($pusdk>0) $row->kgsp=number_format($pusdk, 0, '.', ','); else $row->kgsp='';
				if($pusdi>0) $row->imp='$ '.number_format($pusdi, 2, '.', ','); else $row->imp='';
				if($pusdsi>0) $row->impi='$ '.number_format($pusdsi, 2, '.', ','); else $row->impi='';
				$data[] = $row;	
			endforeach;
			}
			//si se debe muestra
			if($dusdk>0){
			$this->db->select('max(idep)');	
			$result = $this->db->get($this->tablamp.$cic);
			foreach ($result->result() as $row):				
				$row->nomg = ""; $row->noma = "";
				$row->kgsf = "";
				$row->reni="";
				$row->kgscos = "";$row->dif ='';
				$row->fec1="";$row->lotp1="Se Debe";$row->fac="";
				if($dusdk>0) $row->kgsp=number_format($dusdk, 0, '.', ','); else $row->kgsp='';
				if($dusdi>0) $row->imp='$ '.number_format($dusdi, 2, '.', ','); else $row->imp='';
				if($dusdsi>0) $row->impi='$ '.number_format($dusdsi, 2, '.', ','); else $row->impi='';
				$data[] = $row;	
			endforeach;
			}
			}
			}
			//}
			return $data;
		}
		
		
		function actualizarg($filter,$fac,$cic,$mon){
			//busco todos los registros del lote
			//select  from maqpro_19 where lotp='SJ192118002'
			$this->db->select('idep,fac');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablamp.$cic);
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				$data=array($this->fac=>$fac);
				$this->db->where($this->idep,$row->idep);
				$this->db->where($this->moneda,$mon);
				$this->db->update($this->tablamp.$cic,$data);
			endforeach;
			
			}
	 		
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function quitar($id,$tab,$cam,$cic){
			//si se borra en maqpro, se debe actualizar las entradas y la existencia
			//y para eso las busco
			if($tab=='maqpro_'){
				$this->db->select('kgsp,idtp,idgp,idap');
				$this->db->where($this->idep,$id);	
				$resultp = $this->db->get($tab.$cic);
				foreach ($resultp->result() as $row):
				  $kgsq=$row->kgsp;
				  $idtp=$row->idtp;
				  $idgp=$row->idgp;
				  $idap=$row->idap;
				endforeach;
				$this->db->select('ident,kgsent,exient');
				$this->db->where($this->alment,$idap);
				$this->db->where($this->graent,$idgp);
				$this->db->where($this->talent,$idtp);
				$this->db->where($this->cicent,$cic);
				$result = $this->db->get($this->tablae);
				foreach ($result->result() as $row):
				  $kgs=$row->kgsent-$kgsq;
				  $exi=$row->exient-$kgsq;
				  $idq=$row->ident;
				endforeach;
				//realizo actualizacion
				$data = array();
				$data=array($this->kgsent=>$kgs,$this->exient=>$exi);	
				$this->db->where($this->ident,$idq);
				$this->db->update($this->tablae,$data);
			}
			//borro
			
			$this->db->where($cam,$id);
			if($tab=='maqpro_' || $tab=='maqfco_'){$this->db->delete($tab.$cic);} else {$this->db->delete($tab);}
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getGranjas(){
			$this->db->select('idg,nomg');
			$result = $this->db->get($this->tablag);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getAlmacenes(){
			$this->db->select('ida,noma');
			$result = $this->db->get($this->tablaa);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getTallas(){
			$this->db->select('idt,nomt');
			$result = $this->db->get($this->tablat);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function agregarmaq($fec,$alm,$cos,$gra,$cic,$kgc){
			$data=array($this->fece=>$fec,$this->idae=>$alm,$this->idce=>$cos,$this->idge=>$gra,$this->kgscos=>$kgc);			
			$this->db->insert($this->tablam.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizarmaq($id,$fec,$alm,$cos,$gra,$cic,$kgc){
	 		$data=array($this->fece=>$fec,$this->idae=>$alm,$this->idce=>$cos,$this->idge=>$gra,$this->kgscos=>$kgc);
			$this->db->where($this->ide,$id);
			$this->db->update($this->tablam.$cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getmaquiladia($filter,$cic){
			//SELECT fece,noma,nomg,idpe from almacenes inner join( granjas inner join maquila on idge=idg )on idae=ida where fece='2018-06-09'
			$this->db->select('ide,fece,ida,noma,idg,nomg,idce,kgscos',NULL,FALSE);
			$this->db->join('almacenes', 'ida=idae','inner');
			$this->db->join('granjas', 'idg=idge','inner');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablam.$cic);
			$data = array();
			if($result->num_rows()>0){
				$feci=new Libreria();
			foreach($result->result() as $row):
				$row->fec=$feci->fecha21($row->fece);
				if($row->kgscos>0) $row->kgscos=number_format(($row->kgscos), 2, '.', ','); else $row->kgscos='';
				switch($row->idce){
					case 1: $row->cic='Primera';break;
					case 2: $row->cic='Segunda';break;
					case 3: $row->cic='Tercera';break;
					case 4: $row->cic='Cuarta';break;
					case 5: $row->cic='Quinta';break;
					case 6: $row->cic='Sexta';break;
					case 7: $row->cic='Final';break;	
				}	
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function agregarmaqf($est,$grg,$grp,$ntp,$kgt,$idf,$cic){
			$data=array($this->estf=>$est,$this->grgf=>$grg,$this->grpf=>$grp,$this->ntpf=>$ntp,$this->kgtf=>$kgt,$this->idfe=>$idf);			
			$this->db->insert($this->tablamf.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizarmaqf($id,$est,$grg,$grp,$ntp,$kgt,$idf,$cic){
	 		$data=array($this->estf=>$est,$this->grgf=>$grg,$this->grpf=>$grp,$this->ntpf=>$ntp,$this->kgtf=>$kgt,$this->idfe=>$idf);
			$this->db->where($this->idef,$id);
			$this->db->update($this->tablamf.$cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getmaquiladiaf($filter,$cic){
			if($filter['where']!='') $this->db->where($filter['where']);			
			$this->db->order_by($this->estf);
			$result = $this->db->get($this->tablamf.$cic);
			$data = array();
			if($result->num_rows()>0){
				$totf=0;$est='';$gr1='';$gr2='';
				foreach($result->result() as $row):
					//$est=$row->estf;
					if($est!=$row->estf){
						$est=$row->estf; 
						$row->estf1=$row->estf;
					}else{ $row->estf1="";}
					if($gr1!=$row->grgf){
						$gr1=$row->grgf; 
						$row->grgf1=number_format(($row->grgf), 2, '.', ',');	
						
					}else{ $row->grgf1='';}
					if($gr2!=$row->grpf){
						$gr2=$row->grpf; 
						$row->grpf1=number_format(($row->grpf), 2, '.', ',');	
						
					}else{ $row->grpf1='';}
					$totf+=$row->ntpf*$row->kgtf; 
					$row->subf=number_format(($row->ntpf*$row->kgtf), 2, '.', ',');	
					$row->totf=number_format(($totf), 2, '.', ',');	
					$data[] = $row;
				endforeach;
				$this->db->select('max(ide)');	
				$result = $this->db->get($this->tablam.$cic);
				foreach ($result->result() as $row):				
					$row->estf1 = "Total:"; $row->grgf1="";$row->grpf1="";$row->ntpf="";$row->kgtf="";
					if($totf>0) $row->subf=number_format($totf, 2, '.', ','); else $row->subf='';
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}
		
		function getmaquiladiap($filter,$id,$cic){
			//obtengo el total de los kgs en fresco
			$this->db->select('sum(ntpf*kgtf) as totkf');
			$this->db->where($this->idfe,$id);
			$resulta = $this->db->get($this->tablamf.$cic);
			if($resulta->num_rows()>0){
				foreach($resulta->result() as $row):
					$totf=$row->totkf;
				endforeach;
				
			$this->db->select('idep,lotp,idtp,nomt,masp,kgsp,idpe,kgmp,prep,moneda,tcm,fecr,gpop');
			$this->db->join('tallas', 'idt=idtp','inner');
			$this->db->where('lotp !=','');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->order_by('nomt');		
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$fec=new Libreria();
			if($result->num_rows()>0){
				$totp=0;$totd=0;$lot='';$totp1=0;$totd1=0;$totkgs=0;$diar='';
				foreach($result->result() as $row):
					if(is_null($row->fecr)){ $row->fecr1='-';
					} else{
					if($diar!=$row->fecr){
						$diar=$row->fecr; 
						$row->fecr1=$fec->fecha($row->fecr);
					}else{ $row->fecr1="";}
					}
					if($lot!=$row->lotp){
						$lot=$row->lotp; 
						$row->lotp1=$row->lotp;
					}else{ $row->lotp1="";}
					$totkgs+=$row->kgmp*$row->masp;
					if($row->moneda==1){
					$totp+=$row->kgmp*$row->masp;
					$totd+=($row->kgsp*$row->prep)*1.16;	
					
					$row->totp='$'.number_format(($row->kgsp*$row->prep)*1.16, 2, '.', ',');
					}	
					else{
					$totp1+=$row->kgmp*$row->masp;
					$totd1+=($row->kgsp*2.20462*$row->prep)*1.16;	
					$row->totp='$'.number_format((($row->kgsp*2.20462)*$row->prep)*1.16, 2, '.', ',');
						
					}		
					$row->kgsp=number_format(($row->kgsp), 2, '.', ',');	
					//if()$row->prep=number_format(($row->prep), 4, '.', '');	
					$data[] = $row;
				endforeach;
				$this->db->select('max(ide)');	
				$result = $this->db->get($this->tablam.$cic);
				foreach ($result->result() as $row):				
					$row->fecr1 = "";
					$row->lotp1 = "Total:"; $row->nomt="Venta";$row->masp="MN";$row->kgmp="";$row->prep="";
					if($totp>0) $row->kgsp=number_format($totp, 2, '.', ','); else $row->kgsp='';
					if($totd>0) $row->totp='$'.number_format($totd, 2, '.', ','); else $row->totp='';
					
					//$row->ren=number_format(($totp/$totf)*100, 2, '.','');
					//$row->DolE1='';$row->TcE1='';
					$data[] = $row;	
				endforeach;
				$this->db->select('max(ide)');	
				$result = $this->db->get($this->tablam.$cic);
				foreach ($result->result() as $row):				
					$row->fecr1 = "";$row->lotp1 = ""; $row->nomt="";$row->masp="USD";$row->kgmp="";$row->prep="";
					if($totp>0) $row->kgsp=number_format($totp1, 2, '.', ','); else $row->kgsp='';
					if($totd>0) $row->totp='$'.number_format($totd1, 2, '.', ','); else $row->totp='';
					
					if($totf>0) $row->ren=number_format(($totkgs/$totf)*100, 2, '.',''); else $row->ren='';
					$row->totpm='$'.number_format($totd, 2, '.', ',');
					$row->totpu='$'.number_format($totd1, 2, '.', ',');
					//$row->DolE1='';$row->TcE1='';
					 $row->kgsp1=number_format($totkgs, 2, '.', ',');
					$data[] = $row;	
				endforeach;
			}
			}
			return $data;
		}
		function actualizard($id,$can,$uni,$des,$obs,$cans){
	 		$hoy=date("y-m-d H:i:s");
			$data=array($this->can=>$cer,$this->fr=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->cans=>$cans,$this->obs=>$obs);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrard($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		
		function borrarr($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function historygpo($idt){
			$this->db->select('gpoid');
			$this->db->from('tallas');
			$this->db->where('idt =', $idt);
			$query=$this->db->get();
			return $query->row();
		}

		function historyalm($id){
			$this->db->select('noma');
			$this->db->from('almacenes');
			$this->db->where('ida =', $id);
			$query=$this->db->get();
			return $query->row();
		}	
		
    }
    
?>