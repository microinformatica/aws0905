<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almacendia_model extends CI_Model {
        public $id="NS";	        
		public $fr="FechaR";
		public $can="CantidadS";
		public $uni="UnidadS";
		public $des="DescripcionS";
		public $cans="CanS";public $cali="cali";
		public $obs="Obs";
        public $tablasol="solicitudes";
		public $tablareq="requisicion";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getalmacendia($filter,$perfil){
			//Select Requisicion,NS,FechaS,CantidadS,UnidadS,DescripcionS,FechaR,CanS,Obs,FechaSS 
			//FROM solicitudes 
			//INNER JOIN requisicion ON Nr=NRS Where (FechaSS  >= '$fi' AND FechaSS <= '$ff' and CanS >= 0 )  
			//order by FechaS DESC, Requisicion
			$this->db->select('Requisicion,NS,FechaS,CantidadS,UnidadS,DescripcionS,FechaR,CanS,Obs,FechaSS,DATEDIFF( CURDATE(),FechaS ) AS dt,cali',NULL,FALSE);
			$this->db->join('requisicion', 'Nr=NRS','inner');
			$this->db->where('CanS >=',0);
			if($perfil==19)	{
				$this->db->where('DATEDIFF( CURDATE(),FechaS ) >=',15);
				$this->db->where('cali =',1);
			}
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Requisicion');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';$cont=0;$feci="";
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				$fecha=$row->FechaS;$des=$row->DescripcionS;$obs=$row->Obs;$cancelar=$row->CanS;
				$row->des1=$row->DescripcionS;
				$row->FechaS=substr($row->FechaS, 0, 10);
				$row->FechaS = date("d-m-Y",strtotime($row->FechaS));
				$row->hora=substr ($fecha, 11, 8);
				$inc="";$obs1="";
				if($cancelar==1){ $inc=" ** INCOMPLETO **";}
				if($obs!=""){ $obs1=" -( ".$obs." )-";}
				$row->DescripcionS=$des.$inc.$obs1; 
				$row->totp=($cont+=1); 
				$fec=$row->FechaS;
				if($feci!=$fec){	$row->FechaS1=$row->FechaS.' ['.$row->dt.']'; $feci=$row->FechaS;	}else{ $row->FechaS1="";}  				
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsdia($filter,$perfil){
			$this->db->select('Requisicion,NS,FechaS,CantidadS,UnidadS,DescripcionS,FechaR,CanS,Obs,FechaSS,DATEDIFF( CURDATE(),FechaS ) AS dt,cali',NULL,FALSE);
			$this->db->join('requisicion', 'Nr=NRS','inner');
			$this->db->where('CanS >=',0);
			if($perfil==19)	{
				$this->db->where('DATEDIFF( CURDATE(),FechaS ) >=',15);
				$this->db->where('cali =',1);
			}
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Requisicion');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function actualizar($id,$can,$uni,$des,$obs,$cans,$cali){
	 		$hoy=date("y-m-d H:i:s");
			$data=array($this->can=>$cer,$this->fr=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->cans=>$cans,$this->obs=>$obs,$this->cali=>$cali);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
    }
    
?>