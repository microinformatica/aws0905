<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Empleados_model extends CI_Model {
        //private $nombre;
        public $id="id"; public $ide="ide"; public $nombre="nombre";public $ap="ap";public $am="am";public $activo="activo";public $canu="canu";
		public $col="col";public $ciu="ciu";public $edo="edo";public $cp="cp";public $tel="tel";public $curp="curp";public $rfc="rfc";
		public $imss="imss";public $mail="mail";public $depto="depto";public $pues="puesto";public $fec="fecing";public $suel="sueldo";
		public $fnac="fnac";public $cta="ctadep";public $ctav="ctavia";public $sexo="sexo";public $civil="civil";
		public $tabla="empleados";
		
		public $idp="idp";public $pfec="pfec";public $pimp="pimp";public $pide="pide";public $pca="pca";public $pobs="pobs";public $pcancel="pcancel";
		public $tablaPre="prestamos";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function agregar($ide,$nombre,$ap,$am,$canu,$col,$ciu,$edo,$cp,$tel,$curp,$rfc,$imss,$mail,$depto,$pues,$fec,$suel,$nac,$cta,$ctav,$sex,$civ){
			$suel = str_replace(",", "", $suel);	
			$data=array($this->ide=>$ide,$this->nombre=>$nombre,$this->ap=>$ap,$this->am=>$am,$this->canu=>$canu,$this->col=>$col,$this->ciu=>$ciu,$this->edo=>$edo,$this->cp=>$cp,$this->tel=>$tel,$this->curp=>$curp,$this->rfc=>$rfc,$this->imss=>$imss,$this->mail=>$mail,$this->depto=>$depto,$this->pues=>$pues,$this->fec=>$fec,$this->suel=>$suel,$this->fnac=>$nac,$this->cta=>$cta,$this->ctav=>$ctav,$this->sexo=>$sex,$this->civil=>$civ);			
			$this->db->insert($this->tabla,$data);
			$this->db->insert_id();
			return 0;	
		}
		public function actualizar($id,$nombre,$ap,$am,$act,$canu,$col,$ciu,$edo,$cp,$tel,$curp,$rfc,$imss,$mail,$depto,$pues,$fec,$suel,$nac,$cta,$ctav,$sex,$civ){
			$suel = str_replace(",", "", $suel);
			$data=array($this->nombre=>$nombre,$this->ap=>$ap,$this->am=>$am,$this->activo=>$act,$this->canu=>$canu,$this->col=>$col,$this->ciu=>$ciu,$this->edo=>$edo,$this->cp=>$cp,$this->tel=>$tel,$this->curp=>$curp,$this->rfc=>$rfc,$this->imss=>$imss,$this->mail=>$mail,$this->depto=>$depto,$this->pues=>$pues,$this->fec=>$fec,$this->suel=>$suel,$this->fnac=>$nac,$this->cta=>$cta,$this->ctav=>$ctav,$this->sexo=>$sex,$this->civil=>$civ);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0){
				return 1;
			} else {
				return 0;
			}
		}
		public function pagregar($pfec,$pimp,$pide,$pca,$pobs){
			$pimp = str_replace(",", "", $pimp);	
			$data=array($this->pfec=>$pfec,$this->pimp=>$pimp,$this->pide=>$pide,$this->pca=>$pca,$this->pobs=>$pobs);			
			$this->db->insert($this->tablaPre,$data);
			$this->db->insert_id();
			return 0;	
		}
		public function pactualizar($id_post,$pfec,$pimp,$pide,$pca,$pobs){
			$pimp = str_replace(",", "", $pimp);
			$data=array($this->pfec=>$pfec,$this->pimp=>$pimp,$this->pide=>$pide,$this->pca=>$pca,$this->pobs=>$pobs);
			$this->db->where($this->idp,$id_post);
			$this->db->update($this->tablaPre,$data);
			if($this->db->affected_rows()>0){
				return 1;
			} else {
				return 0;
			}
		}
		function pborrar($id){
			$this->db->where($this->idp,$id);
			$this->db->delete($this->tablaPre);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function cancelar($empleado,$valor){						
			$this->db->where($this->pide,$empleado);
			$result = $this->db->get($this->tablaPre);
			foreach ($result->result() as $rowd):
					$reg=$rowd->idp;
					$datad=array($this->pcancel=>$valor);
					$this->db->where($this->idp,$reg);
					$this->db->update($this->tablaPre,$datad);
			endforeach;
				//}
			return;
		}	
		function getUsuarios($filter){
			//$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Zona');
			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			else $this->db->order_by('ide');
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where'],'Desc');
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$cont=0;$fec=new Libreria();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->ing = $fec->fecha($row->fecing);
				$row->sueldo = number_format($row->sueldo, 2, '.', ',');$cont+=1;$row->totp=($cont);
				if($row->ctadep==0) $row->ctadep='';if($row->ctavia==0) $row->ctavia='';
				$data[] = $row;	
				
			endforeach;
			return $data;
		}
		function getNumRows($filter){
			//$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Zona');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getPrestamos($filter){
			$this->db->select('idp,pfec,pimp,pide,pca,pobs,nombre,ap,am,pcancel');
			$this->db->join($this->tabla, $this->ide.'='.$this->pide, 'inner');			
			$this->db->order_by($this->pca);$this->db->order_by($this->pfec);$this->db->order_by($this->pide);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaPre,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaPre);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$cli='';$totgranja=0;$fec=new Libreria();$fech='';
			foreach($result->result() as $row):
				$zona=$row->pca;
				if($cli!=$zona){
						if($totgranja>0){ 	
							//aqui se mete el total segun la zona
							$this->db->select('max(numero)');			
							$resultZ = $this->db->get('clientes');
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->cap = "Total:";$rowZ->pide = "";$rowZ->nom = "";$rowZ->pobs = "";$rowZ->pfecf ="";
								$rowZ->pimpp ='$ '. number_format($totgranja, 2, '.', ',');
								$data[] = $rowZ;	
							endforeach;	
							$totgranja=0;
						}
						//$row->Zona=$row->pca;
						if($row->pca==1){$row->cap='Cargos';} else {$row->cap='Abonos';} 
						$cli=$row->pca; 
				}else{ $row->cap="";} 
				if($row->pfec!=$fech)
				{$row->pfecf = $fec->fecha($row->pfec);
				$fech=$row->pfec;
				}else{
				$row->pfecf = "";}
				$totgranja+=$row->pimp;	
				$row->nom = $row->ap.' '.$row->am.' '.$row->nombre;
				$row->pimpp ='$ '. number_format($row->pimp, 2, '.', ',');
				$row->pimp = number_format($row->pimp, 2, '.', ',');
				$data[] = $row;	
				
			endforeach;
			if($totgranja>0){ 	
				$this->db->select('max(numero)');			
				$resultZ = $this->db->get('clientes');
				foreach ($resultZ->result() as $rowZ):				
					$rowZ->cap = "Total:";$rowZ->pide = "";$rowZ->nom = "";$rowZ->pobs = "";$rowZ->pfecf ="";
					$rowZ->pimpp ='$ '. number_format($totgranja, 2, '.', ',');
					$data[] = $rowZ;	
				endforeach;	
				$totgranja=0;
			}
			return $data;
		}
		function getNumRowsP($filter){
			$this->db->select('idp,pfec,pimp,pide,pca,pobs,nombre,ap,am');
			$this->db->join($this->tabla, $this->ide.'='.$this->pide, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tablaPre);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getEmpleados($filter){
			//$this->db->select('ide,nombre,ap,am');
			$this->db->select('ide,nombre,ap,am,(select sum(pimp) from prestamos where pide=ide and pca=1 and pcancel=0)as car,(select sum(pimp) from prestamos where pide=ide and pca=2 and pcancel=0)as abo,pcancel');
			$this->db->join($this->tabla,$this->ide.'='.$this->pide);
			$this->db->group_by($this->pide);
			$this->db->order_by($this->ide);
			//$this->db->order_by('ap');$this->db->order_by('am');$this->db->order_by('nombre');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaPre,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaPre);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$saldofila=0;$totC=0;$totA=0;
			//Se forma el arreglo que sera retornado
			/*foreach($result->result() as $row):
				$row->nom = '[ '.$row->ide.' ] '.$row->nombre.' '. $row->ap.' '.$row->am;
				$data[] = $row;	
				
			endforeach;*/
			foreach($result->result() as $row):
				$row->nombre = $row->nombre.' '. $row->ap.' '.$row->am;
				$row->nom = '[ '.$row->ide.' ] '.$row->nombre;
				$saldofila=$row->car-$row->abo;
				if($row->car>0){ $totC+=$row->car;$row->car="$ ".number_format($row->car, 2, '.', ','); } else{ $row->car="";}
				if($row->abo>0){ $totA+=$row->abo;$row->abo="$ ".number_format($row->abo, 2, '.', ','); } else{ $row->abo="";}
				$row->sal="$ ".number_format($saldofila, 2, '.', ',');
				$row->sal1=$saldofila;
				$data[] = $row;	
				
			endforeach;
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');			
			foreach ($resultZ->result() as $rowZ):				
				$rowZ->ide = "TOTALES";$rowZ->nombre = "";$rowZ->ap = "";$rowZ->am = "";
				$rowZ->car="$ ".number_format($totC, 2, '.', ',');
				$rowZ->abo="$ ".number_format($totA, 2, '.', ',');
				$rowZ->sal="$ ".number_format($totC-$totA, 2, '.', ',');
				$data[] = $rowZ;	
			endforeach;	
			return $data;
		}
		function getNumRowsE($filter){
			$this->db->join($this->tabla,$this->ide.'='.$this->pide);
			$this->db->group_by($this->pide);
			$this->db->order_by($this->ide);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tablaPre);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}	
		
		function getEmpleadosEDO($filter){
			//$this->db->select('ide,nombre,ap,am');
			//$this->db->select('ide,nombre,ap,am');
			//
			
			$this->db->order_by('pfec');$this->db->order_by('pca','DESC');
			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaPre,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaPre);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$totab=0;$totca=0;$tot=0;$fec=new Libreria(); $saldofila=0;$totC=0;$totA=0;
			$cantidadSI=$result->num_rows();
			if($cantidadSI>0){
			foreach($result->result() as $row):
				$row->fecd = $fec->fecha($row->pfec);
				$tot=$row->pimp;
				if($row->pca==1){$saldofila=$saldofila+$row->pimp;$totC=$totC+$row->pimp;}else{$saldofila=$saldofila-$tot;$totA=$totA+$tot;}
				if($row->pca==1){ $row->car="$ ".number_format($row->pimp, 2, '.', ','); $totca+=$row->pimp;} else{ $row->car="";}
				if($tot>0 && $row->pca==2){ $row->abo="$ ".number_format($tot, 2, '.', ','); $totab+=$tot;} else{ $row->abo="";}
				$row->sal="$ ".number_format($saldofila, 2, '.', ',');
				$data[] = $row;	
			endforeach;
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			$dias=$fec->fecha(date("Y-m-d"));
			foreach ($resultZ->result() as $rowZ):				
				$rowZ->fecd = "TOTALES";$rowZ->pobs = "AL DIA ".$dias;
				$rowZ->car="$ ".number_format($totC, 2, '.', ',');
				$rowZ->abo="$ ".number_format($totA, 2, '.', ',');
				$rowZ->sal="$ ".number_format($totC-$totA, 2, '.', ',');
				$data[] = $rowZ;	
			endforeach;	
			}
			return $data;
		}
		function getNumRowsEDO($filter){
			//$this->db->select('ide,nombre,ap,am,(ap+am+nombre)as nom');
			//$this->db->order_by('ap');$this->db->order_by('am');$this->db->order_by('nombre');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tablaPre);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}		
    }
    
?>