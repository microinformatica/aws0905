<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Ventasm_model extends CI_Model {
        //private $nombre;
        public $id="Numero"; public $nombre="Razon"; public $dom="Dom";public $ubi="ubigra"; public $loc="Loc";public $edo="Edo";
		public $rfc="RFC";public $cp="CP";public $cor="cor";public $con="con";public $tel="tel";public $zona="Zona";public $status="status";
		public $prev="prevta";public $avi="avisoc";public $gui="guiat";
		public $tabla="clientes";
        
		
		public $tablas="salidas";
		public $idsal="idsal";public $fecsal="fecsal";public $estsal="estsal";public $almsal="almsal";public $clisal="clisal";
		public $kgssal="kgssal";public $cajsal="cajsal";public $presal="presal";public $talsal="talsal";public $idents="idents";
		public $folsal="folsal";public $consala="consala";public $consalc="consalc";public $grasal="grasal";public $prekgs="prekgs";
		public $facsal="facsal";public $aviso="aviso";public $usdkgs="usdkgs";public $obssal="obssal";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		public function actualizar($id,$pre,$fac,$avis,$usd,$obs){
			$data=array($this->prekgs=>$pre,$this->facsal=>$fac,$this->aviso=>$avis,$this->usdkgs=>$usd,$this->obssal=>$obs);
			$this->db->where($this->idsal,$id);
			$this->db->update($this->tablas,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
		function getEstatus($cic){
			$queryvg=$this->db->query("SELECT Numero,idg,sigg,nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta,(select sum(Pesos) from depositos where NRC=clisal and bormaq=2 and Car=-1 and Aplicar='$cic') as carvta,(select sum(Pesos) from depositos where NRC=clisal and Des=0 and bormaq=2 and Aplicar='$cic') as depvta,(select sum(Pesos) from depositos where NRC=clisal and bormaq=2 and Des=-1 and Aplicar='$cic') as desvta,sum(kgssal*usdkgs) as impvtau,(select sum(ImporteD) from depositos where NRC=clisal and Des=0 and Aplicar='$cic') as depvtau,(select sum(ImporteD) from depositos where NRC=clisal and Des=-1 and Aplicar='$cic' ) as desvtau from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where estsal=1 and ciclo='$cic' group by clisal");
			$data = array();$tk=0;$tv=0;$tcv=0;$td=0;$ts=0;$gra='';$tg=0;$tkg=0;$tvg=0;$tcvg=0;$tdg=0;$tsg=0;$granjas=0;$tvu=0;$tdu=0;$tsu=0;$tvgu=0;$tdgu=0;$tsgu=0;$tdsgu=0;$tdsu=0;$tds=0;$tdsg=0;$entro=1;
			$fec=new Libreria();
			
			if($queryvg->num_rows()>0){
			 foreach($queryvg->result() as $row):
				$tkg+=$row->kgsvta;$tvg+=$row->impvta;$tcvg+=$row->carvta;$tdg+=$row->depvta;$tsg+=$row->impvta+$row->carvta-$row->depvta-$row->desvta;
				$tvgu+=$row->impvtau;$tdgu+=$row->depvtau;$tsgu+=$row->impvtau-$row->depvtau-$row->desvtau;$tdsgu+=$row->desvtau;$tdsg+=$row->desvta;
				$tk+=$row->kgsvta;$tv+=$row->impvta;$tcv+=$row->carvta;$td+=$row->depvta;
				
				$tvu+=$row->impvtau;$tdu+=$row->depvtau;$tsu+=$row->impvtau-$row->depvtau-$row->desvtau;$tdsu+=$row->desvtau;$tds+=$row->desvta;
				if(($row->impvta)-($row->depvta+$row->desvta)>0) $row->salvta='$'.number_format((($row->impvta+$row->carvta)-($row->depvta+$row->desvta)), 2, '.', ','); else $row->salvta='';
				if(($row->impvtau)-($row->depvtau+$row->desvtau)>0) $row->salvtau='$'.number_format(($row->impvtau-($row->depvtau+$row->desvtau)), 2, '.', ','); else $row->salvtau='';
				$row->salvta1=($row->impvta+$row->carvta)-($row->depvta+$row->desvta);
				$row->fecmn1='';$row->diasmn='';$row->fecus1='';$row->diasus='';
				if(($row->impvta)-($row->depvta+$row->desvta)>0){
					$ts+=($row->impvta+$row->carvta)-($row->depvta+$row->desvta);
				//select fecsal,kgssal,prekgs,usdkgs,(kgssal*prekgs) as vtamn,(kgssal*usdkgs) as vtaus from salidas where clisal=32 order by fecsal desc
				$this->db->select('fecsal,kgssal,prekgs,usdkgs,(kgssal*prekgs) as vtamn,(kgssal*usdkgs) as vtaus');
				$this->db->where('clisal',$row->Numero);
				$this->db->where('ciclo',$cic);
				$this->db->order_by('fecsal','DESC');
				$resultq = $this->db->get($this->tablas);
				$suma=0;
				foreach ($resultq->result() as $rowt):	
						if($suma<=($row->impvta+$row->carvta)-($row->depvta+$row->desvta)){
							$row->fecmn=$rowt->fecsal;$suma+=$rowt->vtamn;
							$row->fecmn1 = $fec->fecha($rowt->fecsal);
							$fechaActual = date('Y-m-d'); 
 							$datetime1 = date_create($rowt->fecsal);
  							$datetime2 = date_create($fechaActual);
  							$contador = date_diff($datetime1, $datetime2);
  							$differenceFormat = '%a';
  							$row->diasmn=$contador->format($differenceFormat);
							$suma+=$rowt->vtamn;
						}	
							
				endforeach;
				}
				$data[] = $row;
			 endforeach;
			 //antes del total poner todos los clientes que no han tenido ventas pero que tienen depositos y son saldo a favor
				/*
					$this->db->select('Razon,(select sum(kgssal*prekgs) from salidas where clisal=Numero) as impvta,(select sum(Pesos) from depositos where NRC=Numero) as depvta,(select sum(Pesos) from depositos where NRC=NUmero and bormaq=2 and Des=-1) as desvta,(select sum(Pesos) from depositos where NRC=NUmero and bormaq=2 and Car=-1) as carvta');
					$this->db->join('depositos', 'Numero=NRC','inner');	
					$this->db->where('Des',0);$this->db->where('factura','0');$this->db->where('estatuss',0);
					$this->db->group_by('Razon');
					$resultq = $this->db->get($this->tabla);
					foreach ($resultq->result() as $rowt):	
						if($rowt->impvta==0){	
						$ts=$ts-$rowt->depvta;
						$rowt->Razon="";
						$rowt->salvta='$'.number_format($rowt->carvta-$rowt->depvta-$rowt->desvta, 2, '.', ',');
						$rowt->salvtau='';
						$data[] = $rowt;	
						}
					endforeach;
				 */ 
			 //total gral
			 	$this->db->select('max(Numero)');	
				$result = $this->db->get($this->tabla);
				foreach ($result->result() as $row):				
					$row->Razon="Gran Total";$row->fecmn1='';$row->diasmn='';$row->fecus1='';$row->diasus='';
					$row->salvta='$ '.number_format($ts, 2, '.', ','); 
					$row->salvtau='$ '.number_format($tsu, 2, '.', ',');
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}	
		
		// VEntas generales y por granja
		//SELECT nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where NRC=clisal) as depvta from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where estsal=1 group by clisal

		function getVentasg($cic,$gra){
			$graz=$gra;	$cic='20'.$cic;
			if($graz==120){
				$queryvg=$this->db->query('SELECT Numero,idg,sigg,nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where NRC=clisal and Des=0 and Car=0 and bormaq=2 and Aplicar='.$cic.') as depvta,sum(kgssal*usdkgs) as impvtau,(select sum(ImporteD) from depositos where NRC=clisal and Des=0 and Aplicar='.$cic.') as depvtau,(select sum(ImporteD) from depositos where NRC=clisal and Des=-1 and Aplicar='.$cic.') as desvtau,(select sum(Pesos) from depositos where NRC=clisal and bormaq=2 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where NRC=clisal and bormaq=2 and Car=-1 and Aplicar='.$cic.') as carvta from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where ciclo='.$cic.' and estsal=1 group by clisal');
			}elseif($graz==0) {
				$queryvg=$this->db->query('SELECT Numero,idg,sigg,nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where ngra=idg and NRC=clisal and Des=0 and Car=0 and bormaq=2 and Aplicar='.$cic.') as depvta,sum(kgssal*usdkgs) as impvtau,(select sum(ImporteD) from depositos where ngra=idg and NRC=clisal and bormaq=2 and Des=0 and Aplicar='.$cic.') as depvtau,(select sum(ImporteD) from depositos where ngra=idg and bormaq=2 and NRC=clisal and Des=-1 and Aplicar='.$cic.') as desvtau,(select sum(Pesos) from depositos where ngra=idg and  NRC=clisal and bormaq=2 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where ngra=idg and NRC=clisal and bormaq=2 and Car=-1  and Aplicar='.$cic.') as carvta from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where ciclo='.$cic.' and estsal=1 group by nomg,clisal');	
			}else{
				$queryvg=$this->db->query('SELECT Numero,idg,sigg,nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where ngra=idg and NRC=clisal and Des=0 and Car=0 and bormaq=2 and Aplicar='.$cic.') as depvta,sum(kgssal*usdkgs) as impvtau,(select sum(ImporteD) from depositos where ngra=idg and NRC=clisal and Des=0 and Aplicar='.$cic.') as depvtau,(select sum(ImporteD) from depositos where ngra=idg and NRC=clisal and Des=-1 and Aplicar='.$cic.') as desvtau,(select sum(Pesos) from depositos where ngra=idg and  NRC=clisal and bormaq=2 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where ngra=idg and  NRC=clisal and bormaq=2 and Car=-1 and Aplicar='.$cic.') as carvta from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where ciclo='.$cic.' and estsal=1 and grasal='.$gra.' group by clisal');	
			}
			$data = array();$tk=0;$tv=0;$tcv=0;$td=0;$ts=0;$gra='';$tg=0;$tkg=0;$tvg=0;$tcvg=0;$tdg=0;$tsg=0;$granjas=0;$tvu=0;$tdu=0;$tsu=0;$tvgu=0;$tdgu=0;$tsgu=0;$tdsgu=0;$tdsu=0;$tds=0;$tdsg=0;$entro=1;
			if($graz==120) $entro=1;
			if($queryvg->num_rows()>0){
			 foreach($queryvg->result() as $row):
				if($gra!=$row->sigg){
					if($tg>0 and $entro==1){
						//antes del total poner todos los clientes que no han tenido ventas pero que tienen depositos y son saldo a favor
					if($graz==120) $this->db->select('Razon,(select sum(kgssal*prekgs) from salidas where ciclo='.$cic.' and clisal=Numero) as impvta,(select sum(Pesos) from depositos where NRC=Numero and Aplicar='.$cic.') as depvta,(select sum(Pesos) from depositos where NRC=Numero and bormaq=2 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where NRC=Numero and bormaq=2 and Car=-1 and Aplicar='.$cic.') as carvta');
					else $this->db->select('Razon,(select sum(kgssal*prekgs) from salidas where ciclo='.$cic.' and  ngra='.$granja.' and clisal=Numero) as impvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and bormaq=2 and Aplicar='.$cic.') as depvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and bormaq=2 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and bormaq=2 and Car=-1 and Aplicar='.$cic.') as carvta');
					$this->db->join('depositos', 'Numero=NRC','inner');
					$this->db->where('Des',0);$this->db->where('factura','0');$this->db->where('estatuss',0);
					if($graz!=120) $this->db->where('ngra',$granja);
					$this->db->group_by('Razon');
					$resultq = $this->db->get($this->tabla);
					if($resultq->num_rows()>0){
					foreach ($resultq->result() as $rowt):	
					 	if($rowt->impvta==0){
						$tdg+=$rowt->depvta;$tsg=$tsg-$rowt->depvta;$ts=$ts-$rowt->depvta;$td+=$rowt->depvta;
						$rowt->sigg="";//$rowt->Razon="";
						$rowt->kgsvta='';$rowt->impvta='';
						$rowt->salvta='$'.number_format($rowt->carvta-$rowt->depvta-$rowt->desvta, 2, '.', ',');
						$rowt->depvta='$'.number_format($rowt->depvta, 2, '.', ',');
						$rowt->impvtau='';$rowt->depvtau='';$rowt->desvtau='';$rowt->salvtau='';
						$data[] = $rowt;
						}
						if($rowt->carvta==0) $rowt->carvta='';
					endforeach;
					} 
						$this->db->select('max(Numero)');	
						$result = $this->db->get($this->tabla);
						foreach ($result->result() as $rowt):				
							$rowt->sigg="";$rowt->Razon="Total Granja";
							$rowt->kgsvta=number_format($tkg, 2, '.', ','); 
							if($tvg>0) $rowt->impvta='$'.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
							if($tcvg>0) $rowt->carvta='$'.number_format($tcvg, 2, '.', ','); else $rowt->carvta='';
							if($tdg>0) $rowt->depvta='$'.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
							if($tdsg>0) $rowt->desvta='$'.number_format($tdsg, 2, '.', ','); else $rowt->desvta='';
							if($tvgu>0) $rowt->impvtau='$'.number_format($tvgu, 2, '.', ','); else $rowt->impvtau='';
							if($tdgu>0) $rowt->depvtau='$'.number_format($tdgu, 2, '.', ','); else $rowt->depvtau='';
							if($tdsgu>0) $rowt->desvtau='$'.number_format($tdsgu, 2, '.', ','); else $rowt->desvtau='';
							$rowt->salvta='$'.number_format($tsg, 2, '.', ',');
							$rowt->salvtau='$'.number_format($tsgu, 2, '.', ',');
							$data[] = $rowt;	
						endforeach;
					   $tkg=0;$tvg=0;$tcvg=0;$tdg=0;$tsg=0;$tvgu=0;$tdgu=0;$tsgu=0;$tdsgu=0;$tdsg=0;
					}	
					
					if($graz==120){$row->sigg='OA';$tg=0;} else {$tg=1;$gra=$row->sigg;$granjas+=1;$granja=$row->idg;}
				}else{ //$row->feccos1="";
					if($graz==120){}else{$granja=$row->idg; $clientes=$row->Numero;}
					$row->sigg="";
				}  
				
				$tkg+=$row->kgsvta;$tvg+=$row->impvta;$tcvg+=$row->carvta;$tdg+=$row->depvta;$tsg+=$row->impvta+$row->carvta-$row->depvta-$row->desvta;
				$tvgu+=$row->impvtau;$tdgu+=$row->depvtau;$tsgu+=$row->impvtau-$row->depvtau-$row->desvtau;$tdsgu+=$row->desvtau;$tdsg+=$row->desvta;
				$tk+=$row->kgsvta;$tv+=$row->impvta;$tcv+=$row->carvta;$td+=$row->depvta;$ts+=$row->impvta+$row->carvta-$row->depvta-$row->desvta;
				$tvu+=$row->impvtau;$tdu+=$row->depvtau;$tsu+=$row->impvtau-$row->depvtau-$row->desvtau;$tdsu+=$row->desvtau;$tds+=$row->desvta;
				if(($row->impvta)-($row->depvta+$row->desvta)!=0) $row->salvta='$'.number_format((($row->impvta+$row->carvta)-($row->depvta+$row->desvta)), 2, '.', ','); else $row->salvta='';
				if($row->impvtau-$row->depvtau!=0) $row->salvtau='$'.number_format(($row->impvtau-$row->depvtau-$row->desvtau), 2, '.', ','); else $row->salvtau='';
				$row->salvta1=($row->impvta+$row->carvta)-($row->depvta+$row->desvta);
				$row->kgsvta=number_format(($row->kgsvta), 2, '.', ','); 
				if($row->impvta>0) $row->impvta='$'.number_format(($row->impvta), 2, '.', ','); else $row->impvta=''; 
				if($row->carvta>0) $row->carvta='$'.number_format(($row->carvta), 2, '.', ','); else $row->carvta='';
				if($row->depvta>0) $row->depvta='$'.number_format(($row->depvta), 2, '.', ','); else $row->depvta=''; 
				if($row->desvta>0) $row->desvta='$'.number_format(($row->desvta), 2, '.', ','); else $row->desvta='';
				if($row->impvtau>0) $row->impvtau='$'.number_format(($row->impvtau), 2, '.', ','); else $row->impvtau=''; 
				if($row->depvtau>0) $row->depvtau='$'.number_format(($row->depvtau), 2, '.', ','); else $row->depvtau='';
				if($row->desvtau>0) $row->desvtau='$'.number_format(($row->desvtau), 2, '.', ','); else $row->desvtau='';
				$data[] = $row;
			 endforeach;
			 //antes del total poner todos los clientes que no han tenido ventas pero que tienen depositos y son saldo a favor
				/*
					if($graz==120) $this->db->select('Razon,(select sum(kgssal*prekgs) from salidas where ciclo='.$cic.' and clisal=Numero) as impvta,(select sum(Pesos) from depositos where NRC=Numero and Aplicar='.$cic.') as depvta,(select sum(Pesos) from depositos where NRC=NUmero and bormaq=1 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where NRC=NUmero and bormaq=1 and Car=-1 and Aplicar='.$cic.') as carvta');
					else $this->db->select('Razon,(select sum(kgssal*prekgs) from salidas where ciclo='.$cic.' and ngra='.$granja.' and clisal=Numero) as impvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and Aplicar='.$cic.') as depvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and bormaq=1 and Des=-1 and Aplicar='.$cic.') as desvta,(select sum(Pesos) from depositos where ngra='.$granja.' and NRC=Numero and bormaq=1 and Car=-1 and Aplicar='.$cic.') as carvta');
					$this->db->join('depositos', 'Numero=NRC','inner');	
					$this->db->where('Des',0);$this->db->where('factura','0');$this->db->where('estatuss',0);
					if($graz!=120) $this->db->where('ngra',$granja);
					$this->db->group_by('Razon');
					$resultq = $this->db->get($this->tabla);
					if($resultq->num_rows()>0){
					foreach ($resultq->result() as $rowt):	
						if($rowt->impvta==0){	
						$tdg+=$rowt->depvta;$tsg=$tsg-$rowt->depvta;$ts=$ts-$rowt->depvta;$td+=$rowt->depvta;
						$rowt->sigg="";//$rowt->Razon="";
						$rowt->kgsvta='';$rowt->impvta='';
						$rowt->salvta='$'.number_format(+$rowt->carvta-$rowt->depvta-$rowt->desvta, 2, '.', ',');
						$rowt->depvta='$'.number_format($rowt->depvta, 2, '.', ',');
						$rowt->impvtau='';$rowt->depvtau='';$rowt->desvtau='';$rowt->salvtau='';
						$data[] = $rowt;	
						}
						if($rowt->carvta==0) $rowt->carvta='';
					endforeach;
					}*/
			 if($tg>0 and $granjas>1){
			 	
						$this->db->select('max(Numero)');	
						$result = $this->db->get($this->tabla);
						foreach ($result->result() as $rowt):				
							$rowt->sigg="";$rowt->Razon="Total Granja";
							$rowt->kgsvta=number_format($tkg, 2, '.', ','); 
							if($tvg>0) $rowt->impvta='$'.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
							if($tcvg>0) $rowt->carvta='$'.number_format($tcvg, 2, '.', ','); else $rowt->carvta='';
							if($tdg>0) $rowt->depvta='$'.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
							if($tdsg>0) $rowt->desvta='$'.number_format($tdsg, 2, '.', ','); else $rowt->desvta='';
							if($tvgu>0) $rowt->impvtau='$'.number_format($tvgu, 2, '.', ','); else $rowt->impvtau='';
							if($tdgu>0) $rowt->depvtau='$'.number_format($tdgu, 2, '.', ','); else $rowt->depvtau='';
							if($tdsgu>0) $rowt->desvtau='$'.number_format($tdsgu, 2, '.', ','); else $rowt->desvtau='';
							$rowt->salvta='$ '.number_format($tsg, 2, '.', ',');
							$rowt->salvtau='$ '.number_format($tsgu, 2, '.', ',');
							$data[] = $rowt;	
						endforeach;
					   $tkg=0;$tvg=0;$tcvg=0;$tdg=0;$tsg=0;$tvgu=0;$tdgu=0;$tsgu=0;$tsg=0;
					}	
			 //total granja
			/* if($granjas>1){
			 	$this->db->select('max(Numero)');	
				$result = $this->db->get($this->tabla);
				foreach ($result->result() as $rowt):				
					$rowt->nomg="";$rowt->Razon="Total Granja";
					$rowt->kgsvta=number_format($tkg, 0, '.', ','); 
					if($tvg>0) $rowt->impvta='$ '.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
					if($tdg>0) $rowt->depvta='$ '.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
					$rowt->salvta='$ '.number_format($tsg, 2, '.', ',');
					$data[] = $rowt;	
				endforeach;
			 }	*/
			 //total gral
			 	$this->db->select('max(Numero)');	
				$result = $this->db->get($this->tabla);
				foreach ($result->result() as $row):				
					$row->sigg="Gran Total";$row->Razon="";
					$row->kgsvta=number_format($tk, 2, '.', ','); 
					if($tv>0) $row->impvta='$'.number_format($tv, 2, '.', ','); else $row->impvta='';
					if($tcv>0) $row->carvta='$'.number_format($tcv, 2, '.', ','); else $row->carvta='';
					if($td>0) $row->depvta='$'.number_format($td, 2, '.', ','); else $row->depvta='';
					if($tds>0) $row->desvta='$'.number_format($tds, 2, '.', ','); else $row->desvta='';
					if($tvu>0) $row->impvtau='$'.number_format($tvu, 2, '.', ','); else $row->impvtau='';
					if($tdu>0) $row->depvtau='$'.number_format($tdu, 2, '.', ','); else $row->depvtau='';
					if($tdsu>0) $row->desvtau='$'.number_format($tdsu, 2, '.', ','); else $row->desvtau='';
					$row->salvta='$ '.number_format($ts, 2, '.', ','); 
					$row->salvtau='$ '.number_format($tsu, 2, '.', ',');
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}	
		function getVentas($filter){
			//SELECT fecsal,folsal,noma,Razon,nomt,cajsal,presal,kgssal,idsal from almacenes inner join( clientes inner join ( tallas inner join salidas on talsal=idt) on clisal=Numero )on almsal=ida where estsal=1
			
			$this->db->select('fecsal,folsal,almsal,noma,consala,clisal,Razon,con,consalc,idsal,estsal,nomt,cajsal,presal,kgssal,prekgs,facsal,aviso,usdkgs,obssal');
			$this->db->join('almacenes', 'ida=almsal','inner');
			$this->db->join('clientes', 'Numero=clisal','inner');
			$this->db->join('tallas', 'idt=talsal','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecsal','DESC');
			$this->db->order_by('folsal','DESC');
			//$this->db->where('estsal','1');
			$result = $this->db->get($this->tablas);
			$data = array();$cic='';$alm='';$gra='';$tk=0;$ti=0;$tiu=0;$tku=0;$tkp=0;
			if($result->num_rows()>0){
				$feci=new Libreria();
			 foreach($result->result() as $row):
				 $cli='';
				 $row->fecsal1=$feci->fecha23($row->fecsal);
				 $row->folsal=str_pad($row->folsal, 5, "0", STR_PAD_LEFT);
				 if($row->estsal==1){$row->estatus='Venta';} else {
				 	$row->estatus='Traslado';
					//busca el almacen del traslado y se le llamara Razon
					$this->db->select('noma as nomc');	
					$this->db->where('ida',$row->clisal);
					$resulta = $this->db->get('almacenes');
					foreach ($resulta->result() as $rowa):
						$cli=$rowa->nomc;
					endforeach;	
					$row->Razon=$cli;
				} 

				 if($row->prekgs>0){
				 	 $row->prekgs1='$ '.number_format(($row->prekgs), 2, '.', ',');
					 $ti+=$row->kgssal*$row->prekgs;
				 	 $row->impvta='$ '.number_format(($row->kgssal*$row->prekgs), 2, '.', ',');
					 $row->prekgs2='';$row->impvtau='';
					 $tkp+=$row->kgssal;
					 //(kgssal*prekgs) as impvta
				 }elseif($row->usdkgs>0){
				 	$row->prekgs2='$ '.number_format(($row->usdkgs), 2, '.', ',');
					$tiu+=$row->kgssal*$row->usdkgs;
				 	$row->impvtau='$ '.number_format(($row->kgssal*$row->usdkgs), 2, '.', ',');
					$row->prekgs1='';$row->impvta='';
					$tku+=$row->kgssal;
				  }elseif($row->prekgs==0 and $row->usdkgs==0) {
				 	$row->prekgs1='';$row->impvta='';$row->prekgs='';
					$row->prekgs2='';$row->impvtau='';$row->usdkgs='';
				 }else{
				 	
				 }
				$tk+=$row->kgssal;
				 $row->kgssal=number_format(($row->kgssal), 2, '.', ',');
				$row->presal=$row->presal.' kgs'; 			 
				if($row->facsal==0) $row->facsal='';  //else $row->facsal=str_pad($row->facsal, 4, "0", STR_PAD_LEFT);
				if($row->aviso==0) $row->aviso='';
				if($row->obssal=='') $row->obssal='';
				$data[] = $row;
			 endforeach;
			//total gral
			 	$this->db->select('max(Numero)');	
				$result = $this->db->get($this->tabla);
				foreach ($result->result() as $row):				
					$row->fecsal1="";$row->folsal="";$row->noma="";$row->Razon="";$row->nomt='kgs. ';$row->facsal="";$row->aviso="";$row->obssal="";
					if($tk>0) $row->kgssal=number_format($tk, 0, '.', ',');  else $row->kgssal='';
					if($tkp>0 && $ti>0) $row->prekgs1='$ '.number_format($ti/$tkp, 2, '.', ','); else $row->prekgs1='';
					if($tku>0 && $tiu>0) $row->prekgs2='$ '.number_format($tiu/$tku, 2, '.', ','); else $row->prekgs2='';
					if($ti>0) $row->impvta='$ '.number_format($ti, 2, '.', ','); else $row->impvta='';
					if($tiu>0) $row->impvtau='$ '.number_format($tiu, 2, '.', ','); else $row->impvtau=''; 
					$data[] = $row;	
				endforeach;
			}
			
			return $data;
		}
    }
?>