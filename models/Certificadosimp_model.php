<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificadosimp_model extends CI_Model {        
        public $id="NumReg";  	//public $id="NumReg";
        public $numcer="NumCer";	//public $numcer="NumCer";
        public $iv="IniVig";	//public $iv="IniVig";
        public $fv="FinVig";	// public $fv="FinVig";
        public $cp="CanPos";	//public $cp="CanPos";
        public $cn="CanNau";	//public $cn="CanNau";
        public $tabla="certificados";	//public $tabla="certificados";
		public $cer="NumCerR";
		public $rem="NumRegR";
        
		public $tablarem="r17";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		
		function getCertificadosimp($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get('r17');
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				//$row->CanPos = number_format(($row->CanPos), 3, '.', ','); if($row->CanPos==0.000){$row->CanPos="";}
				if($raz!=$row->Razon){
					$raz=$row->Razon ;
					if($totgranja>0){
						$this->db->select('max(NumRegR)');
						$resultt = $this->db->get('r17');
						foreach($resultt->result() as $rowt):
							$rowt->Razon = $dom;
							$rowt->FechaR = "";
							$rowt->RemisionR = "Total:";
							$rowt->CantidadRR = number_format(($totgranja), 3, '.', ',');
							$data[] = $rowt;
							$totgranja=0;
						endforeach;
						
					}	
					//$dom=$row->Loc.",".$row->Edo; // aqui es fiscal		
					$dom="Dom. ".$row->ubigra.",".$row->Edo; // aqui es GRanja
				} else{ 
		  			$row->Razon="";	
				
		  		} 
				$canpos=$row->CanPos;$inivig=$row->IniVig;$finvig=$row->FinVig;$cer=$row->NumCer;
				$ent+=$row->CantidadRR;$totgranja+=$row->CantidadRR;
				$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->CanPos = number_format(($row->CanPos), 3, '.', ','); 
				$row->FechaR = date("d-m-Y",strtotime($row->FechaR));
				$data[] = $row;
			endforeach;
			$this->db->select('max(NumRegR)');
			$resultt = $this->db->get('r17');
			foreach($resultt->result() as $rowt):
					$rowt->NumCer = $cer;
					$rowt->IniVig=date("d-m-Y",strtotime($inivig));
					$rowt->FinVig=date("d-m-Y",strtotime($finvig));
					$rowt->CanPos=number_format(($canpos), 3, '.', ',');
					$rowt->dif=number_format(($canpos-$ent), 3, '.', ',');
					$rowt->entregado = number_format(($ent), 3, '.', ',');
					$rowt->Razon = $dom;
					$rowt->FechaR = "";
					$rowt->RemisionR = "Total:";
					$rowt->CantidadRR = number_format(($totgranja), 3, '.', ',');
					$data[] = $rowt;
					$totgranja=0;
			endforeach;
			}
			return $data;
		}
		
		function getNumRowsCimp($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarem);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function verCertificado(){
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		function getCertificadosNum($cer){
			$this->db->where('NumReg',$cer);
			$query = $this->db->get($this->tabla);
			return $query->result();
			
		}
    }
    
?>