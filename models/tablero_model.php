<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Tablero_model extends CI_Model {        
        public $id="ideqp";  	
        public $tip="tipo";
        public $mod="mod";	
        public $dep="depa";	
        public $ubi="ubi";	
        public $fec="fec";	
		public $feca="feca";
		public $fac="fac";
		public $pre="pre";
		public $pro="pro";
		public $ser="servicio";
        public $tabla="equipo";	
		
		public $cat="cat"; public $usu="usu";
		public $tablacat="equipocategoria";
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		public function agregare($tip,$mod,$dep,$ubi,$fec,$feca,$fac,$pre,$pro){
			$pre = str_replace(",", "", $pre);
			$data=array($this->tip=>$tip,$this->mod=>$mod,$this->dep=>$dep,$this->ubi=>$ubi,$this->fec=>$fec,$this->feca=>$feca,$this->fac=>$fac,$this->pre=>$pre,$this->pro=>$pro);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizare($id,$tip,$mod,$dep,$ubi,$fec,$feca,$fac,$pre,$pro){
			$pre = str_replace(",", "", $pre);
			$data=array($this->tip=>$tip,$this->mod=>$mod,$this->dep=>$dep,$this->ubi=>$ubi,$this->fec=>$fec,$this->feca=>$feca,$this->fac=>$fac,$this->pre=>$pre,$this->pro=>$pro);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function getEquipos($filter,$usu){
			$this->db->join('proveedores','Numero=pro','inner');
			$this->db->join('equipocategoria','idcat=ubi','left');
			$this->db->where($this->ser,1);
			if($usu<3) $this->db->where($this->dep,$usu);
			$this->db->order_by($this->id,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $cont=0;
			$cont=$result->num_rows();$t='';
			foreach($result->result() as $row):
				$row->num =$cont;
				$row->fec1 = $fec->fecha($row->fec);
				if($row->feca!=0) $row->fec2 = $fec->fecha($row->feca); else $row->fec2='';
				$row->pre1 = '$ '.number_format($row->pre, 2, '.', ',');
				$row->pre = number_format(($row->pre), 2, '.', ','); if($row->pre==0.00){$row->pre="";}
				$row->pro = $row->Razon;
				switch ($row->tipo){
					case '1': $t='[B] ';break;
					case '2': $t='[F] ';break;
					case '3': $t='[O] ';break;
					case '4': $t='[U] ';break;
					case '5': $t='[I] ';break;
					case '6': $t='[BW] ';break;
					case '7': $t='[C] ';break;
					case '8': $t='[CH] ';break;
					case '9': $t='[H2O] ';break;
				}	
				$row->tipo1 = $t.' '.$row->mod;
				if($row->depa==1){$lab='[Lab 1] ';}else{$lab='[Lab 2] ';}
				if($row->nom!=''){
					if($usu==3){$row->nom=$lab.$row->nom;}else{$row->nom=$row->nom;}				
				}
				else{$row->nom="";}
				$cont-=1;
				$data[] = $row;
			endforeach;
			return $data;
		}
		
		function getNumRowsE($filter){
			$this->db->join('proveedores','Numero=pro','inner');
			$this->db->order_by($this->id,'DESC');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function borrare($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getCategorias($where,$usu){
			$this->db->select('idcat,nom as val');			
			//$this->db->join('equipocategoria', 'cat=tipo', 'inner');			
			//$this->db->where($this->cat,$where['actual']);
			if($usu<3) $this->db->where($this->usu,$usu);
			//$where['usuario']
			if($where['cat']!=0) $this->db->where($where); 
			//if($where['cat']!=0 || $where['usu']!=0) $this->db->where($where);
			$result = $this->db->get($this->tablacat);			
	     	$data = array();  
		    foreach($result->result() as $row):
				$data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		function getEquiposSele($filter,$usu,$extra){
			//$this->db->join('proveedores','Numero=pro','inner');
			$this->db->join('equipocategoria','idcat=ubi','inner');
			if($usu<3) $this->db->where($this->dep,$usu);
			//$this->db->order_by($this->id,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $t='';
			$cantidadre=$result->num_rows();
			if($cantidadre>=1){
			foreach($result->result() as $row):
				//$row->fec2 = $fec->fecha($row->fec);
				switch ($row->tipo){
					case '1': $t='[Bomba] ';break;
					case '2': $t='[Filtro] ';break;
					case '3': $t='[Ozono] ';break;
					case '4': $t='[U.V.] ';break;
					case '5': $t='[Intercambiador] ';break;
					case '6': $t='[Blower] ';break;
					case '7': $t='[Calentador] ';break;
					case '8': $t='[Chiller] ';break;
					case '9': $t='[Agua] ';break;
				}	
				//$row->tipo1 = $t.' '.$row->mod;
				if($row->fec!=0){$row->tipo2 = $row->mod." [Ingresó a Sistema]";} else {$row->tipo2 = $row->mod;} 
				if($row->feca!=0){$row->fec3 = $fec->fecha($row->feca);} else {$row->fec3='';}
				if($row->depa==1){$lab='[Lab 1] ';}else{$lab='[Lab 2] ';}
				if($row->nom!=''){if($usu==3){$row->nom1=$t.' '.$lab.$row->nom;}else{$row->nom1=$t.' '.$row->nom;}}else{$row->nom1="";}
				$data[] = $row;
			endforeach;
			}else{
				//$this->db->join('equipocategoria','idcat=ubi','inner');
				if($usu<3) $this->db->where($this->usu,$usu);
				$this->db->where('idcat',$extra);
				//$this->db->order_by($this->id,'DESC');
				//if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
				if($filter['order']!='') $this->db->order_by($filter['order']);
				//Se realiza la consulta con una limitación, en caso de que sea valida
				If($filter['limit']!=0)
					$result = $this->db->get('equipocategoria',$filter['limit'],$filter['offset']);
				else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
					$result = $this->db->get('equipocategoria');
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array();$fec=new Libreria(); $t='';
				$cantidadre=$result->num_rows();
				
				foreach($result->result() as $row):
				//$row->fec2 = $fec->fecha($row->fec);
				switch ($row->cat){
					case '1': $t='[Bomba] ';break;
					case '2': $t='[Filtro] ';break;
					case '3': $t='[Ozono] ';break;
					case '4': $t='[U.V.] ';break;
					case '5': $t='[Intercambiador] ';break;
					case '6': $t='[Blower] ';break;
					case '7': $t='[Calentador] ';break;
					case '8': $t='[Chiller] ';break;
					case '9': $t='[Agua] ';break;
				}	
				//$row->tipo1 = $t.' '.$row->mod;
				//if($row->fec!=0){$row->tipo2 = $row->mod." [Ingresó a Sistema]";} else {$row->tipo2 = $row->mod;} 
				//if($row->feca!=0){$row->fec3 = $fec->fecha($row->feca);} else {$row->fec3='';}
				if($row->usu==1){$lab='[Lab 1] ';}else{$lab='[Lab 2] ';}
				if($row->nom!=''){if($usu==3){$row->nom1=$t.' '.$lab.$row->nom;}else{$row->nom1=$t.' '.$row->nom;}}else{$row->nom1="";}
				$data[] = $row;
			endforeach;
			}
			return $data;
		}
		public function actualizaDet($id,$mod,$feca){
			$data=array($this->mod=>$mod,$this->feca=>$feca);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}	
		public function agregaDet($mod,$dep,$ubi,$feca,$tip){
			$serv=2;$fac=0;$pro=0;//$fec='0000-00-00';
			//$data=array($this->mod=>$mod,$this->dep=>$dep,$this->ubi=>$ubi,$this->feca=>$feca,$this->ser=>$serv,$this->tip=>$tip,$this->fac=>$fac,$this->pro=>$pro,$this->fec=>$fec);			
			$data=array($this->mod=>$mod,$this->dep=>$dep,$this->ubi=>$ubi,$this->feca=>$feca,$this->ser=>$serv,$this->tip=>$tip,$this->fac=>$fac,$this->pro=>$pro);
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		function quitarDet($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getTecnicosGral($filter,$tip){
			$veces=107;
			$mex=1;	$primera=1;
			$data = array();
	 		while($mex<=$veces){
				//if($primera==1) $mex=12;
				//select feca from equipo where ubi=6 group by feca
				$this->db->select('day(feca) as dia,month(feca) as mes,nom,tipo');
				$this->db->join('equipocategoria', 'idcat=ubi', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('ubi =',$mex);
				$this->db->where('feca >',0);
				$this->db->group_by(array("dia","mes","nom","tipo"));
				$result = $this->db->get('equipo');	
				$cantidadre=$result->num_rows();
				
				if($cantidadre>=1){
				$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}	
					$ini=1;	$tot=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$mes=$row->mes;$datosd [$dia]=$row->dia;
						$datosr [$dia]="*";
						if($tip==0){
						switch ($row->tipo){
							case '1': $t='[Bom] ';break;
							case '2': $t='[Fil] ';break;
							case '3': $t='[Ozo] ';break;
							case '4': $t='[UV] ';break;
							case '5': $t='[Int] ';break;
							case '6': $t='[Blo] ';break;
							case '7': $t='[Cal] ';break;
							case '8': $t='[Chi] ';break;
							case '9': $t='[H2O] ';break;
						}
						$row->nom=$t.' '.$row->nom;
						}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$data[] = $row;
				}
				else{
					$this->db->select('nom');
					$this->db->where('idcat =',$mex);
					$this->db->where('cat =',$tip);
					$result = $this->db->get('equipocategoria');
						foreach ($result->result() as $row):
					 		$data[] = $row;
						endforeach;	
				}
				$mex+=1;
			}
			return $data;
		}
		function getNumRowsTGral($filter){
			return 31; // $result->num_rows();
		}
    }
    
?>