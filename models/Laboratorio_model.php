<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Laboratorio_model extends CI_Model {        
        public $id="idfis";public $tip="tipo";	
		public $tablatipos="fisiologia";
		
		public $idval="idval";public $idfv="idfis";public $val="val";public $des="des";public $nom="nom";
		public $tablaval="fisiologiavalores";
		
		public $idaf="idaf";public $fec="fec";public $pil="pila";public $plf="plf";public $org="org";
		public $bra="bra";public $obsb="obsbra";public $inte="inte";public $exo="exo";public $obse="obsexo";
		public $hep1="hep1";public $hep2="hep2";public $hep3="hep3";public $obs="obs";public $cri="critica";
		public $tablaana="fisiologiaanalisis";
		
		public $fech="fech";public $nrdad="nrdad";public $nadr="nadr";public $estr="estr";
		public $tablahis="analisisresdiadepto";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		//TIPOS
		function getTipos($filter){
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablatipos,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablatipos);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $cont=0;
			$cont=$result->num_rows();
			foreach($result->result() as $row):
				$data[] = $row;
			endforeach;
			return $data;
		}
		function getNumRowsT($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablatipos);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function agregare($tip){
			$data=array($this->tip=>$tip);			
			$this->db->insert($this->tablatipos,$data);
			return $this->db->insert_id();
		}
		public function actualizare($id,$tip){
			$data=array($this->tip=>$tip);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablatipos,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function borrare($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablatipos);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		//VALORES
		function getValores($filter){
			$this->db->order_by($this->val);
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaval,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaval);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $cont=0;
			$cont=$result->num_rows();
			foreach($result->result() as $row):
				$data[] = $row;
			endforeach;
			return $data;
		}
		function getNumRowsV($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaval);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function actualizaDet($id,$val,$des,$nom){
			$data=array($this->val=>$val,$this->des=>$des,$this->nom=>$nom);
			$this->db->where($this->idval,$id);
			$this->db->update($this->tablaval,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}	
		public function agregaDet($val,$des,$fis,$nom){
			$data=array($this->val=>$val,$this->des=>$des,$this->idfv=>$fis,$this->nom=>$nom);			
			$this->db->insert($this->tablaval,$data);
			return $this->db->insert_id();
		}
		function quitarDet($id){
			$this->db->where($this->idval,$id);
			$this->db->delete($this->tablaval);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		//VALORES DE LOS COMBOS DE TIPOS PARA ANALIZAR
		public function verBraval(){
			$this->db->where('idfis =',1);
			$query=$this->db->get($this->tablaval);
			return $query->result();			
		}
		public function verIntval(){
			$this->db->where('idfis =',2);
			$query=$this->db->get($this->tablaval);
			return $query->result();			
		}
		public function verExoval(){
			$this->db->where('idfis =',3);
			$query=$this->db->get($this->tablaval);
			return $query->result();			
		}
		public function verHepval(){
			$this->db->where('idfis =',4);
			$this->db->order_by('val');
			$query=$this->db->get($this->tablaval);
			return $query->result();			
		}		
		//COMBO DE PILAS QUE ESTAN EN PRODCUCCION ACTUAL
		public function verPila(){
			//$this->db->where('Cancelacion =',0);
			$query=$this->db->get('raceways');
			return $query->result();			
		}		
		
		//ANALISIS DIARIO
		public function getFresco($filter){
			//select Nombre,org,(select nom from fisiologiavalores where val=bra and idfis=1)as bra,obsbra,(select nom from fisiologiavalores where val=inte and idfis=2)as inte,(select nom from fisiologiavalores where val=exo and idfis=3)as exo,obsexo,(select nom from fisiologiavalores where val=hep and idfis=4)as hep 
			//from raceways inner join  fisiologiaanalisis on pila=NumPila
			$this->db->select('idaf,pila,fec,org,plf,bra,inte,exo,hep1,hep2,hep3,critica,Nombre,(select nom from fisiologiavalores where val=bra and idfis=1)as bra1,obsbra,(select nom from fisiologiavalores where val=inte and idfis=2)as inte1,(select nom from fisiologiavalores where val=exo and idfis=3)as exo1,obsexo,(select nom from fisiologiavalores where val=hep1 and idfis=4)as hep11,(select nom from fisiologiavalores where val=hep2 and idfis=4)as hep21,(select nom from fisiologiavalores where val=hep3 and idfis=4)as hep31,obs');
			$this->db->join('raceways', 'NumPila=pila','inner');
			$this->db->order_by($this->pil);
			$this->db->order_by($this->org);
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablaana,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
			$result = $this->db->get($this->tablaana);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();/*$fec=new Libreria(); $cont=0;
			$cont=$result->num_rows();*/
			$conta=$result->num_rows();
			if($conta>0){
			$pil='';$plf='';$entro=1;$con=0;$vb=0;$vi=0;$ve=0;$vh=0;$vh1=1;$vh2=1;$tkpi=0;$pila=0;$graldia=0;$gral=0;$he1=0;$he2=0;$he3=0;
			foreach($result->result() as $row):
				
				if($pil!=$row->Nombre){
					//imprimir una linea de separacion
					if($con>0){
						$this->db->select('MAX(Numero)');
						$result = $this->db->get('clientes');
						foreach($result->result() as $rowt):
							$rowt->Nombre='KPI';$rowt->plf1='* * *';$rowt->org='* * *';if($vb==0) $rowt->bra1='XX';elseif($vb>0) $rowt->bra1='';else $rowt->bra1='XXX';//$rowt->bra1=$vb;
							if($vi==0) $rowt->inte1='XX';elseif($vi>0) $rowt->inte1='';else $rowt->inte1='XXX';//$rowt->inte1=$vi;
							if($ve==0) $rowt->exo1='XX';elseif($ve>0) $rowt->exo1='';else $rowt->exo1='XXX';//$rowt->exo1=$ve;
							if($vh==0) $rowt->hep11='XX';elseif($vh>0) $rowt->hep11='';else $rowt->hep11='XXX';//$rowt->hep1=$vh;
							if($vh1==0) $rowt->hep21='XX';elseif($vh1>0) $rowt->hep21='';else $rowt->hep21='XXX';//$rowt->hep1=$vh;
							if($vh2==0) $rowt->hep31='XX';elseif($vh2>0) $rowt->hep31='';else $rowt->hep31='XXX';//$rowt->hep1=$vh;
							$tkpi=$vb+$vi+$ve+$vh+$vh1+$vh2;$graldia+=$tkpi;
							if($tkpi==0) $rowt->kpi1='XX';elseif($tkpi>0) $rowt->kpi1='';else $rowt->kpi1='XXX';
							$rowt->obs='* * *';$vb=0;$vi=0;$ve=0;$vh=0;$vh1=1;$vh2=1;$tkpi=0;							
							$data[] = $rowt;
						endforeach;
						$this->db->select('MAX(Numero)');
						$result = $this->db->get('clientes');
						foreach($result->result() as $rowt):
							//obtener organismos analizados
							$this->db->select('count(*) as ani');	
							$this->db->where('pila',$pila);	
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								$ani=$rowti->ani;
							endforeach;	
							$rowt->Nombre='Res';$rowt->plf1='* * *';$rowt->org='* * *';
							$this->db->select('count(*) as bra1');	
							$this->db->where('bra',1);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								if($rowti->bra1/$ani*100 >= 50)	$rowt->bra1='X-'.number_format($rowti->bra1/$ani*100, 1, '.', ',').'%';
								else $rowt->bra1='';
							endforeach;
							$this->db->select('count(*) as inte1');	
							$this->db->where('inte',2);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								if($rowti->inte1/$ani*100 >= 50)	$rowt->inte1='X-'.number_format($rowti->inte1/$ani*100, 1, '.', ',').'%';
								else $rowt->inte1='';
							endforeach;
							$this->db->select('count(*) as exo1');	
							$this->db->where('exo',1);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								if($rowti->exo1/$ani*100 >= 50)	$rowt->exo1='X-'.number_format($rowti->exo1/$ani*100, 1, '.', ',').'%';
								else $rowt->exo1='';
							endforeach;
							$this->db->select('count(*) as hep11');	
							$this->db->where('hep1',3);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);		
							$gral=0;		
							foreach($resulti->result() as $rowti):
								if($rowti->hep11/$ani*100 >= 30){$rowt->hep11='G1-2: '.number_format($rowti->hep11/$ani*100, 1, '.', ',').'%';
									$gral=1;$he1=$rowti->hep11/$ani*100;
								}
								else $rowt->hep11='';
							endforeach;
							$this->db->select('count(*) as hep21');	
							$this->db->where('hep2',3);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								
								if($rowti->hep21/$ani*100 >= 30){$rowt->hep21='G1-2: '.number_format($rowti->hep21/$ani*100, 1, '.', ',').'%';
									$gral+=1;$he2=$rowti->hep21/$ani*100;
								}
								else $rowt->hep21='';
							endforeach;
							$this->db->select('count(*) as hep31');	
							$this->db->where('hep3',3);	$this->db->where('pila',$pila);		
							if($filter['where']!='') $this->db->where($filter['where']);
							$resulti = $this->db->get($this->tablaana);				
							foreach($resulti->result() as $rowti):
								
								if($rowti->hep31/$ani*100 >= 30){$rowt->hep31='G1-2: '.number_format($rowti->hep31/$ani*100, 1, '.', ',').'%';
									$gral+=1;	$he3=$rowti->hep31/$ani*100;				
								}
								else $rowt->hep31='';
							endforeach;	
							$rowt->kpi1='';
							if($rowt->bra1!='' || $rowt->inte1!=''|| $rowt->exo1!='' || $rowt->hep11!='' || $rowt->hep21!=''|| $rowt->hep31!='') {																	
								if($gral==3) $rowt->kpi1='Gral: '.number_format(($he1+$he2+$he3)/$gral, 1, '.', ',').'%'; 
								elseif($gral==2) $rowt->kpi1='Gral: '.number_format(($he1+$he2)/$gral, 1, '.', ',').'%'; 
									elseif($gral==1)  $rowt->kpi1='Gral: '.number_format(($he1), 1, '.', ',').'%';		
								$rowt->obs='LINEA DE PREVENCION'.'<br><tr><td colspan=11</td>*</tr>';
								$he1=0;$he2=0;$he3=0;
								$data[] = $rowt;
							}
							else{ $rowt->obs='* * *'.'<br><tr><td colspan=11</td>*</tr>';}	
						endforeach;
						$con=0;
					}
				$pil=$row->Nombre;$plf=$row->plf;$row->plf1=$row->plf; $entro=1;}else{$row->Nombre='';$entro=0;$con=1;}
				$pila=$row->pila;
				if($entro==0)if($plf!=$row->plf){ $plf=$row->plf;$row->plf1=$row->plf;}else{	$row->plf1='';}
				$cont=1;$valkpi=0;
				while($cont<=6){
					$fres='';$valkpin=0;
					//if($row->hep2!=NULL) {	
					switch ($cont){
						case '1': $fres=$row->bra1;break;
						case '2': $fres=$row->inte1;break;
						case '3': $fres=$row->exo1;break;
						case '4': $fres=$row->hep11;break;
						case '5': $fres=$row->hep21;break;
						case '6': $fres=$row->hep31;break;
					}
					if($fres!='SR'){
					if(($fres=="OK") or ($fres=="X") or ($fres=="G0") or ($fres=="G1")) $valkpin=1;
					elseif(($fres=="XX") or ($fres=="50%") or ($fres=="G1-2") or ($fres=="G2")) $valkpin=0;
					else $valkpin=-1;
					switch ($cont){
						case '1': $vb+=$valkpin;break;
						case '2': $vi+=$valkpin;break;
						case '3': $ve+=$valkpin;break;
						case '4': $vh+=$valkpin;break;
						case '5': $vh1+=$valkpin;break;
						case '6': $vh2+=$valkpin;break;
					}
					$valkpi+=$valkpin;
					}
					$cont+=1;
				}
				//$row->hep1 = $row->hep1;
				if($row->hep2==0) $row->hep21=''; 
				if($row->hep3==0) $row->hep31='';
				//if($row->hep3!=0)$row->hep31 = $row->hep3; else $row->hep31='';
				$row->inte1 = $row->inte1;
				$row->bra1 = $row->bra1;if($row->obsbra!='') $row->bra1 = $row->bra1." [".$row->obsbra."]";
				$row->exo1 = $row->exo1;if($row->obsexo!='') $row->exo1 = $row->exo1." [".$row->obsexo."]";
				if($row->obs=='') $row->obs='';
				$row->kpi1=$valkpi;
				if($valkpi==0) $row->kpi1='XX';
				elseif($valkpi>0) $row->kpi1='';
				else $row->kpi1='XXX';
				$data[] = $row;		
			endforeach;
			$this->db->select('MAX(Numero)');
			$result = $this->db->get('clientes');
			foreach($result->result() as $rowt):
				$rowt->Nombre='KPI';$rowt->plf1='* * *';$rowt->org='* * *';
				if($vb==0) $rowt->bra1='XX';elseif($vb>0) $rowt->bra1='';else $rowt->bra1='XXX';//$rowt->bra1=$vb;
				if($vi==0) $rowt->inte1='XX';elseif($vi>0) $rowt->inte1='';else $rowt->inte1='XXX';//$rowt->inte1=$vi;
				if($ve==0) $rowt->exo1='XX';elseif($ve>0) $rowt->exo1='';else $rowt->exo1='XXX';//$rowt->exo1=$ve;
				if($vh==0) $rowt->hep11='XX';elseif($vh>0) $rowt->hep11='';else $rowt->hep11='XXX';//$rowt->hep1=$vh;
				if($vh1==0) $rowt->hep21='XX';elseif($vh1>0) $rowt->hep21='';else $rowt->hep21='XXX';//$rowt->hep1=$vh;
				if($vh2==0) $rowt->hep31='XX';elseif($vh2>0) $rowt->hep31='';else $rowt->hep31='XXX';//$rowt->hep1=$vh;
				$tkpi=$vb+$vi+$ve+$vh+$vh1+$vh2;$graldia+=$tkpi;
				if($tkpi==0) $rowt->kpi1='XX';elseif($tkpi>0) $rowt->kpi1='';else $rowt->kpi1='XXX';
				$rowt->obs='* * *';	$rowt->totkpip=$graldia;			
				$data[] = $rowt;
			endforeach;
			$con=0;
			$this->db->select('MAX(Numero)');
			$result = $this->db->get('clientes');
			foreach($result->result() as $rowt):
				//obtener organismos analizados
				$this->db->select('count(*) as ani');	
				$this->db->where('pila',$pila);	
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);				
				foreach($resulti->result() as $rowti):
					$ani=$rowti->ani;
				endforeach;
				$rowt->Nombre='Res';$rowt->plf1='* * *';$rowt->org='* * *';
				$this->db->select('count(*) as bra1');	
				$this->db->where('bra',1);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);				
				foreach($resulti->result() as $rowti):
					if($rowti->bra1/$ani*100 >= 50)	$rowt->bra1='X-'.number_format($rowti->bra1/$ani*100, 1, '.', ',').'%';
					else $rowt->bra1='';
				endforeach;
				$this->db->select('count(*) as inte1');	
				$this->db->where('inte',2);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);				
				foreach($resulti->result() as $rowti):
					if($rowti->inte1/$ani*100 >= 50)	$rowt->inte1='X-'.number_format($rowti->inte1/$ani*100, 1, '.', ',').'%';
					else $rowt->inte1='';
				endforeach;
				$this->db->select('count(*) as exo1');	
				$this->db->where('exo',1);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);				
				foreach($resulti->result() as $rowti):
					if($rowti->exo1/$ani*100 >= 50)	$rowt->exo1='X-'.number_format($rowti->exo1/$ani*100, 1, '.', ',').'%';
					else $rowt->exo1='';
				endforeach;
				$this->db->select('count(*) as hep11');	
				$this->db->where('hep1',3);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);	$he1=0;	
						
				foreach($resulti->result() as $rowti):
					
					if($rowti->hep11/$ani*100 >= 30){$rowt->hep11='G1-2: '.number_format($rowti->hep11/$ani*100, 1, '.', ',').'%';
						$gral=1;	$he1=$rowti->hep11/$ani*100;
					}
					else $rowt->hep11='';
				endforeach;
				$this->db->select('count(*) as hep21');	
				$this->db->where('hep2',3);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);$he2=0;				
				foreach($resulti->result() as $rowti):
					
					if($rowti->hep21/$ani*100 >= 30){$rowt->hep21='G1-2: '.number_format($rowti->hep21/$ani*100, 1, '.', ',').'%';
						$gral+=1;$he2=$rowti->hep21/$ani*100;
					}
					else $rowt->hep21='';
				endforeach;
				$this->db->select('count(*) as hep31');	
				$this->db->where('hep3',3);	$this->db->where('pila',$pila);		
				if($filter['where']!='') $this->db->where($filter['where']);
				$resulti = $this->db->get($this->tablaana);$he3=0;				
				foreach($resulti->result() as $rowti):
					
					if($rowti->hep31/$ani*100 >= 30){$rowt->hep31='G1-2: '.number_format($rowti->hep31/$ani*100, 1, '.', ',').'%';
						$gral+=1;	$he3=$rowti->hep31/$ani*100;				
					}
					else $rowt->hep31='';
				endforeach;		
				$rowt->kpi1=$graldia;
				if($rowt->bra1!='' || $rowt->inte1!=''|| $rowt->exo1!='' || $rowt->hep11!='' || $rowt->hep21!=''|| $rowt->hep31!='') {
					if($gral==3) $rowt->kpi1='Gral: '.number_format(($he1+$he2+$he3)/$gral, 1, '.', ',').'%'; 
					elseif($gral==2) $rowt->kpi1='Gral: '.number_format(($he1+$he2)/$gral, 1, '.', ',').'%'; 
						elseif($gral==1)  $rowt->kpi1='Gral: '.number_format(($he1), 1, '.', ',').'%';	
							
					$rowt->obs='LINEA DE PREVENCION';
					//$rowt->kpi1=$he1.'-'.$he2.'-'.$he3;
					$data[] = $rowt;
				}
				else{ $rowt->obs='* * *'.'<br><tr><td colspan=11</td>*</tr>';}
				
			endforeach;
			}
			return $data;
		}
		function getNumRowsF($filter){
			//$this->db->select('idaf,pila,fec,org,plf,bra,inte,exo,hep,Nombre,(select nom from fisiologiavalores where val=bra and idfis=1)as bra1,obsbra,(select nom from fisiologiavalores where val=inte and idfis=2)as inte1,(select nom from fisiologiavalores where val=exo and idfis=3)as exo1,obsexo,(select nom from fisiologiavalores where val=hep and idfis=4)as hep1,obs');
			$this->db->join('raceways', 'NumPila=pila','inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaana);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function agregara($fec,$pl,$pil,$org,$bra,$obsb,$inte,$exo,$obse,$hep1,$hep2,$hep3,$obs){
			$data=array($this->fec=>$fec,$this->plf=>$pl,$this->pil=>$pil,$this->org=>$org,$this->bra=>$bra,$this->obsb=>$obsb,$this->inte=>$inte,$this->exo=>$exo,$this->obse=>$obse,$this->hep1=>$hep1,$this->hep2=>$hep2,$this->hep3=>$hep3,$this->obs=>$obs);			
			$this->db->insert($this->tablaana,$data);
			return $this->db->insert_id();
		}
		public function actualizara($id,$fec,$pl,$pil,$org,$bra,$obsb,$inte,$exo,$obse,$hep1,$hep2,$hep3,$obs){
			$data=array($this->fec=>$fec,$this->plf=>$pl,$this->pil=>$pil,$this->org=>$org,$this->bra=>$bra,$this->obsb=>$obsb,$this->inte=>$inte,$this->exo=>$exo,$this->obse=>$obse,$this->hep1=>$hep1,$this->hep2=>$hep2,$this->hep3=>$hep3,$this->obs=>$obs);
			$this->db->where($this->idaf,$id);
			$this->db->update($this->tablaana,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function borrara($id){
			$this->db->where($this->idaf,$id);
			$this->db->delete($this->tablaana);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function datoscriticos($pila,$feca,$valor,$solodep){						
				//totales remisiones por ciclo
				if($solodep==0){
				$this->db->where($this->pil,$pila);
				$this->db->where($this->fec,$feca);
				$result = $this->db->get($this->tablaana);
				foreach ($result->result() as $row):
						$reg=$row->idaf;
						$data=array($this->cri=>$valor);
						$this->db->where($this->idaf,$reg);
						$this->db->update($this->tablaana,$data); 
				endforeach;	
					
				}		
			return;
		}
		
		
		
		
		
		
		
		function getEquiposSele($filter,$usu,$extra){
			//$this->db->join('proveedores','Numero=pro','inner');
			$this->db->join('equipocategoria','idcat=ubi','inner');
			if($usu<3) $this->db->where($this->dep,$usu);
			//$this->db->order_by($this->id,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $t='';
			$cantidadre=$result->num_rows();
			if($cantidadre>=1){
			foreach($result->result() as $row):
				//$row->fec2 = $fec->fecha($row->fec);
				switch ($row->tipo){
					case '1': $t='[Bomba] ';break;
					case '2': $t='[Filtro] ';break;
					case '3': $t='[Ozono] ';break;
					case '4': $t='[U.V.] ';break;
					case '5': $t='[Intercambiador] ';break;
					case '6': $t='[Blower] ';break;
					case '7': $t='[Calentador] ';break;
					case '8': $t='[Chiller] ';break;
				}	
				//$row->tipo1 = $t.' '.$row->mod;
				if($row->fec!=0){$row->tipo2 = $row->mod." [Ingresó a Sistema]";} else {$row->tipo2 = $row->mod;} 
				if($row->feca!=0){$row->fec3 = $fec->fecha($row->feca);} else {$row->fec3='';}
				if($row->depa==1){$lab='[Lab 1] ';}else{$lab='[Lab 2] ';}
				if($row->nom!=''){if($usu==3){$row->nom1=$t.' '.$lab.$row->nom;}else{$row->nom1=$t.' '.$row->nom;}}else{$row->nom1="";}
				$data[] = $row;
			endforeach;
			}else{
				//$this->db->join('equipocategoria','idcat=ubi','inner');
				if($usu<3) $this->db->where($this->usu,$usu);
				$this->db->where('idcat',$extra);
				//$this->db->order_by($this->id,'DESC');
				//if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
				if($filter['order']!='') $this->db->order_by($filter['order']);
				//Se realiza la consulta con una limitación, en caso de que sea valida
				If($filter['limit']!=0)
					$result = $this->db->get('equipocategoria',$filter['limit'],$filter['offset']);
				else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
					$result = $this->db->get('equipocategoria');
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array();$fec=new Libreria(); $t='';
				$cantidadre=$result->num_rows();
				
				foreach($result->result() as $row):
				//$row->fec2 = $fec->fecha($row->fec);
				switch ($row->cat){
					case '1': $t='[Bomba] ';break;
					case '2': $t='[Filtro] ';break;
					case '3': $t='[Ozono] ';break;
					case '4': $t='[U.V.] ';break;
					case '5': $t='[Intercambiador] ';break;
					case '6': $t='[Blower] ';break;
					case '7': $t='[Calentador] ';break;
					case '8': $t='[Chiller] ';break;
				}	
				//$row->tipo1 = $t.' '.$row->mod;
				//if($row->fec!=0){$row->tipo2 = $row->mod." [Ingresó a Sistema]";} else {$row->tipo2 = $row->mod;} 
				//if($row->feca!=0){$row->fec3 = $fec->fecha($row->feca);} else {$row->fec3='';}
				if($row->usu==1){$lab='[Lab 1] ';}else{$lab='[Lab 2] ';}
				if($row->nom!=''){if($usu==3){$row->nom1=$t.' '.$lab.$row->nom;}else{$row->nom1=$t.' '.$row->nom;}}else{$row->nom1="";}
				$data[] = $row;
			endforeach;
			}
			return $data;
		}
		
		function getTecnicosGral($filter,$tip){
			$veces=101;
			$mex=1;	$primera=1;
			$data = array();
	 		while($mex<=$veces){
				//if($primera==1) $mex=12;
				//select feca from equipo where ubi=6 group by feca
				$this->db->select('day(feca) as dia,month(feca) as mes,nom,tipo');
				$this->db->join('equipocategoria', 'idcat=ubi', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('ubi =',$mex);
				$this->db->where('feca >',0);
				$this->db->group_by(array("feca"));
				$result = $this->db->get('equipo');	
				$cantidadre=$result->num_rows();
				
				if($cantidadre>=1){
				$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}	
					$ini=1;	$tot=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$mes=$row->mes;$datosd [$dia]=$row->dia;
						$datosr [$dia]="*";
						if($tip==0){
						switch ($row->tipo){
							case '1': $t='[Bom] ';break;
							case '2': $t='[Fil] ';break;
							case '3': $t='[Ozo] ';break;
							case '4': $t='[UV] ';break;
							case '5': $t='[Int] ';break;
							case '6': $t='[Blo] ';break;
							case '7': $t='[Cal] ';break;
							case '8': $t='[Chi] ';break;
						}
						$row->nom=$t.' '.$row->nom;
						}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$data[] = $row;
				}
				else{
					$this->db->select('nom');
					$this->db->where('idcat =',$mex);
					$this->db->where('cat =',$tip);
					$result = $this->db->get('equipocategoria');
						foreach ($result->result() as $row):
					 		$data[] = $row;
						endforeach;	
				}
				$mex+=1;
			}
			return $data;
		}
		function getNumRowsTGral($filter){
			return 31; // $result->num_rows();
		}
		function historial($nadr,$estr){
			$hoy=date("y-m-d H:i:s");
			$this->db->select('DATEDIFF( CURDATE(),max(fech) ) AS uno');
			$result = $this->db->get($this->tablahis);
			foreach ($result->result() as $row):
				$dia=$row->uno;
			endforeach; 
			if($dia>0){
				//$this->db->select('NumPila,CanSie,ActPL');
				//$this->db->where('Cancelacion =', 0);
				//$result = $this->db->get($this->tablaras);
				//foreach ($result->result() as $row):
					//$pila=$row->NumPila;
					$data=array($this->fech=>$hoy,$this->nadr=>$nadr,$this->estr=>$estr);			
					$this->db->insert($this->tablahis,$data);
					$this->db->insert_id();
				//endforeach;
				return 2;
			}else{ return 1; }
		}	
		
		
    }
    
?>