<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Facturado_model extends CI_Model {
        public $numero="Numero"; public $razon="Razon"; public $saldo="Saldo"; public $fecud="FecUD"; public $impud="ImpUD"; public $obsud="ObsUD";
		public $si="SI"; public $zona="Zona";
		public $tabla="r17";
		public $tablasie="siembras";
		public $id="reg"; public $sec="seccion"; public $cli="cliente"; public $cic="ciclo"; public $ori="lab";	public $est="est"; public $sup="sup";
		public $pls="pls"; public $fs="fs";	public $fc="fc"; public $pp="pp"; public $at="at"; public $camf="camf"; public $sob="sob"; 
		public $biop="biop"; public $camp="camp"; public $bioc="bioc"; public $camc="camc";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db            
        }
		
		function getFacturado($filter,$z){
			if($filter['num']>=2014){
				$this->db->select('Numero,Zona,Razon,sum(CantidadRR) as CantidadRR,(select count(*) from siembras where cliente=numero) as est');
			}else{	
				$this->db->select('Numero,Zona,Razon,sum(CantidadRR) as CantidadRR');
			}
			$this->db->where('Estatus','0');	
			$this->db->where('CantidadRR >','0');
			$this->db->group_by(array("Zona", "Razon"));						
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!=''){ $this->db->order_by($filter['order']);}
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='0'){ $this->db->where($filter['where']); }						
			//Se realiza la consulta con una limitación, en caso de que sea valida
			$data = array(); $cli=""; 
			If($filter['num']!=0){
				if($filter['num']==2008){$result = $this->db->get('r08'); }
				if($filter['num']==2009){$result = $this->db->get('r09'); }
				if($filter['num']==2010){$result = $this->db->get('r10'); }
				if($filter['num']==2011){$result = $this->db->get('r11'); }
				if($filter['num']==2012){$result = $this->db->get('r12'); }
				if($filter['num']==2013){$result = $this->db->get('r13'); }
				if($filter['num']==2014){$result = $this->db->get('r14'); }
				if($filter['num']==2015){$result = $this->db->get('r15'); }
				if($filter['num']==2016){$result = $this->db->get('r16'); }				
				if($filter['num']==2017){$result = $this->db->get('r17'); }
			}else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla); 
			}
			if($z==0){
				$totgranja=0;$cont=0;$cliente=0;	
				foreach ($result->result() as $row):				
					$zona=$row->Zona;
					$cte=$row->Numero;
					if($cliente!=$cte){
						$this->db->select('count(*) as ne');
						$this->db->where('lab','Aquapacific');
						$this->db->where('cliente',$row->Numero);
						$result1 = $this->db->get('siembras');$ne=0;
						foreach ($result1->result() as $row1):
							$ne=$row1->ne;
						endforeach;	
						$cliente=$row->Numero; 
					}
					if($cli!=$zona){
						if($totgranja>0){ $totgranja=0;	
							//aqui se mete el total segun la zona
							$this->db->select('(sum(CantidadRR)) as CantidadRR');			
							$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
							$this->db->where('Estatus =',0);
							$this->db->where('Zona =',$cli);							
			    			if($filter['num']==2008){$resultZ = $this->db->get('r08');}
							if($filter['num']==2009){$resultZ = $this->db->get('r09');}
							if($filter['num']==2010){$resultZ = $this->db->get('r10');}
							if($filter['num']==2011){$resultZ = $this->db->get('r11');}
							if($filter['num']==2012){$resultZ = $this->db->get('r12');}
							if($filter['num']==2013){$resultZ = $this->db->get('r13');}
							if($filter['num']==2014){$resultZ = $this->db->get('r14');}
							if($filter['num']==2015){$resultZ = $this->db->get('r15');}
							if($filter['num']==2016){$resultZ = $this->db->get('r16');}
							if($filter['num']==2017){$resultZ = $this->db->get('r17');}
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->Zona = "Total:";$rowZ->Razon = "";$rowZ->est = "";
								$rowZ->CantidadRR = number_format($rowZ->CantidadRR, 3, '.', ',');
								$data[] = $rowZ;	
							endforeach;	
						}
						$row->Zona=$row->Zona; $cli=$row->Zona; 
					}else{ $row->Zona="";} 
					$totgranja+=$row->CantidadRR;
					$row->tot =$row->CantidadRR; 
					$row->CantidadRR = number_format($row->CantidadRR, 3, '.', ',');
					if($filter['num']>=2014){	
					if($row->est==0) $row->est="";	else $row->est=$ne." de ".$row->est;
					}else{$row->est="";}		
					$data[] = $row;	
					$cont+=1;			
				endforeach;	
				//if($cli!=$zona){
						if($totgranja>0){ $totgranja=0;	
							//aqui se mete el total segun la zona
							$this->db->select('(sum(CantidadRR)) as CantidadRR');			
							$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
							$this->db->where('Estatus =',0);
							$this->db->where('Zona =',$cli);							
			    			if($filter['num']==2008){$resultZ = $this->db->get('r08');}
							if($filter['num']==2009){$resultZ = $this->db->get('r09');}
							if($filter['num']==2010){$resultZ = $this->db->get('r10');}
							if($filter['num']==2011){$resultZ = $this->db->get('r11');}
							if($filter['num']==2012){$resultZ = $this->db->get('r12');}
							if($filter['num']==2013){$resultZ = $this->db->get('r13');}
							if($filter['num']==2014){$resultZ = $this->db->get('r14');}
							if($filter['num']==2015){$resultZ = $this->db->get('r15');}
							if($filter['num']==2016){$resultZ = $this->db->get('r16');}
							if($filter['num']==2017){$resultZ = $this->db->get('r17');}
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->Zona = "Total:";$rowZ->Razon = "";$rowZ->est = "";
								$rowZ->CantidadRR = number_format($rowZ->CantidadRR, 3, '.', ',');
								$data[] = $rowZ;	
							endforeach;	
						}
						//$row->Zona=$row->Zona; $cli=$row->Zona; 
					//}			
			}else{
				//Se forma el arreglo que sera retornado	
				$totgranja=0;$cont=0;$cliente=0;	
				foreach ($result->result() as $row):				
					$zona=$row->Zona;
					$cte=$row->Numero;
					if($cliente!=$cte){
						$this->db->select('count(*) as ne');
						$this->db->where('lab','Aquapacific');
						$this->db->where('cliente',$row->Numero);
						$result1 = $this->db->get('siembras');$ne=0;
						foreach ($result1->result() as $row1):
							$ne=$row1->ne;
						endforeach;	
						$cliente=$row->Numero; 
					}
					if($cli!=$zona){
						if($totgranja>0){ $totgranja=0;	}
						$row->Zona=$row->Zona; $cli=$row->Zona; 
					}else{ $row->Zona="";} 
					$totgranja+=$row->CantidadRR;
					$row->tot =$row->CantidadRR; 
					$row->CantidadRR = number_format($row->CantidadRR, 3, '.', ',');
					if($filter['num']>=2014){	
					if($row->est==0) $row->est="";	else $row->est=$ne." de ".$row->est;
					}else{$row->est="";}		
					$data[] = $row;	
					$cont+=1;			
				endforeach;	
				//se pone el total por ciclo entregado
				$this->db->select('(sum(CantidadRR)) as CantidadRR');			
				$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
				$this->db->where('Estatus =',0);
				if($filter['where']!=0){ $this->db->where($filter['where']);}
			    if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}
				if($filter['num']==2017){$result = $this->db->get('r17');}
				foreach ($result->result() as $row):				
					$row->Zona = "Total:";$row->Razon = "";$row->est = "";
					$row->CantidadRR = number_format($row->CantidadRR, 3, '.', ',');
					$data[] = $row;	
				endforeach;	
			}
		return $data;
		}
		function getNumRows($filter){
			$this->db->select('Numero,Zona,Razon,sum(CantidadRR) as CantidadRR');	
			$this->db->where('Estatus','0');	
			$this->db->where('CantidadRR >','0');
			$this->db->group_by(array("Zona", "Razon"));						
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!=''){
				$this->db->order_by($filter['order']);}
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='0'){ $this->db->where($filter['where']); }						
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		
		
		function getEntregado($filter){
			$this->db->select('FechaR,RemisionR,CantidadRR,PrecioR,Folio');
			//$this->db->from('r12');
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				/*if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}*/
				if($filter['num']==2011){$this->db->join('folios_11', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2012){$this->db->join('folios_12', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2013){$this->db->join('folios_13', 'NumRegR = NRem', 'inner');}				
				if($filter['num']==2014){$this->db->join('folios_14', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2015){$this->db->join('folios_15', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2016){$this->db->join('folios_16', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2017){$this->db->join('folios', 'NumRegR = NRem', 'inner');}
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$this->db->join('folios', 'NumRegR = NRem', 'inner');
			}
			$this->db->where('Estatus !=',2);			
			//$this->db->where('Cancelacion =',0);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!=''){
				$this->db->order_by($filter['order']);	
			}	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
			}
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}
				if($filter['num']==2017){$result = $this->db->get($this->tabla);}
				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			}
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta			
			/*if($filter['num']!=0){
				$this->db->where('Numero',$filter['num']);
			}*/				
			/*If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);*/
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $imp=""; $fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$row->tot =$row->CantidadRR;  
				$row->FechaR1 = $fec->fecha($row->FechaR);
				if($row->CantidadRR*$row->PrecioR>0){$row->totimp =$row->CantidadRR*$row->PrecioR;$row->Importe='$ '.number_format(($row->CantidadRR*$row->PrecioR), 2);}else{$row->Importe="";}
				if($row->PrecioR>0){$row->PrecioR='$ '.number_format($row->PrecioR, 2);}else{$row->PrecioR="";}
				$row->CantidadRR=number_format($row->CantidadRR, 3);
				if($filter['num']<2011){
					$row->Folio="";
				}
				$data[] = $row;	
							
			endforeach;	
			//pone los totales remisionados con el importe
			$this->db->select('sum(CantidadRR) as CantidadRRT,sum(CantidadRR*PrecioR) as ImporteT');
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');
			$this->db->where('Estatus !=',2);
			if($filter['order']!=''){
				$this->db->order_by($filter['order']);	
			}	
			
			$this->db->where($filter['where']); 
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$result2 = $this->db->get('r08');}
				if($filter['num']==2009){$result2 = $this->db->get('r09');}
				if($filter['num']==2010){$result2 = $this->db->get('r10');}
				if($filter['num']==2011){$result2 = $this->db->get('r11');}
				if($filter['num']==2012){$result2 = $this->db->get('r12');}
				if($filter['num']==2013){$result2 = $this->db->get('r13');}
				if($filter['num']==2014){$result2 = $this->db->get('r14');}
				if($filter['num']==2015){$result2 = $this->db->get('r15');}
				if($filter['num']==2016){$result2 = $this->db->get('r16');}
				if($filter['num']==2017){$result2 = $this->db->get($this->tabla);}
			}
			else{ 
				$result2 = $this->db->get($this->tabla);
			}
			
			foreach ($result2->result() as $row2):
				$row2->FechaR1="Total:";$row2->RemisionR="";$row2->Folio="";$row2->PrecioR="";
				$row2->Importe='$ '.number_format($row2->ImporteT, 2);
				$row2->CantidadRR=number_format($row2->CantidadRRT, 3);
				$data[] = $row2;	
							
			endforeach;
			return $data;
		}
		function getNumRowsE($filter){
			$this->db->select('FechaR,RemisionR,CantidadRR,PrecioR,Folio');
			//$this->db->from('r12');
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				/*if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}*/
				if($filter['num']==2011){$this->db->join('folios_11', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2012){$this->db->join('folios_12', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2013){$this->db->join('folios_13', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2014){$this->db->join('folios_14', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2015){$this->db->join('folios_15', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2016){$this->db->join('folios_16', 'NumRegR = NRem', 'inner');}
				if($filter['num']==2017){$this->db->join('folios', 'NumRegR = NRem', 'inner');}				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$this->db->join('folios', 'NumRegR = NRem', 'inner');
			}
			$this->db->where('Estatus !=',2);			
			//$this->db->where('Cancelacion =',0);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados				
			/*if($filter['num']!=0)
				$this->db->where('Numero',$filter['num']);*/
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}
				if($filter['num']==2017){$result = $this->db->get($this->tabla);}
				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			}
			//$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function gralzona($filter){			
			//select Zona,format(sum(CantidadRR),3)  from ".$ciclo." inner join clientes on NumCliR=Numero where Estatus=0 group by Zona order by Zona,FechaR
			$this->db->select('Zona,(sum(CantidadRR)) as CantidadRR');			
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
			$this->db->where('Estatus =',0);
			$this->db->group_by('Zona');
			$this->db->order_by('CantidadRR','DESC');			
			if($filter['order']!=''){
				$this->db->order_by($filter['order']);	
			}	
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}
				if($filter['num']==2017){$result = $this->db->get($this->tabla);}
				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			}			
			$tot=0;$cont=0;
			foreach ($result->result() as $row):
					$tot+=$row->CantidadRR; $cont+=1;					
			endforeach;			
			$data = array();// $cli="";
			//Se forma el arreglo que sera retornado
			$cont=0;							
			foreach ($result->result() as $row):
				//row->tot =	$row->CantidadRR; //este es el total
				$row->por = number_format((($row->CantidadRR/$tot)*100), 2, '.', ',')."%";
				$row->cantidadrrg = $row->CantidadRR;
				$row->CantidadRR = number_format($row->CantidadRR, 3, '.', ',');
				$data[] = $row;	
			endforeach;	
			$this->db->select('(sum(CantidadRR)) as tot');
			$this->db->where('Estatus =',0);
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$resultz = $this->db->get('r08');}
				if($filter['num']==2009){$resultz = $this->db->get('r09');}
				if($filter['num']==2010){$resultz = $this->db->get('r10');}
				if($filter['num']==2011){$resultz = $this->db->get('r11');}
				if($filter['num']==2012){$resultz = $this->db->get('r12');}
				if($filter['num']==2013){$resultz = $this->db->get('r13');}
				if($filter['num']==2014){$resultz = $this->db->get('r14');}
				if($filter['num']==2015){$resultz = $this->db->get('r15');}
				if($filter['num']==2016){$resultz = $this->db->get('r16');}
				if($filter['num']==2017){$resultz = $this->db->get($this->tabla);}
				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$resultz = $this->db->get($this->tabla);
			}
			foreach ($resultz->result() as $rowz):	
				$rowz->Zona ="Total: ";
				$rowz->CantidadRR = number_format($rowz->tot, 3, '.', ',');				
				$rowz->por = "";
				$data[] = $rowz;
			endforeach;		
			return $data;												
		}
		function getNumRowsZ($filter){
			$this->db->select('Zona,(sum(CantidadRR)) as CantidadRR');			
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
			$this->db->where('Estatus =',0);
			$this->db->group_by('Zona');
			//$this->db->order_by('Zona','FechaR');			
			if($filter['order']!=''){
				$this->db->order_by($filter['order']);	
			}	
			If($filter['num']!=0){
				//$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
				if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}
				if($filter['num']==2017){$result = $this->db->get($this->tabla);}
				
			}
			else{ //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			}	
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function entregado($ncli=0,$ciclo=0){
			//$ncli=335;
			$this->db->select('FechaR,RemisionR,CantidadRR,PrecioR,Folio');
			//$this->db->from('r12');
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');
			$this->db->join('folios', 'NumRegR = NRem', 'inner');
			$this->db->where('Estatus !=',2);			
			$this->db->where('Cancelacion =',0);
			$this->db->where('Numero',$ncli);
			
			$query = $this->db->get($this->tabla);			
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
		function zonas(){									
			$this->db->select('Zona');
			$this->db->where('Numero >',1);
			//$this->db->where('Saldo >',0);
			//$this->db->where('SI',0);
			//$this->db->where('Zona !=','Varias');	
			$this->db->group_by('Zona');
			//$this->db->order_by('SUM(Saldo)','desc');					
			$query = $this->db->get('clientes');			
			return $query->result();
		}
		function getElements($where){
			$this->db->select('zona,zona as val');			
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
			$this->db->where('Estatus =',0);
			$this->db->group_by('Zona');			
			if($where['actual']==2008){$result = $this->db->get('r08');}
			if($where['actual']==2009){$result = $this->db->get('r09');}
			if($where['actual']==2010){$result = $this->db->get('r10');}
			if($where['actual']==2011){$result = $this->db->get('r11');}
			if($where['actual']==2012){$result = $this->db->get('r12');}
			if($where['actual']==2013){$result = $this->db->get('r13');}
			if($where['actual']==2014){$result = $this->db->get('r14');}
			if($where['actual']==2015){$result = $this->db->get('r15');}
			if($where['actual']==2016){$result = $this->db->get('r16');}
			if($where['actual']==2017){$result = $this->db->get($this->tabla);}			
	     	$data = array();  
		    foreach($result->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}	
		function verSeccion($where){									
			$this->db->select('seccion,seccion as val');
			$this->db->where('cliente =',$where['actual']);
			$this->db->group_by('seccion');
			$result = $this->db->get('siembras');			
			$data = array();  
		    foreach($result->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;
		}	
		function clientes(){									
			$this->db->select('Numero,Razon');
			$query = $this->db->get('clientes');			
			return $query->result();
		}
		
		function getSiembras($filter){
			//"SELECT ciclo FROM siembras WHERE seccion='".$sec."' and cliente='".$txtclaveR."' group by ciclo";	
			$this->db->select('seccion,ciclo');
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
			}	
			$this->db->group_by('seccion');
			//$this->db->order_by('seccion');
			$resultc = $this->db->get($this->tablasie);
			$data = array(); $cic =''; //$entro=0;
			foreach ($resultc->result() as $row1):
				//$ciclo=$row1->ciclo;	
				$sec=$row1->seccion;$seci=$row1->seccion;
				$pls1=0;  $sup1=0; $tota=0;
				//"SELECT reg,ciclo,est,sup,pls,fs,fc,DATEDIFF(fc, fs) as dc,pp,at,camf,sob,biop,camp,bioc,camc,lab 
				//FROM siembras 
				//WHERE seccion='".$sec."' and cliente='".$txtclaveR."'  and ciclo='".$cic."'
				// group by ciclo,lab,est order by ciclo,lab,cast(est as UNSIGNED),reg";
				//$this->db->group_by('ciclo');
				//$this->db->group_by('lab');
				//$this->db->order_by(cast('est as UNSIGNED'));
				$this->db->select('seccion,reg,ciclo,est,sup,pls,fs,fc,DATEDIFF( fc,fs ) AS dc,pp,at,camf,sob,biop,camp,bioc,camc,lab',NULL,FALSE);
				$this->db->where('seccion =',$sec);
				if($filter['where']!=''){
					$this->db->where($filter['where']); 
				}
				//$this->db->order_by('seccion');
				$this->db->order_by('ciclo');		
				$this->db->order_by('lab');
				$this->db->order_by('reg');
				$result = $this->db->get($this->tablasie);
				$seci =''; $lab =''; 
				foreach ($result->result() as $row):
					if($seci!=$row->seccion){$seci =$row->seccion;$row->seccion1 =$row->seccion; }else{$row->seccion1 ='';}
					if($cic!=$row->ciclo){
						if($tota>=1){
							$tota=0; $entro=0;
							//total por ciclo
							//"SELECT ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF(fc, fs)) as dc,avg(pp) as pp,
							//sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,
							//avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2 
							//FROM siembras 
							//WHERE fc!='' and seccion='".$sec."' and cliente='".$txtclaveR."' and ciclo='".$cic."' 
							//group by ciclo";
							$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
							$this->db->where('ciclo =',$cic);
							$this->db->where('fc !=','');
							$this->db->where('seccion =',$seci);
							if($filter['where']!=''){$this->db->where($filter['where']);}
							
							$this->db->group_by('ciclo');
							$resultp = $this->db->get($this->tablasie);
							$cantidadSI=$resultp->num_rows();
							if($cantidadSI>0){ $entro=1;
								foreach ($resultp->result() as $rowp):
									$rowp->seccion1 ='';$rowp->ciclo1 ='';$rowp->lab1 ='Promedio';$rowp->est1 ='';
									//$sup=$rowp->sup; $rowp->sup1 =number_format($sup,2);
									$sup=$rowp->sup; if($sup!=0){$rowp->sup1 =number_format($sup,2);} else {$rowp->sup1='';}
									$pls=0;$pls=$rowp->pls; $rowp->pls1= number_format($pls/1000, 3);
									//$ciclo=$rowp->ciclo;
									$dc=$rowp->dc;
									//$lab=$rowp->lab;
									$rowp->fs1 = '';$rowp->fc1 = '';$rowp->dc1 = '';
									if($dc!=""){
										$sc=$dc/7; if($sc!=0){$rowp->sc1=number_format($sc);} else {$rowp->sc1='';}
										$pp=$rowp->pp; if($pp!=0){$rowp->pp1=number_format($pp,2);}else{$rowp->pp1 = '';}
										$isp=($pp/$dc)*7; if($isp!=0){$rowp->isp1=number_format($isp,2);} else { $rowp->isp1 = '';}
										$at=$rowp->at; if($at!=0){$rowp->at1=number_format($at);} else { $rowp->at1 = '';}
										$aha=$rowp->aha; if($aha!=0){$rowp->aha1=number_format($aha);} else { $rowp->aha1 = '';}
										$cami=$pls/($sup*10000); if($cami!=0){$rowp->cami1=number_format($cami,2);} else { $rowp->cami1 = '';}
										$camf=$rowp->camf; if($camf!=0){$rowp->camf1=number_format($camf,2);} else { $rowp->camf1 = '';}
										$sob=$rowp->sob; if($sob!=0){$rowp->sob1=number_format($sob,2);} else { $rowp->sob1 = '';}
										$biop=$rowp->biop; if($biop!=0){$rowp->biop1=number_format($biop);} else { $rowp->biop1 = '';}
										$pha=$biop/$sup; if($pha!=0){$rowp->pha1=number_format($pha);} else { $rowp->pha1 = '';}
										$camp=$rowp->camp; if($camp!=0){$rowp->camp1=number_format($camp,2);} else { $rowp->camp1 = '';}
										$bioc=$rowp->bioc; if($bioc!=0){$rowp->bioc1=number_format($bioc);} else { $rowp->bioc1 = '';}
										$cha=$bioc/$sup; if($cha!=0){$rowp->cha1=number_format($cha);} else { $rowp->cha1 = '';}
										$camc=$rowp->camc; if($camc!=0){$rowp->camc1=number_format($camc,2);} else { $rowp->camc1 = '';}
										$otc2=$rowp->camfin2; if($otc2!=0){$rowp->otc1=number_format($otc2);} else { $rowp->otc1 = '';}
										$camfin=$camp+$camc; if($camfin!=0){$rowp->camfin1=number_format($camfin,2);} else { $rowp->camfin1 = '';}
										$sobf=($camfin/$cami)*100; if($sobf!=0){$rowp->sobfin1=number_format($sobf,1);} else { $rowp->sobfin1 = '';}
										$otc=$camfin*($sup*10000); if($otc!=0){$rowp->otc1=number_format($otc);} else { $rowp->otc1 = '';}
										$biot=$bioc+$biop; if($biot!=0){$rowp->biot1= number_format($biot);} else { $rowp->biot1 = '';}
										$ren=$biot/$sup;  if($ren!=0){$rowp->ren1= number_format($ren);} else { $rowp->ren1 = '';}
										if($biot>0){ $fca=$at/$biot; $rowp->fca1= number_format($fca, 2);} else {$fca=""; $rowp->fca1="";}
									}else{
										$rowp->sc1 = '';$rowp->pp1 = '';$rowp->isp1 = '';$rowp->at1 = '';$rowp->aha1 = '';
										$rowp->cami1 = '';$rowp->camf1='';$rowp->sob1='';$rowp->biop1='';$rowp->pha1='';$rowp->camp1='';$rowp->bioc1='';
										$rowp->cha1 = '';$rowp->camc1 = '';$rowp->camfin1 = '';$rowp->sobfin1 = '';$rowp->otc1 = '';
										$rowp->biot1='';$rowp->ren1 = '';$rowp->fca1='';
									}	
									$data[] = $rowp;	
								endforeach;
							}
							if($entro==0){
								//total general	
								$this->db->select('sum(sup) as sup,sum(pls) as pls');
								$this->db->where('ciclo =',$cic);
								//$this->db->where('fc !=','');
								$this->db->where('seccion =',$seci);
								if($filter['where']!=''){$this->db->where($filter['where']);}
								$this->db->group_by('ciclo');
								$resultp = $this->db->get($this->tablasie);
								foreach ($resultp->result() as $rowp):
									$rowp->seccion1 ='';$rowp->ciclo1 ='';$rowp->lab1 ='Total';$rowp->est1 ='';
									//$sup=$rowp->sup; $rowp->sup1 =number_format($sup,2);
									$sup=$rowp->sup; if($sup!=0){$rowp->sup1 =number_format($sup,2);} else {$rowp->sup1='';}
									$pls=0;$pls=$rowp->pls; $rowp->pls1= number_format($pls/1000, 3);
									$rowp->fs1 = '';$rowp->fc1 = '';$rowp->dc1 = '';
									$rowp->sc1 = '';$rowp->pp1 = '';$rowp->isp1 = '';$rowp->at1 = '';$rowp->aha1 = '';
									$rowp->cami1 = '';$rowp->camf1='';$rowp->sob1='';$rowp->biop1='';$rowp->pha1='';$rowp->camp1='';$rowp->bioc1='';
									$rowp->cha1 = '';$rowp->camc1 = '';$rowp->camfin1 = '';$rowp->sobfin1 = '';$rowp->otc1 = '';
									$rowp->biot1='';$rowp->ren1 = '';$rowp->fca1='';
									$data[] = $rowp;
								endforeach;
								//$entro=0;
							}
							//aqui se pondra lo general de cada laboratorio
							//$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc',NULL,FALSE);
							$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
							$this->db->where('ciclo =',$cic);
							//$this->db->where('fc !=','');
							$this->db->where('seccion =',$seci);
							if($filter['where']!=''){$this->db->where($filter['where']);}
							$this->db->group_by('ciclo');
							$this->db->group_by('lab');
							$resultl = $this->db->get($this->tablasie);
							$numlabs=$resultl->num_rows();
							if($numlabs>1){
							foreach ($resultl->result() as $rowl):
								$rowl->pinta=1;
								$rowl->seccion1 ='';$rowl->ciclo1 ='';$rowl->lab1 =$rowl->lab;$rowl->est1 ='';
								$rowl->fs1 = '';$rowl->fc1 = '';$rowl->dc1 = '';
								$dc=$rowl->dc;$lab=$rowl->lab;
								//$sup=$rowl->sup; $rowl->sup1 =number_format($sup,2);
								$sup=$rowl->sup; if($sup!=0){$rowl->sup1 =number_format($sup,2);} else {$rowl->sup1='';}
								$pls=0;$pls=$rowl->pls; $rowl->pls1= number_format($pls/1000, 3);
								$entra=0;
								if($dc!=''){
									//ahora obtiene por lab
									$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
									$this->db->where('ciclo =',$cic);
									$this->db->where('fc !=','');
									$this->db->where('seccion =',$seci);
									$this->db->where('lab =',$lab);
									if($filter['where']!=''){$this->db->where($filter['where']);}
									$this->db->group_by('ciclo');
									$this->db->group_by('lab');
									$resultll = $this->db->get($this->tablasie);
									foreach ($resultll->result() as $rowll):
										$rowll->pinta=1;
										$rowll->seccion1 ='';$rowll->ciclo1 ='';$rowll->lab1 =$rowll->lab;$rowll->est1 ='';
										$rowll->fs1 = '';$rowll->fc1 = '';$rowll->dc1 = '';
										$dc=$rowll->dc;$lab=$rowll->lab;
										$sup=$rowll->sup; $rowll->sup1 =number_format($sup,2);
										$pls=0;$pls=$rowll->pls; $rowll->pls1= number_format($pls/1000, 3);
										$sc=$dc/7; if($sc!=0){$rowll->sc1=number_format($sc);} else {$rowll->sc1='';}
										$pp=$rowll->pp; if($pp!=0){$rowll->pp1=number_format($pp,2);}else{$rowll->pp1 = '';}
										$isp=($pp/$dc)*7; if($isp!=0){$rowll->isp1=number_format($isp,2);} else { $rowll->isp1 = '';}
										$at=$rowll->at; if($at!=0){$rowll->at1=number_format($at);} else { $rowll->at1 = '';}
										$aha=$rowll->aha; if($aha!=0){$rowll->aha1=number_format($aha);} else { $rowll->aha1 = '';}
										$cami=$pls/($sup*10000); if($cami!=0){$rowll->cami1=number_format($cami,2);} else { $rowll->cami1 = '';}
										$camf=$rowll->camf; if($camf!=0){$rowll->camf1=number_format($camf,2);} else { $rowll->camf1 = '';}
										$sob=$rowll->sob; if($sob!=0){$rowll->sob1=number_format($sob,2);} else { $rowll->sob1 = '';}
										$biop=$rowll->biop; if($biop!=0){$rowll->biop1=number_format($biop);} else { $rowll->biop1 = '';}
										$pha=$biop/$sup; if($pha!=0){$rowll->pha1=number_format($pha);} else { $rowll->pha1 = '';}
										$camp=$rowll->camp; if($camp!=0){$rowll->camp1=number_format($camp,2);} else { $rowll->camp1 = '';}
										$bioc=$rowll->bioc; if($bioc!=0){$rowll->bioc1=number_format($bioc);} else { $rowll->bioc1 = '';}
										$cha=$bioc/$sup; if($cha!=0){$rowll->cha1=number_format($cha);} else { $rowll->cha1 = '';}
										$camc=$rowll->camc; if($camc!=0){$rowll->camc1=number_format($camc,2);} else { $rowll->camc1 = '';}
										$otc2=$rowll->camfin2; if($otc2!=0){$rowll->otc1=number_format($otc2);} else { $rowll->otc1 = '';}
										$camfin=$camp+$camc; if($camfin!=0){$rowll->camfin1=number_format($camfin,2);} else { $rowll->camfin1 = '';}
										$sobf=($camfin/$cami)*100; if($sobf!=0){$rowll->sobfin1=number_format($sobf,1);} else { $rowll->sobfin1 = '';}
										$otc=$camfin*($sup*10000); if($otc!=0){$rowll->otc1=number_format($otc);} else { $rowll->otc1 = '';}
										$biot=$bioc+$biop; if($biot!=0){$rowll->biot1= number_format($biot);} else { $rowll->biot1 = '';}
										$ren=$biot/$sup;  if($ren!=0){$rowll->ren1= number_format($ren);} else { $rowll->ren1 = '';}
										if($biot>0){ $fca=$at/$biot; $rowll->fca1= number_format($fca, 2);} else {$fca=""; $rowll->fca1="";}
										$entra=1;
									endforeach;	
								}else{
									$rowl->sc1 = '';$rowl->pp1 = '';$rowl->isp1 = '';$rowl->at1 = '';$rowl->aha1 = '';
									$rowl->cami1 = '';$rowl->camf1='';$rowl->sob1='';$rowl->biop1='';$rowl->pha1='';$rowl->camp1='';$rowl->bioc1='';
									$rowl->cha1 = '';$rowl->camc1 = '';$rowl->camfin1 = '';$rowl->sobfin1 = '';$rowl->otc1 = '';
									$rowl->biot1='';$rowl->ren1 = '';$rowl->fca1='';
								}
								if($entra==0) $data[] = $rowl; else $data[] = $rowll;
							endforeach;
							}
						}
						$cic =$row->ciclo;$row->ciclo1 =$row->ciclo;$lab='';$tota+=1;
					}else{$row->ciclo1 =''; $tota+=1;} //$ciclo=$registroMue["ciclo"];
					if($lab!=$row->lab){$lab =$row->lab;$row->lab1 =$row->lab;}else{$row->lab1 ='';} //$lab=$registroMue["lab"];
					if($row->est!=""){$est=$row->est;$row->est1 = $row->est;}else{$row->est1='';}  //$fs=$registroMue["est"];
					if($row->sup>0){$sup=$row->sup;$row->sup1= number_format($row->sup, 2); $sup1+=$sup;}else{$sup=0;$row->sup1="";}  //$sup=$registroMue["sup"];
					if($row->pls>0){$pls=$row->pls;$row->pls1= number_format($row->pls/1000, 3); $pls1+=$pls;}else{$pls=0;$row->pls1="";}  //$pls=$registroMue["pls"];
					//if($row->fs!=""){$row->fs1 = date("d-m-y",strtotime($row->fs));}else{$row->fs1='';}  //$fs=$registroMue["fs"];
					$fec=new Libreria();
					if($row->fs!=""){$fs=$row->fs;$row->fs1 = $fec->fecha($row->fs);}else{$row->fs1='';}  //$fs=$registroMue["fs"];
					//$pls1+=$row->pls;$sup1+=$row->sup;		
					
					$fc=$row->fc; $regsiem=$row->reg;
					if($fc!=""){
						$row->fc1 = $fec->fecha21($row->fc);	
						$dc=$row->dc; if($dc!=0){$row->dc1=$dc;} else {$row->dc1='';}
						$sc=$dc/7; if($sc!=0){$row->sc1=number_format($dc/7);} else {$row->sc1='';}
						$pp=$row->pp; if($pp!=0){$row->pp1=number_format($pp,2);}else{$row->pp1 = '';}
						$isp=($pp/$dc)*7; if($isp!=0){$row->isp1=number_format(($pp/$dc)*7,2);} else { $row->isp1 = '';}
						$at=$row->at; if($at!=0){$row->at1=number_format($at);} else { $row->at1 = '';}
						if($sup>0) $aha=$at/$sup; if($aha!=0){$row->aha1=number_format($aha);} else { $row->aha1 = '';}
						$cami=$pls/($sup*10000); if($cami!=0){$row->cami1=number_format($cami,2);} else { $row->cami1 = '';}
						$camf=$row->camf; if($camf!=0){$row->camf1=number_format($camf,2);} else { $row->camf1 = '';}
						$sob=$row->sob; if($sob!=0){$row->sob1=number_format($sob,2);} else { $row->sob1 = '';}
						$biop=$row->biop; if($biop!=0){$row->biop1=number_format($biop);} else { $row->biop1 = '';}
						$pha=$biop/$sup; if($pha!=0){$row->pha1=number_format($pha);} else { $row->pha1 = '';}
						$camp=$row->camp; if($camp!=0){$row->camp1=number_format($camp,2);} else { $row->camp1 = '';}
						$bioc=$row->bioc; if($bioc!=0){$row->bioc1=number_format($bioc);} else { $row->bioc1 = '';}
						$cha=$bioc/$sup; if($cha!=0){$row->cha1=number_format($cha);} else { $row->cha1 = '';}
						$camc=$row->camc; if($camc!=0){$row->camc1=number_format($camc,2);} else { $row->camc1 = '';}
						$camfin=$camp+$camc; if($camfin!=0){$row->camfin1=number_format($camfin,2);} else { $row->camfin1 = '';}
						$sobf=($camfin/$cami)*100; if($sobf!=0){$row->sobfin1=number_format($sobf,1);} else { $row->sobfin1 = '';}
						$otc=$camfin*($sup*10000); if($otc!=0){$row->otc1=number_format($otc);} else { $row->otc1 = '';}
						$biot=$bioc+$biop; if($biot!=0){$row->biot1= number_format($biot);} else { $row->biot1 = '';}
						$ren=$biot/$sup; if($ren!=0){$row->ren1= number_format($ren);} else { $row->ren1 = '';}
						if($biot>0){ $fca=$at/$biot; $row->fca1= number_format($fca, 2);} else {$fca=""; $row->fca1="";}
					}else{
						$row->fc1 = '';$row->dc1 = '';$row->sc1 = '';$row->pp1 = '';$row->isp1 = '';$row->at1 = '';$row->aha1 = '';
						$row->cami1 = '';$row->camf1='';$row->sob1='';$row->biop1='';$row->pha1='';$row->camp1='';$row->bioc1='';
						$row->cha1 = '';$row->camc1 = '';$row->camfin1 = '';$row->sobfin1 = '';$row->otc1 = '';
						$row->biot1='';$row->ren1 = '';$row->fca1='';
					}
					$data[] = $row;	
				endforeach;
				if($tota>=1){
							$tota=0; $entro=0;
							//total por ciclo
							//"SELECT ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF(fc, fs)) as dc,avg(pp) as pp,
							//sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,
							//avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2 
							//FROM siembras 
							//WHERE fc!='' and seccion='".$sec."' and cliente='".$txtclaveR."' and ciclo='".$cic."' 
							//group by ciclo";
							$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
							$this->db->where('ciclo =',$cic);
							$this->db->where('fc !=','');
							$this->db->where('seccion =',$seci);
							if($filter['where']!=''){$this->db->where($filter['where']);}
							
							$this->db->group_by('ciclo');
							$resultp = $this->db->get($this->tablasie);
							$cantidadSI=$resultp->num_rows();
							if($cantidadSI>0){ $entro=1;
								foreach ($resultp->result() as $rowp):
									$rowp->seccion1 ='';$rowp->ciclo1 ='';$rowp->lab1 ='Promedio';$rowp->est1 ='';
									//$sup=$rowp->sup; $rowp->sup1 =number_format($sup,2);
									$sup=$rowp->sup; if($sup!=0){$rowp->sup1 =number_format($sup,2);} else {$rowp->sup1='';}
									$pls=0;$pls=$rowp->pls; $rowp->pls1= number_format($pls/1000, 3);
									//$ciclo=$rowp->ciclo;
									$dc=$rowp->dc;
									//$lab=$rowp->lab;
									$rowp->fs1 = '';$rowp->fc1 = '';$rowp->dc1 = '';
									if($dc!=""){
										$sc=$dc/7; if($sc!=0){$rowp->sc1=number_format($sc);} else {$rowp->sc1='';}
										$pp=$rowp->pp; if($pp!=0){$rowp->pp1=number_format($pp,2);}else{$rowp->pp1 = '';}
										$isp=($pp/$dc)*7; if($isp!=0){$rowp->isp1=number_format($isp,2);} else { $rowp->isp1 = '';}
										$at=$rowp->at; if($at!=0){$rowp->at1=number_format($at);} else { $rowp->at1 = '';}
										$aha=$rowp->aha; if($aha!=0){$rowp->aha1=number_format($aha);} else { $rowp->aha1 = '';}
										$cami=$pls/($sup*10000); if($cami!=0){$rowp->cami1=number_format($cami,2);} else { $rowp->cami1 = '';}
										$camf=$rowp->camf; if($camf!=0){$rowp->camf1=number_format($camf,2);} else { $rowp->camf1 = '';}
										$sob=$rowp->sob; if($sob!=0){$rowp->sob1=number_format($sob,2);} else { $rowp->sob1 = '';}
										$biop=$rowp->biop; if($biop!=0){$rowp->biop1=number_format($biop);} else { $rowp->biop1 = '';}
										$pha=$biop/$sup; if($pha!=0){$rowp->pha1=number_format($pha);} else { $rowp->pha1 = '';}
										$camp=$rowp->camp; if($camp!=0){$rowp->camp1=number_format($camp,2);} else { $rowp->camp1 = '';}
										$bioc=$rowp->bioc; if($bioc!=0){$rowp->bioc1=number_format($bioc);} else { $rowp->bioc1 = '';}
										$cha=$bioc/$sup; if($cha!=0){$rowp->cha1=number_format($cha);} else { $rowp->cha1 = '';}
										$camc=$rowp->camc; if($camc!=0){$rowp->camc1=number_format($camc,2);} else { $rowp->camc1 = '';}
										$otc2=$rowp->camfin2; if($otc2!=0){$rowp->otc1=number_format($otc2);} else { $rowp->otc1 = '';}
										$camfin=$camp+$camc; if($camfin!=0){$rowp->camfin1=number_format($camfin,2);} else { $rowp->camfin1 = '';}
										$sobf=($camfin/$cami)*100; if($sobf!=0){$rowp->sobfin1=number_format($sobf,1);} else { $rowp->sobfin1 = '';}
										$otc=$camfin*($sup*10000); if($otc!=0){$rowp->otc1=number_format($otc);} else { $rowp->otc1 = '';}
										$biot=$bioc+$biop; if($biot!=0){$rowp->biot1= number_format($biot);} else { $rowp->biot1 = '';}
										$ren=$biot/$sup;  if($ren!=0){$rowp->ren1= number_format($ren);} else { $rowp->ren1 = '';}
										if($biot>0){ $fca=$at/$biot; $rowp->fca1= number_format($fca, 2);} else {$fca=""; $rowp->fca1="";}
									}else{
										$rowp->sc1 = '';$rowp->pp1 = '';$rowp->isp1 = '';$rowp->at1 = '';$rowp->aha1 = '';
										$rowp->cami1 = '';$rowp->camf1='';$rowp->sob1='';$rowp->biop1='';$rowp->pha1='';$rowp->camp1='';$rowp->bioc1='';
										$rowp->cha1 = '';$rowp->camc1 = '';$rowp->camfin1 = '';$rowp->sobfin1 = '';$rowp->otc1 = '';
										$rowp->biot1='';$rowp->ren1 = '';$rowp->fca1='';
									}	
									$data[] = $rowp;	
								endforeach;
							}
							if($entro==0){
								//total general	
								$this->db->select('sum(sup) as sup,sum(pls) as pls');
								$this->db->where('ciclo =',$cic);
								//$this->db->where('fc !=','');
								$this->db->where('seccion =',$seci);
								if($filter['where']!=''){$this->db->where($filter['where']);}
								$this->db->group_by('ciclo');
								$resultp = $this->db->get($this->tablasie);
								foreach ($resultp->result() as $rowp):
									$rowp->seccion1 ='';$rowp->ciclo1 ='';$rowp->lab1 ='Total';$rowp->est1 ='';
									//$sup=$rowp->sup; $rowp->sup1 =number_format($sup,2);
									$sup=$rowp->sup; if($sup!=0){$rowp->sup1 =number_format($sup,2);} else {$rowp->sup1='';}
									$pls=0;$pls=$rowp->pls; $rowp->pls1= number_format($pls/1000, 3);
									$rowp->fs1 = '';$rowp->fc1 = '';$rowp->dc1 = '';
									$rowp->sc1 = '';$rowp->pp1 = '';$rowp->isp1 = '';$rowp->at1 = '';$rowp->aha1 = '';
									$rowp->cami1 = '';$rowp->camf1='';$rowp->sob1='';$rowp->biop1='';$rowp->pha1='';$rowp->camp1='';$rowp->bioc1='';
									$rowp->cha1 = '';$rowp->camc1 = '';$rowp->camfin1 = '';$rowp->sobfin1 = '';$rowp->otc1 = '';
									$rowp->biot1='';$rowp->ren1 = '';$rowp->fca1='';
									$data[] = $rowp;
								endforeach;
								$entro=0;
							}	
							//aqui se pondra lo general de cada laboratorio
							$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
							$this->db->where('ciclo =',$cic);
							//$this->db->where('fc !=','');
							$this->db->where('seccion =',$seci);
							if($filter['where']!=''){$this->db->where($filter['where']);}
							$this->db->group_by('ciclo');
							$this->db->group_by('lab');
							$resultl = $this->db->get($this->tablasie);
							$numlabs=$resultl->num_rows();
							if($numlabs>1){
							foreach ($resultl->result() as $rowl):
								$rowl->pinta=1;
								$rowl->seccion1 ='';$rowl->ciclo1 ='';$rowl->lab1 =$rowl->lab;$rowl->est1 ='';
								$rowl->fs1 = '';$rowl->fc1 = '';$rowl->dc1 = '';
								$dc=$rowl->dc;$lab=$rowl->lab;
								$sup=$rowl->sup; if($sup!=0){$rowl->sup1 =number_format($sup,2);} else {$rowl->sup1='';}
								$pls=0;$pls=$rowl->pls; $rowl->pls1= number_format($pls/1000, 3);
								$entra=0;
								if($dc!=''){
									//ahora obtiene por lab
									$this->db->select('ciclo,lab,sum(sup) as sup,sum(pls) as pls,avg(DATEDIFF( fc,fs )) as dc,avg(pp) as pp,sum(at)/sum(sup) as aha,sum(at) as at,avg(camf) as camf,avg(sob) as sob,sum(biop) as biop,avg(camp) as camp,sum(bioc) as bioc,avg(camc) as camc,sum(((camp+camc)*sup*10000)) as camfin2',NULL,FALSE);
									$this->db->where('ciclo =',$cic);
									$this->db->where('fc !=','');
									$this->db->where('seccion =',$seci);
									$this->db->where('lab =',$lab);
									if($filter['where']!=''){$this->db->where($filter['where']);}
									$this->db->group_by('ciclo');
									$this->db->group_by('lab');
									$resultll = $this->db->get($this->tablasie);
									foreach ($resultll->result() as $rowll):
										$rowll->pinta=1;
										$rowll->seccion1 ='';$rowll->ciclo1 ='';$rowll->lab1 =$rowll->lab;$rowll->est1 ='';
										$rowll->fs1 = '';$rowll->fc1 = '';$rowll->dc1 = '';
										$dc=$rowll->dc;$lab=$rowll->lab;
										$sup=$rowll->sup; $rowll->sup1 =number_format($sup,2);
										$pls=0;$pls=$rowll->pls; $rowll->pls1= number_format($pls/1000, 3);
										$sc=$dc/7; if($sc!=0){$rowll->sc1=number_format($sc);} else {$rowll->sc1='';}
										$pp=$rowll->pp; if($pp!=0){$rowll->pp1=number_format($pp,2);}else{$rowll->pp1 = '';}
										$isp=($pp/$dc)*7; if($isp!=0){$rowll->isp1=number_format($isp,2);} else { $rowll->isp1 = '';}
										$at=$rowll->at; if($at!=0){$rowll->at1=number_format($at);} else { $rowll->at1 = '';}
										$aha=$rowll->aha; if($aha!=0){$rowll->aha1=number_format($aha);} else { $rowll->aha1 = '';}
										$cami=$pls/($sup*10000); if($cami!=0){$rowll->cami1=number_format($cami,2);} else { $rowll->cami1 = '';}
										$camf=$rowll->camf; if($camf!=0){$rowll->camf1=number_format($camf,2);} else { $rowll->camf1 = '';}
										$sob=$rowll->sob; if($sob!=0){$rowll->sob1=number_format($sob,2);} else { $rowll->sob1 = '';}
										$biop=$rowll->biop; if($biop!=0){$rowll->biop1=number_format($biop);} else { $rowll->biop1 = '';}
										$pha=$biop/$sup; if($pha!=0){$rowll->pha1=number_format($pha);} else { $rowll->pha1 = '';}
										$camp=$rowll->camp; if($camp!=0){$rowll->camp1=number_format($camp,2);} else { $rowll->camp1 = '';}
										$bioc=$rowll->bioc; if($bioc!=0){$rowll->bioc1=number_format($bioc);} else { $rowll->bioc1 = '';}
										$cha=$bioc/$sup; if($cha!=0){$rowll->cha1=number_format($cha);} else { $rowll->cha1 = '';}
										$camc=$rowll->camc; if($camc!=0){$rowll->camc1=number_format($camc,2);} else { $rowll->camc1 = '';}
										$otc2=$rowll->camfin2; if($otc2!=0){$rowll->otc1=number_format($otc2);} else { $rowll->otc1 = '';}
										$camfin=$camp+$camc; if($camfin!=0){$rowll->camfin1=number_format($camfin,2);} else { $rowll->camfin1 = '';}
										$sobf=($camfin/$cami)*100; if($sobf!=0){$rowll->sobfin1=number_format($sobf,1);} else { $rowll->sobfin1 = '';}
										$otc=$camfin*($sup*10000); if($otc!=0){$rowll->otc1=number_format($otc);} else { $rowll->otc1 = '';}
										$biot=$bioc+$biop; if($biot!=0){$rowll->biot1= number_format($biot);} else { $rowll->biot1 = '';}
										$ren=$biot/$sup;  if($ren!=0){$rowll->ren1= number_format($ren);} else { $rowll->ren1 = '';}
										if($biot>0){ $fca=$at/$biot; $rowll->fca1= number_format($fca, 2);} else {$fca=""; $rowll->fca1="";}
										$entra=1;
									endforeach;	
								}else{
									$rowl->sc1 = '';$rowl->pp1 = '';$rowl->isp1 = '';$rowl->at1 = '';$rowl->aha1 = '';
									$rowl->cami1 = '';$rowl->camf1='';$rowl->sob1='';$rowl->biop1='';$rowl->pha1='';$rowl->camp1='';$rowl->bioc1='';
									$rowl->cha1 = '';$rowl->camc1 = '';$rowl->camfin1 = '';$rowl->sobfin1 = '';$rowl->otc1 = '';
									$rowl->biot1='';$rowl->ren1 = '';$rowl->fca1='';
								}
								if($entra==0) $data[] = $rowl; else $data[] = $rowll;
							endforeach;
							}
						}
			endforeach;	
			return $data;
		}
		function getNumRowsSie($filter){
			$this->db->select('seccion,reg,ciclo,est,sup,pls,fs,fc,DATEDIFF( fc,fs ) AS dc,pp,at,camf,sob,biop,camp,bioc,camc,lab',NULL,FALSE);
			//$this->db->group_by('ciclo');
			//$this->db->group_by('lab');
			
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
			}
			$this->db->order_by('seccion');
			$this->db->order_by('ciclo');				
			$this->db->order_by('lab');
			//$this->db->order_by(cast('est as UNSIGNED'));
			$this->db->order_by('reg');
			$result = $this->db->get($this->tablasie);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}

		function agregarg($cli,$sec,$fs,$ori,$cic,$est,$sup,$pls){
			//$pos = str_replace(",", "", $pos);$nau = str_replace(",", "", $nau);
			if($cic==""){$cic=1;}
			if($fs!=""){$data=array($this->cli=>$cli,$this->sec=>$sec,$this->fs=>$fs,$this->ori=>$ori,$this->cic=>$cic,$this->est=>$est,$this->sup=>$sup,$this->pls=>$pls);}
			else{$data=array($this->cli=>$cli,$this->sec=>$sec,$this->ori=>$ori,$this->cic=>$cic,$this->est=>$est,$this->sup=>$sup,$this->pls=>$pls);}				
			$this->db->insert($this->tablasie,$data);
			return $this->db->insert_id();
		}
		function actualizarg($id,$cli,$sec,$fs,$ori,$cic,$est,$sup,$pls,$fc,$pp,$at,$camf,$sob,$biop,$camp,$bioc,$camc){
			$aqui=0;// $pls=$pls*1000;	
			if($pls!=""){ $pls = str_replace(",", "", $pls);$pls = str_replace(".", "", $pls);}
			if($at!="") $at = str_replace(",", "", $at);
			if($biop!="") $biop = str_replace(",", "", $biop);
			if($bioc!="") $bioc = str_replace(",", "", $bioc);
			if($cic==""){$cic=1;}
			if($fs!="" && $fc!=''){$data=array($this->cli=>$cli,$this->sec=>$sec,$this->fs=>$fs,$this->ori=>$ori,$this->cic=>$cic,$this->est=>$est,$this->sup=>$sup,$this->pls=>$pls,$this->fc=>$fc,$this->pp=>$pp,$this->at=>$at,$this->camf=>$camf,$this->sob=>$sob,$this->biop=>$biop,$this->camp=>$camp,$this->bioc=>$bioc,$this->camc=>$camc);}
			elseif($fs!=""){$data=array($this->cli=>$cli,$this->sec=>$sec,$this->fs=>$fs,$this->ori=>$ori,$this->cic=>$cic,$this->est=>$est,$this->sup=>$sup,$this->pls=>$pls,$this->pp=>$pp,$this->at=>$at,$this->camf=>$camf,$this->sob=>$sob,$this->biop=>$biop,$this->camp=>$camp,$this->bioc=>$bioc,$this->camc=>$camc);}
				elseif($fc!=""){$aqui=1;}
					else{$data=array($this->cli=>$cli,$this->sec=>$sec,$this->ori=>$ori,$this->cic=>$cic,$this->est=>$est,$this->sup=>$sup,$this->pls=>$pls,$this->pp=>$pp,$this->at=>$at,$this->camf=>$camf,$this->sob=>$sob,$this->biop=>$biop,$this->camp=>$camp,$this->bioc=>$bioc,$this->camc=>$camc);
			}
			if($aqui==0){				
				$this->db->where($this->id,$id);
				$this->db->update($this->tablasie,$data);
				if($this->db->affected_rows()>0){ return 1; } else { return 0; }
			}else{ $aqui+3; return $aqui;	}
		}	
		function borrarg($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasie);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
    }    
?>