<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Proveedores_model extends CI_Model {
        //private $nombre;
        public $id="Numero";
        public $nombre="Razon";
		public $dom="Dom";
		public $loc="Loc";
		public $edo="Edo";
		public $rfc="RFC";
		public $cp="CP";
		public $siglas="Siglas";
		public $tabla="proveedores";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function get_last_ten_entries()
		{
			$query=$this->db->get('proveedores');
			return $query->result();
		}
		public function insertar($nombre,$dom)
		{
			$this->Razon = $nombre;
			$this->Dom = $dom;
			$this->db->insert('proveedores', $this);
		}
		public function verActivos(){
			//$this->db->where($this->status,1);
			$this->db->order_by($this->siglas);
			$this->db->order_by($this->nombre);
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
		public function agregar($nombre,$dom,$loc,$edo,$rfc,$cp,$siglas,$filter){
			if($filter['usuario']=='Anita Espino') $this->tabla='proveedoresa';
			$data=array($this->nombre=>$nombre,$this->dom=>$dom,$this->loc=>$loc,$this->edo=>$edo,$this->rfc=>$rfc,$this->cp=>$cp,$this->siglas=>$siglas);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$nombre,$dom,$loc,$edo,$rfc,$cp,$siglas,$filter){
			if($filter['usuario']=='Anita Espino') $this->tabla='proveedoresa';
			$data=array($this->nombre=>$nombre,$this->dom=>$dom,$this->loc=>$loc,$this->edo=>$edo,$this->rfc=>$rfc,$this->cp=>$cp,$this->siglas=>$siglas);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function getNombre($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->select($this->Razon);
			$query=$this->db->get_where($this->tabla,array($this->Numero=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->Razon;
			}
			return 0;
		}
		public function getDom($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->select($this->Dom);
			$query=$this->db->get_where($this->tabla,array($this->Numero=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->Dom;
			}
			return 0;
		}
		public function getCliente($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('Numero',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		function buscar_activos($nom,$zoni){
			$this->db->like($this->nombre,$nom,'both');	
			$this->db->like($this->siglas,$zoni,'both');
			$this->db->where($this->status,1);
			$this->db->order_by($this->siglas);
			$this->db->order_by($this->nombre);			
			$query=$this->db->get($this->tabla); 
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
		
		function getUsuarios($filter){
			$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Siglas');
			//$sel=array('siglas','Razon','Dom','Loc','Edo','RFC','CP');
			/*$this->db->select('siglas');
			$this->db->select('Razon');
			$this->db->select('Dom');
			$this->db->select('Loc');
			$this->db->select('Edo');
			$this->db->select('RFC');
			$this->db->select('CP');*/
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			if($filter['usuario']=='Anita Espino') $this->tabla='proveedoresa';
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$cp=$row->CP;
				//if($row->CP == 0){ $row->CP="";	}
				//if($row->RFC == 0){ $row->RFC="";	}
				$data[] = $row;	
				/*$data['siglas'] = $row->siglas;
				$data['razon'] = $row->Razon;
				$data['dom'] = $row->Dom;
				$data['loc'] = $row->Loc;
				$data['edo'] = $row->Edo;
				$data['rfc'] = $row->RFC;
				$data['cp'] = $row->CP;*/
			endforeach;
			return $data;
		}
		function getNumRows($filter){
			$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Siglas');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
			
    }
    
?>