<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificados_model extends CI_Model {        
        public $id="NumReg";
        public $numcer="NumCer";
        public $iv="IniVig";
        public $fv="FinVig";
        public $cp="CanPos";
        public $cn="CanNau";
        public $tabla="certificados";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		public function agregar($cer,$fi,$fv,$pos,$nau){
			$pos = str_replace(",", "", $pos);$nau = str_replace(",", "", $nau);
			$data=array($this->numcer=>$cer,$this->iv=>$fi,$this->fv=>$fv,$this->cp=>$pos,$this->cn=>$nau);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$cer,$fi,$fv,$pos,$nau){
			$pos = str_replace(",", "", $pos);$nau = str_replace(",", "", $nau);
			$data=array($this->numcer=>$cer,$this->iv=>$fi,$this->fv=>$fv,$this->cp=>$pos,$this->cn=>$nau);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function getCertificados($filter){
			$this->db->where('NumReg >',0);
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			foreach($result->result() as $row):
				$row->CanPos = number_format(($row->CanPos), 3, '.', ','); if($row->CanPos==0.000){$row->CanPos="";}
				$row->CanNau = number_format(($row->CanNau), 3, '.', ','); if($row->CanNau==0.000){$row->CanNau="";}
				$data[] = $row;
			endforeach;
			return $data;
		}
		
		function getNumRowsC($filter){
			$this->db->where('NumReg >',1);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	
    }
    
?>