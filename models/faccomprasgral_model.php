<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Faccomprasgral_model extends CI_Model {
        public $tabla="proveedores";
		public $nrf="nrf";
		public $conf="con";
		public $pro="prov";
		public $tip="tipo";
		public $fac="fac";
		public $can="can";
		public $pre="pre";
		public $med="uni";
		public $fec="fec";
		public $fcap="fcap";
		
		
		
		public $tablaCon="conceptos";
		public $nrc="nrc";
		public $con="des";
		public $pm="pm";
		public $ana="analizar";
		
		
		var $today;
        
		public $tablaOrdD="ordendet";
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getfaccomprasgral($filter,$buscar,$ciclo){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'con=nrc','inner');
			$this->db->where('analizar',1);
			if($buscar!=''){
				$this->db->like($this->con,$buscar);
			}
			$this->db->group_by('dp'); $this->db->group_by('des');
			//$this->db->order_by('des');$this->db->order_by('Razon');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*$zonas="Todos";
			if($filter['num']!=0){
					//$this->db->where($this->zona,$filter['num']);		
			}	*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();$totact=0;$totant=0;$totinc=0;$cli=0;$totgranja=0;$tex="";
			if($can>0){
			
			
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$dp=$row->dp;
				if($cli!=$dp){
							
						if($totgranja>0){ $totgranja=0;
							if($cli==1) $tex="Dólares"; else $tex="Pesos"; 
							$this->db->select('max(Numero)');
							$resultZ= $this->db->get('clientes');
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "Total ".$tex.":";$rowZ->cantidad = "";$rowZ->ppact = "";$rowZ->importe ="$".number_format($totact, 2, '.', ',');
								$rowZ->pca = "";$rowZ->inc = "";$rowZ->incp = "";$rowZ->anterior = "";$rowZ->ppant = "";$rowZ->incd="$". number_format($totinc, 2, '.', ',');
								$rowZ->impant ="$". number_format($totant, 2, '.', ',');
								$rowZ->proyeccion = "";$rowZ->pp = "";
								$data[] = $rowZ;	
							endforeach;
							$totact=0;$totant=0;$totinc=0;
						}
						$cli=$row->dp;
				}
				$uno=0;$dos=0;$tres=0;$pre=0;$totact+=$row->importe;$totant+=$row->impant;$totgranja+=1;
				$row->inc=number_format(($row->cantidad-$row->anterior), 2, '.', ',')." ".$row->uni;
				//$row->incd="$".number_format(($row->ppact-$row->ppant)*$row->cantidad, 2, '.', ',');
				//$row->incd="$".number_format($row->incd, 2, '.', ',');
				$row->incp="$".number_format($row->ppact-$row->ppant, 2, '.', ',');$row->incp1=$row->ppact-$row->ppant;
				$row->incp2=number_format($row->ppact-$row->ppant, 2, '.', ',');$totinc+=$row->cantidad*($row->incp2);
				$row->incd="$".number_format($row->cantidad*$row->incp2, 2, '.', ',');
				$row->itf="$".number_format(($row->anterior*$row->ppact)-$row->impant, 2, '.', ',');
				$row->itp="$".number_format($row->anterior*$row->ppact, 2, '.', ',');
				$row->ppact="$".number_format($row->ppact, 2, '.', ',');
				
				
				if($row->dp==1){$row->dp="Dólares";}else{$row->dp="Pesos";}
				if($row->anterior!=0){$uno=$row->anterior;$row->anterior = number_format($row->anterior, 2, '.', ',')." ".$row->uni;}else{$row->anterior="";}
				if($row->proyeccion!=0){$tres=$row->proyeccion;$row->proyeccion = number_format($row->proyeccion, 2, '.', ',')." ".$row->uni;}else{$row->proyeccion="";}
				$dos=$row->cantidad;
				$row->cantidad = number_format($row->cantidad, 2, '.', ',')." ".$row->uni;
										
				$row->importe ="$". number_format($row->importe, 2, '.', ',');
				if($row->impant>0){
					$row->ppant="$".number_format($row->ppant, 2, '.', ',');	
					$row->impant ="$". number_format($row->impant, 2, '.', ',');
				}else{$row->impant ='';$row->ppant="";}
				 
				if($dos>0 && $uno>0){
					$row->pca=number_format(((($dos/$uno)-1)*100), 2, '.', ',')."%";$row->pca1=number_format((($dos/$uno)*100), 2, '.', ',');
				} else {$row->pca='';;$row->pca1='';}
				if($dos>0 && $tres>0){$row->pp=number_format((($dos/$tres)*100), 2, '.', ',')."%";} else {$row->pp='';}
				//elseif($uno==0 && $dos==0)  $row->pca=''; else $row->pca='100.00%'; 
				/*if($dos>0) $row->pp=number_format((($tres-$dos)/$dos*100), 2, '.', ',')."%";
				elseif($tres==0 && $dos==0)  $row->pp=''; else $row->pp='100.00%';*/
				$data[] = $row;	
			endforeach;	
			if($totgranja>0){ $totgranja=0;
							if($cli==1) $tex="Dólares"; else $tex="Pesos"; 
							$this->db->select('max(Numero)');
							$resultZ= $this->db->get('clientes');
							foreach ($resultZ->result() as $rowZ):				
								$rowZ->des = "Total ".$tex.":";$rowZ->cantidad = "";$rowZ->ppact = "";$rowZ->importe ="$".number_format($totact, 2, '.', ',');
								$rowZ->pca = "";$rowZ->inc = "";$rowZ->incp = "";$rowZ->anterior = "";$rowZ->ppant = "";$rowZ->incd="$". number_format($totinc, 2, '.', ',');
								$rowZ->impant ="$". number_format($totant, 2, '.', ',');
								$rowZ->proyeccion = "";$rowZ->pp = "";
								$data[] = $rowZ;	
							endforeach;
						}
			}
			return $data;
		}
		
		function getNumRows($filter,$buscar,$ciclo){
			$this->db->select('des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,proyeccion,anterior');
			$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->join($this->tablaCon, 'con=nrc','inner');
			$this->db->where('analizar',1);
			if($buscar!=''){
				$this->db->like($this->con,$buscar);
			}
			$this->db->group_by('des'); //$this->db->group_by('Razon');
			//if($filter['order']!='')
			//	$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
    }
    
?>