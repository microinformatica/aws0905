<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Edoctam_model extends CI_Model {        
        public $nrcd="NRC";	public $idd="Numero"; public $idsi="NSR"; public $saldoc="Saldo"; public $fechad="Fecha";
        public $razond="Razon";	public $importedd="ImporteD"; public $pesosd="Pesos"; public $cuentad="Cuenta";
		public $tcd="TC"; public $obsd="Obs"; public $ndd="ND"; public $camd="Cam"; public $desd="Des"; public $card="Car";public $can="Cancelacion";
		public $aplicard="Aplicar"; public $tablad="depositos"; public $tablac="clientes"; public $tablasi="saldosi";public $impd="impd";
        public $ngra="ngra";
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		//public function agregard($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$gra){
		public function agregard($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$impd,$gra){	
			
			$usd = str_replace(",", "", $usd);				
			$mn = str_replace(",", "", $mn);
			$tipo=$est;$tipoC=$descam;	
			if($tipo==0){ $car=0;$des=0;$tipoC=4;}
			if($tipo==1){ $car=-1;$des=0;$tipoC=4;}
			if($tipo==-1){ $des=-1;$car=0;}
			if($tipoC==3){ $cam=-1;}else{$cam=0;}
			$data=array($this->nrcd=>$numcli,$this->fechad=>$fec,$this->importedd=>$usd,$this->tcd=>$tc,$this->cuentad=>$cta,$this->obsd=>$obs,$this->pesosd=>$mn,$this->aplicard=>$ciclo,$this->desd=>$des,$this->card=>$car,$this->camd=>$cam,$this->ngra=>$gra);			
			$this->db->insert($this->tablad,$data);
			return $this->db->insert_id();			
		}
		
		public function actualizard($id,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$gra){
			$usd = str_replace(",", "", $usd);				
			$mn = str_replace(",", "", $mn);
			$tipo=$est;$tipoC=$descam;	
			if($tipo==0){ $car=0;$des=0;$tipoC=4;}
			if($tipo==1){ $car=-1;$des=0;$tipoC=4;}
			if($tipo==-1){ $des=-1;$car=0;}
			if($tipoC==3){ $cam=-1;}else{$cam=0;}
			
			
			$data=array($this->fechad=>$fec,$this->importedd=>$usd,$this->tcd=>$tc,$this->cuentad=>$cta,$this->obsd=>$obs,$this->pesosd=>$mn,$this->aplicard=>$ciclo,$this->desd=>$des,$this->card=>$car,$this->can=>$cancelar,$this->ngra=>$gra);
			$this->db->where($this->ndd,$id);
			$this->db->update($this->tablad,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function borrard($id){
			$this->db->where($this->ndd,$id);
			$this->db->delete($this->tablad,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
					return 0;
			}			
		}	
		
		
		function verClientes(){
			$this->db->order_by('Razon');
			$query=$this->db->get('clientes');
			return $query->result();			
		}
		function getClientes($filter){
			$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Zona,referencia,con');
			$this->db->join('refbco', 'idcli = Numero','left');
			
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($row->referencia=='') $row->referencia='';				
				if($row->con=='') $row->con='';
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getSaldos($filter,$cli,$cic,$mes,$ao){
			//select (1) as tipo, fecsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,(kgssal*prekgs)as car,(0) as abo from salidas where clisal=11 union all select (2) as tipo,fecha,(1) as talla,(1) as kgssal,(1) as prekgs,(0) as car,(pesos) as abo from depositos where NRC=11 order by fecsal,tipo
			if($ao>0 || $mes>0 ){
					//esto es cuando se elige el año, y se calcula a los años restantes en totales	
					//select (1) as tipo,year(fecsal) as fecsal,(0) as folsal,(0) as talla,sum(kgssal) as kgssal,avg(prekgs) as prekgs,usdkgs,(0) as fac,sum(kgssal*prekgs)as car,sum(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='6' and estsal=1 group by year(fecsal) union all select (2) as tipo,year(fecha),(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(0) as fac,(0) as car,(0) as caru,sum(pesos) as abo,sum(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='6' group by year(fecha) order by fecsal,tipo
					if($cic==0)$querya =("select (1) as tipo,Date_format(fecsal,'%Y') as fecsal,(-1) as folsal,(0) as talla,sum(kgssal) as kgssal,(0) as prekgs,(0) as usdkgs,(0) as fac,sum(kgssal*prekgs)as car,sum(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='$cli' and estsal=1 and year(fecsal)=('$ao'-1) group by year(fecsal) union all select (2) as tipo,year(fecha) as fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(0) as fac,(0) as car,(0) as caru,sum(pesos) as abo,sum(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='$cli' and year(fecha)=('$ao'-1) group by year(fecha)");
					else $querya =("select (1) as tipo,Date_format(fecsal,'%Y') as fecsal,(-1) as folsal,(0) as talla,sum(kgssal) as kgssal,(0) as prekgs,(0) as usdkgs,(0) as fac,sum(kgssal*prekgs)as car,sum(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='$cli' and estsal=1 and year(fecsal)=('$ao'-1) and ciclo='$cic' group by year(fecsal) union all select (2) as tipo,year(fecha) as fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(0) as fac,(0) as car,(0) as caru,sum(pesos) as abo,sum(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='$cli' and year(fecha)=('$ao'-1) and Aplicar='$cic' group by year(fecha)");				
					//$query=$this->db->query("select (1) as tipo, fecsal,folsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,usdkgs,(facsal) as fac,(kgssal*prekgs)as car,(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal=$cli and estsal=1 and month(fecsal)=$mes union all select (2) as tipo,fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(factura) as fac,(0) as car,(0) as caru,(pesos) as abo,(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC=$cli and month(fecha)=$mes order by fecsal,tipo");
					$query1 =("select (1) as tipo,fecsal,folsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,usdkgs,(facsal) as fac,(kgssal*prekgs)as car,(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='$cli' and estsal=1");
					if($ao>0) $query1=$query1.' and year(fecsal)='.$ao;
					if($mes>0) $query1=$query1.' and month(fecsal)='.$mes;
					if($cic>0) $query1=$query1.' and ciclo='.$cic;
					$query2 =("select (2) as tipo,fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(factura) as fac,(0) as car,(0) as caru,(pesos) as abo,(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='$cli'"); //and month(fecha)=$mes"
					if($ao>0) $query2=$query2.' and year(fecha)='.$ao;
					if($mes>0) $query2=$query2.' and month(fecha)='.$mes;
					if($cic>0) $query2=$query2.' and Aplicar='.$cic;
					$query = $this->db->query($querya.' union all '.$query1.' union all '. $query2.' order by fecsal,tipo');
				
			}elseif($cic>0){
				//$query=$this->db->query("select (1) as tipo,fecsal,folsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,usdkgs,(facsal) as fac,(kgssal*prekgs)as car,(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal=$cli and estsal=1 union all select (2) as tipo,fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(factura) as fac,(0) as car,(0) as caru,(pesos) as abo,(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC=$cli order by fecsal,tipo");
				$query1 =("select (1) as tipo,fecsal,folsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,usdkgs,(facsal) as fac,(kgssal*prekgs)as car,(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='$cli' and estsal=1 and ciclo='$cic'");
				$query2 =("select (2) as tipo,fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(factura) as fac,car,(0) as caru,(pesos) as abo,(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='$cli' and Aplicar='$cic'");
				$query = $this->db->query($query1.' union all '. $query2.' order by fecsal,tipo');
				}else{
					$query1 =("select (1) as tipo,fecsal,folsal,(SELECT nomt from tallas where idt=talsal) as talla,kgssal,prekgs,usdkgs,(facsal) as fac,(kgssal*prekgs)as car,(kgssal*usdkgs)as caru,(0) as abo,(0) as abou,(0) as Des,(0) as Obs,(0) as Fecha,(0) as ngra,(0) as ImporteD,(0) as TC,(0) as Cuenta,(0) as Pesos,(0) as ND from salidas where clisal='$cli' and estsal=1");
					$query2 =("select (2) as tipo,fecha,(0) as folsal,(0) as talla,(0) as kgssal,(0) as prekgs,(0) as usdkgs,(factura) as fac,car,(0) as caru,(pesos) as abo,(ImporteD) as abou,Des,Obs,Fecha,ngra,ImporteD,TC,Cuenta,Pesos,ND from depositos where NRC='$cli'");
					$query = $this->db->query($query1.' union all '. $query2.' order by fecsal,tipo');
				}
			$data = array(); $fec=new Libreria(); $sal=0; $salu=0;$dia='';$fol=''; $tv=0; $td=0;$tvu=0; $tdu=0;
			if($query->num_rows()>0){
			 foreach($query->result() as $row):
			 
				if($dia!=$row->fecsal){
						$dia=$row->fecsal;
						if($row->folsal!='-1') $row->fecsal=$fec->fecha($row->fecsal);
				}else{ $row->fecsal="";}
				if($fol!=$row->folsal){
						$fol=$row->folsal; 
						if($row->folsal>0) $row->folsal=str_pad($row->folsal, 5, "0", STR_PAD_LEFT); else $row->folsal="";
					}else{ $row->folsal="";}
					if($row->Des=='-1') $row->folsal=$row->folsal.' '.$row->Obs.' Descto. ';
				if($row->tipo==1 or $row->car==-1){
					$row->carg=$row->car;
					if($row->car!=-1) $tv+=$row->car;
					$tvu+=$row->caru; $row->abo=''; $row->abou='';
					$row->kgssal=number_format($row->kgssal, 0, '.', ',');
					if($row->prekgs!=0) $row->prekgs="$ ".number_format($row->prekgs, 2, '.', ','); else $row->prekgs=''; 
					if($row->usdkgs!=0) $row->usdkgs="$ ".number_format($row->usdkgs, 2, '.', ','); else $row->usdkgs='';
					//if(($sal+$row->car)!=0){
						if($row->car==-1){
								$row->folsal=$row->Obs;
						 	     $row->sal="$ ".number_format(($sal+$row->Pesos), 2, '.', ','); $sal+=$row->Pesos;$tv+=$row->Pesos;
						 	} else {$row->sal="$ ".number_format(($sal+$row->car), 2, '.', ','); $sal+=$row->car;}
					//	 } else {$row->sal=""; }
					if(($salu+$row->caru)!=0) $row->salu="$ ".number_format(($salu+$row->caru), 2, '.', ','); else $row->salu=""; $salu+=$row->caru;
					if($row->car!=0){
						 if($row->car==-1){
						 	     $row->car="$ ".number_format($row->Pesos, 2, '.', ',');
						 } else {$row->car="$ ".number_format($row->car, 2, '.', ',');}
					}else {$row->car="";}
					if($row->caru!=0) $row->caru="$ ".number_format($row->caru, 2, '.', ','); else $row->caru="";
				}else{
					$td+=$row->abo;$tdu+=$row->abou;
					if($row->folsal=='')$row->folsal='';
					$row->talla='';$row->kgssal='';$row->prekgs='';$row->car='';$row->usdkgs='';$row->caru='';
					//if(($sal-$row->abo)!=0) $row->sal="$ ".number_format(($sal-$row->abo), 2, '.', ','); else $row->sal=""; $sal-=$row->abo;
					$row->sal="$ ".number_format(($sal-$row->abo), 2, '.', ','); $sal-=$row->abo;
					if(($salu-$row->abou)!=0) $row->salu="$ ".number_format(($salu-$row->abou), 2, '.', ','); else $row->salu=""; $salu-=$row->abou;
					if($row->abo!=0) $row->abo="$ ".number_format($row->abo, 2, '.', ','); else $row->abo="";
					if($row->abou!=0) $row->abou="$ ".number_format($row->abou, 2, '.', ','); else $row->abou="";
					
				}
				if($row->fac==0) $row->fac=''; //else $row->fac=str_pad($row->fac, 4, "0", STR_PAD_LEFT);
				if($row->talla==0) $row->talla='';
				$data[] = $row;
			 endforeach;
			 $this->db->select('max(Numero)');	
			 $result = $this->db->get('clientes');
			 foreach ($result->result() as $row):				
					$row->fecsal = "Total:";
				 	$row->folsal='';$row->talla='';$row->kgssal='';$row->prekgs='';$row->fac='';$row->usdkgs='';
				 	if($tv>0)$row->car="$ ".number_format($tv, 2, '.', ','); else $row->car='';
					if($td>0)$row->abo="$ ".number_format($td, 2, '.', ','); else $row->abo='';
					if($tvu>0)$row->caru="$ ".number_format($tvu, 2, '.', ','); else $row->caru='';
					if($tdu>0)$row->abou="$ ".number_format($tdu, 2, '.', ','); else $row->abou='';
					if(($tv-$td)!=0) $row->sal="$ ".number_format($tv-$td, 2, '.', ','); else $row->sal="";
					if(($tvu-$tdu)!=0) $row->salu="$ ".number_format($tvu-$tdu, 2, '.', ','); else $row->salu="";
					$data[] = $row;	
			 endforeach;			 
			}
			return $data;
		}
   	}
    
?>
