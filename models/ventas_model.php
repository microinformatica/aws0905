<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//LOAD DATA INFILE '../../../Users/Je/Desktop/maduracion2018.csv' INTO TABLE mad_produccion FIELDS TERMINATED BY ','
//lo de arriba es para pasar de excel a base de datos 


    class Ventas_model extends CI_Model {
        //private $nombre;
       public $idcos="idcos";public $feccos="feccos";public $estcos="estcos";public $grscos="grscos";public $kgscos="kgscos";
        public $numcos="numcos";public $clicos="clicos";public $tipcos="tipcos";public $prebas="prebas";public $numgrab="numgrab";public $folio="folio";
		public $factura="factura";public $aviso="aviso";
        public $tabla="bordo";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function bordogral($filter,$cic){
			//SELECT	feccos, avg(grscos) as grs, sum(kgscos) kgs, sum((grscos+prebas)*kgscos) as imp from bordo group by feccos
			$this->db->select("feccos,folio,factura,aviso,numgrab,clicos,Razon, avg(grscos) as grs, sum(kgscos) as kgs, sum((grscos+prebas)*kgscos) as imp");
			$this->db->join('clientes', 'Numero=clicos','inner');
			//$this->db->join($this->tablaest, $this->idpis.'='.$this->estcos,'inner');   
			$this->db->group_by($this->feccos); 
			$this->db->group_by($this->numgrab);
			$this->db->group_by($this->clicos);
			$this->db->group_by($this->folio);
			$this->db->order_by($this->feccos,'DESC');
			$this->db->order_by($this->folio,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array();  $fec=new Libreria(); 
			$impcv=0;$imp=0;$kgcv=0;
			if($result->num_rows()>0){
			//obtner el total de la compra venta
			//SELECT sum((grscos+prebas)*kgscos) as imp from bordo
			$this->db->select('sum((grscos+prebas)*kgscos) as imp,sum(kgscos) as kgt');	
			if($filter['where']!='') $this->db->where($filter['where']);
			$resulti = $this->db->get($this->tabla.'_'.$cic);
			foreach ($resulti->result() as $rowi):				
				$impcv=$rowi->imp;$kgcv=$rowi->kgt;
			endforeach;	
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$imp=$row->imp;$kg=$row->kgs;
				$row->folio1=str_pad($row->folio, 5, "0", STR_PAD_LEFT);
				switch($row->numgrab){
					case 2: $row->Razon='Kino- '.$row->Razon; break;	case 4: $row->Razon='Ahome- '.$row->Razon; break; case 5: $row->Razon='Oce. Azul '.$row->Razon; break;
					case 6: $row->Razon='Huatabampo- '.$row->Razon; break;					
					case 10: $row->Razon='Topolobampo- '.$row->Razon; break;					
				}
				$row->feccos1=$fec->fecha($row->feccos);
				$row->grs=number_format($row->grs, 2, '.', ',');
				$row->kgs=number_format($row->kgs, 0, '.', ',');
				$row->totk=number_format($kgcv, 0, '.', ',');
				$row->imp='$ '.number_format($row->imp, 2, '.', ',');
				$row->toti='$ '.number_format($impcv, 2, '.', ',');
				if($row->factura==0) $row->factura='';
				if($row->aviso==0) $row->aviso='';
				//$cont=1;
				//if($cont>0) 
				$impcv-=$imp;$kgcv-=$kg;
				$data[] = $row;
			endforeach;
			
			
			}			
			return $data;			
		}
		
		// VEntas generales y por granja
		//SELECT nomg,nomc,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where NRC=clisal) as depvta from granjas inner join( clientes inner join salidas on clisal=idc) on grasal=idg where estsal=1 group by clisal

		function getVentasg($gra,$cic){
			if($gra==0){
			$queryvg=$this->db->query("SELECT nomg,Razon,sum(kgscos) as kgsvta, sum(kgscos*(prebas+grscos)) as impvta, (select sum(Pesos) from depositos where ngra=idg and NRC=clicos ) as depvta from granjas inner join( clientes inner join bordo_$cic on clicos=Numero) on numgrab=idg where tipcos=2 group by nomg,clicos");
			}else {
			$queryvg=$this->db->query("SELECT nomg,Razon,sum(kgscos) as kgsvta, sum(kgscos*(prebas+grscos)) as impvta, (select sum(Pesos) from depositos where ngra=idg and NRC=clicos ) as depvta from granjas inner join( clientes inner join bordo_$cic on clicos=Numero) on numgrab=idg where tipcos=2 and numgrab=$gra group by clicos");	
			}
			$data = array();$tk=0;$tv=0;$td=0;$ts=0;$gra='';$tg=0;$tkg=0;$tvg=0;$tdg=0;$tsg=0;$granjas=0;
			if($queryvg->num_rows()>0){
			 foreach($queryvg->result() as $row):
				if($gra!=$row->nomg){
					if($tg>0){
						$this->db->select('max(idcos)');	
						$result = $this->db->get($this->tabla.'_'.$cic);
						foreach ($result->result() as $rowt):				
							$rowt->nomg="";$rowt->Razon="Total Granja";
							$rowt->kgsvta=number_format($tkg, 0, '.', ','); 
							if($tvg>0) $rowt->impvta='$ '.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
							if($tdg>0) $rowt->depvta='$ '.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
							$rowt->salvta='$ '.number_format($tsg, 2, '.', ',');
							$data[] = $rowt;	
						endforeach;
					   $tkg=0;$tvg=0;$tdg=0;$tsg=0;
					}	
					$tg=1;$gra=$row->nomg;$granjas+=1;
				}else{ //$row->feccos1="";
					$row->nomg="";
				}  
				
				$tkg+=$row->kgsvta;$tvg+=$row->impvta;$tdg+=$row->depvta;$tsg+=$row->impvta-$row->depvta;
				$tk+=$row->kgsvta;$tv+=$row->impvta;$td+=$row->depvta;$ts+=$row->impvta-$row->depvta;
				if($row->impvta-$row->depvta!=0) $row->salvta='$ '.number_format(($row->impvta-$row->depvta), 2, '.', ','); else $row->salvta='';
				$row->salvta1=$row->impvta-$row->depvta;
				$row->kgsvta=number_format(($row->kgsvta), 0, '.', ','); 
				if($row->impvta>0) $row->impvta='$ '.number_format(($row->impvta), 2, '.', ','); else $row->impvta=''; 
				if($row->depvta>0) $row->depvta='$ '.number_format(($row->depvta), 2, '.', ','); else $row->depvta=''; 
				$data[] = $row;
			 endforeach;
			 if($tg>0 and $granjas>1){
						$this->db->select('max(idcos)');	
						$result = $this->db->get($this->tabla.'_'.$cic);
						foreach ($result->result() as $rowt):				
							$rowt->nomg="";$rowt->Razon="Total Granja";
							$rowt->kgsvta=number_format($tkg, 0, '.', ','); 
							if($tvg>0) $rowt->impvta='$ '.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
							if($tdg>0) $rowt->depvta='$ '.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
							$rowt->salvta='$ '.number_format($tsg, 2, '.', ',');
							$data[] = $rowt;	
						endforeach;
					   $tkg=0;$tvg=0;$tdg=0;$tsg=0;
					}	
			 //total granja
			/* if($granjas>1){
			 	$this->db->select('max(idc)');	
				$result = $this->db->get($this->tabla);
				foreach ($result->result() as $rowt):				
					$rowt->nomg="";$rowt->nomc="Total Granja";
					$rowt->kgsvta=number_format($tkg, 0, '.', ','); 
					if($tvg>0) $rowt->impvta='$ '.number_format($tvg, 2, '.', ','); else $rowt->impvta='';
					if($tdg>0) $rowt->depvta='$ '.number_format($tdg, 2, '.', ','); else $rowt->depvta='';
					$rowt->salvta='$ '.number_format($tsg, 2, '.', ',');
					$data[] = $rowt;	
				endforeach;
			 }	*/
			 //total gral
			 	$this->db->select('max(idcos)');	
				$result = $this->db->get($this->tabla.'_'.$cic);
				foreach ($result->result() as $row):				
					$row->nomg="Gran Total";$row->Razon="";
					$row->kgsvta=number_format($tk, 0, '.', ','); 
					if($tv>0) $row->impvta='$ '.number_format($tv, 2, '.', ','); else $row->impvta='';
					if($td>0) $row->depvta='$ '.number_format($td, 2, '.', ','); else $row->depvta='';
					$row->salvta='$ '.number_format($ts, 2, '.', ','); 
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}	
		
		public function actualizar($id,$fac,$avis,$cic){
			$this->db->select("idcos");
			$this->db->where('folio',$id);
			$resultb = $this->db->get($this->tabla.'_'.$cic);
			foreach($resultb->result() as $rowb):
				$data=array($this->factura=>$fac,$this->aviso=>$avis);
				$this->db->where($this->idcos,$rowb->idcos);
				$this->db->update($this->tabla.'_'.$cic,$data);
			endforeach;	
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
		function getVentas($filter,$cic){
			//SELECT fecsal,folsal,noma,nomc,nomt,cajsal,presal,kgssal,idsal from almacenes inner join( clientes inner join ( tallas inner join salidas on talsal=idt) on clisal=idc )on almsal=ida where estsal=1
			
			$this->db->select('fecsal,folsal,almsal,noma,consala,clisal,nomc,conc,consalc,idsal,estsal,nomt,cajsal,presal,kgssal,prekgs,(kgssal*prekgs) as impvta,facsal,aviso');
			$this->db->join('almacenes', 'ida=almsal','inner');
			$this->db->join('clientes', 'idc=clisal','inner');
			$this->db->join('tallas', 'idt=talsal','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecsal','DESC');
			$this->db->order_by('folsal','DESC');
			//$this->db->where('estsal','1');
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array();$cic='';$alm='';$gra='';$tk=0;$ti=0;
			if($result->num_rows()>0){
				$feci=new Libreria();
			 foreach($result->result() as $row):
				 $cli='';
				 $row->fecsal1=$feci->fecha23($row->fecsal);
				 $row->folsal=str_pad($row->folsal, 5, "0", STR_PAD_LEFT);
				 if($row->estsal==1){$row->estatus='Venta';} else {
				 	$row->estatus='Traslado';
					//busca el almacen del traslado y se le llamara nomc
					$this->db->select('noma as nomc');	
					$this->db->where('ida',$row->clisal);
					$resulta = $this->db->get('almacenes');
					foreach ($resulta->result() as $rowa):
						$cli=$rowa->nomc;
					endforeach;	
					$row->nomc=$cli;
				} 
				 if($row->prekgs>0){
				 	 $row->prekgs1='$ '.number_format(($row->prekgs), 2, '.', ',');
					 $ti+=$row->kgssal*$row->prekgs;
				 	 $row->impvta='$ '.number_format(($row->kgssal*$row->prekgs), 2, '.', ',');
					 
				 } else {$row->prekgs1='';$row->impvta='';$row->prekgs='';}
				$tk+=$row->kgssal;
				 $row->kgssal=number_format(($row->kgssal), 0, '.', ',');
				$row->presal=$row->presal.' kgs'; 			 
				if($row->facsal==0) $row->facsal='';  //else $row->facsal=str_pad($row->facsal, 4, "0", STR_PAD_LEFT);
				if($row->aviso==0) $row->aviso='';
				$data[] = $row;
			 endforeach;
			//total gral
			 	$this->db->select('max(idc)');	
				$result = $this->db->get($this->tabla.'_'.$cic);
				foreach ($result->result() as $row):				
					$row->fecsal1="";$row->folsal="";$row->noma="";$row->nomc="";$row->nomt="";$row->facsal="";$row->aviso="";
					if($tk>0) $row->kgssal='kgs. '.number_format($tk, 0, '.', ',');  else $row->kgssal='';
					if($tk>0 && $ti>0) $row->prekgs1='$ '.number_format($ti/$tk, 2, '.', ','); else $row->prekgs1='';
					if($ti>0) $row->impvta='$ '.number_format($ti, 2, '.', ','); else $row->impvta='';
					 
					$data[] = $row;	
				endforeach;
			}
			
			return $data;
		}
    }
?>