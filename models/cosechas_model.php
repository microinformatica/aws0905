<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Cosechas_model extends CI_Model {        
        public $idcos="idcos";public $feccos="feccos";public $estcos="estcos";public $grscos="grscos";public $kgscos="kgscos";
        public $numcos="numcos";public $clicos="clicos";public $tipcos="tipcos";public $prebas="prebas";public $ciccos="ciccos";
        public $tabla="cosechas_22";public $tablac="cosechas_";
		
		public $idpis="idpis";
		public $tablaest="siegra_22";public $tablaestc="siegra_";		
		
		public $idpischa="idpischa";
		public $tablacha="chagra_22";public $tablachac="chagra_";
				
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getElementsb($where,$ciclo){
			$this->db->select("idpis,pisg as val");   
			$this->db->where('cicg',$ciclo);    
        	$this->db->where('numgra',4);
			$result = $this->db->get($this->tablaest);        
			        
        	/*$this->db->select("idpis,pisg as val");       
        	$this->db->where('numgra',4);
			$result = $this->db->get($this->tablaest);*/
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function gral($filter,$ano){
			//SELECT	pisg,orgg,hasg,sum(kgscos) as kgs, avg(grscos) as pp from siegra_18 inner join cosechas on estcos=idpis where numcos=1 group by estcos
			$this->db->select("idpis,pisg,orgg,hasg");
			$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->group_by('idpis');      
			$this->db->order_by('pisg');		
			$result = $this->db->get($this->tablac.$ano);
			$data = array(); $totorg=0;$tothas=0;$totali=0;$totpri=0;$totseg=0;$totter=0;$totcua=0;$totqui=0;$totsex=0;
			$totfin=0;$tottot=0;$totpro=0;$cont=0;$totorgs=0;
			$totsobs=0;$totcs=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$est=$row->idpis;$orgg=$row->orgg;$totorg+=$row->orgg;$tothas+=$row->hasg;$tse=0;$ose=$row->orgg; 
				/*if($row->idpis>=73 && $row->idpis<=91) $row->pisg='I-'.$row->pisg;
					elseif($row->idpis>=92 && $row->idpis<=111) $row->pisg='II-'.$row->pisg;
						elseif($row->idpis>=112 && $row->idpis<=128) $row->pisg='III-'.$row->pisg;
							elseif($row->idpis>=129 && $row->idpis<=149) $row->pisg='IV-'.$row->pisg;*/
				//if($row->idpis>=73 && $row->idpis<=99) $row->pisg='I-'.$row->pisg;
				//	elseif($row->idpis>=112 && $row->idpis<=128) $row->pisg='II-'.$row->pisg;
				//		elseif(($row->idpis>=100 && $row->idpis<=111) || ($row->idpis>=129 && $row->idpis<=149)) $row->pisg='III-'.$row->pisg;		
				//if($row->idpis>=73 && $row->idpis<=91) $row->pisg='I-'.$row->pisg;
				//	elseif($row->idpis>=92 ) $row->pisg='II-'.$row->pisg;	
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				$row->hasg = number_format($row->hasg, 3, '.', ',');
				//sumo el total del alimento actual consumido
				$this->db->select("sum(kgt) as ali");
				$this->db->join($this->tablachac.$ano, $this->idpischa.'='.$this->idpis,'inner'); 
				//$this->db->where($this->numcos,4);
				$this->db->where($this->idpischa,$est); 
				$resulta = $this->db->get($this->tablaestc.$ano);
				foreach($resulta->result() as $rowa):
					$totali+=$rowa->ali;
					if($rowa->ali>0){$row->ali =number_format($rowa->ali, 0, '.', ',');}else{$row->ali ='';}					
				endforeach;
				//busco los de la primera precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,1);
				$this->db->where($this->estcos,$est); 
				$result1 = $this->db->get($this->tablac.$ano);
				foreach($result1->result() as $row1):
					$totpri+=$row1->kgs;
					if($row1->pp>0){$tse+=($row1->kgs/($row1->pp/1000))/($ose*1000);}
					if($row1->kgs>0){$row->kgs1 =number_format($row1->kgs, 0, '.', ',');}else{$row->kgs1 ='';}
					if($row1->pp>0){$row->pp1 =number_format($row1->pp, 2, '.', ',');}else{$row->pp1 ='';}					
				endforeach;
				//busco los de la segunda precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,2);
				$this->db->where($this->estcos,$est); 
				$result2 = $this->db->get($this->tablac.$ano);
				foreach($result2->result() as $row2):
					$totseg+=$row2->kgs;
					if($row2->pp>0){$tse+=($row2->kgs/($row2->pp/1000))/($ose*1000);}
					if($row2->kgs>0){$row->kgs2 =number_format($row2->kgs, 0, '.', ',');}else{$row->kgs2 ='';}
					if($row2->pp>0){$row->pp2 =number_format($row2->pp, 2, '.', ',');}else{$row->pp2 ='';}
				endforeach;
				//busco los de la tercera precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,3);
				$this->db->where($this->estcos,$est); 
				$result3 = $this->db->get($this->tablac.$ano);
				foreach($result3->result() as $row3):
					$totter+=$row3->kgs;
					if($row3->pp>0){$tse+=($row3->kgs/($row3->pp/1000))/($ose*1000);}
					if($row3->kgs>0){$row->kgs3 =number_format($row3->kgs, 0, '.', ',');}else{$row->kgs3 ='';}
					if($row3->pp>0){$row->pp3 =number_format($row3->pp, 2, '.', ',');}else{$row->pp3 ='';}
				endforeach;
				//busco los de la cuarta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,4);
				$this->db->where($this->estcos,$est); 
				$result4 = $this->db->get($this->tablac.$ano);
				foreach($result4->result() as $row4):
					$totcua+=$row4->kgs;
					if($row4->pp>0){$tse+=($row4->kgs/($row4->pp/1000))/($ose*1000);}
					if($row4->kgs>0){$row->kgs4 =number_format($row4->kgs, 0, '.', ',');}else{$row->kgs4 ='';}
					if($row4->pp>0){$row->pp4 =number_format($row4->pp, 2, '.', ',');}else{$row->pp4 ='';}
				endforeach;
				//busco los de la quinta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,5);
				$this->db->where($this->estcos,$est); 
				$result5 = $this->db->get($this->tablac.$ano);
				foreach($result5->result() as $row5):
					$totqui+=$row5->kgs;
					if($row5->pp>0){$tse+=($row5->kgs/($row5->pp/1000))/($ose*1000);}
					if($row5->kgs>0){$row->kgs5 =number_format($row5->kgs, 0, '.', ',');}else{$row->kgs5 ='';}
					if($row5->pp>0){$row->pp5 =number_format($row5->pp, 2, '.', ',');}else{$row->pp5 ='';}
				endforeach;	
				//busco los de la sexta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,6);
				$this->db->where($this->estcos,$est); 
				$result6 = $this->db->get($this->tablac.$ano);
				foreach($result6->result() as $row6):
					$totsex+=$row6->kgs;
					if($row6->pp>0){$tse+=($row6->kgs/($row6->pp/1000))/($ose*1000);}
					if($row6->kgs>0){$row->kgs6 =number_format($row6->kgs, 0, '.', ',');}else{$row->kgs6 ='';}
					if($row6->pp>0){$row->pp6 =number_format($row6->pp, 2, '.', ',');}else{$row->pp6 ='';}
				endforeach;
				//busco los de la final precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,7);
				$this->db->where($this->estcos,$est); 
				$resultf = $this->db->get($this->tablac.$ano);
				foreach($resultf->result() as $rowf):
					$totfin+=$rowf->kgs;
					if($rowf->pp>0){$tse+=($rowf->kgs/($rowf->pp/1000))/($ose*1000);}
					if($rowf->kgs>0){$row->kgsf =number_format($rowf->kgs, 0, '.', ',');}else{$row->kgsf ='';}
					if($rowf->pp>0){$row->ppf =number_format($rowf->pp, 2, '.', ',');}else{$row->ppf ='';}
				endforeach;
				//busco los del total precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp");
				$this->db->join($this->tablaestc.$ano, $this->idpis.'='.$this->estcos,'inner'); 
				//$this->db->where($this->numcos,4);
				$this->db->where($this->estcos,$est); 
				$resultt = $this->db->get($this->tablac.$ano);
				foreach($resultt->result() as $rowt):
					if($rowt->kgs>0){
						$tottot+=$rowt->kgs;
						$totorgs+=($rowt->kgs*1000)/$rowt->pp;
						$kgst=($rowt->kgs*1000)/$rowt->pp;
						$row->fcat =number_format($rowa->ali/$rowt->kgs, 2, '.', ',');
						//$row->fcat =number_format(($rowa->ali/(($row5->kgs*1000)/$row5->pp))*10, 2, '.', ',');
						$row->kgsha =number_format($rowt->kgs/$row->hasg, 0, '.', ',');	
						$row->orgst =number_format(($rowt->kgs*1000)/$rowt->pp, 0, '.', ',');	
						$row->kgst =number_format($rowt->kgs, 0, '.', ',');
						//$row->sob =number_format(($kgst/($orgg*1000))*100, 2, '.', ',');
						if($tse>0){$row->sob =number_format(($tse*100), 2, '.', ',');$totsobs+=$tse;$totcs+=1;} else $row->sob ='';
						
					}else{$row->kgst ='';$row->kgsha ='';$row->fca ='';$row->orgst ='';$row->sob ='';}
					if($rowt->pp>0){$cont+=1;$totpro+=$rowt->pp;$row->ppt =number_format($rowt->pp, 2, '.', ',');}else{$row->ppt ='';}
				endforeach;
				$data[] = $row;
			endforeach;
			//total
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tablac.$ano);
			foreach ($result->result() as $row):				
				$row->pisg = "Total";
				if($totorg>0)$row->orgg = number_format($totorg, 3, '.', ','); else $row->orgg ='';
				if($tothas>0)$row->hasg =number_format($tothas,3, '.', ','); else $row->hasg ='';
				if($totali>0)$row->ali =number_format($totali, 0, '.', ','); else $row->ali =''; 
				if($totpri>0)$row->kgs1 =number_format($totpri, 0, '.', ','); else $row->kgs1 ='';
				$row->pp1='';
				if($totseg>0)$row->kgs2 =number_format($totseg, 0, '.', ','); else $row->kgs2 ='';
				$row->pp2='';
				if($totter>0)$row->kgs3 =number_format($totter, 0, '.', ','); else $row->kgs3 ='';
				$row->pp3='';
				if($totcua>0)$row->kgs4 =number_format($totcua, 0, '.', ','); else $row->kgs4 ='';
				$row->pp4='';
				if($totqui>0)$row->kgs5 =number_format($totqui, 0, '.', ','); else $row->kgs5 ='';
				$row->pp5='';
				if($totsex>0)$row->kgs6 =number_format($totsex, 0, '.', ','); else $row->kgs6 ='';
				$row->pp6='';
				if($totfin>0)$row->kgsf =number_format($totfin, 0, '.', ','); else $row->kgsf ='';
				$row->ppf='';
				if($tottot>0)$row->kgst =number_format($tottot, 0, '.', ','); else $row->kgst ='';
				if($totpro>0)$row->ppt =number_format($totpro/$cont, 2, '.', ','); else $row->ppt ='';
				if($tottot>0)$row->kgsha =number_format($tottot/$tothas, 1, '.', ','); else $row->kgsha ='';
				if($totali>0)$row->fcat =number_format($totali/$tottot, 2, '.', ','); else $row->fcat ='';
				//if($totali>0)$row->fcat =number_format(($totali/$totorgs)*10, 2, '.', ','); else $row->fcat ='';
				if($totorgs>0)$row->orgst =number_format($totorgs, 0, '.', ','); else $row->orgst ='';
				//if($totorgs>0)$row->sob =number_format(($totorgs/($totorg*1000))*100, 2, '.', ','); else $row->sob ='';
				if($totsobs>0)$row->sob =number_format(($totsobs/$totcs)*100, 2, '.', ','); else $row->sob ='';
				$data[] = $row;	
			endforeach;
			}
			return $data;			
		}

// para el general en precios y kilos de venta
//SELECT	feccos, avg(grscos) as grs, sum(kgscos) kgs, sum((grscos+prebas)*kgscos) as imp from cosechas group by feccos
		function cosechas($filter){
			//select feccos,pisg,grscos,kgscos,clicos,numcos,tipcos from siegra_18 inner join cosechas on estcos=idpis
			$this->db->select("idcos,feccos,pisg,grscos,kgscos,clicos,numcos,tipcos,estcos,prebas,ciccos");
			$this->db->join($this->tablaest, $this->idpis.'='.$this->estcos,'inner');       
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tabla);
			$data = array();  $fec=new Libreria(); $dia=''; $maq=''; $cos=''; $cli='';$tot=0;$totimp=0;$totg=0;$totgr=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($dia!=$row->feccos){
					if($tot>0){
						$this->db->select('max(idcos)');	
						$result = $this->db->get($this->tabla);
						foreach ($result->result() as $rowt):				
							$rowt->clicos1 = ""; 
							$rowt->feccos1="Total";$rowt->tipcos1="";$rowt->numcos1="";$rowt->pisg=""; $rowt->grscos='';$rowt->prebas1='';$rowt->pregra='';
							if($tot>0) $rowt->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $rowt->kgscos='';
							$data[] = $rowt;	
						endforeach;
					   $tot=0;$maq=''; $cos=''; $cli='';
					}	
					$row->feccos1=$fec->fecha($row->feccos);$dia=$row->feccos;
				}else{ $row->feccos1="";}  
				if($maq!=$row->tipcos){
					switch($row->tipcos){
						case 1: $row->tipcos1='Maquila'; break;	case 2: $row->tipcos1='Venta'; break; case 3: $row->tipcos1='Donación'; break;					
					}
					$maq=$row->tipcos;
				}else{ $row->tipcos1="";}
				if($cos!=$row->numcos){
					switch($row->numcos){
						case 1: $row->numcos1='Primera'; break;
						case 2: $row->numcos1='Segunda'; break;	
						case 3: $row->numcos1='Tercera'; break;	
						case 4: $row->numcos1='Cuarta'; break;
						case 5: $row->numcos1='Quinta'; break;
						case 6: $row->numcos1='Sexta'; break;
						case 7: $row->numcos1='Final'; break;
					}
					$cos=$row->numcos;
				}else{ $row->numcos1="";}	
				if($cli!=$row->clicos){
					$row->clicos1=$row->clicos;$cli=$row->clicos;
				}else{ $row->clicos1="";} 	
				$tot+=$row->kgscos;$totimp+=($row->grscos+$row->prebas)*$row->kgscos;$totg+=1;$totgr+=$row->grscos;
				$row->pregra='$ '.number_format($row->grscos+$row->prebas, 2, '.', ',');
				$row->grscos=number_format($row->grscos, 2, '.', ',');
				$row->imp='$ '.number_format(($row->grscos+$row->prebas)*$row->kgscos, 2, '.', ',');
				if($row->prebas>0){ $row->prebas1='$ '.number_format($row->prebas, 2, '.', ',');} else {$row->prebas='';$row->prebas1='';}
				$row->kgscos=number_format($row->kgscos, 2, '.', ',');
				$data[] = $row;
			endforeach;
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla);
			foreach ($result->result() as $row):				
				$row->clicos1 = ""; 
				$row->feccos1="Total";$row->tipcos1="";$row->numcos1="";$row->pisg=""; $row->grscos='';$row->prebas1='';$row->pregra='';$row->ciccos='';
				if($totgr>0) $row->grscos=number_format($totgr/$totg, 2, '.', ','); else $row->grscos='';
				if($tot>0) $row->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $row->kgscos='';
				if($totimp>0) $row->imp='$ '.number_format($totimp, 2, '.', ','); else $row->imp='';
				$data[] = $row;	
			endforeach;
			}
			return $data;			
		}
		public function borrarcos($id){
			$this->db->where($this->idcos,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		
		function agregacos($fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic){
			$kgs = str_replace(",", "", $kgs);
			$data=array($this->feccos=>$fec,$this->tipcos=>$tip,$this->numcos=>$num,$this->clicos=>$cli,$this->estcos=>$est,$this->grscos=>$grs,
						$this->kgscos=>$kgs,$this->prebas=>$pre,$this->ciccos=>$cic);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		function actualizacos($id,$fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic){
			$kgs = str_replace(",", "", $kgs);
			$data=array($this->feccos=>$fec,$this->tipcos=>$tip,$this->numcos=>$num,$this->clicos=>$cli,$this->estcos=>$est,$this->grscos=>$grs,
						$this->kgscos=>$kgs,$this->prebas=>$pre,$this->ciccos=>$cic);
			$this->db->where($this->idcos,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
			
		}
		
		function verDepto(){			
			$this->db->order_by('NomDep');			
			$query=$this->db->get('departamento');
			return $query->result();			
		}
			
    }
    
?>
