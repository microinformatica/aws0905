<?php
    class Estadistica_model extends CI_Model{
    	public $numero="Numero";
		public $razon="Razon";
		public $saldo="Saldo";
		public $fecud="FecUD";
		public $impud="ImpUD";
		public $obsud="ObsUD";
		public $si="SI";
		public $zona="Zona";
		public $tabla="clientes";
		var $today;
		function __Construct(){
			parent::__construct();
			$this->load->database();
			$this->today=date('Y-m-d');
		}	
		function zonas(){									
			$this->db->select('Zona, SUM(Saldo) as saldo');
			$this->db->where('Numero >',1);
			$this->db->where('Saldo >',0);
			$this->db->where('SI',0);
			$this->db->where('Zona !=','Varias');	
			$this->db->group_by('Zona');
			$this->db->order_by('SUM(Saldo)','desc');					
			$query = $this->db->get($this->tabla);			
			return $query->result();
		}				
		function zonaseleccionada($zonas){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);	
			$this->db->where('Saldo >',0);
			$this->db->where('Zona =',$zonas);
			$this->db->order_by('Saldo','desc');
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
		public function getClienteEst($id){
			$this->db->select('Numero,Razon,Saldo,FecUD,ImpUD,ObsUD,SI,Zona,DATEDIFF( CURDATE(),FecUD ) AS diferencia',NULL,FALSE);			
			$this->db->where('Numero',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		public function actualizar($id,$rblugar,$obs){
			$data=array($this->si=>$rblugar,$this->obsud=>$obs);
			$this->db->where($this->numero,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
    }
?>