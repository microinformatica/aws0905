<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Produccion_model extends CI_Model {
        public $id="NumPila";public $fec="FecSie";public $pl="PL";public $can="CanSie";public $sal="Sala";public $vacio="Cancelacion";
		public $plact="ActPL";public $ori="Origen";
		public $tablaras="raceways";
		public $tablalar="larvario";
		public $fech="FechaH";public $nph="NumPilaH";public $canh="CanSieH";public $placth="ActPLH";
		public $tablahis="historialrcw";
        
		public $idc="nse";public $pil="pil";public $fecc="fec";public $pls="pls";public $est="est";public $cod="cod";
		public $p1="p1";public $p2="p2";public $p3="p3";public $p4="p4";public $nau="nau";public $mad="mad";public $oric="ori";public $sala="salas";
		public $tablacod="codI_15";public $tablacod2="codII_15";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		public function agregarcod($pil,$fec,$pls,$est,$p1,$p2,$p3,$p4,$nau,$mad,$ori,$cod,$sala,$tabla){
			$can1=$pls;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			$pls =$can1/1000;
			$can1=$nau;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			$nau =$can1/1000;
			if($p1=='' && $p2=='' && $p3=='' && $p4=='' && $mad=='' && $ori=='') $cod='';
			if($tabla==1) $tabla=$this->tablacod; else $tabla=$this->tablacod2;
			$data=array($this->pil=>$pil,$this->fecc=>$fec,$this->pls=>$pls,$this->est=>$est,$this->p1=>$p1,$this->p2=>$p2,$this->p3=>$p3,$this->p4=>$p4,$this->nau=>$nau,$this->mad=>$mad,$this->oric=>$ori,$this->cod=>$cod,$this->sala=>$sala);			
			$this->db->insert($tabla,$data);
			return $this->db->insert_id();
		}
		function actualizarcod($idc,$pil,$fec,$pls,$est,$p1,$p2,$p3,$p4,$nau,$mad,$ori,$cod,$sala,$tabla){
			$can1=$pls;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			$pls =$can1/1000;
			$can1=$nau;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			$nau =$can1/1000;
			if($p1=='' && $p2=='' && $p3=='' && $p4=='' && $mad=='' && $ori=='') $cod='';
			if($tabla==1) $tabla=$this->tablacod; else $tabla=$this->tablacod2;
	 		$data=array($this->pil=>$pil,$this->fecc=>$fec,$this->pls=>$pls,$this->est=>$est,$this->p1=>$p1,$this->p2=>$p2,$this->p3=>$p3,$this->p4=>$p4,$this->nau=>$nau,$this->mad=>$mad,$this->oric=>$ori,$this->cod=>$cod,$this->sala=>$sala);
			$this->db->where($this->idc,$idc);
			$this->db->update($tabla,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function getCod($filter){
			//Select NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF(FecSie, '".$hoy."') AS diferencia FROM raceways where NumPila='".$npil."'";
			//$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,Sala,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia,Origen');
			//$this->db->where($filter['where']);
			$this->db->join($this->tablaras, $this->id.'='.$this->pil,'inner');
			$this->db->order_by('nse','DESC');
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablacod);
			$data = array();$fec=new Libreria();
			foreach ($result->result() as $row):				
				$row->fec1 = $fec->fecha($row->fec);
				if($row->est<=0) $row->est='';
				if($row->pls>0) $row->pls = number_format($row->pls, 3, '.', ','); else $row->pls='';
				if($row->nau>0) $row->nau = number_format($row->nau, 3, '.', ','); else $row->nau='';
				$row->pilis=$row->p1.$row->p2.$row->p3.$row->p4;
				$data[] = $row;	
			endforeach;	
			
			return $data;
		}
		function getNumRowsCod($filter){
			//$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia');
			//$this->db->where($filter['where']);
			$this->db->join($this->tablaras, $this->id.'='.$this->idc,'inner');
			$result = $this->db->get($this->tablacod);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getCod2($filter){
			//Select NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF(FecSie, '".$hoy."') AS diferencia FROM raceways where NumPila='".$npil."'";
			//$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,Sala,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia,Origen');
			//$this->db->where($filter['where']);
			$this->db->join($this->tablaras, $this->id.'='.$this->pil,'inner');
			$this->db->order_by('nse','DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablacod2);
			$data = array();$fec=new Libreria();
			foreach ($result->result() as $row):				
				$row->fec1 = $fec->fecha($row->fec);
				if($row->est<=0) $row->est='';
				if($row->pls>0) $row->pls = number_format($row->pls, 3, '.', ','); else $row->pls='';
				if($row->nau>0) $row->nau = number_format($row->nau, 3, '.', ','); else $row->nau='';
				$row->pilis=$row->p1.$row->p2.$row->p3.$row->p4;
				$data[] = $row;	
			endforeach;	
			
			return $data;
		}
		function getNumRowsCod2($filter){
			//$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia');
			//$this->db->where($filter['where']);
			$this->db->join($this->tablaras, $this->id.'='.$this->idc,'inner');
			$result = $this->db->get($this->tablacod2);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		function getRas($filter){
			$hoy=$filter['num'];
			//Select NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF(FecSie, '".$hoy."') AS diferencia FROM raceways where NumPila='".$npil."'";
			$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,Sala,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia,Origen');
			$this->db->where($filter['where']);
			$result = $this->db->get($this->tablaras);
			$data = array();
			foreach ($result->result() as $row):				
				if($row->Cancelacion==0){
					$row->PL1=$row->PL+abs($row->diferencia);$plact=$row->PL+abs($row->diferencia);
					//actualizar el Pl de las agrupaciones
					//$sqlConsulta="update raceways set ActPL='$d1' where NumPila='".$npil."'";
					//$result=consultaSQL($sqlConsulta,$conexion);
					$numpila=$row->NumPila;
					$data1=array($this->plact=>$plact);
					$this->db->where($this->id,$numpila);
					$this->db->update($this->tablaras,$data1);$dia=0;
					$dia=abs($row->diferencia)+1; //$dia+=1;
					$row->PL11="PL ".($row->PL1+1);
					$row->PL1="PL ".$row->PL1;
					
					if($dia==4){ $row->tra='Cloramina T 5 ppm';$row->tra1='Cloramina T 5 ppm';}
					if($dia==2 || $dia==10 || $dia==18 || $dia==26) {$row->tra='Oxi 8 ppm';$row->tra1='Oxi 8 ppm';}
					if($dia==6 || $dia==14 || $dia==22) {$row->tra='Enroqueen 6 ppm';$row->tra1='Enroqueen 6 ppm';}
					$row->CanSie = number_format($row->CanSie, 1, '.', ',');$row->CanSie1 = number_format($row->CanSie, 1, '.', ',');
					if($row->Sala==0) $row->Sala='';
				}else{
					$row->PL1="";$row->CanSie="";$row->Sala='';
					$row->PL11="";$row->CanSie1="";$row->Sala='';
				}
				
				$data[] = $row;	
			endforeach;	
			
			return $data;
		}
		function getNumRowsR($filter){
			$this->db->select('NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia');
			$this->db->where($filter['where']);
			$result = $this->db->get($this->tablaras);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getRasA($filter){
			//select ActPL, sum(CanSie) as totpl from raceways where CanSie>0 group by ActPL
			$this->db->select('ActPL, sum(CanSie) as totpl');
			$this->db->where('Cancelacion =',0);
			$this->db->group_by('ActPL');
			$result = $this->db->get($this->tablaras);
			$data = array(); $tot=0;
			foreach ($result->result() as $row):				
				$row->PL1="PL ".$row->ActPL;
				$tot+=$row->totpl;
				$row->totpl = number_format($row->totpl, 1, '.', ',');
				$data[] = $row;	
			endforeach;	
			$this->db->select('max(CanSie)');
			$resultt = $this->db->get($this->tablaras);
			foreach ($resultt->result() as $rowt):				
				$rowt->PL1="Tot:";
				$rowt->totpl = number_format($tot, 1, '.', ',');
				$data[] = $rowt;	
			endforeach;
			return $data;
		}
		function getNumRowsRA($filter){
			//select ActPL, sum(CanSie) as totpl from raceways where CanSie>0 group by ActPL
			$this->db->select('ActPL, sum(CanSie) as totpl');
			$this->db->where('Cancelacion =',0);
			$this->db->group_by('ActPL');
			$result = $this->db->get($this->tablaras);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getRasAT($filter){
			$cont=1;$data = array();$tot=0;
			while($cont<=4){
				if($cont==1){ $ini=1; $fin=40; $ras="Exteriores I";}	
				if($cont==2){ $ini=41; $fin=60; $ras="Exteriores II";}
				if($cont==3){ $ini=61; $fin=78; $ras="Genética";}
				if($cont==4){ $ini=87; $fin=90; $ras="Reservas";}
				//if($cont==5){ $ini2=91; $fin2=106;} //$ras="Reservas";}
				$this->db->select('sum(CanSie) as totpl');
				if($cont==2){
				$arr="CanSie >0 and((NumPila >= $ini and NumPila <=$fin) or (NumPila >= 91 and NumPila <=110))";	
				}
				else{
				$arr=array('CanSie >' => 0,'NumPila >=' => $ini,'NumPila <=' => $fin);
				}
				$this->db->where($arr);
				//if($cont==2){$this->db->where('NumPila >=',91);$this->db->where('NumPila <=',106);}
				$result = $this->db->get($this->tablaras);
				foreach ($result->result() as $row):				
					$row->PL1=$ras;$tot+=$row->totpl;
					if($row->totpl>0){$row->totpl = number_format($row->totpl, 1, '.', ',');}else{$row->totpl ="";}
					$data[] = $row;	
				endforeach;	
				$cont+=1;
			}
			$this->db->select('max(CanSie)');
			$resultt = $this->db->get($this->tablaras);
			foreach ($resultt->result() as $rowt):				
				$rowt->PL1="Total:";
				$rowt->totpl = number_format($tot, 1, '.', ',');
				$data[] = $rowt;	
			endforeach;
			//total por origen 
			//SELECT sum(CanSie) as total, (select sum(CanSie) from raceways where Origen='Aquapacific') as mx, (select sum(CanSie) from raceways where Origen='Ecuatoriana') as ec from raceways where Cancelacion=0
			$this->db->select('sum(CanSie) as mx');
			$this->db->where('Origen ','Aquapacific');	
			$resultt = $this->db->get($this->tablaras);
			foreach ($resultt->result() as $rowt):				
				$rowt->PL1="Aquapacific:";
				$rowt->totpl = number_format($rowt->mx, 1, '.', ',');
				$data[] = $rowt;	
			endforeach;
			$this->db->select('sum(CanSie) as mx');
			$this->db->where('Origen ','Ecuatoriana');	
			$resultt = $this->db->get($this->tablaras);
			foreach ($resultt->result() as $rowt):				
				$rowt->PL1="Ecuatoriana:";
				$rowt->totpl = number_format($rowt->mx, 1, '.', ',');
				$data[] = $rowt;	
			endforeach;
			return $data;
		}
		function getNumRowsRAT($filter){
			$cont=4;
			return $cont;
		}
		function actualizar($id,$fec,$pl,$can,$vacio,$sal,$ori){
	 		//$hoy=date("y-m-d H:i:s");
	 		if($vacio==-1){ $can=0;$pl="";$fec=date("Y/m/d");$sal=0;$ori="";}
			$data=array($this->fec=>$fec,$this->pl=>$pl,$this->can=>$can,$this->vacio=>$vacio,$this->sal=>$sal,$this->ori=>$ori);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablaras,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getLar($filter){
			$hoy=$filter['num'];
			$datos [0]="N5";$datos [1]="Z1";$datos [2]="Z2";$datos [3]="Z3";$datos [4]="Z3-M1";$datos [5]="M1";$datos [6]="M2";$datos [7]="M3";
			$datos [8]="M3-PL";$num_datos = count($datos);//$num_datos = 9;
			//Select NumPila,PL,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF(FecSie, '".$hoy."') AS diferencia FROM raceways where NumPila='".$npil."'";
			$this->db->select('NumPila,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia');
			$this->db->where($filter['where']);
			$result = $this->db->get($this->tablalar);
			$data = array();$d1=0;$d2=0;
			foreach ($result->result() as $row):				
				if($row->Cancelacion==0){
					$d1=abs($row->diferencia);
					if($d1>8) { $d2=($d1-8); } else{ if($d1==8) { $d1=8; }} if($d1==0) { $d1=0; }
					if(($d1)<$num_datos){ $row->PL1=$datos [$d1];} else { $row->PL1="PL ".$d2; }
					//$row->PL1=$row->PL1;
					if($row->CanSie>0){$row->CanSie = number_format($row->CanSie, 1, '.', ',');} else { $row->CanSie="";}
				}else{
					$row->PL1="";$row->CanSie="";
				}
				$data[] = $row;	
			endforeach;	
			return $data;
		}
		function getNumRowsL($filter){
			$this->db->select('NumPila,Nombre,Cancelacion,CanSie,FecSie,DATEDIFF( CURDATE(),FecSie ) AS diferencia');
			$this->db->where($filter['where']);
			$result = $this->db->get($this->tablalar);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function actualizarl($id,$fec,$can,$vacio){
	 		//$hoy=date("y-m-d H:i:s");
	 		if($vacio==-1){ $can=0;$fec=date("Y/m/d");	}
			$data=array($this->fec=>$fec,$this->can=>$can,$this->vacio=>$vacio);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablalar,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getLarsie($filter){
			//$hoy=$filter['num'];
			//select FecSie,Nombre,CanSie,Sala from larvario where Cancelacion=0 group by FecSie DESC,Nombre DESC
			$this->db->select('FecSie,Nombre,CanSie,Sala');
			$this->db->where('Cancelacion =', 0);
			$this->db->order_by('FecSie', 'desc');
			$this->db->order_by('Nombre', 'desc');
			$result = $this->db->get($this->tablalar);
			$data = array();$fec='';$sala='';
			foreach ($result->result() as $row):
				if($fec!=$row->FecSie){$fec=$row->FecSie;$row->FecSie=date("d-m-Y",strtotime($row->FecSie)); $sala="";}else{$row->FecSie="";}
				if($sala!=$row->Sala){$sala=$row->Sala; }else{$row->Sala="";}
				if($row->CanSie>0){$row->CanSie = number_format($row->CanSie, 1, '.', ',');} else { $row->CanSie="";}				
				/*if($row->Cancelacion==0){
					$d1=abs($row->diferencia);
					if($d1>8) { $d2=($d1-8); } else{ if($d1==8) { $d1=8; }} if($d1==0) { $d1=0; }
					if(($d1)<$num_datos){ $row->PL1=$datos [$d1];} else { $row->PL1="PL ".$d2; }
					//$row->PL1=$row->PL1;
					
				}else{
					$row->PL1="";$row->CanSie="";
				}*/
				$data[] = $row;	
			endforeach;	
			return $data;
		}
		function getNumRowsLsie($filter){
			$this->db->select('FecSie,Nombre,CanSie,Sala');
			$this->db->where('Cancelacion =', 0);
			$this->db->group_by('FecSie', 'desc');
			$this->db->group_by('Nombre', 'desc');
			$this->db->group_by('CanSie');
			$this->db->group_by('Sala');
			$result = $this->db->get($this->tablalar);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function history(){
			$hoy=date("y-m-d H:i:s");
			$this->db->select('DATEDIFF( CURDATE(),max(FechaH) ) AS uno');
			$result = $this->db->get($this->tablahis);
			foreach ($result->result() as $row):
				$dia=$row->uno;
			endforeach; 
			if($dia>0){
				$this->db->select('NumPila,CanSie,ActPL');
				$this->db->where('Cancelacion =', 0);
				$result = $this->db->get($this->tablaras);
				foreach ($result->result() as $row):
					$pila=$row->NumPila;$canti=$row->CanSie;$pl=$row->ActPL;
					$data=array($this->fech=>$hoy,$this->nph=>$pila,$this->canh=>$canti,$this->placth=>$pl);			
					$this->db->insert($this->tablahis,$data);
					$this->db->insert_id();
				endforeach;
				return 2;
			}else{ return 1; }
		}	
		public function UltimaAct(){
			$this->db->select('max(FechaH) as ultimaact');
			$query=$this->db->get('historialrcw');
			return $query->result();			
		}
		//Salas
		function verSala(){
			$this->db->select('Sala');
			$this->db->group_by('Sala');
			//$this->db->group_by('NumPila');
			$query=$this->db->get('larvario');
			return $query->result();			
		}
		public function verPila(){
			//$this->db->where('Cancelacion =',0);
			
			$query=$this->db->get('raceways');
			
			return $query->result();			
		}
		function ultimocodigo(){									
			$this->db->select('MAX(nse) as ultimo,MAX(nse) as ultimoR');
			$query = $this->db->get($this->tablacod);
			return $query->result();
		}
		function ultimocodigo2(){									
			$this->db->select('MAX(nse) as ultimo,MAX(nse) as ultimoR');
			$query = $this->db->get($this->tablacod2);
			return $query->result();
		}
		function xxgetElements($where){
			$this->db->select('NumPila as val');			
			//$this->db->select("id, nombre as val");       
        	if($where['nom']!=0)
            	$this->db->where($where);  
			//$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
			//$this->db->where($where['where']);
			//$this->db->group_by('Zona');			
			$result = $this->db->get($this->tablaras);			
	     	$data = array();  
		    foreach($result->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		function getElements($where){        
        	$this->db->select("numpila,numpila as val");       
        	if($where['nom']!='0')
	            $this->db->where($where);            
    	    $result = $this->db->get($this->tablaras);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}				
    }    
?>