<?php
    class Menu_model extends CI_Model{
    	public $clave="clave";
		public $claveperfil="clavePerfil";
		public $modulo="modulo";
		public $liga="liga";
		public $imagen="imagen";
		public $tabla="modulos";
		
		function __Construct(){
			parent::__construct();
			$this->load->database();
		}
		
		function opcionesMenu($perfilsel){						
			$this->db->where($this->claveperfil,$perfilsel);			
			$query = $this->db->get($this->tabla);
			return $query->result();
		}		
    }
?>