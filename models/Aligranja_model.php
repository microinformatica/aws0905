<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Aligranja_model extends CI_Model {
		public $idpis="idpis";public $pisg="pisg";public $hasg="hasg";public $espg="espg";public $orgg="orgg";public $prog="prog";
		public $fecg="fecg";public $plg="plg";public $cicg="cicg";public $fecgc="fecgc";public $biogc="biogc";public $ppgc="ppgc";
		public $obsc="obsc";public $numgra="numgra";public $nfd="nfd";
		public $tabla="siegra_22"; public $tablasie="siegra_";
		    	
        public $idcha="idcha";public $idpischa="idpischa";public $feccha="feccha";public $ciccha="ciccha";
		public $numgracha="numgracha";public $fedcha="fedcha";public $codcha="codcha";public $numcha="numcha";public $modcha="modcha";
		public $r0="r0";public $r1="r1";public $r2="r2";public $r3="r3";
		public $kgs="kgs";public $kgt="kgt";public $kgr="kgr";public $kgd="kgd";public $pr="pr";public $rac="rac";
		public $ch1="ch1";public $ch2="ch2";public $ch3="ch3";public $ch4="ch4";public $ch5="ch5";public $ch6="ch6";
		public $cp1="cp1";public $cp2="cp2";public $cp3="cp3";public $cp4="cp4";public $cp5="cp5";public $cp6="cp6";
		public $turno="turno";public $ps="ps";public $pb="pb";public $porbal="porbal";public $porsoy="porsoy";
		public $tablacha="aligracha_22";
		
        
		public $idmod="idmod";public $r0mod="r0mod";public $r1mod="r1mod";public $r2mod="r2mod";public $r3mod="r3mod";
		public $tablamod="facgramod";
		
		public $idfac="idfac";public $porbio="porbio";public $porbiok="porbiok";
		public $tablafac="facgra2_22";
		
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		
		function historypro($pro){
			
			$query=$this->db->query("select (porfac/100) as porfac from facgra2_22 where resfac<=$pro order by resfac DESC limit 1");
			return $query->row();
		}
		
		public function getDatosmes($filter,$mes,$cic){
			$fec=new Libreria();$t1=0;$t2=0;$data = array();	
			//INGRESAR LAS LUNAS
			$this->db->select('day(feclun) as dia, edolun');
			$this->db->where('month(feclun) =',$mes);
			$this->db->where('ciclun =',substr($cic, 2, 2));
			//$this->db->group_by('day(feclun)');
			$resulta = $this->db->get('lunas');
			$ini=1;
			while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
			foreach ($resulta->result() as $row):
				$datosd [$row->dia]=$row->dia; $datosr [$row->dia]=$row->edolun; 
			endforeach;
			$ini=1;
				while($ini<=31){
					switch($ini){
						case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
						case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
						case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
						case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
						case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
						case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
						case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
						case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
						case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
						case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
						case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
						case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
						case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
						case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
						case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
						case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
						case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
						case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
						case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
						case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
						case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
						case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
						case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
						case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
						case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
						case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
						case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
						case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
						case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
						case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
						case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
				}
				$ini+=1;
			}
			$row->pisg ='Luna';
			$data[] = $row;
			//RESULTADOS DE LAS CHAROLAS
			$this->db->select('pisg,idpis');
			$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'join');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$this->db->group_by($this->idpis);
			//$this->db->group_by($this->pisg);
			$querygral = $this->db->get($this->tablacha);
			if($querygral->num_rows()>0){
			$acu=0;$anttec=0;$antsis=0;$ct=1;
			foreach ($querygral->result() as $row):
				//Bbuscar el conteo de charola por estanque
				$pil=$row->idpis;
					//select day(fech) as dia, estr from analisisdepto inner join analisisresdiadepto on nadr=nad where nadr=6 and month(fech)=3 and ndep=17 group by dia
					$this->db->select('day(feccha) as dia, kgd');
					$this->db->where('idpischa =',$pil);
					if($filter['where']!='') {$this->db->where($filter['where']);}
					$this->db->group_by('day(feccha)');$this->db->group_by('kgd');
					//$this->db->group_by('idpischa');
					$resulta = $this->db->get($this->tablacha);
					//$cantidadre=$resulta->num_rows();
					$ini=1; $cont=0;
					while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					foreach ($resulta->result() as $rowa):
						$val=0;
						$datosd [$rowa->dia]=$rowa->dia; 
						$datosr [$rowa->dia]=$rowa->kgd; 
						/*if($rowa->estr>0) $datosvt [$rowa->dia]+=1;
						if($rowa->estr<0 and $rowa->nadr==20) $datosvt [$rowa->dia]+=1;
						$val=$rowa->estr;	
						if($rowa->nadr==20 ){
							if($rowa->estr<=5.0) $val=100;
							elseif(($rowa->estr>=5.01 and $rowa->estr<=20.00) ) $val=60;
								elseif($rowa->estr>=20.01) $val=20;
						}						
						$datosv [$rowa->dia]+=$val;
						*/
					endforeach;
					
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				$data[] = $row;
			endforeach;	
			
			}
			return $data;
				 
		}
		
		
		public function quitarcha($id){
			$this->db->where($this->idcha,$id);
			$this->db->delete($this->tablacha);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function actualizarfac($id,$por,$gra){
			if($gra==4) $data=array($this->porbio=>$por); else $data=array($this->porbiok=>$por);
			$this->db->where($this->idfac,$id);
			$this->db->update($this->tablafac,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function actualizarmod($id,$r0,$r1,$r2,$r3){
			$data=array($this->r0mod=>$r0,$this->r1mod=>$r1,$this->r2mod=>$r2,$this->r3mod=>$r3);
			if($id>0){
			 	$this->db->where($this->idmod,$id);
				$this->db->update($this->tablamod,$data);
			 	if($this->db->affected_rows()>0)
					return 1;
				else
					return 0;
			}else{
				$this->db->insert($this->tablamod,$data);
				return $this->db->insert_id();
			}
		}
		//public function actualizardia($id,$pis,$fec,$cic,$numgra,$ch1,$ch2,$ch3,$ch4,$ch5,$ch6,$tur,$kgt){
		public function actualizardia($id,$pis,$fec,$cic,$numgra,$cod,$kgt,$nfd,$nch,$r0,$r1,$r2,$r3,$mod,$bal,$soy){
			$kgd=0;
			if($nfd>0){ $r0=0;$r1=0;$r2=0;$r3=0;}
			if($mod>0) $kgd=$r0+$r1+$r2+$r3; else $kgd=$kgt;	
			$data=array($this->idpischa=>$pis,$this->feccha=>$fec,$this->ciccha=>$cic,$this->numgracha=>$numgra,$this->codcha=>$cod,$this->kgt=>$kgt,$this->kgd=>$kgd,$this->fedcha=>$nfd,$this->numcha=>$nch,$this->r0=>$r0,$this->r1=>$r1,$this->r2=>$r2,$this->r3=>$r3,$this->modcha=>$mod,$this->porbal=>$bal,$this->porsoy=>$soy);
				/*
			if($tur==1){
			$data=array($this->idpischa=>$pis,$this->feccha=>$fec,$this->ciccha=>$cic,$this->numgracha=>$numgra,$this->ch1=>$ch1,$this->ch2=>$ch2,$this->ch3=>$ch3,$this->ch3=>$ch3,$this->ch4=>$ch4,$this->ch5=>$ch5,$this->ch6=>$ch6,$this->turno=>$tur,$this->kgt=>$kgt,$this->kgd=>$kgt);
			}else {
			$data=array($this->idpischa=>$pis,$this->cp1=>$ch1,$this->cp2=>$ch2,$this->cp3=>$ch3,$this->cp3=>$ch3,$this->cp4=>$ch4,$this->cp5=>$ch5,$this->cp6=>$ch6);	
			}*/
			$this->db->where($this->idcha,$id);
			$this->db->update($this->tablacha,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregardia($pis,$fec,$cic,$numgra,$cod,$kgt,$nfd,$nch,$r0,$r1,$r2,$r3,$mod,$bal,$soy){
			$kgd=0;
			if($mod>0) $kgd=$r0+$r1+$r2+$r3; else $kgd=$kgt;
			$data=array($this->idpischa=>$pis,$this->feccha=>$fec,$this->ciccha=>$cic,$this->numgracha=>$numgra,$this->codcha=>$cod,$this->kgt=>$kgt,$this->kgd=>$kgt,$this->fedcha=>$nfd,$this->numcha=>$nch,$this->modcha=>$mod,$this->r0=>$r0,$this->r1=>$r1,$this->r2=>$r2,$this->r3=>$r3,$this->porbal=>$bal,$this->porsoy=>$soy);		
			$this->db->insert($this->tablacha,$data);
			return $this->db->insert_id();
		}
		
		
		public function quitar($id,$tab,$cam,$ciclo){
			$this->db->where($this->$cam,$id);
			$this->db->delete($tab.'_'.$ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		public function verPiscina(){
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		
		
		public function getDatosfac($filter,$gra){
			$result = $this->db->get($this->tablafac);
			foreach ($result->result() as $row):
				$row->porfac=number_format(($row->porfac), 2, '.', ',');
				if($gra==4)
				$row->porbio=number_format(($row->porbio), 2, '.', ',');
				else
				$row->porbio=number_format(($row->porbiok), 2, '.', ',');
				$data[] = $row;
			endforeach;	
			return $data;	 
		}
		
		public function getDatosmod($filter){
			$result = $this->db->get($this->tablamod);
			foreach ($result->result() as $row):
				if($row->r0mod>0) $row->r0mod=number_format(($row->r0mod), 2, '.', ','); else $row->r0mod='';
				if($row->r1mod>0) $row->r1mod=number_format(($row->r1mod), 2, '.', ','); else $row->r1mod='';
				if($row->r2mod>0) $row->r2mod=number_format(($row->r2mod), 2, '.', ','); else $row->r2mod='';
				if($row->r3mod>0) $row->r3mod=number_format(($row->r3mod), 2, '.', ','); else $row->r3mod='';
				$data[] = $row;
			endforeach;	
			return $data;	 
		}
		
		public function getDatosgral($filter,$ha){
			//select	feccha,sum(kgs) as sistema, sum(kgd) as diario, (sum(kgd)-sum(kgs))as diferencia from chagra_22 group by feccha
			$fec=new Libreria();$t1=0;$t2=0;$data = array();
			//$querygral=$this->db->query("select feccha,sum(kgs) as sistema, sum(kgd) as diario from chagra_22 where feccha>'2017-07-11' group by feccha");
			$this->db->select('feccha,sum(kgs) as sistema, sum(kgd) as diario');
			//$this->db->where($this->feccha.'>','2017-07-11');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$this->db->group_by($this->feccha,'ASC');
			$querygral = $this->db->get($this->tablacha);
			if($querygral->num_rows()>0){
			$acu=0;$anttec=0;$antsis=0;$ct=1;
			foreach ($querygral->result() as $row):
				$t1+=$row->sistema;$t2+=$row->diario; //$antsis=$row->sistema-$antsis;
				if($ct==1){$row->incsis='';$row->inctec='';$row->porsis='';$row->portec='';$row->kgsis='';$row->kgtec='';$ct=2;}
				else{
					$row->incsis=number_format(($row->sistema-$antsis), 1, '.', ',');
					if($antsis>0) $row->porsis=number_format((($row->sistema-$antsis)/$antsis)*100, 1, '.', ','); else $row->porsis='';
					$row->inctec=number_format(($row->diario-$anttec), 1, '.', ',');
					if($anttec>0) $row->portec=number_format((($row->diario-$anttec)/$anttec)*100, 1, '.', ','); else $row->portec='';
					$row->kgsis=number_format((($row->sistema)/$ha), 1, '.', ',');
					$row->kgtec=number_format((($row->diario)/$ha), 1, '.', ',');
				}
				$antsis=$row->sistema;$anttec=$row->diario;
				$row->diferencia=number_format(($row->diario-$row->sistema), 1, '.', ',');$acu+=$row->diario-$row->sistema;
				$row->sistema=number_format(($row->sistema), 1, '.', ',');
				$row->diario=number_format(($row->diario), 1, '.', ',');
				$row->acumulado=number_format(($acu), 1, '.', ',');
				$row->diam=$fec->fecha21($row->feccha);
				$data[] = $row;
			endforeach;	
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):
				$row->diam='Total:';
				$row->incsis='';$row->inctec='';$row->porsis='';$row->portec='';$row->kgsis='';$row->kgtec='';
				$row->diferencia=number_format(($t2-$t1), 1, '.', ',');
				$row->sistema=number_format(($t1), 1, '.', ',');
				$row->diario=number_format(($t2), 1, '.', ',');
				$row->acumulado=number_format(($acu), 1, '.', ',');
				$data[] = $row;
			endforeach;
			}
			return $data;
				 
		}
		public function buscar($id){
			/*$query=$this->db->get($this->tabla);*/
			$this->db->select($this->pisg);
			$query=$this->db->get_where($this->tabla,array($this->idpis=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->pisg;
			}
			return 0;
		}
		
		function getDatosdiaAnte($filter,$fecact,$est,$gra,$secc,$cicl){
		  if($est==0){		
			$this->db->select('idpis,pisg,hasg');
			$this->db->where('numgra =',$gra);
			$this->db->where('cicg =',$cicl);
			if($secc!=0){
			 $this->db->where('secc =',$secc);
			}
			$this->db->order_by('pisg');
			$resulta = $this->db->get($this->tabla);
			$tq=0; $data = array();
			$totkgda=0;$tothas=0;$actid=0;$totkgds=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$totkgd=0;
			$fec=new Libreria();$dias=0;$hecta=0;
			foreach ($resulta->result() as $rowg):
			  $est=$rowg->idpis;$hecta=$rowg->hasg;
			  $rowg->pisg=$rowg->pisg;$rowg->idpischa=$rowg->idpis;
			  $rowg->hasg=$rowg->hasg;$tothas+=$rowg->hasg;
			  $rowg->feccha1='';
			  $rowg->aliant='';$rowg->fecant='';$rowg->res='';$rowg->res3='';$rowg->facsis='';$rowg->kgssis='';$rowg->pr='';$rowg->kgstec='';$rowg->rac1='';$rowg->kgsha='';
			  $query=$this->db->query("select feccha as fecante, kgd from chagra_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$inicio = strtotime($rowali->fecante);
    				$fin = strtotime($fecact);
    				$dif = $fin - $inicio;
    				$dias = (( ( $dif / 60 ) / 60 ) / 24);
					if($dias==1){
						if($rowali->kgd>0) $rowg->aliant=number_format(($rowali->kgd), 2, '.', ',');
						$rowg->fecant=$fec->fecha21($rowali->fecante);
						$totkgda+=number_format($rowali->kgd, 2, '.', ',');
					}
					$entro=1;
				endforeach;	
			  $this->db->select('idcha,feccha,ch1,turno,pb,kgd,idpischa,ciccha,numgracha,pr,kgt,rac');
			  $this->db->where('idpischa =',$rowg->idpis);
			  if($filter['where']!=''){	$this->db->where($filter['where']); }
			  $result = $this->db->get($this->tablacha);
			  foreach ($result->result() as $row):
				$id=$row->idcha;$actid=$row->idcha;$rowg->ciccha=$row->ciccha;
				$rowg->feccha=$row->feccha;  $rowg->kgt=$row->kgt;$rowg->rac=$row->rac; $rowg->ch1=$row->ch1;
				
				if($rowg->idpis>=112 && $rowg->idpis<=128){
					$rowg->res3=($row->ch1);
					$rowg->res='';
				}else{
					$rowg->res=($row->ch1);
					$rowg->res3='';	
				}
				$rowg->idpischa=$row->idpischa;
				$rowg->idcha=$row->idcha;
				$res=($row->ch1);
				
				$rowg->feccha1=$fec->fecha21($row->feccha);
				$facs=0;
				
				if($rowg->idpis>=112 && $rowg->idpis<=128){
						switch($res){
							case 0: $facs=30; break;
							case 1: $facs=25; break;
							case 2: $facs=20; break;
							case 3: $facs=15; break;
							case 4: $facs=10; break;
							case 5: $facs=5; break;
							case 6: $facs=2; break;
							case 7: $facs=0; break;
							case 8: $facs=-2; break;
							case 9: $facs=-5; break;
							case 10: $facs=-10; break;
							case 11: $facs=-13; break;
							case 12: $facs=-15; break;
							case 13: $facs=-18; break;
							case 14: $facs=-20; break;
							case 15: $facs=-25; break;
							case 16: $facs=-30; break;
							case 17: $facs=-35; break;
							case 18: $facs=-40; break;
							case 19: $facs=-43; break;
							case 20: $facs=-45; break;
							case 21: $facs=-48; break;
							case 22: $facs=-50; break;
							case 23: $facs=-55; break;
							case 24: $facs=-60; break;
						}
						$rowg->facsis=number_format(($facs), 2, '.', ',');
				}else{
					if($gra==4){
						$queryfac=$this->db->query("select porfac,porbio as porbio from facgra_22 where $res=resfac ");
					}
					if($gra==2)
						$queryfac=$this->db->query("select porfac,porbiok as porbio from facgra_22 here $res=resfac ");
					foreach($queryfac->result() as $rowfac):	
						$facs=$rowfac->porfac;
						$rowg->facsis=number_format(($rowfac->porfac), 2, '.', ',');
					endforeach;			
				}
				//if($rowg->aliant>0 and $rowg->aliant>0) $rowg->kgssis=number_format(($rowg->aliant+(($rowg->aliant*$facs)/100)), 2, '.', ','); else $rowg->kgssis=0;
				$vari=0;
				$rowg->kgssis=number_format(($rowg->aliant+(($rowg->aliant*$facs)/100)), 2, '.', ',');
				$totkgds+=$rowg->aliant+(($rowg->aliant*$facs)/100);$vari=number_format(($rowg->aliant+(($rowg->aliant*$facs)/100)), 2, '.', '');
				//$dataz=array($this->kgs=>$rowg->kgssis);	
				$dataz=array($this->kgs=>$vari);
				$this->db->where($this->idcha,$actid);
				$this->db->update($this->tablacha,$dataz);
				if($row->kgt==0){ 
					$rowg->pr='';
				}else{
					if($rowg->aliant>0){
					$rowg->pr=number_format((($row->kgt-$rowg->aliant)/$rowg->aliant)*100, 2, '.', ',');
					}
				}	
				if($row->kgt!=0) {
					$rowg->kgstec=number_format($row->kgt, 2, '.', ',');
					if($row->rac>0) $rowg->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ','); else  $rowg->rac1='';
					$totkgd+=$rowg->kgstec;}
				else{
					$rowg->kgstec='';$rowg->rac1='';
				}
				$rowg->kgsha=number_format($rowg->kgstec/$rowg->hasg, 2, '.', ',');
				if($rowg->kgsha==0) $rowg->kgsha='';
				if($gra==4 and $cicl=='2019-1'){
					if($rowg->idpis>=73 && $rowg->idpis<=99) $totkgds1+=$rowg->kgstec;
						elseif($rowg->idpis>=112 && $rowg->idpis<=128) $totkgds2+=$rowg->kgstec;
							elseif(($rowg->idpis>=100 && $rowg->idpis<=111) || ($rowg->idpis>=129 && $rowg->idpis<=149)) $totkgds3+=$rowg->kgstec;
								//elseif($rowg->idpis>=129 && $rowg->idpis<=149) $totkgds4+=$rowg->kgstec;
				}else{
					if($rowg->idpis>=150) $totkgds3+=$rowg->kgstec;
				}
				if($gra==2){
					if($rowg->idpis>=1 && $rowg->idpis<=17) $totkgds1+=$rowg->kgstec;
				}
			  endforeach;
			  $data[] = $rowg;
			endforeach;
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg = "Tot"; $row->fecant=''; $row->feccha1='';$row->idpischa=1;$row->prom='';
				$row->ch1='';$row->res='';$row->res3='';
				$row->facsis='';$row->facbio='';$row->pr='';$row->kgstec='';$row->rac1='';
				$row->kgsha='';
				$row->aliant=number_format($totkgda, 2, '.', ',');
				$row->kgssis=number_format($totkgds, 2, '.', ',');
				$row->kgstec=number_format($totkgd, 2, '.', ',');
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';
				if($tothas>0){ $row->tothas=number_format($totkgd/$tothas, 2, '.', ',');
					$row->tothasa=number_format($totkgda/$tothas, 2, '.', ',');
					$row->kgsha=number_format($totkgd/$tothas, 2, '.', ',');
				}else {$row->tothas='';$row->tothasa='';$row->kgsha='';}
				$row->inc=number_format($totkgd-$totkgda, 2, '.', ',').' kg';
				$data[] = $row;	
			endforeach;	
			}else{
				//$this->db->select('idcha,idpis,pisg,hasg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6,pr,kgt');
				$this->db->select('idcha,idpis,sum(pisg) as pisg,sum(hasg) as hasg,idcha,feccha,ch1,turno,pb,kgd,idpischa,ciccha,numgracha,pr,kgt,rac');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'right');
				$this->db->group_by($this->feccha);
				$this->db->group_by($this->idcha);
				$this->db->group_by($this->idpis);
				//$this->db->group_by($this->pisg);
				$this->db->order_by($this->idpischa,'ASC');
				$this->db->order_by($this->feccha,'ASC');
				if($filter['order']!='')
					$this->db->order_by($filter['order']);	
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
				$result = $this->db->get($this->tablacha);
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array(); $fec=new Libreria();$totkgd=0;$totkgda=0;$totkgds=0;$totkgdb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$tothas=0;
				$totkgdsl2=0;$totkgdbl2=0;$totkgdb2=0;$totkgdsp3=0;$totkgdbp3=0;$porprom=0;$cprom=0;$porl2=0;$cl2=0;
				$pisi=0;
				if($result->num_rows()>0){
					$ultimodia=$fecact;					
					foreach ($result->result() as $row):
						$id=$row->idcha;$fecact=$row->feccha;$pisi=$row->idpischa;
						if($row->idpis>=112 && $row->idpis<=128){
							$row->res3=($row->ch1);
							$row->res='';
						}else{
							$row->res=($row->ch1);
							$row->res3='';	
						}
						//$row->res=($row->ch1);
						$hecta=$row->hasg;$row->ciccha=$row->ciccha;
						$res=($row->ch1);
						$row->resp=0; //($row->cp1);
						$resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
						$row->resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
						if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
						$row->feccha1=$fec->fecha21($row->feccha);
						$row->hasg=$row->hasg;
						if($row->resp<0)$row->resp='';
						/*if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
						if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';*/
						$est=$row->idpis;
						$row->aliant=0;$entro=0;$anter=0;
						$row->aliant='';$row->fecant='';
						$row->pm='p.m.';
						if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
						$query=$this->db->query("select feccha as fecante, kgd from chagra_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd order by feccha DESC limit 1");
						foreach($query->result() as $rowali):	
							$row->aliant=number_format(($rowali->kgd), 2, '.', ',');$anter=$row->aliant;
							$row->fecant=$fec->fecha21($rowali->fecante);
							$entro=1;
						endforeach;	
						if($entro==0){
							$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->resp='';$row->prom='';
							$row->facsis='';$row->facbio='';
							$row->kgssis=number_format(($row->kgd), 2, '.', ',');$totkgds+=number_format($row->kgssis, 2, '.', ',');
							$row->kgsbio=number_format(($row->kgd), 2, '.', ',');$totkgdb+=number_format($row->kgsbio, 2, '.', ',');
						}else{
							$facs=0;
							if($row->idpis>=112 && $row->idpis<=128){
								switch($res){
									case 0: $facs=30; break;
									case 1: $facs=25; break;
									case 2: $facs=20; break;
									case 3: $facs=15; break;
									case 4: $facs=10; break;
									case 5: $facs=5; break;
									case 6: $facs=2; break;
									case 7: $facs=0; break;
									case 8: $facs=-2; break;
									case 9: $facs=-5; break;
									case 10: $facs=-10; break;
									case 11: $facs=-13; break;
									case 12: $facs=-15; break;
									case 13: $facs=-18; break;
									case 14: $facs=-20; break;
									case 15: $facs=-25; break;
									case 16: $facs=-30; break;
									case 17: $facs=-35; break;
									case 18: $facs=-40; break;
									case 19: $facs=-43; break;
									case 20: $facs=-45; break;
									case 21: $facs=-48; break;
									case 22: $facs=-50; break;
									case 23: $facs=-55; break;
									case 24: $facs=-60; break;
								}
							}	
								//$rowg->facsis=number_format(($facs), 2, '.', ',');
							//	$rowfac->porfac=$facs;
							//	$rowfac->porbio=$facs;
							//}else{
								if($gra==4){
									$queryfac=$this->db->query("select porfac,porbio as porbio from facgra_22 where $res=resfac ");
								}
								if($gra==2)
									$queryfac=$this->db->query("select porfac,porbiok as porbio from facgra_22 here $res=resfac ");
								foreach($queryfac->result() as $rowfac):	
									$facs=$rowfac->porfac;
									//$rowg->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
								endforeach;			
							//}
							/*
							if($gra==4)
								$queryfac=$this->db->query("select porfac,porbio as porbio from facgra_22 where $res=resfac ");
							else
								$queryfac=$this->db->query("select porfac,porbiok as porbio from facgra_22 where $res=resfac ");
							foreach($queryfac->result() as $rowfac):	
								$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
								$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
							endforeach;		*/	
							$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 2, '.', ',');$totkgds+=number_format($row->kgssis, 2, '.', ',');
							$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 2, '.', ',');$totkgdb+=number_format($row->kgsbio, 2, '.', ',');
							//}
						}	
						$row->pr='';	
						if($row->kgt==0){ 
							$row->pr=0;
						}else{
							if($row->aliant>0){
								$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ',');
							}
						}	
						if($row->kgt!=0) {
							
							if($row->rac>0) $row->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ',').' kg'; else  $row->rac1='';
							
						}else{
							$row->rac1='';
						}
						$row->kgstec=number_format($row->kgt, 2, '.', ',');$totkgd+=$row->kgstec;
						$row->kgsha=number_format($row->kgstec/$row->hasg, 2, '.', ',');
						if($gra==4 && $cicl=='2019-1'){
							/*if($row->idpis>=73 && $row->idpis<=91) $totkgds1+=$row->kgstec;
								elseif($row->idpis>=92 && $row->idpis<=111) $totkgds2+=$row->kgstec;
									elseif($row->idpis>=112 && $row->idpis<=128) $totkgds3+=$row->kgstec;
										elseif($row->idpis>=129 && $row->idpis<=149) $totkgds4+=$row->kgstec;*/
							if($row->idpis>=73 && $row->idpis<=99) $totkgds1+=$row->kgstec;
								elseif($row->idpis>=112 && $row->idpis<=128) $totkgds2+=$row->kgstec;
									elseif(($row->idpis>=100 && $row->idpis<=111) || ($row->idpis>=129 && $row->idpis<=149)) $totkgds3+=$row->kgstec;
										//elseif($rowg->idpis>=129 && $rowg->idpis<=149) $totkgds4+=$rowg->kgstec;		
									
						}else{
							if($row->idpis>=150) $totkgds3+=$row->kgstec;
						}	
						
						if($gra==2){
							if($row->idpis>=1 && $row->idpis<=17) $totkgds1+=$row->kgstec;
						}	
						if($entro==1){
							if($row->res<5){
								if($gra==4){
									$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
									$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
								else{
									$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
									$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
								}
				 			}else{
				 				$row->kgsam='';
								$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 			}
						}else{
							$row->kgsam='';$row->kgspm='';
						}
						$esta = $row->pisg;
						$data[] = $row;
					endforeach;	
					$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg1 = $esta;
				$row->pisg = "Tot"; $row->fecant=''; $row->feccha1='';$row->prom='';
				$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->res3='';
				$row->facsis='';$row->facbio='';$row->pr='';//$row->kgstec='';
				$row->kgsam='';$row->kgspm='';$row->rac1='';
				$row->aliant='';
				//$row->kgssis='';
				//$row->kgsbio='';
				$row->kgsha='';
				$queryproc=$this->db->query("select avg(ps) pros,avg(pb) as prob,avg(pr) as pror from chagra_22 where feccha = '$fecact'");
				foreach ($queryproc->result() as $rowpro):
					$row->facsis=number_format($rowpro->pros, 2, '.', ',').' %';
					$row->facbio=number_format($rowpro->prob, 2, '.', ',').' %';
					$row->pr=number_format($rowpro->pror, 2, '.', ',').' %';
				endforeach;	
				$row->facsis='';$row->facbio='';$row->pr='';
				$row->kgstec=number_format($totkgd, 2, '.', ',');	
				$row->kgssis=number_format($totkgds, 2, '.', ',');
				//$row->kgssis2=number_format($totkgdsl2, 2, '.', ',');
				//$row->kgssisp=number_format($totkgdsp3, 2, '.', ',');
				$row->kgsbio=number_format($totkgdb, 2, '.', ',');
				
				$row->kgsbio2=number_format($totkgdbl2, 2, '.', ',');	
				
				//$row->facsis2=''; //$row->facsis2=number_format($porl2/$cl2, 2, '.', ',');
				//$row->facsisp='';//$row->facsisp=number_format($porprom/$cprom, 2, '.', ',');
				
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg'; //$row->totkgdsl2=number_format($totkgdsl2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';		 					
				$row->cp1='';$row->cp2='';$row->cp3='';$row->cp4='';$row->cp5='';$row->cp6='';$row->resp='';
				$fin = $ultimodia;$ini = '';
				$ini = strtotime ( '-9 day' , strtotime ( $fin ) ) ;$ini= date ( 'Y-m-d' , $ini);
				//$fin = strtotime ( '-1 day' , strtotime ( $fin ) ) ;$fin= date ( 'Y-m-d' , $fin);
				//arreglo para meter las fechas
				$dias = array();$kg = array();$dias1 = array();$am = array();$pm = array();$lec = array();
				$cont=0;
				while($cont<=9){$kg[$cont]='';$dias[$cont]='';$dias1[$cont]='';$am[$cont]='';$pm[$cont]='';$lec[$cont]='';$cont+=1;}
				$row->fec1= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-9 day' , strtotime ( $fin ) )))));$dias1[0]=$row->fec1;
				$row->fec2= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-8 day' , strtotime ( $fin ) )))));$dias1[1]=$row->fec2;
				$row->fec3= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-7 day' , strtotime ( $fin ) )))));$dias1[2]=$row->fec3;
				$row->fec4= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $fin ) )))));$dias1[3]=$row->fec4;
				$row->fec5= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-5 day' , strtotime ( $fin ) )))));$dias1[4]=$row->fec5;
				$row->fec6= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-4 day' , strtotime ( $fin ) )))));$dias1[5]=$row->fec6;
				$row->fec7= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-3 day' , strtotime ( $fin ) )))));$dias1[6]=$row->fec7;
				$row->fec8= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-2 day' , strtotime ( $fin ) )))));$dias1[7]=$row->fec8;
				$row->fec9= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-1 day' , strtotime ( $fin ) )))));$dias1[8]=$row->fec9;
				$row->fec10= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-0 day' , strtotime ( $fin ) )))));$dias1[9]=$row->fec10;
				
				$querygra=$this->db->query("select feccha,kgd,kgs,(ch1+ch2+ch3+ch4+ch5+ch6) as res1 from chagra_22 where idpischa='$pisi' and feccha>='$ini' and feccha<='$fin' order by feccha");
				$cont=0;
				foreach($querygra->result() as $rowgra):
					$kg[$cont]=$rowgra->kgd;$lec[$cont]=$rowgra->res1;
					$dias[$cont]=$fec->fecha21($rowgra->feccha);
					$pm[$cont]=number_format($rowgra->kgd/$hecta, 2, '.', ',');
					$am[$cont]=number_format($rowgra->kgs/$hecta, 2, '.', ',');
					/*
					if($rowgra->res1<5){
						$am[$cont]=number_format((($rowgra->kgd*30)/100), 2, '.', ','); 
						$pm[$cont]=number_format((($rowgra->kgd*70)/100), 2, '.', ',');
				 	}else{
				 		$am[$cont]=0;
						$pm[$cont]=number_format((($rowgra->kgd)), 2, '.', ',');
				 	}	
					 * */
					$cont+=1;
				endforeach;
				$cont=0; //$row->gra3=number_format($kg[$cont], 2, '.', ',');
				if($dias1[0]==$dias[$cont]) {$row->gram1=number_format($am[$cont], 2, '.', ',');$row->grap1=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram1='';$row->grap1='';}
				if($dias1[1]==$dias[$cont]) {$row->gram2=number_format($am[$cont], 2, '.', ',');$row->grap2=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram2='';$row->grap2='';}
				if($dias1[2]==$dias[$cont]) {$row->gram3=number_format($am[$cont], 2, '.', ',');$row->grap3=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram3='';$row->grap3='';}
				if($dias1[3]==$dias[$cont]) {$row->gram4=number_format($am[$cont], 2, '.', ',');$row->grap4=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram4='';$row->grap4='';}
				if($dias1[4]==$dias[$cont]) {$row->gram5=number_format($am[$cont], 2, '.', ',');$row->grap5=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram5='';$row->grap5='';}
				if($dias1[5]==$dias[$cont]) {$row->gram6=number_format($am[$cont], 2, '.', ',');$row->grap6=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram6='';$row->grap6='';}
				if($dias1[6]==$dias[$cont]) {$row->gram7=number_format($am[$cont], 2, '.', ',');$row->grap7=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram7='';$row->grap7='';}
				if($dias1[7]==$dias[$cont]) {$row->gram8=number_format($am[$cont], 2, '.', ',');$row->grap8=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram8='';$row->grap8='';}
				if($dias1[8]==$dias[$cont]) {$row->gram9=number_format($am[$cont], 2, '.', ',');$row->grap9=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram9='';$row->grap9='';}
				if($dias1[9]==$dias[$cont]) {$row->gram10=number_format($am[$cont], 2, '.', ',');$row->grap10=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram10='';$row->grap10='';}
				$row->fec1=$row->fec1.'<br/> Lec: '.$lec[0].'<br/> Kgs: '.$kg[0];
				$row->fec2=$row->fec2.'<br/> Lec: '.$lec[1].'<br/> Kgs: '.$kg[1];
				$row->fec3=$row->fec3.'<br/> Lec: '.$lec[2].'<br/> Kgs: '.$kg[2];
				$row->fec4=$row->fec4.'<br/> Lec: '.$lec[3].'<br/> Kgs: '.$kg[3];
				$row->fec5=$row->fec5.'<br/> Lec: '.$lec[4].'<br/> Kgs: '.$kg[4];
				$row->fec6=$row->fec6.'<br/> Lec: '.$lec[5].'<br/> Kgs: '.$kg[5];
				$row->fec7=$row->fec7.'<br/> Lec: '.$lec[6].'<br/> Kgs: '.$kg[6];
				$row->fec8=$row->fec8.'<br/> Lec: '.$lec[7].'<br/> Kgs: '.$kg[7];
				$row->fec9=$row->fec9.'<br/> Lec: '.$lec[8].'<br/> Kgs: '.$kg[8];
				if($kg[9]!='') $row->fec10=$row->fec10.'<br/> Lec: '.$lec[9].'<br/> Kgs: '.$kg[9]; 
				$data[] = $row;	
			endforeach;
			}	
		 }	
		 return $data;
		}
		
		
		function getDatosdia1($filter,$fecact,$est,$gra,$secc){
			//if($est==0){
				$this->db->select('idpis,pisg,hasg');
				$this->db->where('secc',$secc);
				$resulta = $this->db->get($this->tabla);
				if($resulta->num_rows()>0){
				 foreach ($resulta->result() as $rowg):
					 
				$this->db->select('idcha,idpis,pisg,hasg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6,pr,kgt');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'right');
				$this->db->group_by($this->idpischa);
				$this->db->order_by($this->idpischa,'ASC');
				$this->db->order_by($this->feccha,'ASC');
				//if($est==0){$this->db->order_by($this->idpischa,'ASC');}
				//else{$this->db->order_by($this->feccha,'DESC');}	
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
				if($filter['order']!='')
					$this->db->order_by($filter['order']);	
				//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
			/*	
			}else{
				$this->db->select('idcha,idpis,pisg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'inner');
				$this->db->order_by($this->idpischa,'ASC');	
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
			}*/
			$result = $this->db->get($this->tablacha);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$totkgd=0;$totkgda=0;$totkgds=0;$totkgdb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$tothas=0;
			$totkgdsl2=0;$totkgdbl2=0;$totkgdb2=0;$totkgdsp3=0;$totkgdbp3=0;$porprom=0;$cprom=0;$porl2=0;$cl2=0;
			$pisi=0;
			//$dias = array();$ali = array();$cont=0;
			//while($cont<=6){$dias[$con]=0;$ali[$con]=0;$cont+=1;}
			
			if($result->num_rows()>0){
			if($est==0){		
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$id=$row->idcha;$row->hasg=$row->hasg;$tothas+=$row->hasg;$actid=$row->idcha;
				$row->res=($row->ch1+$row->ch2+$row->ch3+$row->ch4+$row->ch5+$row->ch6);
				$res=($row->ch1+$row->ch2+$row->ch3+$row->ch4+$row->ch5+$row->ch6);
				$row->resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
				$resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
				$row->resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
				if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
				$row->feccha1=$fec->fecha21($row->feccha);
				if($row->resp<0)$row->resp='';
				if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
				if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';
				$est=$row->idpis;
				$row->aliant=0;$entro=0;
				$row->pm='p.m.';
				if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
				$query=$this->db->query("select feccha as fecante, kgd from chagra_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$row->aliant=number_format(($rowali->kgd), 2, '.', ',');
					$row->fecant=$fec->fecha21($rowali->fecante);
					$totkgda+=number_format($rowali->kgd, 2, '.', ',');
					$entro=1;
				endforeach;	
				if($gra==4)
				$queryfac=$this->db->query("select porfac,porbio as porbio from facgra_22 where $res=resfac ");
				else
				$queryfac=$this->db->query("select porfac,porbiok as porbio from facgra_22 where $res=resfac ");
				foreach($queryfac->result() as $rowfac):	
					$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
					$row->facsis1=number_format(($rowfac->porfac), 2, '.', ','); //.' %';
					$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
					$row->facbio1=number_format(($rowfac->porbio), 2, '.', ','); //.' %';
				endforeach;			
				$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 2, '.', ',');$totkgds+=number_format($row->kgssis, 2, '.', ',');
				$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 2, '.', ',');$totkgdb+=number_format($row->kgsbio, 2, '.', ',');
				$dataz=array($this->kgs=>$row->kgssis);	
				$this->db->where($this->idcha,$actid);
				$this->db->update($this->tablacha,$dataz);
				
				
				if($row->kgt==0){ 
					$row->pr=0;$row->pr1='';
				}else{
					if($row->aliant>0){
					$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ',');
					$row->pr1=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ','); //.' %';
					}
				}	
				$row->kgstec=number_format($row->kgt, 2, '.', ',');$totkgd+=$row->kgstec;
				$row->kgsha=number_format($row->kgstec/$row->hasg, 2, '.', ',');
				if($gra==4){
				if($row->idpis>=73 && $row->idpis<=91) $totkgds1+=$row->kgstec;
				elseif($row->idpis>=92 && $row->idpis<=111) $totkgds2+=$row->kgstec;
				elseif($row->idpis>=112 && $row->idpis<=128) $totkgds3+=$row->kgstec;
				elseif($row->idpis>=129 && $row->idpis<=149) $totkgds4+=$row->kgstec;
				}
				if($gra==2){
					if($row->idpis>=1 && $row->idpis<=17) $totkgds1+=$row->kgstec;
				}	
				
				if($row->res<5){
					if($gra==4){
					$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
					$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
					else{
					$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
					$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
					}
				 }else{
				 	$row->kgsam='';
					$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 }
				
				//$totkgd+=number_format($row->kgsam+$row->kgspm, 2, '.', ',');
				$row->totkgd=number_format($row->kgsam+$row->kgspm, 2, '.', ',');
				//actualiza los kgs del dia y el porcentaje del sistema
				if($entro==1){
				$datacha=array($this->kgd=>$row->totkgd,$this->ps=>$rowfac->porfac,$this->pb=>$rowfac->porbio,$this->pr=>$row->pr);
				$this->db->where($this->idcha,$id);
				$this->db->update($this->tablacha,$datacha);
				}
				$data[] = $row;
				
			endforeach;	
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg = "Tot"; $row->fecant=''; $row->feccha1='';$row->idpischa=1;$row->prom='';
				$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';
				$row->facsis='';$row->facbio='';$row->pr='';$row->kgstec='';
				$row->kgsam='';$row->kgspm='';$row->kgsha='';
				$queryproc=$this->db->query("select avg(ps) pros,avg(pb) as prob,avg(pr) as pror from chagra_22 where feccha = '$fecact'");
				foreach ($queryproc->result() as $rowpro):
					$row->facsis=number_format($rowpro->pros, 2, '.', ',').' %';
					$row->facbio=number_format($rowpro->prob, 2, '.', ',').' %';
					$row->pr=number_format($rowpro->pror, 2, '.', ',').' %';
				endforeach;		
				$row->facsis='';$row->facbio='';$row->pr='';
				$row->aliant=number_format($totkgda, 2, '.', ',');
				$row->kgssis=number_format($totkgds, 2, '.', ',');
				$row->kgsbio=number_format($totkgdb, 2, '.', ',');
				$row->kgstec=number_format($totkgd, 2, '.', ',');
				//$row->kgssis2=number_format($totkgdsl2, 2, '.', ',');
				//$row->kgssisp=number_format($totkgdsp3, 2, '.', ',');	
				//$row->facsis2=''; 
				//$row->facsisp='';
						 					
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';
				$row->tothas=number_format($totkgd/$tothas, 2, '.', ',');
				$row->tothasa=number_format($totkgda/$tothas, 2, '.', ',');
				$row->kgsha=number_format($totkgd/$tothas, 2, '.', ',');
				$row->inc=number_format($totkgd-$totkgda, 2, '.', ',').' kg';
				$row->cp1='';$row->cp2='';$row->cp3='';$row->cp4='';$row->cp5='';$row->cp6='';$row->resp='';
				$data[] = $row;	
							
			endforeach;
			}else{
			//Se forma el arreglo que sera retornado	
			$ultimodia=$fecact;					
			foreach ($result->result() as $row):
				$id=$row->idcha;$fecact=$row->feccha;$pisi=$row->idpischa;
				$row->res=($row->ch1+$row->ch2+$row->ch3+$row->ch4+$row->ch5+$row->ch6);
				$res=($row->ch1+$row->ch2+$row->ch3+$row->ch4+$row->ch5+$row->ch6);
				$row->resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
				$resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
				$row->resp=($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
				if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
				$row->feccha1=$fec->fecha21($row->feccha);
				$row->hasg=$row->hasg;
				if($row->resp<0)$row->resp='';
				if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
				if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';
				$est=$row->idpis;
				$row->aliant=0;$entro=0;$anter=0;
				$row->aliant='';$row->fecant='';
				$row->pm='p.m.';
				if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
				$query=$this->db->query("select feccha as fecante, kgd from chagra_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$row->aliant=number_format(($rowali->kgd), 2, '.', ',');$anter=$row->aliant;
					$row->fecant=$fec->fecha21($rowali->fecante);
					$entro=1;
				endforeach;	
				//$row->fecant=$fec->fecha($row->feccha);
				//$row->aliant=number_format(($row->kgd), 2, '.', ',');
				
				if($entro==0){
					$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->resp='';$row->prom='';
					
					$row->facsis='';$row->facbio='';
					$row->kgssis=number_format(($row->kgd), 2, '.', ',');$totkgds+=number_format($row->kgssis, 2, '.', ',');
					$row->kgsbio=number_format(($row->kgd), 2, '.', ',');$totkgdb+=number_format($row->kgsbio, 2, '.', ',');
					/*
					$row->facsis2='';$row->facbio2='';
					$row->kgssis2=number_format(($row->kgd), 2, '.', ',');$totkgdsl2+=number_format($row->kgssis2, 2, '.', ',');
					$row->kgsbio2=number_format(($row->kgd), 2, '.', ',');$totkgdb2+=number_format($row->kgsbio2, 2, '.', ',');
					
					$row->facsisp='';$row->facbiop='';
					$row->kgssisp=number_format(($row->kgd), 2, '.', ',');$totkgdsp3+=number_format($row->kgssisp, 2, '.', ',');
					$row->kgsbiop=number_format(($row->kgd), 2, '.', ',');$totkgdbp3+=number_format($row->kgsbiop, 2, '.', ',');
					*/
				}else{
					if($gra==4)
					$queryfac=$this->db->query("select porfac,porbio as porbio from facgra_22 where $res=resfac ");
					else
					$queryfac=$this->db->query("select porfac,porbiok as porbio from facgra_22 where $res=resfac ");
					foreach($queryfac->result() as $rowfac):	
						$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
						$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
					endforeach;			
					$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 2, '.', ',');$totkgds+=number_format($row->kgssis, 2, '.', ',');
					$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 2, '.', ',');$totkgdb+=number_format($row->kgsbio, 2, '.', ',');
					
					//aqui obtendra lo de la segunda lectura
					/*if($gra==4)
					$queryfac2=$this->db->query("select porfac,porbio as porbio from facgra_22 where $resp=resfac ");
					else
					$queryfac2=$this->db->query("select porfac,porbiok as porbio from facgra_22 where $resp=resfac ");
					foreach($queryfac2->result() as $rowfac2):	
						$row->facsis2=number_format(($rowfac2->porfac), 2, '.', ',');
						$row->facbio2=number_format(($rowfac2->porbio), 2, '.', ',');
						$porl2+=$row->facbio2;$cl2+=1;
					endforeach;			
					$row->kgssis2=number_format(($row->aliant+(($row->aliant*$rowfac2->porfac)/100)), 2, '.', ',');$totkgdsl2+=number_format($row->kgssis2, 2, '.', ',');
					$row->kgsbio2=number_format(($row->aliant+(($row->aliant*$rowfac2->porbio)/100)), 2, '.', ',');$totkgdbl2+=number_format($row->kgsbio2, 2, '.', ',');
					*/
					//aqui obtendra EL PROMEDIO DE LAS DOS LECTURAS
					/*
					if($gra==4)
					$queryfac3=$this->db->query("select porfac,porbio as porbio from facgra_22 where $row->prom=resfac ");
					else
					$queryfac3=$this->db->query("select porfac,porbiok as porbio from facgra_22 where $row->prom=resfac ");
					foreach($queryfac3->result() as $rowfac3):	
						$row->facsisp=number_format(($rowfac3->porfac), 2, '.', ',');
						$row->facbiop=number_format(($rowfac3->porbio), 2, '.', ',');
						$porprom+=$row->facbiop;$cprom+=1;
					endforeach;			
					$row->kgssisp=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 2, '.', ',');$totkgdsp3+=number_format($row->kgssisp, 2, '.', ',');
					$row->kgsbiop=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 2, '.', ',');$totkgdbp3+=number_format($row->kgsbiop, 2, '.', ',');
					*/
				}	
				$row->pr='';	
				if($row->kgt==0){ 
					$row->pr=0;
				}else{
					if($row->aliant>0){
					$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ',');
					}
				}	
				$row->kgstec=number_format($row->kgt, 2, '.', ',');$totkgd+=$row->kgstec;
				$row->kgsha=number_format($row->kgstec/$row->hasg, 2, '.', ',');
				
				if($gra==4){
					if($row->idpis>=73 && $row->idpis<=91) $totkgds1+=$row->kgstec;
					elseif($row->idpis>=92 && $row->idpis<=111) $totkgds2+=$row->kgstec;
					elseif($row->idpis>=112 && $row->idpis<=128) $totkgds3+=$row->kgstec;
					elseif($row->idpis>=129 && $row->idpis<=149) $totkgds4+=$row->kgstec;
				}
				if($gra==2){
					if($row->idpis>=1 && $row->idpis<=17) $totkgds1+=$row->kgstec;
				}	
				
				if($entro==1){
				if($row->res<5){
					if($gra==4){
						$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
						$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
					else{
						$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
						$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
					}
				 }else{
				 	$row->kgsam='';
					$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 }
				}else{
					$row->kgsam='';$row->kgspm='';
				}
				$esta = $row->pisg;
				$data[] = $row;
				
			endforeach;	
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg1 = $esta;
				$row->pisg = "Tot"; $row->fecant=''; $row->feccha1='';$row->prom='';
				$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';
				$row->facsis='';$row->facbio='';$row->pr='';//$row->kgstec='';
				$row->kgsam='';$row->kgspm='';
				$row->aliant='';
				//$row->kgssis='';
				//$row->kgsbio='';
				$row->kgsha='';
				$queryproc=$this->db->query("select avg(ps) pros,avg(pb) as prob,avg(pr) as pror from chagra_22 where feccha = '$fecact'");
				foreach ($queryproc->result() as $rowpro):
					$row->facsis=number_format($rowpro->pros, 2, '.', ',').' %';
					$row->facbio=number_format($rowpro->prob, 2, '.', ',').' %';
					$row->pr=number_format($rowpro->pror, 2, '.', ',').' %';
				endforeach;	
				$row->facsis='';$row->facbio='';$row->pr='';
				$row->kgstec=number_format($totkgd, 2, '.', ',');	
				$row->kgssis=number_format($totkgds, 2, '.', ',');
				//$row->kgssis2=number_format($totkgdsl2, 2, '.', ',');
				//$row->kgssisp=number_format($totkgdsp3, 2, '.', ',');
				$row->kgsbio=number_format($totkgdb, 2, '.', ',');
				
				$row->kgsbio2=number_format($totkgdbl2, 2, '.', ',');	
				
				//$row->facsis2=''; //$row->facsis2=number_format($porl2/$cl2, 2, '.', ',');
				//$row->facsisp='';//$row->facsisp=number_format($porprom/$cprom, 2, '.', ',');
				
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg'; //$row->totkgdsl2=number_format($totkgdsl2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';		 					
				$row->cp1='';$row->cp2='';$row->cp3='';$row->cp4='';$row->cp5='';$row->cp6='';$row->resp='';
				$fin = $ultimodia;$ini = '';
				$ini = strtotime ( '-9 day' , strtotime ( $fin ) ) ;$ini= date ( 'Y-m-d' , $ini);
				//$fin = strtotime ( '-1 day' , strtotime ( $fin ) ) ;$fin= date ( 'Y-m-d' , $fin);
				//arreglo para meter las fechas
				$dias = array();$kg = array();$dias1 = array();$am = array();$pm = array();
				$cont=0;
				while($cont<=9){$kg[$cont]='';$dias[$cont]='';$dias1[$cont]='';$am[$cont]='';$pm[$cont]='';$cont+=1;}
				$row->fec1= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-9 day' , strtotime ( $fin ) )))));$dias1[0]=$row->fec1;
				$row->fec2= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-8 day' , strtotime ( $fin ) )))));$dias1[1]=$row->fec2;
				$row->fec3= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-7 day' , strtotime ( $fin ) )))));$dias1[2]=$row->fec3;
				$row->fec4= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $fin ) )))));$dias1[3]=$row->fec4;
				$row->fec5= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-5 day' , strtotime ( $fin ) )))));$dias1[4]=$row->fec5;
				$row->fec6= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-4 day' , strtotime ( $fin ) )))));$dias1[5]=$row->fec6;
				$row->fec7= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-3 day' , strtotime ( $fin ) )))));$dias1[6]=$row->fec7;
				$row->fec8= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-2 day' , strtotime ( $fin ) )))));$dias1[7]=$row->fec8;
				$row->fec9= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-1 day' , strtotime ( $fin ) )))));$dias1[8]=$row->fec9;
				$row->fec10= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-0 day' , strtotime ( $fin ) )))));$dias1[9]=$row->fec10;
				$querygra=$this->db->query("select feccha,kgd,(ch1+ch2+ch3+ch4+ch5+ch6) as res1 from chagra_22 where idpischa='$pisi' and feccha>='$ini' and feccha<='$fin'");
				$cont=0;
				foreach($querygra->result() as $rowgra):
					$kg[$cont]=$rowgra->kgd;
					$dias[$cont]=$fec->fecha21($rowgra->feccha);
					if($rowgra->res1<5){
						$am[$cont]=number_format((($rowgra->kgd*30)/100), 2, '.', ','); 
						$pm[$cont]=number_format((($rowgra->kgd*70)/100), 2, '.', ',');
				 	}else{
				 		$am[$cont]=0;
						$pm[$cont]=number_format((($rowgra->kgd)), 2, '.', ',');
				 	}	
					$cont+=1;
				endforeach;
				$cont=0; //$row->gra3=number_format($kg[$cont], 2, '.', ',');
				if($dias1[0]==$dias[$cont]) {$row->gram1=number_format($am[$cont], 2, '.', ',');$row->grap1=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram1='';$row->grap1='';}
				if($dias1[1]==$dias[$cont]) {$row->gram2=number_format($am[$cont], 2, '.', ',');$row->grap2=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram2='';$row->grap2='';}
				if($dias1[2]==$dias[$cont]) {$row->gram3=number_format($am[$cont], 2, '.', ',');$row->grap3=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram3='';$row->grap3='';}
				if($dias1[3]==$dias[$cont]) {$row->gram4=number_format($am[$cont], 2, '.', ',');$row->grap4=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram4='';$row->grap4='';}
				if($dias1[4]==$dias[$cont]) {$row->gram5=number_format($am[$cont], 2, '.', ',');$row->grap5=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram5='';$row->grap5='';}
				if($dias1[5]==$dias[$cont]) {$row->gram6=number_format($am[$cont], 2, '.', ',');$row->grap6=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram6='';$row->grap6='';}
				if($dias1[6]==$dias[$cont]) {$row->gram7=number_format($am[$cont], 2, '.', ',');$row->grap7=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram7='';$row->grap7='';}
				if($dias1[7]==$dias[$cont]) {$row->gram8=number_format($am[$cont], 2, '.', ',');$row->grap8=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram8='';$row->grap8='';}
				if($dias1[8]==$dias[$cont]) {$row->gram9=number_format($am[$cont], 2, '.', ',');$row->grap9=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram9='';$row->grap9='';}
				if($dias1[9]==$dias[$cont]) {$row->gram10=number_format($am[$cont], 2, '.', ',');$row->grap10=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram10='';$row->grap10='';}
				$data[] = $row;	
			endforeach;
			}	
			}
			endforeach;	
			}	
			return $data;
		}
		
		function getElementsb($where,$ciclo){
			$this->db->select("pisg,pisg as val");   
			$this->db->where('cicg',$ciclo);    
        	$this->db->where('numgra',4);
			$this->db->order_by('pisg');
			$result = $this->db->get($this->tabla);        
        	/*$this->db->select("idpis,pisg as val");       
        	if($where['secc']!=0)
	            $this->db->where($where);
			else {
				
				$this->db->where('numgra',4);
			}
			$result = $this->db->get($this->tabla);*/
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementssecc($filest){        
			$ano=substr($filest, 2,2);
			$ciclo=substr($filest, 0,6);
			$secc=substr($filest, 6,2);
			$granja=substr($filest, 8,1);
        	$this->db->select("pisg,pisg as val,cicg");     
			$this->db->where('numgra',$granja);
			if($secc!=0){$this->db->where('secc',$secc);}
			if($ciclo!=0){$this->db->where('cicg',$ciclo);}
	        //$this->db->where('secc >',0);
	        $this->db->order_by('cicg');$this->db->order_by('pisg');
			$result = $this->db->get($this->tablasie.$ano);
        	$data = array();        
        	foreach($result->result() as $row):
				//$row->val='C'.substr($row->cicg, -1).'-'.$row->val;
				
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementsseccprimero($where,$ciclo){
			$this->db->select("idpis,pisg as val");   
			if($ciclo>0) $this->db->where('secc',$ciclo);    
        	$this->db->where('numgra',4);
			$this->db->order_by('pisg');
			$result = $this->db->get($this->tabla);        
        	/*$this->db->select("idpis,pisg as val");       
        	if($where['secc']!=0)
	            $this->db->where($where);
			else {
				
				$this->db->where('numgra',4);
			}
			$result = $this->db->get($this->tabla);*/
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function history($pil){
			$this->db->select($this->cp1." ,".$this->cp2." ,".$this->cp3." ,".$this->cp4." ,".$this->cp5." ,".$this->cp6);
			$this->db->from($this->tablacha);
			$this->db->where('idcha =', $pil);
			$query=$this->db->get();
			return $query->row();
		}	
		function historyhas($pil){
			//SELECT hasg,fedcha,numcha from aligracha_22 inner join siegra_22 on cicg=ciccha where idpischa=1 order by feccha DESC Limit 1
			/*$this->db->select($this->hasg." ,".$this->fedcha." ,".$this->numcha);
			$this->db->join($this->tabla, $this->cicg.'='.$this->ciccha,'inner');
			$this->db->from($this->tablacha,1);
			$this->db->where('idpischa =', $pil);
			$this->db->order_by('feccha1', 'DESC');
			$query=$this->db->get();*/
			
			$query=$this->db->query("SELECT hasg,fedcha,numcha,porbal,porsoy,(porbal+porsoy) as kgta from aligracha_22 inner join siegra_22 on cicg=ciccha where idpischa='$pil' order by feccha DESC Limit 1");
			return $query->row();
		}
		function historymod($mod){
			$this->db->select($this->r0mod." ,".$this->r1mod." ,".$this->r2mod." ,".$this->r3mod);
			$this->db->from($this->tablamod);
			$this->db->where('idmod =', $mod);
			$query=$this->db->get();
			return $query->row();
		}
		
		function getDatosdia($filter,$fecact,$est,$gra,$secc,$cicl){
		 	
		  if($est==0){		
			$this->db->select('idpis,pisg,hasg,cicg,numgra,secc,nfd');
			$this->db->where('numgra =',$gra);
			$this->db->where('cicg =',$cicl);
			$this->db->where('fecg > 0');
			if($secc!=0){
			 $this->db->where('secc =',$secc);
			}
			$this->db->order_by('pisg');
			$resulta = $this->db->get($this->tabla);
			$tq=0; $data = array();
			$totkgda=0;$tothas=0;$actid=0;$totkgds=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$totkgd=0;$totkgsreal=0;
			$totbal=0;$totsoy=0;$totbalant=0;$totsoyant=0;
			$totkgds1s=0;$totkgds1b=0;$totkgds2s=0;$totkgds2b=0;$totkgds3s=0;$totkgds3b=0;$totkgds4s=0;$totkgds4b=0;
			$fec=new Libreria();$dias=0;$hecta=0;
			foreach ($resulta->result() as $rowg):
			  //$est=$rowg->idpis;
			  $est=$rowg->pisg;
			  $hecta=$rowg->hasg;$rowg->secc=$rowg->secc;
			  $rowg->pisg=$rowg->pisg;$rowg->idpischa=$rowg->idpis;
			  $rowg->hasg=number_format($rowg->hasg,1) ;$tothas+=$rowg->hasg;
			  //$rowg->fedcha=$rowg->nfd;
			  $anter=0;
			  $rowg->fedcha='';$rowg->nfdant='';$rowg->kgsbal='';$rowg->kgssoy='';$rowg->kgsbalant='';$rowg->kgssoyant='';
			  $rowg->pesb='';$rowg->fecant='';$rowg->aliant='';$rowg->kgshas='';$rowg->feccha1='';$rowg->res='';$rowg->codcha='';
			  $rowg->numcha='';$rowg->procha='';$rowg->facsis='';$rowg->kgssis='';$rowg->incksa='';$rowg->pr='';$rowg->kgstec='';$rowg->inckta='';
			  $rowg->modcha='';$rowg->pesbfd='';$rowg->r0='';$rowg->r1='';$rowg->r2='';$rowg->r3='';$rowg->kgd='';
			  $rowg->kgsha='';$rowg->res3='';$rowg->rac1='';$modant=0;$chaant=0;$porbalant=0;$porsoyant=0;
			  //busca los gramos pasados
		      $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$est' and fecb < '$fecact' and cicb='$cicl' group by fecb,pesb order by fecb DESC limit 1");
			  foreach($query->result() as $rowgrs):
				$rowg->pesb =number_format($rowgrs->pesb,1,".","");
			  endforeach; 
			  $query=$this->db->query("select (feccha) as fecante,kgd,fedcha,modcha,numcha,porbal,porsoy from $this->tablacha where idpischa='$est' and feccha < '$fecact' and ciccha='$cicl' group by feccha,kgd,fedcha,modcha,numcha,porbal,porsoy order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$inicio = strtotime($rowali->fecante);$modant=$rowali->modcha;$chaant=$rowali->numcha;
					$porbalant=$rowali->porbal;$porsoyant=$rowali->porsoy;
					//$rowg->kgsbalant=number_format($rowali->kgd*($rowali->porbal/100), 1, '.', ',');$totbalant+=$rowg->kgsbalant;
					$rowg->kgsbalant=number_format($rowali->porbal, 1, '.', ',');$totbalant+=$rowg->kgsbalant;
					if($rowg->kgsbalant==0) $rowg->kgsbalant=''; 
					if($rowali->porsoy>0){$rowg->kgssoyant=number_format($rowali->porsoy, 1, '.', ',');$totsoyant+=$rowg->kgssoyant;}
    				
    				$fin = strtotime($fecact);
    				$dif = $fin - $inicio;
    				$dias = (( ( $dif / 60 ) / 60 ) / 24);
    				
					if($dias<=1){
						if($rowali->kgd>0){$rowg->aliant=number_format(($rowali->kgd), 1, '.', ',');$anter=$rowg->aliant;}
						$rowg->fecant=$fec->fecha21($rowali->fecante);
						$totkgda+=number_format($rowali->kgd, 1, '.', ',');
						if($rowali->fedcha>0) {$rowg->nfdant=$rowali->fedcha; $rowg->fedcha=$rowg->nfdant;}
						$rowg->kgshas=number_format($rowali->kgd/$rowg->hasg, 1, '.', ',');
					}
					$entro=1;
				endforeach;	
			
				
			  if($modant>0) $rowg->modcha=$modant;
			  if($chaant>0) $rowg->numcha=$chaant;
			  //if($porbalant>0) $rowg->porbal=$porbalant;
			  //if($porsoyant>0) $rowg->porsoy=$porsoyant;
			  $this->db->select('idcha,feccha,fedcha,codcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,numcha,modcha,r0,r1,r2,r3,porbal,porsoy');
			  //$this->db->select('idcha,feccha,ch1,turno,pb,kgd,idpischa,ciccha,numgracha,pr,kgt,rac');
			  $this->db->where('idpischa =',$est);
			  $this->db->where('ciccha =',$cicl);
			  if($filter['where']!=''){	$this->db->where($filter['where']); }
			  $result = $this->db->get($this->tablacha);
			  foreach ($result->result() as $row):
				$id=$row->idcha;$actid=$row->idcha;$rowg->ciccha=$row->ciccha; $peso=0;
				if($row->feccha>0) $rowg->feccha=$row->feccha; 
				if($row->fedcha>0) $rowg->fedcha=$row->fedcha; 
				$rowg->kgt=$row->kgt;$rowg->rac=$row->rac; $rowg->codcha=$row->codcha;
				$rowg->porbal=$row->porbal;$rowg->porsoy=$row->porsoy;
				//if($row->porbal>0) $rowg->porbal=$row->porbal; else $rowg->porbal=$porbalant;
				//if($row->porsoy>0) $rowg->porsoy=$row->porsoy; else $rowg->porsoy=$porsoyant;
				//$rowg->numcha=$row->numcha;
				if($row->numcha>0) $rowg->numcha=$row->numcha; else $rowg->numcha=$chaant;
				if($row->modcha>0) $rowg->modcha=$row->modcha; else $rowg->modcha=$modant; 
				$rowg->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
				$rowg->res=($row->codcha);$res=$rowg->procha;
				$rowg->res3='';	
				$rowg->idpischa=$row->idpischa;
				$rowg->idcha=$row->idcha;
				$rowg->feccha1=$fec->fecha21($row->feccha);
				$facs=0;
					if($gra==4){
						//select porfac,porbio as porbio from facgra2_22 where resfac<='2.33' order by resfac DESC limit 1	
						$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
					}
					foreach($queryfac->result() as $rowfac):	
						$facs=$rowfac->porfac;
						$rowg->facsis=number_format(($rowfac->porfac), 0, '.', ',');
					endforeach;			
				$rowg->kgssis=number_format(($anter+(($anter*$facs)/100)), 0, '.', ',');$totkgds+=number_format($rowg->kgssis, 0, '.', ',');
				if($rowg->kgssis-$anter!=0) $rowg->incksa=number_format($rowg->kgssis-$anter, 0, '.', ',');
				$dataz=array($this->kgs=>$rowg->kgssis);	
				$this->db->where($this->idcha,$actid);
				$this->db->update($this->tablacha,$dataz);
				if($row->kgt==0){ 
					$rowg->pr='';
				}else{
					if($anter>0){
						$rowg->pr=number_format((($row->kgt-$anter)/$anter)*100, 0, '.', ',');
						if($rowg->pr==0) $rowg->pr='';
					}
				}	
				$rowgkgssoy=0;$rowgkgsbal=0;
				if($row->kgt!=0) {
					$rowg->kgstec=number_format($row->kgt, 1, '.', ',');
					$rowg->kgsbal=number_format($row->porbal, 1, '.', ',');$totbal+=$rowg->kgsbal; $rowgkgsbal=$rowg->kgsbal;
					if($rowg->kgsbal==0) $rowg->kgsbal=''; 
					if($row->porsoy>0){$rowg->kgssoy=number_format($row->porsoy, 1, '.', ',');$totsoy+=$rowg->kgssoy;$rowgkgssoy=$rowg->kgssoy;}
					if($rowg->kgstec-$anter!=0) $rowg->inckta=number_format($rowg->kgstec-$anter, 1, '.', ',');
					if($row->rac>0) $rowg->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ','); else  $rowg->rac1='';
					$totkgd+=$rowg->kgstec;
					if($rowg->fedcha>0) $peso=number_format($rowgkgsbal/$rowg->fedcha, 2, '.', ',');
				}else{
					$rowg->kgstec='';$rowg->rac1='';
				}
				$rowg->kgsha=number_format($rowg->kgstec/$rowg->hasg, 1, '.', ',');
				if($rowg->kgsha==0) $rowg->kgsha='';
				
				$rowgkgd=0;
				if($row->fedcha>0){
					$rowg->pesbfd=$peso;
					if($peso==0) $rowg->pesbfd=''; 
					$rowg->kgd=number_format($row->kgd,1);$rowgkgd=$rowg->kgd;
					$rowg->modcha='';
				} 
				else{
					if($row->r0>0) $rowg->r0=number_format($row->r0, 1, '.', ',');	
					if($row->r1>0) $rowg->r1=number_format($row->r1, 1, '.', ',');
					if($row->r2>0) $rowg->r2=number_format($row->r2, 1, '.', ',');
					if($row->r3>0) $rowg->r3=number_format($row->r3, 1, '.', ',');
					if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0){$rowg->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 1, '.', ','); $rowgkgd=$rowg->kgd;}
				} 
				switch ($rowg->secc) {
					case '41': $totkgds1+=$rowgkgd;$totkgds1s+=$rowgkgssoy;$totkgds1b+=$rowgkgsbal;break;
					case '42': $totkgds2+=$rowgkgd;$totkgds2s+=$rowgkgssoy;$totkgds2b+=$rowgkgsbal;break;
					case '43': $totkgds3+=$rowgkgd;$totkgds3s+=$rowgkgssoy;$totkgds3b+=$rowgkgsbal;break;
					case '44': $totkgds4+=$rowgkgd;$totkgds4s+=$rowgkgssoy;$totkgds4b+=$rowgkgsbal;break;
				}
				$totkgsreal+=$rowgkgd;
			  endforeach;
			  $data[] = $rowg;
			endforeach;
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg = "Tot";$row->fecant='';$row->pesb='';$row->fecant='';$row->nfdant='';$row->aliant='';$row->kgshas='';
				$row->feccha1='';$row->codcha='';$row->numcha='';$row->procha='';$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
				$row->kgstec='';$row->inckta='';$row->pesbfd='';$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';$row->kgd='';
				$row->idpischa=1;$row->prom='';$row->kgsbal='';$row->kgssoy='';$row->kgsbalant='';$row->kgssoyant='';
				$row->fedcha='';$row->res='';
			  	$row->ch1='';$row->res='';$row->res3='';$row->facbio='';$row->pr='';$row->rac1='';$row->kgsha='';
				if($tothas>0) $row->hasg=number_format($tothas, 1, '.', ',');
				if($totkgda>0) $row->aliant=number_format($totkgda, 1, '.', ',');
				if($totbalant>0) $row->kgsbalant=number_format($totbalant, 1, '.', ',');
				if($totsoyant>0) $row->kgssoyant=number_format($totsoyant, 1, '.', ',');
				if($totkgds>0) $row->kgssis=number_format($totkgds, 0, '.', ',');
				if($totkgds>0) $row->kgstec=number_format($totkgd, 1, '.', ',');
				if($totbal>0) $row->kgsbal=number_format($totbal, 1, '.', ',');
				if($totsoy>0) $row->kgssoy=number_format($totsoy, 1, '.', ',');
				$row->totkgds1=number_format($totkgds1, 1, '.', ',').' kg';$row->totkgds1s=number_format($totkgds1s, 1, '.', ',');$row->totkgds1b=number_format($totkgds1b, 1, '.', ',');
				$row->totkgds2=number_format($totkgds2, 1, '.', ',').' kg';$row->totkgds2s=number_format($totkgds2s, 1, '.', ',');$row->totkgds2b=number_format($totkgds2b, 1, '.', ',');                         
				$row->totkgds3=number_format($totkgds3, 1, '.', ',').' kg';$row->totkgds3s=number_format($totkgds3s, 1, '.', ',');$row->totkgds3b=number_format($totkgds3b, 1, '.', ',');
				$row->totkgds4=number_format($totkgds4, 1, '.', ',').' kg';$row->totkgds4s=number_format($totkgds4s, 1, '.', ',');$row->totkgds4b=number_format($totkgds4b, 1, '.', ',');
				$row->soya=number_format($totkgds1s+$totkgds2s+$totkgds3s+$totkgds4s, 1, '.', ',');
				$row->balanceado=number_format($totkgds1b+$totkgds2b+$totkgds3b+$totkgds4b, 1, '.', ',');
				if($totkgsreal>0)$row->kgd=number_format($totkgsreal, 1, '.', ',');
				if($tothas>0){ $row->tothas=number_format($totkgd/$tothas, 2, '.', ',');
					$row->tothasa=number_format($totkgda/$tothas, 2, '.', ',');
					if($totkgd/$tothas > 0)$row->kgsha=number_format($totkgd/$tothas, 1, '.', ',');
				}else {$row->tothas='';$row->tothasa='';$row->kgsha='';}
				$row->inc=number_format($totkgd-$totkgda, 2, '.', ',').' kg';
				$data[] = $row;	
			endforeach;	
			}else{
				//$this->db->select('idcha,idpis,pisg,hasg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6,pr,kgt');
				$this->db->select('idcha,idpis,sum(pisg) as pisg,sum(hasg) as hasg,idcha,feccha,fedcha,codcha,modcha,numcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,secc,r0,r1,r2,r3,porbal,porsoy');
				//$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'right');
				$this->db->join($this->tabla, $this->pisg.'='.$this->idpischa,'right');
				$this->db->where('fecg > 0');
				$this->db->where('cicg',$cicl);
				$this->db->group_by($this->feccha);
				$this->db->group_by($this->idcha);
				//$this->db->group_by($this->idpis);
				$this->db->group_by($this->pisg);
				$this->db->order_by($this->idpischa,'ASC');
				$this->db->order_by($this->feccha,'ASC');
				if($filter['order']!='')
					$this->db->order_by($filter['order']);	
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
				$result = $this->db->get($this->tablacha);
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array(); $fec=new Libreria();$totkgd=0;$totkgda=0;$totkgds=0;$totkgdb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$tothas=0;
				$totkgdsl2=0;$totkgdbl2=0;$totkgdb2=0;$totkgdsp3=0;$totkgdbp3=0;$porprom=0;$cprom=0;$porl2=0;$cl2=0;$peso=0;
				$pisi=0;$modant=0;$chaant=0;$totkgsreal=0;$porbalant=0;$porsoyant=0;$totbal=0;$totsoy=0;$totbalant=0;$totsoyant=0;
				if($result->num_rows()>0){
					$ultimodia=$fecact;					
					foreach ($result->result() as $row):
						$id=$row->idcha;$fecact=$row->feccha;$pisi=$row->idpischa;$row->pesb ='';
						$row->nfdant='';$row->kgshas='';$row->procha='';$row->kgsbal='';$row->kgssoy='';$row->kgsbalant='';$row->kgssoyant='';
						$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
						$row->kgstec='';$row->inckta='';$row->pesbfd=''; //$row->r0='';$row->r1='';$row->r2='';$row->r3=''; 
						if($row->fedcha>0) $row->nfdant=$row->fedcha; 
						
						//if($row->idpis>=112 && $row->idpis<=128){
						//	$row->res3=($row->ch1);
						//	$row->res='';
						//}else{
							$row->res=($row->codcha);
							$row->res3='';	
						//}
						//$row->res=($row->ch1);
						$hecta=$row->hasg;$row->ciccha=$row->ciccha;
						//$res=($row->codcha);
						$res=number_format($row->codcha/$row->numcha, 2, '.', ',');
						
						
						 //busca los gramos pasados
					    $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$pisi' and fecb < '$fecact' and cicb='$cicl' group by fecb,pesb order by fecb DESC limit 1");
						foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,1,".","");
						endforeach; 
						
						$row->resp=0; //($row->cp1);
						$resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
						$row->resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
						if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
						$row->feccha1=$fec->fecha21($row->feccha);
						$row->hasg=$row->hasg;
						if($row->resp<0)$row->resp='';
						/*if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
						if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';*/
						$est=$row->idpis;$estpes=$row->idpischa;
						$row->aliant=0;$entro=0;$anter=0;
						$row->aliant='';$row->fecant='';
						$row->pm='p.m.';
						if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
						$query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$estpes' and fecb < '$fecact'and cicb='$cicl' group by fecb,pesb order by fecb DESC limit 1");
			  			foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,2,".","");
			  			endforeach; 
						
						$query=$this->db->query("select feccha as fecante,kgd,modcha,numcha,porbal,porsoy from aligracha_22 where idpischa='$est' and feccha < '$fecact' and ciccha='$cicl' group by feccha,kgd,modcha,numcha,porbal,porsoy order by feccha DESC limit 1");
						foreach($query->result() as $rowali):	
							$row->aliant=number_format(($rowali->kgd), 1, '.', ',');$anter=$row->aliant;
							$modant=$rowali->modcha;$chaant=$rowali->numcha;$porbalant=$rowali->porbal;$porsoyant=$rowali->porsoy;
							$row->kgsbalant=number_format($rowali->porbal, 1, '.', ',');$totbalant+=$row->kgsbalant;
							if($row->kgsbalant==0) $row->kgsbalant=''; 
							if($rowali->porsoy>0){$row->kgssoyant=number_format($rowali->porsoy, 1, '.', ',');$totsoyant+=$row->kgssoyant;}
							$row->fecant=$fec->fecha21($rowali->fecante);
							$row->kgshas=number_format($rowali->kgd/$row->hasg, 1, '.', ',');
							$entro=1;
						endforeach;	
						//if($modant>0) $row->modcha=$modant;
			  			if($chaant>0) $row->numcha=$chaant;
						if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
						if($row->modcha>0) $row->modcha=$row->modcha; // else $row->modcha=$modant; 
						$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
						if($entro==0){
							$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->resp='';$row->prom='';
							$row->facsis='';$row->facbio='';
							$row->kgssis=number_format(($row->kgd), 1, '.', ',');$totkgds+=number_format($row->kgssis, 1, '.', ',');
							$row->kgsbio=number_format(($row->kgd), 1, '.', ',');$totkgdb+=number_format($row->kgsbio, 1, '.', ',');
						}else{
							$facs=0;
								//if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
								//if($row->modcha>0) $row->modcha=$row->modcha; else $row->modcha=$modant; 
								$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
								
								if($gra==4){
									//$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where $res=resfac ");
									$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
								}
								
								foreach($queryfac->result() as $rowfac):	
									$facs=$rowfac->porfac;
									//$rowg->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facsis=number_format(($rowfac->porfac), 0, '.', ',');
									$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
								endforeach;			
							$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 1, '.', ',');$totkgds+=number_format($row->kgssis, 1, '.', ',');
							$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 1, '.', ',');$totkgdb+=number_format($row->kgsbio, 1, '.', ',');
							$row->incksa=number_format($row->kgssis-$row->aliant, 0, '.', ',');
						}	
						$row->pr='';	
						if($row->kgt==0){ 
							$row->pr=0;
						}else{
							if($row->aliant>0){
								$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 0, '.', ',');
							}
						}	
						if($row->kgt!=0) {
							
							if($row->rac>0) $row->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ',').' kg'; else  $row->rac1='';
							
						}else{
							$row->rac1='';
						}
						$row->kgstec=number_format($row->kgt, 1, '.', ',');$totkgd+=$row->kgstec;$kgstec=$row->kgt;
						$row->kgsbal=number_format($row->porbal, 1, '.', ',');$totbal+=$row->kgsbal;$kgsbal=$row->porbal;
						if($row->kgsbal==0){$row->kgsbal=''; $kgsbal=0;}
						if($row->porsoy>0){$row->kgssoy=number_format($row->porsoy, 1, '.', ',');$totsoy+=$row->kgssoy;}
						//if($row->fedcha>0) $peso=number_format($row->kgstec/$row->fedcha, 2, '.', ',');
						if($row->fedcha>0) $peso=number_format($kgsbal/$row->fedcha, 2, '.', ',');
						$row->kgsha=number_format($kgstec/$row->hasg, 1, '.', ',');
						$row->inckta=number_format($kgstec-$anter, 1, '.', ',');
						
						if($row->fedcha>0){
							$row->pesbfd=$peso;
							if($peso==0) $row->pesbfd='';
							$row->kgd=number_format($row->kgd,1);
							$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';
						}else{
							if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0) $row->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 1, '.', ',');	
							if($row->r0>0) $row->r0=number_format($row->r0, 1, '.', ','); else $row->r0=''; 	
							if($row->r1>0) $row->r1=number_format($row->r1, 1, '.', ','); else $row->r1='';
							if($row->r2>0) $row->r2=number_format($row->r2, 1, '.', ','); else $row->r2='';
							if($row->r3>0) $row->r3=number_format($row->r3, 1, '.', ','); else $row->r3='';
							
						} 
						switch ($row->secc) {
							case '41': $totkgds1+=$row->kgd;break;
							case '42': $totkgds2+=$row->kgd;break;
							case '43': $totkgds3+=$row->kgd;break;
							case '44': $totkgds4+=$row->kgd;break;
						}
						
							
						if($entro==1){
							if($row->res<5){
								if($gra==4){
									$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
									$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
								else{
									//$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
									//$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
								}
				 			}else{
				 				$row->kgsam='';
								$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 			}
						}else{
							$row->kgsam='';$row->kgspm='';
						}
						$esta = $row->pisg;
						$totkgsreal+=$row->kgd;
						$data[] = $row;
					endforeach;	
					$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg1 = $esta;
				$row->pisg = "Tot"; $row->fecant=''; $row->feccha1='';$row->prom='';$row->codcha='';
				$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->res3='';
				$row->facsis='';$row->facbio='';$row->pr='';//$row->kgstec='';
				$row->kgsam='';$row->kgspm='';$row->rac1='';
				$row->aliant='';
				$row->kgsha='';$row->kgsbal='';$row->kgssoy='';$row->kgsbalant='';$row->kgssoyant='';
				$row->hasg='';$row->fecant='';$row->pesb='';$row->fecant='';$row->nfdant='';$row->aliant='';$row->kgshas='';
				$row->feccha1='';$row->codcha='';$row->numcha='';$row->procha='';$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
				$row->kgstec='';$row->inckta='';$row->pesbfd='';$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';$row->kgd='';
				$row->idpischa=1;$row->prom='';
				$row->fedcha='';$row->res='';
				$queryproc=$this->db->query("select avg(ps) pros,avg(pb) as prob,avg(pr) as pror from chagra_22 where feccha = '$fecact'");
				foreach ($queryproc->result() as $rowpro):
					$row->facsis=number_format($rowpro->pros, 2, '.', ',').' %';
					$row->facbio=number_format($rowpro->prob, 2, '.', ',').' %';
					$row->pr=number_format($rowpro->pror, 2, '.', ',').' %';
				endforeach;	
				$row->facsis='';$row->facbio='';$row->pr='';
				//if($totbalant>0) $row->kgsbalant=number_format($totbalant, 1, '.', ',');
				//if($totsoyant>0) $row->kgssoyant=number_format($totsoyant, 1, '.', ',');
				if($totbal>0) $row->kgsbal=number_format($totbal, 1, '.', ',');
				if($totsoy>0) $row->kgssoy=number_format($totsoy, 1, '.', ',');
				if($totkgd>0) $row->kgstec=number_format($totkgd, 1, '.', ',');	
				if($totkgds>0) $row->kgssis=number_format($totkgds, 1, '.', ',');
				$row->kgsbio=number_format($totkgdb, 2, '.', ',');
				$row->kgsbio2=number_format($totkgdbl2, 2, '.', ',');	
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg'; //$row->totkgdsl2=number_format($totkgdsl2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';		
				if($totkgsreal>0)$row->kgd=number_format($totkgsreal, 1, '.', ','); 					
				$row->cp1='';$row->cp2='';$row->cp3='';$row->cp4='';$row->cp5='';$row->cp6='';$row->resp='';
				$fin = $ultimodia;$ini = '';
				$ini = strtotime ( '-9 day' , strtotime ( $fin ) ) ;$ini= date ( 'Y-m-d' , $ini);
				//$fin = strtotime ( '-1 day' , strtotime ( $fin ) ) ;$fin= date ( 'Y-m-d' , $fin);
				//arreglo para meter las fechas
				$dias = array();$kg = array();$dias1 = array();$am = array();$pm = array();$lec = array();
				$cont=0;
				while($cont<=9){$kg[$cont]='';$dias[$cont]='';$dias1[$cont]='';$am[$cont]='';$pm[$cont]='';$lec[$cont]='';$cont+=1;}
				$row->fec1= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-9 day' , strtotime ( $fin ) )))));$dias1[0]=$row->fec1;
				$row->fec2= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-8 day' , strtotime ( $fin ) )))));$dias1[1]=$row->fec2;
				$row->fec3= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-7 day' , strtotime ( $fin ) )))));$dias1[2]=$row->fec3;
				$row->fec4= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $fin ) )))));$dias1[3]=$row->fec4;
				$row->fec5= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-5 day' , strtotime ( $fin ) )))));$dias1[4]=$row->fec5;
				$row->fec6= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-4 day' , strtotime ( $fin ) )))));$dias1[5]=$row->fec6;
				$row->fec7= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-3 day' , strtotime ( $fin ) )))));$dias1[6]=$row->fec7;
				$row->fec8= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-2 day' , strtotime ( $fin ) )))));$dias1[7]=$row->fec8;
				$row->fec9= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-1 day' , strtotime ( $fin ) )))));$dias1[8]=$row->fec9;
				$row->fec10= $fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-0 day' , strtotime ( $fin ) )))));$dias1[9]=$row->fec10;
				
				$querygra=$this->db->query("select feccha,kgd,kgs,(ch1+ch2+ch3+ch4+ch5+ch6) as res1 from chagra_22 where idpischa='$pisi' and feccha>='$ini' and feccha<='$fin' order by feccha");
				$cont=0;
				foreach($querygra->result() as $rowgra):
					$kg[$cont]=$rowgra->kgd;$lec[$cont]=$rowgra->res1;
					$dias[$cont]=$fec->fecha21($rowgra->feccha);
					$pm[$cont]=number_format($rowgra->kgd/$hecta, 2, '.', ',');
					$am[$cont]=number_format($rowgra->kgs/$hecta, 2, '.', ',');
					$cont+=1;
				endforeach;
				$cont=0; //$row->gra3=number_format($kg[$cont], 2, '.', ',');
				if($dias1[0]==$dias[$cont]) {$row->gram1=number_format($am[$cont], 2, '.', ',');$row->grap1=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram1='';$row->grap1='';}
				if($dias1[1]==$dias[$cont]) {$row->gram2=number_format($am[$cont], 2, '.', ',');$row->grap2=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram2='';$row->grap2='';}
				if($dias1[2]==$dias[$cont]) {$row->gram3=number_format($am[$cont], 2, '.', ',');$row->grap3=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram3='';$row->grap3='';}
				if($dias1[3]==$dias[$cont]) {$row->gram4=number_format($am[$cont], 2, '.', ',');$row->grap4=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram4='';$row->grap4='';}
				if($dias1[4]==$dias[$cont]) {$row->gram5=number_format($am[$cont], 2, '.', ',');$row->grap5=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram5='';$row->grap5='';}
				if($dias1[5]==$dias[$cont]) {$row->gram6=number_format($am[$cont], 2, '.', ',');$row->grap6=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram6='';$row->grap6='';}
				if($dias1[6]==$dias[$cont]) {$row->gram7=number_format($am[$cont], 2, '.', ',');$row->grap7=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram7='';$row->grap7='';}
				if($dias1[7]==$dias[$cont]) {$row->gram8=number_format($am[$cont], 2, '.', ',');$row->grap8=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram8='';$row->grap8='';}
				if($dias1[8]==$dias[$cont]) {$row->gram9=number_format($am[$cont], 2, '.', ',');$row->grap9=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram9='';$row->grap9='';}
				if($dias1[9]==$dias[$cont]) {$row->gram10=number_format($am[$cont], 2, '.', ',');$row->grap10=number_format($pm[$cont], 2, '.', ',');$cont+=1;} else {$row->gram10='';$row->grap10='';}
				$row->fec1=$row->fec1.'<br/> Lec: '.$lec[0].'<br/> Kgs: '.$kg[0];
				$row->fec2=$row->fec2.'<br/> Lec: '.$lec[1].'<br/> Kgs: '.$kg[1];
				$row->fec3=$row->fec3.'<br/> Lec: '.$lec[2].'<br/> Kgs: '.$kg[2];
				$row->fec4=$row->fec4.'<br/> Lec: '.$lec[3].'<br/> Kgs: '.$kg[3];
				$row->fec5=$row->fec5.'<br/> Lec: '.$lec[4].'<br/> Kgs: '.$kg[4];
				$row->fec6=$row->fec6.'<br/> Lec: '.$lec[5].'<br/> Kgs: '.$kg[5];
				$row->fec7=$row->fec7.'<br/> Lec: '.$lec[6].'<br/> Kgs: '.$kg[6];
				$row->fec8=$row->fec8.'<br/> Lec: '.$lec[7].'<br/> Kgs: '.$kg[7];
				$row->fec9=$row->fec9.'<br/> Lec: '.$lec[8].'<br/> Kgs: '.$kg[8];
				//if($kg[9]!='')
				$row->fec10=$row->fec10.'<br/> Lec: '.$lec[9].'<br/> Kgs: '.$kg[9]; 
				$data[] = $row;	
			endforeach;
			}	
		 }	
		 return $data;
		}

		function getDatosdiag($filter,$fecact,$est,$gra,$secc,$cicl,$fec2){
			
		  if($est==0){		
			$this->db->select('idpis,pisg,hasg,cicg,numgra,secc,nfd');
			$this->db->where('numgra =',$gra);
			$this->db->where('cicg =',$cicl);
			if($secc!=0){
			 $this->db->where('secc =',$secc);
			}
			$resulta = $this->db->get($this->tabla);
			$tq=0; $data = array();
			$totkgda=0;$tothas=0;$actid=0;$totkgds=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$totkgd=0;$totkgsreal=0;
			$fec=new Libreria();$dias=0;$hecta=0;
			foreach ($resulta->result() as $rowg):
			  $est=$rowg->idpis;$hecta=$rowg->hasg;$rowg->secc=$rowg->secc;
			  $rowg->pisg=$rowg->pisg;$rowg->idpischa=$rowg->idpis;
			  $rowg->hasg=$rowg->hasg;$tothas+=$rowg->hasg;
			  //$rowg->fedcha=$rowg->nfd;
			  $rowg->fedcha='';$rowg->nfdant='';
			  $rowg->pesb='';$rowg->fecant='';$rowg->aliant='';$rowg->kgshas='';$rowg->feccha1='';$rowg->res='';$rowg->codcha='';
			  $rowg->numcha='';$rowg->procha='';$rowg->facsis='';$rowg->kgssis='';$rowg->incksa='';$rowg->pr='';$rowg->kgstec='';$rowg->inckta='';
			  $rowg->modcha='';$rowg->pesbfd='';$rowg->r0='';$rowg->r1='';$rowg->r2='';$rowg->r3='';$rowg->kgd='';
			  $rowg->kgsha='';$rowg->res3='';$rowg->rac1='';$modant=0;$chaant=0;
			  //busca los gramos pasados
		      $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$est' and fecb < '$fecact' group by fecb,pesb order by fecb DESC limit 1");
			  foreach($query->result() as $rowgrs):
				$rowg->pesb =number_format($rowgrs->pesb,1,".","");
			  endforeach; 
			  $query=$this->db->query("select feccha as fecante,kgd,fedcha,modcha,numcha from $this->tablacha where idpischa='$est' and feccha < '$fecact' group by feccha,kgd,fedcha,modcha,numcha order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$inicio = strtotime($rowali->fecante);$modant=$rowali->modcha;$chaant=$rowali->numcha;
    				$fin = strtotime($fecact);
    				$dif = $fin - $inicio;
    				$dias = (( ( $dif / 60 ) / 60 ) / 24);
					if($dias==1){
						if($rowali->kgd>0) $rowg->aliant=number_format(($rowali->kgd), 0, '.', ',');
						$rowg->fecant=$fec->fecha21($rowali->fecante);
						$totkgda+=number_format($rowali->kgd, 0, '.', ',');
						if($rowali->fedcha>0) {$rowg->nfdant=$rowali->fedcha; $rowg->fedcha=$rowg->nfdant;}
						$rowg->kgshas=number_format($rowali->kgd/$rowg->hasg, 1, '.', ',');
						
					}
					$entro=1;
				endforeach;	
			  if($modant>0) $rowg->modcha=$modant;
			  if($chaant>0) $rowg->numcha=$chaant;
			  $this->db->select('idcha,feccha,fedcha,codcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,numcha,modcha,r0,r1,r2,r3,porbal,porsoy');
			  //$this->db->select('idcha,feccha,ch1,turno,pb,kgd,idpischa,ciccha,numgracha,pr,kgt,rac');
			  $this->db->where('idpischa =',$rowg->idpis);
			  if($filter['where']!=''){	$this->db->where($filter['where']); }
			  $result = $this->db->get($this->tablacha);
			  foreach ($result->result() as $row):
				$id=$row->idcha;$actid=$row->idcha;$rowg->ciccha=$row->ciccha; $peso=0;
				if($row->feccha>0) $rowg->feccha=$row->feccha; 
				if($row->fedcha>0) $rowg->fedcha=$row->fedcha; 
				$rowg->kgt=$row->kgt;$rowg->rac=$row->rac; $rowg->codcha=$row->codcha;
				//$rowg->numcha=$row->numcha;
				if($row->numcha>0) $rowg->numcha=$row->numcha; else $rowg->numcha=$chaant;
				if($row->modcha>0) $rowg->modcha=$row->modcha; else $rowg->modcha=$modant; 
				$rowg->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
				$rowg->res=($row->codcha);$res=$rowg->procha;
				$rowg->res3='';	
				$rowg->idpischa=$row->idpischa;
				$rowg->idcha=$row->idcha;
				$rowg->feccha1=$fec->fecha21($row->feccha);
				$facs=0;
					if($gra==4){
						//select porfac,porbio as porbio from facgra2_22 where resfac<='2.33' order by resfac DESC limit 1	
						$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
					}
					foreach($queryfac->result() as $rowfac):	
						$facs=$rowfac->porfac;
						$rowg->facsis=number_format(($rowfac->porfac), 0, '.', ',');
					endforeach;			
				$rowg->kgssis=number_format(($rowg->aliant+(($rowg->aliant*$facs)/100)), 0, '.', ',');$totkgds+=number_format($rowg->kgssis, 0, '.', ',');
				$rowg->incksa=number_format($rowg->kgssis-$rowg->aliant, 0, '.', ',');
				$dataz=array($this->kgs=>$rowg->kgssis);	
				$this->db->where($this->idcha,$actid);
				$this->db->update($this->tablacha,$dataz);
				if($row->kgt==0){ 
					$rowg->pr='';
				}else{
					if($rowg->aliant>0){
					$rowg->pr=number_format((($row->kgt-$rowg->aliant)/$rowg->aliant)*100, 1, '.', ',');
					}
				}	
				if($row->kgt!=0) {
					$rowg->kgstec=number_format($row->kgt, 0, '.', ',');
					$rowg->inckta=number_format($rowg->kgstec-$rowg->aliant, 0, '.', ',');
					if($row->rac>0) $rowg->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ','); else  $rowg->rac1='';
					$totkgd+=$rowg->kgstec;
					if($rowg->fedcha>0) $peso=number_format($rowg->kgstec/$rowg->fedcha, 0, '.', ',');
				}else{
					$rowg->kgstec='';$rowg->rac1='';
				}
				$rowg->kgsha=number_format($rowg->kgstec/$rowg->hasg, 2, '.', ',');
				$rowg->kgshasy=number_format($rowg->porsoy/$rowg->hasg, 2, '.', ',');
				$rowg->kgshabl=number_format($rowg->porbal/$rowg->hasg, 2, '.', ',');
				if($rowg->kgsha==0) $rowg->kgsha='';
				if($rowg->kgshasy==0) $rowg->kgshasy='';
				if($rowg->kgshabl==0) $rowg->kgshabl='';
				if($row->fedcha>0){
					$rowg->pesbfd=$peso;
					$rowg->kgd=$row->kgd;
					$rowg->modcha='';
					
				} 
				else{
					if($row->r0>0) $rowg->r0=number_format($row->r0, 1, '.', ',');	
					if($row->r1>0) $rowg->r1=number_format($row->r1, 1, '.', ',');
					if($row->r2>0) $rowg->r2=number_format($row->r2, 1, '.', ',');
					if($row->r3>0) $rowg->r3=number_format($row->r3, 1, '.', ',');
					if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0) $rowg->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 0, '.', ',');
				} 
				switch ($rowg->secc) {
					case '41': $totkgds1+=$rowg->kgd;break;
					case '42': $totkgds2+=$rowg->kgd;break;
					case '43': $totkgds3+=$rowg->kgd;break;
					case '44': $totkgds4+=$rowg->kgd;break;
				}
				
				$totkgsreal+=$rowg->kgd;
			  endforeach;
			  $data[] = $rowg;
			endforeach;
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg = "Tot";$row->fecant='';$row->pesb='';$row->fecant='';$row->nfdant='';$row->aliant='';$row->kgshas='';
				$row->feccha1='';$row->codcha='';$row->numcha='';$row->procha='';$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
				$row->kgstec='';$row->inckta='';$row->pesbfd='';$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';$row->kgd='';
				$row->idpischa=1;$row->prom='';
				$row->fedcha='';$row->res='';
			  	$row->ch1='';$row->res='';$row->res3='';$row->facbio='';$row->pr='';$row->rac1='';$row->kgsha='';
				$row->hasg=number_format($tothas, 1, '.', ',');
				$row->aliant=number_format($totkgda, 0, '.', ',');
				$row->kgssis=number_format($totkgds, 0, '.', ',');
				$row->kgstec=number_format($totkgd, 0, '.', ',');
				$row->totkgds1=number_format($totkgds1, 2, '.', ',').' kg';
				$row->totkgds2=number_format($totkgds2, 2, '.', ',').' kg';
				$row->totkgds3=number_format($totkgds3, 2, '.', ',').' kg';
				$row->totkgds4=number_format($totkgds4, 2, '.', ',').' kg';
				if($totkgsreal>0)$row->kgd=number_format($totkgsreal, 0, '.', ',');
				if($tothas>0){ $row->tothas=number_format($totkgd/$tothas, 2, '.', ',');
					$row->tothasa=number_format($totkgda/$tothas, 2, '.', ',');
					$row->kgsha=number_format($totkgd/$tothas, 2, '.', ',');
				}else {$row->tothas='';$row->tothasa='';$row->kgsha='';}
				$row->inc=number_format($totkgd-$totkgda, 2, '.', ',').' kg';
				$data[] = $row;	
			endforeach;	
			}else{
				//$this->db->select('idcha,idpis,pisg,hasg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6,pr,kgt');
				$this->db->select('idcha,idpis,sum(pisg) as pisg,sum(hasg) as hasg,idcha,feccha,fedcha,codcha,modcha,numcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,secc,r0,r1,r2,r3,porbal,porsoy');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'right');
				$this->db->group_by($this->feccha);
				$this->db->group_by($this->idcha);
				$this->db->group_by($this->idpis);
				//$this->db->group_by($this->pisg);
				$this->db->order_by($this->idpischa,'ASC');
				$this->db->order_by($this->feccha,'ASC');
				if($filter['order']!='')
					$this->db->order_by($filter['order']);	
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
				$result = $this->db->get($this->tablacha);
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array(); $fec=new Libreria();$totkgd=0;$totkgda=0;$totkgds=0;$totkgdb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$tothas=0;
				$totkgdsl2=0;$totkgdbl2=0;$totkgdb2=0;$totkgdsp3=0;$totkgdbp3=0;$porprom=0;$cprom=0;$porl2=0;$cl2=0;$peso=0;
				$pisi=0;$modant=0;$chaant=0;$totkgsreal=0;$cod=0;
				if($result->num_rows()>0){
					$ultimodia=$fecact;					
					foreach ($result->result() as $row):
						$id=$row->idcha;$fecact=$row->feccha;$pisi=$row->idpischa;$row->pesb ='';$cod=$row->codcha;
						$row->nfdant='';$row->kgshas='';$row->procha='';
						$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
						$row->kgstec='';$row->inckta='';$row->pesbfd=''; //$row->r0='';$row->r1='';$row->r2='';$row->r3=''; 
						if($row->fedcha>0) $row->nfdant=$row->fedcha; 
						
						//if($row->idpis>=112 && $row->idpis<=128){
						//	$row->res3=($row->ch1);
						//	$row->res='';
						//}else{
							$row->res=($row->codcha);
							$row->res3='';	
						//}
						//$row->res=($row->ch1);
						$hecta=$row->hasg;$row->ciccha=$row->ciccha;
						//$res=($row->codcha);
						$res=number_format($row->codcha/$row->numcha, 2, '.', ',');
						
						
						 //busca los gramos pasados
					    $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$pisi' and fecb < '$fecact' group by fecb,pesb order by fecb DESC limit 1");
						foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,1,".","");
						endforeach; 
						
						$row->resp=0; //($row->cp1);
						$resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
						$row->resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
						if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
						$row->feccha1=$fec->fecha21($row->feccha);
						$row->hasg=$row->hasg;
						if($row->resp<0)$row->resp='';
						/*if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
						if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';*/
						$est=$row->idpis;$estpes=$row->idpischa;
						$row->aliant=0;$entro=0;$anter=0;
						$row->aliant='';$row->fecant='';
						$row->pm='p.m.';
						if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
						$query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$estpes' and fecb < '$fecact' group by fecb,pesb order by fecb DESC limit 1");
			  			foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,2,".","");
			  			endforeach; 
						
						$query=$this->db->query("select feccha as fecante,kgd,modcha,numcha from aligracha_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd,modcha,numcha order by feccha DESC limit 1");
						foreach($query->result() as $rowali):	
							$row->aliant=number_format(($rowali->kgd), 0, '.', ',');$anter=$row->aliant;
							$modant=$rowali->modcha;$chaant=$rowali->numcha;
							$row->fecant=$fec->fecha21($rowali->fecante);
							$row->kgshas=number_format($rowali->kgd/$row->hasg, 1, '.', ',');
							$entro=1;
						endforeach;	
						//if($modant>0) $row->modcha=$modant;
			  			if($chaant>0) $row->numcha=$chaant;
						if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
						if($row->modcha>0) $row->modcha=$row->modcha; // else $row->modcha=$modant; 
						$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
						if($entro==0){
							$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->resp='';$row->prom='';
							$row->facsis='';$row->facbio='';
							$row->kgssis=number_format(($row->kgd), 0, '.', ',');$totkgds+=number_format($row->kgssis, 0, '.', ',');
							$row->kgsbio=number_format(($row->kgd), 0, '.', ',');$totkgdb+=number_format($row->kgsbio, 0, '.', ',');
						}else{
							$facs=0;
								//if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
								//if($row->modcha>0) $row->modcha=$row->modcha; else $row->modcha=$modant; 
								$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
								
								if($gra==4){
									//$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where $res=resfac ");
									$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
								}
								
								foreach($queryfac->result() as $rowfac):	
									$facs=$rowfac->porfac;
									//$rowg->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
								endforeach;			
							$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 0, '.', ',');$totkgds+=number_format($row->kgssis, 0, '.', ',');
							$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 0, '.', ',');$totkgdb+=number_format($row->kgsbio, 0, '.', ',');
							$row->incksa=number_format($row->kgssis-$row->aliant, 0, '.', ',');
						}	
						$row->pr='';	
						if($row->kgt==0){ 
							$row->pr=0;
						}else{
							if($row->aliant>0){
								$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ',');
							}
						}	
						if($row->kgt!=0) {
							
							if($row->rac>0) $row->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ',').' kg'; else  $row->rac1='';
							
						}else{
							$row->rac1='';
						}
						$row->kgstec=number_format($row->kgt, 0, '.', ',');$totkgd+=$row->kgstec;
						if($row->fedcha>0) $peso=number_format($row->kgstec/$row->fedcha, 0, '.', ',');
						
						$row->inckta=number_format($row->kgstec-$row->aliant, 0, '.', ',');
						if($row->fedcha>0){
							$row->pesbfd=$peso;
							$row->kgd=$row->kgd;
							$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';
						}else{
							if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0) $row->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 0, '.', ',');	
							if($row->r0>0) $row->r0=number_format($row->r0, 1, '.', ','); else $row->r0=''; 	
							if($row->r1>0) $row->r1=number_format($row->r1, 1, '.', ','); else $row->r1='';
							if($row->r2>0) $row->r2=number_format($row->r2, 1, '.', ','); else $row->r2='';
							if($row->r3>0) $row->r3=number_format($row->r3, 1, '.', ','); else $row->r3='';
							
						} 
						$row->kgsha=number_format($row->kgd/$row->hasg, 2, '.', ',');
						$row->kgshapro=number_format($row->kgssis/$row->hasg, 2, '.', ',');
						$row->kgshasy=number_format($row->porsoy/$row->hasg, 2, '.', ',');
						$row->kgshabl=number_format($row->porbal/$row->hasg, 2, '.', ',');
						switch ($row->secc) {
							case '41': $totkgds1+=$row->kgd;break;
							case '42': $totkgds2+=$row->kgd;break;
							case '43': $totkgds3+=$row->kgd;break;
							case '44': $totkgds4+=$row->kgd;break;
						}
						
						if($entro==1){
							if($row->res<5){
								if($gra==4){
									$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
									$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
								else{
									//$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
									//$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
								}
				 			}else{
				 				$row->kgsam='';
								$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 			}
						}else{
							$row->kgsam='';$row->kgspm='';
						}
						$totkgsreal+=$row->kgd;
						$esta = $row->pisg;$row->piscina=$row->pisg;
						$luna='';
						//INGRESAR LAS LUNAS
						$this->db->select('edolun');
						$this->db->where('feclun =',$row->feccha);
						$this->db->where('ciclun =',substr($cicl, 2, 2));
						$resultalun = $this->db->get('lunas');
						foreach ($resultalun->result() as $rowlun):
							$luna=$rowlun->edolun; 
						endforeach;
						$row->feccha1=$fec->fecha21($row->feccha).'<br/> Kgs: '.$row->kgd.'<br/> Cód: '.$row->codcha.'<br/>'.$luna;
						$data[] = $row;
					endforeach;	
					
			}	
		 }	
		 return $data;
		}

		function getDatosdiagali($filter,$fecact,$est,$gra,$secc,$cicl,$fec2,$fm,$hora){
			
		  if($est==0){		
			$this->db->select('idpis,pisg,hasg,cicg,numgra,secc,nfd');
			$this->db->where('numgra =',$gra);
			$this->db->where('cicg =',$cicl);
			//if($secc!=0){
			 $this->db->where('secc =',$secc);
			//}
			$resulta = $this->db->get($this->tabla);
			$tq=0; $data = array();
			$totkgda=0;$tothas=0;$actid=0;$totkgds=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$totkgd=0;$totkgsreal=0;
			$fec=new Libreria();$dias=0;$hecta=0;
			foreach ($resulta->result() as $rowg):
			  $est=$rowg->idpis;$hecta=$rowg->hasg;$rowg->secc=$rowg->secc;
			  $rowg->pisg=$rowg->pisg;$rowg->idpischa=$rowg->idpis;
			  $rowg->hasg=$rowg->hasg;$tothas+=$rowg->hasg;
			  //$rowg->fedcha=$rowg->nfd;
			  $rowg->fedcha='';$rowg->nfdant='';$anter=0;
			  $rowg->pesb='';$rowg->fecant='';$rowg->aliant='';$rowg->kgshas='';$rowg->feccha1='';$rowg->res='';$rowg->codcha='';
			  $rowg->numcha='';$rowg->procha='';$rowg->facsis='';$rowg->kgssis='';$rowg->incksa='';$rowg->pr='';$rowg->kgstec='';$rowg->inckta='';
			  $rowg->modcha='';$rowg->pesbfd='';$rowg->r0='';$rowg->r1='';$rowg->r2='';$rowg->r3='';$rowg->kgd='';
			  $rowg->kgsha='';$rowg->res3='';$rowg->rac1='';$modant=0;$chaant=0;
			  //busca los gramos pasados
		      $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$est' and fecb < '$fecact' group by fecb,pesb order by fecb DESC limit 1");
			  foreach($query->result() as $rowgrs):
				$rowg->pesb =number_format($rowgrs->pesb,1,".","");
			  endforeach; 
			  $query=$this->db->query("select feccha as fecante,kgd,fedcha,modcha,numcha,porbal,porsoy from $this->tablacha where idpischa='$est' and feccha < '$fecact' and fedcha > 0 group by feccha,kgd,fedcha,modcha,numcha,porbal,porsoy order by feccha DESC limit 1");
				foreach($query->result() as $rowali):	
					$inicio = strtotime($rowali->fecante);$modant=$rowali->modcha;$chaant=$rowali->numcha;
    				$fin = strtotime($fecact);
    				$dif = $fin - $inicio;
    				$dias = (( ( $dif / 60 ) / 60 ) / 24);
					if($dias==1){
						if($rowali->kgd>0){ $rowg->aliant=number_format(($rowali->kgd), 0, '.', ',');$anter=$rowg->aliant;}
						$rowg->fecant=$fec->fecha21($rowali->fecante);
						$totkgda+=number_format($rowali->kgd, 0, '.', ',');
						if($rowali->fedcha>0) {$rowg->nfdant=$rowali->fedcha; $rowg->fedcha=$rowg->nfdant;}
						$rowg->kgshas=number_format($rowali->kgd/$rowg->hasg, 1, '.', ',');
						
					}
					$entro=1;
				endforeach;	
			  if($modant>0) $rowg->modcha=$modant;
			  if($chaant>0) $rowg->numcha=$chaant;
			  $rkgd=0;
			  $this->db->select('idcha,feccha,fedcha,codcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,numcha,modcha,r0,r1,r2,r3,porbal,porsoy');
			  //$this->db->select('idcha,feccha,ch1,turno,pb,kgd,idpischa,ciccha,numgracha,pr,kgt,rac');
			  $this->db->where('idpischa =',$rowg->idpis);
			  if($fm==1 or $fm==3) $this->db->where('fedcha >',0); else $this->db->where('fedcha =',0);
			  if($filter['where']!=''){	$this->db->where($filter['where']); }
			  $result = $this->db->get($this->tablacha);
			  foreach ($result->result() as $row):
				$id=$row->idcha;$actid=$row->idcha;$rowg->ciccha=$row->ciccha; $peso=0;$rr0=0;$rr1=0;$rr2=0;$rr3=0;
				if($row->feccha>0) $rowg->feccha=$row->feccha; 
				if($row->fedcha>0) $rowg->fedcha=$row->fedcha; 
				$rowg->kgt=$row->kgt;$rowg->rac=$row->rac; $rowg->codcha=$row->codcha;
				//$rowg->numcha=$row->numcha;
				if($row->numcha>0) $rowg->numcha=$row->numcha; else $rowg->numcha=$chaant;
				if($row->modcha>0) $rowg->modcha=$row->modcha; else $rowg->modcha=$modant; 
				$rowg->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
				$rowg->res=($row->codcha);$res=$rowg->procha;
				$rowg->res3='';	
				$rowg->idpischa=$row->idpischa;
				$rowg->idcha=$row->idcha;
				$rowg->feccha1=$fec->fecha21($row->feccha);
				$facs=0;
				if($gra==4){
					//select porfac,porbio as porbio from facgra2_22 where resfac<='2.33' order by resfac DESC limit 1	
					$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
				}
				foreach($queryfac->result() as $rowfac):	
					$facs=$rowfac->porfac;
					$rowg->facsis=number_format(($rowfac->porfac), 0, '.', ',');
				endforeach;			
				$rowg->kgssis=number_format(($anter+(($anter*$facs)/100)), 0, '.', ',');$totkgds+=number_format($rowg->kgssis, 0, '.', ',');
				$rowg->incksa=number_format($rowg->kgssis-$anter, 0, '.', ',');
				$dataz=array($this->kgs=>$rowg->kgssis);	
				$this->db->where($this->idcha,$actid);
				$this->db->update($this->tablacha,$dataz);
				if($row->kgt==0){$rowg->pr='';}
				else{
					if($anter>0){
					$rowg->pr=number_format((($row->kgt-$anter)/$anter)*100, 1, '.', ',');
					}
				}	
				if($row->kgt!=0) {
					$rowg->kgstec=number_format($row->kgt, 0, '.', ',');
					$rowg->kgsbal=number_format($row->porbal, 1, '.', ','); //$totbal+=$rowg->kgsbal;
					if($rowg->kgsbal==0) $rowg->kgsbal=''; 
					if($row->porsoy>0){$rowg->kgssoy=number_format($row->porsoy, 1, '.', ','); //$totsoy+=$rowg->kgssoy;
					}
					$rowg->inckta=number_format($rowg->kgstec-$anter, 0, '.', ',');
					if($row->rac>0) $rowg->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ','); else  $rowg->rac1='';
					$totkgd+=$rowg->kgstec;
					if($rowg->fedcha>0) $peso=number_format($rowg->kgstec/$rowg->fedcha, 2, '.', ',');
				}else{
					$rowg->kgstec='';$rowg->rac1='';
				}
				$rowg->kgsha=number_format($rowg->kgstec/$rowg->hasg, 2, '.', ',');
				if($rowg->kgsha==0) $rowg->kgsha='';
				if($fm==1){$rowg->estali='';$rowg->pesali='';}
				if($row->porsoy>0 and $fm==3){
					$this->db->select('max(idcha)');	
					$resultali = $this->db->get($this->tablacha);		
					foreach ($resultali->result() as $rowalif):
						$rowalif->estali=$rowg->pisg;
						$rowalif->hasali= number_format($rowg->hasg,1);
						$rowalif->totali=$rowg->kgssoy;
						$data[] = $rowalif;
					endforeach;	
					switch ($rowg->secc) {
						case '41': $totkgds1+=$rowg->kgssoy;break;
						case '42': $totkgds2+=$rowg->kgssoy;break;
						case '43': $totkgds3+=$rowg->kgssoy;break;
						case '44': $totkgds4+=$rowg->kgssoy;break;
					}
				}elseif($row->porbal>0 and $fm==1){
					$rowg->pesbfd=$peso;
					$rowg->kgd=$row->kgd;
					$rowg->modcha='';
					$this->db->select('max(idcha)');	
					$resultali = $this->db->get($this->tablacha);		
					foreach ($resultali->result() as $rowalif):
						$rowalif->estali=$rowg->pisg;
						$rowalif->totali=$rowg->kgsbal;
						if($row->codcha>0) $rowalif->codcha=$row->codcha; else $rowalif->codcha='';
						$rowalif->pesali=$peso;
						if($rowg->kgsbal>=25){$rowalif->sacos=intval($rowg->kgsbal/25);
							$rowalif->kilos=number_format($rowg->kgsbal-($rowalif->sacos*25),1);
							if($rowalif->kilos==0) $rowalif->kilos='';
						}else{
							$rowalif->sacos='';$rowalif->kilos='';
						}
						$data[] = $rowalif;
					endforeach;	
					switch ($rowg->secc) {
						case '41': $totkgds1+=$rowg->kgsbal;break;
						case '42': $totkgds2+=$rowg->kgsbal;break;
						case '43': $totkgds3+=$rowg->kgsbal;break;
						case '44': $totkgds4+=$rowg->kgsbal;break;
					}
				}else{
					if($row->r0>0){ $rowg->r0=number_format($row->r0, 1, '.', ','); $rr0=$rowg->r0;}
					if($row->r1>0){ $rowg->r1=number_format($row->r1, 1, '.', ','); $rr1=$rowg->r1;}
					if($row->r2>0){ $rowg->r2=number_format($row->r2, 1, '.', ','); $rr2=$rowg->r2;}
					if($row->r3>0){ $rowg->r3=number_format($row->r3, 1, '.', ','); $rr3=$rowg->r3;}
					if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0){ $rowg->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 0, '.', ','); $rkgd=$rowg->kgd;}
					$this->db->select('max(idcha)');	
					$resultalim = $this->db->get($this->tablacha);		
					foreach ($resultalim->result() as $rowalim):
						$rowalim->estali=$rowg->pisg;
						if($hora==9){
							$rowalim->totali=$rowg->r0;
							$rowalim->r1=$rowg->r1;
							if($rowg->r0>0 || $rowg->r1>0) $data[] = $rowalim;
							switch ($rowg->secc) {
								case '41': $totkgds1+=$rr0+$rr1;break;
								case '42': $totkgds2+=$rr0+$rr1;break;
								case '43': $totkgds3+=$rr0+$rr1;break;
								case '44': $totkgds4+=$rr0+$rr1;break;
							}
						}
						if($hora==3){
							$rowalim->totali=$rowg->r2;
							if($rowg->r2>0) $data[] = $rowalim;
							switch ($rowg->secc) {
								case '41': $totkgds1+=$rr2;break;
								case '42': $totkgds2+=$rr2;break;
								case '43': $totkgds3+=$rr2;break;
								case '44': $totkgds4+=$rr2;break;
							}
						}
						if($hora==7){
							$rowalim->totali=$rowg->r3;
							if($rowg->r3>0) $data[] = $rowalim;
							switch ($rowg->secc) {
								case '41': $totkgds1+=$rr3;break;
								case '42': $totkgds2+=$rr3;break;
								case '43': $totkgds3+=$rr3;break;
								case '44': $totkgds4+=$rr3;break;
							}
						}
						//$rowalif->totali=$row->kgd;
						//$rowalif->pesali=$peso;
						
					endforeach;	
				} 
				
				$totkgsreal+=$rkgd;
			  endforeach;
			  //$data[] = $rowg;
			endforeach;
			if($totkgds1>0 || $totkgds2>0 || $totkgds3>0 || $totkgds4>0){
				$this->db->select('max(idcha)');	
				$result = $this->db->get($this->tablacha);		
				foreach ($result->result() as $row):				
					$row->estali = "Tot";
					switch ($secc) {
						case '41': $row->totali=number_format($totkgds1, 0, '.', ',').' kg';break;
						case '42': $row->totali=number_format($totkgds2, 0, '.', ',').' kg';break;
						case '43': $row->totali=number_format($totkgds3, 0, '.', ',').' kg';break;
						case '44': $row->totali=number_format($totkgds4, 0, '.', ',').' kg';break;
					}
					$row->pesali='';$row->hasali='';$row->codcha='';
				  $data[] = $row;	
				endforeach;	
			}
			}else{
				//$this->db->select('idcha,idpis,pisg,hasg,idcha,feccha,ch1,ch2,ch3,ch4,ch5,ch6,turno,pb,kgd,idpischa,ciccha,numgracha,cp1,cp2,cp3,cp4,cp5,cp6,pr,kgt');
				$this->db->select('idcha,idpis,sum(pisg) as pisg,sum(hasg) as hasg,idcha,feccha,fedcha,codcha,modcha,numcha,kgd,idpischa,ciccha,numgracha,pr,kgt,rac,secc,r0,r1,r2,r3');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'right');
				$this->db->group_by($this->feccha);
				$this->db->group_by($this->idcha);
				$this->db->group_by($this->idpis);
				//$this->db->group_by($this->pisg);
				$this->db->order_by($this->idpischa,'ASC');
				$this->db->order_by($this->feccha,'ASC');
				if($filter['order']!='')
					$this->db->order_by($filter['order']);	
				if($filter['where']!=''){
					$this->db->where($filter['where']); }
				$result = $this->db->get($this->tablacha);
				//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
				$data = array(); $fec=new Libreria();$totkgd=0;$totkgda=0;$totkgds=0;$totkgdb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;$tothas=0;
				$totkgdsl2=0;$totkgdbl2=0;$totkgdb2=0;$totkgdsp3=0;$totkgdbp3=0;$porprom=0;$cprom=0;$porl2=0;$cl2=0;$peso=0;
				$pisi=0;$modant=0;$chaant=0;$totkgsreal=0;
				if($result->num_rows()>0){
					$ultimodia=$fecact;					
					foreach ($result->result() as $row):
						$id=$row->idcha;$fecact=$row->feccha;$pisi=$row->idpischa;$row->pesb ='';
						$row->nfdant='';$row->kgshas='';$row->procha='';
						$row->facsis='';$row->kgssis='';$row->incksa='';$row->pr='';
						$row->kgstec='';$row->inckta='';$row->pesbfd=''; //$row->r0='';$row->r1='';$row->r2='';$row->r3=''; 
						if($row->fedcha>0) $row->nfdant=$row->fedcha; 
						
						//if($row->idpis>=112 && $row->idpis<=128){
						//	$row->res3=($row->ch1);
						//	$row->res='';
						//}else{
							$row->res=($row->codcha);
							$row->res3='';	
						//}
						//$row->res=($row->ch1);
						$hecta=$row->hasg;$row->ciccha=$row->ciccha;
						//$res=($row->codcha);
						$res=number_format($row->codcha/$row->numcha, 2, '.', ',');
						
						
						 //busca los gramos pasados
					    $query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$pisi' and fecb < '$fecact' group by fecb,pesb order by fecb,pesb DESC limit 1");
						foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,1,".","");
						endforeach; 
						
						$row->resp=0; //($row->cp1);
						$resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6);
						$row->resp=0; //($row->cp1+$row->cp2+$row->cp3+$row->cp4+$row->cp5+$row->cp6); //$ultimodia=$row->feccha;
				
						if($row->resp>=0)$row->prom=number_format(($row->res+$row->resp)/2, 0, '.', ','); else $row->prom=number_format(($row->res), 0, '.', ',');
						$row->feccha1=$fec->fecha21($row->feccha);
						$row->hasg=$row->hasg;
						if($row->resp<0)$row->resp='';
						/*if($row->cp1==-1) $row->cp1='';if($row->cp2==-1) $row->cp2='';if($row->cp3==-1) $row->cp3='';if($row->cp4==-1) $row->cp4='';
						if($row->cp5==-1) $row->cp5='';if($row->cp6==-1) $row->cp6='';*/
						$est=$row->idpis;$estpes=$row->idpischa;
						$row->aliant=0;$entro=0;$anter=0;
						$row->aliant='';$row->fecant='';
						$row->pm='p.m.';
						if($gra==4) $row->ppm='70.00%'; else $row->ppm='80.00%';
						$query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=pisg where idpisb='$estpes' and fecb < '$fecact' group by fecb,pesb order by fecb DESC limit 1");
			  			foreach($query->result() as $rowgrs):
							$row->pesb =number_format($rowgrs->pesb,2,".","");
			  			endforeach; 
						
						$query=$this->db->query("select feccha as fecante,kgd,modcha,numcha from aligracha_22 where idpischa='$est' and feccha < '$fecact' group by feccha,kgd,modcha,numcha order by feccha DESC limit 1");
						foreach($query->result() as $rowali):	
							$row->aliant=number_format(($rowali->kgd), 0, '.', ',');$anter=$row->aliant;
							$modant=$rowali->modcha;$chaant=$rowali->numcha;
							$row->fecant=$fec->fecha21($rowali->fecante);
							$row->kgshas=number_format($rowali->kgd/$row->hasg, 1, '.', ',');
							$entro=1;
						endforeach;	
						//if($modant>0) $row->modcha=$modant;
			  			if($chaant>0) $row->numcha=$chaant;
						if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
						if($row->modcha>0) $row->modcha=$row->modcha; // else $row->modcha=$modant; 
						$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
						if($entro==0){
							$row->ch1='';$row->ch2='';$row->ch3='';$row->ch4='';$row->ch5='';$row->ch6='';$row->res='';$row->resp='';$row->prom='';
							$row->facsis='';$row->facbio='';
							$row->kgssis=number_format(($row->kgd), 0, '.', ',');$totkgds+=number_format($row->kgssis, 0, '.', ',');
							$row->kgsbio=number_format(($row->kgd), 0, '.', ',');$totkgdb+=number_format($row->kgsbio, 0, '.', ',');
						}else{
							$facs=0;
								//if($row->numcha>0) $row->numcha=$row->numcha; else $row->numcha=$chaant;
								//if($row->modcha>0) $row->modcha=$row->modcha; else $row->modcha=$modant; 
								$row->procha=number_format($row->codcha/$row->numcha, 2, '.', ',');
								
								if($gra==4){
									//$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where $res=resfac ");
									$queryfac=$this->db->query("select porfac,porbio as porbio from facgra2_22 where resfac<=$res order by resfac DESC limit 1");
								}
								
								foreach($queryfac->result() as $rowfac):	
									$facs=$rowfac->porfac;
									//$rowg->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facsis=number_format(($rowfac->porfac), 2, '.', ',');
									$row->facbio=number_format(($rowfac->porbio), 2, '.', ',');
								endforeach;			
							$row->kgssis=number_format(($row->aliant+(($row->aliant*$rowfac->porfac)/100)), 0, '.', ',');$totkgds+=number_format($row->kgssis, 0, '.', ',');
							$row->kgsbio=number_format(($row->aliant+(($row->aliant*$rowfac->porbio)/100)), 0, '.', ',');$totkgdb+=number_format($row->kgsbio, 0, '.', ',');
							$row->incksa=number_format($row->kgssis-$row->aliant, 0, '.', ',');
						}	
						$row->pr='';	
						if($row->kgt==0){ 
							$row->pr=0;
						}else{
							if($row->aliant>0){
								$row->pr=number_format((($row->kgt-$row->aliant)/$row->aliant)*100, 2, '.', ',');
							}
						}	
						if($row->kgt!=0) {
							
							if($row->rac>0) $row->rac1=$row->rac.' de '.number_format($row->kgt/$row->rac, 2, '.', ',').' kg'; else  $row->rac1='';
							
						}else{
							$row->rac1='';
						}
						$kgstec=$row->kgt;
						$row->kgstec=number_format($row->kgt, 0, '.', ',');$totkgd+=$row->kgstec;
						if($row->fedcha>0) $peso=number_format($row->kgstec/$row->fedcha, 0, '.', ',');
						
						$row->inckta=number_format($kgstec-$anter, 0, '.', ',');
						if($row->fedcha>0){
							$row->pesbfd=$peso;
							$row->kgd=$row->kgd;
							$row->modcha='';$row->r0='';$row->r1='';$row->r2='';$row->r3='';
						}else{
							if($row->r0>0 ||$row->r1>0 || $row->r2>0 || $row->r2>0) $row->kgd=number_format($row->r0+$row->r1+$row->r2+$row->r3, 0, '.', ',');	
							if($row->r0>0) $row->r0=number_format($row->r0, 1, '.', ','); else $row->r0=''; 	
							if($row->r1>0) $row->r1=number_format($row->r1, 1, '.', ','); else $row->r1='';
							if($row->r2>0) $row->r2=number_format($row->r2, 1, '.', ','); else $row->r2='';
							if($row->r3>0) $row->r3=number_format($row->r3, 1, '.', ','); else $row->r3='';
							
						} 
						$row->kgsha=number_format($row->kgd/$row->hasg, 2, '.', ',');
						$row->kgshapro=number_format($row->kgssis/$row->hasg, 2, '.', ',');
						switch ($row->secc) {
							case '41': $totkgds1+=$row->kgd;break;
							case '42': $totkgds2+=$row->kgd;break;
							case '43': $totkgds3+=$row->kgd;break;
							case '44': $totkgds4+=$row->kgd;break;
						}
						
						if($entro==1){
							if($row->res<5){
								if($gra==4){
									$row->kgsam=number_format((($row->kgstec*30)/100), 2, '.', ','); 
									$row->kgspm=number_format((($row->kgstec*70)/100), 2, '.', ',');}
								else{
									//$row->kgsam=number_format((($row->kgstec*20)/100), 2, '.', ','); 
									//$row->kgspm=number_format((($row->kgstec*80)/100), 2, '.', ',');	
								}
				 			}else{
				 				$row->kgsam='';
								$row->kgspm=number_format((($row->kgstec)), 2, '.', ',');
				 			}
						}else{
							$row->kgsam='';$row->kgspm='';
						}
						$totkgsreal+=$row->kgd;
						$esta = $row->pisg;$row->piscina=$row->pisg;
						$luna='';
						//INGRESAR LAS LUNAS
						$this->db->select('edolun');
						$this->db->where('feclun =',$row->feccha);
						$this->db->where('ciclun =',substr($cicl, 2, 2));
						$resultalun = $this->db->get('lunas');
						foreach ($resultalun->result() as $rowlun):
							$luna=$rowlun->edolun; 
						endforeach;
						$row->feccha1=$fec->fecha21($row->feccha).'<br/> Kgs: '.$row->kgd.'<br/>'.$luna;
						$data[] = $row;
					endforeach;	
					
			}	
		 }	
		 return $data;
		}

		public function getDatosaliacu($filter,$fec,$numgra,$ciclo){
			//SELECT idpischa,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(kgt*(porsoy/100)) as tkgs,sum(kgt*(porbal/100)) as tkgb 
			//from aligracha_22 where ciccha='2021-1' and feccha<'2021-03-25' group by idpischa
			$this->db->select('pisg,hasg,secc,sum(kgs) as tkgsi,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(porsoy) as tkgs,sum(porbal) as tkgb');
			$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'join');
			$this->db->group_by('idpischa');$this->db->group_by('hasg');$this->db->group_by('secc');
			if($filter['where']!=''){	$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablacha);
			$data = array();
			if($result->num_rows()>0){
				$thas=0;$tsi=0;$ts=0;$tb=0;$stsi=0;$sts=0;$stb=0;$atsi=0;$ats=0;$atb=0;$ssb=0;$asb=0;$totkgds1=0;$totkgds2=0;$totkgds3=0;$totkgds4=0;
				$totkgds1s=0;$totkgds1b=0;$totkgds2s=0;$totkgds2b=0;$totkgds3s=0;$totkgds3b=0;$totkgds4s=0;$totkgds4b=0;
			foreach ($result->result() as $row):
				$row->semsoy='';$row->acusoy='';$row->totsoy='';
				$row->sembal='';$row->acubal='';$row->totbal='';
				$row->semsb='';$row->acusb='';$row->soybal='';$row->kgshas='';
				$row->semsi='';$row->acusi='';$row->sybsi='';$row->kghasi='';
				
				$row->hasg=number_format($row->hasg, 1, '.', ',');$thas+=$row->hasg;
				
				if($row->tkgs>0){ $row->totsoy=number_format(($row->tkgs), 1, '.', ',');$ts+=$row->totsoy;}
				if($row->tkgb>0){ $row->totbal=number_format(($row->tkgb), 1, '.', ',');$tb+=$row->totbal;}
				$row->soybal=number_format(($row->tkgd), 1, '.', ',');
				if($row->tkgsi>0){ $row->sybsi=number_format(($row->tkgsi), 1, '.', ',');$tsi+=$row->sybsi;}
				//suma los kgs de la semana
				//SELECT idpischa,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(kgt*(porsoy/100)) as tkgs,sum(kgt*(porbal/100)) as tkgb 
				//from aligracha_22 where ciccha='2021-1' and feccha>'2021-03-18' and feccha<'2021-03-25' group by idpischa
				$diasem= date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $fec ) ))));
				$this->db->select('sum(kgs) as tkgsi,sum(kgt) as tkgt,sum(kgd) as tkgd,sum(porsoy) as tkgs,sum(porbal) as tkgb');
				$this->db->join($this->tabla, $this->idpis.'='.$this->idpischa,'join');
				$this->db->where('feccha >=',$diasem);
				if($filter['where']!=''){	$this->db->where($filter['where']); }
				$this->db->where('idpischa =',$row->pisg);
				$this->db->group_by('idpischa');
				$resultant = $this->db->get($this->tablacha);
				foreach ($resultant->result() as $rowant):
					if($rowant->tkgs>0){$row->semsoy=number_format(($rowant->tkgs), 1, '.', ',');$sts+=$row->semsoy;}
					if(($row->totsoy-$row->semsoy)>0){$row->acusoy=number_format(($row->totsoy-$row->semsoy), 1, '.', ',');$ats+=$row->acusoy;}
					if($rowant->tkgb>0){$row->sembal=number_format(($rowant->tkgb), 1, '.', ',');$stb+=$row->sembal;}
					if(($row->totbal-$row->sembal)>0){$row->acubal=number_format(($row->totbal-$row->sembal), 1, '.', ',');$atb+=$row->acubal;}
					if($row->semsoy+$row->sembal>0){$row->semsb=number_format($row->semsoy+$row->sembal, 1, '.',',');$ssb+=$row->semsb;
						$row->kgshas=number_format(($row->semsb/$row->hasg), 2, '.', ',');
						$row->kghasi=number_format(($row->tkgsi/$row->hasg), 2, '.', ',');
					}
					if(($row->soybal-$row->semsb)>0){$row->acusb=number_format(($row->soybal-$row->semsb), 1, '.', ',');$asb+=$row->acusb;}
					if($rowant->tkgsi>0){$row->semsi=number_format(($rowant->tkgsi), 1, '.', ',');$stsi+=$row->semsi;}
					if(($row->sybsi-$row->semsi)>0){$row->acusi=number_format(($row->sybsi-$row->semsi), 1, '.', ',');$atsi+=$row->acusi;}
				endforeach;	
				
				switch ($row->secc) {
					case '41': $totkgds1+=$row->tkgd;$totkgds1s+=$row->semsoy;$totkgds1b+=$row->sembal;break;
					case '42': $totkgds2+=$row->tkgd;$totkgds2s+=$row->semsoy;$totkgds2b+=$row->sembal;break;
					case '43': $totkgds3+=$row->tkgd;$totkgds3s+=$row->semsoy;$totkgds3b+=$row->sembal;break;
					case '44': $totkgds4+=$row->tkgd;$totkgds4s+=$row->semsoy;$totkgds4b+=$row->sembal;break;
				}
				$data[] = $row;
			endforeach;	
			$this->db->select('max(idcha)');	
			$result = $this->db->get($this->tablacha);		
			foreach ($result->result() as $row):				
				$row->pisg = "Tot";$row->hasg='';
				$row->semsoy='';$row->acusoy='';$row->totsoy='';
				$row->sembal='';$row->acubal='';$row->totbal='';
				$row->semsb='';$row->acusb='';$row->soybal='';$row->kgshas='';
				$row->semsi='';$row->acusi='';$row->sybsi='';$row->kghasi='';
				if($thas>0){ $row->hasg=number_format($thas, 1, '.', ',');}
				if($tsi>0){ $row->sybsi=number_format($tsi, 1, '.', ',');}
				if($stsi>0){ $row->semsi=number_format($stsi, 1, '.', ',');}
				if($atsi>0){ $row->acusi=number_format($atsi, 1, '.', ',');}
				if($ts>0){ $row->totsoy=number_format($ts, 1, '.', ',');}
				if($sts>0){ $row->semsoy=number_format($sts, 1, '.', ',').'</br>Soya';}
				if($ats>0){ $row->acusoy=number_format($ats, 1, '.', ',').'</br>AcuAnt';}
				if($tb>0){ $row->totbal=number_format($tb, 1, '.', ',');}
				if($stb>0){ $row->sembal=number_format($stb, 1, '.', ',').'</br>Bal';}
				if($atb>0){ $row->acubal=number_format($atb, 1, '.', ',').'</br>AcuAnt';}
				if($ssb>0){ $row->semsb=number_format($ssb, 1, '.', ',').'</br>Ambos';}
				if($asb>0){ $row->acusb=number_format($asb, 1, '.', ',').'</br>AcuAnt';}
				if($tb+$ts>0){ $row->soybal=number_format(($ts+$tb), 1, '.', ',');}
				if($thas>0){ $row->kgshas=number_format(($ts+$tb)/$thas, 2, '.', ',');$row->kghasi=number_format(($tsi)/$thas, 2, '.', ',');}
				$row->totkgds1=number_format($totkgds1, 1, '.', ',').' kg';$row->totkgds1s=number_format($totkgds1s, 1, '.', ',');$row->totkgds1b=number_format($totkgds1b, 1, '.', ',');
				$row->totkgds2=number_format($totkgds2, 1, '.', ',').' kg';$row->totkgds2s=number_format($totkgds2s, 1, '.', ',');$row->totkgds2b=number_format($totkgds2b, 1, '.', ',');                         
				$row->totkgds3=number_format($totkgds3, 1, '.', ',').' kg';$row->totkgds3s=number_format($totkgds3s, 1, '.', ',');$row->totkgds3b=number_format($totkgds3b, 1, '.', ',');
				$row->totkgds4=number_format($totkgds4, 1, '.', ',').' kg';$row->totkgds4s=number_format($totkgds4s, 1, '.', ',');$row->totkgds4b=number_format($totkgds4b, 1, '.', ',');
			  $data[] = $row;	
			endforeach;	
			}
			return $data;	 
		}

    }
    
?>
