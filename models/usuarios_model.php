<?php
    class Usuarios_model extends CI_Model{
    	public $id="id";
		public $usuario="usuario";
		public $password="pass";
		public $correo="correo";
		public $perfil="perfil";
		public $tabla="usuarios";
		
		function __Construct(){
			parent::__construct();
			$this->load->database();
		}
		
		function log_in($visitante,$password){
			//$this->db->select($this->id." ,".$this->correo." ,".$this->usuario." ,".$this->perfil);
			$this->db->select($this->id." ,".$this->correo." ,".$this->usuario." ,".$this->perfil.",(SELECT descripcion from perfiles where clave=".$this->perfil.") AS descripcion");
			$this->db->from($this->tabla);
			$this->db->where($this->correo,$visitante);
			$this->db->where($this->password,MD5($password));
			$query=$this->db->get();
			if($query->num_rows()>0) 
			return $query->row();
			else
			return '0';	
		}
		function log_per($perfi){
			$this->db->select('clave,descripcion');
			$this->db->from('perfiles');
			$this->db->where('clave',$perfi);			
			$query=$this->db->get();
			return $query->row();
		}
    }
?>