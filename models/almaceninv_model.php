<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almaceninv_model extends CI_Model {
        public $tabla="proveedores";
		
		public $tabladep="departamentoa";
		
		public $numsal="NumSal";public $fecs="FecS";public $nummate="NumMatE";public $cans="CanS";public $ndeps="NDepS";public $cbs="CBS";
		public $tablasal="almsal_23";
		 
		public $nummat="NumMat";public $cb="CB";public $nommat="NomMat";public $ana="Analizar";public $gpo="Grupo";public $exis="Existencia";public $stock="Stock";
		public $tablamat="almmat";
		
		public $nument="NumEnt";public $fece="FecE";public $cbe="CBE";public $prov="prov";public $face="FacE";public $cane="CanE";public $pres="pres";
		public $pree="PreE";public $dole="DolE";public $tce="TcE";public $exist="Exis";
		public $tablaent="alment_23";
		
		public $tablaOrdD="ordendet";
		
		public $tablasie="siegra_23";

//select NomMat from almmat inner join almsal_23 on CBS=CB where Analizar=1 and Grupo='alimento' and NDepS=17

        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		 //SELECT NomDepA,CBS,NomMat,SUM(CanS) FROM departamentoa INNER JOIN (almmat INNER JOIN almsal_23 ON CB=CBS )ON NDepS=NDepA WHERE Analizar='1' AND (NDepA<70 OR (NDepA>=220 AND NDepA<=222)) GROUP BY NomDepA,NomMat ORDER BY NomDepA,NomMat
		public function quitarent($id,$tab,$cam,$cic){
			//busca si hay salidas las quita tambien
			//DELETE FROM usuarios
			//WHERE edad>35 and edad<50
			$this->db->where($this->nummate,$id);
			$this->db->delete('almsal_'.$cic);
			//borra entrada
			$this->db->where($cam,$id);
			$this->db->delete($tab.$cic);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}

		function getconsumociclo($ao,$ciclo,$gra){
			//SELECT CBS,NomMat FROM departamentoa INNER JOIN (almmat INNER JOIN almsal_23 ON CB=CBS )ON NDepS=NDepA WHERE Analizar='1' AND (NDepA<70 OR (NDepA>=220 AND NDepA<=222)) GROUP BY NomMat ORDER BY NomMat
			$data = array();$est1=0;
			$query=$this->db->query("SELECT CBS,NomMat FROM departamentoa INNER JOIN (almmat INNER JOIN almsal_$ao ON CB=CBS )ON NDepS=NDepA WHERE Analizar='1' AND (NDepA<70 OR (NDepA>=220 AND NDepA<=222)) GROUP BY NomMat ORDER BY NomMat");
			foreach($query->result() as $row):
				$est=1;$tot=0;
				$row->d1='';$row->d2='';$row->d3='';$row->d4='';$row->d5='';$row->d6='';$row->d7='';$row->d8='';
				$row->d9='';$row->d10='';
				$row->d11='';$row->d12='';$row->d13='';$row->d14='';$row->d15='';$row->d16='';$row->d17='';
				$row->d18='';$row->d19='';$row->d20='';
				$row->d21='';$row->d22='';$row->d23='';$row->d24='';$row->d25='';$row->d26='';$row->d27='';
				$row->d28='';$row->d29='';$row->d30='';
				$row->d31='';$row->d32='';$row->d33='';$row->d34='';$row->d35='';$row->d36='';$row->d37='';
				$row->d38='';$row->d39='';$row->d40='';
				$row->d41='';$row->d42='';$row->d43='';$row->d44='';$row->d45='';$row->d46='';$row->d47='';
				$row->d48='';$row->d49='';$row->d50='';
				$row->d51='';$row->d52='';$row->d53='';$row->d54='';$row->d55='';$row->d56='';$row->d57='';
				$row->d58='';$row->d59='';$row->d60='';
				$row->d61='';$row->d62='';$row->d63='';$row->d64='';$row->d65='';$row->d66='';$row->d67='';
				$row->d68='';$row->d69='';$row->d70='';
				$row->d71='';$row->d72='';$row->d73='';$row->d74='';$row->d75='';$row->d76='';$row->d77='';
				$row->d78='';$row->d79='';$row->d80='';;$row->tot='';
				while($est<=80){
					$con=0;$dia='';$ini='';
					$querydia=$this->db->query("SELECT fecg,fecgc FROM siegra_$ao where numgra='$gra' and pisg='$est' and cicg='2023-$ciclo'");
					foreach($querydia->result() as $rowdia):
						$ini=$rowdia->fecg;
						if(is_null($rowdia->fecgc))$dia=date('Y-m-d'); else $dia=$rowdia->fecgc;
					endforeach; 
					if($est<=77){
						$queryce=$this->db->query("SELECT SUM(CanS) as can FROM almsal_$ao  where CBS='$row->CBS' and NDepS='$est' and (FecS>='$ini' and FecS<='$dia')"); //fecgc
					}else{
						switch ($est) {
							case '78': $est1=220;break;case '79': $est1=221;break;case '80': $est1=222;break;
						}
						$queryce=$this->db->query("SELECT SUM(CanS) as can FROM almsal_$ao where CBS='$row->CBS' and NDepS='$est1' and (FecS>='$ini' and FecS<='$dia')");	
					}
					foreach($queryce->result() as $rowce):
						$con=number_format($rowce->can,0,'.',',');$tot+=$rowce->can;;
					endforeach; 
					if($con>0){	
						switch ($est) {
							case '1': $row->d1=$con; break;
							case '2': $row->d2=$con; break;
							case '3': $row->d3=$con; break;
							case '4': $row->d4=$con; break;
							case '5': $row->d5=$con; break;
							case '6': $row->d6=$con; break;
							case '7': $row->d7=$con; break;
							case '8': $row->d8=$con; break;
							case '9': $row->d9=$con; break;
							case '10': $row->d10=$con; break;
							case '11': $row->d11=$con; break;
							case '12': $row->d12=$con; break;
							case '13': $row->d13=$con; break;
							case '14': $row->d14=$con; break;
							case '15': $row->d15=$con; break;
							case '16': $row->d16=$con; break;
							case '17': $row->d17=$con; break;
							case '18': $row->d18=$con; break;
							case '19': $row->d19=$con; break;
							case '20': $row->d20=$con; break;
							case '21': $row->d21=$con; break;
							case '22': $row->d22=$con; break;
							case '23': $row->d23=$con; break;
							case '24': $row->d24=$con; break;
							case '25': $row->d25=$con; break;
							case '26': $row->d26=$con; break;
							case '27': $row->d27=$con; break;
							case '28': $row->d28=$con; break;
							case '29': $row->d29=$con; break;
							case '30': $row->d30=$con; break;
							case '31': $row->d31=$con; break;
							case '32': $row->d32=$con; break;
							case '33': $row->d33=$con; break;
							case '34': $row->d34=$con; break;
							case '35': $row->d35=$con; break;
							case '36': $row->d36=$con; break;
							case '37': $row->d37=$con; break;
							case '38': $row->d38=$con; break;
							case '39': $row->d39=$con; break;
							case '40': $row->d40=$con; break;
							case '41': $row->d41=$con; break;
							case '42': $row->d42=$con; break;
							case '43': $row->d43=$con; break;
							case '44': $row->d44=$con; break;
							case '45': $row->d45=$con; break;
							case '46': $row->d46=$con; break;
							case '47': $row->d47=$con; break;
							case '48': $row->d48=$con; break;
							case '49': $row->d49=$con; break;
							case '50': $row->d50=$con; break;
							case '51': $row->d51=$con; break;
							case '52': $row->d52=$con; break;
							case '53': $row->d53=$con; break;
							case '54': $row->d54=$con; break;
							case '55': $row->d55=$con; break;
							case '56': $row->d56=$con; break;
							case '57': $row->d57=$con; break;
							case '58': $row->d58=$con; break;
							case '59': $row->d59=$con; break;
							case '60': $row->d60=$con; break;
							case '61': $row->d61=$con; break;
							case '62': $row->d62=$con; break;
							case '63': $row->d63=$con; break;
							case '64': $row->d64=$con; break;
							case '65': $row->d65=$con; break;
							case '66': $row->d66=$con; break;
							case '67': $row->d67=$con; break;
							case '68': $row->d68=$con; break;
							case '69': $row->d69=$con; break;
							case '70': $row->d70=$con; break;
							case '71': $row->d71=$con; break;
							case '72': $row->d72=$con; break;
							case '73': $row->d73=$con; break;
							case '74': $row->d74=$con; break;
							case '75': $row->d75=$con; break;
							case '76': $row->d76=$con; break;
							case '77': $row->d77=$con; break;
							case '78': $row->d78=$con; break;
							case '79': $row->d79=$con; break;
							case '80': $row->d80=$con; break;
						};
					};	
					$est+=1;
				}	
				if($tot>0) $row->tot=number_format($tot,0,'.',',');
				$data[] = $row;
			endforeach; 
			return $data; 	
		}	
		function getconsultas($filter){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			//$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			//$this->db->join($this->tablamat, 'cb=cbe','inner');
			$this->db->order_by($this->fece,'DESC');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablaent);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				//$str1 = <<<EOF $row->NomMat EOF;
				$row->FecE = $fec->fecha($row->FecE);
				$row->PreE= '$ '.$row->PreE;
				if($row->DolE>0) $row->DolE= '$ '.$row->DolE; else  $row->DolE='';
				if($row->Exis>0) $row->Exis= $row->Exis.' '.$row->pres; else $row->Exis='';
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		function getconsultasmov($filter,$cod){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			//$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			//$this->db->join($this->tablamat, 'cb=cbe','inner');
			$query=$this->db->query("select prov,FecE,FacE,CanE,pres from alment_23 where CBE='$cod' Union all select NomDepA,FecS,('s') as sal,CanS,NdepS from departamentoa inner join (almsal_23 inner join alment_23 on NumMatE=NumEnt) on NDepS=NDepA where CBS='$cod' order by FecE");
			//$this->db->order_by($this->fece,'DESC');
			//if($filter['where']!=''){$this->db->where($filter['where']); }
			//$result = $this->db->get($this->tablaent);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$query->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();$exis=0;
			//Se forma el arreglo que sera retornado							
			foreach ($query->result() as $row):
				//$str1 = <<<EOF $row->NomMat EOF;
				$row->FecE = $fec->fecha($row->FecE);
				
				if($row->FacE=='s') {$row->ent='';$row->FacE='';$row->sal=$row->CanE;$row->Exis=$exis-$row->CanE;}
				else {$row->ent=$row->CanE;$row->sal='';$row->Exis=$exis+$row->CanE;}
				$exis=$row->Exis;
				//$row->PreE= '$ '.$row->PreE;
				//if($row->DolE>0) $row->DolE= '$ '.$row->DolE; else  $row->DolE='';
				//if($row->Exis>0) $row->Exis= $row->Exis.' '.$row->pres; else $row->Exis='';
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		function borrar($id,$exisa,$mat){
			//buscar la Existencia de entrada y restarle la salida
			//$exis=0;
			//$exis=$exisa-$can;
			$data=array($this->exist=>$exisa);
			$this->db->where($this->nument,$mat);
			$this->db->update($this->tablaent,$data);
			//Registra lo borrado	
			$this->db->where($this->numsal,$id);
			$this->db->delete($this->tablasal);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function agregars($cb,$fec,$mat,$can,$dep,$exisa){
			//buscar la Existencia de entrada y restarle la salida
			$exis=0;
			$exis=$exisa-$can;
			$data=array($this->exist=>$exis);
			$this->db->where($this->nument,$mat);
			$this->db->update($this->tablaent,$data);
			//Registra la salida
			$data=array($this->cbs=>$cb,$this->fecs=>$fec,$this->nummate=>$mat,$this->cans=>$can,$this->ndeps=>$dep);		
			$this->db->insert($this->tablasal,$data);
			return $this->db->insert_id();
		}
		
		public function actualizars($id,$fec,$mat,$can,$dep,$exisa){
			//buscar la Existencia de entrada y restarle la salida
			$exis=0;
			$exis=$exisa-$can;
			$data=array($this->exist=>$exis);
			$this->db->where($this->nument,$mat);
			$this->db->update($this->tablaent,$data);
			//Registra la salida
			$data=array($this->fecs=>$fec,$this->cans=>$can,$this->ndeps=>$dep);		
			$this->db->where($this->numsal,$id);
			$this->db->update($this->tablasal,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function actualizare($id,$fec,$prov,$fac,$cane,$pres,$pre,$dol,$tc){
			//obtener la existencia actual
			$ent=0;$exi=0;$exis=0;
			$this->db->select('CanE,Exis');
			$this->db->where($this->nument,$id);
			$resultt = $this->db->get($this->tablaent);
			foreach ($resultt->result() as $rowt):				
				$ent = $rowt->CanE;
				$exi = $rowt->Exis;
			endforeach;
			if($cane>$ent) $exis=$cane-$ent; else $exis=$ent-$cane;
			$exi+=$exis; 
			$data=array($this->fece=>$fec,$this->prov=>$prov,$this->face=>$fac,$this->cane=>$cane,$this->pres=>$pres,$this->pree=>$pre,$this->dole=>$dol,$this->tce=>$tc,$this->exist=>$exi); 
			$this->db->where($this->nument,$id);
			$this->db->update($this->tablaent,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregare($cb,$fec,$prov,$fac,$can,$pres,$pre,$dol,$tc){
			$data=array($this->cbe=>$cb,$this->fece=>$fec,$this->prov=>$prov,$this->face=>$fac,$this->cane=>$can,$this->pres=>$pres,$this->pree=>$pre,$this->dole=>$dol,$this->tce=>$tc,$this->exist=>$can);		
			$this->db->insert($this->tablaent,$data);
			return $this->db->insert_id();
		}
		public function actualizarcb($id,$nom,$ana,$sto){
			if($sto=='') $sto=0;
			$data=array($this->nommat=>$nom,$this->ana=>$ana,$this->stock=>$sto); 
			$this->db->where($this->nummat,$id);
			$this->db->update($this->tablamat,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarcb($cb,$nom,$ana,$sto){
			if($sto=='') $sto=0;
			$data=array($this->cb=>$cb,$this->nommat=>$nom,$this->ana=>$ana,$this->stock=>$sto);		
			$this->db->insert($this->tablamat,$data);
			return $this->db->insert_id();
		}
		
		// para pasar de excel a sql
		//LOAD DATA INFILE 'c:/materiales2.csv' INTO TABLE materiales FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';
		function getmaterialesgral($filter){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			//$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			//$this->db->join($this->tabla, 'prov=numero','inner');
			$this->db->select('NumMat,CB,NomMat,Grupo,Analizar,Stock,(select sum(Exis) from alment_23 where CBE=CB)as Existencia');
			$this->db->order_by($this->nommat);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablamat);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				//$str1 = <<<EOF $row->NomMat EOF;
				//$row->NomMat= addslashes($row->NomMat);
				//$cadena= $row->NomMat;
				//$row->NomMat= addslashes($cadena_con_comillas_dobles);
				if($row->Stock>0) $row->Stock=$row->Stock; else $row->Stock='';
				if($row->Existencia>0) $row->Existencia=$row->Existencia; else $row->Existencia='';
				$row->NomMat=  htmlspecialchars($row->NomMat, ENT_COMPAT); 
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		
		function existencia($mat){
			$this->db->select('Exis');
			$this->db->where($this->nument,$mat);
			$result = $this->db->get($this->tablaent);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			foreach ($result->result() as $row):
				$data[] = $row;	
			endforeach;	
			}
			return $data;
		}
		
		function getentradas($filter){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			//$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			$this->db->join($this->tablamat, 'cb=cbe','inner');
			$this->db->order_by($this->prov);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablaent);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				//$str1 = <<<EOF $row->NomMat EOF;
				//$row->NomMat= addslashes($row->NomMat);
				//$cadena= $row->NomMat;
				//$row->NomMat= addslashes($cadena_con_comillas_dobles);
				$row->NomMat=  htmlspecialchars($row->NomMat, ENT_COMPAT); 
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		
		
		function getexistencia($filter){
			//select des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe from conceptos inner join( proveedores inner join fc14 on prov=Numero) on con=nrc group by des,Razon
			//$this->db->select('nrc,des,Razon,sum(can) as cantidad,uni,sum(can*pre) as importe,(sum(can*pre)/sum(can))as ppact,proyeccion,anterior,impant,(impant/anterior)as ppant,dp');
			$this->db->join($this->tablamat, 'cb=cbe','inner');
			$this->db->where('Exis >', 0);
			//$this->db->order_by($this->prov);
			$this->db->order_by($this->fece);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablaent);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$row->FecE = $fec->fecha($row->FecE);
				//$str1 = <<<EOF $row->NomMat EOF;
				//$row->NomMat= addslashes($row->NomMat);
				//$cadena= $row->NomMat;
				//$row->NomMat= addslashes($cadena_con_comillas_dobles);
				//$row->NomMat=  htmlspecialchars($row->NomMat, ENT_COMPAT); 
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		/*select	prov,FecE,Face,CanE,pres,PreE,DolE,TcE from alment_23 where month(FecE)=1 and CBE='3000000' order by prov,FecE
		select	* from alment_23 where month(FecE)=1 and CBE='3000000' order by prov,FecE
		select	FecS,CanS,NumMatE,NDepS from almsal_23 where month(FecS)=1 and CBS='3000000'  order by FecS
		select	* from almsal_23 where month(FecS)=1 and CBS='3000000'  order by FecS*/
		
		//select	NumEnt,prov,FecE,Face,CanE,pres,PreE,DolE,TcE from alment_23 where month(FecE)=1 and CBE='magnetizada'  
		// Union all 
		// select NumMatE,prov,FecS,('s') as sal,CanS,PreE,DolE,NomDepA,NDepS from departamentoa inner join (almsal_23 inner join alment_23 on NumMatE=NumEnt) on NDepS=NDepA where month(FecS)=1 and CBS='magnetizada' 
		// order by FecE,NumEnt
		
		
		
		function getinv($filter,$mes,$dt){
			//select CB,NomMat,(select sum(CanE) from alment_23 where CBE=CB and month(FecE) = '2')as Ent,(select sum(CanS) from almsal_23 where CBS=CB and month(FecS) = '2')as Sal 
			//from almmat where Analizar>0 order by Grupo,NomMat
			$this->db->select('CB,NomMat,(select sum(CanE) from alment_23 where CBE=CB and month(FecE) = '.$mes.')as Ent,(select sum(CanS) from almsal_23 where CBS=CB and month(FecS) = '.$mes.')as Sal');
			$this->db->where('Analizar >', 0);
			//$this->db->order_by('Grupo');
			$this->db->order_by('NomMat');
			$result = $this->db->get($this->tablamat);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();
			foreach ($result->result() as $row):
				$cod=$row->CB;
				if($row->Ent>0 || $row->Sal>0){
					//presentacion
					$this->db->select('pres');
					$this->db->where('CBE', $row->CB);
					$result = $this->db->get($this->tablaent);
					foreach ($result->result() as $rowp):
						$pre=$rowp->pres;
					endforeach;	
					if($row->Ent>0) $row->CanE1=number_format($row->Ent, 3, '.', ',').' '.$pre; else $row->CanE1='';
					if($row->Sal>0) $row->CanS1= number_format($row->Sal, 3, '.', ',').' '.$pre; else $row->CanS1='';
					//if($row->Ent-$row->Sal!=0)$row->Exis=number_format($row->Ent-$row->Sal, 3, '.', ',').' '.$pre; else $row->Exis='';
					$row->prov='';$row->dia ='';$row->fac ='';$row->pre ='';$row->cm ='';$row->ca ='';
					$data[] = $row;	
					//$query=$this->db->query("select NumEnt,prov,FecE,Face,CanE,pres,PreE,DolE,TcE from alment_23 where month(FecE)=1 and CBE='magnetizada'  Union all select NumMatE,prov,FecS,('s') as sal,CanS,NdepS,PreE,NomDepA,NDepS from departamentoa inner join (almsal_23 inner join alment_23 on NumMatE=NumEnt) on NDepS=NDepA where month(FecS)=1 and CBS='magnetizada' order by FecE,NumEnt");
					if($dt==0){
						$query=$this->db->query("select NumEnt,prov,FecE,Face,sum(CanE) as CanE,pres,PreE,DolE,TcE,('') as depa from alment_23 where month(FecE)='$mes' and CBE='$cod' group by prov,depa,NumEnt,Fece,Face,pres,PreE,DolE Union all select count(NumMatE) as NumMatE,prov,count(FecS) as FecS,('s') as sal,sum(CanS) as CanS,count(NdepS) as NdepS,count(PreE) as PreE,count(DolE) as DolE,count(TcE) as TcE,NomDepA from departamentoa inner join (almsal_23 inner join alment_23 on NumMatE=NumEnt) on NDepS=NDepA where month(FecS)='$mes' and CBS='$cod' group by prov,NomDepA order by FacE,depa");
					}else{
						$query=$this->db->query("select NumEnt,prov,FecE,Face,CanE,pres,PreE,DolE,TcE,('') as depa from alment_23 where month(FecE)='$mes' and CBE='$cod' Union all select NumMatE,prov,FecS,('s') as sal,CanS,NdepS,PreE,DolE,TcE,NomDepA from departamentoa inner join (almsal_23 inner join alment_23 on NumMatE=NumEnt) on NDepS=NDepA where month(FecS)='$mes' and CBS='$cod' order by FecE,depa");		
					}	
					
					$ant=0;$suma=0;
					foreach($query->result() as $rowdet):
						if($rowdet->NumEnt>0){
						$rowdet->NomMat='';$rowdet->CanE1='';$rowdet->CanS1=''; //$rowdet->Exis='';
						if($rowdet->TcE>0) {$rowdet->PreE=$rowdet->DolE;}
						if($rowdet->Face!='s'){
							$rowdet->dia = $fec->fecha22($rowdet->FecE);
							 $rowdet->fac = $rowdet->Face;
							 $rowdet->CanE1 =$rowdet->CanE;$rowdet->CanS1 ='';
							 if($rowdet->TcE>0) $dol =' (USD)'; else $dol ='(MN)';
							$rowdet->prov=$rowdet->dia.'- '.ucwords(strtolower($rowdet->prov)).$dol;
							$rowdet->pre='$ '.number_format($rowdet->PreE, 2, '.', ',');
							$rowdet->cm='$ '.number_format($rowdet->CanE*$rowdet->PreE, 2, '.', ','); 
							$suma=1;
						} else {
							$rowdet->fac ='';$rowdet->CanE1 ='';
							$rowdet->CanS1 =number_format($rowdet->CanE, 3, '.', ',');
							if($dt==0){ $rowdet->dia ='';$rowdet->prov=$rowdet->depa; $rowdet->pre='';$rowdet->cm='';}
							else {$rowdet->dia = $fec->fecha22($rowdet->FecE);$rowdet->prov=$rowdet->dia.'- '.$rowdet->depa; $rowdet->pre='$ '.number_format($rowdet->PreE, 2, '.', ',');
							$rowdet->cm='$ '.number_format($rowdet->CanE*$rowdet->PreE, 2, '.', ',');
							}
							$suma=0;
						}
						if($dt==0 and $rowdet->Face=='s'){
							$rowdet->ca='';
						}else{
							
						if($suma==0){
							$rowdet->ca='$ '.number_format(($ant-($rowdet->CanE*$rowdet->PreE)), 2, '.', ',');
							$ant-=$rowdet->CanE*$rowdet->PreE;
						}
						else{
							$rowdet->ca='$ '.number_format(($ant+($rowdet->CanE*$rowdet->PreE)), 2, '.', ',');
							$ant+=$rowdet->CanE*$rowdet->PreE;
						}
						}
						$data[] = $rowdet;	
						}			
					endforeach;	
				}
			endforeach;	
			}
			return $data;
		}
		function getinv1($filter,$mes){
			//select CB,NomMat,(select sum(CanE) from alment_23 where CBE=CB and month(FecE) = '2')as Ent,(select sum(CanS) from almsal_23 where CBS=CB and month(FecS) = '2')as Sal 
			//from almmat where Analizar>0 order by Grupo,NomMat
			$this->db->select('CB,NomMat,(select sum(CanE) from alment_23 where CBE=CB and month(FecE) = '.$mes.')as Ent,(select sum(CanS) from almsal_23 where CBS=CB and month(FecS) = '.$mes.')as Sal');
			$this->db->where('Analizar >', 0);
			//$this->db->order_by('Grupo');
			$this->db->order_by('NomMat');
			$result = $this->db->get($this->tablamat);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();
			foreach ($result->result() as $row):
				if($row->Ent>0 || $row->Sal>0){
					/*if($gpo!=$row->Grupo){
						$gpo=$row->Grupo; 
					}else{ $row->Grupo="";}*/
					//presentacion
					$this->db->select('pres');
					$this->db->where('CBE', $row->CB);
					$result = $this->db->get($this->tablaent);
					foreach ($result->result() as $rowp):
						$pre=$rowp->pres;
					endforeach;	
					if($row->Ent>0) $row->CanE1=number_format($row->Ent, 3, '.', ',').' '.$pre; else $row->CanE1='';
					if($row->Sal>0) $row->CanS1= number_format($row->Sal, 3, '.', ',').' '.$pre; else $row->CanS1='';
					if($row->Ent-$row->Sal!=0)$row->Exis=number_format($row->Ent-$row->Sal, 3, '.', ',').' '.$pre; else $row->Exis='';
					$data[] = $row;	
				}
			endforeach;	
			}
			return $data;
		}
		function getsalidas($filter){
			//SELECT FecS,NomDepA,NomMat,CanS,pres,PreE,(CanS*PreE)as tot from 
			// departamentoa inner join( alment_23 inner join( almmat inner join almsal_23 on CBS=CB) on NumEnt=NumMatE) on NDepA=NDepS where FecS='2117-09-30' 
			$this->db->select('NumSal,NumMatE,FecS,CBS,NDepA,NomDepA,NomMat,CanS,pres,PreE,(CanS*PreE)as tot');
			$this->db->join($this->tabladep, 'NDepA=NDepS','inner');
			$this->db->join($this->tablaent, 'NumEnt=NumMatE','inner');
			$this->db->join($this->tablamat,'CBS=CB' ,'inner');
			$this->db->order_by($this->numsal,'DESC');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablasal);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				$row->FecS1 = $fec->fecha($row->FecS);
				$row->PreE1="$ ". number_format($row->PreE, 2, '.', ',');
				$row->CanS1= $row->CanS.' '.$row->pres; 
				$row->tot="$ ". number_format($row->tot, 2, '.', ',');
				$data[] = $row;	
			endforeach;	
			
			}
			return $data;
		}
		function getsalidasdep($filter,$dt){
			//SELECT FecS,NomDepA,NomMat,CanS,pres,PreE,(CanS*PreE)as tot from 
			// departamentoa inner join( alment_23 inner join( almmat inner join almsal_23 on CBS=CB) on NumEnt=NumMatE) on NDepA=NDepS where FecS='2117-09-30' 
			
			if($dt==0){
				$this->db->select('NDepA,NomDepA,NomMat,sum(CanS) as CanS,sum(CanS*PreE)as tot,pres');
			}else{	
				//$this->db->select('NDepA,NomDepA,NomMat,sum(CanS) as CanS,pres,PreE,sum(CanS*PreE)as tot,DolE,TcE');
				$this->db->select('NumSal,NumMatE,FecS,CBS,NDepA,NomDepA,NomMat,CanS,pres,PreE,(CanS*PreE)as tot');
			}
			$this->db->join($this->tabladep, 'NDepA=NDepS','inner');
			$this->db->join($this->tablaent, 'NumEnt=NumMatE','inner');
			$this->db->join($this->tablamat,'CBS=CB' ,'inner');
			$this->db->order_by('NomDepA');$this->db->order_by('NomMat');
			$this->db->order_by('FecS');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($dt==0){
				$this->db->group_by('NomMat');
				$this->db->group_by('pres');
			}else{	
				$this->db->group_by('NDepA');
				$this->db->group_by('NomMat');
				$this->db->group_by('PreE');
				$this->db->group_by('pres');
				$this->db->group_by('DolE');
				$this->db->group_by('TcE');	
			}
			$result = $this->db->get($this->tablasal);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();$toti=0;$deps='';$dia='';
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($deps!=$row->NDepA){
					$deps=$row->NDepA; 
				}else{ $row->NomDepA="";}
				if($dt==1){
				if($dia!=$row->FecS){
					$row->FecS1 = $fec->fecha($row->FecS);
					$dia=$row->FecS;
				}else{ $row->FecS1="";}  
				}else{ $row->FecS1="";}  
				$toti+=$row->tot;
				if($dt==1){
				$row->PreE1="$ ". number_format($row->PreE, 2, '.', ',');
				}else{$row->PreE1='';}
				$row->CanS1= number_format($row->CanS, 2, '.', ',').' '.$row->pres; 
				$row->tot="$ ". number_format($row->tot, 2, '.', ',');
				$data[] = $row;	
			endforeach;	
			$this->db->select('max(NumSal)');	
			$result = $this->db->get($this->tablasal);
			foreach ($result->result() as $row):				
				$row->NomDepA = "Total: "; $row->FecS1="";$row->NomMat="";$row->CanS1="";$row->PreE1="";
				if($toti>0) $row->tot='$ '.number_format($toti, 2, '.', ','); else $row->tot='';
				$data[] = $row;	
			endforeach;	
			}
			return $data;
		}
		//Salidas mensuales departamentos
		//SELECT NomDepA,NomMat,sum(CanS),pres,PreE,sum(CanS*PreE)as tot from departamentoa inner join( alment_23 inner join( almmat inner join almsal_23 on CBS=CB) on NumEnt=NumMatE) on NDepA=NDepS where Analizar>0 and month(FecS)=11 group by NomDepA,NomMat,PreE,pres
		
		function getsalidasdepm($filter,$dt,$dep){
			//SELECT FecS,NomDepA,NomMat,CanS,pres,PreE,(CanS*PreE)as tot from 
			// departamentoa inner join( alment_23 inner join( almmat inner join almsal_23 on CBS=CB) on NumEnt=NumMatE) on NDepA=NDepS where FecS='2117-09-30' 
			if($dt==0){
				$this->db->select('NomDepA,NomMat,sum(CanS) as CanS,sum(CanS*PreE)as tot,pres');
			}else{	
			$this->db->select('NDepA,NomDepA,NomMat,sum(CanS) as CanS,pres,PreE,sum(CanS*PreE)as tot,DolE,TcE');
			}	
			$this->db->join($this->tabladep, 'NDepA=NDepS','inner');
			$this->db->join($this->tablaent, 'NumEnt=NumMatE','inner');
			$this->db->join($this->tablamat,'CBS=CB' ,'inner');
			$this->db->where('Analizar >', 0);
			$this->db->group_by('NomDepA');
			if($dt==0){
				$this->db->group_by('NomMat');
				$this->db->group_by('pres');
			}else{	
			$this->db->group_by('NDepA');
			$this->db->group_by('NomMat');
			$this->db->group_by('PreE');
			$this->db->group_by('pres');
			$this->db->group_by('DolE');
			$this->db->group_by('TcE');	
			}
			
			
			//$this->db->group_by('Cans');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablasal);
			$can=$result->num_rows();
			$data = array();
			if($can>0){
			$fec=new Libreria();$toti=0;$deps='';$mat='';$totcan=0;$totimp=0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($dt==0){
					//$row->NomDepA = ""; 
					if($deps!=$row->NomDepA){
					$deps=$row->NomDepA; 
					}else{ $row->NomDepA="";}
				
					$row->PreE1="";$row->CanS1="";
					$row->CanS1= number_format($row->CanS, 3, '.', ',').' '.$row->pres; 
					$row->tot="$ ". number_format($row->tot, 2, '.', ',');
					$row->DolE1='';$row->TcE1='';
					$data[] = $row;	
				}	
				else{	
				//detalle
				if($totcan>0 && $row->NomMat != $mat){
				//if($totcan>0 ){
					$this->db->select('max(Numero)');	
					$resulta = $this->db->get('clientes');
					foreach ($resulta->result() as $rowa):				
						$rowa->NomDepA = ""; 
						$rowa->CanS1=number_format($totcan, 3, '.', ',');
						$rowa->tot="$ ". number_format($totimp, 2, '.', ',');
						$rowa->NomMat="Total:";$rowa->PreE1="";
						$rowa->DolE1='';$rowa->TcE1='';
						$data[] = $rowa;	
					endforeach;		
					$totcan=0;$totimp=0;
				}
				if($deps!=$row->NDepA){
					$deps=$row->NDepA; 
				}else{ $row->NomDepA="";}
				
				if($mat!=$row->NomMat){
					//$row->FecS1 = $fec->fecha($row->FecS);
					$mat=$row->NomMat;
				}else{ $row->NomMat="";}
				  
				$toti+=$row->tot;$totcan+=$row->CanS;$totimp+=$row->tot;
				$row->PreE1="$ ". number_format($row->PreE, 2, '.', ',');
				if($row->DolE>0) $row->DolE1="$ ". number_format($row->DolE, 2, '.', ','); else $row->DolE1='';
				if($row->TcE>0) $row->TcE1="$ ". number_format($row->TcE, 2, '.', ','); else $row->TcE1='';
				$row->CanS1= number_format($row->CanS, 3, '.', ',').' '.$row->pres; 
				$row->tot="$ ". number_format($row->tot, 2, '.', ',');
				$data[] = $row;	
				}
			endforeach;	
			if($dt==1){
			if($totcan>0 ){
				//if($totcan>0 ){
					$this->db->select('max(Numero)');	
					$resulta = $this->db->get('clientes');
					foreach ($resulta->result() as $rowa):				
						$rowa->NomDepA = ""; 
						$rowa->CanS1=number_format($totcan, 3, '.', ',');
						$rowa->tot="$ ". number_format($totimp, 2, '.', ',');
						$rowa->NomMat="Total:";$rowa->PreE1="";
						$rowa->DolE1='';$rowa->TcE1='';
						$data[] = $rowa;	
					endforeach;		
					$totcan=0;$totimp=0;
				}
			$this->db->select('max(NumSal)');	
			$result = $this->db->get($this->tablasal);
			foreach ($result->result() as $row):				
				$row->NomDepA = "Total:"; $row->NomMat="";$row->CanS1="";$row->PreE1="";
				if($toti>0) $row->tot='$ '.number_format($toti, 2, '.', ','); else $row->tot='';
				$row->DolE1='';$row->TcE1='';
				$data[] = $row;	
			endforeach;
			}	
			if($dep==0){
			//agrupaciones de cosnumos por mes	
			$this->db->select('NomMat,sum(CanS) as CanS,sum(CanS*PreE)as tot,pres');
			$this->db->join($this->tabladep, 'NDepA=NDepS','inner');
			$this->db->join($this->tablaent, 'NumEnt=NumMatE','inner');
			$this->db->join($this->tablamat,'CBS=CB' ,'inner');
			$this->db->where('Analizar >', 0);
			$this->db->group_by('NomMat');
			$this->db->group_by('pres');
			//$this->db->group_by('tot');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablasal);$cont=1;
			foreach ($result->result() as $row):				
				if($cont==1) {$row->NomDepA = "TOTALES AGRUPADOS"; $cont=0;} else {$row->NomDepA ='';} 
				$row->PreE1="";$row->CanS1="";$row->co=1;
				$row->CanS1= number_format($row->CanS, 3, '.', ',').' '.$row->pres; 
				$row->tot="$ ". number_format($row->tot, 2, '.', ',');
				$row->DolE1='';$row->TcE1='';
				$data[] = $row;	
			endforeach;
			
			}
			}
			return $data;
		}
		function historycb($cb){
			//select max(numfn) as ultimo from facnot where tipfn=1									
			$this->db->select('NomMat');
			$this->db->where($this->cb, $cb);
			$query = $this->db->get($this->tablamat);
			if($query->num_rows()>0)
				return $query->row();
			else {
				return 0;
			}
		}
		function getProves(){
			$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->group_by('Razon');
			$this->db->group_by('Numero');
			$this->db->group_by('Siglas');
			$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getDeptos(){
			//$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->order_by('NomDepA');
			$result = $this->db->get($this->tabladep);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getAlimento(){
			$this->db->select('NumEnt,CBE,NomMat,FacE,CanE,pres,NumMat,Exis');
			$this->db->join('alment_23', 'CB=CBE','inner');
			$this->db->where('Analizar =',1);
			$this->db->order_by('NomMat');
			$result = $this->db->get($this->tablamat);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->Exis= number_format($row->Exis, 3, '.', ',').' '.$row->pres; 
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getConsumos($filter,$ali,$mes){
			//$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			//$this->db->where('Analizar =',1);
			//$this->db->order_by('NomMat');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$result = $this->db->get($this->tablasie);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$ini=1; while($ini<=31){$datosd [$ini]='';$td [$ini]='';$ini+=1;}
			foreach($result->result() as $row):
				$est=$row->pisg;$tdg=0;
				//extrae el consumo del dia correspondiente al mes 
				$this->db->select('day(FecS) as dia,CanS');
				//$this->db->where('month(FecS) =','month('.$mes.')');
				$this->db->where('month(FecS) =',$mes);
				$this->db->where('NDepS =',$est);
				$this->db->where('NumMatE =',$ali);
				$resulta = $this->db->get($this->tablasal);
				$ini=1; while($ini<=31){$datosr [$ini]='';$ini+=1;}
				foreach($resulta->result() as $rowa):
					$dia=$rowa->dia;
					$datosd [$dia]=$rowa->dia;
					$datosr [$dia]=$rowa->CanS;
					$td [$dia]+=$rowa->CanS;$tdg+=$rowa->CanS;
				endforeach;	
				$ini=1;
				while($ini<=31){
					switch($ini){
						case 1: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d1 = number_format($datosr [$ini],2,'.',',');} else {  $row->d1 ="";} break;
						case 2: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d2 = number_format($datosr [$ini],2,'.',',');} else {  $row->d2 ="";} break;
						case 3: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d3 = number_format($datosr [$ini],2,'.',',');} else {  $row->d3 ="";} break;
						case 4: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d4 = number_format($datosr [$ini],2,'.',',');} else {  $row->d4 ="";} break;
						case 5: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d5 = number_format($datosr [$ini],2,'.',',');} else {  $row->d5 ="";} break;
						case 6: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d6 = number_format($datosr [$ini],2,'.',',');} else {  $row->d6 ="";} break;
						case 7: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d7 = number_format($datosr [$ini],2,'.',',');} else {  $row->d7 ="";} break;
						case 8: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d8 = number_format($datosr [$ini],2,'.',',');} else {  $row->d8 ="";} break;
						case 9: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d9 = number_format($datosr [$ini],2,'.',',');} else {  $row->d9 ="";} break;
						case 10: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d10 = number_format($datosr [$ini],2,'.',',');} else {  $row->d10 ="";} break;
						case 11: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d11 = number_format($datosr [$ini],2,'.',',');} else {  $row->d11 ="";} break;
						case 12: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d12 = number_format($datosr [$ini],2,'.',',');} else {  $row->d12 ="";} break;
						case 13: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d13 = number_format($datosr [$ini],2,'.',',');} else {  $row->d13 ="";} break;
						case 14: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d14 = number_format($datosr [$ini],2,'.',',');} else {  $row->d14 ="";} break;
						case 15: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d15 = number_format($datosr [$ini],2,'.',',');} else {  $row->d15 ="";} break;
						case 16: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d16 = number_format($datosr [$ini],2,'.',',');} else {  $row->d16 ="";} break;
						case 17: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d17 = number_format($datosr [$ini],2,'.',',');} else {  $row->d17 ="";} break;
						case 18: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d18 = number_format($datosr [$ini],2,'.',',');} else {  $row->d18 ="";} break;
						case 19: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d19 = number_format($datosr [$ini],2,'.',',');} else {  $row->d19 ="";} break;
						case 20: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d20 = number_format($datosr [$ini],2,'.',',');} else {  $row->d20 ="";} break;
						case 21: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d21 = number_format($datosr [$ini],2,'.',',');} else {  $row->d21 ="";} break;
						case 22: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d22 = number_format($datosr [$ini],2,'.',',');} else {  $row->d22 ="";} break;
						case 23: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d23 = number_format($datosr [$ini],2,'.',',');} else {  $row->d23 ="";} break;
						case 24: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d24 = number_format($datosr [$ini],2,'.',',');} else {  $row->d24 ="";} break;
						case 25: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d25 = number_format($datosr [$ini],2,'.',',');} else {  $row->d25 ="";} break;
						case 26: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d26 = number_format($datosr [$ini],2,'.',',');} else {  $row->d26 ="";} break;
						case 27: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d27 = number_format($datosr [$ini],2,'.',',');} else {  $row->d27 ="";} break;
						case 28: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d28 = number_format($datosr [$ini],2,'.',',');} else {  $row->d28 ="";} break;
						case 29: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d29 = number_format($datosr [$ini],2,'.',',');} else {  $row->d29 ="";} break;
						case 30: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d30 = number_format($datosr [$ini],2,'.',',');} else {  $row->d30 ="";} break;
						case 31: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d31 = number_format($datosr [$ini],2,'.',',');} else {  $row->d31 ="";} break;
					}
					$ini+=1;
				}
				if($tdg>0) $row->tot =number_format($tdg, 2, '.', ','); else $row->tot ='';
				$data[] = $row;	
				
			endforeach;
			//total dia
			$tg=0;
			$this->db->select('max(Numero)');
				$result = $this->db->get('clientes');
				foreach ($result->result() as $row):
					$row->pisg ="Tot";
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini]; } else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 =number_format(($td [$ini]), 2, '.', ',');$tg+=$td [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				endforeach;
				$row->tot =number_format($tg, 2, '.', ',');
				$data[] = $row;	
			return $data;
		}
    }
    
?>