<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Circal_model extends CI_Model {
        public $nfic="nfic";
		public $fecd="fecd";
		public $pro="pro";
		public $sol="sol";
		public $eje="eje";
		public $estatus="estatus";
		public $fecf="fecf";
		public $obs="obs";
		public $pri="priori";
		public $tabla="circal";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		//Salas
		function verDiaj(){
			$this->db->select('fecd');	
			$this->db->group_by('fecd');
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		function getcircal($filter){
			$this->db->select('nfic,fecd,pro,sol,eje,estatus,fecf,obs,priori,DATEDIFF( fecf,fecd ) AS termino,DATEDIFF( CURDATE(),fecd ) AS actual',NULL,FALSE);
			$this->db->order_by('fecd');
			//$this->db->order_by('priori','DESC');
			//$this->db->order_by('nfic');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }			
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$dia='';$cont=0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($dia!=$row->fecd){
					$row->fecdd = $fec->fecha21($row->fecd); $dia=$row->fecd;
				}else{
					$row->fecdd ='';
				}
				if($row->fecf!='0000-00-00'){
					 $row->fecff = $fec->fecha21($row->fecf)."<br> DT [".$row->termino."]";
					 $row->focodia =$row->termino;
				}else{
					$row->fecff = "DT [".$row->actual."]";$row->focodia =$row->actual;
					$row->fecf='';	
				}	
				$row->totp=($cont+=1); 			
				$data[] = $row;
			endforeach;					
			return $data;
		}
		
		function getNumRows($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados			
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function agregar($fecd,$pro,$sol,$eje,$estatus,$fecf,$obs,$pri){
			/*if(($fecf==0)){ //$fecf='0000-00-00';
				 $data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->obs=>$obs,$this->pri=>$pri);}	
			else{ $data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->fecf=>$fecf,$this->obs=>$obs,$this->pri=>$pri);}			
			
			 * 
			 */
			// $fecf='0';
			//$data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->fecf=>$fecf,$this->obs=>$obs,$this->pri=>$pri);
			$data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->obs=>$obs,$this->pri=>$pri);
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		
		public function actualizar($id,$fecd,$pro,$sol,$eje,$estatus,$fecf,$obs,$pri){
			if(($fecf==0)){ $fecf='0000-00-00';}
			if(($fecf=='') or ($estatus=='Proceso')) $data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->obs=>$obs,$this->pri=>$pri);
			else $data=array($this->fecd=>$fecd,$this->pro=>$pro,$this->sol=>$sol,$this->eje=>$eje,$this->estatus=>$estatus,$this->fecf=>$fecf,$this->obs=>$obs,$this->pri=>$pri);
			$this->db->where($this->nfic,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
	
    }
    
?>