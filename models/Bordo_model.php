<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Bordo_model extends CI_Model {        
        public $idcos="idcos";public $feccos="feccos";public $estcos="estcos";public $grscos="grscos";
        public $kgscos="kgscos";public $numcos="numcos";public $clicos="clicos";public $tipcos="tipcos";
        public $prebas="prebas";public $numgrab="numgrab";public $folio="folio";public $ncicos="ncicos";
        public $tabla="bordo";
		
		public $idpis="idpis";
		public $tablaest="siegra";		
		
		public $idpischa="idpischa";
		public $tablacha="chagra_22";
				
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function ultimofolio($cic){									
			/*$this->db->select('MAX(folio) as folio');
			$query = $this->db->get($this->tabla.'_'.$cic);*/
			$this->db->select('MAX(folio) as folio');
			$this->db->from($this->tabla.'_'.$cic);
			$query=$this->db->get();
			return $query->row();
			//return $query->result();
		}
		function historyhas($pil){
			$this->db->select($this->hasg);
			$this->db->from($this->tabla);
			$this->db->where('idpis =', $pil);
			$query=$this->db->get();
			return $query->row();
		}
		function verClientes(){
			$this->db->select('Numero,Razon');
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		
		/*
		function getElementsbB($filter,$ciclo){
			$this->db->select("idpis,pisg as val");   
			    
			if($filter['where']!='') $this->db->where($filter['where']);	
        	$this->db->where('numgra',4);
			$result = $this->db->get($this->tablaest.'_'.'19');        
			        
        	
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		*/
		function getElementsb($where,$cic){        
        	$this->db->select("idpis,pisg as val,cicg");     
			if($where['numgra']!=0){
				$this->db->where('numgra',$where['numgra']);
			}
	       $this->db->where('secc >',0);
	       $this->db->order_by('cicg');$this->db->order_by('pisg');
			$result = $this->db->get($this->tablaest.'_'.'22');
        	$data = array();        
        	foreach($result->result() as $row):
				$row->val='C'.substr($row->cicg, -1).'-'.$row->val;
				
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function getElementsbp($filest){        
			$ano=substr($filest, 0,2);
			$ciclo=substr($filest, 2,1);
			$granja=substr($filest, 3,2);
        	$this->db->select("idpis,pisg as val,cicg");     
			$this->db->where('numgra',$granja);
			if($ciclo!=0){$this->db->where('cicg','20'.$ano.'-'.$ciclo);}
	        $this->db->where('secc >',0);
	        $this->db->order_by('cicg');$this->db->order_by('pisg');
			$result = $this->db->get($this->tablaest.'_'.$ano);
        	$data = array();        
        	foreach($result->result() as $row):
				$row->val='C'.substr($row->cicg, -1).'-'.$row->val;
				
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function secciones($where,$cic){        
        	$this->db->select("secc,secc as val");     
			if($where['numgra']!=0){
			$this->db->where('numgra',$where['numgra']);
			
			}
	        $this->db->where('secc >',0);
			$this->db->group_by('secc');
			$result = $this->db->get($this->tablaest.'_'.'22');
        	$data = array();        
        	foreach($result->result() as $row):
        		switch($row->secc){
					case 61: $row->val='1'; break;	case 62: $row->val='2'; break;
					case 41: $row->val='1'; break;	case 42: $row->val='2'; break;	case 43: $row->val='3'; break;	
					case 44: $row->val='4'; break;
					case 101: $row->val='1'; break;
				}
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		function gral($filter,$cic){
			//SELECT	pisg,orgg,hasg,sum(kgscos) as kgs, avg(grscos) as pp from siegra_19 inner join bordo on estcos=idpis where numcos=1 group by estcos
			$this->db->select("idpis,pisg,orgg,hasg,fecg");
			$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->group_by('idpis');      
			$this->db->order_by('pisg');		
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array(); $totorg=0;$tothas=0;$totali=0;$totpri=0;$totseg=0;$totter=0;$totcua=0;$totqui=0;$totsex=0;$totfin=0;
			$tottot=0;$totpro=0;$cont=0;$totorgs=0;$dsi=0;
			$totvta=0;$totorgst=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$est=$row->idpis;$orgg=$row->orgg;$totorg+=$row->orgg;$tothas+=$row->hasg;
				$dsi=$row->fecg;$vtaest=0;
				$org1=0;$org2=0;$org3=0;$org4=0;$org5=0;$org6=0;$orgf=0;

				/*if($row->idpis>=1 && $row->idpis<=28) $row->pisg='H-I-'.$row->pisg;
				  elseif($row->idpis>=29 && $row->idpis<=56) $row->pisg='H-II-'.$row->pisg;
				  	elseif($row->idpis>=73 && $row->idpis<=99) $row->pisg='A-I-'.$row->pisg;
						elseif($row->idpis>=112 && $row->idpis<=128) $row->pisg='A-II-'.$row->pisg;
							elseif(($row->idpis>=100 && $row->idpis<=111) || ($row->idpis>=129 && $row->idpis<=149)) $row->pisg='A-III-'.$row->pisg;
				*/

					/*elseif($row->idpis>=73 && $row->idpis<=91) $row->pisg='A-I-'.$row->pisg;
					  elseif($row->idpis>=92 && $row->idpis<=111) $row->pisg='A-II-'.$row->pisg;
						elseif($row->idpis>=112 && $row->idpis<=128) $row->pisg='A-III-'.$row->pisg;
							elseif($row->idpis>=129 && $row->idpis<=149) $row->pisg='A-IV-'.$row->pisg;*/
							
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				$row->hasg = number_format($row->hasg, 3, '.', ',');
				/*
				//sumo el total del alimento actual consumido
				$this->db->select("sum(kgt) as ali");
				$this->db->join($this->tablacha, $this->idpischa.'='.$this->estcos,'inner'); 
				//$this->db->where($this->numcos,4);
				$this->db->where($this->idpischa,$est); 
				$resulta = $this->db->get($this->tabla);
				foreach($resulta->result() as $rowa):
					$totali+=$rowa->ali;
					if($rowa->ali>0){$row->ali =number_format($rowa->ali, 0, '.', ',');}else{$row->ali ='';}					
				endforeach;
				 */
				 
				//busco los de la primera precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,1);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result1 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result1->result() as $row1):
					if($row1->vta>0){$row->vta1 =number_format($row1->vta, 2, '.', ',');$vtaest+=$row1->vta;}else{$row->vta1 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,1);
				$this->db->where($this->estcos,$est); 
				$result1 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result1->result() as $row1):
					$totpri+=$row1->kgs;$org1=$row1->orgspre;
					if($row1->kgs>0){$row->kgs1 =number_format($row1->kgs, 0, '.', ',');}else{$row->kgs1 ='';}
					if($row1->pp>0){$row->pp1 =number_format($row1->pp, 2, '.', ',');}else{$row->pp1 ='';}				
					if($row1->pre>0 and $row1->tipcos==2){$row->pre1 =number_format($row1->pre, 2, '.', ',');}else{$row->pre1 ='';}
					//if($row1->vta>0 and $row1->tipcos==2){$row->vta1 =number_format($row1->vta, 2, '.', ',');$vtaest+=$row1->vta;}else{$row->vta1 ='';}
				endforeach;
				//busco los de la segunda precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,2);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result2 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result2->result() as $row2):
					if($row2->vta>0){$row->vta2 =number_format($row2->vta, 2, '.', ',');$vtaest+=$row2->vta;}else{$row->vta2 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,2);
				$this->db->where($this->estcos,$est); 
				$result2 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result2->result() as $row2):
					$totseg+=$row2->kgs;$org2=$row2->orgspre;
					if($row2->kgs>0){$row->kgs2 =number_format($row2->kgs, 0, '.', ',');}else{$row->kgs2 ='';}
					if($row2->pp>0){$row->pp2 =number_format($row2->pp, 2, '.', ',');}else{$row->pp2 ='';}
					if($row2->pre>0 and $row2->tipcos==2){$row->pre2 =number_format($row2->pre, 2, '.', ',');}else{$row->pre2 ='';}
					//if($row2->vta>0 and $row2->tipcos==2){$row->vta2 =number_format($row2->vta, 2, '.', ',');$vtaest+=$row2->vta;}else{$row->vta2 ='';}
				endforeach;
				//busco los de la tercera precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,3);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result3 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result3->result() as $row3):
					if($row3->vta>0){$row->vta3 =number_format($row3->vta, 2, '.', ',');$vtaest+=$row3->vta;}else{$row->vta3 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,3);
				$this->db->where($this->estcos,$est); 
				$result3 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result3->result() as $row3):
					$totter+=$row3->kgs;$org3=$row3->orgspre;
					if($row3->kgs>0){$row->kgs3 =number_format($row3->kgs, 0, '.', ',');}else{$row->kgs3 ='';}
					if($row3->pp>0){$row->pp3 =number_format($row3->pp, 2, '.', ',');}else{$row->pp3 ='';}
					if($row3->pre>0 and $row3->tipcos==2){$row->pre3 =number_format($row3->pre, 2, '.', ',');}else{$row->pre3 ='';}
					//if($row3->vta>0 and $row3->tipcos==2){$row->vta3 =number_format($row3->vta, 2, '.', ',');$vtaest+=$row3->vta;}else{$row->vta3 ='';}
				endforeach;
				//busco los de la cuarta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,4);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result4 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result4->result() as $row4):
					if($row4->vta>0){$row->vta4 =number_format($row4->vta, 2, '.', ',');$vtaest+=$row4->vta;}else{$row->vta4 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,4);
				$this->db->where($this->estcos,$est); 
				$result4 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result4->result() as $row4):
					$totcua+=$row4->kgs;$org4=$row4->orgspre;
					if($row4->kgs>0){$row->kgs4 =number_format($row4->kgs, 0, '.', ',');}else{$row->kgs4 ='';}
					if($row4->pp>0){$row->pp4 =number_format($row4->pp, 2, '.', ',');}else{$row->pp4 ='';}
					if($row4->pre>0 and $row4->tipcos==2){$row->pre4 =number_format($row4->pre, 2, '.', ',');}else{$row->pre4 ='';}
					//if($row4->vta>0 and $row4->tipcos==2){$row->vta4 =number_format($row4->vta, 2, '.', ',');$vtaest+=$row4->vta;}else{$row->vta4 ='';}
				endforeach;
				//busco los de la quinta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,5);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result5 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result5->result() as $row5):
					if($row5->vta>0){$row->vta5 =number_format($row5->vta, 2, '.', ',');$vtaest+=$row5->vta;}else{$row->vta5 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,5);
				$this->db->where($this->estcos,$est); 
				$result5 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result5->result() as $row5):
					$totqui+=$row5->kgs;$org5=$row5->orgspre;
					if($row5->kgs>0){$row->kgs5 =number_format($row5->kgs, 0, '.', ',');}else{$row->kgs5 ='';}
					if($row5->pp>0){$row->pp5 =number_format($row5->pp, 2, '.', ',');}else{$row->pp5 ='';}
					if($row5->pre>0 and $row5->tipcos==2){$row->pre5 =number_format($row5->pre, 2, '.', ',');}else{$row->pre5 ='';}
					//if($row5->vta>0 and $row5->tipcos==2){$row->vta5 =number_format($row5->vta, 2, '.', ',');$vtaest+=$row5->vta;}else{$row->vta5 ='';}
				endforeach;
				//busco los de la sexta precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,6);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$result6 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result6->result() as $row6):
					if($row6->vta>0){$row->vta6 =number_format($row6->vta, 2, '.', ',');$vtaest+=$row6->vta;}else{$row->vta6 ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,6);
				$this->db->where($this->estcos,$est); 
				$result6 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result6->result() as $row6):
					$totsex+=$row6->kgs;$org6=$row6->orgspre;
					if($row6->kgs>0){$row->kgs6 =number_format($row6->kgs, 0, '.', ',');}else{$row->kgs6 ='';}
					if($row6->pp>0){$row->pp6 =number_format($row6->pp, 2, '.', ',');}else{$row->pp6 ='';}
					if($row6->pre>0 and $row6->tipcos==2){$row->pre6 =number_format($row6->pre, 2, '.', ',');}else{$row->pre6 ='';}
					//if($row6->vta>0 and $row6->tipcos==2){$row->vta6 =number_format($row6->vta, 2, '.', ',');$vtaest+=$row6->vta;}else{$row->vta6 ='';}
				endforeach;
				//busco los de la final precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos*(grscos+prebas)) as vta");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,7);$this->db->where($this->estcos,$est);$this->db->where($this->tipcos,2);
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vta>0){$row->vtaf =number_format($rowf->vta, 2, '.', ',');$vtaest+=$rowf->vta;}else{$row->vtaf ='';}
				endforeach;
				$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,feccos,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numcos,7);
				$this->db->where($this->estcos,$est); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					$totfin+=$rowf->kgs;$orgf=$rowf->orgspre;
					if($rowf->kgs>0){$row->kgsf =number_format($rowf->kgs, 0, '.', ',');}else{$row->kgsf ='';}
					if($rowf->pp>0){$row->ppf =number_format($rowf->pp, 2, '.', ',');}else{$row->ppf ='';}
					if($rowf->pre>0){$row->pref =number_format($rowf->pre, 2, '.', ',');}else{$row->pref ='';}
					//if($rowf->vta>0){$row->vtaf =number_format($rowf->vta, 2, '.', ',');$vtaest+=$rowf->vta;}else{$row->vtaf ='';}
				endforeach;
				
				$fecha1 = new DateTime($dsi);
    			$fecha2 = new DateTime($rowf->feccos);
    			$resultado = $fecha1->diff($fecha2);
    			$row->dc=$resultado->format('%a');
				//$resultado->format('%R%a días');
				//busco los del total precosecha de acuerdo al estanque
				$this->db->select("sum(kgscos) as kgs, avg(grscos) as pp,sum(kgscos*(grscos+prebas)) as venta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				//$this->db->where($this->numcos,4);
				$this->db->where($this->estcos,$est); 
				$result5 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result5->result() as $row5):
					if($row5->kgs>0){
						$tottot+=$row5->kgs;
						$totorgs+=($row5->kgs*1000)/$row5->pp;
						$kgst=($row5->kgs*1000)/$row5->pp;
						//$row->fcat =number_format($rowa->ali/$row5->kgs, 2, '.', ',');
						//$row->kgsha =number_format($row5->kgs/$row->hasg, 0, '.', ',');	
						$row->orgst =number_format(($row5->kgs*1000)/$row5->pp, 0, '.', ',');	
						$row->kgst =number_format($row5->kgs, 0, '.', ',');
						//$row->sob =number_format(($kgst/($orgg*1000))*100, 2, '.', ',');
						if($orgg>0) $row->sob =number_format((($org1+$org2+$org3+$org4+$orgf)/($orgg))*100, 2, '.', ','); else $row->sob ='';
						//$row->vta ='$ '.number_format($row5->venta, 2, '.', ',');
						if($vtaest>0) $row->vta ='$ '.number_format($vtaest, 2, '.', ','); else $row->vta ='';
						//$totvta+=$row5->venta;
						
					}else{$row->kgst ='';$row->kgsha ='';$row->fca ='';$row->orgst ='';$row->sob ='';}
					if($row5->pp>0){$cont+=1;$totpro+=$row5->pp;$row->ppt =number_format($row5->pp, 1, '.', ',');}else{$row->ppt ='';}
				endforeach;
				$totorgst+=$org1+$org2+$org3+$org4+$org5+$orgf;
				$totvta+=$vtaest;
				$data[] = $row;
			endforeach;
			//total
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->pisg = "Total";$row->dc='';
				if($totorg>0)$row->orgg = number_format($totorg, 3, '.', ','); else $row->orgg ='';
				if($tothas>0)$row->hasg =number_format($tothas,3, '.', ','); else $row->hasg ='';
				//if($totali>0)$row->ali =number_format($totali, 0, '.', ','); else $row->ali =''; 
				if($totpri>0)$row->kgs1 =number_format($totpri, 0, '.', ','); else $row->kgs1 ='';
				$row->pp1='';$row->pre1='';$row->vta1='';
				if($totseg>0)$row->kgs2 =number_format($totseg, 0, '.', ','); else $row->kgs2 ='';
				$row->pp2='';$row->pre2='';$row->vta2='';
				if($totter>0)$row->kgs3 =number_format($totter, 0, '.', ','); else $row->kgs3 ='';
				$row->pp3='';$row->pre3='';$row->vta3='';
				if($totcua>0)$row->kgs4 =number_format($totcua, 0, '.', ','); else $row->kgs4 ='';
				$row->pp4='';$row->pre4='';$row->vta4='';
				if($totqui>0)$row->kgs5 =number_format($totqui, 0, '.', ','); else $row->kgs5 ='';
				$row->pp5='';$row->pre5='';$row->vta5='';
				if($totsex>0)$row->kgs6 =number_format($totsex, 0, '.', ','); else $row->kgs6 ='';
				$row->pp6='';$row->pre6='';$row->vta6='';
				if($totfin>0)$row->kgsf =number_format($totfin, 0, '.', ','); else $row->kgsf ='';
				$row->ppf='';$row->pref='';$row->vtaf='';
				if($tottot>0)$row->kgst =number_format($tottot, 0, '.', ','); else $row->kgst ='';
				if($totpro>0)$row->ppt =number_format($totpro/$cont, 2, '.', ','); else $row->ppt ='';
				//if($tottot>0)$row->kgsha =number_format($tottot/$tothas, 1, '.', ','); else $row->kgsha ='';
				//if($totali>0)$row->fcat =number_format($totali/$tottot, 2, '.', ','); else $row->fcat ='';
				if($totorgs>0)$row->orgst =number_format($totorgs, 0, '.', ','); else $row->orgst ='';
				//if($totorgs>0)$row->sob =number_format(($totorgs/($totorg*1000))*100, 2, '.', ','); else $row->sob ='';
				if($totorg>0)$row->sob =number_format(($totorgst/($totorg))*100, 2, '.', ','); else $row->sob ='';
				$row->vta ='$ '.number_format($totvta, 2, '.', ',');
				$data[] = $row;	
			endforeach;
			}
			return $data;			
		}

		function bordogral($filter,$cic){
			//SELECT	feccos, avg(grscos) as grs, sum(kgscos) kgs, sum((grscos+prebas)*kgscos) as imp from bordo group by feccos
			$this->db->select("feccos,folio,numgrab,clicos,Razon, avg(grscos) as grs, sum(kgscos) as kgs, sum((grscos+prebas)*kgscos) as imp");
			$this->db->join('clientes', 'Numero=clicos','inner');
			//$this->db->join($this->tablaest, $this->idpis.'='.$this->estcos,'inner');   
			
			$this->db->group_by($this->folio);
			$this->db->group_by($this->feccos); 
			$this->db->group_by($this->numgrab);
			$this->db->group_by($this->clicos);
			$this->db->order_by($this->feccos,'DESC');
			$this->db->order_by($this->folio,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array();  $fec=new Libreria(); 
			$impcv=0;$imp=0;$kgcv=0;
			if($result->num_rows()>0){
			//obtner el total de la compra venta
			//SELECT sum((grscos+prebas)*kgscos) as imp from bordo
			$this->db->select('sum((grscos+prebas)*kgscos) as imp,sum(kgscos) as kgt');	
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where($this->tipcos,2);
			$resulti = $this->db->get($this->tabla.'_'.$cic);
			foreach ($resulti->result() as $rowi):				
				$impcv=$rowi->imp;
			endforeach;	
			$this->db->select('sum((grscos+prebas)*kgscos) as imp,sum(kgscos) as kgt');	
			if($filter['where']!='') $this->db->where($filter['where']);
			$resulti = $this->db->get($this->tabla.'_'.$cic);
			foreach ($resulti->result() as $rowi):				
				$kgcv=$rowi->kgt;
			endforeach;	
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$imp=$row->imp;$kg=$row->kgs;
				$row->folio1=str_pad($row->folio, 5, "0", STR_PAD_LEFT);
				switch($row->numgrab){
					case 2: $row->Razon='Kino- '.$row->Razon; break;
					case 4: $row->Razon='Ahome- '.$row->Razon; break;
					case 5: $row->Razon='Oce. Azul '.$row->Razon; break;
					case 6: $row->Razon='Huatabampo- '.$row->Razon; break;					
					case 10: $row->Razon='Topolobampo- '.$row->Razon; break;					
				}
				$row->feccos1=$fec->fecha($row->feccos);
				$row->grs=number_format($row->grs, 2, '.', ',');
				$row->kgs=number_format($row->kgs, 0, '.', ',');
				$row->totk=number_format($kgcv, 0, '.', ',');
				if($row->clicos!=12  && $row->clicos!=2) $row->imp='$ '.number_format($row->imp, 2, '.', ','); else $row->imp='';
				$row->toti='$ '.number_format($impcv, 2, '.', ',');
				//$cont=1;
				//if($cont>0) 
				if($row->clicos!=12 && $row->clicos!=2) $impcv-=$imp;
				$kgcv-=$kg;
				$data[] = $row;
			endforeach;
			
			
			}			
			return $data;			
		}
		function bordo($filter,$cic){
			//select feccos,pisg,grscos,kgscos,clicos,numcos,tipcos from siegra_19 inner join bordo on estcos=idpis
			$this->db->select("idcos,feccos,pisg,grscos,kgscos,clicos,Razon,numcos,tipcos,estcos,prebas,numgrab,folio");
			$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner');       
			$this->db->join('clientes', 'Numero=clicos','inner');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->order_by($this->folio);		
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array();  $fec=new Libreria(); $dia=''; $maq=''; $cos=''; $cli='';$gra='';$tot=0;$totimp=0;$totg=0;$totgr=0;
			$granjas=1;$totd=0;$totimpd=0;$totgd=0;$totgrd=0;$fol=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//if($dia!=$row->feccos){
				if($gra!=$row->numgrab){
					if($tot>0){
						$this->db->select('max(idcos)');	
						$result = $this->db->get($this->tabla.'_'.$cic);
						foreach ($result->result() as $rowt):				
							$rowt->clicos1 = ""; $rowt->nomg = ""; $rowt->folio2 = "";
							$rowt->feccos1="";$rowt->tipcos1="";$rowt->numcos1="";$rowt->pisg="Total"; $rowt->prebas1='';$rowt->pregra='';
							if($totgr>0) $rowt->grscos=number_format($totgr/$totg, 2, '.', ','); else $rowt->grscos='';
							if($tot>0) $rowt->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $rowt->kgscos='';
							if($totimp>0) $rowt->imp='$ '.number_format($totimp, 2, '.', ','); else $row->impt='';
							$data[] = $rowt;	
						endforeach;
					   $tot=0;$maq=''; $cos=''; $cli='';$totgr=0;$totg=0;$totimp=0;$granjas+=1;
					}	
					$row->feccos1=$fec->fecha($row->feccos);$dia=$row->feccos;
				}else{ $row->feccos1="";} 
				if($gra!=$row->numgrab){
					switch($row->numgrab){
						case 2: $row->nomg='OA-Kino'; break;	case 4: $row->nomg='OA-Ahome'; break; case 5: $row->nomg='Oce. Azul';break;
						case 6: $row->nomg='OA-Huatabampo'; break;					
						case 10: $row->nomg='OA-Topolobampo'; break;					
					}
					$gra=$row->numgrab;
				}else{ $row->nomg="";} 
				if($maq!=$row->tipcos){
					switch($row->tipcos){
						case 1: $row->tipcos1='Maquila'; break;	case 2: $row->tipcos1='Venta'; break; case 3: $row->tipcos1='Donación'; break;					
					}
					$maq=$row->tipcos;
				}else{ $row->tipcos1="";}
				if($cos!=$row->numcos){
					switch($row->numcos){
						case 1: $row->numcos1='Primera'; break;	case 2: $row->numcos1='Segunda'; break;	case 3: $row->numcos1='Tercera'; break;
						case 4: $row->numcos1='Cuarta'; break; case 5: $row->numcos1='Quinta'; break; case 6: $row->numcos1='Sexta'; break; 
						case 7: $row->numcos1='Final'; break;
					}
					$cos=$row->numcos;
				}else{ $row->numcos1="";}	
				if($cli!=$row->clicos){
					$row->clicos1=$row->Razon;$cli=$row->clicos;
				}else{ $row->clicos1="";} 	
				if($fol!=$row->folio){
					$row->folio2=str_pad($row->folio, 5, "0", STR_PAD_LEFT);$fol=$row->folio;
				}else{ $row->folio2="";}
				
				$tot+=$row->kgscos;
				if($row->tipcos==2) $totimp+=($row->grscos+$row->prebas)*$row->kgscos;
				$totg+=1;$totgr+=$row->grscos;
				$totd+=$row->kgscos;

				if($row->tipcos==2) $totimpd+=($row->grscos+$row->prebas)*$row->kgscos;
				$totgd+=1;
				$totgrd+=$row->grscos;
				$row->pregra='$ '.number_format($row->grscos+$row->prebas, 2, '.', ',');
				$row->grscos=number_format($row->grscos, 2, '.', ',');
				if($row->tipcos==2) $row->imp='$ '.number_format(($row->grscos+$row->prebas)*$row->kgscos, 2, '.', ',');
				else $row->imp='';
				if($row->prebas>0){ $row->prebas1='$ '.number_format($row->prebas, 2, '.', ',');} else {$row->prebas='';$row->prebas1='';}
				$row->kgscos=number_format($row->kgscos, 2, '.', ',');
				$data[] = $row;
			endforeach;
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->clicos1 = ""; $row->nomg = ""; $row->folio2 = "";
				$row->feccos1="";$row->tipcos1="";$row->numcos1="";$row->pisg="Total"; $row->grscos='';$row->prebas1='';$row->pregra='';
				if($totgr>0) $row->grscos=number_format($totgr/$totg, 2, '.', ','); else $row->grscos='';
				if($tot>0) $row->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $row->kgscos='';
				if($totimp>0) $row->imp='$ '.number_format($totimp, 2, '.', ','); else $row->imp='';
				$data[] = $row;	
			endforeach;
			//este total se imprime si hay mas de una granja en el dia
			if($granjas>1){
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->clicos1 = ""; $row->nomg = "";  $row->folio2 = "";
				$row->feccos1="Total Dia";$row->tipcos1="";$row->numcos1="";$row->pisg=""; $row->grscos='';$row->prebas1='';$row->pregra='';
				if($totgrd>0) $row->grscos=number_format($totgrd/$totgd, 2, '.', ','); else $row->grscos='';
				if($totd>0) $row->kgscos=number_format($totd, 2, '.', ',').' kgs.'; else $row->kgscos='';
				if($totimpd>0) $row->imp='$ '.number_format($totimpd, 2, '.', ','); else $row->imp='';
				$data[] = $row;	
			endforeach;
			}
			}
			return $data;			
		}

		function bordoc($filter,$cic){
			//select feccos,pisg,grscos,kgscos,clicos,numcos,tipcos from siegra_19 inner join bordo on estcos=idpis
			$this->db->select("idcos,feccos,pisg,grscos,kgscos,clicos,Razon,numcos,tipcos,estcos,prebas,numgrab,folio");
			$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner');       
			$this->db->join('clientes', 'Numero=clicos','inner');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->order_by($this->numgrab);$this->db->order_by($this->folio);$this->db->order_by($this->estcos);
			$result = $this->db->get($this->tabla.'_'.$cic);
			$data = array();  $fec=new Libreria(); $dia=''; $maq=''; $cos=''; $cli='';$gra='';$tot=0;$totimp=0;$totg=0;$totgr=0;
			$granjas=1;$totd=0;$totimpd=0;$totgd=0;$totgrd=0;$fol=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//if($dia!=$row->feccos){
				if($gra!=$row->numgrab){
					if($tot>0){
						$this->db->select('max(idcos)');	
						$result = $this->db->get($this->tabla.'_'.$cic);
						foreach ($result->result() as $rowt):				
							$rowt->clicos1 = ""; $rowt->nomg = ""; $rowt->folio2 = "";
							$rowt->feccos1="";$rowt->tipcos1="";$rowt->numcos1="";$rowt->pisg="Total"; $rowt->prebas1='';$rowt->pregra='';
							if($totgr>0) $rowt->grscos=number_format($totgr/$totg, 2, '.', ','); else $rowt->grscos='';
							if($tot>0) $rowt->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $rowt->kgscos='';
							if($totimp>0) $rowt->imp='$ '.number_format($totimp, 2, '.', ','); else $row->impt='';
							$data[] = $rowt;	
						endforeach;
					   $tot=0;$maq=''; $cos=''; $cli='';$totgr=0;$totg=0;$totimp=0;$granjas+=1;
					}	
					$row->feccos1=$fec->fecha($row->feccos);$dia=$row->feccos;
				}else{ $row->feccos1="";} 
				if($gra!=$row->numgrab){
					switch($row->numgrab){
						case 2: $row->nomg='OA-Kino'; break;	case 4: $row->nomg='OA-Ahome'; break; case 5: $row->nomg='Oce. Azul'; break;
						case 6: $row->nomg='OA-Huatabampo'; break;					
						case 10: $row->nomg='OA-Topolobampo'; break;
					}
					$gra=$row->numgrab;
				}else{ $row->nomg="";} 
				if($maq!=$row->tipcos){
					switch($row->tipcos){
						case 1: $row->tipcos1='Maquila'; break;	case 2: $row->tipcos1='Venta'; break; case 3: $row->tipcos1='Donación'; break;					
					}
					$maq=$row->tipcos;
				}else{ $row->tipcos1="";}
				if($cos!=$row->numcos){
					switch($row->numcos){
						case 1: $row->numcos1='Primera'; break;	case 2: $row->numcos1='Segunda'; break;	case 3: $row->numcos1='Tercera'; break;
						case 4: $row->numcos1='Cuarta'; break;	case 5: $row->numcos1='Quinta'; break; break; case 6: $row->numcos1='Sexta'; break;
						case 7: $row->numcos1='Final'; break;
					}
					$cos=$row->numcos;
				}else{ $row->numcos1="";}	
				if($cli!=$row->clicos){
					$row->clicos1=$row->Razon;$cli=$row->clicos;
				}else{ $row->clicos1="";} 	
				if($fol!=$row->folio){
					$row->folio2=str_pad($row->folio, 5, "0", STR_PAD_LEFT);$fol=$row->folio;
				}else{ $row->folio2="";}
				
				$tot+=$row->kgscos;
				if($row->tipcos==2) $totimp+=($row->grscos+$row->prebas)*$row->kgscos;
				$totg+=1;$totgr+=$row->grscos;
				$totd+=$row->kgscos;
				if($row->tipcos==2)$totimpd+=($row->grscos+$row->prebas)*$row->kgscos;
				$totgd+=1;$totgrd+=$row->grscos;
				if($row->tipcos==2) $row->pregra='$ '.number_format($row->grscos+$row->prebas, 2, '.', ',');
				else $row->pregra='';
				$row->grscos=number_format($row->grscos, 2, '.', ',');
				if($row->tipcos==2)$row->imp='$ '.number_format(($row->grscos+$row->prebas)*$row->kgscos, 2, '.', ',');
				else $row->imp='';
				if($row->prebas>0){ $row->prebas1='$ '.number_format($row->prebas, 2, '.', ',');} else {$row->prebas='';$row->prebas1='';}
				$row->kgscos=number_format($row->kgscos, 2, '.', ',');
				$data[] = $row;
			endforeach;
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->clicos1 = ""; $row->nomg = ""; $row->folio2 = "";
				$row->feccos1="";$row->tipcos1="";$row->numcos1="";$row->pisg="Total"; $row->grscos='';$row->prebas1='';$row->pregra='';
				if($totgr>0) $row->grscos=number_format($totgr/$totg, 2, '.', ','); else $row->grscos='';
				if($tot>0) $row->kgscos=number_format($tot, 2, '.', ',').' kgs.'; else $row->kgscos='';
				if($totimp>0) $row->imp='$ '.number_format($totimp, 2, '.', ','); else $row->imp='';
				$data[] = $row;	
			endforeach;
			//este total se imprime si hay mas de una granja en el dia
			if($granjas>1){
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->clicos1 = ""; $row->nomg = "";  $row->folio2 = "";
				$row->feccos1="Total Dia";$row->tipcos1="";$row->numcos1="";$row->pisg=""; $row->grscos='';$row->prebas1='';$row->pregra='';
				if($totgrd>0) $row->grscos=number_format($totgrd/$totgd, 2, '.', ','); else $row->grscos='';
				if($totd>0) $row->kgscos=number_format($totd, 2, '.', ',').' kgs.'; else $row->kgscos='';
				if($totimpd>0) $row->imp='$ '.number_format($totimpd, 2, '.', ','); else $row->imp='';
				$data[] = $row;	
			endforeach;
			}
			}
			return $data;			
		}
		
		
		public function borrarcos($id,$cic){
			$this->db->where($this->idcos,$id);
			$this->db->delete($this->tabla.'_'.$cic);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		
		function agregacos($fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$gra,$fol,$cic,$nci){
			$kgs = str_replace(",", "", $kgs);$nci='20'.$cic.'-'.$nci;
			$data=array($this->feccos=>$fec,$this->tipcos=>$tip,$this->numcos=>$num,$this->clicos=>$cli,$this->estcos=>$est,$this->grscos=>$grs,
						$this->kgscos=>$kgs,$this->prebas=>$pre,$this->numgrab=>$gra,$this->folio=>$fol,$this->ncicos=>$nci);			
			$this->db->insert($this->tabla.'_'.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizacos($id,$fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$gra,$fol,$cic,$nci){
			$kgs = str_replace(",", "", $kgs);$nci='20'.$cic.'-'.$nci;
			$data=array($this->feccos=>$fec,$this->tipcos=>$tip,$this->numcos=>$num,$this->clicos=>$cli,$this->estcos=>$est,$this->grscos=>$grs,
						$this->kgscos=>$kgs,$this->prebas=>$pre,$this->numgrab=>$gra,$this->folio=>$fol,$this->ncicos=>$nci);
			$this->db->where($this->idcos,$id);
			$this->db->update($this->tabla.'_'.$cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
			
		}
		
		function verDepto(){			
			$this->db->order_by('NomDep');			
			$query=$this->db->get('departamento');
			return $query->result();			
		}
			
    }
    
?>