<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Cargos_model extends CI_Model {
        //private $nombre;
        public $id="NumSol";
        public $fec="FechaS";
		public $enc="NumBioS";
		public $des="NomDesS";
		public $uni="NumUniS";
		public $ali="Ali";
		public $com="Com";
		public $cas="Cas";
		public $hos="Hos";
		public $fit="Fit";
		public $rep="Rep";
		public $rem="NumRemS";
		public $cli="NumCliS";
		public $car="Cargo";		
		public $cor="Corte";
		public $tabla="solicitud";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		public function agregar($fec,$rem,$enc,$des,$uni,$ali,$com,$cas,$hos,$fit,$tot,$rep,$cg,$corte){
			if($ali>0) $ali = str_replace(",", "", $ali); else $ali=0;
			if($com>0) $com = str_replace(",", "", $com); else $com=0;
			if($cas>0) $cas = str_replace(",", "", $cas); else $cas=0;
			if($hos>0) $hos = str_replace(",", "", $hos); else $hos=0;
			if($fit>0) $fit = str_replace(",", "", $fit); else $fit=0;
			if($cg>0) $cg = str_replace(",", "", $cg); else $cg=0;
			if($rep>0) $rep = str_replace(",", "", $rep); else $rep=0;		
			$tot=$ali+$com+$cas+$hos+$fit+$rep+$cg;
			$data=array($this->fec=>$fec,$this->rem=>$rem,$this->enc=>$enc,$this->des=>$des,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->car=>$tot,$this->rep=>$rep,$this->cor=>$corte);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$fec,$rem,$enc,$des,$uni,$ali,$com,$cas,$hos,$fit,$tot,$rep,$cg,$cli,$corte){
			/*$ali = str_replace(",", "", $ali);$com = str_replace(",", "", $com);$cas = str_replace(",", "", $cas);
			$hos = str_replace(",", "", $hos);$fit = str_replace(",", "", $fit);*/
			if($ali>0) $ali = str_replace(",", "", $ali); else $ali=0;
			if($com>0) $com = str_replace(",", "", $com); else $com=0;
			if($cas>0) $cas = str_replace(",", "", $cas); else $cas=0;
			if($hos>0) $hos = str_replace(",", "", $hos); else $hos=0;
			if($fit>0) $fit = str_replace(",", "", $fit); else $fit=0;
			if($cg>0) $cg = str_replace(",", "", $cg); else $cg=0;
			if($des=="Complemento Gasto"){  $rep=$cg; $uni=0; $cg=0;}
			if($rep>0) $rep = str_replace(",", "", $rep); else $rep=0;
			if($des=="Improvistos"){  $uni=0;}		
			//if($rep>0) $rep = str_replace(",", "", $rep); else $rep=0;
			if($des=="Saldo Ciclo Anterior"){  $uni=0;}
			$tot=$ali+$com+$cas+$hos+$fit+$rep+$cg;
			$data=array($this->fec=>$fec,$this->rem=>$rem,$this->enc=>$enc,$this->des=>$des,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->car=>$tot,$this->rep=>$rep,$this->cli=>$cli,$this->cor=>$corte);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function getCliente($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('NumRegR',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		public function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('biologo');
			return $query->result();			
		}		
		public function verDestino(){
			//SELECT nomdes from viaticos group by nomdes	
			$this->db->select('NomDes');
			$this->db->where('numdes >',0);
			$this->db->group_by('NomDes'); 
			$query=$this->db->get('viaticos');
			return $query->result();			
		}
		public function verChofer(){			
			$this->db->where('activo =',0);
			$this->db->order_by('numcho');			
			$query=$this->db->get('chofer');
			return $query->result();			
		}
		public function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		public function verRemision(){
			$this->db->where('RemisionR >',0);			
			$this->db->order_by('NumRegR');
			$query=$this->db->get('r21');
			return $query->result();			
		}
		function getCargos($filter,$ciclo){
			//$this->db->select('NumSol,NomDesS,NumUniS,NumBioS,FechaS,Ali,Com,Cas,Hos,Fit,Rep,NumRemS,NumCliS,CarAbo,NumChoS,Cargo,Litros,Obser,NumRegR,RemisionR,NumCliR,NumBio,Nombio');
			$this->db->join('biologo', 'numbios=numbio', 'inner');
			$this->db->join('unidad', 'numunis=numuni', 'left');
			//$this->db->join('r13', 'numrems=numregr', 'inner');
			//$this->db->join('chofer', 'NumChoS=NumCho', 'inner');
			$this->db->where('numsol >',0);
			$this->db->where('carabo =',1);
			if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			
			$this->db->order_by('fechas','DESC');
			$this->db->order_by('nombio');	
			$this->db->order_by('nomdess');
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*if($filter['num']!=0){				
					$this->db->where($this->estatus,$filter['num']);				
			}*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			
			
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			 $encargado="a";$fecha1=date("y-m-d");$sub1=0;$lin=0;
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$row->FechaS1=$row->FechaS;
				$row->FechaS1 = $fec->fecha($row->FechaS);
				if($fecha1!=$row->FechaS){
					if($lin==1){
					//linea en blanco
					$this->db->select('count(*)');
					$resultt = $this->db->get($this->tabla);
					foreach($resultt->result() as $rowt):
						$rowt->FechaS1=".";$rowt->NomBio =".";$rowt->NomDesS =".";$rowt->NomUni =".";
						$rowt->Ali1 =".";$rowt->Com1 =".";$rowt->Cas1 =".";$rowt->Hos1 =".";$rowt->Fit1 =".";$rowt->Cargo =".";
						//$rowt->RemisionR="";$rowt->Razon="";
						//$rowt->Cargo1="$ ".number_format($totca, 2, '.', ',');
						//$rowt->Abono1="$ ".number_format($totab, 2, '.', ',');
						//$rowt->saldofila="$ ".number_format($totca-$totab, 2, '.', ',');
						$data[] = $rowt;
					endforeach;
						
					}
					$fecha1=$row->FechaS; $encargado="a";$lin=1;
					//$row->FechaS1=$row->FechaS;
				} else{ 
		  			if($encargado!=$row->NomBio){
		  			} else {
		  			$row->FechaS1="";	
		  			//$row->FechaS1="";
		  		} 
				}			
				if($encargado!=$row->NomBio){ $encargado=$row->NomBio;}
				else{
					$row->NomBio="";
				}
				if($row->NomDesS == "Improvistos" or $row->NomDesS == "Complemento Gasto" or $row->NomDesS == "Saldo Ciclo Anterior"){ $row->NomUni="";	}
				$row->Viatico = '$ '.number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$sub1=($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit);
				$row->sub =number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit), 2, '.', ',');
				//$row->sub = number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$row->Cargo = '$ '.number_format(($row->Cargo), 2, '.', ',');
				if($row->Ali>0){ $row->Ali1 = "Ali: $".number_format(($row->Ali), 2, '.', ',');$row->Ali = number_format(($row->Ali), 2, '.', ',');} else {$row->Ali ="";$row->Ali1 ="";}
				if($row->Com>0){ $row->Com1 = "Com: $".number_format(($row->Com), 2, '.', ',');$row->Com = number_format(($row->Com), 2, '.', ',');} else {$row->Com ="";$row->Com1 ="";}
				if($row->Cas>0){ $row->Cas1 = "Cas: $".number_format(($row->Cas), 2, '.', ',');$row->Cas = number_format(($row->Cas), 2, '.', ',');} else {$row->Cas ="";$row->Cas1 ="";}
				if($row->Hos>0){ $row->Hos1 = "Hos: $".number_format(($row->Hos), 2, '.', ',');$row->Hos = number_format(($row->Hos), 2, '.', ',');} else {$row->Hos ="";$row->Hos1 ="";}
				if($row->Fit>0){ $row->Fit1 = "Fit: $".number_format(($row->Fit), 2, '.', ',');$row->Fit = number_format(($row->Fit), 2, '.', ',');} else {$row->Fit ="";$row->Fit1 ="";}
				$txtRep=0;$txtRepCG=0;	
				if($row->NomDesS == "Improvistos" || $row->NomDesS == "Saldo Ciclo Anterior"){
					$row->imp=number_format(($row->Rep), 2, '.', ',');$txtRep=$row->Rep;$txtRepCG=0;
					$row->Ali ="";$row->Com ="";$row->Cas ="";$row->Hos ="";$row->Fit ="";
				}
				if($row->NomDesS == "Complemento Gasto"){$row->cg=number_format(($row->Rep), 2, '.', ',');$txtRepCG=$row->Rep;$txtRep=0;}
				$row->tot=number_format(($sub1+$txtRep+$txtRepCG), 2, '.', ',');	
				$data[] = $row;
				
			endforeach;
			return $data;
		}
		
		function getNumRows($filter,$ciclo){
			$this->db->join('biologo', 'numbios=numbio', 'inner');
			$this->db->join('unidad', 'numunis=Numuni', 'left');
			//$this->db->join('r13', 'numrems=numregr', 'inner');
			$this->db->where('numsol >',0);
			$this->db->where('carabo =',1);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	public function imprimirdetalle(){
			//$ncli=335;
			$this->db->select('NumSol,NomDesS,NomUni,NomBio,FechaS,Ali,Com,Cas,Hos,Fit,Rep,NumRemS,NumCliS,Cargo');
			//$this->db->from('r12');
			$this->db->join('biologo', 'NumBioS=NumBio', 'inner');
			$this->db->join('unidad', 'NumUniS=NumUni', 'inner');
			$this->db->where('NumSol >',0);			
			$this->db->where('CarAbo =',1);
			//$this->db->where('Numero',220);
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Nombio');
			$this->db->order_by('NomDesS');
			$query = $this->db->get('solicitud');			
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
	function getElements($where){
		if($where['NomDes']!='Todos'){			
		$this->db->select('NumUni,NomUni as val');
		$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
		//$this->db->where('NumDes >',0);
		$this->db->where('Activo =',0);	
		$this->db->group_by('NumUni');        
        $this->db->where($where);
        $result=$this->db->get('viaticos');
        $data = array();        
        foreach($result->result() as $row):
            $data[] = $row;
        endforeach;        
        return $data;
		}
    }	
	function getCargosVia($filter){
			//select Alimentacion,Combustible,Casetas,Hospedaje,Fitosanitaria,ObsDes from viaticos where NomDes='".$des."' and NumUniD='".$uni."'";
			if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*if($filter['num']!=0){				
					$this->db->where($this->estatus,$filter['num']);				
			}*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			
			
			If($filter['limit']!=0)
				$result = $this->db->get('viaticos',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('viaticos');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->totals=number_format(($row->Alimentacion+$row->Combustible+$row->Casetas+$row->Hospedaje+$row->Fitosanitaria), 2, '.', ',');
				if($row->Alimentacion>0){ $row->Alimentacion = number_format(($row->Alimentacion), 2, '.', ',');} else {$row->Alimentacion ="";}
				if($row->Combustible>0){ $row->Combustible = number_format(($row->Combustible), 2, '.', ',');} else {$row->Combustible ="";}
				if($row->Casetas>0){ $row->Casetas = number_format(($row->Casetas), 2, '.', ',');} else {$row->Casetas ="";}
				if($row->Hospedaje>0){ $row->Hospedaje = number_format(($row->Hospedaje), 2, '.', ',');} else {$row->Hospedaje ="";}
				if($row->Fitosanitaria>0){ $row->Fitosanitaria = number_format(($row->Fitosanitaria), 2, '.', ',');} else {$row->Fitosanitaria ="";}
					
				$data[] = $row;				
			endforeach;
			return $data;
		}
		
		function getNumRowsVia($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get('viaticos');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function remision($numrem){									
			$this->db->select('NumCliR');
			$this->db->where('NumRegR =',$numrem);	
			$query = $this->db->get('r18');
			return $query->result();
		}		
    }
    
?>