<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Referencia_model extends CI_Model {        
       
		public $idedo="idedo";public $num="num";public $nomedo="nomedo";public $tablaedo="estados";		
		public $idzon="idzon";public $idzonedo="idzonedo";public $nomzon="nomzon";public $ref1="ref1";public $ref2="ref2";public $tablaedozon="estadoszonas";
		public $idbco="idbco";public $idcli="idcli";public $refedo="refedo";public $refzon="refzon";public $refcli="refcli";public $refbco="refbco";
		public $ref="referencia";public $tablarefbco="refbco";
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function agregarbco($idcli,$edo,$zon,$cli,$bco){
			$data=array($this->idcli=>$idcli,$this->refedo=>$edo,$this->refzon=>$zon,$this->refcli=>$cli,$this->refbco=>$bco,$this->ref=>$edo.$zon.$cli.$bco);			
			$this->db->insert($this->tablarefbco,$data);
			return $this->db->insert_id();
		}
		public function actualizarbco($id,$idcli,$edo,$zon,$cli,$bco){
			$data=array($this->idcli=>$idcli,$this->refedo=>$edo,$this->refzon=>$zon,$this->refcli=>$cli,$this->refbco=>$bco,$this->ref=>$edo.$zon.$cli.$bco);
			$this->db->where($this->idbco,$id);
			$this->db->update($this->tablarefbco,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		
		
		
		public function agregarzon($num,$nomzon,$ref1,$ref2){
			$data=array($this->idzonedo=>$num,$this->nomzon=>$nomzon,$this->ref1=>$ref1,$this->ref2=>$ref2);			
			$this->db->insert($this->tablaedozon,$data);
			return $this->db->insert_id();
		}
		public function actualizarzon($id,$num,$nomzon,$ref1,$ref2){
			$data=array($this->idzonedo=>$num,$this->nomzon=>$nomzon,$this->ref1=>$ref1,$this->ref2=>$ref2);
			$this->db->where($this->idzon,$id);
			$this->db->update($this->tablaedozon,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		
		function verEdo(){
			//$this->db->group_by('nomedo');
			$query=$this->db->get($this->tablaedo);
			return $query->result();			
		}
		//Estados
		function getEstados($filter){
			//select num,nomedo,idzon,nomzon,ref1,ref2 from estados left join estadoszonas on idzonedo=num order by num,ref1
			$this->db->select('idedo,num,nomedo,idzon,nomzon,ref1,ref2');
			$this->db->join($this->tablaedozon, $this->idzonedo.'='.$this->num,'left');
			$this->db->order_by($this->num);
			$this->db->order_by($this->ref1);
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaedo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaedo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			$conta=$result->num_rows();$num=0;
			foreach($result->result() as $row):
				if($num!=$row->num){
					$num=$row->num;	
					$row->num1=$row->num;$row->nomedo1=$row->nomedo;
				}else{
					$row->num1='';$row->nomedo1='';
				}
				if($row->nomzon==''){$row->nomzon='';}
				if($row->ref1==''){$row->ref1='';}
				if($row->ref2==''){$row->ref2='';}
				$data[] = $row;
			endforeach;
			return $data;
		}
		function getNumRowsE($filter){
			//$this->db->join('clientes', 'Numero=idcte','inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaedo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		//Estados
		function getEstadosBco($filter,$edob,$zonb){
			//select Razon,idcli,refedo,refzon,refcli,refbco from clientes inner join refbco on idcli=numero
			$this->db->select('nomedo,nomzon,razon,dom,cp,loc,edo,idbco,idcli,refedo,refzon,refcli,refbco,referencia,ref1,idzonedo');
			$this->db->join('clientes', 'numero ='.$this->idcli,'inner');
			$this->db->join('estadoszonas', 'ref1 ='.$this->refzon,'inner');
			$this->db->join('estados', 'num ='.$this->refedo,'inner');
			$this->db->order_by($this->ref);
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablarefbco);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			$conta=$result->num_rows();$num=0;$ref2=0;$ref1=0;$zon='';$totgranja=0;$edos='';
				foreach($result->result() as $row):
					if($edos!=$row->nomedo){
						$edos=$row->nomedo;
					}else{ $row->nomedo="";	}
					if($zon!=$row->nomzon){
						$zon=$row->nomzon; $edob=$row->refedo;$zonb=$row->refzon;
					    if($totgranja>0){
						$this->db->select('idzonedo,ref1,max(ref2)+1 as ref2');
						$this->db->where($this->idzonedo,$edob);
						$this->db->where($this->ref1,$zonb);
						$result = $this->db->get($this->tablaedozon);
						foreach($result->result() as $rowt):
							$rowt->nomedo='';$rowt->nomzon='';
							$rowt->razon='Siguiente a Gestionar';
							if($conta>0){$rowt->idcli='';$rowt->idzonedo=$edo;$rowt->ref1=$ref1;$rowt->ref2=$ref2;$rowt->referencia=$edo.$ref1.$ref2.'-';}else{$rowt->referencia=$rowt->idzonedo.$rowt->ref1.$rowt->ref2.'-';}
							$data[] = $rowt;
						endforeach;
						$totgranja=0;
						}			
					} else{ 
		  				$row->nomzon="";	
				
		  			}
					$edo=$row->refedo;
					$ref1=$row->refzon;
					$ref2=$row->refcli;$ref2+=1;
					$totgranja+=1;
					$data[] = $row;
				endforeach;
				$this->db->select('idzonedo,ref1,max(ref2)+1 as ref2');
				$this->db->where($this->idzonedo,$edob);
				$this->db->where($this->ref1,$zonb);
				$result = $this->db->get($this->tablaedozon);
				foreach($result->result() as $row):
					$row->nomedo='';$row->nomzon='';
					$row->razon='Siguiente a Gestionar';
					if($conta>0){$row->idcli='';$row->idzonedo=$edo;$row->ref1=$ref1;$row->ref2=$ref2;$row->referencia=$edo.$ref1.$ref2.'-';}else{$row->referencia=$row->idzonedo.$row->ref1.$row->ref2.'-';}
					
					$data[] = $row;
				endforeach;
			return $data;
		}
		function getNumRowsB($filter){
			//$this->db->join('clientes', 'Numero=idcte','inner');
			$this->db->select('razon,idcli,refedo,refzon,refcli,refbco');
			$this->db->join('clientes', 'numero ='.$this->idcli,'inner');
			$this->db->join('estadoszonas', 'ref1 ='.$this->refzon,'inner');
			$this->db->join('estados', 'num ='.$this->refedo,'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarefbco);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function agregaraedo($num,$nomedo){
			$data=array($this->num=>$num,$this->nomedo=>$nomedo);			
			$this->db->insert($this->tablaedo,$data);
			return $this->db->insert_id();
		}
		public function actualizaredo($id,$nomedo){
			$data=array($this->nomedo=>$nomedo);
			$this->db->where($this->idedo,$id);
			$this->db->update($this->tablaedo,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		public function buscarcli1($id){
			$conta=0;
			$this->db->select(count($this->idcli));
			$this->db->where($this->idcli,$id);
			$resultb = $this->db->get($this->tablarefbco);
			//$conta=$result->num_rows();
			foreach($resultb->result() as $row):	
        	   $conta+=1;
        	endforeach; 
			if($conta>0){
				return 1;
			}else {
				return 0;
			}
		}
		function buscarcli($id){
			$this->db->select('idcli as can');
			$this->db->where($this->idcli,$id);
			$query = $this->db->get($this->tablarefbco);
			return $query->row();
		}
		function borraras($id,$ciclo){
			$this->db->where($this->ids,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		function buscaredo($id){
			$this->db->select('nomedo,num');
			$this->db->where($this->nomedo,$id);
			$query = $this->db->get($this->tablaedo);
			return $query->row();
		}
		function getElementsC($where){
			$this->db->select($this->nomedo.','.$this->num);
			$query = $this->db->get($this->tablaedo);		
	     	$data = array();  
		    foreach($query->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		function getElementsCEZ($filter){
			//select idzonedo,nomedo from estados inner join estadoszonas on idzonedo=num group by idzonedo
			$this->db->select($this->idzonedo.','.$this->nomedo.','.$this->num);
			$this->db->join($this->tablaedo, $this->num.'='.$this->idzonedo,'inner');
			$this->db->group_by($this->idzonedo);
			$query = $this->db->get($this->tablaedozon);		
	     	$data = array();  
		    foreach($query->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		function getElementsCZ($filter){
			$this->db->select($this->nomzon.','.$this->ref1);
			$this->db->order_by($this->ref1);
			if($filter['where']!='') $this->db->where($filter['where']); 
			$query = $this->db->get($this->tablaedozon);		
	     	$data = array();  
		    foreach($query->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		function quitarcor($id,$ciclo){
			$this->db->where($this->idcd,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		public function agregarcor($fec,$can,$cic,$tip){
			$data=array($this->fecd=>$fec,$this->cancd=>$can,$this->origencd=>$tip);			
			$this->db->insert($cic,$data);
			return $this->db->insert_id();
		}
		public function actualizarcor($id,$fec,$can,$cic){
			$data=array($this->fecd=>$fec,$this->cancd=>$can);
			$this->db->where($this->idcd,$id);
			$this->db->update($cic,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		public function UltimaAct(){
			$this->db->select('max(FechaH) as ultimaact');
			$query=$this->db->get('historialrcw');
			return $query->result();			
		}
		function getClientes($filter){
			$this->db->order_by('Razon,referencia,refedo,refzon,refbco,refcli');
			$this->db->join('refbco', 'idcli = Numero','left');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			foreach($result->result() as $row):
				if($row->referencia=='') $row->referencia='';
				$data[] = $row;
			endforeach;
			return $data;
		}
		function getNumRowsC($filter){
			$this->db->join('refbco', 'idcli = Numero','left');
			if($filter['where']!='') $this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);	
			$result = $this->db->get('clientes');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
    }
    
?>