<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Facoa_model extends CI_Model {
        //private $nombre;
        public $tabla="proveedores";
        public $tablaag="alment_";  public $CBE="CBE";
		public $tablalm="almmat"; public $CB="CB";
		
		public $tablaf="fac_oa_";
		public $idag="idag";public $fecag="fecag";public $facag="facag";public $idgag="idgag";public $aliag="aliag";
		public $medag="medag";public $tonag="tonag";public $tcag="tcag";public $fecpag="fecpag";public $obspag="obspag";
		public $proag="proag";public $imppag="imppag";public $impfac="impfac";public $tcpag="tcpag";
		public $tipag="tipag";
		public $fecfle="fecfle";public $facfle="facfle";public $profle="profle";public $subfle="subfle";
		public $numpag="numpag";
		public $estatuss="estatuss";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function alimentos($cic){
			$this->db->select('aliag');
			$this->db->group_by($this->aliag);
			$query = $this->db->get($this->tablaf.$cic);			
			return $query->result();
		}
		public function alimentosg($cic){
			$this->db->select('CB,NomMat');
			$this->db->join($this->tablalm, $this->CB.'='.$this->CBE,'inner');
			$this->db->group_by('CBE');
			$query = $this->db->get($this->tablaag.$cic);			
			return $query->result();
		}
		function getElementsb($filter){
			$cic=18;        
        	$this->db->select("medag,medag as val"); 
        	if($filter['where']!='') $this->db->where($filter['where']);
        	$this->db->group_by($this->medag);      
        	$result = $this->db->get($this->tablaf.$cic);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		
		public function agregar($feca,$gra,$fol,$des,$med,$ton,$tc,$fecp,$obs,$pro,$cic,$ip,$if,$tcp,$tip,$fecf,$facf,$prof,$subf,$npag,$ests){
			$ip = str_replace(",", "", $ip);$if = str_replace(",", "", $if);$subf = str_replace(",", "", $subf);
			$data=array($this->fecag=>$feca,$this->idgag=>$gra,$this->facag=>$fol,$this->aliag=>$des,$this->medag=>$med,$this->tonag=>$ton,$this->tcag=>$tc,$this->fecpag=>$fecp,$this->obspag=>$obs,$this->proag=>$pro,$this->imppag=>$ip,$this->impfac=>$if,$this->tcpag=>$tcp,$this->tipag=>$tip,$this->fecfle=>$fecf,$this->facfle=>$facf,$this->profle=>$prof,$this->subfle=>$subf,$this->numpag=>$npag,$this->estatuss=>$ests);				
			$this->db->insert($this->tablaf.$cic,$data);
			$this->db->insert_id();
			return 0;	
		}
		public function actualizar($id,$feca,$gra,$fol,$des,$med,$ton,$tc,$fecp,$obs,$pro,$cic,$ip,$if,$tcp,$tip,$fecf,$facf,$prof,$subf,$npag,$ests){
			$ip = str_replace(",", "", $ip);$if = str_replace(",", "", $if);$subf = str_replace(",", "", $subf);
			$data=array($this->fecag=>$feca,$this->idgag=>$gra,$this->facag=>$fol,$this->aliag=>$des,$this->medag=>$med,$this->tonag=>$ton,$this->tcag=>$tc,$this->fecpag=>$fecp,$this->obspag=>$obs,$this->proag=>$pro,$this->imppag=>$ip,$this->impfac=>$if,$this->tcpag=>$tcp,$this->tipag=>$tip,$this->fecfle=>$fecf,$this->facfle=>$facf,$this->profle=>$prof,$this->subfle=>$subf,$this->numpag=>$npag,$this->estatuss=>$ests);
			$this->db->where($this->idag,$id);
			$this->db->update($this->tablaf.$cic,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function borrar($id,$cic){
			$this->db->where($this->idag,$id);
			$this->db->delete($this->tablaf.$cic);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getfacoa($filter,$cic){
			//
			/*$this->db->select('fecsal,folsal,almsal,noma,consala,clisal,nomc,conc,consalc,idsal,estsal,nomt,cajsal,presal,kgssal,prekgs,(kgssal*prekgs) as impvta,facsal');
			$this->db->join('almacenes', 'ida=almsal','inner');
			$this->db->join('clientes', 'idc=clisal','inner');
			$this->db->join('tallas', 'idt=talsal','inner');*/
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecpag','DESC');
			$this->db->order_by('idag','DESC');
			//$this->db->where('estsal','1');
			$result = $this->db->get($this->tablaf.$cic);
			$data = array(); $fec=new Libreria(); $tmn=0; $tus=0;$tus1=0; $tton=0; $tusf=0;$tcos=0;$pag1=0;$fac1=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				$pag=0; $fac=0;$inc=0;$pag1=0;
				$tton+=$row->tonag;
				$row->fecpag1 = $fec->fecha21($row->fecpag);
				if($row->numpag==0) $row->numpag='';
				if($row->subfle>0){$row->subfle = number_format(($row->subfle), 2, '.', ',');;}else{$row->subfle='';}
				if($row->fecag=='0000-00-00'){$row->fecag1='';}else{$row->fecag1 = $fec->fecha21($row->fecag);}
				if($row->tcpag>0 && $row->tipag==1){
					$pag=$row->imppag/$row->tcpag;	
					//$pag1=$row->imppag/$row->tcpag;
					//$pag1=substr($pag, 0,9);	 
					$pag=substr($row->imppag/$row->tcpag, 0,9);
					$pag1=substr($pag, 0,8);
					$inc=substr($pag, 8, 1);
					if($inc>=5) $inc=0.01; else $inc=0;
					//$pag=substr($pag1, 0,8);
					$pag=$pag1+$inc;
					//$pag1=number_format(($row->imppag/$row->tcpag), 2);
					$tmn+=$row->imppag; $tus+=$pag;$tus1+=$pag;$tcos+=$pag*$row->tcpag;
					$row->costo='$ '.number_format(($pag*$row->tcpag), 2, '.', ',');
					$row->imppag1='$ '.number_format(($row->imppag/$row->tcpag), 2, '.', ',');
					$row->impmn='$ '.number_format(($row->imppag), 2, '.', ',');
					$row->imppag=number_format(($row->imppag), 2, '.', ',');
					//$row->tcpag=number_format(($row->tcpag), 4, '.', ',');
				}else{
					 //$row->tcpag=''; $row->tcpag1='';
					$tcos+=$row->tcpag*$row->imppag;$tmn+=$row->imppag;
					$row->costo='$ '.number_format(($row->tcpag*$row->imppag), 2, '.', ',');
					$row->impmn='$ '.number_format(($row->imppag), 2, '.', ',');
					$pag=$row->imppag;$tus+=$row->imppag;
					//$row->imppag1='$ '.number_format(($row->imppag), 2, '.', ',');
					$row->imppag1='';
					$row->imppag=number_format(($row->imppag), 2, '.', ',');
				}
				$row->tcpag=number_format(($row->tcpag), 4, '.', ',');
				if($row->tcpag>0 && $row->tipag==1) $row->tcpag1='$ '.number_format(($row->tcpag), 4, '.', ','); else $row->tcpag1='';
				if($row->tcag>0){
					$row->tcag1='$ '.number_format(($row->tcag), 2, '.', ','); 	
					$row->tcag=number_format(($row->tcag), 2, '.', ',');
				}else{ $row->tcag='';$row->tcag1='';}
				$row->tonag=number_format(($row->tonag), 3, '.', ',');
				if($row->impfac>0){
					$tusf+=$row->impfac;
					$fac=$row->impfac;
					$row->impfac1='$ '.number_format(($row->impfac), 2, '.', ',');
					//$fac1=number_format(($row->impfac), 2);
					$row->impfac=number_format(($row->impfac), 2, '.', ',');
					
				} else {$row->impfac1='';$row->impfac='';$fac=0;}
				$row->des=$row->aliag.'% '.$row->medag; 
				if($row->estatuss==0){ $row->estatuss='';}
				if(($pag-$fac)==0){$row->dif='';$row->dif1=1;} else {$row->dif='$ '.number_format(($pag-$fac), 2, '.', ',');$row->dif1=$pag-$fac;}
				$data[] = $row;
			 endforeach;
			 $this->db->select('count(*)');
			 $resultt = $this->db->get($this->tablaf.$cic);
		     foreach($resultt->result() as $rowt):
				$rowt->numpag ="Total";
				$rowt->fecpag1="";$rowt->proag ="";$rowt->des ="";$rowt->tcpag1 ="";$rowt->fecag1 ="";$rowt->facag ="";$rowt->estatuss='';
				$rowt->tcag1 ="";
				$rowt->tonag =number_format($tton, 3, '.', ',');
				$rowt->impmn ='$ '.number_format($tmn, 2, '.', ',');
				$rowt->imppag1 ='$ '.number_format($tus1, 2, '.', ',');
				$rowt->impfac1 =' ';
				//$rowt->impfac1 ='$ '.number_format($tusf, 2, '.', ',');
				$rowt->dif ='$ '.number_format($tus-$tusf, 2, '.', ',');$rowt->dif1=$tus-$tusf;
				$rowt->costo ='$ '.number_format($tcos, 2, '.', ',');$rowt->dif1=$tus-$tusf;
				$data[] = $rowt;
			 endforeach;
			}
			return $data;
		}
		function getfle($filter,$cic){
			//
			/*$this->db->select('fecsal,folsal,almsal,noma,consala,clisal,nomc,conc,consalc,idsal,estsal,nomt,cajsal,presal,kgssal,prekgs,(kgssal*prekgs) as impvta,facsal');
			$this->db->join('almacenes', 'ida=almsal','inner');
			$this->db->join('clientes', 'idc=clisal','inner');
			$this->db->join('tallas', 'idt=talsal','inner');*/
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecpag','DESC');
			//$this->db->order_by('folsal','DESC');
			//$this->db->where('estsal','1');
			$result = $this->db->get($this->tablaf.$cic);
			$data = array(); $fec=new Libreria(); $totf=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $row->fecpag2 = $fec->fecha($row->fecpag);
				if($row->subfle>0){
					$totf+=($row->subfle+($row->subfle*.16))-($row->subfle*.04);
					$row->iva ='$ '. number_format(($row->subfle*.16), 2, '.', ',');
					$row->ret ='$ '. number_format(($row->subfle*.04), 2, '.', ',');
					$row->total ='$ '. number_format((($row->subfle+($row->subfle*.16))-($row->subfle*.04)), 2, '.', ',');
					$row->subfle ='$ '. number_format(($row->subfle), 2, '.', ',');
				}else{
					$row->subfle='';$row->iva ='';$row->ret ='';$row->total ='';
				}
				if($row->fecfle=='0000-00-00' || $row->fecfle==''){$row->fecfle1='';}else{$row->fecfle1 = $fec->fecha21($row->fecfle);}
				if($row->profle==''){$row->profle='';}				
				$data[] = $row;				
			 endforeach;
			 $this->db->select('count(*)');
			 $resulttf = $this->db->get($this->tablaf.$cic);
		     foreach($resulttf->result() as $rowft):
				$rowft->numpag ='Total';$rowft->fecpag2 ='';$rowft->tonag='';$rowft->fecfle1='';$rowft->profle='';$rowft->facfle='';
				$rowft->subfle='';$rowft->iva ='';$rowft->ret ='';
				$rowft->total='$ '. number_format(($totf), 2, '.', ',');
				$data[] = $rowft;
			 endforeach;
			}
			return $data;
		}
		function getProves(){
			$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			$this->db->group_by('Razon');
			$this->db->group_by('Numero');
			$this->db->group_by('Siglas');
			$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getAli($filter,$cic,$mesd){
			//$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			//$this->db->where('Analizar =',1);
			//$this->db->order_by('NomMat');
			$this->db->select('month(fecpag) as mes,day(fecpag) as dia,sum(tonag) as tonag,aliag,medag');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$this->db->group_by('month(fecpag)');
			$this->db->group_by('aliag');
			$this->db->group_by('medag');
			$this->db->order_by('month(fecpag)');
			$result = $this->db->get($this->tablaf.$cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $mes='';
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			$ini=1; while($ini<=31){$datosd [$ini]='';$td [$ini]='';$ini+=1;}
			foreach($result->result() as $row):
				if($row->mes!=$mes){
					$mes=$row->mes;
					switch($row->mes){
					case 1: $row->mes1='Ene'; break;
					case 2: $row->mes1='Feb'; break;
					case 3: $row->mes1='Mar'; break;
					case 4: $row->mes1='Abr'; break;
					case 5: $row->mes1='May'; break;
					case 6: $row->mes1='Jun'; break;
					case 7: $row->mes1='Jul'; break;
					case 8: $row->mes1='Ago'; break;
					case 9: $row->mes1='Sep'; break;
					case 10: $row->mes1='Oct'; break;
					case 11: $row->mes1='Nov'; break;
					case 12: $row->mes1='Dic'; break;
				}	
				}else{
					$row->mes1='';
				}
				$ali=$row->aliag;$med=$row->medag;
				$row->ali=$row->aliag.'% '.$row->medag;
				$tdg=0;
				//extrae el consumo del dia correspondiente al mes 
				$this->db->select('day(fecpag) as dia,tonag');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('aliag =',$ali);
				$this->db->where('medag =',$med);
				$this->db->where('month(fecpag) =',$mes);
				$resulta = $this->db->get($this->tablaf.$cic);
				$ini=1; while($ini<=31){$datosr [$ini]='';$ini+=1;}
				foreach($resulta->result() as $rowa):
					$dia=$rowa->dia;
					$datosd [$dia]=$rowa->dia;
					$datosr [$dia]+=$rowa->tonag;
					$td [$dia]+=$rowa->tonag;$tdg+=$rowa->tonag;
				endforeach;	
				$ini=1;
				while($ini<=31){
					switch($ini){
						case 1: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d1 = number_format($datosr [$ini],1,'.',',');} else {  $row->d1 ="";} break;
						case 2: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d2 = number_format($datosr [$ini],1,'.',',');} else {  $row->d2 ="";} break;
						case 3: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d3 = number_format($datosr [$ini],1,'.',',');} else {  $row->d3 ="";} break;
						case 4: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d4 = number_format($datosr [$ini],1,'.',',');} else {  $row->d4 ="";} break;
						case 5: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d5 = number_format($datosr [$ini],1,'.',',');} else {  $row->d5 ="";} break;
						case 6: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d6 = number_format($datosr [$ini],1,'.',',');} else {  $row->d6 ="";} break;
						case 7: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d7 = number_format($datosr [$ini],1,'.',',');} else {  $row->d7 ="";} break;
						case 8: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d8 = number_format($datosr [$ini],1,'.',',');} else {  $row->d8 ="";} break;
						case 9: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d9 = number_format($datosr [$ini],1,'.',',');} else {  $row->d9 ="";} break;
						case 10: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d10 = number_format($datosr [$ini],1,'.',',');} else {  $row->d10 ="";} break;
						case 11: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d11 = number_format($datosr [$ini],1,'.',',');} else {  $row->d11 ="";} break;
						case 12: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d12 = number_format($datosr [$ini],1,'.',',');} else {  $row->d12 ="";} break;
						case 13: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d13 = number_format($datosr [$ini],1,'.',',');} else {  $row->d13 ="";} break;
						case 14: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d14 = number_format($datosr [$ini],1,'.',',');} else {  $row->d14 ="";} break;
						case 15: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d15 = number_format($datosr [$ini],1,'.',',');} else {  $row->d15 ="";} break;
						case 16: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d16 = number_format($datosr [$ini],1,'.',',');} else {  $row->d16 ="";} break;
						case 17: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d17 = number_format($datosr [$ini],1,'.',',');} else {  $row->d17 ="";} break;
						case 18: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d18 = number_format($datosr [$ini],1,'.',',');} else {  $row->d18 ="";} break;
						case 19: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d19 = number_format($datosr [$ini],1,'.',',');} else {  $row->d19 ="";} break;
						case 20: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d20 = number_format($datosr [$ini],1,'.',',');} else {  $row->d20 ="";} break;
						case 21: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d21 = number_format($datosr [$ini],1,'.',',');} else {  $row->d21 ="";} break;
						case 22: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d22 = number_format($datosr [$ini],1,'.',',');} else {  $row->d22 ="";} break;
						case 23: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d23 = number_format($datosr [$ini],1,'.',',');} else {  $row->d23 ="";} break;
						case 24: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d24 = number_format($datosr [$ini],1,'.',',');} else {  $row->d24 ="";} break;
						case 25: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d25 = number_format($datosr [$ini],1,'.',',');} else {  $row->d25 ="";} break;
						case 26: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d26 = number_format($datosr [$ini],1,'.',',');} else {  $row->d26 ="";} break;
						case 27: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d27 = number_format($datosr [$ini],1,'.',',');} else {  $row->d27 ="";} break;
						case 28: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d28 = number_format($datosr [$ini],1,'.',',');} else {  $row->d28 ="";} break;
						case 29: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d29 = number_format($datosr [$ini],1,'.',',');} else {  $row->d29 ="";} break;
						case 30: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d30 = number_format($datosr [$ini],1,'.',',');} else {  $row->d30 ="";} break;
						case 31: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d31 = number_format($datosr [$ini],1,'.',',');} else {  $row->d31 ="";} break;
					}
					$ini+=1;
				}
				if($tdg>0) $row->tot =number_format($tdg, 1, '.', ','); else $row->tot ='';
				$data[] = $row;	
				
			endforeach;
			//total dia
			$tg=0;
			$this->db->select('max(Numero)');
				$result = $this->db->get('clientes');
				foreach ($result->result() as $row):
					$row->mes1 ="Tot";$row->ali ="";
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				endforeach;
				$row->tot =number_format($tg, 1, '.', ',');
				$data[] = $row;	
				}
			return $data;
		}
		function getAlig($filter,$cic,$mesd){
			//$this->db->select('Numero,Razon,Siglas');
			//$this->db->join($ciclo, 'Numero=prov','inner');
			//$this->db->where('Analizar =',1);
			//$this->db->order_by('NomMat');
			$this->db->select('month(FecE) as mes,day(FecE) as dia,CanE,CBE');
			$this->db->join('almmat', 'CB=CBE','inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$this->db->where('Analizar =',1);
			$this->db->group_by('month(FecE)');
			$this->db->group_by('CBE');
			$this->db->order_by('month(FecE)');
			$result = $this->db->get($this->tablaag.$cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $mes='';
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			$ini=1; while($ini<=31){$datosd [$ini]='';$td [$ini]='';$ini+=1;}
			foreach($result->result() as $row):
				if($row->mes!=$mes){
					$mes=$row->mes;
					switch($row->mes){
					case 1: $row->mes1='Ene'; break;
					case 2: $row->mes1='Feb'; break;
					case 3: $row->mes1='Mar'; break;
					case 4: $row->mes1='Abr'; break;
					case 5: $row->mes1='May'; break;
					case 6: $row->mes1='Jun'; break;
					case 7: $row->mes1='Jul'; break;
					case 8: $row->mes1='Ago'; break;
					case 9: $row->mes1='Sep'; break;
					case 10: $row->mes1='Oct'; break;
					case 11: $row->mes1='Nov'; break;
					case 12: $row->mes1='Dic'; break;
				}	
				}else{
					$row->mes1='';
				}
				$ali=$row->CBE;
				$row->ali=$row->CBE;
				$tdg=0;
				//extrae el consumo del dia correspondiente al mes 
				$this->db->select('day(FecE) as dia,CanE');
				$this->db->join('almmat', 'CB=CBE','inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('Analizar =',1);
				$this->db->where('CBE =',$ali);
				$this->db->where('month(FecE) =',$mes);
				$resulta = $this->db->get($this->tablaag.$cic);
				$ini=1; while($ini<=31){$datosr [$ini]='';$ini+=1;}
				foreach($resulta->result() as $rowa):
					$dia=$rowa->dia;
					$datosd [$dia]=$rowa->dia;
					$datosr [$dia]+=$rowa->CanE/1000;
					$td [$dia]+=$rowa->CanE/1000;$tdg+=$rowa->CanE/1000;
				endforeach;	
				$ini=1;
				while($ini<=31){
					switch($ini){
						case 1: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d1 = number_format($datosr [$ini],1,'.',',');} else {  $row->d1 ="";} break;
						case 2: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d2 = number_format($datosr [$ini],1,'.',',');} else {  $row->d2 ="";} break;
						case 3: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d3 = number_format($datosr [$ini],1,'.',',');} else {  $row->d3 ="";} break;
						case 4: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d4 = number_format($datosr [$ini],1,'.',',');} else {  $row->d4 ="";} break;
						case 5: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d5 = number_format($datosr [$ini],1,'.',',');} else {  $row->d5 ="";} break;
						case 6: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d6 = number_format($datosr [$ini],1,'.',',');} else {  $row->d6 ="";} break;
						case 7: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d7 = number_format($datosr [$ini],1,'.',',');} else {  $row->d7 ="";} break;
						case 8: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d8 = number_format($datosr [$ini],1,'.',',');} else {  $row->d8 ="";} break;
						case 9: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d9 = number_format($datosr [$ini],1,'.',',');} else {  $row->d9 ="";} break;
						case 10: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d10 = number_format($datosr [$ini],1,'.',',');} else {  $row->d10 ="";} break;
						case 11: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d11 = number_format($datosr [$ini],1,'.',',');} else {  $row->d11 ="";} break;
						case 12: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d12 = number_format($datosr [$ini],1,'.',',');} else {  $row->d12 ="";} break;
						case 13: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d13 = number_format($datosr [$ini],1,'.',',');} else {  $row->d13 ="";} break;
						case 14: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d14 = number_format($datosr [$ini],1,'.',',');} else {  $row->d14 ="";} break;
						case 15: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d15 = number_format($datosr [$ini],1,'.',',');} else {  $row->d15 ="";} break;
						case 16: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d16 = number_format($datosr [$ini],1,'.',',');} else {  $row->d16 ="";} break;
						case 17: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d17 = number_format($datosr [$ini],1,'.',',');} else {  $row->d17 ="";} break;
						case 18: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d18 = number_format($datosr [$ini],1,'.',',');} else {  $row->d18 ="";} break;
						case 19: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d19 = number_format($datosr [$ini],1,'.',',');} else {  $row->d19 ="";} break;
						case 20: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d20 = number_format($datosr [$ini],1,'.',',');} else {  $row->d20 ="";} break;
						case 21: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d21 = number_format($datosr [$ini],1,'.',',');} else {  $row->d21 ="";} break;
						case 22: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d22 = number_format($datosr [$ini],1,'.',',');} else {  $row->d22 ="";} break;
						case 23: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d23 = number_format($datosr [$ini],1,'.',',');} else {  $row->d23 ="";} break;
						case 24: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d24 = number_format($datosr [$ini],1,'.',',');} else {  $row->d24 ="";} break;
						case 25: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d25 = number_format($datosr [$ini],1,'.',',');} else {  $row->d25 ="";} break;
						case 26: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d26 = number_format($datosr [$ini],1,'.',',');} else {  $row->d26 ="";} break;
						case 27: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d27 = number_format($datosr [$ini],1,'.',',');} else {  $row->d27 ="";} break;
						case 28: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d28 = number_format($datosr [$ini],1,'.',',');} else {  $row->d28 ="";} break;
						case 29: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d29 = number_format($datosr [$ini],1,'.',',');} else {  $row->d29 ="";} break;
						case 30: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d30 = number_format($datosr [$ini],1,'.',',');} else {  $row->d30 ="";} break;
						case 31: if($datosd [$ini]==$ini and $datosr [$ini]>0){ $row->d31 = number_format($datosr [$ini],1,'.',',');} else {  $row->d31 ="";} break;
					}
					$ini+=1;
				}
				if($tdg>0) $row->tot =number_format($tdg, 1, '.', ','); else $row->tot ='';
				$data[] = $row;	
				
			endforeach;
			//total dia
			$tg=0;
			$this->db->select('max(Numero)');
				$result = $this->db->get('clientes');
				foreach ($result->result() as $row):
					$row->mes1 ="Tot";$row->ali ="";
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini]; } else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 =number_format(($td [$ini]), 1, '.', ',');$tg+=$td [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				endforeach;
				$row->tot =number_format($tg, 1, '.', ',');
				$data[] = $row;	
				}
			return $data;
		}
    }
?>