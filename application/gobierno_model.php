<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Gobierno_model extends CI_Model {        
        public $id="NumReg";  	//public $id="NumReg";
        public $numcer="NumCer";	//public $numcer="NumCer";
        public $iv="IniVig";	//public $iv="IniVig";
        public $fv="FinVig";	// public $fv="FinVig";
        public $cp="CanPos";	//public $cp="CanPos";
        public $cn="CanNau";	//public $cn="CanNau";
        public $tabla="certificados";	//public $tabla="certificados";
		public $cer="NumCerR";
		public $rem="NumRegR";
        
		public $tablarem="r15";

		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		public function agregarc($cer,$fi,$fv,$pos,$nau){
			$pos = str_replace(",", "", $pos);$nau = str_replace(",", "", $nau);
			$this->db->select('max(NumReg) as ultimo');
			$result = $this->db->get($this->tabla); $nuevo=0;
			foreach($result->result() as $row):
				$nuevo=$row->ultimo+1;
			endforeach;	
			$data=array($this->id=>$nuevo,$this->numcer=>$cer,$this->iv=>$fi,$this->fv=>$fv,$this->cp=>$pos,$this->cn=>$nau);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizarc($id,$cer,$fi,$fv,$pos,$nau){
			$pos = str_replace(",", "", $pos);$nau = str_replace(",", "", $nau);
			$data=array($this->numcer=>$cer,$this->iv=>$fi,$this->fv=>$fv,$this->cp=>$pos,$this->cn=>$nau);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function getCertificados($filter){
			$this->db->where('NumReg >',0);
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			foreach($result->result() as $row):
				$row->CanPos = number_format(($row->CanPos), 3, '.', ','); if($row->CanPos==0.000){$row->CanPos="";}
				$row->CanNau = number_format(($row->CanNau), 3, '.', ','); if($row->CanNau==0.000){$row->CanNau="";}
				$row->IniVig1 = $fec->fecha($row->IniVig);$row->FinVig1 = $fec->fecha($row->FinVig);
				$data[] = $row;
			endforeach;
			return $data;
		}
		
		function getNumRowsC($filter){
			$this->db->where('NumReg >',0);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function borrarc($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	
		//para imprimir
		function getCertificadosimp($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get('r15');
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				//$row->CanPos = number_format(($row->CanPos), 3, '.', ','); if($row->CanPos==0.000){$row->CanPos="";}
				if($raz!=$row->Razon){
					$raz=$row->Razon;$dom=$row->Loc.",".$row->Edo; 
					if($totgranja>0){
						$this->db->select('max(NumRegR)');
						$resultt = $this->db->get('r15');
						foreach($resultt->result() as $rowt):
							$rowt->Razon = $dom;
							$rowt->FechaR = "";
							$rowt->RemisionR = "Total:";
							$rowt->CantidadRR = number_format(($totgranja), 3, '.', ',');
							$data[] = $rowt;
							$totgranja=0;
						endforeach;
					}			
				} else{ 
		  			$row->Razon="";	
				
		  		} 
				$canpos=$row->CanPos;$inivig=$row->IniVig;$finvig=$row->FinVig;$cer=$row->NumCer;
				$ent+=$row->CantidadRR;$totgranja+=$row->CantidadRR;
				$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->CanPos = number_format(($row->CanPos), 3, '.', ','); 
				$row->FechaR = date("d-m-Y",strtotime($row->FechaR));
				$data[] = $row;
			endforeach;
			$this->db->select('max(NumRegR)');
			$resultt = $this->db->get('r15');
			foreach($resultt->result() as $rowt):
					$rowt->NumCer = $cer;
					$rowt->IniVig=date("d-m-Y",strtotime($inivig));
					$rowt->FinVig=date("d-m-Y",strtotime($finvig));
					$rowt->CanPos=number_format(($canpos), 3, '.', ',');
					$rowt->dif=number_format(($canpos-$ent), 3, '.', ',');
					$rowt->entregado = number_format(($ent), 3, '.', ',');
					$rowt->Razon = $dom;
					$rowt->FechaR = "";
					$rowt->RemisionR = "Total:";
					$rowt->CantidadRR = number_format(($totgranja), 3, '.', ',');
					$data[] = $rowt;
					$totgranja=0;
			endforeach;
			}
			return $data;
		}
		
		function getNumRowsCimp($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarem);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function verCertificado(){
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		function getCertificadosNum($cer){
			$this->db->where('NumReg',$cer);
			$query = $this->db->get($this->tabla);
			return $query->result();
			
		}
		
		//asignacion
		function getCertificadosasi($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablarem);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				//$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->Cantidad = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->FechaR = date("d-m-Y",strtotime($row->FechaR));
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsasi($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarem);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getCertificadosasic($filter){
			$this->db->select('NumReg,NumCer,IniVig,FinVig,CanPos,(select sum(CantidadRR) from r15 where NumCerR=NumReg and Estatus=0) as entregados');
			$this->db->order_by('NumReg','DESC');
			$result = $this->db->get($this->tabla);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				if($row->NumReg>0){
					$row->IniVig = date("d-m-Y",strtotime($row->IniVig));
					$row->FinVig = date("d-m-Y",strtotime($row->FinVig));
					$row->pos = number_format(($row->CanPos), 3, '.', ',');
				}else{ $row->IniVig = "";$row->FinVig = "";$row->pos = "";	}
				if($row->entregados>0){$row->ent = number_format(($row->entregados), 3, '.', ',');}else{$row->ent="";}
				$row->dif = number_format(($row->CanPos-$row->entregados), 3, '.', ',');
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsasic($filter){
			$this->db->select('NumReg,NumCer,IniVig,FinVig,CanPos,(select sum(CantidadRR) from r15 where NumCerR=NumReg and Estatus=0) as entregados');
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		/*function verCertificado(){
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		public function getCertificadosNum($cer){			
			$this->db->where($this->id,$cer);
			$query = $this->db->get($this->tabla);
			return $query->result();
			
		}*/
		function verRemision(){
			$this->db->where('NumRegR >',0);			
			$this->db->where('RemisionR !=',0);
			$this->db->order_by($this->rem);
			$query=$this->db->get($this->tablarem);
			return $query->result();			
		}
	 	function actualizara($id,$cer){
			$data=array($this->cer=>$cer);
			$this->db->where($this->rem,$id);
			$this->db->update($this->tablarem,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function actualizarga($cer,$ini,$fin){
			while($ini <= $fin){					
				$data=array($this->cer=>$cer);		
				$this->db->where($this->rem,$ini);	
				$this->db->update($this->tablarem,$data);
				$ini=$ini+1;	
			}
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
    }
    
?>