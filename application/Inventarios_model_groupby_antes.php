<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Inventarios_model extends CI_Model {
        public $tablam="maquilas_";
		public $ide="ide";public $fece="fece";public $idae="idae";public $idce="idce";public $idge="idge";	
		
		public $tablamf="maqfco_"; 
		public $idef="idef";public $estf="estf";public $grgf="grgf";public $grpf="grpf";public $ntpf="ntpf";public $kgtf="kgtf";
		public $idfe="idfe";
		
		public $tablamp="maqpro_";
		public $idep="idep";public $lotp="lotp";public $idtp="idtp";public $masp="masp";public $kgmp="kgmp";public $kgsp="kgsp";
		public $idgp="idgp";public $idap="idap";public $prep="prep";public $fecp="fecp";
		public $idpe="idpe";public $estp="estp";public $gpop="gpop";
		
		public $tablae="entradas";	
        public $ident="ident";public $alment="alment";public $graent="graent";public $talent="talent";public $kgsent="kgsent";
		public $exient="exient";public $cicent="cicent";public $estent="estent";public $idsalt="idsalt";
        
		public $tablat="tallas";
		public $idt="idt";public $nomt="nomt";
		
		public $tablas="salidas";
		public $idsal="idsal";public $fecsal="fecsal";public $estsal="estsal";public $almsal="almsal";public $clisal="clisal";
		public $kgssal="kgssal";public $cajsal="cajsal";public $presal="presal";public $talsal="talsal";public $idents="idents";
		public $folsal="folsal";public $consala="consala";public $consalc="consalc";public $grasal="grasal";public $clo="ciclo";
		public $gpos="gpos";
		
		public $tablac="clientes";
		public $numero="Numero";public $con="con";public $razon="Razon";
		
        public $tablag="granjas";	
		public $tablaa="almacenes";
				
		public $idclicb="idclicb";public $nomcb="nomcb";public $domcb="domcb";public $telcb="telcb";
		public $tablacotcli="cotbcli";
		
		public $idtalcb="idtalcb";public $foltalcb="foltalcb";public $talcb="talcb";public $pmtalcb="pmtalcb";public $pytalcb="pytalcb";
		public $clicb="clicb";public $feccb="feccb";public $zonacb="zonacb";
		public $tablacottal="cotbtalla";
		//select idge,ide,(select sum(ntpf*kgtf) from maqfco_19 where idfe=ide)as fresco,( select sum(masp*kgmp) from maqpro_19 where idpe=ide )as maquilado from maquilas_19 group by ide order by idge,ide
		
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getrc($filter){
			//SELECT nomcb,nomt,feccb,foltalcb,pmtalcb,pytalcb from cotbcli inner join( tallas inner join cotbtalla on talcb=idt )on clicb=idclicb order by clicb,nomt,feccb
			$this->db->select("nomcb,nomt,feccb,foltalcb,pmtalcb,pytalcb,zonacb");
			$this->db->join($this->tablacottal, 'clicb=idclicb','inner');
			$this->db->join($this->tablat, 'talcb=idt','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('clicb');$this->db->order_by('nomt');$this->db->order_by('feccb');
			$result = $this->db->get($this->tablacotcli);
			$data = array();$fec=new Libreria();$cli='';$tal='';$dia='';$fol='';$zon='';
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 if($cli!=$row->nomcb) {$cli=$row->nomcb;}else{ $row->nomcb="";}
				 if($tal!=$row->nomt) {$tal=$row->nomt;}else{ $row->nomt="";}
				 if($dia!=$row->feccb) {$dia=$row->feccb;$row->feccb = $fec->fecha($row->feccb);}else{ $row->feccb="";}
				 if($fol!=$row->foltalcb) {$fol=$row->foltalcb;$row->foltalcb=str_pad(($row->foltalcb), 5, "0", STR_PAD_LEFT);}else{ $row->foltalcb="";}
				 if($zon!=$row->zonacb) {$zon=$row->zonacb;}else{ $row->zonacb="";}
				 if($row->pmtalcb==0) $row->pmtalcb=''; else $row->pmtalcb='$'.number_format(($row->pmtalcb), 2, '.', ','); 
				 if($row->pytalcb==0) $row->pytalcb=''; else $row->pytalcb='$'.number_format(($row->pytalcb), 2, '.', ',');
				 $data[] = $row;
			 endforeach;
			}
			return $data;
		}	
		function getElementsrc($cic){        
        	$this->db->select("idt,nomt as val");
			$this->db->join($this->tablat, 'talcb=idt','inner');
			$this->db->group_by($this->idt);
			$this->db->group_by($this->nomt);
			$result = $this->db->get($this->tablacottal);
			$datac = array();        
        	foreach($result->result() as $rowc):
            	$datac[] = $rowc;
        	endforeach;        
        	return $datac;
    	}
		function datosclicot($cli){        
        	$this->db->select("nomcb,domcb,telcb");
			$this->db->from($this->tablacotcli);
			$this->db->where('idclicb',$cli);
			$query=$this->db->get();
			return $query->row();
    	}
		
		function agregarcotcli($nom,$dom,$tel){
			$datac=array($this->nomcb=>$nom,$this->domcb=>$dom,$this->telcb=>$tel);			
			$this->db->insert($this->tablacotcli,$datac);
			return $this->db->insert_id();
		}
		function actualizarcotcli($id,$nom,$dom,$tel){
			$datac=array($this->nomcb=>$nom,$this->domcb=>$dom,$this->telcb=>$tel);			
			$this->db->where($this->idclicb,$id);
			$this->db->update($this->tablacotcli,$datac);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function agregarcot($fec,$fol,$cli,$tal,$pmen,$pmay,$zon){
			$datat=array($this->feccb=>$fec,$this->clicb=>$cli,$this->foltalcb=>$fol,$this->talcb=>$tal,$this->pmtalcb=>$pmen,$this->pytalcb=>$pmay,$this->zonacb=>$zon);			
			$this->db->insert($this->tablacottal,$datat);
			return $this->db->insert_id();
		}
		
		function actualizarcot($id,$tal,$pmen,$pmay,$zon){
			$datat=array($this->talcb=>$tal,$this->pmtalcb=>$pmen,$this->pytalcb=>$pmay,$this->zonacb=>$zon);			
			$this->db->where($this->idtalcb,$id);
			$this->db->update($this->tablacottal,$datat);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		
		}
		function getcotizacionesd($filter){
			$this->db->join($this->tablat, 'talcb=idt','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablacottal);
			$data = array();
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 if($row->pmtalcb==0) $row->pmtalcb1=''; else $row->pmtalcb1='$'.number_format(($row->pmtalcb), 2, '.', ','); 
				 if($row->pytalcb==0) $row->pytalcb1=''; else $row->pytalcb1='$'.number_format(($row->pytalcb), 2, '.', ',');
				 if($row->zonacb=='') $row->zonacb=''; 
				  
				 $data[] = $row;
			 endforeach;
			}
			return $data;
		}	
		
		function getcotizaciones($filter){
			//$this->db->select('feccb,foltalcb,idclicb,nomcb,domcb,telcb');	
			$this->db->join($this->tablacottal, 'clicb=idclicb','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->group_by('foltalcb');$this->db->group_by('foltalcb');
			$result = $this->db->get($this->tablacotcli);
			$data = array();$fec=new Libreria();
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $row->feccot1 = $fec->fecha($row->feccb);
				 $row->folcot1=str_pad(($row->foltalcb), 5, "0", STR_PAD_LEFT);
				 $row->foltalcb=str_pad(($row->foltalcb), 5, "0", STR_PAD_LEFT);
				 $data[] = $row;
			 endforeach;
			}
			return $data;
		}	
	
		function getactual($filter,$cic){
			//select idt,nomt,sum(kgsp)as procesados,(select sum(kgssal) from salidas where talsal=idtp and estsal=1)as ventas 
			//from tallas inner join maqpro_19 on idtp=idt where lotp!='' group by idt order by idt
			
			//sacar el total de lo que queda para obtener el promedio por talla
			$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and ciclo=20'.$cic.')as ventas ');
			$this->db->join('tallas', 'idt=idtp','inner');
			$this->db->where('lotp !=','');
			//$this->db->group_by('idt');
			$this->db->group_by('gpoid');
			$this->db->group_by('nomt');
			//$this->db->order_by('gpoid');
			//$this->db->order_by('idt');
			//$this->db->group_by('gpoid');
			$this->db->order_by('nomt');
			
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$procesado=0;$ventas=0;$existencia=0;$procesadoa=0;$ventasa=0;$existenciaa=0;
			$procesadok=0;$ventask=0;$existenciak=0;
			$procesadoh=0;$ventash=0;$existenciah=0;$procesadot=0;$ventast=0;$existenciat=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				$existencia+=$row->procesado-$row->ventas;
			 endforeach;
			}
			
			
			$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and ciclo=20'.$cic.')as ventas ');
			$this->db->join('tallas', 'idt=idtp','inner');
			$this->db->where('lotp !=','');
			//$this->db->group_by('idt');
			//$this->db->order_by('idt');
			$this->db->group_by('gpoid');$this->db->group_by('nomt');
			$this->db->order_by('nomt');
			
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$procesado=0;$ventas=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 //buscar los datos de la granja de Ahome de acuerdo a la talla
				$row->procesadoa='';$row->ventasa='';$row->existenciaa='';$row->precioa='';
				$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal=4  and ciclo=20'.$cic.')as ventas,(select avg(prekgs) from salidas where talsal=idtp and estsal=1 and grasal=4  and ciclo=20'.$cic.')as precio ');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				$this->db->where('idgp =',4);
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('idt');
				$this->db->order_by('idt');
				$resulta = $this->db->get($this->tablamp.$cic);
				foreach($resulta->result() as $rowa):
					$procesadoa+=$rowa->procesado;$ventasa+=$rowa->ventas;
					if($rowa->procesado-$rowa->ventas>0) $row->existenciaa=number_format(($rowa->procesado-$rowa->ventas), 2, '.', ',').' kgs'; //else $row->existencia='';
					if($rowa->procesado>0) $row->procesadoa=number_format(($rowa->procesado), 2, '.', ','); //else $row->procesado='';
					if($rowa->ventas>0) $row->ventasa=number_format(($rowa->ventas), 2, '.', ','); // else $row->ventas='';
					if($rowa->precio>0) $row->precioa='$'.number_format(($rowa->precio), 2, '.', ','); // else $row->ventas='';
				endforeach;	
				
				 //buscar los datos de la granja de Kino de acuerdo a la talla
				$row->procesadok='';$row->ventask='';$row->existenciak=''; $row->preciok='';
				$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal=2  and ciclo=20'.$cic.')as ventas,(select avg(prekgs) from salidas where talsal=idtp and estsal=1 and grasal=4  and ciclo=20'.$cic.')as precio');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				$this->db->where('idgp =',2); 
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('idt');
				$this->db->order_by('idt');
				$resultk = $this->db->get($this->tablamp.$cic);
				foreach($resultk->result() as $rowk):
					$procesadok+=$rowk->procesado;$ventask+=$rowk->ventas;
					if($rowk->procesado-$rowk->ventas>0) $row->existenciak=number_format(($rowk->procesado-$rowk->ventas), 2, '.', ',').' kgs'; //else $row->existencia='';
					if($rowk->procesado>0) $row->procesadok=number_format(($rowk->procesado), 2, '.', ','); //else $row->procesado='';
					if($rowk->ventas>0) $row->ventask=number_format(($rowk->ventas), 2, '.', ','); // else $row->ventas='';
					if($rowk->precio>0) $row->precioa='$'.number_format(($rowk->precio), 2, '.', ','); // else $row->ventas='';
				endforeach;
					
				 //buscar los datos de la granja de Huatabampo de acuerdo a la talla
				$row->procesadoh='';$row->ventash='';$row->existenciah=''; $row->precioh='';
				$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal=6  and ciclo=20'.$cic.')as ventas,(select avg(prekgs) from salidas where talsal=idtp and estsal=1 and grasal=4  and ciclo=20'.$cic.')as precio');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				$this->db->where('idgp =',6); 
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('idt');
				$this->db->order_by('idt');
				$resultk = $this->db->get($this->tablamp.$cic);
				foreach($resultk->result() as $rowk):
					$procesadoh+=$rowk->procesado;$ventash+=$rowk->ventas;
					if($rowk->procesado-$rowk->ventas>0) $row->existenciah=number_format(($rowk->procesado-$rowk->ventas), 2, '.', ',').' kgs'; //else $row->existencia='';
					if($rowk->procesado>0) $row->procesadoh=number_format(($rowk->procesado), 2, '.', ','); //else $row->procesado='';
					if($rowk->ventas>0) $row->ventash=number_format(($rowk->ventas), 2, '.', ','); // else $row->ventas='';
					if($rowk->precio>0) $row->precioh='$'.number_format(($rowk->precio), 2, '.', ','); // else $row->ventas='';
				endforeach;	
				
				//buscar los datos de la granja de Topolobampo de acuerdo a la talla
				$row->procesadot='';$row->ventast='';$row->existenciat=''; $row->preciot='';
				$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal=10  and ciclo=20'.$cic.')as ventas,(select avg(prekgs) from salidas where talsal=idtp and estsal=1 and grasal=4  and ciclo=20'.$cic.')as precio');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				$this->db->where('idgp =',10); 
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('idt');
				$this->db->order_by('idt');
				$resultt = $this->db->get($this->tablamp.$cic);
				foreach($resultt->result() as $rowt):
					$procesadot+=$rowt->procesado;$ventast+=$rowt->ventas;
					if($rowt->procesado-$rowt->ventas>0) $row->existenciat=number_format(($rowt->procesado-$rowt->ventas), 2, '.', ',').' kgs'; //else $row->existencia='';
					if($rowt->procesado>0) $row->procesadot=number_format(($rowt->procesado), 2, '.', ','); //else $row->procesado='';
					if($rowt->ventas>0) $row->ventast=number_format(($rowt->ventas), 2, '.', ','); // else $row->ventas='';
					if($rowt->precio>0) $row->preciot='$'.number_format(($rowt->precio), 2, '.', ','); // else $row->ventas='';
				endforeach;		

				$procesado+=$row->procesado;$ventas+=$row->ventas;
				//if($row->idge==2) $row->granja='OA-Kino'; elseif($row->idge==4) $row->granja='OA-Ahome'; else  $row->granja='';
				if((($row->procesado-$row->ventas)/$existencia)>0)$row->por=number_format((($row->procesado-$row->ventas)/$existencia)*100, 2, '.', ',').'%'; else $row->por='';
				if($row->procesado-$row->ventas>0) $row->existencia=number_format(($row->procesado-$row->ventas), 2, '.', ',').' kgs'; else $row->existencia='';
				if($row->procesado>0) $row->procesado=number_format(($row->procesado), 2, '.', ','); else $row->procesado='';
				if($row->ventas>0) $row->ventas=number_format(($row->ventas), 2, '.', ','); else $row->ventas='';
				 
				$data[] = $row;
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->procesadoa='';$rowt->ventasa='';$rowt->existenciaa='';
				if($procesadoa>0) $rowt->procesadoa=number_format(($procesadoa), 2, '.', ',').' kgs'; 
				if($ventasa>0) $rowt->ventasa=number_format(($ventasa), 2, '.', ',').' kgs';
				if($procesadoa>0) $rowt->existenciaa=number_format(($procesadoa-$ventasa), 2, '.', ',').' kgs';  
				$rowt->procesadok='';$rowt->ventask='';$rowt->existenciak=''; 
				if($procesadok>0) $rowt->procesadok=number_format(($procesadok), 2, '.', ',').' kgs'; 
				if($ventask>0) $rowt->ventask=number_format(($ventask), 2, '.', ',').' kgs';
				if($procesadok>0) $rowt->existenciak=number_format(($procesadok-$ventask), 2, '.', ',').' kgs';  
				$rowt->procesadoh='';$rowt->ventash='';$rowt->existenciah=''; 
				if($procesadoh>0) $rowt->procesadoh=number_format(($procesadoh), 2, '.', ',').' kgs'; 
				if($ventash>0) $rowt->ventash=number_format(($ventash), 2, '.', ',').' kgs';
				if($procesadoh>0) $rowt->existenciah=number_format(($procesadoh-$ventash), 2, '.', ',').' kgs';
				$rowt->procesadot='';$rowt->ventast='';$rowt->existenciat=''; 
				if($procesadot>0) $rowt->procesadot=number_format(($procesadot), 2, '.', ',').' kgs'; 
				if($ventast>0) $rowt->ventast=number_format(($ventast), 2, '.', ',').' kgs';
				if($procesadot>0) $rowt->existenciat=number_format(($procesadot-$ventast), 2, '.', ',').' kgs';
				$rowt->nomt='Total:';$rowt->por='';$rowt->precioa='';$rowt->preciok='';$rowt->precioh='';$rowt->preciot='';
				if($procesado>0) $rowt->procesado=number_format(($procesado), 2, '.', ',').' kgs'; else $rowt->procesado='';
				if($ventas>0) $rowt->ventas=number_format(($ventas), 2, '.', ',').' kgs'; else $rowt->ventas='';
				if($procesado>0) $rowt->existencia=number_format(($procesado-$ventas), 2, '.', ',').' kgs'; else $rowt->existencia='';
				$data[] = $rowt;	
			 endforeach;
			
			return $data;
		}
		
		function gettodo($filter,$cic){
			//select idge,sum((select sum(ntpf*kgtf) from maqfco_19 where idfe=ide))as fresco,sum(( select sum(masp*kgmp) from maqpro_19 where idpe=ide and lotp!='' ))as maquilado
			// from maquilas_19 group by idge order by idge,ide 
			$this->db->select("idge,sum((select sum(ntpf*kgtf) from maqfco_$cic where idfe=ide))as fresco,sum(( select sum(masp*kgmp) from maqpro_$cic where idpe=ide and lotp!='' ))as maquilado");
			$this->db->where('idge >',0);
			$this->db->group_by('idge');
			$this->db->order_by('idge');
			$this->db->order_by('ide');
			$result = $this->db->get($this->tablam.$cic);
			$data = array();$fresco=0;$maquilado=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				$fresco+=$row->fresco;$maquilado+=$row->maquilado;
				if($row->idge==2) $row->granja='OA-Kino'; elseif($row->idge==4) $row->granja='OA-Ahome'; elseif($row->idge==5) $row->granja='Matacahui'; elseif($row->idge==6) $row->granja='OA-Huatabampo';elseif($row->idge==10) $row->granja='OA-Topolobampo'; elseif($row->idge==12) $row->granja='Astorga';else  $row->granja='';
				if($row->maquilado!='' && $row->fresco!='' ) $row->rendimiento=number_format((($row->maquilado/$row->fresco)*100), 2, '.', ','); else $row->rendimiento='';
				if($row->fresco>0) $row->fresco=number_format(($row->fresco), 2, '.', ','); else $row->fresco='';
				if($row->maquilado>0) $row->maquilado=number_format(($row->maquilado), 2, '.', ','); else $row->mauqilado='';
				
				$data[] = $row;
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->granja='Total:';
				if($fresco>0) $rowt->fresco=number_format(($fresco), 2, '.', ','); else $rowt->fresco='';
				if($maquilado>0) $rowt->maquilado=number_format(($maquilado), 2, '.', ','); else $rowt->maquilado='';
				if($maquilado!=0 && $fresco!=0 ) $rowt->rendimiento=number_format((($maquilado/$fresco)*100), 2, '.', ','); else $rowt->rendimiento='';
				$data[] = $rowt;	
			 endforeach;
			
			return $data;
		}
		
		
        function getexitall($filter,$cicx){
			//SELECT nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal 
			// from tallas inner join entradas on talent=idt group by  talent
			$this->db->select('talent,grupo,gpoid,nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal');
			//$this->db->join('almacenes', 'ida=alment','inner');
			//$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->group_by('alment');
			//$this->db->group_by('graent');
			//$this->db->group_by('talent');
			$this->db->group_by('gpoid');
			$this->db->order_by('grupo');
			//$this->db->order_by('idt');					
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;$exiact=0;$talla=0;$gpo=0;
			$exisel=0;$exifri=0;$exibar=0;$exisea=0;$exisol=0;$exibaj=0;$exifre=0;$exiame=0;$exiali=0;$exizel=0;
			$exibas=0;$exicmo=0;$exifmr=0;$exitma=0;$exitsc=0;$eximcc=0;$exihor=0;$exifga=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $entradas=0;$salidas=0;
				$talla=$row->talent;$gpo=$row->gpoid;
				//$exiact+=$row->ent-$row->sal;
				//
				$row->exisel='';$row->exifri='';$row->exibar='';$row->exisea='';$row->exisol='';$row->exibaj='';
				$row->exifre='';$row->exiame='';$row->exiali='';$row->exizel='';$row->exibas='';$row->exicmo='';
				$row->exifmr='';$row->exitma='';$row->exitsc='';$row->eximcc='';$row->exihor='';$row->exifga='';
				//suma la existecncia por alamacen 1
				$almac=1;
				while($almac<=21){
					$e=0;
					$this->db->select('sum(kgsp) as totkg');	
					//$this->db->where('idtp',$talla);
					$this->db->where('gpop',$gpo);
					$this->db->where('idap',$almac);
					$this->db->join($this->tablamp.$cicx, 'idtp=idt','inner');
					$resultes = $this->db->get('tallas'); 
					foreach ($resultes->result() as $rowes):
						$e=$rowes->totkg;
					endforeach;
					//suma todas las salidas de esta entrada
					$s=0;
					$this->db->select('sum(kgssal) as kgssal');	
					//$this->db->where($this->talsal,$talla);
					$this->db->where('gpos',$gpo);
					$this->db->where('almsal',$almac);
					$this->db->where('ciclo','20'.$cicx);
					$resulta = $this->db->get($this->tablas);
					foreach ($resulta->result() as $rows):	
						$s=$rows->kgssal;
					endforeach;
				 	$row->eent=$e;$row->ssal=$s;
					if(($e-$s)>0){
						switch ($almac) {
							case 1: $row->exisel=number_format(($e-$s), 2, '.', ',');$exisel+=$e-$s;$almac=2;break;
							case 2: $row->exifri=number_format(($e-$s), 2, '.', ',');$exifri+=$e-$s;$almac=4;break;
							case 4: $row->exibar=number_format(($e-$s), 2, '.', ',');$exibar+=$e-$s;$almac=5;break;
							case 5: $row->exisea=number_format(($e-$s), 2, '.', ',');$exisea+=$e-$s;$almac=6;break;
							case 6: $row->exisol=number_format(($e-$s), 2, '.', ',');$exisol+=$e-$s;$almac=7;break;
							case 7: $row->exibaj=number_format(($e-$s), 2, '.', ',');$exibaj+=$e-$s;$almac=8;break;
							case 8: $row->exifre=number_format(($e-$s), 2, '.', ',');$exifre+=$e-$s;$almac=9;break;
							case 9: $row->exiame=number_format(($e-$s), 2, '.', ',');$exiame+=$e-$s;$almac=10;break;
							case 10: $row->exiali=number_format(($e-$s), 2, '.', ',');$exiali+=$e-$s;$almac=11;break;
							case 11: $row->exizel=number_format(($e-$s), 2, '.', ',');$exizel+=$e-$s;$almac=13;break;
							case 13: $row->exibas=number_format(($e-$s), 2, '.', ',');$exibas+=$e-$s;$almac=15;break;
							case 15: $row->exicmo=number_format(($e-$s), 2, '.', ',');$exicmo+=$e-$s;$almac=16;break;
							case 16: $row->exifmr=number_format(($e-$s), 2, '.', ',');$exifmr+=$e-$s;$almac=17;break;
							case 17: $row->exitma=number_format(($e-$s), 2, '.', ',');$exitma+=$e-$s;$almac=18;break;
							case 18: $row->exitsc=number_format(($e-$s), 2, '.', ',');$exitsc+=$e-$s;$almac=19;break;
							case 19: $row->eximcc=number_format(($e-$s), 2, '.', ',');$eximcc+=$e-$s;$almac=20;break;
							case 20: $row->exihor=number_format(($e-$s), 2, '.', ',');$exihor+=$e-$s;$almac=21;break;
							case 21: $row->exifga=number_format(($e-$s), 2, '.', ',');$exifga+=$e-$s;$almac=22;break;
						}
						$entradas+=$e-$s;
					}else{
						switch ($almac) {
							case 1: $row->exisel='';$almac=2;break;
							case 2: $row->exifri='';$almac=4;break;
							case 4: $row->exibar='';$almac=5;break;
							case 5: $row->exisea='';$almac=6;break;
							case 6: $row->exisol='';$almac=7;break;
							case 7: $row->exibaj='';$almac=8;break;
							case 8: $row->exifre='';$almac=9;break;
							case 9: $row->exiame='';$almac=10;break;
							case 10: $row->exiali='';$almac=11;break;
							case 11: $row->exizel='';$almac=13;break;
							case 13: $row->exibas='';$almac=15;break;
							case 15: $row->exicmo='';$almac=16;break;
							case 16: $row->exifmr='';$almac=17;break;
							case 17: $row->exitma='';$almac=18;break;
							case 18: $row->exitsc='';$almac=19;break;
							case 19: $row->eximcc='';$almac=20;break;
							case 20: $row->exihor='';$almac=21;break;
							case 21: $row->exifga='';$almac=22;break;
						}
					}
				}
				$row->exiact=number_format(($entradas), 2, '.', ',').' kgs';
				$data[] = $row;
				$tot=1;
			 	endforeach;
			}
			//Total
			$this->db->select('max(ident)');
			$resulta = $this->db->get($this->tablae);
			foreach ($resulta->result() as $rowt):	
				$rowt->grupo='Total:';
				if($exisel>0) $rowt->exisel=number_format(($exisel), 2, '.', ','); else $rowt->exisel='';
				if($exifri>0) $rowt->exifri=number_format(($exifri), 2, '.', ','); else $rowt->exifri='';
				if($exibar>0) $rowt->exibar=number_format(($exibar), 2, '.', ','); else $rowt->exibar='';
				if($exisea>0) $rowt->exisea=number_format(($exisea), 2, '.', ','); else $rowt->exisea='';
				if($exisol>0) $rowt->exisol=number_format(($exisol), 2, '.', ','); else $rowt->exisol='';
				if($exibaj>0) $rowt->exibaj=number_format(($exibaj), 2, '.', ','); else $rowt->exibaj='';
				if($exifre>0) $rowt->exifre=number_format(($exifre), 2, '.', ','); else $rowt->exifre='';
				if($exiame>0) $rowt->exiame=number_format(($exiame), 2, '.', ','); else $rowt->exiame='';
				if($exiali>0) $rowt->exiali=number_format(($exiali), 2, '.', ','); else $rowt->exiali='';
				if($exizel>0) $rowt->exizel=number_format(($exizel), 2, '.', ','); else $rowt->exizel='';
				if($exibas>0) $rowt->exibas=number_format(($exibas), 2, '.', ','); else $rowt->exibas='';
				if($exicmo>0) $rowt->exicmo=number_format(($exicmo), 2, '.', ','); else $rowt->exicmo='';
				if($exifmr>0) $rowt->exifmr=number_format(($exifmr), 2, '.', ','); else $rowt->exifmr='';
				if($exitma>0) $rowt->exitma=number_format(($exitma), 2, '.', ','); else $rowt->exitma='';
				if($exitsc>0) $rowt->exitsc=number_format(($exitsc), 2, '.', ','); else $rowt->exitsc='';
				if($eximcc>0) $rowt->eximcc=number_format(($eximcc), 2, '.', ','); else $rowt->eximcc='';
				if($exihor>0) $rowt->exihor=number_format(($exihor), 2, '.', ','); else $rowt->exihor='';
				if($exifga>0) $rowt->exifga=number_format(($exifga), 2, '.', ','); else $rowt->exifga='';
				//if(($exisel-$exifri)>0) $rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea), 2, '.', ',').' kgs'; else $rowt->exiact='';
				$rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea+$exisol+$exibaj+$exifre+$exiame+$exiali+$exizel+$exibas+$exicmo+$exifmr+$exitma+$exitsc+$eximcc+$exihor+$exifga), 2, '.', ',').' kgs'; 
				$data[] = $rowt;	
			 endforeach;
			
			return $data;
		}
		function getexitall12($filter,$cicx){
			//SELECT nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal 
			// from tallas inner join entradas on talent=idt group by  talent
			$this->db->select('talent,nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal');
			//$this->db->join('almacenes', 'ida=alment','inner');
			//$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->group_by('alment');
			//$this->db->group_by('graent');
			$this->db->group_by('talent');
			//$this->db->order_by('gpoid');
			$this->db->order_by('nomt');
			//$this->db->order_by('idt');					
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;$exiact=0;$talla=0;$exisel=0;$exifri=0;$exibar=0;$exisea=0;$exisol=0;$exibaj=0;$exifre=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $entradas=0;$salidas=0;
				$talla=$row->talent;
				//$exiact+=$row->ent-$row->sal;
				//
				$row->exisel='';$row->exifri='';$row->exibar='';$row->exisea='';$row->exisol='';$row->exibaj='';$row->exifre='';
				//suma la existecncia por alamacen 1
				$almac=1;
				while($almac<=8){
					$e=0;
				$this->db->select('sum(kgsp) as totkg');	
				$this->db->where('idtp',$talla);
				$this->db->where('idap',$almac);
				$this->db->join($this->tablamp.$cicx, 'idtp=idt','inner');
				$resultes = $this->db->get('tallas'); 
				foreach ($resultes->result() as $rowes):
					$e=$rowes->totkg;
				endforeach;
				//suma todas las salidas de esta entrada
				$s=0;
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->talsal,$talla);
				$this->db->where('almsal',$almac);
				$this->db->where('ciclo','20'.$cicx);
				$resulta = $this->db->get($this->tablas);
				foreach ($resulta->result() as $rows):	
					$s=$rows->kgssal;
				endforeach;
				 $row->eent=$e;$row->ssal=$s;
				if(($e-$s)>0){
					switch ($almac) {
						case 1: $row->exisel=number_format(($e-$s), 2, '.', ',');$exisel+=$e-$s;$almac=2;break;
						case 2: $row->exifri=number_format(($e-$s), 2, '.', ',');$exifri+=$e-$s;$almac=4;break;
						case 4: $row->exibar=number_format(($e-$s), 2, '.', ',');$exibar+=$e-$s;$almac=5;break;
						case 5: $row->exisea=number_format(($e-$s), 2, '.', ',');$exisea+=$e-$s;$almac=6;break;
						case 6: $row->exisol=number_format(($e-$s), 2, '.', ',');$exisol+=$e-$s;$almac=7;break;
						case 7: $row->exibaj=number_format(($e-$s), 2, '.', ',');$exibaj+=$e-$s;$almac=8;break;
						case 8: $row->exifre=number_format(($e-$s), 2, '.', ',');$exifre+=$e-$s;$almac=9;break;
					}
					$entradas+=$e-$s;
				}else{
					switch ($almac) {
						case 1: $row->exisel='';$almac=2;break;
						case 2: $row->exifri='';$almac=4;break;
						case 4: $row->exibar='';$almac=5;break;
						case 5: $row->exisea='';$almac=6;break;
						case 6: $row->exisol='';$almac=7;break;
						case 7: $row->exibaj='';$almac=8;break;
						case 8: $row->exifre='';$almac=9;break;
					}
				}
				
				}
				
				$row->exiact=number_format(($entradas), 2, '.', ',').' kgs';
				
				$data[] = $row;
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->nomt='Total:';
				if($exisel>0) $rowt->exisel=number_format(($exisel), 2, '.', ','); else $rowt->exisel='';
				if($exifri>0) $rowt->exifri=number_format(($exifri), 2, '.', ','); else $rowt->exifri='';
				if($exibar>0) $rowt->exibar=number_format(($exibar), 2, '.', ','); else $rowt->exibar='';
				if($exisea>0) $rowt->exisea=number_format(($exisea), 2, '.', ','); else $rowt->exisea='';
				if($exisol>0) $rowt->exisol=number_format(($exisol), 2, '.', ','); else $rowt->exisol='';
				if($exibaj>0) $rowt->exibaj=number_format(($exibaj), 2, '.', ','); else $rowt->exibaj='';
				if($exifre>0) $rowt->exifre=number_format(($exifre), 2, '.', ','); else $rowt->exifre='';
				//if(($exisel-$exifri)>0) $rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea), 2, '.', ',').' kgs'; else $rowt->exiact='';
				$rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea+$exisol+$exibaj+$exifre), 2, '.', ',').' kgs'; 
				$data[] = $rowt;	
			 endforeach;
			
			return $data;
		}
		 function getexitall2noseusa($filter,$cicx){
			//SELECT nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal 
			// from tallas inner join entradas on talent=idt group by  talent
			$this->db->select('talent,nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal');
			//$this->db->join('almacenes', 'ida=alment','inner');
			//$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->group_by('alment');
			//$this->db->group_by('graent');
			$this->db->group_by('talent');
			//$this->db->order_by('ida');
			//$this->db->order_by('nomg');
			//$this->db->order_by('idt');					
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;$exiact=0;$talla=0;$exisel=0;$exifri=0;$exibar=0;$exisea=0;$exisol=0;$exibaj=0;$exifre=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $entradas=0;$salidas=0;
				$talla=$row->talent;
				//$exiact+=$row->ent-$row->sal;
				//
				$row->exisel='';$row->exifri='';$row->exibar='';$row->exisea='';$row->exisol='';$row->exibaj='';$row->exifre='';
				//suma la existecncia por alamacen
				$this->db->select('sum(kgsp) as totkg');	
				$this->db->where('idtp',$talla);
				$this->db->where('idap',1);
				$this->db->join($this->tablamp.$cicx, 'idtp=idt','inner');
				//if($filter['where']!='') $this->db->where($filter['where']);
				$resultes = $this->db->get('tallas'); 
				foreach ($resultes->result() as $rowes):
					$e=$rowes->totkg;
				endforeach;
				//suma todas las salidas de esta entrada
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->talsal,$talla);
				$this->db->where('almsal',1);
				$result = $this->db->get($this->tablas);
				foreach ($result->result() as $rows):	
					$s=$rows->kgssal;
				endforeach;
				if($e-$s>0){$row->exisel=number_format(($e-$s), 2, '.', ',');$exisel+=$e-$s;$entradas+=$e-$s;}
				else{$row->exisel='';}
			/*	
				$this->db->select('sum(kgsent) as enta,(select sum(kgssal) from salidas where talsal=talent and almsal=1)as sala');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->estent,1);
				$this->db->where($this->alment,1);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$results = $this->db->get($this->tablae);
				foreach($results->result() as $rows):
					if($rows->enta-$rows->sala>0){$row->exisel=number_format(($rows->enta-$rows->sala), 2, '.', ',');$exisel+=$rows->enta-$rows->sala;$entradas+=$rows->enta-$rows->sala;}
				endforeach;	
				//$row->exiact=number_format(($entradas+$salidas), 2, '.', ',').' kgs';
				*/
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=2)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,2);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exifri=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exifri+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				//$row->exiact=number_format(($entradas+$salidas), 2, '.', ',').' kgs';
				
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=4)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,4);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exibar=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exibar+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				//$row->exiact=number_format(($entradas+$salidas), 2, '.', ',').' kgs';
				
				
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=5)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,5);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exisea=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exisea+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=6)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,6);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exisol=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exisol+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=7)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,7);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exibaj=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exibaj+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				
				$this->db->select('sum(kgsent) as entf,(select sum(kgssal) from salidas where talsal=talent and almsal=8)as salf');
				//$this->db->join('tallas', 'idt=talent','inner');
				$this->db->where($this->alment,8);
				$this->db->where($this->talent,$talla);
				//$this->db->group_by('talent');
				$resultf = $this->db->get($this->tablae);
				foreach($resultf->result() as $rowf):
					if($rowf->entf-$rowf->salf>0){$row->exifre=number_format(($rowf->entf-$rowf->salf), 2, '.', ',');$exifre+=$rowf->entf-$rowf->salf;$entradas+=$rowf->entf-$rowf->salf;}
				endforeach;
				
				$row->exiact=number_format(($entradas), 2, '.', ',').' kgs';
				
				$data[] = $row;
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->nomt='Total:';
				if($exisel>0) $rowt->exisel=number_format(($exisel), 2, '.', ','); else $rowt->exisel='';
				if($exifri>0) $rowt->exifri=number_format(($exifri), 2, '.', ','); else $rowt->exifri='';
				if($exibar>0) $rowt->exibar=number_format(($exibar), 2, '.', ','); else $rowt->exibar='';
				if($exisea>0) $rowt->exisea=number_format(($exisea), 2, '.', ','); else $rowt->exisea='';
				if($exisol>0) $rowt->exisol=number_format(($exisol), 2, '.', ','); else $rowt->exisol='';
				if($exibaj>0) $rowt->exibaj=number_format(($exibaj), 2, '.', ','); else $rowt->exibaj='';
				if($exifre>0) $rowt->exifre=number_format(($exifre), 2, '.', ','); else $rowt->exifre='';
				//if(($exisel-$exifri)>0) $rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea), 2, '.', ',').' kgs'; else $rowt->exiact='';
				$rowt->exiact=number_format(($exisel+$exifri+$exibar+$exisea+$exisol+$exibaj+$exifre), 2, '.', ',').' kgs'; 
				$data[] = $rowt;	
			 endforeach;
			
			return $data;
		}
		
		
		function getinventariose($filter,$cic){
			//SELECT nomt,sum(kgsent) as ent,(select sum(kgssal) from salidas where talsal=talent)as sal from tallas inner join entradas on talent=idt group by  talent
			$this->db->select('noma,nomg,nomt,sum(kgsent) as kgsent,exient,cicent,cona,siga,sigg,alment,graent,talent,ident,gpoid');
			$this->db->join('almacenes', 'ida=alment','inner');
			$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where('kgsent >',0);
			$this->db->group_by('alment');
			$this->db->group_by('graent');
			$this->db->group_by('talent');
			$this->db->order_by('ida');
			$this->db->order_by('nomg');
			$this->db->order_by('idt');					
			//$this->db->order_by('gpoid');$this->db->order_by('grupo');
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $data1 = array();
				 $idtalla=$row->talent;$idgranja=$row->graent;
				 $idSalida=$row->ident;$e=0;$s=0;$exi=0;
				 $row->pre=1;
				if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				if($alm!=$row->noma){
					if($tot>0){
						$this->db->select('max(ident)');
			 			$resulta = $this->db->get($this->tablae);
			 			foreach ($resulta->result() as $rowt):		
							$rowt->pre=1; 		
							$rowt->cic='';$rowt->noma1 = ""; $rowt->nomg='';$rowt->nomt='Total:';
							$rowt->kgsent=number_format(($tote), 2, '.', ',');
							$rowt->kgssal=number_format(($tots), 2, '.', ',');
							$rowt->exient=number_format(($tote-$tots), 2, '.', ',');
							$data[] = $rowt;	
			 			endforeach;
					   $tot=0;$tote=0;$tots=0;$gra='';
					}		
					$alm=$row->noma;$row->noma1=$row->noma;
				
				}else{ $row->noma1="";}
				if($gra!=$row->nomg){$gra=$row->nomg;}else{ $row->nomg="";}
				$e=$row->kgsent;
				$tote+=$row->kgsent;
				$row->kgsent=number_format(($row->kgsent), 2, '.', ',');
				
				//suma todas las salidas de esta entrada
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->group_by($idSalida);
				$result = $this->db->get($this->tablas);
				foreach ($result->result() as $rows):	
					$s=$rows->kgssal;
					$tots+=$rows->kgssal;			
					if($rows->kgssal > 0) $row->kgssal=number_format($rows->kgssal, 2, '.', ','); else $row->kgssal='';					
				endforeach;
				$row->exient=number_format(($e-$s), 2, '.', ',');$exi=$e-$s;
				$data[] = $row;
				
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->pre=1; 			
				$rowt->cic='';$rowt->noma1 = ""; $rowt->nomg='';$rowt->nomt='Total:';
				$rowt->kgsent=number_format(($tote), 2, '.', ',');
				if($tots>0) $rowt->kgssal=number_format(($tots), 2, '.', ','); else $rowt->kgssal='';
				$rowt->exient=number_format(($tote-$tots), 2, '.', ',');
				$data[] = $rowt;	
			 endforeach;
			return $data;
		}
		
		function agregarsal($fec,$est,$alm,$cli,$caj,$pre,$tal,$ent,$fol,$coa,$coc,$gra,$clo,$gpos){
			//agrego la salida
			$kgs=$caj*$pre;	
			//si el estatus es traslado relizo una entrada y se realiza lo equivalente a lo procesado
			if($est==2){
				$this->db->select('MAX(idsal) as ultimo');
				$query = $this->db->get($this->tablas);
				$uf=0;
				foreach($query->result() as $row): 
					$uf=$row->ultimo+1;										
				endforeach;
				//aqui se genera la entrada al almacen
				$dataent=array($this->talent=>$tal,$this->alment=>$cli,$this->graent=>$gra,$this->kgsent=>$kgs,$this->cicent=>$clo,$this->estent=>2,$this->idsalt=>$uf);	
				$this->db->insert($this->tablae,$dataent);
				//aqui lo procesado en maquila
				//$data=array($this->lotp=>$lot,$this->idtp=>$idt,$this->masp=>$mas,$this->kgmp=>$kgm,$this->idgp=>$idg,$this->idap=>$ida,$this->idpe=>$idp,$this->kgsp=>$kgsp,$this->prep=>$pre,$this->fecp=>$fec);
				$datapro=array($this->idtp=>$tal,$this->masp=>$caj,$this->kgmp=>$pre,$this->idgp=>$gra,$this->idap=>$cli,$this->kgsp=>$kgs,$this->fecp=>$fec,$this->estp=>2,$this->prep=>0,$this->lotp=>'',$this->idpe=>$fol,$this->gpop=>$gpos);			
				$this->db->insert($this->tablamp.$clo,$datapro);
			}	
			$clo='20'.$clo;
			$data=array($this->fecsal=>$fec,$this->estsal=>$est,$this->almsal=>$alm,$this->clisal=>$cli,$this->cajsal=>$caj,$this->presal=>$pre,$this->kgssal=>$kgs,$this->talsal=>$tal,$this->idents=>$ent,$this->folsal=>$fol,$this->consala=>$coa,$this->consalc=>$coc,$this->grasal=>$gra,$this->clo=>$clo,$this->gpos=>$gpos);			
			$this->db->insert($this->tablas,$data);
			return $this->db->insert_id();
		}
		function actualizarsal($id_post,$caj,$pre,$coa,$coc,$est,$idents,$gpos){
	 		$kgs=$caj*$pre;
			if($est==2){
				//obtengo las cajas y presentacion para saber si se suma o se resta a la entrada en curso
				$this->db->select('cajsal,presal');
				$this->db->where($this->idsal,$id_post);
				$querys = $this->db->get($this->tablas);
				foreach($querys->result() as $rows): 
					$kgsa=$rows->cajsal*$rows->presal;										
				endforeach;
				//busco el origen de entrada para actualizarla
				$this->db->select('kgsent,exient');
				$this->db->where($this->ident,$idents);
				$querye = $this->db->get($this->tablae);
				foreach($querye->result() as $rowe): 
					$kgse=$rowe->kgsent;										
				endforeach;
				//actualizo la entrada
				if($kgs>$kgsa){
					//sumo
					$dif=$kgse+($kgs-$kgsa);
				}else{
					//resto
					$dif=$kgse-($kgsa-$kgs);
				}
				$data=array($this->kgsent=>$dif);			
				$this->db->where($this->idsalt,$id_post);
				$this->db->update($this->tablae,$data);
				
				
				//ahora actualizo la entrada por traslado
				
				//busco el origen de entrada para actualizarla
				$this->db->select('kgsent,exient');
				$this->db->where($this->idsalt,$id_post);
				$querye = $this->db->get($this->tablae);
				foreach($querye->result() as $rowe): 
					$kgse=$rowe->kgsent;										
				endforeach;
				//actualizo la entrada
				if($kgs>$kgse){
					//sumo
					$dif=$kgse+($kgs-$kgse);
				}else{
					//resto
					$dif=$kgse-($kgse-$kgs);
				}
				$data=array($this->kgsent=>$dif);			
				$this->db->where($this->idsalt,$id_post);
				$this->db->update($this->tablae,$data);
				
			}
			$data=array($this->cajsal=>$caj,$this->presal=>$pre,$this->kgssal=>$kgs,$this->consala=>$coa,$this->consalc=>$coc,$this->gpos=>$gpos);			
			$this->db->where($this->idsal,$id_post);
			$this->db->update($this->tablas,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
//existecnia con cajas
//SELECT idgp,idtp,nomt,sum(kgsp) as totkg,sum(masp), kgmp from tallas inner join maqpro_19 on idtp=idt group by idgp,idtp,kgmp
		function getsalidas($filter){
			//SELECT fecsal,folsal,noma,Razon,idsal from almacenes inner join( clientes inner join salidas on clisal=Numero )on almsal=ida group by folsal order by fecsal
			$this->db->select('fecsal,folsal,almsal,noma,consala,clisal,Razon,con,consalc,idsal,estsal');
			$this->db->join('almacenes', 'ida=almsal','inner');
			$this->db->join('clientes', 'Numero=clisal','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->group_by('folsal');
			$this->db->order_by('fecsal','DESC');
			$this->db->order_by('folsal','DESC');
			$result = $this->db->get($this->tablas);
			$data = array();$cic='';$alm='';$gra='';
			if($result->num_rows()>0){
				 $feci=new Libreria();
			 foreach($result->result() as $row):
				$row->fecsal1=$feci->fecha23($row->fecsal);
				$row->folsal=str_pad($row->folsal, 5, "0", STR_PAD_LEFT);
				 if($row->estsal==1) $row->estatus='Venta'; else $row->estatus='Traslado'; 
				/*if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				if($alm!=$row->noma){$alm=$row->noma;$row->noma1=$row->noma;}else{ $row->noma1="";}
				if($gra!=$row->nomg){$gra=$row->nomg;}else{ $row->nomg="";}
				$row->kgsent=number_format(($row->kgsent), 2, '.', ',');
				$row->exient=number_format(($row->exient), 2, '.', ',');
				$row->kgssal='';*/
				$data[] = $row;
			 endforeach;
			}
			return $data;
		}
		function getsalidasd($filter){
			//SELECT fecsal,folsal,noma,Razon,idsal from almacenes inner join( clientes inner join salidas on clisal=Numero )on almsal=ida group by folsal order by fecsal
			$this->db->select('idsal,kgssal,cajsal,presal,talsal,idents,folsal,nomt,consala,consalc,nomg,gpos');
			$this->db->join('tallas', 'idt=talsal','inner');
			$this->db->join('granjas', 'idg=grasal','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->group_by('folsal');
			//$this->db->order_by('fecsal','DESC');
			$result = $this->db->get($this->tablas);
			$data = array();$cic='';$alm='';$gra='';
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				$row->kgssal=number_format(($row->kgssal), 2, '.', ',');
				$row->presal=number_format(($row->presal), 3, '.', ',');
				$row->cajsal=number_format(($row->cajsal), 2, '.', ',');
				/*if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				if($alm!=$row->noma){$alm=$row->noma;$row->noma1=$row->noma;}else{ $row->noma1="";}
				if($gra!=$row->nomg){$gra=$row->nomg;}else{ $row->nomg="";}
				$row->kgsent=number_format(($row->kgsent), 2, '.', ',');
				$row->exient=number_format(($row->exient), 2, '.', ',');
				$row->kgssal='';*/
				$data[] = $row;
			 endforeach;
			}
			return $data;
		}
		
		
		
		function getentradas($filter,$cic){
			//SELECT fece,noma,nomg,idpe from almacenes inner join( granjas inner join inventarios on idge=idg )on idae=ida where fece='2018-06-09'
			$this->db->select($this->fecp);
			//$this->db->join('tallas', 'idt=idtp','inner');
			$this->db->group_by($this->fecp);
			$this->db->order_by($this->fecp);
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();
			if($result->num_rows()>0){
			 $feci=new Libreria();$tot= array();
			 $cont=1;$diap='';$nt=0;
			 //cuentas el numero de tallas
			 $this->db->select('count(*) as nt');
			 $resultt = $this->db->get('tallas');
			 foreach($resultt->result() as $rowt):
				 $nt=$rowt->nt;
			 endforeach;		 
			 while($cont<=$nt){$tot[$cont]=0;$cont+=1; }	
			 foreach($result->result() as $row):
				$row->dia=$feci->fecha21($row->fecp);$diap=$row->fecp;
				$row->t1='';$row->t2='';$row->t3='';$row->t4='';$row->t5='';$row->t6='';$row->t7='';$row->t8='';$row->t9='';$row->t10='';                  
				$row->t11='';$row->t12='';$row->t13='';$row->t14='';$row->t15='';$row->t16='';$row->t17='';$row->t18='';$row->t19='';$row->t20='';
				$row->t21='';$row->t22='';$row->t23='';$row->t24='';$row->t25='';$row->t26='';$row->t27='';$row->t28='';$row->t29='';$row->t30='';
				$row->t31='';$row->t32='';$row->t33='';$row->t34='';$row->t35='';$row->t36='';$row->t37='';$row->t38='';$row->t39='';$row->t40='';
				$row->t41='';$row->t42='';$row->t43='';$row->t44='';$row->t45='';$row->t46='';$row->t47='';$row->t48='';$row->t49='';$row->t50='';
				$row->t51='';$row->t52='';$row->t53='';$row->t54='';$row->t55='';$row->t56='';$row->t57='';$row->t58='';$row->t59='';$row->t60='';$row->t61='';$row->t62='';$row->t63='';$row->t64='';$row->t65='';$row->t66='';$row->t67='';
				$row->tg='';
				$td=0;$kgsp=0;
				//de acuerdo a la fecha busco el total procesado segun la talla
				//$query=$this->db->query("select	fecp,idtp,sum(kgsp) as kgsp,nomt from tallas inner join maqpro_$cic on idtp=idt where fecp='$diap' group by nomt order by fecp,nomt");
				$this->db->select("fecp,idtp,sum(kgsp) as kgsp");
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where($this->fecp,$diap);
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->group_by($this->nomt);
				$this->db->order_by($this->fecp);
				$this->db->order_by($this->nomt);
				$query = $this->db->get($this->tablamp.$cic);
				foreach ($query->result() as $rowq):
					$td+=$rowq->kgsp;$kgsp=$rowq->kgsp;
					switch($rowq->idtp){
						case 1: $row->t1=$kgsp;$tot[1]+=$kgsp; break;
						case 2: $row->t2=$kgsp;$tot[2]+=$kgsp; break;
						case 3: $row->t3=$kgsp;$tot[3]+=$kgsp; break;
						case 4: $row->t4=$kgsp;$tot[4]+=$kgsp; break;
						case 5: $row->t5=$kgsp;$tot[5]+=$kgsp; break;
						case 6: $row->t6=$kgsp;$tot[6]+=$kgsp; break;
						case 7: $row->t7=$kgsp;$tot[7]+=$kgsp; break;
						case 8: $row->t8=$kgsp;$tot[8]+=$kgsp; break;
						case 9: $row->t9=$kgsp;$tot[9]+=$kgsp; break;
						case 10: $row->t10=$kgsp;$tot[10]+=$kgsp; break;
						case 11: $row->t11=$kgsp;$tot[11]+=$kgsp; break;
						case 12: $row->t12=$kgsp;$tot[12]+=$kgsp; break;
						case 13: $row->t13=$kgsp;$tot[13]+=$kgsp; break;
						case 14: $row->t14=$kgsp;$tot[14]+=$kgsp; break;
						case 15: $row->t15=$kgsp;$tot[15]+=$kgsp; break;
						case 16: $row->t16=$kgsp;$tot[16]+=$kgsp; break;
						case 17: $row->t17=$kgsp;$tot[17]+=$kgsp; break;
						case 18: $row->t18=$kgsp;$tot[18]+=$kgsp; break;
						case 19: $row->t19=$kgsp;$tot[19]+=$kgsp; break;
						case 20: $row->t20=$kgsp;$tot[20]+=$kgsp; break;
						case 21: $row->t21=$kgsp;$tot[21]+=$kgsp; break;
						case 22: $row->t22=$kgsp;$tot[22]+=$kgsp; break;
						case 23: $row->t23=$kgsp;$tot[23]+=$kgsp; break;
						case 24: $row->t24=$kgsp;$tot[24]+=$kgsp; break;
						case 25: $row->t25=$kgsp;$tot[25]+=$kgsp; break;
						case 26: $row->t26=$kgsp;$tot[26]+=$kgsp; break;
						case 27: $row->t27=$kgsp;$tot[27]+=$kgsp; break;
						case 28: $row->t28=$kgsp;$tot[28]+=$kgsp; break;
						case 29: $row->t29=$kgsp;$tot[29]+=$kgsp; break;
						case 30: $row->t30=$kgsp;$tot[30]+=$kgsp; break;
						case 31: $row->t31=$kgsp;$tot[31]+=$kgsp; break;
						case 32: $row->t32=$kgsp;$tot[32]+=$kgsp; break;
						case 33: $row->t33=$kgsp;$tot[33]+=$kgsp; break;
						case 34: $row->t34=$kgsp;$tot[34]+=$kgsp; break;
						case 35: $row->t35=$kgsp;$tot[35]+=$kgsp; break;
						case 36: $row->t36=$kgsp;$tot[36]+=$kgsp; break;
						case 37: $row->t37=$kgsp;$tot[37]+=$kgsp; break;
						case 38: $row->t38=$kgsp;$tot[38]+=$kgsp; break;
						case 39: $row->t39=$kgsp;$tot[39]+=$kgsp; break;
						case 40: $row->t40=$kgsp;$tot[40]+=$kgsp; break;
						case 41: $row->t41=$kgsp;$tot[41]+=$kgsp; break;
						case 42: $row->t42=$kgsp;$tot[42]+=$kgsp; break;
						case 43: $row->t43=$kgsp;$tot[43]+=$kgsp; break;
						case 44: $row->t44=$kgsp;$tot[44]+=$kgsp; break;
						case 45: $row->t45=$kgsp;$tot[45]+=$kgsp; break;
						case 46: $row->t46=$kgsp;$tot[46]+=$kgsp; break;
						case 47: $row->t47=$kgsp;$tot[47]+=$kgsp; break;
						case 48: $row->t48=$kgsp;$tot[48]+=$kgsp; break;
						case 49: $row->t49=$kgsp;$tot[49]+=$kgsp; break;
						case 50: $row->t50=$kgsp;$tot[50]+=$kgsp; break;
						case 51: $row->t51=$kgsp;$tot[51]+=$kgsp; break;
						case 52: $row->t52=$kgsp;$tot[52]+=$kgsp; break;
						case 53: $row->t53=$kgsp;$tot[53]+=$kgsp; break;
						case 54: $row->t54=$kgsp;$tot[54]+=$kgsp; break;
						case 55: $row->t55=$kgsp;$tot[55]+=$kgsp; break;
						case 56: $row->t56=$kgsp;$tot[56]+=$kgsp; break;
						case 57: $row->t57=$kgsp;$tot[57]+=$kgsp; break;						
						case 58: $row->t58=$kgsp;$tot[58]+=$kgsp; break;
						case 59: $row->t59=$kgsp;$tot[59]+=$kgsp; break;
						case 60: $row->t60=$kgsp;$tot[60]+=$kgsp; break;
						case 61: $row->t61=$kgsp;$tot[61]+=$kgsp; break;
						case 62: $row->t62=$kgsp;$tot[62]+=$kgsp; break;
						case 63: $row->t63=$kgsp;$tot[63]+=$kgsp; break;
						case 64: $row->t64=$kgsp;$tot[64]+=$kgsp; break;
						case 65: $row->t65=$kgsp;$tot[65]+=$kgsp; break;
						case 66: $row->t66=$kgsp;$tot[66]+=$kgsp; break;
						case 67: $row->t67=$kgsp;$tot[67]+=$kgsp; break;
					}
				endforeach;	
				$row->tg = $td; 
				$data[] = $row; 
			 endforeach;
			 //totales
			
			 $cont=1;$tg=0;
			 while($cont<=$nt){$tg+=$tot[$cont];$cont+=1; }
			 $this->db->select('max(ide)');	
			 $result = $this->db->get($this->tablam.$cic);
			 foreach ($result->result() as $row):				
				$row->dia = "Total:";$row->tg = $tg; 
				 $row->t1='';$row->t2='';$row->t3='';$row->t4='';$row->t5='';$row->t6='';$row->t7='';$row->t8='';$row->t9='';$row->t10='';$row->t11='';$row->t12='';$row->t13='';$row->t14='';$row->t15='';$row->t16='';$row->t17='';$row->t18='';$row->t19='';$row->t20='';
				$row->t21='';$row->t22='';$row->t23='';$row->t24='';$row->t25='';$row->t26='';$row->t27='';$row->t28='';$row->t29='';$row->t30='';
				$row->t31='';$row->t32='';$row->t33='';$row->t34='';$row->t35='';$row->t36='';$row->t37='';$row->t38='';$row->t39='';$row->t40='';
				$row->t41='';$row->t42='';$row->t43='';$row->t44='';$row->t45='';$row->t46='';$row->t47='';$row->t48='';$row->t49='';$row->t50='';
				$row->t51='';$row->t52='';$row->t53='';$row->t54='';$row->t55='';$row->t56='';$row->t57='';$row->t58='';$row->t59='';$row->t60='';$row->t61='';$row->t62='';$row->t63='';$row->t64='';$row->t65='';$row->t66='';$row->t67='';
				if($tot[1]>0) $row->t1=$tot[1];	if($tot[2]>0) $row->t2=$tot[2]; if($tot[3]>0) $row->t3=$tot[3];
				if($tot[4]>0) $row->t4=$tot[4];	if($tot[5]>0) $row->t5=$tot[5];	if($tot[6]>0) $row->t6=$tot[6];
				if($tot[7]>0) $row->t7=$tot[7];	if($tot[8]>0) $row->t8=$tot[8];	if($tot[9]>0) $row->t9=$tot[9];
				if($tot[10]>0) $row->t10=$tot[10]; if($tot[11]>0) $row->t11=$tot[11]; if($tot[12]>0) $row->t12=$tot[12];
				if($tot[13]>0) $row->t13=$tot[13]; if($tot[14]>0) $row->t14=$tot[14]; if($tot[15]>0) $row->t15=$tot[15];
				if($tot[16]>0) $row->t16=$tot[16]; if($tot[17]>0) $row->t17=$tot[17]; if($tot[18]>0) $row->t18=$tot[18];
				if($tot[19]>0) $row->t19=$tot[19]; if($tot[20]>0) $row->t20=$tot[20]; if($tot[21]>0) $row->t21=$tot[21]; 
				if($tot[22]>0) $row->t22=$tot[22]; if($tot[23]>0) $row->t23=$tot[23]; if($tot[24]>0) $row->t24=$tot[24];
				if($tot[25]>0) $row->t25=$tot[25]; if($tot[26]>0) $row->t26=$tot[26]; if($tot[27]>0) $row->t27=$tot[27];
				if($tot[28]>0) $row->t28=$tot[28]; if($tot[29]>0) $row->t29=$tot[29]; if($tot[30]>0) $row->t30=$tot[30];
				if($tot[31]>0) $row->t31=$tot[31]; if($tot[32]>0) $row->t32=$tot[32]; if($tot[33]>0) $row->t33=$tot[33];
				if($tot[34]>0) $row->t34=$tot[34]; if($tot[35]>0) $row->t35=$tot[35]; if($tot[36]>0) $row->t36=$tot[36];
				if($tot[37]>0) $row->t37=$tot[37]; if($tot[38]>0) $row->t38=$tot[38]; if($tot[39]>0) $row->t39=$tot[39];
				if($tot[40]>0) $row->t40=$tot[40]; if($tot[41]>0) $row->t41=$tot[41]; if($tot[42]>0) $row->t42=$tot[42];
				if($tot[43]>0) $row->t43=$tot[43]; if($tot[44]>0) $row->t44=$tot[44]; if($tot[45]>0) $row->t45=$tot[45];
				if($tot[46]>0) $row->t46=$tot[46]; if($tot[47]>0) $row->t47=$tot[47]; if($tot[48]>0) $row->t48=$tot[48];
				if($tot[49]>0) $row->t49=$tot[49]; if($tot[50]>0) $row->t50=$tot[50]; if($tot[51]>0) $row->t51=$tot[51];
				if($tot[52]>0) $row->t52=$tot[52]; if($tot[53]>0) $row->t53=$tot[53]; if($tot[54]>0) $row->t54=$tot[54];
				if($tot[55]>0) $row->t55=$tot[55]; if($tot[56]>0) $row->t56=$tot[56]; if($tot[57]>0) $row->t57=$tot[57];
				if($tot[58]>0) $row->t58=$tot[58]; if($tot[59]>0) $row->t59=$tot[59]; if($tot[60]>0) $row->t60=$tot[60];
				if($tot[61]>0) $row->t61=$tot[61]; if($tot[62]>0) $row->t62=$tot[62]; if($tot[63]>0) $row->t63=$tot[63];
				if($tot[64]>0) $row->t64=$tot[64]; if($tot[65]>0) $row->t65=$tot[56]; if($tot[66]>0) $row->t66=$tot[66];
				if($tot[67]>0) $row->t67=$tot[67];
				$data[] = $row;	
			 endforeach;
			}
			return $data;
		}
		public function quitar($id,$tab,$cam,$est,$fol,$tal,$cic){
		//public function quitar($id,$tab,$cam,$ident,$kgs){
			//se tiene que actualizar la existencia en las entradas
			//buscar a quien se le va a sumar esto que se quite.
			/*$kgs = str_replace(",", "", $kgs);
			$this->db->select('ident,exient');
			$this->db->where($this->ident,$ident);	
			$result = $this->db->get($this->tablae);
			if($result->num_rows()>0){
				$exis=0;
				foreach ($result->result() as $row):
				  $exi=$row->exient+$kgs;
				  $ide=$row->ident;
				endforeach;
				$data=array($this->exient=>$exi);	
				$this->db->where($this->ident,$ide);
				$this->db->update($this->tablae,$data);
			}*/
			
			// veo si la salida es un traslado
			if($est==2){
				//elimino en maqpro_ 
				//SELECT idep from maqpro_19 where estp=2 and idpe=1 and idtp=7
				$fol=intval($fol); 
				$this->db->select('idep');
				$this->db->where($this->estp,$est);$this->db->where($this->idpe,$fol);$this->db->where($this->idtp,$tal);
				$results = $this->db->get($this->tablamp.$cic);
				foreach($results->result() as $row):
					$idsp=$row->idep;
				endforeach;
				$this->db->where($this->idep,$idsp);
				$this->db->delete($this->tablamp.$cic);
				// elimino la entrada
				$this->db->where($this->idsalt,$id);
				$this->db->delete($this->tablae);
			}
			//ejecuto el borrado de la salida
			$this->db->where($cam,$id);
			$this->db->delete($tab);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function quitartal($id,$tab,$cam){
			$this->db->where($cam,$id);
			$this->db->delete($tab);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getGranjas(){
			$this->db->select('idg,nomg');
			$result = $this->db->get($this->tablag);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getAlmacenes(){
			$this->db->select('ida,noma');
			$result = $this->db->get($this->tablaa);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getClientes(){
			$this->db->select('Numero,Razon');
			$result = $this->db->get($this->tablac);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getClientescot(){
			$this->db->select('idclicb,nomcb');
			$result = $this->db->get($this->tablacotcli);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getElementscb($cic){        
        	$this->db->select("idt,nomt as val");
			$this->db->join('tallas', 'idt=idtp','inner');
			$this->db->group_by($this->idt);
			$this->db->group_by($this->nomt);
			$result = $this->db->get($this->tablamp.$cic);
			$datac = array();        
        	foreach($result->result() as $rowc):
            	$datac[] = $rowc;
        	endforeach;        
        	return $datac;
    	}
		
		function precioscot($tal){        
        	$this->db->select("premen,premay");
			$this->db->from($this->tablat);
			$this->db->where('idt',$tal);
			$query=$this->db->get();
			return $query->row();
    	}
		function getElementsb($where){        
        	       
        	if($where['est']==1){
				$this->db->select("Numero,Razon as val");$this->db->order_by('Razon');
	            $result = $this->db->get($this->tablac);
			}else {
				$this->db->select("ida as Numero,noma as val");$this->db->order_by('noma');
	            $result = $this->db->get($this->tablaa);
			}
			
        	$datac = array();        
        	foreach($result->result() as $rowc):
            	$datac[] = $rowc;
        	endforeach;        
        	return $datac;
    	}
		function contacto($cli,$est){
			if($est==1){
				$this->db->select('con,Razon');
				$this->db->from($this->tablac);
				$this->db->where('Numero =', $cli);
				$query=$this->db->get();
			}else {
				$this->db->select('cona as con,noma as Razon,avisoc as avisoc');
				$this->db->from($this->tablaa);
				$this->db->where('ida =', $cli);
				$query=$this->db->get();
				
			}
			
			
			return $query->row();
		}
		
		function getTallas(){
			$this->db->select('idt,nomt');
			$this->db->order_by('gpoid');$this->db->order_by('nomt');
			$result = $this->db->get($this->tablat);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getTallas1($cic){
			//select idtp,nomt from maqpro_19 inner join tallas on idt=idtp group by idt
			$this->db->select('idt,nomt');
			$this->db->join('tallas', 'idt=idtp','inner');
			//$this->db->where('nomt !=','');
			$this->db->group_by($this->idt);
			$this->db->group_by($this->nomt);
			$result = $this->db->get($this->tablamp.$cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		
		function getinventarios($filter,$cic){
			//SELECT fece,noma,nomg,idpe from almacenes inner join( granjas inner join inventarios on idge=idg )on idae=ida where fece='2018-06-09'
			$this->db->select('noma,nomg,nomt,sum(kgsent) as kgsent,exient,cicent,cona,siga,sigg,alment,graent,talent,ident,gpoid');
			$this->db->join('almacenes', 'ida=alment','inner');
			$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where('kgsent >',0);
			$this->db->group_by('alment');
			$this->db->group_by('graent');
			$this->db->group_by('talent');
			$this->db->order_by('ida');
			$this->db->order_by('nomg');
			//$this->db->order_by('idt');		
			$this->db->order_by('gpoid');$this->db->order_by('grupo');			
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 $data1 = array();
				 $idtalla=$row->talent;$idgranja=$row->graent;$idalmacen=$row->alment;
				 $idSalida=$row->ident;$e=0;$s=0;$exi=0;
				 $row->pre=1;
				if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				if($alm!=$row->noma){
					if($tot>0){
						$this->db->select('max(ident)');
			 			$resulta = $this->db->get($this->tablae);
			 			foreach ($resulta->result() as $rowt):	
							$rowt->pre=1;			
							$rowt->cic='';$rowt->noma1 = ""; $rowt->nomg='';$rowt->nomt='Total:';
							$rowt->kgsent=number_format(($tote), 2, '.', ',');
							$rowt->kgssal=number_format(($tots), 2, '.', ',');
							$rowt->exient=number_format(($tote-$tots), 2, '.', ',');
							$data[] = $rowt;	
			 			endforeach;
					   $tot=0;$tote=0;$tots=0;$gra='';
					}		
					$alm=$row->noma;$row->noma1=$row->noma;
				
				}else{ $row->noma1="";}
				if($gra!=$row->nomg){$gra=$row->nomg;}else{ $row->nomg="";}
				$e=$row->kgsent;
				
				//$row->kgsent=number_format(($row->kgsent), 2, '.', ',');
				// debo sumar todas las entradas....
				//SELECT idgp,idtp,nomt,sum(kgsp) as totkg from tallas inner join maqpro_19 on idtp=idt where idtp=2 and idgp=4 and idap=1
				$this->db->select('sum(kgsp) as totkg');	
				if($filter['where']!=''){
				$this->db->where('idtp',$idtalla);
				$this->db->where('idgp',$idgranja);
				$this->db->where('idap',$idalmacen);
				}
				$this->db->join($this->tablamp.$cic, 'idtp=idt','inner');
				//if($filter['where']!='') $this->db->where($filter['where']);
				$resultes = $this->db->get('tallas'); 
				foreach ($resultes->result() as $rowes):
					$e=$rowes->totkg;$tote+=$rowes->totkg;
					$row->kgsent=number_format(($rowes->totkg), 2, '.', ',');
					
				endforeach;
				
				//suma todas las salidas de esta entrada
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->group_by($idSalida);
				$result = $this->db->get($this->tablas);
				foreach ($result->result() as $rows):	
					$s=$rows->kgssal;
					$tots+=$rows->kgssal;			
					if($rows->kgssal > 0) $row->kgssal=number_format($rows->kgssal, 2, '.', ','); else $row->kgssal='';					
				endforeach;
				$row->exient=number_format(($e-$s), 2, '.', ',');$exi=$e-$s;
				//aCTUALIZAR LA EXISTENCIA
				$data1=array($this->exient=>$exi);	
				$this->db->where($this->ident,$idSalida);
				$this->db->update($this->tablae,$data1);				
				$data[] = $row;
				//aqui ingresariamos la dstribucion por presentación de cajas 22 o 10 kg entradas
				//SELECT idgp,idtp,nomt,sum(kgsp) as totkg,sum(masp), kgmp from tallas inner join maqpro_19 on idtp=idt group by idgp,idtp,kgmp
				$this->db->select('idgp,idtp,sum(kgsp) as totkg,sum(masp), kgmp');	
				$this->db->where($this->idgp,$idgranja);
				$this->db->where($this->idtp,$idtalla);
				$this->db->where($this->idap,$idalmacen);
				//$this->db->where($this->estp,1);
				//$this->db->group_by('idgp');
				$this->db->group_by('kgmp');
				$resultdeta = $this->db->get($this->tablamp.$cic);
				foreach ($resultdeta->result() as $rowdt):	
					$pent=0;
					$rowdt->cic='';$rowdt->noma1='';$rowdt->nomg='';
					$rowdt->nomt=$rowdt->kgmp;
					$pent=$rowdt->totkg;
					$rowdt->kgsent=number_format(($rowdt->totkg), 2, '.', ',');
					//aqui buscare todas las salidas de acuerdo a la presentacion
					$this->db->select('sum(kgssal) as totkg');	
					$this->db->where($this->grasal,$idgranja);
					$this->db->where($this->talsal,$idtalla);
					$this->db->where($this->almsal,$idalmacen);
					$this->db->where($this->presal,$rowdt->kgmp);
					$this->db->where('ciclo','20'.$cic);
					//$this->db->group_by('idgp');
					//$this->db->group_by('presal');
					$resultdetas = $this->db->get($this->tablas);
					$psal=0;
					foreach ($resultdetas->result() as $rowds):	
						if($rowds->totkg>0){$rowdt->kgssal=number_format(($rowds->totkg), 2, '.', ',');$psal=$rowds->totkg;} else {$rowdt->kgssal='';}
					endforeach;
						$rowdt->exient='['.($pent-$psal)/$rowdt->kgmp.'c] '.number_format(($pent-$psal), 0, '.', ',').'kg';
					//if($pent-$psal>0) $rowdt->exient='['.($pent-$psal)/$rowdt->kgmp.'c] '.number_format(($pent-$psal), 0, '.', ',').'kg'; else $rowdt->exient='';
					
					$data[] = $rowdt;
				endforeach;
				$tot=1;
			 endforeach;
			 
			}
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->pre=1; 			
				$rowt->cic='';$rowt->noma1 = ""; $rowt->nomg='';$rowt->nomt='Total:';
				$rowt->kgsent=number_format(($tote), 2, '.', ',');
				if($tots>0) $rowt->kgssal=number_format(($tots), 2, '.', ','); else $rowt->kgssal='';
				$rowt->exient=number_format(($tote-$tots), 2, '.', ',');
				$data[] = $rowt;	
			 endforeach;
			return $data;
		}
		function agregarmaqf($est,$grg,$grp,$ntp,$kgt,$idf){
			$data=array($this->estf=>$est,$this->grgf=>$grg,$this->grpf=>$grp,$this->ntpf=>$ntp,$this->kgtf=>$kgt,$this->idfe=>$idf);			
			$this->db->insert($this->tablamf,$data);
			return $this->db->insert_id();
		}
		function actualizarmaqf($id,$est,$grg,$grp,$ntp,$kgt,$idf){
	 		$data=array($this->estf=>$est,$this->grgf=>$grg,$this->grpf=>$grp,$this->ntpf=>$ntp,$this->kgtf=>$kgt,$this->idfe=>$idf);
			$this->db->where($this->idef,$id);
			$this->db->update($this->tablamf,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getinventariosdiaf($filter){
			if($filter['where']!='') $this->db->where($filter['where']);			
			//$this->db->order_by($this->idef,'DESC');
			$result = $this->db->get($this->tablamf);
			$data = array();
			if($result->num_rows()>0){
				$totf=0;$est='';$gr1='';$gr2='';
				foreach($result->result() as $row):
					//$est=$row->estf;
					if($est!=$row->estf){
						$est=$row->estf; 
						$row->estf1=$row->estf;
					}else{ $row->estf1="";}
					if($gr1!=$row->grgf){
						$gr1=$row->grgf; 
						$row->grgf1=number_format(($row->grgf), 2, '.', ',');	
						
					}else{ $row->grgf1='';}
					if($gr2!=$row->grpf){
						$gr2=$row->grpf; 
						$row->grpf1=number_format(($row->grpf), 2, '.', ',');	
						
					}else{ $row->grpf1='';}
					$totf+=$row->ntpf*$row->kgtf; 
					$row->subf=number_format(($row->ntpf*$row->kgtf), 2, '.', ',');	
					$row->totf=number_format(($totf), 2, '.', ',');	
					$data[] = $row;
				endforeach;
				$this->db->select('max(ide)');	
				$result = $this->db->get($this->tablam);
				foreach ($result->result() as $row):				
					$row->estf1 = "Total:"; $row->grgf1="";$row->grpf1="";$row->ntpf="";$row->kgtf="";
					if($totf>0) $row->subf=number_format($totf, 2, '.', ','); else $row->subf='';
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}
		function agregarmaqp($lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec){
			$kgsp=$mas*$kgm;
			//busco la talla sino existo la registro
			$this->db->select('ident,kgsent,exient');
			$this->db->where($this->talent,$idt);	
			$result = $this->db->get($this->tablae);
			if($result->num_rows()==0){
				//ingresa
				$data=array($this->talent=>$idt,$this->alment=>$ida,$this->graent=>$idg,$this->kgsent=>$kgsp,$this->exient=>$kgsp);	
				$this->db->insert($this->tablae,$data);
			}else{
				//extraigo los datos a actualizar
				$kgs=0;$exis=0;
				foreach ($result->result() as $row):
				  $kgs=$row->kgsent+$kgsp;
				  $exi=$row->exient+$kgsp;
				  $id=$row->ident;
				endforeach;
				//realizo actualizacion
				$data=array($this->kgsent=>$kgs,$this->exient=>$exi);	
				$this->db->where($this->ident,$id);
				$this->db->update($this->tablae,$data);
			}		
			
			$data=array($this->lotp=>$lot,$this->idtp=>$idt,$this->masp=>$mas,$this->kgmp=>$kgm,$this->idgp=>$idg,$this->idap=>$ida,$this->idpe=>$idp,$this->kgsp=>$kgsp,$this->prep=>$pre,$this->fecp=>$fec);			
			$this->db->insert($this->tablamp,$data);
			return $this->db->insert_id();
		}
		function actualizarmaqp($id,$lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec){
			$kgsp=$mas*$kgm;
	 		$data=array($this->lotp=>$lot,$this->idtp=>$idt,$this->masp=>$mas,$this->kgmp=>$kgm,$this->idgp=>$idg,$this->idap=>$ida,$this->idpe=>$idp,$this->kgsp=>$kgsp,$this->prep=>$pre,$this->fecp=>$fec);
			$this->db->where($this->idep,$id);
			$this->db->update($this->tablamp,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getinventariosdiap($filter,$id){
			//obtengo el total de los kgs en fresco
			$this->db->select('sum(ntpf*kgtf) as totkf');
			$this->db->where($this->idfe,$id);
			$resulta = $this->db->get($this->tablamf);
			if($resulta->num_rows()>0){
				foreach($resulta->result() as $row):
					$totf=$row->totkf;
				endforeach;
			}	
			$this->db->select('idep,lotp,idtp,nomt,masp,kgsp,idpe,kgmp,prep');
			$this->db->join('tallas', 'idt=idtp','inner');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->order_by('nomt');		
			$result = $this->db->get($this->tablamp);
			$data = array();
			if($result->num_rows()>0){
				$totp=0;$totd=0;$lot='';
				foreach($result->result() as $row):
					if($lot!=$row->lotp){
						$lot=$row->lotp; 
						$row->lotp1=$row->lotp;
					}else{ $row->lotp1="";}
					$totp+=$row->kgmp*$row->masp;
					$totd+=($row->kgsp*$row->prep)*1.16;
					$row->totp='$'.number_format(($row->kgsp*$row->prep)*1.16, 2, '.', ',');
					$row->kgsp=number_format(($row->kgsp), 2, '.', ',');	
					$row->prep=number_format(($row->prep), 2, '.', '');	
					$data[] = $row;
				endforeach;
				$this->db->select('max(ide)');	
				$result = $this->db->get($this->tablam);
				foreach ($result->result() as $row):				
					$row->lotp1 = "Total:"; $row->nomt="";$row->masp="";$row->kgmp="";$row->prep="";
					if($totp>0) $row->kgsp=number_format($totp, 2, '.', ','); else $row->kgsp='';
					if($totd>0) $row->totp='$'.number_format($totd, 2, '.', ','); else $row->totp='';
					$row->ren=number_format(($totp/$totf)*100, 2, '.','');
					//$row->DolE1='';$row->TcE1='';
					$data[] = $row;	
				endforeach;
			}
			return $data;
		}
		function actualizard($id,$can,$uni,$des,$obs,$cans){
	 		$hoy=date("y-m-d H:i:s");
			$data=array($this->can=>$cer,$this->fr=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->cans=>$cans,$this->obs=>$obs);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrard($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function ultimofolio($cic){									
			$this->db->select('MAX(folsal) as folio');
			$this->db->where('ciclo','20'.$cic);
			$this->db->from($this->tablas);
			$query=$this->db->get();
			return $query->row();
		}
		function ultimofoliocot(){									
			$this->db->select('MAX(foltalcb) as folio');
			$this->db->from($this->tablacottal);
			$query=$this->db->get();
			return $query->row();
		}
		
		function borrarr($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		/*
		function ultimofolio(){									
			$this->db->select('MAX(folsal) as ultimo');
			$query = $this->db->get($this->tablas);
			return $query->result();
		}
		  */
		
    }
    
?>