<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Remisiones_model extends CI_Model {
        //private $nombre;
        public $id="NumRegR";public $fec="FechaR";public $rem="RemisionR";public $canr="CantidadR";public $canrr="CantidadRR";
		public $prer="PrecioR";public $dcor="DescuentoR";public $ncr="NumCliR";public $aviso="AvisoR";public $obsr="ObsR";
		public $tipo="Tipo";public $estatus="Estatus";public $recibida="Recibida";public $origen="Origen";public $enc="NumBioR";
		public $cho="NumChoR";public $uni="NumUniR";public $ela="NumElaR";public $cvr="CV";public $guia="Guia";
		public $tabla="r18";
        
		
		public $nmue="NMue";public $m1="M1";public $m2="M2";public $m3="M3";public $nfol="NFol";public $pilcos="Pilcos";public $plcos="Plcos";public $pescos="Pescos";
		public $tablacv="muestras";
		
		public $nesc="nesc";public $fecesc="fecesc";public $uniesc="uniesc";public $choesc="choesc";public $km="km";
		public $cv="cv";public $cp="cp";public $rv="rv";public $rl="rl";public $renesc="renesc";public $reme1="reme1";public $reme2="reme2";
		public $tablaesc="escaner";
				
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function verPila(){
			//$this->db->where('Cancelacion =',0);
			
			$query=$this->db->get('raceways');
			
			return $query->result();			
		}
		
		function getElementsZC($where){
			$this->db->select("Numero, Razon as val"); 
			//$this->db->join('unidad', 'NumUni=NumUniR', 'inner');
			$this->db->join('clientes', 'Numero=NumCliR', 'inner');
			$this->db->where($where);
			//if($where['Zona']!=0)	$this->db->where($where);
			$this->db->group_by('Numero');$this->db->group_by('Razon'); 
			$this->db->order_by('Razon');
			$result=$this->db->get($this->tabla);//SE CONSULTA.
			$data = array();//SE CREA UN ARREGLO VACIO 
			foreach($result->result() as $row):
				$data[] = $row;//SE LLENA EL ARREGLO CON EL RESULTADO DE LA CONSULTA
			endforeach; 
			return $data;//REGRESAMOS EL ARREGLO
		} 
		
		function getElementsZU($where){
			$this->db->select("NumUni, NomUni as val"); 
			$this->db->join('unidad', 'NumUni=NumUniR', 'inner');
			$this->db->join('clientes', 'Numero=NumCliR', 'inner');
			$this->db->where($where);
			//if($where['Zona']!=0)	$this->db->where($where);
			$this->db->group_by('NomUni');$this->db->group_by('NumUni'); 
			$result=$this->db->get($this->tabla);//SE CONSULTA.
			$data = array();//SE CREA UN ARREGLO VACIO 
			foreach($result->result() as $row):
				$data[] = $row;//SE LLENA EL ARREGLO CON EL RESULTADO DE LA CONSULTA
			endforeach; 
			return $data;//REGRESAMOS EL ARREGLO
		} 
		
		function verCli(){
			$this->db->select('Numero,Razon,prevta,avisoc,con');
			$this->db->join('clientes', 'Numero=NumCliR', 'inner');
			$this->db->group_by('Razon');$this->db->group_by('Numero');
			$this->db->order_by('Razon');
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}		
		function verZon(){
			$this->db->select('Zona');
			$this->db->join('clientes', 'Numero=NumCliR', 'inner');
			$this->db->group_by('Zona');
			$this->db->order_by('Zona');
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}	
		public function verRemision(){
			$this->db->where('RemisionR >',0);			
			$this->db->order_by('NumRegR','DESC');
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}		
		public function agregar($id,$fec,$rem,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$fol,$ur,$ori,$cic,$folio,$enc,$cho,$uni,$ela,$gui){
			$can1=$can;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			if($tipo == '1' || $tipo == '2'){$can1 =$can1/1000;}			
			$dco =$dco/100;			
			$cantdes1=($can1-($can1*$dco));
			//Obtengo El Saldo Actual				
			$this->db->select('Saldo');
			$this->db->where('Numero',$cli);
			$result = $this->db->get('clientes');
			foreach($result->result() as $row):
				$saldoact=$row->Saldo;	
			endforeach;	
			//actualizo saldo de cliente
			$saldoactual=0;
			$saldoactual=$saldoact+($cantdes1*$pre);										
			$datasc=array('Saldo'=>$saldoactual);
			$this->db->where('Numero',$cli);
			$this->db->update('clientes',$datasc);
			//folio ingresar
			//$sqlConsulta ="insert into folios (NFolio,Folio,RemFol,NRem) values ('$nuevos','$folio','$nr','$cant')";		
			if($fol == ""){ $fol="SN";}
			$data=array('Folio'=>$fol,'RemFol'=>$rem,'NRem'=>$ur);			
			$this->db->insert($folio,$data);	
			
			/*$can =$can/1000;
			$pre =$pre/100;
			$dco =$dco/100;*/
			$can = str_replace(",", "", $can);$can = str_replace(".", "", $can);$can =$can/1000;					
			$dco =$dco/100;			
			$cantdes=($can-($can*$dco));
			//$cantdes=($can-($can*$dco));
			$data=array($this->id=>$ur,$this->fec=>$fec,$this->rem=>$rem,$this->ncr=>$cli,$this->canr=>$can,$this->prer=>$pre,$this->aviso=>$avi,$this->obsr=>$obs,$this->tipo=>$tipo,$this->estatus=>$est,$this->canrr=>$cantdes,$this->dcor=>$dco,$this->origen=>$ori,$this->enc=>$enc,$this->cho=>$cho,$this->uni=>$uni,$this->ela=>$ela,$this->guia=>$gui);			
			$this->db->insert($cic,$data);
			
			return $this->db->insert_id();
		}
		public function actualizar($id,$fec,$rem,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$fol,$ur,$rec,$ori,$cic,$folio,$enc,$cho,$uni,$ela,$gui){
							
			//Obtengo El Saldo Actual				
			$this->db->select('Saldo');
			$this->db->where('Numero',$cli);
			$result = $this->db->get('clientes');
			foreach($result->result() as $row):
				$saldoact=$row->Saldo;	
			endforeach;			
			//obtengo la cantidaremisionada y el precio de la remision que se va a modificar
			$this->db->select('(CantidadRR*PrecioR) as quitar');
			$this->db->where('NumRegR',$id);
			$result = $this->db->get($cic);
			foreach($result->result() as $row):
				$quitale=$row->quitar;	
			endforeach;			
			if($tipo=='3'){ $quitale=$quitale*1000;}
			//al siguiente resultado le voy a sumar lo nuevo
			$saldoprevio=0;
			$saldoprevio=($saldoact-$quitale);
			
			$can1=$can;
			$can1 = str_replace(",", "", $can1);$can1 = str_replace(".", "", $can1);
			if($tipo == '1' || $tipo == '2'){$can1 =$can1/1000;}			
			$dco =$dco/100;			
			$cantdes1=($can1-($can1*$dco));
			$saldoactual=0;
			$saldoactual=$saldoprevio+($cantdes1*$pre);
			//actualizo saldo de cliente								
			$datasc=array('Saldo'=>$saldoactual);
			$this->db->where('Numero',$cli);
			$this->db->update('clientes',$datasc);
			
			
			//$sqlConsulta="update folios set Folio='$folio' where NRem='".$claveR."'";
			if($fol == ""){ $fol="SN";}
			$data=array('Folio'=>$fol);
			$this->db->where('NRem',$id);
			$this->db->update($folio,$data);
							
			/*
			if($rec == "√"){ $rec=-1;	}
			else{
				 $rec=0;	
			}*/
			
			$can = str_replace(",", "", $can);$can = str_replace(".", "", $can);$can =$can/1000;					
			$dco =$dco/100;			
			$cantdes=($can-($can*$dco));
			
			$data=array($this->fec=>$fec,$this->rem=>$rem,$this->ncr=>$cli,$this->canr=>$can,$this->prer=>$pre,$this->aviso=>$avi,$this->obsr=>$obs,$this->tipo=>$tipo,$this->estatus=>$est,$this->canrr=>$cantdes,$this->recibida=>$rec,$this->dcor=>$dco,$this->origen=>$ori,$this->enc=>$enc,$this->cho=>$cho,$this->uni=>$uni,$this->ela=>$ela,$this->guia=>$gui);
			$this->db->where($this->id,$id);
			$this->db->update($cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}

		public function actualizacv($id,$m1,$m2,$m3,$pil,$pl,$pes){
			$data=array($this->m1=>$m1,$this->m2=>$m2,$this->m3=>$m3,$this->pilcos=>$pil,$this->plcos=>$pl,$this->pescos=>$pes);
			$this->db->where($this->nmue,$id);
			$this->db->update($this->tablacv,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		public function agregacv($m1,$m2,$m3,$fol,$pil,$pl,$pes){
			$data=array($this->m1=>$m1,$this->m2=>$m2,$this->m3=>$m3,$this->nfol=>$fol,$this->pilcos=>$pil,$this->plcos=>$pl,$this->pescos=>$pes);			
			$this->db->insert($this->tablacv,$data);
			
			return $this->db->insert_id();
		}
		
		public function actualizaresc($id,$fec,$uni,$cho,$km,$cv,$cp,$rv,$rl,$ren,$reme1,$reme2){
			$data=array($this->fecesc=>$fec,$this->uniesc=>$uni,$this->choesc=>$cho,$this->km=>$km,$this->cv=>$cv,$this->cp=>$cp,$this->rv=>$rv,$this->rl=>$rl,$this->renesc=>$ren,$this->reme1=>$reme1,$this->reme2=>$reme2);
			$this->db->where($this->nesc,$id);
			$this->db->update($this->tablaesc,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		public function agregaresc($fec,$uni,$cho,$km,$cv,$cp,$rv,$rl,$ren,$reme1,$reme2){
			$data=array($this->fecesc=>$fec,$this->uniesc=>$uni,$this->choesc=>$cho,$this->km=>$km,$this->cv=>$cv,$this->cp=>$cp,$this->rv=>$rv,$this->rl=>$rl,$this->renesc=>$ren,$this->reme1=>$reme1,$this->reme2=>$reme2);			
			$this->db->insert($this->tablaesc,$data);
			
			return $this->db->insert_id();
		}
		
		function quitarDet($id){
			$this->db->where($this->nmue,$id);
			$this->db->delete($this->tablacv);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function getcv($filter){
			/*$query=$this->db->get($tabla);*/
			$this->db->select('NMue,M1,M2,M3,NFol,Pilcos,Plcos,Pescos');
			$this->db->order_by('NMue', 'DESC');
			if($filter['num']!=0)	$this->db->where($this->nfol,$filter['num']);
			$result = $this->db->get($this->tablacv);
			$data = array();$m1=0;$m2=0;$m3=0;$cont=0;$promcv=0;$contcv=0;$toti=0;$cvr=0;
			foreach ($result->result() as $row):				
					$m1=$row->M1;$m2=$row->M2;$m3=$row->M3;
					if($m1>0) $cont+=1;if($m2>0) $cont+=1;if($m3>0) $cont+=1;
					$array = array(2);
					$array= array ($m1,$m2,$m3);
					$desv=sd($array,$cont);
					if($cont>0) $row->prom = number_format((($m1+$m2+$m3)/$cont), 1, '.', ','); else $row->prom=0;
					$row->cv1=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$promcv+=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$row->canpls=number_format(((($m1+$m2+$m3)/$cont)*500/.270), 0,'.', ',');$toti+=((($m1+$m2+$m3)/$cont)*500/.270);
					if($row->Pescos>0) $row->mili=number_format(($row->Pescos/(((($m1+$m2+$m3)/$cont)*500/.270)))*1000, 3,'.', ','); else $row->mili='';
					$contcv+=1;
					$row->cv=number_format((((($promcv)/$contcv))), 2, '.', ',');
					$cvr=number_format((((($promcv)/$contcv))), 2, '.', ',');
					if($row->Pilcos=='') $row->Pilcos='';
					if($row->Pescos=='') $row->Pescos='';
					if($row->Plcos=='') $row->Plcos='';
					$data[] = $row;
					//actualizar CV en remision
					$data1=array($this->cvr=>$cvr);
					$this->db->where($this->id,$filter['num']);
					$this->db->update('r18',$data1);	
					$cont=0;		
			endforeach;	
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row1):
				$row1->Pilcos = "Total";$row1->M1 = "";$row1->M2 = "";$row1->M3 = "";$row1->prom = "";$row1->cv1 = $cvr;$row1->cv = $cvr;
				$row1->canpls = number_format(($toti), 0, '.', ',');
				$data[] = $row1;
			endforeach;
			return $data;
		}
		public function getEscaner($filter){
			/*$query=$this->db->get($tabla);*/
			//$this->db->select('NMue,M1,M2,M3,NFol');
			//$this->db->order_by('NMue', 'DESC');
			$this->db->join('chofer', 'NumCho=choesc', 'inner');
			$this->db->join('unidad', 'NumUni=uniesc', 'inner');
			//$this->db->join('r18', 'reme1=NumRegR or reme2=NumRegR','inner');
			//$this->db->join('r18', 'reme2=NumRegR', 'inner');
			//$this->db->group_by('reme1');$this->db->group_by('reme2');
			$this->db->order_by('fecesc', 'DESC');$this->db->order_by('NomUni', 'ASC');$this->db->order_by('nesc', 'ASC');
			if($filter['num']!=0)	$this->db->where($this->uniesc,$filter['num']);
			if($filter['num']!=0)	$this->db->where($this->uniesc,$filter['num']);
			$result = $this->db->get($this->tablaesc);
			$data = array();$m1=0;$m2=0;$m3=0;$cont=0;$promcv=0;$contcv=0;$fecha1='';
			 $fec=new Libreria();
			foreach ($result->result() as $row):
					if($fecha1!=$row->fecesc){
						$fecha1=$row->fecesc; 
						$row->fecescd = $fec->fecha($row->fecesc);
					} else{ 
		  				$row->fecescd="";
					}
					
					$row->unidad=$row->NomUni;$row->chofer=$row->NomCho;
					if($row->cp==0) $row->cp='';		
					if($row->renesc==0) $row->renesc='';
					if($row->rv==0) $row->rv='';
					if($row->rl==0) $row->rl='';
					if($row->reme1==0) $row->reme1=''; //else $row->re1='*';  
					if($row->reme2==0) $row->reme2=''; //else $row->re2='*';
					//$row->re1=$row->RemisionR;  $row->re2=$row->RemisionR;
					/*$m1=$row->M1;$m2=$row->M2;$m3=$row->M3;
					if($m1>0) $cont+=1;if($m2>0) $cont+=1;if($m3>0) $cont+=1;
					$array = array(2);
					$array= array ($m1,$m2,$m3);
					$desv=sd($array,$cont);
					if($cont>0) $row->prom = number_format((($m1+$m2+$m3)/$cont), 1, '.', ','); else $row->prom=0;
					$row->cv1=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$promcv+=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$contcv+=1;
					$row->cv=number_format((((($promcv)/$contcv))), 2, '.', ',');
					$cvr=number_format((((($promcv)/$contcv))), 2, '.', ',');*/
					$data[] = $row;
						
			endforeach;	
			
			return $data;
		}
		public function getEscanerr($cli,$mes){
			
			$query=$this->db->query("SELECT NumRegR,day(FechaR) as FechaR,RemisionR,Razon,NomBio,NomCho,NomUni,
									(select km from escaner where reme1=RemisionR) as kms,
									(select km from escaner where reme2=RemisionR) as kml,
									(select cv from escaner where reme1=RemisionR) as cvs,
									(select cv from escaner where reme2=RemisionR) as cvl,
									(select cp from escaner where reme1=RemisionR) as cps,
									(select cp from escaner where reme2=RemisionR) as cpl,
									(select renesc from escaner where reme2=RemisionR) as rene, 
									(select rv from escaner where reme2=RemisionR) as rv,
									(select rl from escaner where reme2=RemisionR) as rl
									from biologo inner join(chofer inner join(unidad inner join(clientes inner join r18 on NumCliR=Numero) on NumUniR=NumUni) on NumChoR=NumCho) on NumBioR=NumBio where NumUniR=$cli and month(FechaR)=$mes order by RemisionR");
			$data = array();$m1=0;$m2=0;$m3=0;$cont=0;$promcv=0;$contcv=0;$fecha1='';
			 $fec=new Libreria();
			foreach ($query->result() as $row):
					if($fecha1!=$row->FechaR){
						$fecha1=$row->FechaR; 
						//$row->fechaer = $fec->fecha($row->FechaR);
						$row->fechaer =$row->FechaR;
					} else{ 
		  				$row->fechaer="";
					}
					
					if($row->kml-$row->kms>0) $row->kmr=number_format($row->kml-$row->kms, 2, '.', ','); else $row->kmr='';
					if($row->cvl-$row->cvs>0) $row->cvr=number_format($row->cvl-$row->cvs, 2, '.', ','); else $row->cvr='';
					if($row->cpl-$row->cps>0) $row->cpr=number_format($row->cpl-$row->cps, 2, '.', ','); else $row->cpr='';
					if($row->cvr+$row->cpr>0) $row->cvn=number_format(($row->cvl-$row->cvs)+($row->cpl-$row->cps), 2, '.', ','); else $row->cvn='';
					if($row->rv+$row->rl>0) $row->lt=number_format($row->rv+$row->rl, 2, '.', ','); else $row->lt='';
					if($row->kml-$row->kms>0) $row->renv=number_format((($row->kml-$row->kms)/(($row->cvl-$row->cvs)+($row->cpl-$row->cps))), 2, '.', ','); else $row->renv='';
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0) $row->des=number_format( ( ((($row->rv+$row->rl)-(($row->cvl-$row->cvs)+($row->cpl-$row->cps)))/((($row->cvl-$row->cvs)+($row->cpl-$row->cps))))*100 ),2, '.', ','); else $row->des='';
					if($row->kms==0)$row->kms='';
					if($row->kml==0)$row->kml='';
					if($row->cvs==0)$row->cvs='';
					if($row->cvl==0)$row->cvl='';
					if($row->cps==0)$row->cps='';
					if($row->cpl==0)$row->cpl='';
					if($row->rene==0)$row->rene='';
					if($row->rv==0)$row->rv='';
					if($row->rl==0)$row->rl='';
					//$rem=$row->NumRegR;
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0){
					$datas=array('descom'=>$row->des); 
					$this->db->where($this->id,$row->NumRegR);
					$this->db->update($this->tabla,$datas); 
					}
					$data[] = $row;
			endforeach;	
			
			return $data;
		}
		public function getEscanerrc($cli){
			
			$query=$this->db->query("SELECT NumRegR,FechaR,RemisionR,NomUni,NomBio,NomCho,NomUni,
									(select km from escaner where reme1=RemisionR) as kms,
									(select km from escaner where reme2=RemisionR) as kml,
									(select cv from escaner where reme1=RemisionR) as cvs,
									(select cv from escaner where reme2=RemisionR) as cvl,
									(select cp from escaner where reme1=RemisionR) as cps,
									(select cp from escaner where reme2=RemisionR) as cpl,
									(select renesc from escaner where reme2=RemisionR) as rene, 
									(select rv from escaner where reme2=RemisionR) as rv,
									(select rl from escaner where reme2=RemisionR) as rl
									from biologo inner join(chofer inner join(unidad inner join(clientes inner join r18 on NumCliR=Numero) on NumUniR=NumUni) on NumChoR=NumCho) on NumBioR=NumBio where NumCliR=$cli order by RemisionR");
			$data = array();$m1=0;$m2=0;$m3=0;$cont=0;$promcv=0;$contcv=0;$fecha1='';
			 $fec=new Libreria();
			foreach ($query->result() as $row):
					if($fecha1!=$row->FechaR){
						$fecha1=$row->FechaR; 
						$row->fechaer = $fec->fecha($row->FechaR);
						//$row->fechaer =$row->FechaR;
					} else{ 
		  				$row->fechaer="";
					}
					
					if($row->kml-$row->kms>0) $row->kmr=number_format($row->kml-$row->kms, 2, '.', ','); else $row->kmr='';
					if($row->cvl-$row->cvs>0) $row->cvr=number_format($row->cvl-$row->cvs, 2, '.', ','); else $row->cvr='';
					if($row->cpl-$row->cps>0) $row->cpr=number_format($row->cpl-$row->cps, 2, '.', ','); else $row->cpr='';
					if($row->cvr+$row->cpr>0) $row->cvn=number_format(($row->cvl-$row->cvs)+($row->cpl-$row->cps), 2, '.', ','); else $row->cvn='';
					if($row->rv+$row->rl>0) $row->lt=number_format($row->rv+$row->rl, 2, '.', ','); else $row->lt='';
					if($row->kml-$row->kms>0) $row->renv=number_format((($row->kml-$row->kms)/(($row->cvl-$row->cvs)+($row->cpl-$row->cps))), 2, '.', ','); else $row->renv='';
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0) $row->des=number_format( ( ((($row->rv+$row->rl)-(($row->cvl-$row->cvs)+($row->cpl-$row->cps)))/((($row->cvl-$row->cvs)+($row->cpl-$row->cps))))*100 ),2, '.', ','); else $row->des='';
					if($row->kms==0)$row->kms='';
					if($row->kml==0)$row->kml='';
					if($row->cvs==0)$row->cvs='';
					if($row->cvl==0)$row->cvl='';
					if($row->cps==0)$row->cps='';
					if($row->cpl==0)$row->cpl='';
					if($row->rene==0)$row->rene='';
					if($row->rv==0)$row->rv='';
					if($row->rl==0)$row->rl='';
					//$rem=$row->NumRegR;
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0){
					$datas=array('descom'=>$row->des); 
					$this->db->where($this->id,$row->NumRegR);
					$this->db->update($this->tabla,$datas); 
					}
					$data[] = $row;
			endforeach;	
			
			return $data;
		}
		public function getEscanerrz($filter){
			$this->db->select('NumRegR,FechaR,Razon,RemisionR,NomUni,NomBio,NomCho,NomUni,
								(select km from escaner where reme1=RemisionR) as kms,
								(select km from escaner where reme2=RemisionR) as kml,
								(select cv from escaner where reme1=RemisionR) as cvs,
								(select cv from escaner where reme2=RemisionR) as cvl,
								(select cp from escaner where reme1=RemisionR) as cps,
								(select cp from escaner where reme2=RemisionR) as cpl,
								(select renesc from escaner where reme2=RemisionR) as rene, 
								(select rv from escaner where reme2=RemisionR) as rv,
								(select rl from escaner where reme2=RemisionR) as rl');
			$this->db->join('biologo', 'NumBio=NumBioR', 'inner');
			$this->db->join('chofer', 'NumCho=NumChoR', 'inner');
			$this->db->join('unidad', 'NumUni=NumUniR', 'inner');
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			if($filter['where']!='')	{$this->db->where($filter['where']);}			
			$this->db->order_by('razon');	$this->db->order_by('remisionr');
			$result = $this->db->get('r18');
			$data = array();$m1=0;$m2=0;$m3=0;$cont=0;$promcv=0;$contcv=0;$fecha1='';$razon='';
			 $fec=new Libreria();
			 foreach($result->result() as $row):
					if($razon!=$row->Razon){$razon=$row->Razon;} else{$row->Razon="";}
					if($fecha1!=$row->FechaR){
						$fecha1=$row->FechaR; 
						$row->fechaer = $fec->fecha($row->FechaR);
					} else{ 
		  				$row->fechaer="";
					}
					if($row->kml-$row->kms>0) $row->kmr=number_format($row->kml-$row->kms, 2, '.', ','); else $row->kmr='';
					if($row->cvl-$row->cvs>0) $row->cvr=number_format($row->cvl-$row->cvs, 2, '.', ','); else $row->cvr='';
					if($row->cpl-$row->cps>0) $row->cpr=number_format($row->cpl-$row->cps, 2, '.', ','); else $row->cpr='';
					if($row->cvr+$row->cpr>0) $row->cvn=number_format(($row->cvl-$row->cvs)+($row->cpl-$row->cps), 2, '.', ','); else $row->cvn='';
					if($row->rv+$row->rl>0) $row->lt=number_format($row->rv+$row->rl, 2, '.', ','); else $row->lt='';
					if($row->kml-$row->kms>0) $row->renv=number_format((($row->kml-$row->kms)/(($row->cvl-$row->cvs)+($row->cpl-$row->cps))), 2, '.', ','); else $row->renv='';
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0) $row->des=number_format( ( ((($row->rv+$row->rl)-(($row->cvl-$row->cvs)+($row->cpl-$row->cps)))/((($row->cvl-$row->cvs)+($row->cpl-$row->cps))))*100 ),2, '.', ','); else $row->des='';
					if($row->kms==0)$row->kms='';
					if($row->kml==0)$row->kml='';
					if($row->cvs==0)$row->cvs='';
					if($row->cvl==0)$row->cvl='';
					if($row->cps==0)$row->cps='';
					if($row->cpl==0)$row->cpl='';
					if($row->rene==0)$row->rene='';
					if($row->rv==0)$row->rv='';
					if($row->rl==0)$row->rl='';
					//$rem=$row->NumRegR;
					if((($row->cvl-$row->cvs)+($row->cpl-$row->cps))>0){
					$datas=array('descom'=>$row->des); 
					$this->db->where($this->id,$row->NumRegR);
					$this->db->update($this->tabla,$datas); 
					}
					$data[] = $row;
			endforeach;	
			
			return $data;
		}
		public function getCliente($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('NumRegR',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		
		function getRemisiones($filter,$cic,$folio){
			//Select FechaR,RemisionR,CantidadR,Tipo,NumCliR,Razon,AvisoR,Estatus,PrecioR,NumRegR,DescuentoR,Cancelacion,CantidadRR,Recibida FROM r14 
			//INNER JOIN clientes ON r14.NumCliR=clientes.Numero Where FechaR >= '$fi' AND FechaR <= '$ff' order by RemisionR DESC
			
			//$this->db->select($this->zona,$this->nombre,$this->dom,$this->loc,$this->edo,$this->rfc,$this->cp);
			//$sel=array('Zona','Razon','Dom','Loc','Edo','RFC','CP');
			
			//2.-$this->db->select('*');
			//1.-
			$this->db->select('FechaR,RemisionR,CantidadR,Tipo,NumCliR,Razon,AvisoR,Estatus,PrecioR,NumRegR,DescuentoR,Cancelacion,CantidadRR,Recibida,Folio,ObsR,Origen,NumBioR,NomBio,NumChoR,NomCho,NumUniR,NomUni,NumElaR,Guia');
			
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			$this->db->join($folio, 'NumRegR = NRem','inner');			
			$this->db->join('biologo', 'NumBio=NumBioR', 'left');
			$this->db->join('chofer', 'NumCho=NumChoR', 'left');
			$this->db->join('unidad', 'NumUni=NumUniR', 'left');
			//asignar folio
			/*$sqlConsultaF="Select Folio FROM folios where NRem='".$claveR."'";		
			$resultF=consultaSQL($sqlConsultaF,$conexion);
			$registroF=mysql_fetch_array($resultF);
			$folio=$registroF["Folio"];*/	
			
			/*$this->db->select('Dom');
			$this->db->select('Loc');
			$this->db->select('Edo');
			$this->db->select('RFC');
			$this->db->select('CP');*/
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			else {
				$this->db->order_by('remisionr','DESC');	
			}
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			$entro=0;
			if($filter['where']!='')
				{$this->db->where($filter['where']); $entro=1;}
			
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=10){
				if($filter['num']>=0 && $filter['num']<=2){
					$this->db->where($this->estatus,$filter['num']);}
				else{
					if($filter['num']==-1){
					$this->db->where($this->recibida,$filter['num']);}
					if($filter['num']==4){
					$this->db->where($this->recibida,0);}
					//$this->db->where('r14.FechaR >=' , 'txtFI');
					//$this->db->where('r14.FechaR <=' , 'txtFF');
				}
			}
			//Se realiza la consulta con una limitación, en caso de que sea valida
			
			
			If($filter['limit']!=0)
				$result = $this->db->get($cic,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$ela='';
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$cp=$row->CP;
				if($row->AvisoR == '0'){ $row->AvisoR="";	}
				if($row->NumBioR == '0'){ $row->NomBio="";	}
				/*if($row->Recibida == '-1'  ){ $row->Recibida="√";	}
				else{
					 $row->Recibida="";	
				}*/
				$row->FechaR1 = $fec->fecha($row->FechaR);
				//$row->FechaR1 = date("d-m-Y",strtotime($row->FechaR));
				$row->CantidadR = number_format($row->CantidadR, 3, '.', ',');
				$row->PrecioR = number_format($row->PrecioR, 2, '.', ',');
				$row->DescuentoR = number_format(($row->DescuentoR*100), 2, '.', ',');
				$ela='';
				if($row->NumElaR==4) {$ela='Rafael';}
				if($row->NumElaR==5) {$ela='José';}
				$row->ela =$ela;
				$row->ObsR =$row->ObsR;
				$data[] = $row;
			endforeach;
			if($entro==1){
				$this->db->select('sum(CantidadRR) as total');
				$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				$this->db->join($folio, 'NumRegR = NRem', 'inner');		
				$this->db->join('biologo', 'NumBio=NumBioR', 'left');
				$this->db->join('chofer', 'NumCho=NumChoR', 'left');
				$this->db->join('unidad', 'NumUni=NumUniR', 'left');
				$this->db->where('Estatus =',0);				
				$this->db->where($filter['where']);
				if($filter['num']!=10){
				if($filter['num']>=0 && $filter['num']<=2){
					$this->db->where($this->estatus,$filter['num']);}
				else{
					if($filter['num']==-1){
					$this->db->where($this->recibida,$filter['num']);}
					if($filter['num']==4){
					$this->db->where($this->recibida,0);}
					//$this->db->where('r14.FechaR >=' , 'txtFI');
					//$this->db->where('r14.FechaR <=' , 'txtFF');
				}
			}
				$result = $this->db->get($cic);		
				foreach ($result->result() as $row):				
					$row->FechaR1 = "Total:";$row->RemisionR = "";
					$row->CantidadR = number_format($row->total, 3, '.', ',');;
					$row->Razon = "";$row->NomBio = "";$row->NomCho = "";$row->NomUni = "";
					$row->AvisoR = "";$row->Folio = "";$row->ObsR = "";$row->PrecioR = "";$row->ela ='';
					$data[] = $row;			
				endforeach;			
			}
			return $data;
		}
		function getNumRows($filter,$cic,$folio){
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			$this->db->join($folio, 'NumRegR = NRem', 'inner');
			$this->db->join('biologo', 'NumBio=NumBioR', 'inner');
			$this->db->join('chofer', 'NumCho=NumChoR', 'left');
			$this->db->join('unidad', 'NumUni=NumUniR', 'left');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=10){
				//$this->db->where($this->id,$filter['num']);
				if($filter['num']>=0 && $filter['num']<=2){
					$this->db->where($this->estatus,$filter['num']);}
				else{
					if($filter['num']==-1){
					$this->db->where($this->recibida,$filter['num']);}
					if($filter['num']==4){
					$this->db->where($this->recibida,0);}
					//$this->db->where('r14.FechaR >=' , 'txtFI');
					//$this->db->where('r14.FechaR <=' , 'txtFF');
				}
			}
			$result = $this->db->get($cic);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function ultimaremision(){									
			$this->db->select('MAX(RemisionR) as ultimo,MAX(NumRegR) as ultimoR');
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
		function getClientes($filter){
			$this->db->select('Numero,Razon,prevta,avisoc,con,guiat');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get('clientes',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$cp=$row->CP;
				if($row->con=='') $row->con='';
				$data[] = $row;	
				/*$data['zona'] = $row->Zona;
				$data['razon'] = $row->Razon;
				$data['dom'] = $row->Dom;
				$data['loc'] = $row->Loc;
				$data['edo'] = $row->Edo;
				$data['rfc'] = $row->RFC;
				$data['cp'] = $row->CP;*/
			endforeach;
			return $data;
		}
		function getNumClientes($filter){
			
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			$result = $this->db->get('clientes');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function verEscaner(){
			//$this->db->where('numbio >',1);
			//$this->db->where('activo =',0);
			$query=$this->db->get('escaner');
			return $query->result();			
		}
		
		function getElements($where){
		//if($where['NomDes']!='Todos'){			
		$this->db->select('nesc,km as val');
		//$this->db->join('unidad', 'uniesc=NumUni', 'inner');
		//$this->db->where('NumDes >',0);
		//$this->db->where('Activo =',0);	
		//$this->db->group_by('NumUni');        
        $this->db->where($where);
        $result=$this->db->get('escaner');
        $data = array();        
        foreach($result->result() as $row):
            $data[] = $row;
        endforeach;        
        return $data;
		//}
    	}	
		public function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		public function verUnidad(){
			//$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		public function verChofer(){			
			$this->db->where('activo =',0);
			$this->db->order_by('numcho');			
			$query=$this->db->get('chofer');
			return $query->result();			
		}
		function getcvimpcv($filter,$cic){
			//SELECT NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni from biologo inner join(chofer inner join(unidad inner join(clientes inner join r18 on NumCliR=Numero) on NumUniR=NumUni) on NumChoR=NumCho) on NumBioR=NumBio order by RemisionR
			$this->db->select('NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni,NumElaR');
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			$this->db->join('biologo', 'NumBio=NumBioR', 'inner');
			$this->db->join('chofer', 'NumCho=NumChoR', 'inner');
			$this->db->join('unidad', 'NumUni=NumUniR', 'inner');
			$this->db->where('Estatus <',2);
			if($filter['where']!='')
				{$this->db->where($filter['where']);}
			//$result = $this->db->get($cic);
			$result = $this->db->get($cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->FechaR1 = $fec->fecha($row->FechaR);
				$row->LocEdo = $row->Loc.' '.$row->Edo;
				if($row->NumElaR==1) $row->Ela='Orlando'; elseif($row->NumElaR==2) $row->Ela='David'; elseif($row->NumElaR==3) $row->Ela='Jesus'; elseif($row->NumElaR==4) $row->Ela='Rafael'; elseif($row->NumElaR==5) $row->Ela='Jose'; else $row->Ela='';
				$fol=$row->NumRegR;
				$data[] = $row;
				//$this->db->select('NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni,NumElaR');
				$this->db->where('NFol',$fol);
				$resulta = $this->db->get('muestras');
				if($resulta->num_rows()>0){
					$promcv=0;$contcv=0;
				foreach($resulta->result() as $rowm):
					$m1=$rowm->M1;$m2=$rowm->M2;$m3=$rowm->M3;$cont=0;
					if($m1>0) $cont+=1;if($m2>0) $cont+=1;if($m3>0) $cont+=1;
					$array = array(2);
					$array= array ($m1,$m2,$m3);
					$desv=sd($array,$cont);
					
					$rowm->FechaR1='';
					$rowm->Ela='';
					$rowm->RemisionR='';
					$rowm->Razon='['.$rowm->M1.'] - ['.$rowm->M2.'] - ['.$rowm->M3.']'.' P: '.number_format((($m1+$m2+$m3)/$cont), 1, '.', ',');
					
					
					$rowm->LocEdo='C.V. - ['.number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',').']';
					$rowm->NomBio='';
					
					//$rowm->NomCho='Desv. Est.: '. number_format($desv, 2, '.', ',');
					$rowm->NomCho='';$rowm->cv=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');;
					$rowm->NomUni='';
					$promcv+=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$contcv+=1;
					$data[] = $rowm;
				endforeach;
				$this->db->select('MAX(Numero)');
				$result = $this->db->get('clientes');
				foreach($result->result() as $rowt):
					$rowt->FechaR1='';
					$rowt->Ela='';
					$rowt->NomUni='';
					$rowt->RemisionR='';
					$rowt->Razon='';$rowt->LocEdo='Prom: '.number_format((((($promcv)/$contcv))), 2, '.', ',');;
					$rowt->NomBio='';$rowt->NomCho='';
					
					$rowt->cv=number_format((((($promcv)/$contcv))), 2, '.', ',');						
					$data[] = $rowt;
				endforeach;
				}
			endforeach;
			return $data;
		}
	
					
    }
    function sd_square($x, $mean) { return pow($x - $mean,2); } 
		function sd($array,$cont) { 
    	// square root of sum of squares devided by N-1 
		return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)) ); 
		//return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,$cont, (array_sum($array) / $cont) ) ) ) / $cont );
		} 
?>