<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Larvario_model extends CI_Model {
        public $nrs="nrs";public $fecsal="fecsal";public $nsal="nsal";public $ncs="ncs";public $nausem="nausem";public $slar="slar";public $sras="sras";
		public $tablasal="siesallar_18";
		
		public $nrpl="nrpl";public $fecsp="fecsp";public $nmad="nmad";public $tqp="tqp";public $nco="nco";public $cansie="cansie";
		public $feccp="feccp";public $p1="p1";public $p2="p2";public $p3="p3";public $o1="o1";public $o2="o2";public $o3="o3";
		public $pesada="pesada";public $repa="repa";public $cancos="cancos";public $te1="te1";public $ce1="ce1";
		public $te2="te2";public $ce2="ce2";public $te3="te3";public $ce3="ce3";
		public $tablapil="siepillar_18";
       
	   	public $npext="npext";public $fecext="fecsieext";public $pilext="pilext";public $canext="cansieext";public $procnco="procnco";
	   	public $procsal="procsal";public $procpil="procpil";
		public $tablaext="siepilext_18";
	   
	    function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		//SELECT pilext,sum(cansieext) as sie from siepilext_18 where procnco=12 group by pilext
		//SELECT sum(cansieext) as sie from siepilext_18 where procnco=12
		//SELECT nsal,procnco,sum(cansieext) as sie f
		// rom siepilext_18 inner join siesallar_18 on ncs=procnco group by procnco order by procnco DESC
		function getsalExt($filter){
			$this->db->select('nsal,procnco,sum(cansieext) as sie ');	
			$this->db->join($this->tablasal, 'ncs=procnco', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']);}	
			$this->db->group_by($this->procnco);
			$this->db->group_by($this->nsal);
			$this->db->order_by($this->procnco, 'desc');
			$result = $this->db->get($this->tablaext );
			$data = array();$sobgen=0; $fec=new Libreria();
			foreach ($result->result() as $row):
				//$row->dia = $fec->fecha22($row->fecsal);
				$row->corsal='C'.$row->procnco.'</br>'.$row->nsal;
				if($row->sie>0){$row->siembrae = number_format($row->sie/1000, 3, '.', ',');} else { $row->siembrae="";}
				$row->cosechae="";
				$row->soblare="";
				//if($row->slar>0){$row->cosechae = number_format($row->slar, 3, '.', ',');} else { $row->cosechae="";}
				//if($row->slar>0){$row->soblar = number_format(($row->slar/$row->nausem)*100, 1, '.', ',');} else { $row->soblar="";}
				$data[] = $row;	
			endforeach;	
			return $data;
		}
		function actualizarc($id,$fec,$p1,$o1,$p2,$o2,$p3,$o3,$pesa,$repa,$cos,$te1,$ce1,$te2,$ce2,$te3,$ce3,$nco,$pil,$sala){
			$ce1 = str_replace(",", "", $ce1);$ce2 = str_replace(",", "", $ce2);$ce3 = str_replace(",", "", $ce3);
			//actualizar los datos o ingresar a exteriores las pilas de salas, busca si la pila de exyteriores esta registrada	
			$ide=0;$pls=0;
			// saber si tiene dos tanques de exteriores o uno
			if($te1>0){ //entoces solo trae datos el tanque 1
				$query=$this->db->get_where($this->tablaext,array($this->procnco=>$nco,$this->pilext=>$te1,$this->procpil=>$pil));
			if($query->num_rows>0){
			  	//encontro registro actuliza datos	
			 	foreach ($query->result() as $rowe):
			   		$ide=$rowe->npext;$pls=$rowe->cansieext;
			 	endforeach;	
			 	//hay que saber si se corrigio la cantidad de la pila de larvas para saber si se suma o se resta la cantidad
			 	//se va a buscar la cantidad que tiene ce1 en la bd si es mayor a la variable $ce1 se le resta y si es menor se suma
			 	$plsa=0;
			 	$this->db->select($this->ce1);
				$querya=$this->db->get_where($this->tablapil,array($this->nrpl=>$id));
				 foreach ($querya->result() as $rowa):
			  		$plsa=$rowa->ce1;
			   	endforeach;	
				if($ce1>$plsa) $pls+=$ce1-$plsa; elseif($ce1<$plsa) $pls-=$plsa-$ce1; //else $pls=$plsa; 
				$datae=array($this->fecext=>$fec,$this->canext=>$pls,$this->procsal=>$sala);
				$this->db->where($this->npext,$ide);
				$this->db->update($this->tablaext,$datae);
			}else{
			  //registrar los datos
			   $datae=array($this->fecext=>$fec,$this->pilext=>$te1,$this->canext=>$ce1,$this->procnco=>$nco,$this->procpil=>$pil,$this->procsal=>$sala);			
			   $this->db->insert($this->tablaext,$datae);
			   $this->db->insert_id(); 	
			}
			}
			if($te2>0){ //entoces solo trae datos el tanque 1
				$query=$this->db->get_where($this->tablaext,array($this->procnco=>$nco,$this->pilext=>$te2,$this->procpil=>$pil));
			if($query->num_rows>0){
			  	//encontro registro actuliza datos	
			 	foreach ($query->result() as $rowe):
			   		$ide=$rowe->npext;$pls=$rowe->cansieext;
			 	endforeach;	
			 	//hay que saber si se corrigio la cantidad de la pila de larvas para saber si se suma o se resta la cantidad
			 	//se va a buscar la cantidad que tiene ce1 en la bd si es mayor a la variable $ce1 se le resta y si es menor se suma
			 	$plsa=0;
			 	$this->db->select($this->ce2);
				$querya=$this->db->get_where($this->tablapil,array($this->nrpl=>$id));
				 foreach ($querya->result() as $rowa):
			  		$plsa=$rowa->ce2;
			   	endforeach;	
				if($ce2>$plsa) $pls+=$ce2-$plsa; elseif($ce2<$plsa) $pls-=$plsa-$ce2; //else $pls=$plsa; 
				$datae=array($this->fecext=>$fec,$this->canext=>$pls,$this->procsal=>$sala);
				$this->db->where($this->npext,$ide);
				$this->db->update($this->tablaext,$datae);
			}else{
			  //registrar los datos
			   $datae=array($this->fecext=>$fec,$this->pilext=>$te2,$this->canext=>$ce2,$this->procnco=>$nco,$this->procpil=>$pil,$this->procsal=>$sala);			
			   $this->db->insert($this->tablaext,$datae);
			   $this->db->insert_id(); 	
			}
			}
			if($te3>0){ //entoces solo trae datos el tanque 1
				$query=$this->db->get_where($this->tablaext,array($this->procnco=>$nco,$this->pilext=>$te3,$this->procpil=>$pil));
			if($query->num_rows>0){
			  	//encontro registro actuliza datos	
			 	foreach ($query->result() as $rowe):
			   		$ide=$rowe->npext;$pls=$rowe->cansieext;
			 	endforeach;	
			 	//hay que saber si se corrigio la cantidad de la pila de larvas para saber si se suma o se resta la cantidad
			 	//se va a buscar la cantidad que tiene ce1 en la bd si es mayor a la variable $ce1 se le resta y si es menor se suma
			 	$plsa=0;
			 	$this->db->select($this->ce3);
				$querya=$this->db->get_where($this->tablapil,array($this->nrpl=>$id));
				 foreach ($querya->result() as $rowa):
			  		$plsa=$rowa->ce3;
			   	endforeach;	
				if($ce3>$plsa) $pls+=$ce3-$plsa; elseif($ce3<$plsa) $pls-=$plsa-$ce3; //else $pls=$plsa; 
				$datae=array($this->fecext=>$fec,$this->canext=>$pls,$this->procsal=>$sala);
				$this->db->where($this->npext,$ide);
				$this->db->update($this->tablaext,$datae);
			}else{
			  //registrar los datos
			   $datae=array($this->fecext=>$fec,$this->pilext=>$te3,$this->canext=>$ce3,$this->procnco=>$nco,$this->procpil=>$pil,$this->procsal=>$sala);			
			   $this->db->insert($this->tablaext,$datae);
			   $this->db->insert_id(); 	
			}
			}
			//Actualiza los datos de las pilas de larvas	
			$cos = str_replace(",", "", $cos);
			if(($p1+$p2+$p3)>0) $cos=0;
			$data=array($this->feccp=>$fec,$this->p1=>$p1,$this->o1=>$o1,$this->p2=>$p2,$this->o2=>$o2,$this->p3=>$p3,$this->o3=>$o3,$this->pesada=>$pesa,$this->repa=>$repa,$this->cancos=>$cos,$this->te1=>$te1,$this->ce1=>$ce1,$this->te2=>$te2,$this->ce2=>$ce2,$this->te3=>$te3,$this->ce3=>$ce3);
			$this->db->where($this->nrpl,$id);
			$this->db->update($this->tablapil,$data);
			
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getsalcose($filter){
			$this->db->select('fecsieext,pilext,sum(cansieext) as sie');	
			if($filter['where']!=''){$this->db->where($filter['where']);}	
			$this->db->group_by($this->pilext);
			$this->db->group_by($this->fecext);
			$result = $this->db->get($this->tablaext);
			$resultado=$result->num_rows();
			$data = array();
			if($resultado>0){
			$sobgen=0; $fec=new Libreria();$fe='';$totsiee=0;
			
			foreach ($result->result() as $row):
				if($fe!=$row->fecsieext){
					$fe=$row->fecsieext; 
					$row->dias = $fec->fecha22($row->fecsieext);
				}else{ $row->dias="";}
				if($row->sie>0){$totsiee+=$row->sie/1000;$row->cansiee = number_format($row->sie/1000, 3, '.', ',');} else { $row->cansiee="";}
				$data[] = $row;	
			endforeach;	
			//totales
			$this->db->select('max(npext)');	
			$result = $this->db->get($this->tablaext);
			foreach ($result->result() as $row):				
				$row->dias= "Total"; $row->pilext="";$row->diac= "";$row->pes= "";$row->rep= "";$row->pro= "";
				$row->cansiee=number_format($totsiee, 3, '.', ',');
				
				$data[] = $row;	
			endforeach;
			}
			return $data;
		}
		function getsalcos($filter){
			$this->db->order_by($this->tqp);
			if($filter['where']!='')
				{$this->db->where($filter['where']);}	
			$result = $this->db->get($this->tablapil);
			$resultado=$result->num_rows();
			$data = array();
			if($resultado>0){
			$totsie=0;$totcos=0;$totpro=0;$tp=0;$totcos2=0;$cont=0;$tottra=0;
			$sobgen=0; $fec=new Libreria();$fe='';$fc='';$entro=0;$pil=0;
			while($pil<=11){
				$pila[$pil]=0;$pil+=1;
			}
			foreach ($result->result() as $row):
				$nco=$row->nco;$sie=0;$cos=0;
				
				if($row->nmad==1) $row->mad='I'; else $row->mad='II';
				if($fe!=$row->fecsp){
					$fe=$row->fecsp; 
					$row->dias = $fec->fecha22($row->fecsp);
				}else{ $row->dias="";}
				
				$totsie+=$row->cansie;$sie=$row->cansie;
				$row->cansie = number_format($row->cansie, 3, '.', ',');
				//cosechas
				$pe=0;$prom1=0;$prom2=0;$prom3=0;$pro=0;
				if($fc!=$row->feccp){
					$fc=$row->feccp; 
					if($row->feccp!='')$row->diac = $fec->fecha22($row->feccp); else $row->diac="";
				}else{ $row->diac="";}
				if($row->p1>0 && $row->o1>0){ $prom1=substr(($row->p1/$row->o1),0,7); $pe+=1;}
				if($row->p2>0 && $row->o2>0){ $prom2=substr(($row->p2/$row->o2),0,7); $pe+=1;}
				if($row->p3>0 && $row->o3>0){ $prom3=substr(($row->p3/$row->o3),0,7); $pe+=1;}
				$inc=0;
				if($pe>0) {
					if($pe==1){
						$pro=$prom1;						
						
					}else{
						$prom1=substr($prom1, 0,6);	
						$prom2=substr($prom2, 0,6);
						$prom3=substr($prom3, 0,6);
						$pro=($prom1+$prom2+$prom3)/$pe;
					} 
					$inc=substr($pro, 6, 1);
					if($inc>=5) $inc='0.0001'; else $inc=0;
					$pro=substr($pro, 0,6);
					
					$pro=$pro+$inc;
					$row->pro=$pro;
				}else{ $pro=0;$row->pro='';}
				if($row->pesada>0) $row->pes=number_format($row->pesada*1000, 0, '.', ','); else $row->pes='';
				if($row->repa>0) $row->rep=number_format($row->repa*1000, 0, '.', ','); else $row->rep='';
				if($row->te1>0){ $tottra+=$row->ce1;$row->ce1=number_format($row->ce1, 0, '.', ',');} else {$row->te1='';$row->ce1='';}
				if($row->te2>0){ $tottra+=$row->ce2; $row->ce2=number_format($row->ce2, 0, '.', ',');} else {$row->te2='';$row->ce2='';}
				if($row->te3>0){ $tottra+=$row->ce3; $row->ce3=number_format($row->ce3, 0, '.', ',');} else {$row->te3='';$row->ce3='';}
				if($pro>0){
					 $totcos+=(($row->pesada+$row->repa))/$pro;
					//$totpro+=$totcos;
					 //$totpro+=((($row->pesada+$row->repa)*1000)/$pro)/($row->cansie*10000); 
					 $tp+=1;
					 $row->cancos=number_format((($row->pesada+$row->repa)*1000)/$row->pro, 3, '.', ',');
					 
					 //$row->cancos=substr($row->cancos, 0,9);
					if((($row->pesada+$row->repa))/$row->pro>1000) {$row->cancos=substr($row->cancos, 0,9);$row->cancos1=$row->cancos;} else {$row->cancos=substr($row->cancos, 0,7);$row->cancos1=$row->cancos;}
					 $can=$row->cancos;
					 $can = str_replace(",", "", $can);
					 $totcos2+=floatval($can);
					 $cos=($row->pesada+$row->repa)/$pro;
					 //$row->sobpil=number_format(((($row->pesada+$row->repa)*1000)/$row->pro)/($row->cansie*10000), 0, '.', ',');
					 $row->sobpil=number_format(($cos/$sie)*100, 1, '.', ',');$entro+=1;
				} else {
					$totcos2+=($row->cancos*1000);
					if($row->cancos>0) $row->sobpil= number_format(($row->cancos/$sie)*100, 1, '.', ','); else $row->sobpil='';
					if($row->cancos>0) $row->cancos = number_format($row->cancos*1000, 0, '.', ','); else $row->cancos ='';
					$entro+=1;
				}
				$pila[$cont]=$row->tqp;
				$sob[$cont]=$row->sobpil;
				if($row->te1>0 && $row->te2>0 && $row->te3>0) $ext[$cont]=$row->te1.'-'.$row->te2.'-'.$row->te3; elseif($row->te1>0 && $row->te2>0) $ext[$cont]=$row->te1.'-'.$row->te2; else $ext[$cont]=$row->te1;
				$cont+=1;
				if($row->p1>0) $row->p1 = number_format($row->p1, 2, '.', ','); else $row->p1="";
				if($row->p2>0) $row->p2 = number_format($row->p2, 2, '.', ','); else $row->p2="";
				if($row->p3>0) $row->p3 = number_format($row->p3, 2, '.', ','); else $row->p3="";
				if($row->o1==0) $row->o1="";
				if($row->o2==0) $row->o2="";
				if($row->o3==0) $row->o3="";	
				if($row->pesada+$row->repa>0)$row->kgs=number_format($row->pesada+$row->repa, 3, '.', ','); else $row->kgs='';
				$row->pesada=number_format($row->pesada, 3, '.', ',');			
				$row->repa=number_format($row->repa, 3, '.', ',');
				
				$data[] = $row;	
			endforeach;	
			//actualiza el total de siembra de nauplios de la sala
			$totcos2=$totcos2/1000;$totcosl=0;
			$totcos1=number_format($totcos2, 3, '.','');
			$data1=array($this->nausem=>$totsie,$this->slar=>$totcos1);
			$this->db->where($this->ncs,$nco);
			$this->db->update($this->tablasal,$data1);
			$totcosl=$totcos1;
			//totales
			$this->db->select('max(nrs)');	
			$result = $this->db->get($this->tablasal);
			foreach ($result->result() as $row):				
				$row->dias= "Total"; $row->mad="";$row->tqp="";$row->diac= "";$row->pes= "";$row->rep= "";$row->kgs= "";$row->pro= "";
				$row->cansie=number_format($totsie, 3, '.', ',');
				if($totcos1>0) $row->cancos=number_format($totcos1, 3, '.', ','); else $row->cancos='';
				if((($totcos1/$totsie)*100) > 0) $row->sobpil=number_format(($totcos1/$totsie)*100, 1, '.', ''); else $row->sobpil='';
				if($entro>0){
				$cont=0; //$row->gra3=number_format($kg[$cont], 2, '.', ',');
				if($pila[0]==1) {$row->sobp1=$sob[$cont];$row->ex1=$ext[$cont];} else {$row->sobp1='';$row->ex1='';}$cont+=1;
				if($pila[1]==2) {$row->sobp2=$sob[$cont];$row->ex2=$ext[$cont];} else {$row->sobp2='';$row->ex2='';}$cont+=1;
				if($pila[2]==3) {$row->sobp3=$sob[$cont];$row->ex3=$ext[$cont];} else {$row->sobp3='';$row->ex3='';}$cont+=1;
				if($pila[3]==4) {$row->sobp4=$sob[$cont];$row->ex4=$ext[$cont];} else {$row->sobp4='';$row->ex4='';}$cont+=1;
				if($pila[4]==5) {$row->sobp5=$sob[$cont];$row->ex5=$ext[$cont];} else {$row->sobp5='';$row->ex5='';}$cont+=1;
				if($pila[5]==6) {$row->sobp6=$sob[$cont];$row->ex6=$ext[$cont];} else {$row->sobp6='';$row->ex6='';}$cont+=1;
				if($pila[6]==7) {$row->sobp7=$sob[$cont];$row->ex7=$ext[$cont];} else {$row->sobp7='';$row->ex7='';}$cont+=1;
				if($pila[7]==8) {$row->sobp8=$sob[$cont];$row->ex8=$ext[$cont];} else {$row->sobp8='';$row->ex8='';}$cont+=1;
				if($pila[8]==9) {$row->sobp9=$sob[$cont];$row->ex9=$ext[$cont];} else {$row->sobp9='';$row->ex9='';}$cont+=1;
				if($pila[9]==10) {$row->sobp10=$sob[$cont];$row->ex10=$ext[$cont];} else {$row->sobp10='';$row->ex10='';}$cont+=1;
				if($pila[10]==11) {$row->sobp11=$sob[$cont];$row->ex11=$ext[$cont];} else {$row->sobp11='';$row->ex11='';}$cont+=1;
				if($pila[11]==12) {$row->sobp12=$sob[$cont];$row->ex12=$ext[$cont];} else {$row->sobp12='';$row->ex12='';}$cont+=1;
				}
				$row->te1='';$row->ce2='';$row->te3='';$row->ce3='';
				if($tottra>0) $row->ce1=number_format($tottra/1000, 3, '.', ','); else $row->ce1='';
				if((($tottra/1000)-$totcosl)!=0) {$row->te2='Dif';$row->ce2=number_format((($tottra/1000)-$totcosl), 3, '.', ',');} else {$row->te2='';$row->ce2='';}
				$data[] = $row;	
			endforeach;
			}
			return $data;
		}
		public function agregarp($fec,$can,$mad,$pil,$nco){
			$can = str_replace(",", "", $can);
			$data=array($this->fecsp=>$fec,$this->nmad=>$mad,$this->tqp=>$pil,$this->cansie=>$can,$this->nco=>$nco);			
			$this->db->insert($this->tablapil,$data);
			return $this->db->insert_id();
		}
		function actualizarp($id,$fec,$can,$mad,$pil,$nco){
			$can = str_replace(",", "", $can);
			$data=array($this->fecsp=>$fec,$this->nmad=>$mad,$this->tqp=>$pil,$this->cansie=>$can,$this->nco=>$nco);
			$this->db->where($this->nrpl,$id);
			$this->db->update($this->tablapil,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		
		
		function ultimacorrida(){									
			$this->db->select('MAX(ncs) as ultimo');
			$query = $this->db->get($this->tablasal);
			return $query->result();
		}
		 
		function getsalLar($filter){
			$this->db->order_by($this->ncs, 'desc');
			$result = $this->db->get($this->tablasal);
			$data = array();$sobgen=0; $fec=new Libreria();
			foreach ($result->result() as $row):
				$row->dia = $fec->fecha22($row->fecsal);
				$row->corsal='C'.$row->ncs.'</br>'.$row->nsal;
				if($row->nausem>0){$row->siembra = number_format($row->nausem, 3, '.', ',');} else { $row->siembra="";}
				if($row->slar>0){$row->cosecha = number_format($row->slar, 3, '.', ',');} else { $row->cosecha="";}
				if($row->slar>0){$row->soblar = number_format(($row->slar/$row->nausem)*100, 1, '.', ',');} else { $row->soblar="";}
				//if($row->sras>0){$row->sobext = number_format($row->sras, 2, '.', ',');} else { $row->sobext="";}
				//if($row->sras>0){$row->sobgen = number_format($sobgen, 2, '.', ',');} else { $row->sobgen="";}
				$data[] = $row;	
			endforeach;	
			return $data;
		}
 
		public function agregars($fec,$sal,$uc){
			$data=array($this->fecsal=>$fec,$this->nsal=>$sal,$this->ncs=>$uc);			
			$this->db->insert($this->tablasal,$data);
			return $this->db->insert_id();
		}
		function actualizars($id,$fec,$sal){
			$data=array($this->fecsal=>$fec,$this->nsal=>$sal);
			$this->db->where($this->nrs,$id);
			$this->db->update($this->tablasal,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		//pilas
		public function verPila(){
			//$this->db->where('Cancelacion =',0);
			
			$query=$this->db->get('raceways');
			
			return $query->result();			
		}
						
    }    
?>

