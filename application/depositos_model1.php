<?php
    class Depositos_model extends CI_Model {       
        public $nrc="NRC";
		public $id="Numero";
		public $fecha="Fecha";
        public $razon="Razon";		
		public $imported="ImporteD";
		public $pesos="Pesos";
		public $cuenta="Cuenta";
		public $tc="TC";
		public $obs="Obs";
		public $nd="ND";
		public $cam="Cam";
		public $des="Des";
		public $car="Car";
		public $aplicar="Aplicar";
		public $tabla="depositos";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function agregar($fec,$dolar,$tc,$cta,$obs,$pesos,$cliente,$ciclo){
			$data=array($this->fecha=>$fec,$this->imported=>$dolar,$this->tc=>$tc,$this->cuenta=>$cta,$this->obs=>$obs,$this->pesos=>$pesos,$this->nrc=>$cliente,$this->aplicar=>$ciclo);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($nd,$fec,$dolar,$tc,$cta,$obs,$pesos,$cliente,$ciclo){
			$pesos = str_replace(",", "", $pesos);
			$dolar = str_replace(",", "", $dolar);
			$tc = str_replace(",", "", $tc);
			$data=array($this->fecha=>$fec,$this->imported=>$dolar,$this->tc=>$tc,$this->cuenta=>$cta,$this->obs=>$obs,$this->pesos=>$pesos,$this->nrc=>$cliente,$this->aplicar=>$ciclo);
			$this->db->where($this->nd,$nd);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		public function verActivos($fi,$ff){
			$this->db->select('Numero,Fecha,Razon,ImporteD,Pesos,Cuenta,TC,Obs,ND,Cam');
			$this->db->from($this->tabla);
			$this->db->join('clientes', 'depositos.NRC=clientes.Numero', 'inner');
			$this->db->where("Fecha >= '$fi' AND Fecha <= '$ff' AND ((depositos.Des = 0 AND depositos.Car=0) OR (depositos.Cam=-1))");			
			$this->db->order_by($this->fecha,'desc');
			$this->db->order_by($this->razon);
			$query = $this->db->get();
			//return $query->result();
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
		
		public function getDeposito($id){
			$this->db->select('Numero,Fecha,Razon,ImporteD,Pesos,Cuenta,TC,Obs,ND,Cam,Zona,Aplicar');	
			$this->db->from($this->tabla);		
			$this->db->join('clientes', 'depositos.NRC=clientes.Numero', 'inner');
			$this->db->where('ND',$id);
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		public function eliminarDeposito($id){
			$this->db->select('Numero,Fecha,Razon,ImporteD,Pesos,Cuenta,TC,Obs,ND,Cam,Zona,Aplicar');	
			$this->db->from($this->tabla);		
			$this->db->join('clientes', 'depositos.NRC=clientes.Numero', 'inner');
			$this->db->where('ND',$id);
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		
		function buscar_activos($nom,$fi,$ff){
			$this->db->select('Numero,Fecha,Razon,ImporteD,Pesos,Cuenta,TC,Obs,ND,Cam');
			$this->db->from($this->tabla);
			$this->db->join('clientes', 'depositos.NRC=clientes.Numero', 'inner');
			$this->db->like($this->razon,$nom,'both');
			$this->db->where("Fecha >= '$fi' AND Fecha <= '$ff' AND ((depositos.Des = 0 AND depositos.Car=0) OR (depositos.Cam=-1))");			
			//$this->db->where("NRC='".$clacli."' and (depositos.Des = 0 AND depositos.Car=0 and depositos.Cam=0)) or (NRC='".$clacli."' and (depositos.Des = -1 AND depositos.Car=0 and depositos.Cam=-1)");
			$this->db->order_by($this->fecha);
			$this->db->order_by($this->razon);
			$query = $this->db->get();
			//return $query->result();
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
		
		
		public function getClienteD($id){
			$this->db->where('Numero',$id);
			$query=$this->db->get('clientes');
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		
		
		public function verActivosC(){
			$this->db->where('status',1);
			//$this->db->order_by('Zona');
			$this->db->order_by('Razon');
			$query = $this->db->get('clientes');
			return $query->result();
		}
		
		
    }
    
?>