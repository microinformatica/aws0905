<?php
    
    class Alumnos_model extends CI_Model {
        //private $nombre;
        public $id="id";
        public $nombre="nombre";
		public $edad="edad";
		public $status="status";
		public $tabla="alumnos";
        function __construct() {
            $this->load->database(); //carga librerias para manejar db
            parent::__construct(); //llamar al constructor de CI_Model
        }
		
		function get_last_ten_entries()
		{
			$query=$this->db->get('alumnos');
			return $query->result();
		}
		public function insertar($nombre,$edad)
		{
			$this->nombre = $nombre;
			$this->edad = $edad;
			$this->db->insert('alumnos', $this);
		}
		public function verActivos(){
			/*$query=$this->db->get('alumnos');*/
			$this->db->where($this->status,1);
			$this->db->order_by("nombre");
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
		public function agregar($nombre,$edad,$status){
			$data=array($this->nombre=>$nombre,$this->edad=>$edad,$this->status=>$status);
			/*$query=$this->db->get($tabla);*/
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$nombre,$edad){
			$data=array($this->nombre=>$nombre,$this->edad=>$edad);
			/*$query=$this->db->get($tabla);*/
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function activar($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,array($this->status=>'1'));
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function desactivar($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,array($this->status=>'0'));
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function getNombre($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->select($this->nombre);
			$query=$this->db->get_where($this->tabla,array($this->id=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->nombre;
			}
			return 0;
		}
		public function getEdad($id){
			/*$query=$this->db->get($tabla);*/
			$this->db->select($this->edad);
			$query=$this->db->get_where($this->tabla,array($this->id=>$id));
			if($query->num_rows>0){
			 foreach($query->result() as $fila)
			 	return $fila->edad;
			}
			return 0;
		}
		public function getAlumno($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('id',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		function buscar_activos($nombre){
			$this->db->like($this->nombre,$nombre,'both');	
			$this->db->where($this->status,1);
			$query=$this->db->get($this->tabla); 
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
		
    }
    
?>