<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Coeficiente_model extends CI_Model {
       	
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
			function kpilog($filter){
				//select nad,nom from analisisdepto inner join analisisresdiadepto on nadr=nad where month(fech)=3 and ndep=17 group by nad,nom
				$this->db->select('nad,nom');
				$this->db->join('analisisresdiadepto', 'nadr=nad', 'inner');	
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->group_by('nad');$this->db->group_by('nom');
				$result = $this->db->get('analisisdepto');
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				$data = array();$datose=array();$datosd=array();$datosr=array();$datosv=array();$datosvt=array();$nadr=0;
				if($cantidadre>0){
				$ini=1; while($ini<=31){$datosv [$ini]='';$datosvt [$ini]='';$ini+=1;}	
				foreach ($result->result() as $row):
					$nadr=$row->nad;
					//select day(fech) as dia, estr from analisisdepto inner join analisisresdiadepto on nadr=nad where nadr=6 and month(fech)=3 and ndep=17 group by dia
					$this->db->select('day(fech) as dia, estr,nadr');
					$this->db->join('analisisresdiadepto', 'nadr=nad', 'inner');	
					$this->db->where('nadr =',$nadr);
					if($filter['where']!='') {$this->db->where($filter['where']);}
					$this->db->group_by('day(fech)');$this->db->group_by('estr');
					$resulta = $this->db->get('analisisdepto');
					//$cantidadre=$resulta->num_rows();
					$ini=1; $cont=0;
					while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					foreach ($resulta->result() as $rowa):
						$val=0;
						$datosd [$rowa->dia]=$rowa->dia; 
						$datosr [$rowa->dia]=$rowa->estr; 
						if($rowa->estr>0) $datosvt [$rowa->dia]+=1;
						$val=$rowa->estr;	
						if($rowa->nadr==9 ){
							if($rowa->estr>=0.01 and $rowa->estr<=3.0) $val=100;
							elseif(($rowa->estr>=3.01 and $rowa->estr<=4.99) ) $val=60;
								elseif($rowa->estr>=5.0) $val=20;
						}						
						$datosv [$rowa->dia]+=$val;
						
					endforeach;
					
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$data[] = $row;	
				endforeach;
				//kpi general departamento
				$this->db->select('max(Numero)');
				$result = $this->db->get('clientes');
				foreach ($result->result() as $row):
					$row->nom ="KPI";
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				endforeach;
				$data[] = $row;	
				}
				return $data;
			}
				
			function getent($filter,$cic,$fi){
				$ini=1;
				$data = array();$fec=new Libreria();
				$fr [1]='';$fr [2]='';$fr [3]='';$fr [4]='';$fr [5]='';$fr [6]='';$fr [7]='';
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):
					$row->zon ="";
					$row->cli ="";
					$fecha = $fi;$fr1 = $fi;
					$fr [1]=$fecha;$row->d1 =$fec->fecha($fecha);
					$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d2 =$fec->fecha($nuevafecha);
					$fr [2]=$nuevafecha;$fr2 = $nuevafecha;
					$nuevafecha = strtotime ( '+2 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d3 =$fec->fecha($nuevafecha);
					$fr [3]=$nuevafecha;$fr3 = $nuevafecha;
					$nuevafecha = strtotime ( '+3 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d4 =$fec->fecha($nuevafecha);
					$fr [4]=$nuevafecha;$fr4 = $nuevafecha;
					$nuevafecha = strtotime ( '+4 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d5 =$fec->fecha($nuevafecha);
					$fr [5]=$nuevafecha;$fr5 = $nuevafecha;
					$nuevafecha = strtotime ( '+5 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d6 =$fec->fecha($nuevafecha);
					$fr [6]=$nuevafecha;$fr6 = $nuevafecha;
					$nuevafecha = strtotime ( '+6 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );$row->d7 =$fec->fecha($nuevafecha);
					$fr [7]=$nuevafecha;$ff=$nuevafecha;$fr7 = $nuevafecha;
					$row->uf =$nuevafecha;
					$row->tc='';
					$data[] = $row;
				endforeach;	
				$clie='';$d1=0;$d2=0;$d3=0;$d4=0;$d5=0;$d6=0;$d7=0;$gt=0;
				$query=$this->db->query("select Zona,NumCliR,Razon from clientes inner join r17 on NumCliR=Numero where FechaR>='$fi' and FechaR<='$ff' and Estatus<=1 group by r17.NumCliR order by Zona,Razon");
				foreach($query->result() as $row):
					if($clie!=$row->Zona){ $clie=$row->Zona;
						$row->zon=$row->Zona;
					}else{ $row->zon="";} 		
					$row->cli=$row->Razon;$row->ncli=$row->NumCliR;$row->zon1=$row->Zona;
					$numc=$row->NumCliR;
					//aqui se buscaran todas las entregas de acuerdo al cliente
					$nz=0;
					$row->d1 ="";$row->d2 ="";$row->d3 ="";$row->d4 ="";$row->d5 ="";$row->d6 ="";$row->d7 ="";$var='';
					$tc=0;$can=0;
					//$querycs=$this->db->query("select FechaR,RemisionR,Ser from serbio join r17 on NumCliS=NumCliR where NumCliR= '$numc' and FechaR>='$fi' and FechaR<='$ff' and Estatus<=1 order by FechaR,RemisionR");
					//$querycs=$this->db->query("select FechaR,RemisionR,(select Ser from serbio where NumCliS='$numc' and NumRemS=NumRegR group by NumRegR)as Ser from r17 where NumCliR= '$numc' and FechaR>='$fi' and FechaR<='$ff' and Estatus<=1 order by FechaR,RemisionR");
					$querycs=$this->db->query("select NumCliR,FechaR,Ser,sum(CantidadRR) as Cantidad from r17 left join serbio on NumRemS=NumRegR where NumCliR= '$numc' and FechaR>='$fi' and FechaR<='$ff' and Estatus<=1 group by FechaR,Ser");
					foreach($querycs->result() as $rowcs):
						$fe=$rowcs->FechaR;
						$ini=1;$tc+=$rowcs->Cantidad;$gt+=$rowcs->Cantidad;
						//if($rowcs->CantidadRR>0)$can=$rowcs->CantidadRR;
						$can=intval($rowcs->Cantidad);
						//$can+=$rowcs->Cantidad;
						if($rowcs->Ser==2)$var='.'; elseif($rowcs->Ser==-2) $var='-'; else $var='';
						//while($ini<=7){
						 //switch($ini){
							//case 1: 
							if($fr [1]==$fe){ $row->d1 = $var.$can;$d1+=$rowcs->Cantidad;} 
							//	break;
							//case 2:
							if($fr [2]==$fe){ $row->d2 = $var.$can;$d2+=$rowcs->Cantidad;}
							// break;
							//case 3: 
							if($fr [3]==$fe){ $row->d3 = $var.$can;$d3+=$rowcs->Cantidad;} 
							//break;
							//case 4: 
							if($fr [4]==$fe){ $row->d4 = $var.$can;$d4+=$rowcs->Cantidad;}
							 //break;
							//case 5: 
							if($fr [5]==$fe){ $row->d5 = $var.$can;$d5+=$rowcs->Cantidad;} 
							//break;
							//case 6: 
							if($fr [6]==$fe){ $row->d6 = $var.$can;$d6+=$rowcs->Cantidad;}
							// break;
							//case 7: 
							if($fr [7]==$fe){ $row->d7 = $var.$can;$d7+=$rowcs->Cantidad;}
							//break;
						 //} 
						//$ini+=1;
					//	}			
										
					endforeach;
					//$can=0;
					 if($tc>0)$row->tc =number_format($tc, 3, '.', ','); else $row->tc='';
					 $tc=0;
					$data[] = $row;
				endforeach;		
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);$por=0;
				foreach ($result->result() as $row):
					$row->zon ="";
					$row->cli ="Total Día";
					if($d1>0){$row->d1 =number_format($d1, 3, '.', ',');$por+=1;} else $row->d1='';
					if($d2>0){$row->d2 =number_format($d2, 3, '.', ',');$por+=1;} else $row->d2='';
					if($d3>0){$row->d3 =number_format($d3, 3, '.', ',');$por+=1;} else $row->d3='';
					if($d4>0){$row->d4 =number_format($d4, 3, '.', ',');$por+=1;} else $row->d4='';
					if($d5>0){$row->d5 =number_format($d5, 3, '.', ',');$por+=1;} else $row->d5='';
					if($d6>0){$row->d6 =number_format($d6, 3, '.', ',');$por+=1;} else $row->d6='';
					if($d7>0){$row->d7 =number_format($d7, 3, '.', ',');$por+=1;} else $row->d7='';
					if($gt>0){$row->tc =number_format($gt, 3, '.', ',');} else $row->tc='';
				$data[] = $row;
				endforeach;	
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):
					$row->zon ="Clientes";
					$row->cli ="Satisfechos";$ini=1;$tc=0;
					while($ini<=7){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); 
					$queryt=$this->db->get('r17');
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Ser) as Ser');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Ser =',2);
					$query=$this->db->get('r17');
					foreach($query->result() as $rowcs):
						$d1=$rowcs->Ser;
						 switch($ini){
						 	case 1: if($d1>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d1=''; break;
							case 2: if($d1>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d2=''; break;
							case 3: if($d1>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d3=''; break;
							case 4: if($d1>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d4=''; break;
							case 5: if($d1>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d5=''; break;
							case 6: if($d1>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d6=''; break;
							case 7: if($d1>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d7=''; break;
						 }	 
						 //busco para actualizar o agregar kpi general del dia
						 if($d1>0){
							 $this->db->select('nrdad');
						 	$this->db->where('fech =',$fr [$ini]);$this->db->where('nadr =',6);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	if($tt>0) $por1=number_format((($d1/$tt)*100), 1, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr [$ini],'nadr'=>6);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }		 
					endforeach;
					$ini+=1;	
					}if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';							
				$data[] = $row;
				endforeach;	
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):
					$row->zon ="";
					$row->cli ="Insatisfechos";$ini=1;$tc=0;
					while($ini<=7){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); 
					$queryt=$this->db->get('r17');
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Ser) as Ser');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Ser =',-2);
					$query=$this->db->get('r17');
					foreach($query->result() as $rowcs):
						$d1=$rowcs->Ser;
						 switch($ini){
						 	case 1: if($d1>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d1=''; break;
							case 2: if($d1>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d2=''; break;
							case 3: if($d1>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d3=''; break;
							case 4: if($d1>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d4=''; break;
							case 5: if($d1>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d5=''; break;
							case 6: if($d1>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d6=''; break;
							case 7: if($d1>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d7=''; break;
						 }	 
					endforeach;
					$ini+=1;	
					}if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';		
				$data[] = $row;
				endforeach;	
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):
					$row->zon ="";
					$row->cli ="Sin Calificación";$ini=1;$tc=0;
					while($ini<=7){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); 
					$queryt=$this->db->get('r17');
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Ser) as Ser');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Folio >',0); $this->db->where('Ser =',0); 
					$query=$this->db->get('r17');
					foreach($query->result() as $rowcs):
						$d1=$rowcs->Ser;
						 switch($ini){
						 	case 1: if($d1>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d1=''; break;
							case 2: if($d1>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d2=''; break;
							case 3: if($d1>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d3=''; break;
							case 4: if($d1>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d4=''; break;
							case 5: if($d1>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d5=''; break;
							case 6: if($d1>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d6=''; break;
							case 7: if($d1>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d7=''; break;
						 }	 
					endforeach;
					$ini+=1;	
					}if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';
					
				$data[] = $row;
				endforeach;	
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):
					$row->zon ="";
					$row->cli ="Por Procesar";$ini=1;$tc=0;
					while($ini<=7){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); 
					$queryt=$this->db->get('r17');
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR)as Ser');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Folio >',0);  
					$query=$this->db->get('r17');
					foreach($query->result() as $rowcs):
						$d1=$tt-$rowcs->Ser;
						 switch($ini){
						 	case 1: if($d1>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d1=''; break;
							case 2: if($d1>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d2=''; break;
							case 3: if($d1>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d3=''; break;
							case 4: if($d1>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d4=''; break;
							case 5: if($d1>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d5=''; break;
							case 6: if($d1>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d6=''; break;
							case 7: if($d1>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',').'%';$tc+=($d1/$tt);} else $row->d7=''; break;
						 }	 
					endforeach;
					$ini+=1;	
					}if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';
					
				$data[] = $row;
				endforeach;
				
				return $data;	
			}
		function verDepto(){			
			$this->db->order_by('NomDep');			
			$query=$this->db->get('departamento');
			return $query->result();			
		}
		//Detalle
		function getdeta($filter,$cic,$fi,$cli){
				$ini=1;$fecha = $fi;
				$data = array();$fec=new Libreria();$ct=0;$var='';
				$nuevafecha = strtotime ( '+6 day' , strtotime ( $fecha ) ) ;$ff = date ( 'Y-m-d' , $nuevafecha );
				$query=$this->db->query("select FechaR,RemisionR,CantidadRR,CV,NumElaR,(select NomBio from biologo where NumBio=NumBioR)as tec,(select NomCho from chofer where NumCho=NumChoR)as cho,(select NomUni from unidad where NumUni=NumUniR)as uni,Folio,Ser,Obs from r17 left join serbio on NumRemS=NumRegR where NumCliR='$cli' and FechaR>='$fi' and FechaR<='$ff' and Estatus<=1 order by FechaR,RemisionR");
				foreach($query->result() as $row):
					if($row->Ser==2)$var='.'; elseif($row->Ser==-2) $var='-'; else $var='';
					$row->RemisionR=$var.$row->RemisionR;
					$row->Fecha1 = $fec->fecha($row->FechaR);$ct+=$row->CantidadRR;
					if($row->NumElaR==1) ($row->cos='Orlando Morales');
					if($row->NumElaR==2) ($row->cos='David Labrador');
					if($row->NumElaR==3) ($row->cos='Jesús Zatarain');
					$row->can =number_format($row->CantidadRR, 3, '.', ',');
					if($row->CV>0) $row->CV=number_format($row->CV, 2, '.', ','); else $row->CV='';
					if($row->Folio==null){$row->Folio="";}
					if($row->Obs==null){$row->Obs="";}
					$data[] = $row;
				endforeach;	
				$this->db->select('max(RemisionR)');
				$result = $this->db->get($cic);
				foreach ($result->result() as $row):	
					$row->Fecha1 = 'Total:';$row->RemisionR='';
					$row->can =number_format($ct, 3, '.', ',');
					$row->CV ='';$row->tec='';$row->cos='';$row->Folio='';$row->Obs='';
				$data[] = $row;
				endforeach;		
				return $data;	
			}	
	
	//Gral Mes viaticos
		function gettec($filter,$cic,$mes){
			$ini=1;
			$data = array();
			//mes
			
			$fecha='2017-'.$mes.'-01';
			while($ini<=31){
				$fr1 [$ini]=$fecha;
				$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$fecha = date ( 'Y-m-d' , $fecha ); 
				$ini+=1;				
			}
			$ini=1;
			
			//agarrar los numeros de biologos activos
			$this->db->select('NumBio,NomBio,Alias'); $this->db->where('alias !=',''); $this->db->where('activo =',0); 
			$this->db->where('numbio !=',63);
			$this->db->order_by('nombio');$this->db->order_by('NomBio');
			$query=$this->db->get('biologo');
			$bio=1; $cont=0;
			foreach ($query->result() as $row):
				$datosBi [$bio]=$row->NumBio; $datoslug [$bio]=$row->Alias;
				$bio+=1;$cont+=1;
			endforeach;
			$nbio=$bio;//$nb=29;$nbio=29;
			$nb=1;			
			while($nb<$nbio){
			    $biolog=$datosBi [$nb];$ini=1;$entro2=0;
				$this->db->select('Alias,day(FechaR) as dia,RemisionR,Loc,Edo,NumRegR');
				$this->db->join('biologo', 'NumBio=NumBioR', 'inner');	$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				//$this->db->join('serbio', 'NumRemS = NumRegR','left');
				//$this->db->group_by('NumRemS');
				$this->db->where('Estatus <',2); $this->db->where('NumBio =',$biolog);
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->order_by('NomBio');$this->db->order_by('dia','ASC');	$this->db->order_by('RemisionR', 'ASC');
				$result = $this->db->get($cic);
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;$diac=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$datosd [$dia]=$row->dia;$rem=$row->NumRegR;
						$ser='';$adm='';
						//$datosr [$dia]=$row->RemisionR.'-'.$row->Loc.' '.substr($row->Edo, 0, 3);
						//if($row->Ser==0 and $row->Folio!='') $ser='[SC]';
							//else if($row->Ser!=0) $ser='[*]';
						$this->db->select('Ser,Folio,Adm');	
						$this->db->where('NumRemS =',$rem);
						$resulta = $this->db->get('serbio');
						$cantidadser=$resulta->num_rows(); 
						if($cantidadser>=1){
							foreach ($resulta->result() as $rowser):
								$adm=$rowser->Adm;
								if($adm!=0){
									if($adm==2){$adm='.';}
									if($adm==-2){$adm='-';}
									if($adm==1){$adm=' ';}
								}else{$adm='';}
								if($rowser->Ser==0 and $rowser->Folio!=''){$ser='[SC]';}
							endforeach;	
						}else{
							$ser='[F]';$adm='';
						}	
						if($diac!=$dia){
							//$datosr [$dia]=$adm.'['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser; $diac=$dia;
							$datosr [$dia]=$adm.$row->RemisionR; $diac=$dia;
						}
						else{
							//$datosr [$dia]=$adm.$datosr [$dia].'<br>['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser;
							$datosr [$dia]=$adm.$datosr [$dia].$row->RemisionR;
						}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tec=$datoslug [$nb];$data[] = $row;
				}
				else{
					$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec=$datoslug [$nb];
					$row->d1 ="";$row->d2 ="";$row->d3 ="";$row->d4 ="";$row->d5 ="";$row->d6 ="";$row->d7 ="";$row->d8 ="";$row->d9 ="";$row->d10 ="";
					$row->d11 ="";$row->d12 ="";$row->d13 ="";$row->d14 ="";$row->d15 ="";$row->d16 ="";$row->d17 ="";$row->d18 ="";$row->d19 ="";$row->d20 ="";
					$row->d21 ="";$row->d22 ="";$row->d23 ="";$row->d24 ="";$row->d25 ="";$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";
					$row->d31 ="";
					$data[] = $row;
					endforeach;	
				}
			$nb+=1;
			}
					/*$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec='dia';
					$row->d1 =$fr1 [1];$row->d2 =$fr1 [2];$row->d3 =$fr1 [3];$row->d4 =$fr1 [4];$row->d5 =$fr1 [5];
					$row->d6 =$fr1 [6];$row->d7 =$fr1 [7];$row->d8 =$fr1 [8];$row->d9 =$fr1 [9];$row->d10 =$fr1 [10];
					$row->d11 =$fr1 [11];$row->d12 =$fr1 [12];$row->d13 =$fr1 [13];$row->d14 =$fr1 [14];$row->d15 =$fr1 [15];
					$row->d16 =$fr1 [16];$row->d17 =$fr1 [17];$row->d18 =$fr1 [18];$row->d19 =$fr1 [19];$row->d20 =$fr1 [20];
					$row->d21 =$fr1 [21];$row->d22 =$fr1 [22];$row->d23 =$fr1 [23];$row->d24 =$fr1 [24];$row->d25 =$fr1 [25];
					$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";$row->d31 ="";
					$data[] = $row;
					endforeach;	*/
			//general porcentaje dia
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($cic);
			foreach ($result->result() as $row):
				$row->tec ="KPI";$ini=1;$tc=0;
				while($ini<=31){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
					$queryt=$this->db->get('r17');
					//if($queryt->num_rows()>0){
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Adm) as Adm');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Adm =',2);$this->db->where('NumBioR !=',63);
					$query=$this->db->get('r17');
					
					
					/*$fecis=$fr1 [$ini];
					$query=$this->db->query("select count(RemisionR),(count(Adm))as Adm from r17 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=2 and NumbioR!=63");*/
					foreach($query->result() as $rowcs):
						$adm1=0;
						$d1=$rowcs->Adm; 
						//$querys=$this->db->query("select count(Adm)as Adm1 from r17 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=1 and NumbioR!=63");
						$this->db->select('count(RemisionR),count(Folio),count(Adm) as Adm1');
						$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
						$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Adm =',1);$this->db->where('NumBioR !=',63);
						$querys=$this->db->get('r17');
						 foreach($querys->result() as $rowcss):
							$adm1=$rowcss->Adm1;
						endforeach;	
						if($adm1 > 0) $d1=($d1)+($adm1*.75); 
						 switch($ini){
						 	case 1: if($d1>0 and $tt>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d1=''; break;
							case 2: if($d1>0 and $tt>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d2=''; break;
							case 3: if($d1>0 and $tt>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d3=''; break;
							case 4: if($d1>0 and $tt>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d4=''; break;
							case 5: if($d1>0 and $tt>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d5=''; break;
							case 6: if($d1>0 and $tt>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d6=''; break;
							case 7: if($d1>0 and $tt>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d7=''; break;
							case 8: if($d1>0 and $tt>0){$row->d8 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d8=''; break;
							case 9: if($d1>0 and $tt>0){$row->d9 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d9=''; break;
							case 10: if($d1>0 and $tt>0){$row->d10 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d10=''; break;
							case 11: if($d1>0 and $tt>0){$row->d11 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d11=''; break;
							case 12: if($d1>0 and $tt>0){$row->d12 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d12=''; break;
							case 13: if($d1>0 and $tt>0){$row->d13 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d13=''; break;
							case 14: if($d1>0 and $tt>0){$row->d14 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d14=''; break;
							case 15: if($d1>0 and $tt>0){$row->d15 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d15=''; break;
							case 16: if($d1>0 and $tt>0){$row->d16 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d16=''; break;
							case 17: if($d1>0 and $tt>0){$row->d17 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d17=''; break;
							case 18: if($d1>0 and $tt>0){$row->d18 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d18=''; break;
							case 19: if($d1>0 and $tt>0){$row->d19 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d19=''; break;
							case 20: if($d1>0 and $tt>0){$row->d20 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d20=''; break;
							case 21: if($d1>0 and $tt>0){$row->d21 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d21=''; break;
							case 22: if($d1>0 and $tt>0){$row->d22 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d22=''; break;
							case 23: if($d1>0 and $tt>0){$row->d23 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d23=''; break;
							case 24: if($d1>0 and $tt>0){$row->d24 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d24=''; break;
							case 25: if($d1>0 and $tt>0){$row->d25 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d25=''; break;
							case 26: if($d1>0 and $tt>0){$row->d26 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d26=''; break;
							case 27: if($d1>0 and $tt>0){$row->d27 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d27=''; break;
							case 28: if($d1>0 and $tt>0){$row->d28 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d28=''; break;
							case 29: if($d1>0 and $tt>0){$row->d29 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d29=''; break;
							case 30: if($d1>0 and $tt>0){$row->d30 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d30=''; break;
							case 31: if($d1>0 and $tt>0){$row->d31 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d31=''; break;
						 }	 
						 //busco para actualizar o agregar kpi general del dia
						 
						 if($d1>0){
							 $this->db->select('nrdad');
						 	$this->db->where('fech =',$fr1 [$ini]);$this->db->where('nadr =',7);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	if($tt>0) $por1=number_format((($d1/$tt)*100), 1, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr1 [$ini],'nadr'=>7);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }		 
					endforeach;
					//}
				  $ini+=1;	
				}//if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';							
				$data[] = $row;
				endforeach;
			return $data;	
		}
//Gral Mes transporte tecnicos
		function gettectra($filter,$cic,$mes){
			$ini=1;
			$data = array();
			//mes
			
			$fecha='2017-'.$mes.'-01';
			while($ini<=31){
				$fr1 [$ini]=$fecha;
				$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$fecha = date ( 'Y-m-d' , $fecha ); 
				$ini+=1;				
			}
			$ini=1;
			
			//agarrar los numeros de biologos activos
			$this->db->select('NumBio,NomBio,Alias'); $this->db->where('alias !=',''); $this->db->where('activo =',0); 
			$this->db->where('numbio !=',63);
			$this->db->order_by('nombio');$this->db->order_by('NomBio');
			$query=$this->db->get('biologo');
			$bio=1; $cont=0;
			foreach ($query->result() as $row):
				$datosBi [$bio]=$row->NumBio; $datoslug [$bio]=$row->Alias;
				$bio+=1;$cont+=1;
			endforeach;
			$nbio=$bio;//$nb=29;$nbio=29;
			$nb=1;			
			while($nb<$nbio){
			    $biolog=$datosBi [$nb];$ini=1;$entro2=0;
				$this->db->select('Alias,day(FechaR) as dia,RemisionR,Loc,Edo,NumRegR');
				$this->db->join('biologo', 'NumBio=NumBioR', 'inner');	$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				//$this->db->join('serbio', 'NumRemS = NumRegR','left');
				//$this->db->group_by('NumRemS');
				$this->db->where('Estatus <',2); $this->db->where('NumBio =',$biolog);
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->order_by('NomBio');$this->db->order_by('dia','ASC');	$this->db->order_by('RemisionR', 'ASC');
				$result = $this->db->get($cic);
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;$diac=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$datosd [$dia]=$row->dia;$rem=$row->NumRegR;
						$ser='';$adm='';
						//$datosr [$dia]=$row->RemisionR.'-'.$row->Loc.' '.substr($row->Edo, 0, 3);
						//if($row->Ser==0 and $row->Folio!='') $ser='[SC]';
							//else if($row->Ser!=0) $ser='[*]';
						$this->db->select('Ser,Folio,Act');	
						$this->db->where('NumRemS =',$rem);
						$resulta = $this->db->get('serbio');
						$cantidadser=$resulta->num_rows(); 
						if($cantidadser>=1){
							foreach ($resulta->result() as $rowser):
								$adm=$rowser->Act;
								if($adm!=0){
									if($adm==2){$adm='.';}
									if($adm==-2){$adm='-';}
									if($adm==1){$adm='1.';}
								}else{$adm='';}
								if($rowser->Ser==0 and $rowser->Folio!=''){$ser='[SC]';}
							endforeach;	
						}else{
							$ser='[F]';$adm='';
						}	
						if($diac!=$dia){
							//$datosr [$dia]=$adm.'['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser; $diac=$dia;
							$datosr [$dia]=$adm.$row->RemisionR; $diac=$dia;
						}
						else{
							//$datosr [$dia]=$adm.$datosr [$dia].'<br>['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser;
							$datosr [$dia]=$adm.$datosr [$dia].$row->RemisionR;
						}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tec=$datoslug [$nb];$data[] = $row;
				}
				else{
					$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec=$datoslug [$nb];
					$row->d1 ="";$row->d2 ="";$row->d3 ="";$row->d4 ="";$row->d5 ="";$row->d6 ="";$row->d7 ="";$row->d8 ="";$row->d9 ="";$row->d10 ="";
					$row->d11 ="";$row->d12 ="";$row->d13 ="";$row->d14 ="";$row->d15 ="";$row->d16 ="";$row->d17 ="";$row->d18 ="";$row->d19 ="";$row->d20 ="";
					$row->d21 ="";$row->d22 ="";$row->d23 ="";$row->d24 ="";$row->d25 ="";$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";
					$row->d31 ="";
					$data[] = $row;
					endforeach;	
				}
			$nb+=1;
			}
					/*$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec='dia';
					$row->d1 =$fr1 [1];$row->d2 =$fr1 [2];$row->d3 =$fr1 [3];$row->d4 =$fr1 [4];$row->d5 =$fr1 [5];
					$row->d6 =$fr1 [6];$row->d7 =$fr1 [7];$row->d8 =$fr1 [8];$row->d9 =$fr1 [9];$row->d10 =$fr1 [10];
					$row->d11 =$fr1 [11];$row->d12 =$fr1 [12];$row->d13 =$fr1 [13];$row->d14 =$fr1 [14];$row->d15 =$fr1 [15];
					$row->d16 =$fr1 [16];$row->d17 =$fr1 [17];$row->d18 =$fr1 [18];$row->d19 =$fr1 [19];$row->d20 =$fr1 [20];
					$row->d21 =$fr1 [21];$row->d22 =$fr1 [22];$row->d23 =$fr1 [23];$row->d24 =$fr1 [24];$row->d25 =$fr1 [25];
					$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";$row->d31 ="";
					$data[] = $row;
					endforeach;	*/
			//general porcentaje dia
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($cic);
			foreach ($result->result() as $row):
				$row->tec ="KPI";$ini=1;$tc=0;
				while($ini<=31){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
					$queryt=$this->db->get('r17');
					//if($queryt->num_rows()>0){
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Act) as Act');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Act =',2);$this->db->where('NumBioR !=',63);
					$query=$this->db->get('r17');
					
					
					/*$fecis=$fr1 [$ini];
					$query=$this->db->query("select count(RemisionR),(count(Adm))as Adm from r17 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=2 and NumbioR!=63");*/
					foreach($query->result() as $rowcs):
						$adm1=0;
						$d1=$rowcs->Act; 
						//$querys=$this->db->query("select count(Adm)as Adm1 from r17 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=1 and NumbioR!=63");
						$this->db->select('count(RemisionR),count(Folio),count(Act) as Act1');
						$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
						$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Act =',1);$this->db->where('NumBioR !=',63);
						$querys=$this->db->get('r17');
						 foreach($querys->result() as $rowcss):
							$act1=$rowcss->Act1;
						endforeach;	
						if($act1 > 0) $d1=($d1)+($act1*.75); 
						 switch($ini){
						 	case 1: if($d1>0 and $tt>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d1=''; break;
							case 2: if($d1>0 and $tt>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d2=''; break;
							case 3: if($d1>0 and $tt>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d3=''; break;
							case 4: if($d1>0 and $tt>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d4=''; break;
							case 5: if($d1>0 and $tt>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d5=''; break;
							case 6: if($d1>0 and $tt>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d6=''; break;
							case 7: if($d1>0 and $tt>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d7=''; break;
							case 8: if($d1>0 and $tt>0){$row->d8 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d8=''; break;
							case 9: if($d1>0 and $tt>0){$row->d9 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d9=''; break;
							case 10: if($d1>0 and $tt>0){$row->d10 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d10=''; break;
							case 11: if($d1>0 and $tt>0){$row->d11 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d11=''; break;
							case 12: if($d1>0 and $tt>0){$row->d12 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d12=''; break;
							case 13: if($d1>0 and $tt>0){$row->d13 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d13=''; break;
							case 14: if($d1>0 and $tt>0){$row->d14 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d14=''; break;
							case 15: if($d1>0 and $tt>0){$row->d15 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d15=''; break;
							case 16: if($d1>0 and $tt>0){$row->d16 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d16=''; break;
							case 17: if($d1>0 and $tt>0){$row->d17 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d17=''; break;
							case 18: if($d1>0 and $tt>0){$row->d18 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d18=''; break;
							case 19: if($d1>0 and $tt>0){$row->d19 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d19=''; break;
							case 20: if($d1>0 and $tt>0){$row->d20 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d20=''; break;
							case 21: if($d1>0 and $tt>0){$row->d21 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d21=''; break;
							case 22: if($d1>0 and $tt>0){$row->d22 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d22=''; break;
							case 23: if($d1>0 and $tt>0){$row->d23 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d23=''; break;
							case 24: if($d1>0 and $tt>0){$row->d24 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d24=''; break;
							case 25: if($d1>0 and $tt>0){$row->d25 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d25=''; break;
							case 26: if($d1>0 and $tt>0){$row->d26 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d26=''; break;
							case 27: if($d1>0 and $tt>0){$row->d27 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d27=''; break;
							case 28: if($d1>0 and $tt>0){$row->d28 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d28=''; break;
							case 29: if($d1>0 and $tt>0){$row->d29 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d29=''; break;
							case 30: if($d1>0 and $tt>0){$row->d30 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d30=''; break;
							case 31: if($d1>0 and $tt>0){$row->d31 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d31=''; break;
						 }	 
						 //busco para actualizar o agregar kpi general del dia
						 
						 if($d1>0){
							 $this->db->select('nrdad');
						 	$this->db->where('fech =',$fr1 [$ini]);$this->db->where('nadr =',8);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	if($tt>0) $por1=number_format((($d1/$tt)*100), 1, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr1 [$ini],'nadr'=>8);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }		 
					endforeach;
					//}
				  $ini+=1;	
				}//if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';							
				$data[] = $row;
				endforeach;
			return $data;	
		}	
		function getcos($filter,$cic,$mes){
			$ini=1;
			$data = array();	
			//mes
			
			$fecha='2017-'.$mes.'-01';
			while($ini<=31){
				$fr1 [$ini]=$fecha;
				$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$fecha = date ( 'Y-m-d' , $fecha ); 
				$ini+=1;				
			}		
			$ini=1;
			$datosBi [1]='1'; $datoslug [1]='ORLANDO';
			$datosBi [2]='2'; $datoslug [2]='DAVID';
			$datosBi [3]='3'; $datoslug [3]='JESUS';
			$bio=3;$cont=3;
			$nbio=$bio; 
			$nb=1;			
			while($nb<=$nbio){
			    $biolog=$datosBi [$nb];$ini=1;$entro2=0; $conttec=0;$contteccv=0; 
				$this->db->select('day(FechaR) as dia,RemisionR,CV');
				$this->db->where('Estatus <',2); $this->db->where('NumElaR =',$biolog);
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->order_by('dia','ASC');	$this->db->order_by('RemisionR', 'ASC');
				$result = $this->db->get($cic);
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1;
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$datost [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;$diac=0; $tot2=0;$cvdiat=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$datosd [$dia]=$row->dia;
						
						if($row->CV>0){$contteccv+=number_format($row->CV, 2, '.', ',');$cvdiat=$row->CV;$conttec+=1;$tot2+=1;}
						if($diac!=$dia){
							if($row->CV>0){
							$datosr [$dia]=$row->RemisionR.'<br>['.number_format($row->CV, 2, '.', ',').'] <br>';
							//$cvdiat=$cvdiat/$tot2;
							//$datost [$dia]=number_format($cvdiat, 2, '.', ',').$tot2;$cvdiat=0;$tot2=0;
							}
							else{ 
							$datosr [$dia]=$row->RemisionR;
							}
							$diac=$dia;
						}
						else{
							if($row->CV>0){
							$datosr [$dia]=$datosr [$dia].'<br>'.$row->RemisionR.'<br>['.number_format($row->CV, 2, '.', ',').'] <br>';
							//$cvdiat=$cvdiat/$tot2;
							//$datost [$dia]=number_format($cvdiat, 2, '.', ',').$tot2;$cvdiat=0;$tot2=0;
							}
							else {
							$datosr [$dia]=$datosr [$dia].'<br>'.$row->RemisionR;	
							}
						}
						$tot+=1;
					endforeach;
					
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->dc1 = $datosr [$ini];} else {  $row->dc1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->dc2 = $datosr [$ini];} else {  $row->dc2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->dc3 = $datosr [$ini];} else {  $row->dc3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->dc4 = $datosr [$ini];} else {  $row->dc4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->dc5 = $datosr [$ini];} else {  $row->dc5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->dc6 = $datosr [$ini];} else {  $row->dc6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->dc7 = $datosr [$ini];} else {  $row->dc7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->dc8 = $datosr [$ini];} else {  $row->dc8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->dc9 = $datosr [$ini];} else {  $row->dc9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->dc10 = $datosr [$ini];} else {  $row->dc10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->dc11 = $datosr [$ini];} else {  $row->dc11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->dc12 = $datosr [$ini];} else {  $row->dc12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->dc13 = $datosr [$ini];} else {  $row->dc13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->dc14 = $datosr [$ini];} else {  $row->dc14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->dc15 = $datosr [$ini];} else {  $row->dc15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->dc16 = $datosr [$ini];} else {  $row->dc16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->dc17 = $datosr [$ini];} else {  $row->dc17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->dc18 = $datosr [$ini];} else {  $row->dc18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->dc19 = $datosr [$ini];} else {  $row->dc19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->dc20 = $datosr [$ini];} else {  $row->dc20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->dc21 = $datosr [$ini];} else {  $row->dc21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->dc22 = $datosr [$ini];} else {  $row->dc22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->dc23 = $datosr [$ini];} else {  $row->dc23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->dc24 = $datosr [$ini];} else {  $row->dc24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->dc25 = $datosr [$ini];} else {  $row->dc25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->dc26 = $datosr [$ini];} else {  $row->dc26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->dc27 = $datosr [$ini];} else {  $row->dc27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->dc28 = $datosr [$ini];} else {  $row->dc28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->dc29 = $datosr [$ini];} else {  $row->dc29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->dc30 = $datosr [$ini];} else {  $row->dc30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->dc31 = $datosr [$ini];} else {  $row->dc31 ="";} break;
						}
						$ini+=1;
					}
					$uno=$datoslug [$nb].' ['.number_format(($contteccv/$conttec), 2, '.', ',').'] <br> Distribucion:'; //.$contteccv.'-'.$conttec;
					$sep='';
					//aqui sacamos la distribucion por CV					
					$query=$this->db->query("SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>0 and CV<=1 and month(FechaR)=$mes and Estatus<2
											union all SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>1 and CV<=2 and month(FechaR)=$mes and Estatus<2
											union all SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>2 and CV<=3 and month(FechaR)=$mes and Estatus<2
											union all SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>3 and CV<=4 and month(FechaR)=$mes and Estatus<2
											union all SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>4 and CV<=5 and month(FechaR)=$mes and Estatus<2
											union all SELECT sum(CV)/count(*),count(*) as tot from $cic where NumElaR=$nb and CV>5 and month(FechaR)=$mes and Estatus<2");
					$cotn=0;$cotn2=1;
					foreach ($query->result() as $rowtt):
						if($rowtt->tot>0){
						  if($cotn<5)$sep=$sep.'<br> de '.$cotn.' a '.$cotn2.' -['.number_format((($rowtt->tot/$conttec)*100), 0, '.', ',').'%]';
						  if($cotn==5)$sep=$sep.'<br> mas de '.$cotn2.' -['.number_format((($rowtt->tot/$conttec)*100), 0, '.', ',').'%]';}
						else{
						  if($cotn<5)$sep=$sep.'<br> de '.$cotn.' a '.$cotn2;
						  if($cotn==5) $sep=$sep.'<br> mas de '.$cotn2;
						}
						//
						$cotn+=1; if($cotn2<5)$cotn2+=1;
					endforeach;	
					$row->tecc=$uno.$sep;
					$data[] = $row;
					$query=$this->db->query("SELECT day(FechaR) as dia,AVG(CV) as pro from $cic where NumElaR=$nb and Estatus <=1 and CV!=0 and month(FechaR)=$mes group by FechaR");
					//$result = $this->db->get($cic);
					foreach ($query->result() as $row):
						if($row->pro>0)$datost [$row->dia] = number_format($row->pro, 2, '.', ',');
					endforeach;	
					$row->tecc='KPI';
					//
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->dc1 = $datost [$ini];} else {  $row->dc1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->dc2 = $datost [$ini];} else {  $row->dc2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->dc3 = $datost [$ini];} else {  $row->dc3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->dc4 = $datost [$ini];} else {  $row->dc4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->dc5 = $datost [$ini];} else {  $row->dc5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->dc6 = $datost [$ini];} else {  $row->dc6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->dc7 = $datost [$ini];} else {  $row->dc7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->dc8 = $datost [$ini];} else {  $row->dc8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->dc9 = $datost [$ini];} else {  $row->dc9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->dc10 = $datost [$ini];} else {  $row->dc10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->dc11 = $datost [$ini];} else {  $row->dc11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->dc12 = $datost [$ini];} else {  $row->dc12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->dc13 = $datost [$ini];} else {  $row->dc13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->dc14 = $datost [$ini];} else {  $row->dc14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->dc15 = $datost [$ini];} else {  $row->dc15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->dc16 = $datost [$ini];} else {  $row->dc16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->dc17 = $datost [$ini];} else {  $row->dc17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->dc18 = $datost [$ini];} else {  $row->dc18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->dc19 = $datost [$ini];} else {  $row->dc19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->dc20 = $datost [$ini];} else {  $row->dc20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->dc21 = $datost [$ini];} else {  $row->dc21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->dc22 = $datost [$ini];} else {  $row->dc22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->dc23 = $datost [$ini];} else {  $row->dc23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->dc24 = $datost [$ini];} else {  $row->dc24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->dc25 = $datost [$ini];} else {  $row->dc25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->dc26 = $datost [$ini];} else {  $row->dc26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->dc27 = $datost [$ini];} else {  $row->dc27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->dc28 = $datost [$ini];} else {  $row->dc28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->dc29 = $datost [$ini];} else {  $row->dc29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->dc30 = $datost [$ini];} else {  $row->dc30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->dc31 = $datost [$ini];} else {  $row->dc31 ="";} break;
						}
						$ini+=1;
					}
					
					
					$data[] = $row;
				}
				
			$nb+=1;
			}
			//kpi general cv
			$ini=1;
			while($ini<=31){$datosd [$ini]="";$datost [$ini]="";$datosg [$ini]="";$ini+=1;}
			$query=$this->db->query("SELECT day(FechaR) as dia,AVG(CV) as pro from $cic where NumElaR>0 and Estatus <=1 and CV!=0 and month(FechaR)=$mes group by FechaR");
					//$result = $this->db->get($cic);
					foreach ($query->result() as $row):
						//if($row->pro>0)
						$datosd [$row->dia]=$row->dia;
						$datost [$row->dia] = number_format($row->pro, 2, '.', ',');
						$datoscv [$row->dia] = $row->pro;
					endforeach;	
					$row->tecc='KPI Día';
					//
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->dc1 = $datost [$ini];} else {  $row->dc1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->dc2 = $datost [$ini];} else {  $row->dc2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->dc3 = $datost [$ini];} else {  $row->dc3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->dc4 = $datost [$ini];} else {  $row->dc4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->dc5 = $datost [$ini];} else {  $row->dc5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->dc6 = $datost [$ini];} else {  $row->dc6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->dc7 = $datost [$ini];} else {  $row->dc7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->dc8 = $datost [$ini];} else {  $row->dc8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->dc9 = $datost [$ini];} else {  $row->dc9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->dc10 = $datost [$ini];} else {  $row->dc10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->dc11 = $datost [$ini];} else {  $row->dc11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->dc12 = $datost [$ini];} else {  $row->dc12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->dc13 = $datost [$ini];} else {  $row->dc13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->dc14 = $datost [$ini];} else {  $row->dc14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->dc15 = $datost [$ini];} else {  $row->dc15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->dc16 = $datost [$ini];} else {  $row->dc16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->dc17 = $datost [$ini];} else {  $row->dc17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->dc18 = $datost [$ini];} else {  $row->dc18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->dc19 = $datost [$ini];} else {  $row->dc19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->dc20 = $datost [$ini];} else {  $row->dc20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->dc21 = $datost [$ini];} else {  $row->dc21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->dc22 = $datost [$ini];} else {  $row->dc22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->dc23 = $datost [$ini];} else {  $row->dc23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->dc24 = $datost [$ini];} else {  $row->dc24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->dc25 = $datost [$ini];} else {  $row->dc25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->dc26 = $datost [$ini];} else {  $row->dc26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->dc27 = $datost [$ini];} else {  $row->dc27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->dc28 = $datost [$ini];} else {  $row->dc28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->dc29 = $datost [$ini];} else {  $row->dc29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->dc30 = $datost [$ini];} else {  $row->dc30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->dc31 = $datost [$ini];} else {  $row->dc31 ="";} break;
						}
						//busco para actualizar o agregar kpi general del dia
						 if($datost [$ini]>0){
							 $this->db->select('nrdad');
						 	$this->db->where('fech =',$fr1 [$ini]);$this->db->where('nadr =',9);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	$por1=number_format($datoscv [$ini], 2, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			//$por1=$datost [$ini];
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr1 [$ini],'nadr'=>9);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }	
						$ini+=1;
					}
					
					$data[] = $row;
			return $data;	
		}	

//SELECT nad,nom from analisisdepto where ndep=17
		function getcvimp($filter,$cic){
			//SELECT NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni from biologo inner join(chofer inner join(unidad inner join(clientes inner join r17 on NumCliR=Numero) on NumUniR=NumUni) on NumChoR=NumCho) on NumBioR=NumBio order by RemisionR
			$this->db->select('NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni,NumElaR');
			$this->db->join('clientes', 'NumCliR = Numero', 'inner');
			$this->db->join('biologo', 'NumBio=NumBioR', 'inner');
			$this->db->join('chofer', 'NumCho=NumChoR', 'inner');
			$this->db->join('unidad', 'NumUni=NumUniR', 'inner');
			$this->db->where('Estatus <',2);
			if($filter['where']!='')
				{$this->db->where($filter['where']);}
			//$result = $this->db->get($cic);
			$result = $this->db->get($cic);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->FechaR1 = $fec->fecha($row->FechaR);
				$row->LocEdo = $row->Loc.' '.$row->Edo;
				if($row->NumElaR==1) $row->Ela='Orlando'; elseif($row->NumElaR==2) $row->Ela='David'; elseif($row->NumElaR==3) $row->Ela='Jesus'; else $row->Ela='';
				$fol=$row->NumRegR;
				$data[] = $row;
				//$this->db->select('NumRegR,FechaR,RemisionR,Razon,Loc,Edo,NomBio,NomCho,NomUni,NumElaR');
				$this->db->where('NFol',$fol);
				$resulta = $this->db->get('muestras');
				if($resulta->num_rows()>0){
					$promcv=0;$contcv=0;
				foreach($resulta->result() as $rowm):
					$m1=$rowm->M1;$m2=$rowm->M2;$m3=$rowm->M3;$cont=0;
					if($m1>0) $cont+=1;if($m2>0) $cont+=1;if($m3>0) $cont+=1;
					$array = array(2);
					$array= array ($m1,$m2,$m3);
					$desv=sd($array,$cont);
					
					$rowm->FechaR1='';
					$rowm->Ela='';
					$rowm->RemisionR='';
					$rowm->Razon='['.$rowm->M1.'] - ['.$rowm->M2.'] - ['.$rowm->M3.']'.' P: '.number_format((($m1+$m2+$m3)/$cont), 1, '.', ',');
					
					
					$rowm->LocEdo='C.V. - ['.number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',').']';
					$rowm->NomBio='';
					
					//$rowm->NomCho='Desv. Est.: '. number_format($desv, 2, '.', ',');
					$rowm->NomCho='';$rowm->cv=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');;
					$rowm->NomUni='';
					$promcv+=number_format((($desv/(($m1+$m2+$m3)/$cont))*100), 2, '.', ',');
					$contcv+=1;
					$data[] = $rowm;
				endforeach;
				$this->db->select('MAX(Numero)');
				$result = $this->db->get('clientes');
				foreach($result->result() as $rowt):
					$rowt->FechaR1='';
					$rowt->Ela='';
					$rowt->NomUni='';
					$rowt->RemisionR='';
					$rowt->Razon='';$rowt->LocEdo='Prom: '.number_format((((($promcv)/$contcv))), 2, '.', ',');;
					$rowt->NomBio='';$rowt->NomCho='';
					
					$rowt->cv=number_format((((($promcv)/$contcv))), 2, '.', ',');						
					$data[] = $rowt;
				endforeach;
				}
			endforeach;
			return $data;
		}
   
   }
	function sd_square($x, $mean) { return pow($x - $mean,2); } 
	function sd($array,$cont) { 
    	// square root of sum of squares devided by N-1 
		return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)) ); 
		//return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,$cont, (array_sum($array) / $cont) ) ) ) / $cont );
	} 
	
	

    	
?>