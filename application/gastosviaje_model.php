<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Gastosviaje_model extends CI_Model {
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		function getgastosviaje($filter,$ciclo,$solicitud){
			//select FechaR,RemisionR,Razon,Loc,NomBio,NomUni,NomDesS,(Ali+Com+Cas+Hos+Fit+Rep)as tot,(Rep) as Imp,Obser from clientes inner join(unidad inner join(biologo inner join(solicitud inner join r14 on NumRemS=NumRegR )on NumBio=NumBioS)on NumUni=NumUniS)on NumCliR=Numero where CarAbo=2
			//"select NumRemS,NumCliS,FechaR,RemisionR,(select Razon from clientes where Numero=NumCliS ) as Razon,(select NomBio from biologo where NumBio=NumBioS) as NomBio,(select NomCho from chofer where NumCho=NumChoS) as NomCho,(select NomUni from unidad where NumUni=NumUniS) as NomUni,(select Loc from clientes where Numero=NumCliS ) as Loc,Ali,Litros,Com,Cas,Hos,Fit,SUM(Ali+Com+Cas+Hos+Fit) as Total from r13 inner join solicitud ON NumRemS=NumRegR where CarAbo='2' and Rep=0 group by RemisionR order by Total";
			/*
			$this->db->select('FechaR,(select Razon from clientes where Numero=NumCliS ) as Razon,RemisionR,(select Alias from biologo where NumBio=NumBioS) as Alias,(select NomCho from chofer where NumCho=NumChoS) as NomCho,(select NomUni from unidad where NumUni=NumUniS) as NomUni,(select Loc from clientes where Numero=NumCliS ) as Loc,Ali,Litros,Com,Cas,Hos,Fit,SUM(Ali+Com+Cas+Hos+Fit) as tot,Obser,NumBioS,NomDesS,NumRemS,NumCliS');
			$this->db->join($solicitud, 'NumRemS=NumRegR','inner');
			$this->db->where('CarAbo',2);$this->db->where('Rep',0);
			$this->db->group_by('RemisionR');
			$this->db->order_by('tot');
			*/
			$this->db->select('FechaR,RemisionR,Razon,Loc,Alias,NomUni,NomDesS,(Ali+Com+Cas+Hos+Fit+Rep)as tot,Com,Rep,Obser,NumBioS,NumRemS,NomCho');
			$this->db->join($solicitud, 'NumRemS=NumRegR','inner');
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('biologo', 'NumBio=NumBioS','inner');
			$this->db->join('unidad', 'NumUni=NumUniS','inner');
			$this->db->join('chofer', 'NumCho=NumChoS','inner');
			$this->db->where('CarAbo',2);
			$this->db->order_by('RemisionR');
			$this->db->where('NomDesS !=','Improvistos');
			$this->db->where('NomDesS !=','Otros');
			//$this->db->group_by('NomBio');
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
				//$this->db->order_by('Razon');$this->db->order_by('NumUni');
			}
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$can=$result->num_rows();
			$data = array();$fec=new Libreria();$bio=0;$rem=0;$totact=0;$totimp=0;$tototo=0;
			if($can>0){
				//Se forma el arreglo que sera retornado							
				foreach ($result->result() as $row):
					$row->fec1 = $fec->fecha($row->FechaR);$totact+=$row->tot;
					//if($row->NomDesS!='Improvistos' && $row->NomDesS!='Otros') $row->tot ="$". number_format($row->tot, 2, '.', ','); else $row->tot ='';
					
					if($row->Com>0) $row->Com ="$". number_format($row->Com, 2, '.', ','); else $row->Com ='';
					$bio=$row->NumBioS;$rem=$row->NumRemS;$obs='';
					$obs='Rem ['.$row->RemisionR.'] '.$row->Obser;
						//se obtienen los improvistos
						$this->db->select('Rep,Obser');
						$this->db->join($solicitud, 'NumRemS=NumRegR','inner');
						$this->db->join('clientes', 'NumCliR=Numero','inner');
						$this->db->join('biologo', 'NumBio=NumBioS','inner');
						$this->db->join('unidad', 'NumUni=NumUniS','inner');
						$this->db->join('chofer', 'NumCho=NumChoS','inner');
						$this->db->where('CarAbo',2);
						//$this->db->where('Rep >',0);
						$this->db->where('NomDesS =','Improvistos');
						$this->db->where('NomDesS !=','Otros');
						$this->db->where('NumBioS',$bio);
						$this->db->where('NumRemS',$rem);
						if($filter['where']!=''){ $this->db->where($filter['where']);}
						$resultb = $this->db->get($ciclo);
						$canb=$resultb->num_rows();$obsi='';$impro=0;
						if($canb>0){
							foreach ($resultb->result() as $rowb):
								if($rowb->Rep>0){
									$obsi=$rowb->Obser; $totimp+=$rowb->Rep;$impro=$rowb->Rep;$totact+=$rowb->Rep;
									$row->Rep ="$". number_format($rowb->Rep, 2, '.', ','); 
									}else{ $row->Rep ='';}
							endforeach;		
						}else{
							$row->Rep ='';
						}
						//se obtienen los otros
						$this->db->select('sum(Rep) as Rep,Obser');
						$this->db->join($solicitud, 'NumRemS=NumRegR','inner');
						$this->db->join('clientes', 'NumCliR=Numero','inner');
						$this->db->join('biologo', 'NumBio=NumBioS','inner');
						$this->db->join('unidad', 'NumUni=NumUniS','inner');
						$this->db->join('chofer', 'NumCho=NumChoS','inner');
						$this->db->where('CarAbo',2);
						$this->db->where('Rep >',0);
						$this->db->where('NomDesS =','Otros');
						$this->db->where('NumBioS',$bio);
						$this->db->where('NumRemS',$rem);
						$this->db->group_by('NumBioS');
						if($filter['where']!=''){ $this->db->where($filter['where']);}
						$resulto = $this->db->get($ciclo);
						$cano=$resulto->num_rows();$obso='';$otros=0;
						if($cano>0){
							foreach ($resulto->result() as $rowo):
								if($rowo->Rep>0){
									$obso=$obso.' '.$rowo->Obser; 
									$otros=$rowo->Rep;$totact+=$rowo->Rep;$tototo+=$rowo->Rep;
								}	
							endforeach;		
						}
						// se obtienen las observaciones
						$this->db->select('Obser');
						$this->db->join($solicitud, 'NumRemS=NumRegR','inner');
						$this->db->join('clientes', 'NumCliR=Numero','inner');
						$this->db->join('biologo', 'NumBio=NumBioS','inner');
						$this->db->join('unidad', 'NumUni=NumUniS','inner');
						$this->db->join('chofer', 'NumCho=NumChoS','inner');
						$this->db->where('CarAbo',2);
						$this->db->where('Rep >',0);
						$this->db->where('NomDesS !=','Otros');
						$this->db->where('NumBioS',$bio);
						$this->db->where('NumRemS',$rem);
						if($filter['where']!=''){ $this->db->where($filter['where']);}
						$resultb = $this->db->get($ciclo);
						$canb=$resultb->num_rows();$obsi='';
						foreach ($resultb->result() as $rowb):
							$obsi=$obsi.' '.$rowb->Obser.' '.$obso;
						endforeach;		
						$row->Obser=$obs.' '.$obsi;$row->tot ="$". number_format(($row->tot+$impro+$otros), 2, '.', ',');
					$data[] = $row;	
				endforeach;	
				$this->db->select('max(Numero)');
				$resultZ= $this->db->get('clientes');
				foreach ($resultZ->result() as $rowZ):				
					$rowZ->fec1 = "Total :";$rowZ->Razon = "";$rowZ->Loc = "";$rowZ->Alias = "";$rowZ->NomCho =""; //
					$rowZ->NomUni = "";$rowZ->NomDesS = "";$rowZ->tot = "$".number_format($totact, 2, '.', ',');$rowZ->Com = "";$rowZ->Rep = "$".number_format($totimp, 2, '.', ',');$rowZ->Obser="";
					//$rowZ->impant ="$". number_format($totant, 2, '.', ',');
					$data[] = $rowZ;	
				endforeach;
			}
			return $data;
		}

		function getgastosviajeRes($filter,$ciclo,$solicitud){
			//select NomBio,(select Sum(Ali+Com+Cas+Hos+Fit+Rep) from solicitud where NumBioS=NumBio and CarAbo=1) as cargo,
			//(select Sum(Ali+Com+Cas+Hos+Fit+Rep) from solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS!='Improvistos') as Gasto,
			//(select Sum(Rep) from solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS='Improvistos') as Improvistos,
			//(select Sum(Rep) from solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS='Otros') as Otros 
			//from biologo inner join solicitud on NumBioS=NumBio group by NumBioS
			$query=$this->db->query("select sum(Rep) as imptot from $solicitud where CarAbo=2 and NomDesS='Improvistos'");
			$imptot=0;
			foreach ($query->result() as $row):
				$imptot=$row->imptot;
			endforeach;	
			$query=$this->db->query("select NomDesS,Com,NomBio,(select Sum(Ali+Com+Cas+Hos+Fit+Rep) from $solicitud where NumBioS=NumBio and CarAbo=1) as Cargo,
									 (select Sum(Ali+Com+Cas+Hos+Fit) from $solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS!='Improvistos' and NomDesS!='Otros') as Gasto, 
									 (select Sum(Rep) from $solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS='Improvistos') as Improvistos, 
									 (select Sum(Rep) from $solicitud where NumBioS=NumBio and CarAbo=2 and NomDesS='Otros') as Otros 
									 from biologo inner join $solicitud on NumBioS=NumBio group by NumBioS order by NomBio");
			//Se forma el arreglo que sera retornado
			$tc=0;$tg=0;$ti=0;$to=0;$tta=0;$ts=0;
			foreach ($query->result() as $row):
				$ta=0;$ta=$row->Gasto+$row->Improvistos+$row->Otros;
				$saldo=0;$saldo=$row->Cargo-$ta;				
				$tc+=$row->Cargo;$tg+=$row->Gasto;$ti+=$row->Improvistos;$to+=$row->Otros;
				if($imptot>0 && $row->Improvistos>0){$row->por = number_format(($row->Improvistos/$imptot)*100, 2, '.', ',')."%";
				$row->por1 = number_format(($row->Improvistos/$imptot)*100, 2, '.', ',');
				}else{$row->por ="";}
				if($row->Cargo>0){$row->Cargo = "$".number_format($row->Cargo, 2, '.', ',');}else{$row->Cargo ="";}
				if($row->Gasto>0){$row->Gasto = "$".number_format($row->Gasto, 2, '.', ',');}else{$row->Gasto ="";}
				if($row->Improvistos>0){$row->Improvistos = "$".number_format($row->Improvistos, 2, '.', ',');}else{$row->Improvistos ="";}
				
				if($row->Otros>0){$row->Otros = "$".number_format($row->Otros, 2, '.', ',');}else{$row->Otros ="";}
				if($ta>0){$row->totabo = "$".number_format($ta, 2, '.', ',');}else{$row->totabo ="";}
				if($saldo>0){$row->Saldo = "$".number_format($saldo, 2, '.', ',');}else{$row->Saldo ="";}
				$data[] = $row;	
			endforeach;
			$tta=$tg+$ti+$to;$ts=$tc-$tta;
			$this->db->select('max(Numero)');
			$resultZ= $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):				
				$rowZ->NomBio = "Total :";
				if($tc>0){$rowZ->Cargo = "$".number_format($tc, 2, '.', ',');}else{$rowZ->Cargo ="";}
				if($tg>0){$rowZ->Gasto = "$".number_format($tg, 2, '.', ',');}else{$rowZ->Gasto ="";}
				if($ti>0){$rowZ->Improvistos = "$".number_format($ti, 2, '.', ',');}else{$rowZ->Improvistos ="";}
				$rowZ->por = "";
				if($to>0){$rowZ->Otros = "$".number_format($to, 2, '.', ',');}else{$rowZ->Otros ="";}
				if($tta>0){$rowZ->totabo = "$".number_format($tta, 2, '.', ',');}else{$rowZ->totabo ="";}
				if($ts>0){$rowZ->Saldo = "$".number_format($ts, 2, '.', ',');}else{$rowZ->Saldo ="";}
				$data[] = $rowZ;	
			endforeach;	
			return $data;
		}
		
		function getZonasvta($filter){
			$mes=1;$ac=date("Y"); $data = array(); $tota=0;$totb=0;$totc=0;$totd=0;$tote=0;$totf=0;$totg=0;$ao1=date("Y");$ao2=date("y");$ao3=date("y")-1;
  			while($mes<=19){
  				//if($mes==0){ $nommes="-"; }
	 			if($mes==1){ $nommes="Angostura"; } if($mes==2){ $nommes="Campeche"; } if($mes==3){ $nommes="Chiapas"; } if($mes==4){ $nommes="Colima"; }
	 			if($mes==5){ $nommes="El Dorado"; } if($mes==6){ $nommes="Elota"; } if($mes==7){ $nommes="Guasave"; } if($mes==8){ $nommes="Guerrero"; }
	 			if($mes==9){ $nommes="Hermosillo"; } if($mes==10){ $nommes="Mochis"; } if($mes==11){ $nommes="Navolato"; } if($mes==12){ $nommes="Navojoa"; } 
	 			if($mes==13){ $nommes="Nayarit";} 
				if($mes==14){ $nommes="Obregon"; } if($mes==15){ $nommes="Sur Sinaloa"; } if($mes==16){ $nommes="Tamaulipas"; } 
				if($mes==17){ $nommes="Tabasco"; }
				if($mes==18){ $nommes="Veracruz";}
				if($mes==19){ $nommes="Yucatan";}
				$this->db->select('max(Numero)');
				$resultZ = $this->db->get('clientes');	
				foreach ($resultZ->result() as $rowT):
						$rowT->zon=$nommes;$uno=0;$dos=0;
						//$contador=1; 
						$ao=date("Y"); 
						//while($contador<=7){
		  				$query=$this->db->query("SELECT sum(CantidadRR) as canti From r$ao2 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->a='';
						foreach ($query->result() as $rowa):
							$var=number_format($rowa->canti, 3, '.', ','); $uno=$rowa->canti;
							if($rowa->canti>0 && $ao=$ao1){$rowT->a= $var;$tota+=$rowa->canti;}else{$rowT->a='';}
						endforeach;	
							$query=$this->db->query("select sum(Com) as canti from clientes inner join solicitud on NumCliS=Numero where CarAbo=2 AND Zona='$nommes'");
							$rowT->ac='';
							foreach ($query->result() as $rowac):
								$var='$ '.number_format($rowac->canti, 2, '.', ','); //$uno=$rowac->canti;
								if($rowac->canti>0 && $ao1='2014'){$rowT->ac= $var;$totb+=$rowac->canti;}else{$rowT->ac='';}
							endforeach;
						$ao1=$ao1-1;
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r$ao3 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->b='';
						foreach ($query->result() as $rowb):
							$var= number_format($rowb->canti, 3, '.', ','); $dos=$rowb->canti; 
							if($rowb->canti>0 && $ao=$ao1){$rowT->b= $var;$totc+=$rowb->canti;}else{$rowT->b='';}
						endforeach;	
							$query=$this->db->query("select sum(Com) as canti from clientes inner join solicitud_$ao3 on NumCliS=Numero where CarAbo=2 AND Zona='$nommes'");
							$rowT->bc='';
							foreach ($query->result() as $rowbc):
								$var='$ '.number_format($rowbc->canti, 2, '.', ','); //$uno=$rowbc->canti;
								if($rowbc->canti>0 && $ao='2013'){$rowT->bc= $var;$totd+=$rowbc->canti;}else{$rowT->bc='';}
							endforeach;
						$ao1=$ao1-1;
						if($dos>0) $rowT->por=number_format((($uno-$dos)/$dos*100), 2, '.', ',')."%";
						elseif($uno==0 && $dos==0)  $rowT->por=''; else $rowT->por='100.00%';
						$data[] = $rowT;	
				endforeach;
				$mes=$mes+=1;
			}
			$this->db->select('max(Numero)');
			$resultZ = $this->db->get('clientes');		
			foreach ($resultZ->result() as $rowT):
			$rowT->zon='Total:';$rowT->por=number_format((($tota-$totb)/$totb*100), 2, '.', ',')."%";;
			$rowT->a=number_format($tota, 3, '.', ',');
			$rowT->ac='$ '.number_format($totb, 2, '.', ',');
			$rowT->b=number_format($totc, 3, '.', ',');
			$rowT->bc='$ '.number_format($totd, 2, '.', ',');
			$data[] = $rowT;
			endforeach;
			
			return $data;
		}

		function getZonasvtacd($filter){
			$ao2=date("y");
			$this->db->select('Razon,RemisionR,NomUni,sum(Com) as Com');
			$this->db->join('solicitud', 'NumRemS=NumRegR','inner');
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			//$this->db->join('biologo', 'NumBio=NumBioS','inner');
			$this->db->join('unidad', 'NumUni=NumUniS','inner');
			//$this->db->join('chofer', 'NumCho=NumChoS','inner');
			$this->db->where('CarAbo',2);
			$this->db->where('Com >',0);
			$this->db->group_by('Razon');
			$this->db->group_by('RemisionR');
			//$this->db->group_by('RemisionR');
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
				//$this->db->order_by('Razon');$this->db->order_by('NumUni');
			}	
			$result = $this->db->get('r'.$ao2);
			$canb=$result->num_rows(); $tc=0;
			if($canb>0){
				foreach ($result->result() as $row):
					$f=explode(" ",$row->NomUni);
					$row->NomUni=$f[0]; 
					$tc+=$row->Com;
					$row->Com='$'.number_format($row->Com, 2, '.', ',');
					$data[] = $row;
				endforeach;
				$this->db->select('max(Numero)');
				$resultZ = $this->db->get('clientes');	
				foreach ($resultZ->result() as $rowT):
					$rowT->Razon='Total: 20'.$ao2;$rowT->Com='$'.number_format($tc, 2, '.', ',');;
					$rowT->RemisionR='';
					$rowT->NomUni='';
					$data[] = $rowT;
				endforeach;
			return $data;
			} else {return 0;}
		}
		function getZonasvtacd1($filter){
			$ao3=date("y")-1;
			$this->db->select('Razon,RemisionR,NomUni,sum(Com) as Com');
			$this->db->join('solicitud_'.$ao3, 'NumRemS=NumRegR','inner');
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			//$this->db->join('biologo', 'NumBio=NumBioS','inner');
			$this->db->join('unidad', 'NumUni=NumUniS','inner');
			//$this->db->join('chofer', 'NumCho=NumChoS','inner');
			$this->db->where('CarAbo',2);
			$this->db->where('Com >',0);
			$this->db->group_by('Razon');
			$this->db->group_by('RemisionR');
			//$this->db->group_by('RemisionR');
			if($filter['where']!=''){
				$this->db->where($filter['where']); 
				//$this->db->order_by('Razon');$this->db->order_by('NumUni');
			}	
			$result = $this->db->get('r'.$ao3);
			$canb=$result->num_rows(); $tc=0;
			if($canb>0){
				foreach ($result->result() as $row):
					$f=explode(" ",$row->NomUni);
					$row->NomUni=$f[0]; 
					$tc+=$row->Com;
					$row->Com='$'.number_format($row->Com, 2, '.', ',');
					$data[] = $row;
				endforeach;
				$this->db->select('max(Numero)');
				$resultZ = $this->db->get('clientes');	
				foreach ($resultZ->result() as $rowT):
					$rowT->Razon='Total: 20'.$ao3;$rowT->Com='$'.number_format($tc, 2, '.', ',');;
					$rowT->RemisionR='';
					$rowT->NomUni='';
					$data[] = $rowT;
				endforeach;
			return $data;
			} else {return 0;}
		}
    }


    
?>