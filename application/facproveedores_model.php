<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Facproveedores_model extends CI_Model {
        public $id="nrf";	
		public $fec="fec";
		public $fcap="fcap";
		public $bas="bas";
		public $mov="mov";
		public $nrp="nrp";
		public $idp="Numero";        
		public $t16="t16";
		public $t11="t11";
		public $t0="t0";
		public $exc="exc";
		public $iva="iva";
		public $isr="isr";
		public $riva="riva";
		public $fac="fac";
		public $pol="pol";
		public $apl="aplicar";
        //public $tablafacpro="fp13";
		public $tablaprov="proveedores";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getfacproveedores($filter,$est){
			//SELECT bas,mov,RFC,Razon,t16 from facproveedores inner join proveedores on Numero=nrp
			if($est==0){
				$this->db->select('nrf,fec,bas,mov,nrp,RFC,Razon,t16,t11,t0,exc,iva,isr,riva,fac,pol,aplicar');
				$this->db->join($this->tablaprov, 'Numero = nrp', 'inner');
				if($filter['where']!='') $this->db->where($filter['where']);
				//$this->db->order_by($this->fcap,'DESC');
				$result = $this->db->get($filter['num']);
				 
			}else{
				$this->db->select('bas,count(mov) as mov,RFC,Razon,sum(t16) as t16,sum(t11) as t11,sum(t0) as t0,sum(exc) as exc,sum(iva) as iva,sum(isr) as isr,sum(riva) as riva,fac,pol');
				$this->db->join($this->tablaprov, 'Numero = nrp', 'inner');
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->order_by('Razon');
				$this->db->group_by($this->nrp);
				$result = $this->db->get($filter['num']);
				//$result = $this->db->get($this->tablafacpro);
			}
			$data = array();$cont=0;
			if($result->num_rows()>0){
			$t16=0;$t11=0;$t0=0;$exc=0;$imp=0;$iva=0;$sub=0;$isr=0;$riva=0;$tot=0;	
			foreach($result->result() as $row):
				$t16+=$row->t16;$t11+=$row->t11;$t0+=$row->t0;$exc+=$row->exc;$iva+=$row->iva;$isr+=$row->isr;$riva+=$row->riva;
				$row->imp=$row->t16+$row->t11+$row->t0+$row->exc;$imp+=$row->imp;
				$row->sub=$row->imp+$row->iva;$sub+=$row->sub;
				$row->tot=$row->sub-($row->isr+$row->riva);$tot+=$row->tot;
				$row->bas1=$row->bas;
				//if($row->bas==1){$row->bas1='Bie';}if($row->bas==2){$row->bas1='Arr';}if($row->bas==3){$row->bas1='Ser';}
				if($row->t16!=0){$row->t161 ="$". number_format($row->t16, 2, '.', ',');$row->t16 =number_format($row->t16, 2, '.', ',');}else{$row->t161="";$row->t16="";}
				if($row->t11!=0){$row->t111 ="$". number_format($row->t11, 2, '.', ',');$row->t11 =number_format($row->t11, 2, '.', ',');}else{$row->t111="";$row->t11="";}
				if($row->t0!=0){$row->t01 ="$". number_format($row->t0, 2, '.', ',');$row->t0 =number_format($row->t0, 2, '.', ',');}else{$row->t01="";$row->t0="";}
				if($row->exc!=0){$row->exc1 ="$". number_format($row->exc, 2, '.', ',');$row->exc =number_format($row->exc, 2, '.', ',');}else{$row->exc1="";$row->exc="";}
				if($row->imp!=0){$row->imp ="$". number_format($row->imp, 2, '.', ',');}else{$row->imp="";}
				if($row->iva!=0){$row->iva1 ="$". number_format($row->iva, 2, '.', ',');$row->iva =number_format($row->iva, 2, '.', ',');}else{$row->iva1="";$row->iva="";}
				if($row->sub!=0){$row->sub ="$". number_format($row->sub, 2, '.', ',');}else{$row->sub="";}
				if($row->isr!=0){$row->isr1 ="$". number_format($row->isr, 2, '.', ',');$row->isr =number_format($row->isr, 2, '.', ',');}else{$row->isr1="";$row->isr="";}
				if($row->riva!=0){$row->riva1 ="$". number_format($row->riva, 2, '.', ',');$row->riva =number_format($row->riva, 2, '.', ',');}else{$row->riva1="";$row->riva="";}
				if($row->tot!=0){$row->tot ="$". number_format($row->tot, 2, '.', ',');}else{$row->tot="";}
				if($est!=0){$row->fac='';$row->pol='';}
				$row->totp=($cont+=1);
				$data[] = $row;
			endforeach;
			$this->db->select('max(nrp)');
			$result = $this->db->get($filter['num']);
			//$result = $this->db->get($this->tablafacpro); 
			foreach($result->result() as $row):
				$row->bas1='Total';$row->mov='';$row->RFC='';$row->Razon='';$row->fac='';$row->pol='';
				if($t16!=0){$row->t161 ="$". number_format($t16, 2, '.', ',');}else{$row->t161="";}
				if($t11!=0){$row->t111 ="$". number_format($t11, 2, '.', ',');}else{$row->t111="";}
				if($t0!=0){$row->t01 ="$". number_format($t0, 2, '.', ',');}else{$row->t01="";}
				if($exc!=0){$row->exc1 ="$". number_format($exc, 2, '.', ',');}else{$row->exc1="";}
				if($imp!=0){$row->imp ="$". number_format($imp, 2, '.', ',');}else{$row->imp="";}
				if($iva!=0){$row->iva1 ="$". number_format($iva, 2, '.', ',');}else{$row->iva1="";}
				if($sub!=0){$row->sub ="$". number_format($sub, 2, '.', ',');}else{$row->sub="";}
				if($isr!=0){$row->isr1 ="$". number_format($isr, 2, '.', ',');}else{$row->isr1="";}
				if($riva!=0){$row->riva1 ="$". number_format($riva, 2, '.', ',');}else{$row->riva1="";}
				if($tot!=0){$row->tot ="$". number_format($tot, 2, '.', ',');}else{$row->tot="";}
				$row->totp=($cont);
				$data[] = $row;
			endforeach;
			}
			return $data;
		}
		function getNumRowsreq($filter,$est){
			if($est==0){
				$this->db->select('nrf,fec,bas,mov,nrp,RFC,Razon,t16,t11,t0,exc,iva,isr,riva,fac,pol,aplicar');
				$this->db->join($this->tablaprov, 'Numero = nrp', 'inner');
				if($filter['where']!='') $this->db->where($filter['where']);
				//$this->db->order_by($this->fcap,'DESC');
				$result = $this->db->get($filter['num']);
				 
			}else{
				$this->db->select('bas,count(mov) as mov,RFC,Razon,sum(t16) as t16,sum(t11) as t11,sum(t0) as t0,sum(exc) as exc,sum(iva) as iva,sum(isr) as isr,sum(riva) as riva,fac,pol');
				$this->db->join($this->tablaprov, 'Numero = nrp', 'inner');
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->order_by('Razon');
				$this->db->group_by($this->nrp);
				$result = $this->db->get($filter['num']);
				//$result = $this->db->get($this->tablafacpro);
			}
			return $result->num_rows();//Se regresan la cantidad de registros encontrados 
		}
		function agregar($fec,$bas,$mov,$nrp,$t16,$t11,$t0,$exc,$iva,$isr,$riva,$fac,$pol,$ciclo,$aplicar){
			$t16 = str_replace(",", "", $t16);$t11 = str_replace(",", "", $t11);
			$t0 = str_replace(",", "", $t0);$exc = str_replace(",", "", $exc);
			$iva = str_replace(",", "", $iva);$isr = str_replace(",", "", $isr);
			$riva = str_replace(",", "", $riva);
			$hoy=date("y-m-d H:i:s");	
			if($mov!=0){
				$data=array($this->fcap=>$hoy,$this->fec=>$fec,$this->bas=>$bas,$this->mov=>$mov,$this->nrp=>$nrp,$this->t16=>$t16,$this->t11=>$t11,$this->t0=>$t0,$this->exc=>$exc,$this->iva=>$iva,$this->isr=>$isr,$this->riva=>$riva,$this->fac=>$fac,$this->pol=>$pol,$this->apl=>$aplicar);
			}else{
				$data=array($this->fcap=>$hoy,$this->fec=>$fec,$this->bas=>$bas,$this->nrp=>$nrp,$this->t16=>$t16,$this->t11=>$t11,$this->t0=>$t0,$this->exc=>$exc,$this->iva=>$iva,$this->isr=>$isr,$this->riva=>$riva,$this->fac=>$fac,$this->pol=>$pol,$this->apl=>$aplicar);
			}			
			$this->db->insert($ciclo,$data);
			//$this->db->insert($this->tablafacpro,$data);
			return $this->db->insert_id();
		}
		function actualizar($id,$fec,$bas,$mov,$nrp,$t16,$t11,$t0,$exc,$iva,$isr,$riva,$fac,$pol,$ciclo,$aplicar){
			$t16 = str_replace(",", "", $t16);$t11 = str_replace(",", "", $t11);
			$t0 = str_replace(",", "", $t0);$exc = str_replace(",", "", $exc);
			$iva = str_replace(",", "", $iva);$isr = str_replace(",", "", $isr);
			$riva = str_replace(",", "", $riva);
	 		//if($cans==0){$hoy=0;}else{$hoy=date("y-m-d H:i:s");}
			$data=array($this->fec=>$fec,$this->bas=>$bas,$this->mov=>$mov,$this->nrp=>$nrp,$this->t16=>$t16,$this->t11=>$t11,$this->t0=>$t0,$this->exc=>$exc,$this->iva=>$iva,$this->isr=>$isr,$this->riva=>$riva,$this->fac=>$fac,$this->pol=>$pol,$this->apl=>$aplicar);
			$this->db->where($this->id,$id);
			$this->db->update($ciclo,$data);
			//$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrar($id,$ciclo){
			$this->db->where($this->id,$id);
			$this->db->delete($ciclo);
			//$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function getClientes($filter){
			$this->db->select('Numero,Razon,Siglas');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaprov,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaprov);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumClientes($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tablaprov);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
}
?>