<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Programa_model extends CI_Model {        
       
		public $idpd="idpd";public $fecpd="fecpd";public $idcte="idcte";public $canpd="canpd";public $porpd="porpd";
		public $obspd="obspd";public $tipopl="tipopl";
	
		public $ids="ids";public $fecs="fecs";public $fect="fect";public $cans="cans";public $cor="corrida";
		public $sala="sala";public $ori="origens";

		public $cancm="cancm";
		
		public $idcd="idcd";public $fecd="fecd";public $cancd="cancd";public $origencd="origencd";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		//PROGRAMA DIA
		function getProgramadia($filter,$ciclo,$ori){
			$this->db->select('idpd,idcte,Razon,fecpd,canpd,porpd,obspd,tipopl,ubigra,con,tel');
			$this->db->join('clientes', 'Numero=idcte','inner');
			$this->db->order_by('tipopl');
			$this->db->order_by('Razon');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $cont=0;$tot=0;$totd=0;$conta=0;
			$conta=$result->num_rows();$pl='';$con=0;
			foreach($result->result() as $row):
				if($pl!=$row->tipopl){
					//imprimir una linea de separacion
					if($con>0){
						$this->db->select('MAX(Numero)');
						$result = $this->db->get('clientes');
						foreach($result->result() as $rowt):
							$rowt->tipopl1='';$rowt->Razon='Total:';
							$rowt->canpd1 = number_format($tot, 1, '.', ',');
							$data[] = $rowt;
						endforeach;
						$tot=0;
					}
					
					 $pl=$row->tipopl;$row->tipopl1 =$row->tipopl; $con=1;}else{$row->tipopl1='';$entro=0;$con=1;}
				
				$row->canpd1 = number_format(($row->canpd*(1+($row->porpd/100))), 1, '.', ',');
				$tot+=$row->canpd1;
				$totd+=$row->canpd1;
				$data[] = $row;
			endforeach;
			if($conta>0){
			$this->db->select('MAX(Numero)');
			$result = $this->db->get('clientes');
			foreach($result->result() as $rowt):
				$rowt->tipopl1='';$rowt->Razon='Total:';
				$rowt->canpd1 = number_format($tot, 1, '.', ',');
				$data[] = $rowt;
			endforeach;
			if($cont>0 && $ori=='Todos'){
				$this->db->select('MAX(Numero)');
				$result = $this->db->get('clientes');
				foreach($result->result() as $rowt):
					$rowt->tipopl1='Total Día:';$rowt->Razon='';
					$rowt->canpd1 = number_format($totd, 1, '.', ',');
					$data[] = $rowt;
				endforeach;
			}
			}
			return $data;
		}
		function getNumRowsPd($filter,$ciclo){
			$this->db->join('clientes', 'Numero=idcte','inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function agregara($fec,$idcte,$can,$por,$obs,$tip,$cic){
			$data=array($this->fecpd=>$fec,$this->idcte=>$idcte,$this->canpd=>$can,$this->porpd=>$por,$this->obspd=>$obs,$this->tipopl=>$tip);			
			$this->db->insert($cic,$data);
			return $this->db->insert_id();
		}
		public function actualizara($id,$fec,$idcte,$can,$por,$obs,$tip,$cic){
			$data=array($this->fecpd=>$fec,$this->idcte=>$idcte,$this->canpd=>$can,$this->porpd=>$por,$this->obspd=>$obs,$this->tipopl=>$tip);
			$this->db->where($this->idpd,$id);
			$this->db->update($cic,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function borrara($id,$ciclo){
			$this->db->where($this->idpd,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getClientes($filter){
			$this->db->order_by('Razon');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get('clientes',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			foreach($result->result() as $row):
				$data[] = $row;
			endforeach;
			return $data;
		}
		function getNumRowsC($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get('clientes');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		function getMesProg($filter,$ciclo,$tip,$mes){
			//busco el saldo del mes anterior
			$saldomesant=0.0;
			if($mes==12) {$mess=12;} elseif($mes==1){ $mess=12;} else {$mess=$mes-1;}
			if($mes<12) {
			$this->db->select('cancm');
			$this->db->where('mesc =',$mess);
			$this->db->where('origencm =',$tip);
			$results = $this->db->get('progcm_'.$ciclo);
			foreach($results->result() as $rows):
				$saldomesant = number_format($rows->cancm, 1, '.', ',');
			endforeach;
			}
			$mes1=$mes;$ci=$ciclo-2000;
			function weekday($fechas){
		 	   $fechas=str_replace("/","-",$fechas);
		    	list($dia,$mes1,$anio)=explode("-",$fechas);
		    	return (((mktime ( 0, 0, 0, $mes1, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
			}
			$ini=1; while($ini<=31){ //$datosdia [$ini]="";
			$datosdiap [$ini]="";$datosdiaf [$ini]="";$ini+=1;}
			$ini=1;$a=0;
			while($ini<=31){
				//$diaN=weekday(date("$ini/$mes/Y"));
				if($mes1==12 && $ciclo=='prog_2014') $a=date("Y")-1; else $a=date("Y"); 
				$diaN=weekday(date($ini."/".$mes1."/".$a));
				if ( $diaN == 0 ) { $dia="Lun";}
				if ( $diaN == 1 ) { $dia="Mar";}
				if ( $diaN == 2 ) { $dia="Mié";}
				if ( $diaN == 3 ) { $dia="Jue";}
				if ( $diaN == 4 ) { $dia="Vie";}
				if ( $diaN == 5 ) { $dia="Sáb";}
				if ( $diaN == 6 ) { $dia="Dom";}
				$datosdia [$ini]=$dia;
				$ini+=1;
			}
			
			$query=$this->db->query("select NumCliR,Razon,day(FechaR) as dias,Edo,Zona from r$ci inner join clientes on Numero=NumCliR where (month(FechaR)='$mes' and Origen='$tip' and Estatus<2) group by NumCliR  
									union select idcte,Razon,day(fecpd) as dia,Edo,Zona from prog_$ciclo inner join clientes on Numero=idcte where (month(fecpd)='$mes' and tipopl='$tip')  group by idcte order by Edo,Zona,Razon");
			//$this->db->select('NumCliR,Razon,day(FechaR) as dias,Edo,Zona')->from('r'.$ci)->join('clientes', 'Numero=NumCliR', 'inner')>where('month(FechaR)=',$mes)->where('Origen=',$tip)->where('Estatus<',2)->group_by('NumCliR');
			/*$this->db->select('NumCliR,Razon,day(FechaR) as dias,Edo,Zona');
			$this->db->from('r'.$ci);
			$this->db->join('clientes', 'Numero=NumCliR', 'inner');
			$this->db->where('month(FechaR)=',$mes);
			$this->db->where('Origen=',$tip);
			$this->db->where('Estatus<',2);
			$this->db->group_by('NumCliR');
			$this->db->select('idcte,Razon,day(fecpd) as dia,Edo,Zona');
			$this->db->from('prog_'.$ciclo);
			$this->db->join('clientes', 'Numero=idcte', 'inner');
			$this->db->where('month(fecpd)=',$mes);
			$this->db->where('tipopl=',$tip);
			$this->db->group_by('idcte');
			$this->db->order_by('Edo');
			$this->db->order_by('Zona');
			$this->db->order_by('Razon');
			if($filter['where']!=''){$this->db->where($filter['where']);}
			$query=$this->db->get();*/
			/* primer ejemplo
			// #1 SubQueries no.1 -------------------------------------------
			$this->db->select('title, content, date');
			$this->db->from('mytable');
			$query = $this->db->get();
			$subQuery1 = $this->db->_compile_select();
			$this->db->_reset_select();
			// #2 SubQueries no.2 -------------------------------------------
			$this->db->select('title, content, date');
			$this->db->from('mytable2');
			$query = $this->db->get();
			$subQuery2 = $this->db->_compile_select();
			$this->db->_reset_select();
			// #3 Union with Simple Manual Queries --------------------------
			$this->db->query("select * from ($subQuery1 UNION $subQuery2) as unionTable");
			// #3 (alternative) Union with another Active Record ------------
			$this->db->from("($subQuery1 UNION $subQuery2)");
			$this->db->get();
			*/ 
			
			/* segundo ejemplo
			// Query #1
			$this->db->select('title, content, date');
			$this->db->from('mytable1');
			$query1 = $this->db->get()->result();
			// Query #2
			$this->db->select('title, content, date');
			$this->db->from('mytable2');
			$query2 = $this->db->get()->result();
			// Merge both query results
			$query = array_merge($query1, $query2);
			*/
			 
			 /* tercer ejemplo
			$query1 = $this->db->get('Example_Table1');
			$join1 = $this->db->last_query();
			$query2 = $this->db->get('Example_Table2');
			$join2 = $this->db->last_query();
			$union_query = $this->db->query($join1.' UNION '.$join2.' ORDER BY column1,column2);
			*/
			
			$data = array();
			$cte=0;$aux=0;	$paso=0;	$tdp=0;$tdf=0; $salmes=0;
			$cantidad=$query->num_rows();
			if($cantidad>0){
				//dia de semana
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Cliente';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: $row->d1=$datosdia [$ini]; break;
							case 2: $row->d2=$datosdia [$ini]; break;
							case 3: $row->d3=$datosdia [$ini]; break;
							case 4: $row->d4=$datosdia [$ini]; break;
							case 5: $row->d5=$datosdia [$ini]; break;
							case 6: $row->d6=$datosdia [$ini]; break;
							case 7: $row->d7=$datosdia [$ini]; break;
							case 8: $row->d8=$datosdia [$ini]; break;
							case 9: $row->d9=$datosdia [$ini]; break;
							case 10: $row->d10=$datosdia [$ini]; break;
							case 11: $row->d11=$datosdia [$ini]; break;
							case 12: $row->d12=$datosdia [$ini]; break;
							case 13: $row->d13=$datosdia [$ini]; break;
							case 14: $row->d14=$datosdia [$ini]; break;
							case 15: $row->d15=$datosdia [$ini]; break;
							case 16: $row->d16=$datosdia [$ini]; break;
							case 17: $row->d17=$datosdia [$ini]; break;
							case 18: $row->d18=$datosdia [$ini]; break;
							case 19: $row->d19=$datosdia [$ini]; break;
							case 20: $row->d20=$datosdia [$ini]; break;
							case 21: $row->d21=$datosdia [$ini]; break;
							case 22: $row->d22=$datosdia [$ini]; break;
							case 23: $row->d23=$datosdia [$ini]; break;
							case 24: $row->d24=$datosdia [$ini]; break;
							case 25: $row->d25=$datosdia [$ini]; break;
							case 26: $row->d26=$datosdia [$ini]; break;
							case 27: $row->d27=$datosdia [$ini]; break;
							case 28: $row->d28=$datosdia [$ini]; break;
							case 29: $row->d29=$datosdia [$ini]; break;
							case 30: $row->d30=$datosdia [$ini]; break;
							case 31: $row->d31=$datosdia [$ini]; break;
						}
						$ini+=1;
					}
					$row->tdc='TOT'; 
					$data[] = $row;	
 				endforeach;	
			foreach($query->result() as $row):
				$cte=$row->NumCliR;$paso=1;
				if($cte!=$aux){
					$aux=$row->NumCliR;
					//$row->Razon=$row->Razon.$cte;
					$ini=1; 
					while($ini<=31){
						$datosd [$ini]="";$datosr [$ini]="";$datose [$ini]="";
						$datosfs [$ini]="";$datossa [$ini]="";$datossc [$ini]="";$datossp [$ini]="";$datosspe [$ini]="";$datossds [$ini]="";
						$datoscor [$ini]="";
						$ini+=1;
					}
					$tdc=0;$ini=0;$cte=0;$diab=1; $totp=0; $tote=0;
				while($diab<=31){
					//$dia=$row->dias;$raz=$row->Razon;
					//primero obtengo lo programada segun el dia y el cliente
					$this->db->select('Razon,day(fecpd) as dia,month(fecpd) as mes,obspd,((canpd*(1+(porpd/100))))as can');
					$this->db->join('clientes', 'Numero=idcte', 'inner');
					//if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('idcte =',$aux);
					$this->db->where('month(fecpd) =',$mes);
					$this->db->where('day(fecpd) =',$diab);
					$this->db->where('tipopl =',$tip);
					$resultC = $this->db->get('prog_'.$ciclo);$entra=0;
					foreach ($resultC->result() as $rowc):
						if($rowc->obspd!=''){$row->Razon=$rowc->Razon.'<br> '.' ['.$rowc->obspd.']';}
						$ini=$rowc->dia;$datosd [$ini]=$ini;$datosdiap [$ini]+=number_format($rowc->can, 1, '.', ',');$tdp+=number_format($rowc->can, 1, '.', ',');
						$datosr [$ini]=number_format($rowc->can, 1, '.', ',');$datose [$ini]='-';$entra=1;$totp+=number_format($rowc->can, 1, '.', ',');
					endforeach;	
					//segundo obtengo lo facturado segun el dia y el cliente
					$this->db->select('day(FechaR) as dia,month(FechaR) as mes,(sum(CantidadRR)/1000)as can');
					$this->db->join('clientes', 'Numero=NumCliR', 'inner');
					//if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('NumCliR =',$aux);
					$this->db->where('month(FechaR) =',$mes);
					$this->db->where('day(FechaR) =',$diab);
					$this->db->where('Origen =',$tip);
					$this->db->where('Estatus <',2);
					$resultF = $this->db->get('r'.$ci);
					foreach ($resultF->result() as $rowf):
						$ini=$rowf->dia;$datosd [$ini]=$ini;
						if($ini>=1 && $ini<=31) $datosdiaf [$ini]+=number_format($rowf->can, 1, '.', ',');
						$tdf+=number_format($rowf->can, 1, '.', ',');
						$datose [$ini]=number_format($rowf->can, 1, '.', ',');$tote+=number_format($rowf->can, 1, '.', ',');
						if($entra==0){$datosr [$ini]='-';}
					endforeach;
					//tercero obtengo los dias de siembra de la sala, sala,corrida y produccion estimada
					$this->db->select('day(fect) as dia,month(fect) as mes,sala,corrida,fecs,fect,cans,origens');
					$this->db->where('month(fect) =',$mes);
					$this->db->where('day(fect) =',$diab);
					$this->db->where('origens =',$tip);
					$resultS = $this->db->get('progs_'.$ciclo);$fec=new Libreria();
					foreach ($resultS->result() as $rows):
						$ini=$rows->dia;$datosfs [$ini]=$fec->fecha22($rows->fecs);
						$datossa [$ini]=$rows->sala;$datossc [$ini]=$rows->corrida;
						$datossp [$ini]=number_format($rows->cans, 1, '.', ',');$datosspe [$ini]=number_format($rows->cans, 1, '.', ',');
						//if($ini>=1 && $ini<=31) $datosdiaf [$ini]+=number_format($rowf->can, 1, '.', ',');
						//$tdf+=number_format($rowf->can, 1, '.', ',');
						//$datose [$ini]=number_format($rowf->can, 1, '.', ',');$tote+=number_format($rowf->can, 1, '.', ',');
						//if($entra==0){$datosr [$ini]='-';}
					endforeach;
					$diab=$diab+1;	
				}	
				$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini].'<br>'.$datose [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					if($totp==0) $totp='-'; if($tote==0) $tote='-';
					$row->tdc=$totp.'<br>'.$tote; 
					$data[] = $row;
				}
			endforeach;	
			//total por dia programado;
				//select day(fecpd),(sum((canpd*(1+(porpd/100)))))as can from clientes inner join prog_2014 on idcte=Numero where month(fecpd)=8 group by day(fecpd)
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Programado';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosdiap [$ini]>0){ $row->d1=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d1="";break;}
							case 2: if($datosdiap [$ini]>0){ $row->d2=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d2="";break;}
							case 3: if($datosdiap [$ini]>0){ $row->d3=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d3="";break;}
							case 4: if($datosdiap [$ini]>0){ $row->d4=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d4="";break;}
							case 5: if($datosdiap [$ini]>0){ $row->d5=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d5="";break;}
							case 6: if($datosdiap [$ini]>0){ $row->d6=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d6="";break;}
							case 7: if($datosdiap [$ini]>0){ $row->d7=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d7="";break;}
							case 8: if($datosdiap [$ini]>0){ $row->d8=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d8="";break;}
							case 9: if($datosdiap [$ini]>0){ $row->d9=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d9="";break;}
							case 10: if($datosdiap [$ini]>0){ $row->d10=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d10="";break;}
							case 11: if($datosdiap [$ini]>0){ $row->d11=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d11="";break;}
							case 12: if($datosdiap [$ini]>0){ $row->d12=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d12="";break;}
							case 13: if($datosdiap [$ini]>0){ $row->d13=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d13="";break;}
							case 14: if($datosdiap [$ini]>0){ $row->d14=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d14="";break;}
							case 15: if($datosdiap [$ini]>0){ $row->d15=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d15="";break;}
							case 16: if($datosdiap [$ini]>0){ $row->d16=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d16="";break;}
							case 17: if($datosdiap [$ini]>0){ $row->d17=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d17="";break;}
							case 18: if($datosdiap [$ini]>0){ $row->d18=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d18="";break;}
							case 19: if($datosdiap [$ini]>0){ $row->d19=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d19="";break;}
							case 20: if($datosdiap [$ini]>0){ $row->d20=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d20="";break;}
							case 21: if($datosdiap [$ini]>0){ $row->d21=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d21="";break;}
							case 22: if($datosdiap [$ini]>0){ $row->d22=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d22="";break;}
							case 23: if($datosdiap [$ini]>0){ $row->d23=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d23="";break;}
							case 24: if($datosdiap [$ini]>0){ $row->d24=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d24="";break;}
							case 25: if($datosdiap [$ini]>0){ $row->d25=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d25="";break;}
							case 26: if($datosdiap [$ini]>0){ $row->d26=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d26="";break;}
							case 27: if($datosdiap [$ini]>0){ $row->d27=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d27="";break;}
							case 28: if($datosdiap [$ini]>0){ $row->d28=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d28="";break;}
							case 29: if($datosdiap [$ini]>0){ $row->d29=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d29="";break;}
							case 30: if($datosdiap [$ini]>0){ $row->d30=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d30="";break;}
							case 31: if($datosdiap [$ini]>0){ $row->d31=number_format($datosdiap [$ini], 1, '.', ',');$datosspe [$ini]=number_format($datossp [$ini]-$datosdiap [$ini], 1, '.', ',');} else {$row->d31="";break;}
						}
						$ini+=1;
					}
					//$row->tdc=$tdp;
					$row->tdc=number_format($tdp, 1, '.', ','); 
					
					$data[] = $row;	
			 endforeach;
					//total por dia facturado;
				//select day(fecpd),(sum((canpd*(1+(porpd/100)))))as can from clientes inner join prog_2014 on idcte=Numero where month(fecpd)=8 group by day(fecpd)
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Remisionado';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosdiaf [$ini]>0) $row->d1=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d1="";break;
							case 2: if($datosdiaf [$ini]>0) $row->d2=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d2="";break;
							case 3: if($datosdiaf [$ini]>0) $row->d3=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d3="";break;
							case 4: if($datosdiaf [$ini]>0) $row->d4=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d4="";break;
							case 5: if($datosdiaf [$ini]>0) $row->d5=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d5="";break;
							case 6: if($datosdiaf [$ini]>0) $row->d6=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d6="";break;
							case 7: if($datosdiaf [$ini]>0) $row->d7=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d7="";break;
							case 8: if($datosdiaf [$ini]>0) $row->d8=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d8="";break;
							case 9: if($datosdiaf [$ini]>0) $row->d9=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d9="";break;
							case 10: if($datosdiaf [$ini]>0) $row->d10=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d10="";break;
							case 11: if($datosdiaf [$ini]>0) $row->d11=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d11="";break;
							case 12: if($datosdiaf [$ini]>0) $row->d12=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d12="";break;
							case 13: if($datosdiaf [$ini]>0) $row->d13=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d13="";break;
							case 14: if($datosdiaf [$ini]>0) $row->d14=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d14="";break;
							case 15: if($datosdiaf [$ini]>0) $row->d15=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d15="";break;
							case 16: if($datosdiaf [$ini]>0) $row->d16=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d16="";break;
							case 17: if($datosdiaf [$ini]>0) $row->d17=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d17="";break;
							case 18: if($datosdiaf [$ini]>0) $row->d18=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d18="";break;
							case 19: if($datosdiaf [$ini]>0) $row->d19=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d19="";break;
							case 20: if($datosdiaf [$ini]>0) $row->d20=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d20="";break;
							case 21: if($datosdiaf [$ini]>0) $row->d21=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d21="";break;
							case 22: if($datosdiaf [$ini]>0) $row->d22=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d22="";break;
							case 23: if($datosdiaf [$ini]>0) $row->d23=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d23="";break;
							case 24: if($datosdiaf [$ini]>0) $row->d24=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d24="";break;
							case 25: if($datosdiaf [$ini]>0) $row->d25=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d25="";break;
							case 26: if($datosdiaf [$ini]>0) $row->d26=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d26="";break;
							case 27: if($datosdiaf [$ini]>0) $row->d27=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d27="";break;
							case 28: if($datosdiaf [$ini]>0) $row->d28=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d28="";break;
							case 29: if($datosdiaf [$ini]>0) $row->d29=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d29="";break;
							case 30: if($datosdiaf [$ini]>0) $row->d30=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d30="";break;
							case 31: if($datosdiaf [$ini]>0) $row->d31=number_format($datosdiaf [$ini], 1, '.', ','); else $row->d31="";break;
						}
						$ini+=1;
					}
					//$row->tdc=$tdf; 
					$row->tdc=number_format($tdf, 1, '.', ',');
					$data[] = $row;	
			 endforeach;	
			 //fechas de siembra;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Fecha de siembra';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosfs [$ini]>0) $row->d1=$datosfs [$ini]; else $row->d1="";break;
							case 2: if($datosfs [$ini]>0) $row->d2=$datosfs [$ini]; else $row->d2="";break;
							case 3: if($datosfs [$ini]>0) $row->d3=$datosfs [$ini]; else $row->d3="";break;
							case 4: if($datosfs [$ini]>0) $row->d4=$datosfs [$ini]; else $row->d4="";break;
							case 5: if($datosfs [$ini]>0) $row->d5=$datosfs [$ini]; else $row->d5="";break;
							case 6: if($datosfs [$ini]>0) $row->d6=$datosfs [$ini]; else $row->d6="";break;
							case 7: if($datosfs [$ini]>0) $row->d7=$datosfs [$ini]; else $row->d7="";break;
							case 8: if($datosfs [$ini]>0) $row->d8=$datosfs [$ini]; else $row->d8="";break;
							case 9: if($datosfs [$ini]>0) $row->d9=$datosfs [$ini]; else $row->d9="";break;
							case 10: if($datosfs [$ini]>0) $row->d10=$datosfs [$ini]; else $row->d10="";break;
							case 11: if($datosfs [$ini]>0) $row->d11=$datosfs [$ini]; else $row->d11="";break;
							case 12: if($datosfs [$ini]>0) $row->d12=$datosfs [$ini]; else $row->d12="";break;
							case 13: if($datosfs [$ini]>0) $row->d13=$datosfs [$ini]; else $row->d13="";break;
							case 14: if($datosfs [$ini]>0) $row->d14=$datosfs [$ini]; else $row->d14="";break;
							case 15: if($datosfs [$ini]>0) $row->d15=$datosfs [$ini]; else $row->d15="";break;
							case 16: if($datosfs [$ini]>0) $row->d16=$datosfs [$ini]; else $row->d16="";break;
							case 17: if($datosfs [$ini]>0) $row->d17=$datosfs [$ini]; else $row->d17="";break;
							case 18: if($datosfs [$ini]>0) $row->d18=$datosfs [$ini]; else $row->d18="";break;
							case 19: if($datosfs [$ini]>0) $row->d19=$datosfs [$ini]; else $row->d19="";break;
							case 20: if($datosfs [$ini]>0) $row->d20=$datosfs [$ini]; else $row->d20="";break;
							case 21: if($datosfs [$ini]>0) $row->d21=$datosfs [$ini]; else $row->d21="";break;
							case 22: if($datosfs [$ini]>0) $row->d22=$datosfs [$ini]; else $row->d22="";break;
							case 23: if($datosfs [$ini]>0) $row->d23=$datosfs [$ini]; else $row->d23="";break;
							case 24: if($datosfs [$ini]>0) $row->d24=$datosfs [$ini]; else $row->d24="";break;
							case 25: if($datosfs [$ini]>0) $row->d25=$datosfs [$ini]; else $row->d25="";break;
							case 26: if($datosfs [$ini]>0) $row->d26=$datosfs [$ini]; else $row->d26="";break;
							case 27: if($datosfs [$ini]>0) $row->d27=$datosfs [$ini]; else $row->d27="";break;
							case 28: if($datosfs [$ini]>0) $row->d28=$datosfs [$ini]; else $row->d28="";break;
							case 29: if($datosfs [$ini]>0) $row->d29=$datosfs [$ini]; else $row->d29="";break;
							case 30: if($datosfs [$ini]>0) $row->d30=$datosfs [$ini]; else $row->d30="";break;
							case 31: if($datosfs [$ini]>0) $row->d31=$datosfs [$ini]; else $row->d31="";break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;
			//salas;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Sala';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datossa [$ini]>0) $row->d1=$datossa [$ini]; else $row->d1="";break;
							case 2: if($datossa [$ini]>0) $row->d2=$datossa [$ini]; else $row->d2="";break;
							case 3: if($datossa [$ini]>0) $row->d3=$datossa [$ini]; else $row->d3="";break;
							case 4: if($datossa [$ini]>0) $row->d4=$datossa [$ini]; else $row->d4="";break;
							case 5: if($datossa [$ini]>0) $row->d5=$datossa [$ini]; else $row->d5="";break;
							case 6: if($datossa [$ini]>0) $row->d6=$datossa [$ini]; else $row->d6="";break;
							case 7: if($datossa [$ini]>0) $row->d7=$datossa [$ini]; else $row->d7="";break;
							case 8: if($datossa [$ini]>0) $row->d8=$datossa [$ini]; else $row->d8="";break;
							case 9: if($datossa [$ini]>0) $row->d9=$datossa [$ini]; else $row->d9="";break;
							case 10: if($datossa [$ini]>0) $row->d10=$datossa [$ini]; else $row->d10="";break;
							case 11: if($datossa [$ini]>0) $row->d11=$datossa [$ini]; else $row->d11="";break;
							case 12: if($datossa [$ini]>0) $row->d12=$datossa [$ini]; else $row->d12="";break;
							case 13: if($datossa [$ini]>0) $row->d13=$datossa [$ini]; else $row->d13="";break;
							case 14: if($datossa [$ini]>0) $row->d14=$datossa [$ini]; else $row->d14="";break;
							case 15: if($datossa [$ini]>0) $row->d15=$datossa [$ini]; else $row->d15="";break;
							case 16: if($datossa [$ini]>0) $row->d16=$datossa [$ini]; else $row->d16="";break;
							case 17: if($datossa [$ini]>0) $row->d17=$datossa [$ini]; else $row->d17="";break;
							case 18: if($datossa [$ini]>0) $row->d18=$datossa [$ini]; else $row->d18="";break;
							case 19: if($datossa [$ini]>0) $row->d19=$datossa [$ini]; else $row->d19="";break;
							case 20: if($datossa [$ini]>0) $row->d20=$datossa [$ini]; else $row->d20="";break;
							case 21: if($datossa [$ini]>0) $row->d21=$datossa [$ini]; else $row->d21="";break;
							case 22: if($datossa [$ini]>0) $row->d22=$datossa [$ini]; else $row->d22="";break;
							case 23: if($datossa [$ini]>0) $row->d23=$datossa [$ini]; else $row->d23="";break;
							case 24: if($datossa [$ini]>0) $row->d24=$datossa [$ini]; else $row->d24="";break;
							case 25: if($datossa [$ini]>0) $row->d25=$datossa [$ini]; else $row->d25="";break;
							case 26: if($datossa [$ini]>0) $row->d26=$datossa [$ini]; else $row->d26="";break;
							case 27: if($datossa [$ini]>0) $row->d27=$datossa [$ini]; else $row->d27="";break;
							case 28: if($datossa [$ini]>0) $row->d28=$datossa [$ini]; else $row->d28="";break;
							case 29: if($datossa [$ini]>0) $row->d29=$datossa [$ini]; else $row->d29="";break;
							case 30: if($datossa [$ini]>0) $row->d30=$datossa [$ini]; else $row->d30="";break;
							case 31: if($datossa [$ini]>0) $row->d31=$datossa [$ini]; else $row->d31="";break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;	
			//corrida;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Corrida';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datossc [$ini]>0) $row->d1=$datossc [$ini]; else $row->d1="";break;
							case 2: if($datossc [$ini]>0) $row->d2=$datossc [$ini]; else $row->d2="";break;
							case 3: if($datossc [$ini]>0) $row->d3=$datossc [$ini]; else $row->d3="";break;
							case 4: if($datossc [$ini]>0) $row->d4=$datossc [$ini]; else $row->d4="";break;
							case 5: if($datossc [$ini]>0) $row->d5=$datossc [$ini]; else $row->d5="";break;
							case 6: if($datossc [$ini]>0) $row->d6=$datossc [$ini]; else $row->d6="";break;
							case 7: if($datossc [$ini]>0) $row->d7=$datossc [$ini]; else $row->d7="";break;
							case 8: if($datossc [$ini]>0) $row->d8=$datossc [$ini]; else $row->d8="";break;
							case 9: if($datossc [$ini]>0) $row->d9=$datossc [$ini]; else $row->d9="";break;
							case 10: if($datossc [$ini]>0) $row->d10=$datossc [$ini]; else $row->d10="";break;
							case 11: if($datossc [$ini]>0) $row->d11=$datossc [$ini]; else $row->d11="";break;
							case 12: if($datossc [$ini]>0) $row->d12=$datossc [$ini]; else $row->d12="";break;
							case 13: if($datossc [$ini]>0) $row->d13=$datossc [$ini]; else $row->d13="";break;
							case 14: if($datossc [$ini]>0) $row->d14=$datossc [$ini]; else $row->d14="";break;
							case 15: if($datossc [$ini]>0) $row->d15=$datossc [$ini]; else $row->d15="";break;
							case 16: if($datossc [$ini]>0) $row->d16=$datossc [$ini]; else $row->d16="";break;
							case 17: if($datossc [$ini]>0) $row->d17=$datossc [$ini]; else $row->d17="";break;
							case 18: if($datossc [$ini]>0) $row->d18=$datossc [$ini]; else $row->d18="";break;
							case 19: if($datossc [$ini]>0) $row->d19=$datossc [$ini]; else $row->d19="";break;
							case 20: if($datossc [$ini]>0) $row->d20=$datossc [$ini]; else $row->d20="";break;
							case 21: if($datossc [$ini]>0) $row->d21=$datossc [$ini]; else $row->d21="";break;
							case 22: if($datossc [$ini]>0) $row->d22=$datossc [$ini]; else $row->d22="";break;
							case 23: if($datossc [$ini]>0) $row->d23=$datossc [$ini]; else $row->d23="";break;
							case 24: if($datossc [$ini]>0) $row->d24=$datossc [$ini]; else $row->d24="";break;
							case 25: if($datossc [$ini]>0) $row->d25=$datossc [$ini]; else $row->d25="";break;
							case 26: if($datossc [$ini]>0) $row->d26=$datossc [$ini]; else $row->d26="";break;
							case 27: if($datossc [$ini]>0) $row->d27=$datossc [$ini]; else $row->d27="";break;
							case 28: if($datossc [$ini]>0) $row->d28=$datossc [$ini]; else $row->d28="";break;
							case 29: if($datossc [$ini]>0) $row->d29=$datossc [$ini]; else $row->d29="";break;
							case 30: if($datossc [$ini]>0) $row->d30=$datossc [$ini]; else $row->d30="";break;
							case 31: if($datossc [$ini]>0) $row->d31=$datossc [$ini]; else $row->d31="";break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;	
			//produccion estimada;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Producción Estimada';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datossp [$ini]>0) $row->d1=$datossp [$ini]; else $row->d1="";break;
							case 2: if($datossp [$ini]>0) $row->d2=$datossp [$ini]; else $row->d2="";break;
							case 3: if($datossp [$ini]>0) $row->d3=$datossp [$ini]; else $row->d3="";break;
							case 4: if($datossp [$ini]>0) $row->d4=$datossp [$ini]; else $row->d4="";break;
							case 5: if($datossp [$ini]>0) $row->d5=$datossp [$ini]; else $row->d5="";break;
							case 6: if($datossp [$ini]>0) $row->d6=$datossp [$ini]; else $row->d6="";break;
							case 7: if($datossp [$ini]>0) $row->d7=$datossp [$ini]; else $row->d7="";break;
							case 8: if($datossp [$ini]>0) $row->d8=$datossp [$ini]; else $row->d8="";break;
							case 9: if($datossp [$ini]>0) $row->d9=$datossp [$ini]; else $row->d9="";break;
							case 10: if($datossp [$ini]>0) $row->d10=$datossp [$ini]; else $row->d10="";break;
							case 11: if($datossp [$ini]>0) $row->d11=$datossp [$ini]; else $row->d11="";break;
							case 12: if($datossp [$ini]>0) $row->d12=$datossp [$ini]; else $row->d12="";break;
							case 13: if($datossp [$ini]>0) $row->d13=$datossp [$ini]; else $row->d13="";break;
							case 14: if($datossp [$ini]>0) $row->d14=$datossp [$ini]; else $row->d14="";break;
							case 15: if($datossp [$ini]>0) $row->d15=$datossp [$ini]; else $row->d15="";break;
							case 16: if($datossp [$ini]>0) $row->d16=$datossp [$ini]; else $row->d16="";break;
							case 17: if($datossp [$ini]>0) $row->d17=$datossp [$ini]; else $row->d17="";break;
							case 18: if($datossp [$ini]>0) $row->d18=$datossp [$ini]; else $row->d18="";break;
							case 19: if($datossp [$ini]>0) $row->d19=$datossp [$ini]; else $row->d19="";break;
							case 20: if($datossp [$ini]>0) $row->d20=$datossp [$ini]; else $row->d20="";break;
							case 21: if($datossp [$ini]>0) $row->d21=$datossp [$ini]; else $row->d21="";break;
							case 22: if($datossp [$ini]>0) $row->d22=$datossp [$ini]; else $row->d22="";break;
							case 23: if($datossp [$ini]>0) $row->d23=$datossp [$ini]; else $row->d23="";break;
							case 24: if($datossp [$ini]>0) $row->d24=$datossp [$ini]; else $row->d24="";break;
							case 25: if($datossp [$ini]>0) $row->d25=$datossp [$ini]; else $row->d25="";break;
							case 26: if($datossp [$ini]>0) $row->d26=$datossp [$ini]; else $row->d26="";break;
							case 27: if($datossp [$ini]>0) $row->d27=$datossp [$ini]; else $row->d27="";break;
							case 28: if($datossp [$ini]>0) $row->d28=$datossp [$ini]; else $row->d28="";break;
							case 29: if($datossp [$ini]>0) $row->d29=$datossp [$ini]; else $row->d29="";break;
							case 30: if($datossp [$ini]>0) $row->d30=$datossp [$ini]; else $row->d30="";break;
							case 31: if($datossp [$ini]>0) $row->d31=$datossp [$ini]; else $row->d31="";break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;	
			//produccion menos entrega;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Producción menos entrega';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datossa [$ini]>0) $row->d1=$datosspe [$ini]; else $row->d1="";break;
							case 2: if($datossa [$ini]>0) $row->d2=$datosspe [$ini]; else $row->d2="";break;
							case 3: if($datossa [$ini]>0) $row->d3=$datosspe [$ini]; else $row->d3="";break;
							case 4: if($datossa [$ini]>0) $row->d4=$datosspe [$ini]; else $row->d4="";break;
							case 5: if($datossa [$ini]>0) $row->d5=$datosspe [$ini]; else $row->d5="";break;
							case 6: if($datossa [$ini]>0) $row->d6=$datosspe [$ini]; else $row->d6="";break;
							case 7: if($datossa [$ini]>0) $row->d7=$datosspe [$ini]; else $row->d7="";break;
							case 8: if($datossa [$ini]>0) $row->d8=$datosspe [$ini]; else $row->d8="";break;
							case 9: if($datossa [$ini]>0) $row->d9=$datosspe [$ini]; else $row->d9="";break;
							case 10: if($datossa [$ini]>0) $row->d10=$datosspe [$ini]; else $row->d10="";break;
							case 11: if($datossa [$ini]>0) $row->d11=$datosspe [$ini]; else $row->d11="";break;
							case 12: if($datossa [$ini]>0) $row->d12=$datosspe [$ini]; else $row->d12="";break;
							case 13: if($datossa [$ini]>0) $row->d13=$datosspe [$ini]; else $row->d13="";break;
							case 14: if($datossa [$ini]>0) $row->d14=$datosspe [$ini]; else $row->d14="";break;
							case 15: if($datossa [$ini]>0) $row->d15=$datosspe [$ini]; else $row->d15="";break;
							case 16: if($datossa [$ini]>0) $row->d16=$datosspe [$ini]; else $row->d16="";break;
							case 17: if($datossa [$ini]>0) $row->d17=$datosspe [$ini]; else $row->d17="";break;
							case 18: if($datossa [$ini]>0) $row->d18=$datosspe [$ini]; else $row->d18="";break;
							case 19: if($datossa [$ini]>0) $row->d19=$datosspe [$ini]; else $row->d19="";break;
							case 20: if($datossa [$ini]>0) $row->d20=$datosspe [$ini]; else $row->d20="";break;
							case 21: if($datossa [$ini]>0) $row->d21=$datosspe [$ini]; else $row->d21="";break;
							case 22: if($datossa [$ini]>0) $row->d22=$datosspe [$ini]; else $row->d22="";break;
							case 23: if($datossa [$ini]>0) $row->d23=$datosspe [$ini]; else $row->d23="";break;
							case 24: if($datossa [$ini]>0) $row->d24=$datosspe [$ini]; else $row->d24="";break;
							case 25: if($datossa [$ini]>0) $row->d25=$datosspe [$ini]; else $row->d25="";break;
							case 26: if($datossa [$ini]>0) $row->d26=$datosspe [$ini]; else $row->d26="";break;
							case 27: if($datossa [$ini]>0) $row->d27=$datosspe [$ini]; else $row->d27="";break;
							case 28: if($datossa [$ini]>0) $row->d28=$datosspe [$ini]; else $row->d28="";break;
							case 29: if($datossa [$ini]>0) $row->d29=$datosspe [$ini]; else $row->d29="";break;
							case 30: if($datossa [$ini]>0) $row->d30=$datosspe [$ini]; else $row->d30="";break;
							case 31: if($datossa [$ini]>0) $row->d31=$datosspe [$ini]; else $row->d31="";break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;	
			//obtiene los datos del corte
				$this->db->select('day(fecd) as dia,cancd');
				$this->db->where('month(fecd) =',$mes);
				$this->db->where('origencd =',$tip);
				//if($filter['where']!=''){$this->db->where($filter['where']);}
				$resultS = $this->db->get('progcd_'.$ciclo); $ini=0;
				foreach ($resultS->result() as $rows):
					$ini=$rows->dia;
					$datoscor [$ini]=number_format($rows->cancd, 1, '.', ',');
				endforeach;		
			//deficit o superavit;
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Deficit o Superavit';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: $row->d1=number_format($saldomesant-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 2: $row->d2=number_format($row->d1-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 3: $row->d3=number_format($row->d2-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 4: $row->d4=number_format($row->d3-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 5: $row->d5=number_format($row->d4-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 6: $row->d6=number_format($row->d5-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 7: $row->d7=number_format($row->d6-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 8: $row->d8=number_format($row->d7-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 9: $row->d9=number_format($row->d8-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 10: $row->d10=number_format($row->d9-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 11: $row->d11=number_format($row->d10-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 12: $row->d12=number_format($row->d11-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 13: $row->d13=number_format($row->d12-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 14: $row->d14=number_format($row->d13-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 15: $row->d15=number_format($row->d14-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 16: $row->d16=number_format($row->d15-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 17: $row->d17=number_format($row->d16-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 18: $row->d18=number_format($row->d17-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 19: $row->d19=number_format($row->d18-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 20: $row->d20=number_format($row->d19-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 21: $row->d21=number_format($row->d20-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 22: $row->d22=number_format($row->d21-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 23: $row->d23=number_format($row->d22-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 24: $row->d24=number_format($row->d23-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 25: $row->d25=number_format($row->d24-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 26: $row->d26=number_format($row->d25-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 27: $row->d27=number_format($row->d26-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 28: $row->d28=number_format($row->d27-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 29: $row->d29=number_format($row->d28-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 30: $row->d30=number_format($row->d29-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
							case 31: $row->d31=number_format($row->d30-$datosdiap [$ini]+$datossp [$ini]+$datoscor [$ini], 1, '.', ','); break;
						}
						$ini+=1;
					}
					$row->tdc= number_format($row->d31, 1, '.', ',');$salmes=$row->tdc; 
					$data[] = $row;	
 				endforeach;	
			//cortes del día
				$this->db->select('max(Numero)');
				$resulta = $this->db->get('clientes');	 	
				foreach ($resulta->result() as $row):
					$row->Razon ='Corte';
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: $row->d1=$datoscor [$ini]; break;
							case 2: $row->d2=$datoscor [$ini]; break;
							case 3: $row->d3=$datoscor [$ini]; break;
							case 4: $row->d4=$datoscor [$ini]; break;
							case 5: $row->d5=$datoscor [$ini]; break;
							case 6: $row->d6=$datoscor [$ini]; break;
							case 7: $row->d7=$datoscor [$ini]; break;
							case 8: $row->d8=$datoscor [$ini]; break;
							case 9: $row->d9=$datoscor [$ini]; break;
							case 10: $row->d10=$datoscor [$ini]; break;
							case 11: $row->d11=$datoscor [$ini]; break;
							case 12: $row->d12=$datoscor [$ini]; break;
							case 13: $row->d13=$datoscor [$ini]; break;
							case 14: $row->d14=$datoscor [$ini]; break;
							case 15: $row->d15=$datoscor [$ini]; break;
							case 16: $row->d16=$datoscor [$ini]; break;
							case 17: $row->d17=$datoscor [$ini]; break;
							case 18: $row->d18=$datoscor [$ini]; break;
							case 19: $row->d19=$datoscor [$ini]; break;
							case 20: $row->d20=$datoscor [$ini]; break;
							case 21: $row->d21=$datoscor [$ini]; break;
							case 22: $row->d22=$datoscor [$ini]; break;
							case 23: $row->d23=$datoscor [$ini]; break;
							case 24: $row->d24=$datoscor [$ini]; break;
							case 25: $row->d25=$datoscor [$ini]; break;
							case 26: $row->d26=$datoscor [$ini]; break;
							case 27: $row->d27=$datoscor [$ini]; break;
							case 28: $row->d28=$datoscor [$ini]; break;
							case 29: $row->d29=$datoscor [$ini]; break;
							case 30: $row->d30=$datoscor [$ini]; break;
							case 31: $row->d31=$datoscor [$ini]; break;
						}
						$ini+=1;
					}
					$row->tdc=''; 
					$data[] = $row;	
 				endforeach;	
			// actualiza el saldo mensual	
			$datasaldo=array($this->cancm=>$salmes);
			$this->db->where('mesc =',$mes);
			$this->db->where('origencm =',$tip);
			$this->db->update('progcm_'.$ciclo,$datasaldo);
			//pone los dias del mes como encabezado	
			/*
			 $ini=1;
			 while($ini<=31){
				switch($ini){
					case 1: $row->e1=$datosdia [$ini];break;case 2: $row->e2=$datosdia [$ini];break;
					case 3: $row->e3=$datosdia [$ini];break;case 4: $row->e4=$datosdia [$ini];break;
					case 5: $row->e5=$datosdia [$ini];break;case 6: $row->e6=$datosdia [$ini];break;
					case 7: $row->e7=$datosdia [$ini];break;case 8: $row->e8=$datosdia [$ini];break;
					case 9: $row->e9=$datosdia [$ini];break;case 10: $row->e10=$datosdia [$ini];break;
					case 11: $row->e11=$datosdia [$ini];break;case 12: $row->e12=$datosdia [$ini];break;
					case 13: $row->e13=$datosdia [$ini];break;case 14: $row->e14=$datosdia [$ini];break;
					case 15: $row->e15=$datosdia [$ini];break;case 16: $row->e16=$datosdia [$ini];break;
					case 17: $row->e17=$datosdia [$ini];break;case 18: $row->e18=$datosdia [$ini];break;
					case 19: $row->e19=$datosdia [$ini];break;case 20: $row->e20=$datosdia [$ini];break;
					case 21: $row->e21=$datosdia [$ini];break;case 22: $row->e22=$datosdia [$ini];break;
					case 23: $row->e23=$datosdia [$ini];break;case 24: $row->e24=$datosdia [$ini];break;
					case 25: $row->e25=$datosdia [$ini];break;case 26: $row->e26=$datosdia [$ini];break;
					case 27: $row->e27=$datosdia [$ini];break;case 28: $row->e28=$datosdia [$ini];break;
					case 29: $row->e29=$datosdia [$ini];break;case 30: $row->e30=$datosdia [$ini];break;
					case 31: $row->e31=$datosdia [$ini];break;
				}
				$ini+=1;
			 }*/
			}	
			return $data;
		}
		//Salas
		function verSala(){
			$this->db->group_by('sala');
			$query=$this->db->get('larvario');
			return $query->result();			
		}
		//PROGRAMA DIA
		function getProgramadiaS($filter,$ciclo,$ori){
			$this->db->select('ids,origens,sala,corrida,fecs,fect,cans,DATEDIFF( fect,fecs ) AS dia',NULL,FALSE);
			//$this->db->join('clientes', 'Numero=idcte','inner');
			//$this->db->order_by('tipopl');
			$this->db->order_by('fecs','DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $cont=0;$tot=0;$totd=0;$conta=0;
			$conta=$result->num_rows();$pl='';$con=0;$fec=new Libreria();
			foreach($result->result() as $row):
				$row->fecs1 = $fec->fecha($row->fecs);$row->fect1 = $fec->fecha($row->fect);
				$row->cans = number_format($row->cans, 1, '.', ',');				
				$data[] = $row;
			endforeach;			
			return $data;
		}
		function getNumRowsS($filter,$ciclo){
			//$this->db->join('clientes', 'Numero=idcte','inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function agregaras($fecs,$fect,$can,$cor,$sala,$ori,$cic){
			$data=array($this->fecs=>$fecs,$this->fect=>$fect,$this->cans=>$can,$this->cor=>$cor,$this->sala=>$sala,$this->ori=>$ori);			
			$this->db->insert($cic,$data);
			return $this->db->insert_id();
		}
		public function actualizaras($id,$fecs,$fect,$can,$cor,$sala,$ori,$cic){
			$data=array($this->fecs=>$fecs,$this->fect=>$fect,$this->cans=>$can,$this->cor=>$cor,$this->sala=>$sala,$this->ori=>$ori);
			$this->db->where($this->ids,$id);
			$this->db->update($cic,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		function borraras($id,$ciclo){
			$this->db->where($this->ids,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		function buscarcd($fec,$tip,$ciclo){
			$this->db->select('idcd,cancd');
			$this->db->where($this->fecd,$fec);
			$this->db->where($this->origencd,$tip);
			$query = $this->db->get($ciclo);
			return $query->row();
		}
		function quitarcor($id,$ciclo){
			$this->db->where($this->idcd,$id);
			$this->db->delete($ciclo);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		public function agregarcor($fec,$can,$cic,$tip){
			$data=array($this->fecd=>$fec,$this->cancd=>$can,$this->origencd=>$tip);			
			$this->db->insert($cic,$data);
			return $this->db->insert_id();
		}
		public function actualizarcor($id,$fec,$can,$cic){
			$data=array($this->fecd=>$fec,$this->cancd=>$can);
			$this->db->where($this->idcd,$id);
			$this->db->update($cic,$data);
			if($this->db->affected_rows()>0){
				return 1;
			}else {
				return 0;
			}
		}
		public function UltimaAct(){
			$this->db->select('max(FechaH) as ultimaact');
			$query=$this->db->get('historialrcw');
			return $query->result();			
		}
   
  //select fecpd,canpd,porpd,obspd,tipopl from prog_2015 where idcte=160 and month(fecpd)=12 order by fecpd  
  	function mesC($filter,$ciclo,$idcte,$mes){
			//$this->db->select('fecpd,canpd,porpd,obspd,tipopl');
			//$this->db->join('clientes', 'Numero=idcte','inner');
			$this->db->where('idcte',$idcte);
			$this->db->where('month(fecpd)',$mes);
			$this->db->order_by('fecpd');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$fec=new Libreria(); $conta=0;$tot=0;
			$conta=$result->num_rows();$fec=new Libreria();
			foreach($result->result() as $row):
				$row->fec1 = $fec->fecha($row->fecpd);
				$row->can1 = number_format($row->canpd, 1, '.', ',');				
				$row->por1 = number_format($row->porpd, 1, '.', ',');
				$row->tot1 = number_format(($row->canpd*(1+($row->porpd/100))), 1, '.', ',');
				$tot+=$row->tot1;
				$data[] = $row;
			endforeach;	
			//total
			$this->db->select('max(Numero)');
			$resulta = $this->db->get('clientes');	 	
			foreach ($resulta->result() as $row):
				$row->tipopl = '';$row->fec1 = '';$row->can1 = '';$row->por1 = '';
				$row->tot1 = number_format($tot, 1, '.', ',');				
				$data[] = $row;
			endforeach;				
			return $data;
		}
 }
?>