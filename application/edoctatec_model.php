<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Edoctatec_model extends CI_Model {        
        public $id="NumSol"; public $fec="FechaS";public $cli="NumCliS";public $enc="NumBioS";public $des="NomDesS";
		public $cho="NumChoS";public $uni="NumUniS";public $ali="Ali";public $com="Com";public $cas="Cas";
		public $hos="Hos";public $fit="Fit";public $rep="Rep";public $obs="Obser";public $rem="NumRemS";
		public $car="Cargo";public $cab="CarAbo";public $cor="Corte";
		public $tabla="solicitud";	
		
		public $nfac="nfac";public $fecf="fecf";public $facf="facf";public $prof="prof";public $tipf="tipf";public $impf="impf";
		public $obsf="obsf";public $gasf="gasf";public $dedf="dedf";
		public $tablafac="facturatec";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		function agregar($fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte){
			if($ali!=''){$ali = str_replace("$", "", $ali);$ali = str_replace(",", "", $ali);} else {$ali=0;}
			if($com!=''){$com = str_replace("$", "", $com);$com = str_replace(",", "", $com);} else {$com=0;}
			if($cas!=''){$cas = str_replace("$", "", $cas);$cas = str_replace(",", "", $cas);} else {$cas=0;}
			if($hos!=''){$hos = str_replace("$", "", $hos);$hos = str_replace(",", "", $hos);} else {$hos=0;}
			if($fit!=''){$fit = str_replace("$", "", $fit);$fit = str_replace(",", "", $fit);} else {$fit=0;}
			if($rep!=''){$rep = str_replace("$", "", $rep);$rep = str_replace(",", "", $rep);} else {$rep=0;}
			$tot=$ali+$com+$cas+$hos+$fit+$rep;
			$cab=2;
			if($repos=="1") {$des="Otros";}	
			if($repos=="2") {$des="Improvistos";}	
			if($repos=="3") {$des=$des;}
			$data=array($this->fec=>$fec,$this->rem=>$rem,$this->enc=>$enc,$this->des=>$des,$this->cho=>$cho,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->car=>$tot,$this->rep=>$rep,$this->obs=>$obs,$this->cab=>$cab,$this->cli=>$cli,$this->cor=>$corte);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		function actualizar($id,$fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte){
			if($ali!=''){$ali = str_replace("$", "", $ali);$ali = str_replace(",", "", $ali);} else {$ali=0;}
			if($com!=''){$com = str_replace("$", "", $com);$com = str_replace(",", "", $com);} else {$com=0;}
			if($cas!=''){$cas = str_replace("$", "", $cas);$cas = str_replace(",", "", $cas);} else {$cas=0;}
			if($hos!=''){$hos = str_replace("$", "", $hos);$hos = str_replace(",", "", $hos);} else {$hos=0;}
			if($fit!=''){$fit = str_replace("$", "", $fit);$fit = str_replace(",", "", $fit);} else {$fit=0;}
			if($rep!=''){$rep = str_replace("$", "", $rep);$rep = str_replace(",", "", $rep);} else {$rep=0;}
			$tot=$ali+$com+$cas+$hos+$fit;
			if($repos=="1") {$des="Otros";$tot=$rep;}	
			if($repos=="2") {$des="Improvistos";$tot=$rep;}	
			if($repos=="3") {$des=$des;$tot+=$rep;}
			$data=array($this->fec=>$fec,$this->rem=>$rem,$this->enc=>$enc,$this->des=>$des,$this->cho=>$cho,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->car=>$tot,$this->rep=>$rep,$this->obs=>$obs,$this->cli=>$cli,$this->cor=>$corte);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function agregafac($fecf,$facf,$prof,$tipf,$impf,$gasf,$dedf){
			$impf = str_replace(",", "", $impf);
			$data=array($this->fecf=>$fecf,$this->facf=>$facf,$this->prof=>$prof,$this->tipf=>$tipf,$this->impf=>$impf,$this->gasf=>$gasf,$this->dedf=>$dedf);			
			$this->db->insert($this->tablafac,$data);
			return $this->db->insert_id();
		}
		function actualizafac($id,$fecf,$facf,$prof,$tipf,$impf,$dedf){
			$impf = str_replace(",", "", $impf);
			$data=array($this->fecf=>$fecf,$this->facf=>$facf,$this->prof=>$prof,$this->tipf=>$tipf,$this->impf=>$impf,$this->dedf=>$dedf);				
			$this->db->where($this->nfac,$id);
			$this->db->update($this->tablafac,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$query=$this->db->get('biologo');
			return $query->result();			
		}	
		function verProveedor(){
			$this->db->order_by('Razon');
			$query=$this->db->get('proveedoresa');
			return $query->result();			
		}	
		function verTecnico1($nt){
			$this->db->where('numbio =',$nt);
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		/*function verDestino(){
			$this->db->where('numdes >',0);
			$this->db->group_by('nomdes');
			$query=$this->db->get('viaticos');
			return $query->result();			
		}*/
		public function verDestino(){
			$this->db->select('NomDes');
			$this->db->where('numdes >',0);
			$this->db->group_by('NomDes'); 
			$query=$this->db->get('viaticos');
			return $query->result();			
		}
		function verChofer(){			
			$this->db->where('activo =',0);
			$this->db->order_by('numcho');			
			$query=$this->db->get('chofer');
			return $query->result();			
		}
		function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		function verRemision(){
			$this->db->where('RemisionR >',0);			
			$this->db->order_by('NumRegR','DESC');
			$query=$this->db->get('r18');
			return $query->result();			
		}
		function getSaldo($filter,$mes){
			//general de todos
			//select NomBio,(select sum(cargo)from solicitud where CarAbo=1 and NumbioS=NumBio) as cargos,(select sum(cargo)from solicitud where CarAbo=2 and NumbioS=NumBio) as abonos from biologo where Activo=0 and NumBio>1 
			
			//select NumSol,NomDesS,RemisionR,Razon,FechaS,Ali,Com,Cas,Hos,Fit,Rep,CarAbo,Cargo,Obser 
			// from biologo 
			//inner join (r13 inner join(clientes inner join solicitud on NumCliS=Numero) on NumRemS=NumRegR) on NumBioS=NumBio 
			//where NumBioS='".$biologo."' and NumSol>0 
			// order by RemisionR,FechaS,CarAbo,NomDesS
			//$this->db->select('NumSol,NomDesS,RemisionR,(select Razon from clientes where numclis=numero) as Razon,FechaS,Ali,Com,Cas,Hos,Fit,Rep,CarAbo,Cargo,Obser,NumBioS,NumUniS,NumChoS,NumRemS');
			//$this->db->join('clientes', 'numclis=numero','inner'
			//$this->db->join('r13', 'numrems=numregr','inner');
			
			$this->db->select('NumSol,NomDesS,(select NomBio from biologo where NumBioS=NumBio) as nombio,(select RemisionR from r18 where NumRemS=NumRegR) as RemisionR,Razon,FechaS,Ali,Com,Cas,Hos,Fit,Rep,CarAbo,Cargo,Obser,NumBioS,NumUniS,NumChoS,NumRemS,Corte');
			$this->db->join('clientes', 'numclis=numero','inner');						
			//$this->db->join('r13', 'numrems=numregr','inner');			
			//$this->db->where('numsol >',0);
			
			$this->db->order_by('remisionr');
			$this->db->order_by('fechas');
			$this->db->order_by('carabo');	
			$this->db->order_by('nomdess');
			
			if($filter['where']!=''){ $this->db->where($filter['where']); }
			if($mes>0){$this->db->where('month(FechaS) =',$mes);}			
			$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$totab=0;$totca=0;$zona="";$fecha1=date("y-m-d");$sub1=0;$cli="";$saldofila=0;$totC=0;$totA=0;
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($fecha1!=$row->FechaS){
					$fecha1=$row->FechaS; 
					$row->FechaS1=$row->FechaS;
				} else{ 
		  			$row->FechaS1="";
				}
				if($row->NomDesS=="Improvistos" || $row->NomDesS=="Otros"){$row->tot=number_format(($row->Rep), 2, '.', ',');$tot=$row->Rep;}
				else{$row->tot=number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit), 2, '.', ',');$tot=$row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit;}				
				//$tot=$row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep;
				if($row->CarAbo==1){$saldofila=$saldofila+$row->Cargo;$totC=$totC+$row->Cargo;}else{$saldofila=$saldofila-$tot;$totA=$totA+$tot;}	
				
				if($row->NomDesS!="Improvistos" && $row->NomDesS!="Complemento Gasto" && $row->CarAbo==1){
					if($row->NomDesS=="Saldo Ciclo Anterior"){$row->Razon="Saldo Ciclo Anterior";}	 
				} else {
					
					if($row->CarAbo==2){ $row->Razon=$row->NomDesS." ".$row->Obser;}
					 elseif($row->NomDesS=="Improvistos" ){$row->Razon="Improvistos";}
					 	else{$row->Razon="Complemento Gasto";}					 
				} 
				if($row->CarAbo==1){ $row->Cargo1="$ ".number_format($row->Cargo, 2, '.', ','); $row->Cargo2=$row->Cargo; $totca+=$row->Cargo;} else{ $row->Cargo1="";}
				if($tot>0 && $row->CarAbo==2){ $row->Abono1="$ ".number_format($tot, 2, '.', ','); $row->Abono2=$tot; $totab+=$tot;} else{ $row->Abono1="";}
				$row->saldofila="$ ".number_format($saldofila, 2, '.', ',');
				$row->sub =number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit), 2, '.', ',');				
				if($row->sub=='0.00'){ $row->sub ="";}
				if($row->Ali>0){ $row->Ali = number_format(($row->Ali), 2, '.', ',');} else {$row->Ali ="";}
				if($row->Com>0){ $row->Com = number_format(($row->Com), 2, '.', ',');} else {$row->Com ="";}
				if($row->Cas>0){ $row->Cas = number_format(($row->Cas), 2, '.', ',');} else {$row->Cas ="";}
				if($row->Hos>0){ $row->Hos = number_format(($row->Hos), 2, '.', ',');} else {$row->Hos ="";}
				if($row->Fit>0){ $row->Fit = number_format(($row->Fit), 2, '.', ',');} else {$row->Fit ="";}								
				if($row->Rep>0){ $row->Rep = number_format(($row->Rep), 2, '.', ',');} else {$row->Rep ="";}
				$data[] = $row;
			endforeach;
			//poner el total general
			$this->db->select('max(Cargo) as totc');
			$resultt = $this->db->get($this->tabla);
			foreach($resultt->result() as $rowt):
				$rowt->FechaS1="Total";
				$rowt->RemisionR="";$rowt->Razon="";
				$rowt->Cargo1="$ ".number_format($totca, 2, '.', ',');
				$rowt->Abono1="$ ".number_format($totab, 2, '.', ',');
				$rowt->saldofila="$ ".number_format($totca-$totab, 2, '.', ',');
				$data[] = $rowt;
			endforeach;	
			return $data;
		}
		
		function getNumRowsS($filter,$mes){
			//$this->db->select('NumSol,NomDesS,RemisionR,(select Razon from clientes where numclis=numero) as Razon,FechaS,Ali,Com,Cas,Hos,Fit,Rep,CarAbo,Cargo,Obser,NumBioS,NumUniS,NumChoS');			
			$this->db->join('clientes', 'numclis=numero','inner');						
			//$this->db->join('r13', 'numrems=numregr','inner');
			//$this->db->join('biologo', 'numbios=numbio');
			//$this->db->where('numsol >',0);
			if($filter['where']!=''){ $this->db->where($filter['where']); }//Se toman en cuenta los filtros solicitados
			if($mes>0){$this->db->where('month(FechaS) =',$mes);}
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function quitarDet($id){
			$this->db->where($this->nfac,$id);
			$this->db->delete($this->tablafac);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	/*public function imprimirdetalle(){
			//$ncli=335;
			$this->db->select('NumSol,NomDesS,NomUni,NomBio,FechaS,Ali,Com,Cas,Hos,Fit,Rep,NumRemS,NumCliS,Cargo');
			//$this->db->from('r12');
			$this->db->join('biologo', 'NumBioS=NumBio', 'inner');
			$this->db->join('unidad', 'NumUniS=NumUni', 'inner');
			$this->db->where('NumSol >',0);			
			$this->db->where('CarAbo =',1);
			//$this->db->where('Numero',220);
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Nombio');
			$this->db->order_by('NomDesS');
			$query = $this->db->get('solicitud');			
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
	function getElements($where){
		if($where['NomDes']!='Todos'){			
		$this->db->select('NumUni,NomUni as val');
		$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
		//$this->db->where('NumDes >',0);
		$this->db->where('Activo =',0);	
		$this->db->group_by('NumUni');        
        $this->db->where($where);
        $result=$this->db->get('viaticos');
        $data = array();        
        foreach($result->result() as $row):
            $data[] = $row;
        endforeach;        
        return $data;
		}
    }*/
	function remision($numrem){									
			$this->db->select('NumCliR');
			$this->db->where('NumRegR =',$numrem);	
			$query = $this->db->get('r18');
			return $query->result();
		}
	
	function getSaldoG(){
			//general de todos
			//select NomBio,(select sum(cargo)from solicitud where CarAbo=1 and NumbioS=NumBio) as cargos,(select sum(cargo)from solicitud where CarAbo=2 and NumbioS=NumBio) as abonos from biologo where Activo=0 and NumBio>1 
			$query=$this->db->query("select NumBio,NomBio,
									(select sum(cargo)from solicitud where CarAbo=1 and NumbioS=NumBio) as cargos,
									(select sum(cargo)from solicitud where CarAbo=2 and NumbioS=NumBio) as abonos 
									from biologo where Activo=0 and NumBio>1");
			
			//$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();$totab=0;$totca=0;$zona="";$fecha1=date("y-m-d");$sub1=0;$cli="";$saldofila=0;$totC=0;$totA=0;
			//Se forma el arreglo que sera retornado
			foreach($query->result() as $row):
				$totab+=$row->abonos;$totca+=$row->cargos;
				$row->FechaS1="";
				$row->RemisionR=$row->NumBio;
				$row->Razon=$row->NomBio;
				$row->Cargo1="$ ".number_format($row->cargos, 2, '.', ',');
				$row->Abono1="$ ".number_format($row->abonos, 2, '.', ',');
				$row->saldofila="$ ".number_format(($row->cargos-$row->abonos), 2, '.', ',');
				$data[] = $row;
			endforeach;
			//poner el total general
			$this->db->select('max(Cargo) as totc');
			$resultt = $this->db->get($this->tabla);
			foreach($resultt->result() as $rowt):
				$rowt->FechaS1="Total";
				$rowt->RemisionR="";$rowt->Razon="";
				$rowt->Cargo1="$ ".number_format($totca, 2, '.', ',');
				$rowt->Abono1="$ ".number_format($totab, 2, '.', ',');
				$rowt->saldofila="$ ".number_format($totca-$totab, 2, '.', ',');
				$data[] = $rowt;
			endforeach;
			return $data;
		}
		
		function getNumRowsG($filter,$mes){
			//$this->db->select('NumSol,NomDesS,RemisionR,(select Razon from clientes where numclis=numero) as Razon,FechaS,Ali,Com,Cas,Hos,Fit,Rep,CarAbo,Cargo,Obser,NumBioS,NumUniS,NumChoS');			
			$this->db->join('clientes', 'numclis=numero','inner');						
			//$this->db->join('r13', 'numrems=numregr','inner');
			//$this->db->join('biologo', 'numbios=numbio');
			//$this->db->where('numsol >',0);
			if($filter['where']!=''){ $this->db->where($filter['where']); }//Se toman en cuenta los filtros solicitados
			if($mes>0){$this->db->where('month(FechaS) =',$mes);}
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		function getdetafac($filter){
			//general de todos
			//select NomBio,(select sum(cargo)from solicitud where CarAbo=1 and NumbioS=NumBio) as cargos,(select sum(cargo)from solicitud where CarAbo=2 and NumbioS=NumBio) as abonos from biologo where Activo=0 and NumBio>1 
			//$query=$this->db->query("select * from facturatec where gasf=$id");
			$this->db->join('proveedoresa', 'numero=prof','inner');
			$this->db->order_by('dedf','DESC');$this->db->order_by('fecf');	
			if($filter['where']!=''){ $this->db->where($filter['where']); }
			$result = $this->db->get('facturatec');
			//$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//$totab=0;$totca=0;$zona="";$fecha1=date("y-m-d");$sub1=0;$cli="";$saldofila=0;$totC=0;$totA=0;
			//Se forma el arreglo que sera retornado
			$ta=0;$tc=0;$tp=0;$th=0;$to=0;$tt=0;$ttc=0;
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				$row->fecf1 = $fec->fecha($row->fecf);
				$row->impf1=number_format($row->impf, 2, '.', ',');
				//$row->Razon=ucwords((strtolower($row->Razon)));
				$row->Razon=$row->Razon;
				$row->alif='';$row->conf='';$row->casf='';$row->hosf='';$row->otrf='';
				switch($row->tipf){
					case 1: $row->alif = '$ '.number_format($row->impf, 2, '.', ',');$ta+=$row->impf; break;
					case 2: $row->conf = '$ '.number_format($row->impf, 2, '.', ',');$tc+=$row->impf; break;
					case 3: $row->casf = '$ '.number_format($row->impf, 2, '.', ',');$tp+=$row->impf; break;
					case 4: $row->hosf = '$ '.number_format($row->impf, 2, '.', ',');$th+=$row->impf; break;
					case 5: $row->otrf = '$ '.number_format($row->impf, 2, '.', ',');$to+=$row->impf; break;
				}
				$row->totf = '$ '.number_format($row->impf, 2, '.', ',');$tt+=$row->impf;
				if($row->dedf==1)  $ttc+=$row->impf;
				$data[] = $row;
			endforeach;
			//poner el total general
			$this->db->select('max(Cargo) as totc');
			$resultt = $this->db->get($this->tabla);
			foreach($resultt->result() as $rowt):
				$rowt->fecf1="";$rowt->facf="";$rowt->Razon="Total Neto";
				if($ta>0){$rowt->alif="$ ".number_format($ta, 2, '.', ',');} else {$rowt->alif='';}
				if($tc>0){$rowt->conf="$ ".number_format($tc, 2, '.', ',');} else {$rowt->conf='';}
				if($tp>0){$rowt->casf="$ ".number_format($tp, 2, '.', ',');} else {$rowt->casf='';}
				if($th>0){$rowt->hosf="$ ".number_format($th, 2, '.', ',');} else {$rowt->hosf='';}
				if($to>0){$rowt->otrf="$ ".number_format($to, 2, '.', ',');} else {$rowt->otrf='';}
				if($tt>0){$rowt->totf='$ '.number_format($tt, 2, '.', ',');} else {$rowt->totf='';}
				$data[] = $rowt;
			endforeach;
			//poner lo contable
			$this->db->select('max(Cargo) as totc');
			$resultt = $this->db->get($this->tabla);
			foreach($resultt->result() as $rowt):
				$rowt->fecf1="";$rowt->facf="";$rowt->Razon="Total Contable";
				$rowt->alif='';$rowt->conf='';$rowt->casf='';$rowt->hosf='';$rowt->otrf='';
				if($ttc>0){
					$rowt->otrf=number_format(($ttc/$tt)*100, 2, '.', ',') .'%';	
					$rowt->totf='$ '.number_format($ttc, 2, '.', ',');
				} else {$rowt->otrf='';$rowt->totf='';}
				$data[] = $rowt;
			endforeach;
			//poner lo no deducible 
			$this->db->select('max(Cargo) as totc');
			$resultt = $this->db->get($this->tabla);
			foreach($resultt->result() as $rowt):
				$rowt->fecf1="";$rowt->facf="";$rowt->Razon="Total No Deducible";
				$rowt->alif='';$rowt->conf='';$rowt->casf='';$rowt->hosf='';$rowt->otrf='';
				//if($ttc>0){
					$rowt->otrf=number_format((($tt-$ttc)/$tt)*100, 2, '.', ',') .'%';
					$rowt->totf='$ '.number_format($tt-$ttc, 2, '.', ',');
					$rowt->dedf=0;
				//} else {$rowt->otrf='';$rowt->totf='';}
				$data[] = $rowt;
			endforeach;
			}
			return $data;
		}
		function ultimafactura(){									
			$this->db->select('MAX(NumSol) as ultimo');
			$query = $this->db->get($this->tabla);
			return $query->result();
		}
    }
    
?>