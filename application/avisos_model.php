<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Avisos_model extends CI_Model {
        public $tablaa="avi_cos_";
		public $idavi="idavi";public $fecavi="fecavi";public $folavi="folavi";public $preavi="preavi";public $delavi="delavi";	
		
		public $tablac="avi_cli_"; 
		public $idavic="idavic";public $cliavi="cliavi";public $canavi="canavi";public $idavicli="idavicli";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function busquedas($filter,$cic){
			//select fecavi,folavi,Edo,Razon,canavi from clientes 
			//inner join(  avi_cos_18 inner join avi_cli_18 on idavicli=folavi) on cliavi=Numero 
			//order by fecavi,folavi,Edo,Razon
			$this->db->select('fecavi,folavi,Edo,Razon,canavi,cliavi,idavicli');
			$this->db->join($this->tablaa.$cic, 'folavi=idavicli','inner');
			$this->db->join('clientes', 'Numero=cliavi','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecavi');	
			$this->db->order_by('folavi');
			$this->db->order_by('Edo');
			$this->db->order_by('Razon');
			$result = $this->db->get($this->tablac.$cic);
			$data = array();
			if($result->num_rows()>0){
				$feci=new Libreria();$fea='';$fol='';$cli=0; $ent=0; $sol=0;
			foreach($result->result() as $row):
				$cli=$row->cliavi;$avi=$row->idavicli;
				if($fea!=$row->fecavi){
					$row->fec=$feci->fecha21($row->fecavi);
					$fea=$row->fecavi;
				}else{ $row->fec="";}
				if($fol!=$row->folavi){
					$fol=$row->folavi;
				}else{ $row->folavi="";}
				$sol=$row->canavi;
				$row->canavi=number_format($row->canavi, 0, '.', ',');
				//sumar todas las cantidades remisionadas por aviso y cliente
				//select sum(CantidadRR) as canent from r18 where NumCliR='400' and AvisoR='062352'
				$row->canent=''; 
				$this->db->select('sum(CantidadRR) as canent');
				$this->db->where('NumCliR',$cli);
				$this->db->where('AvisoR',$avi);
				$resulte = $this->db->get('r'.$cic);
				foreach($resulte->result() as $rowe):
					$ent=$rowe->canent*1000;
					if($ent>0) $row->canent=number_format($rowe->canent*1000, 0, '.', ',');
				endforeach;	
				$row->dif=number_format($sol-$ent, 0, '.', ',');
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		
		function agregaravi($fec,$fol,$pre,$del,$cic){
			$data=array($this->fecavi=>$fec,$this->folavi=>$fol,$this->preavi=>$pre,$this->delavi=>$del);			
			$this->db->insert($this->tablaa.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizaravi($id,$fec,$fol,$pre,$del,$cic){
	 		$data=array($this->fecavi=>$fec,$this->folavi=>$fol,$this->preavi=>$pre,$this->delavi=>$del);
			$this->db->where($this->idavi,$id);
			$this->db->update($this->tablaa.$cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getavisos($filter,$cic){
			//select idavi,folavi,(select sum(canavi) from avi_cli_18 where idavi=idavicli) can, preavi,delavi from avi_cos_18 
			
			$this->db->select('idavi,fecavi,folavi,(select sum(canavi) from avi_cli_18 where folavi=idavicli) can, preavi,delavi');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->order_by('fecavi','DESC');$this->db->order_by('folavi','DESC');		
			$result = $this->db->get($this->tablaa.$cic);
			$data = array();
			if($result->num_rows()>0){
				$feci=new Libreria();
			foreach($result->result() as $row):
				
				$row->fec=$feci->fecha21($row->fecavi);
				if($row->can>0)$row->can=number_format($row->can, 0, '.', ','); else $row->can='';
				$row->preavi=number_format($row->preavi, 2, '.', ',');
				$row->pre=$row->preavi.'/kg';
				//$row->fol=str_pad($row->folavi, 6, "0", STR_PAD_LEFT);	
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getavisosc($filter,$cic){
			//select Razon,Edo,canavi from clientes inner join avi_cli_18 on cliavi=Numero where idavicli=1
			$this->db->select('Numero,Razon,Edo,idavic,cliavi,canavi,(idavicli) as folio');
			$this->db->join('clientes', 'Numero=cliavi','inner');
			if($filter['where']!='') $this->db->where($filter['where']);	
			$result = $this->db->get($this->tablac.$cic);
			$data = array();
			if($result->num_rows()>0){
				
			foreach($result->result() as $row):
				
				$row->canavi=number_format($row->canavi, 0, '.', ',');
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		
		function getClientes($filter){
			$this->db->select('Numero,Razon,Edo,con');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$cp=$row->CP;
				if($row->con=='') $row->con='';
				$data[] = $row;	
			endforeach;
			return $data;
		}
		
		function agregaravif($cli,$can,$clia,$cic){
			//$can=$can*1000;
			$data=array($this->cliavi=>$cli,$this->canavi=>$can,$this->idavicli=>$clia);			
			$this->db->insert($this->tablac.$cic,$data);
			return $this->db->insert_id();
		}
		function actualizaravif($id,$cli,$can,$clia,$cic){
			$can = str_replace(",", "", $can);
	 		$data=array($this->cliavi=>$cli,$this->canavi=>$can,$this->idavicli=>$clia);
			$this->db->where($this->idavic,$id);
			$this->db->update($this->tablac.$cic,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function quitar($id,$tab,$cam,$cic){
			//borro
			$this->db->where($cam,$id);
			$this->db->delete($tab.$cic);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
    }
    
?>