<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almacenes_model extends CI_Model {
        //private $nombre;
        public $id="ida"; public $nombre="noma"; public $dom="Dom"; public $loc="Loc";public $edo="Edo";
		public $rfc="RFC";public $cp="CP";public $cor="cor";public $conc="cona";public $tel="tel";public $zona="Zona";
		public $avi="avisoc";public $fec="fecavi";public $tip="tipo";public $mar="marca";public $mod="modelo";public $col="color";
		public $pla="placas";public $cho="chofer";public $gui="guia";public $fle="flete";public $pes="pesnom";public $pue="pespue";
		public $tabla="almacenes";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		
		public function agregar($nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$cor,$con,$tel,$avi,$fec,$tip,$mar,$mod,$col,$pla,$cho,$gui,$fle,$pes,$pue){
			$data=array($this->nombre=>$nombre,$this->dom=>$dom,$this->loc=>$loc,$this->edo=>$edo,$this->rfc=>$rfc,$this->cp=>$cp,$this->zona=>$zona,$this->cor=>$cor,$this->conc=>$con,$this->tel=>$tel,$this->avi=>$avi,$this->fec=>$fec,$this->tip=>$tip,$this->mar=>$mar,$this->mod=>$mod,$this->col=>$col,$this->pla=>$pla,$this->cho=>$cho,$this->gui=>$gui,$this->fle=>$fle,$this->pes=>$pes,$this->pue=>$pue);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$cor,$con,$tel,$avi,$fec,$tip,$mar,$mod,$col,$pla,$cho,$gui,$fle,$pes,$pue){
			$data=array($this->nombre=>$nombre,$this->dom=>$dom,$this->loc=>$loc,$this->edo=>$edo,$this->rfc=>$rfc,$this->cp=>$cp,$this->zona=>$zona,$this->cor=>$cor,$this->conc=>$con,$this->tel=>$tel,$this->avi=>$avi,$this->fec=>$fec,$this->tip=>$tip,$this->mar=>$mar,$this->mod=>$mod,$this->col=>$col,$this->pla=>$pla,$this->cho=>$cho,$this->gui=>$gui,$this->fle=>$fle,$this->pes=>$pes,$this->pue=>$pue);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function borrara($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		function getUsuarios($filter){
			$this->db->select('ida,noma,Dom,Loc,Edo,RFC,CP,Zona,cona,cor,tel,avisoc,fecavi,guia,tipo,marca,modelo,color,placas,chofer,flete,pesnom,pespue');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumRows($filter){
			//$this->db->select('idc,nomc,Dom,Loc,Edo,RFC,CP,Zona');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
			
    }
    
?>