<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Parametros_model extends CI_Model {
        public $tablapar="pargra";

        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function parametros($filter,$ano,$tip){
			//$cic='20'.$ano.'-'.$cic;
			if($tip==1){
				$this->db->select('(fecfq) as dia,(o1fq) as mini,(o2fq) as maxi,(2) as lim');
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->order_by('fecfq');
				$result = $this->db->get($this->tablapar.'_'.$ano);
			//$queryvg=$this->db->query("SELECT (fecfq) as dia,(o1fq) as mini,(o2fq) as maxi,(2) as lim from pargra_$ano where numgrap='$gra' and cicfq='$cic' and idpisfq='$est' order by fecfq");
			}else{
				$this->db->select('(fecfq) as dia,(t1fq) as mini,(t2fq) as maxi');
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->order_by('fecfq');
				$result = $this->db->get($this->tablapar.'_'.$ano);
			//$queryvg=$this->db->query("SELECT (fecfq) as dia,(t1fq) as mini,(t2fq) as maxi from pargra_$ano where numgrap='$gra' and cicfq='$cic' and idpisfq='$est' order by fecfq");	
			}
			$data = array();
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
			 	$data[] = $row;
			 endforeach;
			 
			} 
			return $data;
		}	
    }
?>