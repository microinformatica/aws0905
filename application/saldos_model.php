<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Saldos_model extends CI_Model {
          public $can="Cancelacion";
		//depositos
		public $nrcd="NRC";	public $idd="Numero"; public $idsi="NSR"; public $saldoc="Saldo"; public $fechad="Fecha";
        public $razond="Razon";	public $importedd="ImporteD"; public $pesosd="Pesos"; public $cuentad="Cuenta";
		public $tcd="TC"; public $obsd="Obs"; public $ndd="ND"; public $camd="Cam"; public $desd="Des"; public $card="Car";
		public $aplicard="Aplicar"; public $tablad="depositos"; public $tablac="clientes"; public $tablasi="saldosi";public $impd="impd";
		
		public $id="NumRegR"; public $fec="FechaR"; public $rem="RemisionR"; public $canr="CantidadR"; public $canrr="CantidadRR";
		public $prer="PrecioR"; public $dcor="DescuentoR"; public $ncr="NumCliR"; public $aviso="AvisoR"; public $obsr="ObsR";
		public $tipo="Tipo"; public $estatus="Estatus"; public $recibida="Recibida";public $imp="imp";
		//public $tabla="r13";       
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		function remisiones($filter,$cancela){
			$data = array();	$fec=new Libreria(); $as='';
			if($filter['where']!=''){ $this->db->where($filter['where']); }						
			If($filter['num']!=0){
				$this->db->where('Estatus <=',1);
				$this->db->order_by('FechaR');
				//$this->db->o_by('FechaR');
				$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				$dec=$filter['num']; $dec=explode("0",$dec,2); $dec=(int)$dec[1]; $ciclo="r".$dec; $tbl=$dec;
				$result = $this->db->get($ciclo); //$ciclo='r08';
				/*if($filter['num']==2008){$result = $this->db->get('r08');$ciclo='r08';}
				if($filter['num']==2009){$result = $this->db->get('r09');$ciclo='r09';}
				if($filter['num']==2010){$result = $this->db->get('r10');$ciclo='r10';}
				if($filter['num']==2011){$result = $this->db->get('r11');$ciclo='r11';}
				if($filter['num']==2012){$result = $this->db->get('r12');$ciclo='r12';}
				if($filter['num']==2013){$result = $this->db->get('r13');$ciclo='r13';}
				if($filter['num']==2014){$result = $this->db->get('r14');$ciclo='r14';}	
				if($filter['num']==2015){$result = $this->db->get('r15');$ciclo='r15';}
				if($filter['num']==2016){$result = $this->db->get('r16');$ciclo='r16';}*/
				$tot5=0;$entra=0;$tot4=0;
				foreach ($result->result() as $row):	
					$tipo=$row->Tipo;
					$row->FechaR1 = $fec->fecha($row->FechaR);
					if($tipo==1)$row->especie="Postlarvas";if($tipo==2)$row->especie="Nauplios";if($tipo==3)$row->especie="Adulto";	
					$cantidadF=$row->CantidadR-(($row->CantidadR*($row->DescuentoR*100))/100);
					$precio=$row->PrecioR;
					$row->CantidadR1 = number_format($row->CantidadR, 3, '.', ',');
					if($row->DescuentoR>0){$row->DescuentoR1 = number_format($row->DescuentoR*100, 2, '.', ',');}else{$row->DescuentoR1 ="";}
					$row->CantidadRR1 = number_format($row->CantidadRR, 3, '.', ',');
					
					if($row->imp==0)$row->imp1='1'; elseif ($row->imp==1)$row->imp1='2'; else $row->imp1='3'; 
					$row->PrecioR1 = "$".number_format($row->PrecioR, 2, '.', ',');
					$row->importe="$".number_format(($cantidadF*$precio), 2, '.', ',');
					$esta=$row->Estatus;
					if($esta==1){$row->DescuentoR1 = "SIN";$row->CantidadRR1 = "VALOR";$row->PrecioR1 = "COMERCIAL";$row->importe="";}
					else{$tot4+=$cantidadF;$tot5+=$cantidadF*$precio;}
					$f=explode("-",$row->FechaR);$as=(int)$f[0]; //$mesas=date("m"); if($mesas==12)  $as+=1;
					$row->tabla=$ciclo;$row->ciclo=$as;
					if($row->Estatus == 0){ $row->Estatus="Facturacion";}
					if($row->Estatus == 1){ $row->Estatus="Reposicion";	}
					$entra=1;
					$data[] = $row;	
				endforeach;	
				// el total por ciclo
					$this->db->select('sum(CantidadR) as CantidadRT,sum(CantidadRR) as CantidadRRT,sum(CantidadRR*PrecioR) as ImporteT');
					if($filter['where']!=''){ $this->db->where($filter['where']); }	
					$this->db->where('Estatus <=',1);
					$this->db->where('Cancelacion =',0);
					$this->db->join('clientes', 'NumCliR = Numero', 'inner');
					$result = $this->db->get($ciclo);
					$cantidadresul=$result->num_rows();
					if($entra==1){					
					foreach ($result->result() as $row):	
						$row->FechaR1 = "Total Ciclo-".$as; //.$cantidadresul;
						$row->RemisionR = "";
						$row->especie = "";$row->imp1 = "";
						$row->CantidadR1 = number_format($row->CantidadRT, 3, '.', ',');
						$row->DescuentoR1 = "";
						$row->CantidadRR1 = number_format($tot4, 3, '.', ',');
						$row->PrecioR1 = "";
						$row->importe="$".number_format(($tot5), 2, '.', ',');
						$row->tabla=$ciclo;$row->ciclo=$as; //$row->NumRegR=0;
						$data[] = $row;					
					endforeach;	
					}
			}else{ //todos los ciclos
				$origen=date("Y");
				//if ($origen<2015) $origen=date("Y")+1;  //cuando se entregan en diciembre se suma uno, 2015 se cambia en diciembre a 2016
				$mes=date("m"); if($mes==11 and $origen==2015)  $origen+=1; // o tambin funciona este
				else if($mes==12)  $origen+=1;
				$origen=explode("0",$origen,2); $origen=(int)$origen[1]; $nt=($origen-8)+1;$cuenta=1;$ini=8;//$sqlConsulta="";							
				while($cuenta<=$nt){
					//$this->db->select('NumRegR,FechaR,RemisionR,NumCliR,CantidadR,PrecioR,Tipo,AvisoR,Estatus,DescuentoR,CantidadRR,Cancelacion,Recibida');
					$this->db->where('Estatus <=',1);
					$this->db->order_by('FechaR');
					if($filter['where']!=''){ $this->db->where($filter['where']); }	
					$this->db->join('clientes', 'NumCliR = Numero', 'inner');
					if($ini<10){ $ciclo="r0".$ini; $ci=$ciclo;} else { $ciclo="r".$ini;$ci=$ciclo;}
					$result = $this->db->get($ciclo);	
					$tot=0;	$tot5=0; $tot4=0;	$mesultimo=0;	
					foreach ($result->result() as $row):	
						$tipo=$row->Tipo; 
						$mesultimo=date("m", strtotime($row->FechaR)); // esto se hizo para entregas en dic
						$row->FechaR1 = $fec->fecha($row->FechaR);
						if($tipo==1)$row->especie="Postlarvas";if($tipo==2)$row->especie="Nauplios";if($tipo==3)$row->especie="Adulto";	
						$cantidadF=$row->CantidadR-(($row->CantidadR*($row->DescuentoR*100))/100);
						$precio=$row->PrecioR;
						$row->CantidadR1 = number_format($row->CantidadR, 3, '.', ',');
						if($row->DescuentoR>0){$row->DescuentoR1 = number_format($row->DescuentoR*100, 2, '.', ',');}else{$row->DescuentoR1 ="";}
						$row->CantidadRR1 = number_format($row->CantidadRR, 3, '.', ',');
						//if($row->imp==0)$row->imp1='1'; else $row->imp1='2';
						if($row->imp==0)$row->imp1='1'; elseif ($row->imp==1)$row->imp1='2'; else $row->imp1='3';
						$row->PrecioR1 = "$".number_format($row->PrecioR, 2, '.', ',');
						$row->importe="$".number_format(($cantidadF*$precio), 2, '.', ',');
						
						$esta=$row->Estatus;
						if($esta==1){$row->DescuentoR1 = "SIN";$row->CantidadRR1 = "VALOR";$row->PrecioR1 = "COMERCIAL";$row->importe="";}
						else{$tot4+=$cantidadF;$tot5+=$cantidadF*$precio;}
						$f=explode("-",$row->FechaR);$as=(int)$f[0]; 
						//$f=explode("r",$ci);$as=(int)$f[0]; 
						if($row->Cancelacion==0){$tot=1;}
						$row->tabla=$ciclo;$row->ciclo=$as;
						if($row->Estatus == 0){ $row->Estatus="Facturacion";}
						if($row->Estatus == 1){ $row->Estatus="Reposicion";	}
						$data[] = $row;	
					endforeach;	
					// el total por ciclo
					$this->db->select('sum(CantidadR) as CantidadRT,sum(CantidadRR) as CantidadRRT,sum(CantidadRR*PrecioR) as ImporteT');
					if($filter['where']!=''){ $this->db->where($filter['where']); }	
					$this->db->where('Estatus <=',1);
					//$this->db->where('Cancelacion =',0);
					$this->db->join('clientes', 'NumCliR = Numero', 'inner');
					$result = $this->db->get($ciclo);		
					$cantidadre=$result->num_rows();
					foreach ($result->result() as $row):	
						if($row->CantidadRRT>0){
							if($as==2015 && $mesultimo==12) $as=2016; //solucion temporal si hay entregas en 2015
							$row->FechaR1 = "Total Ciclo-".$as;
							$row->RemisionR = "";$row->imp1 = "";
							$row->especie = "";
							$row->CantidadR1 = number_format($row->CantidadRT, 3, '.', ',');
							$row->DescuentoR1 = "";
							$row->CantidadRR1 = number_format($tot4, 3, '.', ',');
							$row->PrecioR1 = "";
							if($tot==1){$row->importe="$".number_format(($tot5), 2, '.', ',');}else{$row->importe="$0.00";}
							$row->tabla=$ciclo;$row->ciclo=$as; //$row->NumRegR=0;
							$data[] = $row;	
						}						
					endforeach;	
					$ini+=1;$cuenta+=1;
				}
			}		
			
			return $data;
		}
				
		
		function getNumRowsR($filter){			
			$this->db->where('Estatus <=',1);
			if($filter['where']!=''){ $this->db->where($filter['where']); }
			If($filter['num']!=0){
				$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				$dec=$filter['num']; $dec=explode("0",$dec,2); $dec=(int)$dec[1]; $ciclo="r".$dec; $tbl=$dec;
				$result = $this->db->get($ciclo);
				
				/*if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}*/
			}else{
				$origen=date("Y"); 
				//$mes=date("m"); if($mes==12)  $origen+=1;
				$origen=explode("0",$origen,2); $origen=(int)$origen[1]; $nt=($origen-8)+1;$cuenta=1;$ini=8;//$sqlConsulta="";				
				while($cuenta<=$nt){
					if($filter['where']!=''){ $this->db->where($filter['where']); }	
					$this->db->join('clientes', 'NumCliR = Numero', 'inner');
					if($ini<10){ $ciclo="r0".$ini; } else { $ciclo="r".$ini;}
					$result = $this->db->get($ciclo);
					$ini+=1;$cuenta+=1;
				}
			}
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}

		
		function depositos($filter,$cliente,$cancela,$imp){
			//SELECT Fecha,ImporteD,TC,Cuenta,Obs,ND,Des,Car,Pesos,Cancelacion,Aplicar FROM depositos WHERE ImporteD > 0 AND NRC ='".$opc."' ORDER BY Aplicar,Fecha,ND
			$data = array();	$saldo=0; $fec=new Libreria(); $as='';
			//se pone la saldo inicial
			If($filter['num']==0){
				//SELECT Importe,NSR,Cancelacion FROM saldosi WHERE NCR ='".$opc."'";
				//$this->db->select('Importe,NCR,Cancelacion');
				$this->db->where('NCR =',$cliente);
				if($cancela==-1){$this->db->where('Cancelacion =',0);}
				$result = $this->db->get('saldosi');
				$cantidadSI=$result->num_rows();
				if($cantidadSI>0){
				foreach ($result->result() as $row):
					$row->Aplicar="";$row->Obs="";$row->Fecha1="";$row->importe1="";$row->tc1="";$row->pesos1="";$row->imp2='';		
					$row->total1="Saldo Inicial";
					$row->importe1="$".number_format($row->Importe, 2, '.', ',');					
					$row->saldo1="$".number_format($row->Importe, 2, '.', ',');
					$row->saldo2=number_format($row->Importe, 2);
					$row->saldo2 = str_replace(",", "", $row->saldo2);
					$row->saldo=$row->Importe;
					$saldo=$row->Importe;
					$row->cancelarsi=$row->Cancelacion;
					if($cancela==0){
						$row->saldo1="";
						$row->saldo=0;
						$saldo=0;
											
					}
					$data[] = $row;	
				endforeach;
				}
			}
			If($filter['num']!=0){
				//se pone la venta por años
				$this->db->select('sum((CantidadR-(CantidadR*DescuentoR))*PrecioR) as ventaanual');
				$this->db->join('clientes', 'NumCliR=Numero', 'inner');
				$this->db->where('Numero =',$cliente);
				$this->db->where('Cancelacion =',0);
				$this->db->where('Estatus <',1);	
				if($imp!=100) $this->db->where('imp =',$imp);
				$dec=$filter['num']; $dec=explode("0",$dec,2); $dec=(int)$dec[1]; $ciclo="r".$dec; $tbl=$dec;
				$result = $this->db->get($ciclo);
				
				/*if($filter['num']==2008){$result = $this->db->get('r08');}
				if($filter['num']==2009){$result = $this->db->get('r09');}
				if($filter['num']==2010){$result = $this->db->get('r10');}
				if($filter['num']==2011){$result = $this->db->get('r11');}
				if($filter['num']==2012){$result = $this->db->get('r12');}
				if($filter['num']==2013){$result = $this->db->get('r13');}
				if($filter['num']==2014){$result = $this->db->get('r14');}
				if($filter['num']==2015){$result = $this->db->get('r15');}
				if($filter['num']==2016){$result = $this->db->get('r16');}*/
				$cantidadC=$result->num_rows();
				if($cantidadC>0){
					foreach ($result->result() as $row):	
						$row->total1=$filter['num'];$row->Fecha1="";$row->tc1="";$row->pesos1="";$row->Aplicar="";$row->Obs="";
						$row->importe1="$".number_format($row->ventaanual, 2, '.', ',');
						//$row->ImporteD2=number_format($row->ImporteD, 2, '.', ',');
						//$row->cancelard=$row->Cancelacion;
						$row->imp2='';	
						//
						$row->saldo1="$".number_format($row->ventaanual+$saldo, 2, '.', ',');
						$row->saldo2=number_format($row->ventaanual+$saldo, 2);
						$row->saldo2 = str_replace(",", "", $row->saldo2);
						$row->saldo=$row->ventaanual+$saldo;					
						$saldo=$saldo+$row->ventaanual;
						$data[] = $row;	
					endforeach;
				}
				//aqui inician depositos y cargos segun año
				if($filter['where']!=''){ $this->db->where($filter['where']); }
				$this->db->where('ImporteD >',0);$this->db->where('Aplicar',$filter['num']);
				$this->db->order_by("Aplicar");$this->db->order_by("Fecha");$this->db->order_by("ND");
				$result =$this->db->get('depositos');
				foreach ($result->result() as $row):	
					if($row->ImporteD>0){
						$row->total1="";
						$row->Fecha1 = $fec->fecha($row->Fecha);
						//if($row->impd==0)$row->imp2='1'; else $row->imp2='2';
						if($row->impd==0)$row->imp2='1'; elseif ($row->impd==1)$row->imp2='2'; else $row->imp2='3';
						if($row->Des==-1) $row->total1="Dcto."; if($row->Car==-1) $row->total1="Cargo";
						$row->importe1="$".number_format($row->ImporteD, 2, '.', ',');	
						$row->ImporteD2=number_format($row->ImporteD, 2, '.', ',');
						$row->cancelard=$row->Cancelacion;					
						if($row->TC>0){$row->tc1="$".number_format($row->TC, 4, '.', ',');}else{$row->tc1="";}
						if($row->Pesos>0){$row->pesos1="$".number_format($row->Pesos, 2, '.', ',');$row->Pesos=number_format($row->Pesos, 2, '.', ',');}else{$row->pesos1="";}
						if($row->Obs==null){$row->Obs=" ";}
						$can=$row->Cancelacion;$car=$row->Car;
						if($can==0){ if($car==0){$saldo=$saldo-$row->ImporteD;} else {$saldo=$saldo+$row->ImporteD;}}
						$row->saldo1="$".number_format($saldo, 2, '.', ',');
						$row->saldo2=number_format($saldo, 2);
						$row->saldo2 = str_replace(",", "", $row->saldo2);
						$row->saldo=$saldo;
						$data[] = $row;
					}	
				endforeach;	
			}else{ //todos los ciclos
				//aqui incian los depositos,cargos y descuentos del ciclo
					if($filter['where']!=''){ $this->db->where($filter['where']); }
					$this->db->where('ImporteD >',0);$this->db->where('Aplicar',2007);
					$this->db->order_by("Aplicar");	$this->db->order_by("Fecha");$this->db->order_by("ND");					
					$result = $this->db->get('depositos');					
					foreach ($result->result() as $row):	
						if($row->ImporteD>0){
							$row->total1="";
							$row->Fecha1 = $fec->fecha($row->Fecha);
							//if($row->impd==0)$row->imp2='1'; else $row->imp2='2';
							if($row->impd==0)$row->imp2='1'; elseif ($row->impd==1)$row->imp2='2'; else $row->imp2='3';
							if($row->Des==-1) $row->total1="Dcto."; if($row->Car==-1) $row->total1="Cargo";
							$row->importe1="$".number_format($row->ImporteD, 2, '.', ',');
							$row->ImporteD2=number_format($row->ImporteD, 2, '.', ',');
							$row->cancelard=$row->Cancelacion;
							if($row->TC>0){$row->tc1="$".number_format($row->TC, 4, '.', ',');}else{$row->tc1="";}						
							if($row->Pesos>0){$row->pesos1="$".number_format($row->Pesos, 2, '.', ',');$row->Pesos=number_format($row->Pesos, 2, '.', ',');}else{$row->pesos1="";}
							if($row->Obs==null){$row->Obs=" ";}
							$can=$row->Cancelacion;$car=$row->Car;
							if($row->Cancelacion==0){
								if($can==0){ if($car==0){$saldo=$saldo-$row->ImporteD;} else {$saldo=$saldo+$row->ImporteD;}}
								//$row->saldo1=$saldo;
								$row->saldo1="$".number_format($saldo, 2, '.', ',');
								$row->saldo2=number_format($saldo, 2);
								$row->saldo2 = str_replace(",", "", $row->saldo2);
								$row->saldo=$saldo;
							}else{
								//$row->saldo1=0;
								$row->saldo1="";$row->saldo=0;
							}
							$data[] = $row;
						}	
					endforeach;
					
					$origen=date("Y"); 
					if ($origen<2016) $origen=date("Y")+1;  //cuando se entregan en diciembre se suma uno, 2015 se cambia en diciembre a 2016
					//$mes=date("m"); //$actual=date("Y");                    	
                    //		if($mes==12)  $origen+=1;
					$origen=explode("0",$origen,2); $origen=(int)$origen[1]; $nt=($origen-8)+1;$cuenta=1;$ini=8;//$sqlConsulta="";							
					
					while($cuenta<=$nt){
						//aqui se mete la venta total por ciclo
						$as=0;
						if($ini<10){ $as="200".$ini;$ciclo="r0".$ini;} else { $ciclo="r".$ini;$as="20".$ini;}
						$this->db->select('sum((CantidadR-(CantidadR*DescuentoR))*PrecioR) as ventaanual');
						$this->db->join('clientes', 'NumCliR=Numero', 'inner');
						$this->db->where('NumCliR =',$cliente);
						$this->db->where('Cancelacion =',0);						
						$this->db->where('Estatus <',1);
						if($imp!=100) $this->db->where('imp =',$imp);	
						//if($imp==1){$this->db->where('imp =',1);}
						$result = $this->db->get($ciclo);
						$cantidadC=$result->num_rows();
						if($cantidadC>0){
						foreach ($result->result() as $row):							
							if($row->ventaanual>0){
								$row->imp2='';
								$row->total1=$as;$row->Fecha1="";$row->tc1="";$row->pesos1="";$row->Aplicar="";$row->Obs="";	
								$row->importe1="$".number_format($row->ventaanual, 2, '.', ',');	
								$row->saldo1="$".number_format($row->ventaanual+$saldo, 2, '.', ',');
								$row->saldo2=number_format($row->ventaanual+$saldo, 2);
								$row->saldo2 = str_replace(",", "", $row->saldo2);
								$row->saldo=$row->ventaanual+$saldo;								
								$saldo=$saldo+$row->ventaanual;								
								$data[] = $row;								
							}	
						endforeach;
						}
						//aqui incian los depositos,cargos y descuentos del ciclo
						if($filter['where']!=''){ $this->db->where($filter['where']); }
						$this->db->where('ImporteD >',0);$this->db->where('Aplicar',$as);	
						$this->db->order_by("Aplicar");	$this->db->order_by("Fecha");$this->db->order_by("ND");
						$result = $this->db->get('depositos');					
						foreach ($result->result() as $row):	
							if($row->ImporteD>0){
								$row->total1="";
								$row->Fecha1 = $fec->fecha($row->Fecha);
								//if($row->impd==0)$row->imp2='1'; else $row->imp2='2';
								if($row->impd==0)$row->imp2='1'; elseif ($row->impd==1)$row->imp2='2'; else $row->imp2='3';
								if($row->Des==-1) $row->total1="Dcto."; if($row->Car==-1) $row->total1="Cargo";
								$row->importe1="$".number_format($row->ImporteD, 2, '.', ',');
								$row->ImporteD2=number_format($row->ImporteD, 2, '.', ',');
								$row->cancelard=$row->Cancelacion;
								if($row->TC>0){$row->tc1="$".number_format($row->TC, 4, '.', ',');}else{$row->tc1="";}
								if($row->Pesos>0){$row->pesos1="$".number_format($row->Pesos, 2, '.', ',');$row->Pesos=number_format($row->Pesos, 2, '.', ',');}else{$row->pesos1="";}
								if($row->Obs==null){$row->Obs=" ";}												
								$can=$row->Cancelacion;$car=$row->Car;
								if($row->Cancelacion==0){
									if($can==0){ if($car==0){$saldo=$saldo-$row->ImporteD;} else {$saldo=$saldo+$row->ImporteD;}}
									//$row->saldo1=$saldo;
									$row->saldo1="$".number_format($saldo, 2, '.', ',');
									$row->saldo2=number_format($saldo, 2);
									$row->saldo2 = str_replace(",", "", $row->saldo2);
									$row->saldo=$saldo;
								}else{
									//$row->saldo1=0;
									$row->saldo1="";$row->saldo=0;
								}
								$data[] = $row;
							}	
						endforeach;	
					
					$ini+=1;$cuenta+=1;					
				}
				
			}		
			
			return $data;
		}
				
		
		function getNumRowsD($filter){			
			If($filter['num']!=0){
				if($filter['where']!=''){ $this->db->where($filter['where']); }
				$this->db->where('ImporteD >',0);
				$this->db->where('Aplicar',$filter['num']);
				$this->db->order_by("Aplicar");
				$this->db->order_by("Fecha");
				$this->db->order_by("ND");
				$result = $this->db->get('depositos');
				
			}else{
				//$origen=date("Y"); $origen=explode("0",$origen,2); $origen=(int)$origen[1]; $nt=($origen-8)+1;$cuenta=1;$ini=8;//$sqlConsulta="";				
				//while($cuenta<=$nt){
					if($filter['where']!=''){ $this->db->where($filter['where']); }
					$this->db->where('ImporteD >',0);
					$this->db->order_by("Aplicar");
					$this->db->order_by("Fecha");
					$this->db->order_by("ND");
					//if($ini<10){ $ciclo="r0".$ini; } else { $ciclo="r".$ini;}
					$result = $this->db->get('depositos');
					//$ini+=1;$cuenta+=1;
				//}
			}
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}

		
		
		function rescandatos($cliente,$tabla,$valor,$ciclo,$solodep){						
				//totales remisiones por ciclo
				if($solodep==0){
				$this->db->where('NumCliR =',$cliente);
				$result = $this->db->get($tabla);
				foreach ($result->result() as $row):
						$reg=$row->NumRegR;
						$data=array($this->can=>$valor);
						$this->db->where('NumRegR =',$reg);
						$this->db->update($tabla,$data); 
				endforeach;	
					
				}		
				//totales depositos por ciclo
				//if($numrem==0){
				$this->db->where('NRC =',$cliente);
				$result = $this->db->get('depositos');
				foreach ($result->result() as $rowd):
						$reg=$rowd->ND;
						$datad=array($this->can=>$valor);
						$this->db->where('ND =',$reg);
						$this->db->where('Aplicar =',$ciclo);
						$this->db->update('depositos',$datad);
				endforeach;
				//}
			return;
		}	
	
		public function actualizard($id,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$impd){
			$usd = str_replace(",", "", $usd);				
			$mn = str_replace(",", "", $mn);
			$tipo=$est;$tipoC=$descam;	
			if($tipo==0){ $car=0;$des=0;$tipoC=4;}
			if($tipo==1){ $car=-1;$des=0;$tipoC=4;}
			if($tipo==2){ $des=-1;$car=0;}
			if($tipoC==3){ $cam=-1;}else{$cam=0;}
			
			
			$data=array($this->fechad=>$fec,$this->importedd=>$usd,$this->tcd=>$tc,$this->cuentad=>$cta,$this->obsd=>$obs,$this->pesosd=>$mn,$this->aplicard=>$ciclo,$this->desd=>$des,$this->card=>$car,$this->camd=>$cam,$this->can=>$cancelar,$this->impd=>$impd);
			$this->db->where($this->ndd,$id);
			$this->db->update($this->tablad,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function borrard($id,$usd,$numcli){
			$this->db->where($this->ndd,$id);
			$this->db->delete($this->tablad,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
					return 0;
			}			
		}	
		public function actsaldocli($id,$saldo){
			//$usd = str_replace(",", "", $saldo);				
			$data=array($this->saldoc=>$saldo);
			$this->db->where($this->idd,$id);
			$this->db->update($this->tablac,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function actualizarsi($id,$cancelar){
			//$usd = str_replace(",", "", $saldo);				
			$data=array($this->can=>$cancelar);
			$this->db->where($this->idsi,$id);
			$this->db->update($this->tablasi,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function actualizarr($id,$fec,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$tabla,$cancelar,$imp){
			$can = str_replace(",", "", $can);$can = str_replace(".", "", $can); $can =$can/1000;
			$dco =$dco/100;	
			if($est == "Reposicion"){ $est=1; $pre=0; $dco=0; $cantdes=0;}
			if($est == "Facturacion"){ $est=0; $cantdes=($can-($can*$dco));}		
			$data=array($this->fec=>$fec,$this->ncr=>$cli,$this->canr=>$can,$this->prer=>$pre,$this->aviso=>$avi,$this->obsr=>$obs,$this->tipo=>$tipo,$this->estatus=>$est,$this->canrr=>$cantdes,$this->recibida=>$rec,$this->dcor=>$dco,$this->can=>$cancelar,$this->imp=>$imp);
			$this->db->where($this->id,$id);
			$this->db->update($tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
		public function agregarDCD($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam){
			$usd = str_replace(",", "", $usd);				
			$mn = str_replace(",", "", $mn);
			$tipo=$est;$tipoC=$descam;	
			if($tipo==0){ $car=0;$des=0;$tipoC=4;}
			if($tipo==1){ $car=-1;$des=0;$tipoC=4;}
			if($tipo==2){ $des=-1;$car=0;}
			if($tipoC==3){ $cam=-1;}else{$cam=0;}
			$data=array($this->nrcd=>$numcli,$this->fechad=>$fec,$this->importedd=>$usd,$this->tcd=>$tc,$this->cuentad=>$cta,$this->obsd=>$obs,$this->pesosd=>$mn,$this->aplicard=>$ciclo,$this->desd=>$des,$this->card=>$car,$this->camd=>$cam);			
			$this->db->insert($this->tablad,$data);
			return $this->db->insert_id();			
		}
		function getClientes($filter){
			$this->db->select('Numero,Razon,Dom,Loc,Edo,RFC,CP,Zona,referencia,con');
			$this->db->join('refbco', 'idcli = Numero','left');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get('clientes',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($row->referencia=='') $row->referencia='';				
				if($row->con=='') $row->con='';
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getNumClientes($filter){
			
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			$result = $this->db->get('clientes');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}			
    }    
?>