<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Limpieza_model extends CI_Model {
		public $idemcv="idemcv"; public $desmcv="desmcv"; public $catmcv="catmcv";
		public $tablamcv="man_mcv";
        	
        public $idmaq="idmaq";public $idmcv="idmcv";public $fecmaq="fecmaq";public $motmaq="motmaq";public $tramaq="tramaq";
        public $shimaq="shimaq";public $susmaq="susmaq";
		public $tablamaq="man_maq_";
		
		public $idcar="idcar";public $idcmv="idcmv";public $feccar="feccar";public $motcar="motcar";public $tracar="tracar";
        public $bomcar="bomcar";
		public $tablacar="man_car_";
		
		public $idveh="idveh";public $idvmc="idvmc";public $fecveh="fecveh";public $motveh="motveh";public $traveh="traveh";
        public $shiveh="shiveh";public $susveh="susveh";
		public $tablaveh="man_veh_";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function history($fec,$cic,$cat){
			if($cat==1) {$dia='fecmaq';$campodia='fecmaq ='; $tabla='man_maq_'; }
			if($cat==2) {$dia='feccar';$campodia='feccar ='; $tabla='man_car_';}
			if($cat==3) {$dia='fecveh';$campodia='fecveh ='; $tabla='man_veh_';}
			$this->db->select($dia.' as dia');
			$this->db->from($tabla.$cic);
			$this->db->where($campodia, $fec);
			$query=$this->db->get();
			return $query->row();
		}
		public function limpiezaCat($cat){
			$this->db->select('idemcv,desmcv');
			$this->db->where('catmcv =',$cat);	
			$this->db->order_by($this->idemcv);
			$query=$this->db->get($this->tablamcv);
			return $query->result();			
		}
		public function quitar($id,$tab,$cam,$ciclo){
			$this->db->where($cam,$id);
			$this->db->delete($tab.$ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function agregarmcv($fecha,$mcv,$mot,$tra,$shi,$sus,$cic,$cat,$bom,$todos){
			switch ($cat) {
				case '1': $cat='Maquinaria';break;
				case '2': $cat='Carcamo';break;
				case '3': $cat='Vehiculo';break;
			}
			// se registran todos
			if($todos==1){
			//SELECT count(*) from man_mcv where catmcv='Maquinaria' and estmcv='1'
			$this->db->select('idemcv');
			$this->db->where($this->catmcv,$cat);
			$query = $this->db->get($this->tablamcv);
			foreach($query->result() as $row):
				switch ($cat) {
					case 'Maquinaria': {$data=array($this->fecmaq=>$fecha,$this->idmcv=>$row->idemcv,$this->motmaq=>$mot,$this->tramaq=>$tra,$this->shimaq=>$shi,$this->susmaq=>$sus);			
							   			$this->db->insert($this->tablamaq.$cic,$data);
							   			break;	
						  	  			}	
					case 'Carcamo': {$data=array($this->feccar=>$fecha,$this->idcmv=>$row->idemcv,$this->motcar=>$mot,$this->tracar=>$tra,$this->bomcar=>$bom);			
							   			$this->db->insert($this->tablacar.$cic,$data);
							   			break;	
						  	  			}
					case 'Vehiculo': {$data=array($this->fecveh=>$fecha,$this->idvmc=>$row->idemcv,$this->motveh=>$mot,$this->traveh=>$tra,$this->shiveh=>$shi,$this->susveh=>$sus);			
							   			$this->db->insert($this->tablaveh.$cic,$data);
							   			break;	
						  	  			}
				}
			endforeach;	
			}else{
				//solo uno
				switch ($cat) {
					case 'Maquinaria': {$data=array($this->fecmaq=>$fecha,$this->idmcv=>$mcv,$this->motmaq=>$mot,$this->tramaq=>$tra,$this->shimaq=>$shi,$this->susmaq=>$sus);			
							   			$this->db->insert($this->tablamaq.$cic,$data);
							   			break;	
						  	  			}	
					case 'Carcamo': {$data=array($this->feccar=>$fecha,$this->idcmv=>$mcv,$this->motcar=>$mot,$this->tracar=>$tra,$this->bomcar=>$bom);			
							   			$this->db->insert($this->tablacar.$cic,$data);
							   			break;	
						  	  			}
					case 'Vehiculo': {$data=array($this->fecveh=>$fecha,$this->idvmc=>$mcv,$this->motveh=>$mot,$this->traveh=>$tra,$this->shiveh=>$shi,$this->susveh=>$sus);			
							   			$this->db->insert($this->tablaveh.$cic,$data);
							   			break;	
						  	  			}
				}
			}
			return $this->db->insert_id();
		}
		function actualizarmcv($id,$mot,$tra,$shi,$sus,$cic,$cat,$bom){
			switch ($cat) {
				case '1': {$data=array($this->motmaq=>$mot,$this->tramaq=>$tra,$this->shimaq=>$shi,$this->susmaq=>$sus);			
						   			$this->db->where($this->idmaq,$id);
									$this->db->update($this->tablamaq.$cic,$data);
						   			break;	
					  	  			}	
				case '2': {$data=array($this->motcar=>$mot,$this->tracar=>$tra,$this->bomcar=>$bom);			
						   			$this->db->where($this->idcar,$id);
									$this->db->update($this->tablacar.$cic,$data);
						   			break;	
					  	  		}
				case '3': {$data=array($this->motveh=>$mot,$this->traveh=>$tra,$this->shiveh=>$shi,$this->susveh=>$sus);			
						   			$this->db->where($this->idveh,$id);
									$this->db->update($this->tablaveh.$cic,$data);
						   			break;	
					  	  		}
			}
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		
		function getmaq($filter,$ciclo){
			$this->db->join($this->tablamcv,$this->idemcv .' = '. $this->idmcv, 'inner');
			$this->db->order_by($this->idemcv);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$query = $this->db->get($this->tablamaq.$ciclo);
			$data = array();
			foreach($query->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getcar($filter,$ciclo){
			$this->db->join($this->tablamcv,$this->idemcv .' = '. $this->idcmv, 'inner');
			$this->db->order_by($this->idemcv);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$query = $this->db->get($this->tablacar.$ciclo);
			$data = array();
			foreach($query->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getveh($filter,$ciclo){
			$this->db->join($this->tablamcv,$this->idemcv .' = '. $this->idvmc, 'inner');
			$this->db->order_by($this->idemcv);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$query = $this->db->get($this->tablaveh.$ciclo);
			$data = array();
			foreach($query->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function getlimpiezaG($filter,$ciclo,$mes,$cat){
			if($cat==1) {$dia='fecmaq';$campo='idmcv'; $tabla='man_maq_'; $tot=4;$catcam='maq';}
			if($cat==2) {$dia='feccar';$campo='idcmv'; $tabla='man_car_'; $tot=3;$catcam='car';}
			if($cat==3) {$dia='fecveh';$campo='idvmc'; $tabla='man_veh_'; $tot=4;$catcam='veh';}
			$this->db->select('idemcv,desmcv');
			$this->db->join($this->tablamcv,$this->idemcv .' = '. $campo, 'inner');
			$this->db->group_by('desmcv');$this->db->group_by('idemcv');
			$this->db->order_by($this->idemcv);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			$query = $this->db->get($tabla.$ciclo);	
			$data = array();$datosd=array();
			foreach($query->result() as $row):
				$veces=1;
				while($veces<=$tot){
					$this->db->select('count(*)');
					$resultc = $this->db->get($tabla.$ciclo);
					foreach ($resultc->result() as $rowc):	
						$ini=1;
						while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
						if(($cat==1 || $cat==3) && $veces==1){ $rowc->desmcv=$row->desmcv;$rowc->nom='Mot';$cam='mot';}
						elseif (($cat==1 || $cat==3) && $veces==2) {$rowc->desmcv=''; $rowc->nom='Tra';$cam='tra';}
						elseif (($cat==1 || $cat==3) && $veces==3) {$rowc->desmcv=''; $rowc->nom='Hid';$cam='shi';}
						elseif (($cat==1 || $cat==3) && $veces==4) {$rowc->desmcv=''; $rowc->nom='Sus';$cam='sus';}
						if($cat==2 && $veces==1){ $rowc->desmcv=$row->desmcv;$rowc->nom='Mot';$cam='mot';}
						elseif ($cat==2 && $veces==2) {$rowc->desmcv=''; $rowc->nom='Tra';$cam='tra';}
						elseif ($cat==2 && $veces==3) {$rowc->desmcv=''; $rowc->nom='Bom';$cam='bom';}
						//busca los datos de la evaluacion
						//SELECT * from man_maq_21 where month(fecmaq)=4 and idmcv=1 order by fecmaq
						$this->db->select('day('.$dia.') as diaeva, ('.$cam.$catcam.') as mot');
						$this->db->where($campo,$row->idemcv);
						$this->db->where('month('.$dia.')',$mes);
						$this->db->order_by($dia);
						$queryeva = $this->db->get($tabla.$ciclo);
						foreach($queryeva->result() as $roweva):
							$datosd [$roweva->diaeva]=$roweva->diaeva;
							$datosr [$roweva->diaeva]=$roweva->mot;
						endforeach;
						$ini=1;
						while($ini<=31){
							switch($ini){
								case 1: if($datosd [$ini]==$ini){ $rowc->d1 = $datosr [$ini];} else {  $rowc->d1 ="";} break;
								case 2: if($datosd [$ini]==$ini){ $rowc->d2 = $datosr [$ini];} else {  $rowc->d2 ="";} break;
								case 3: if($datosd [$ini]==$ini){ $rowc->d3 = $datosr [$ini];} else {  $rowc->d3 ="";} break;
								case 4: if($datosd [$ini]==$ini){ $rowc->d4 = $datosr [$ini];} else {  $rowc->d4 ="";} break;
								case 5: if($datosd [$ini]==$ini){ $rowc->d5 = $datosr [$ini];} else {  $rowc->d5 ="";} break;
								case 6: if($datosd [$ini]==$ini){ $rowc->d6 = $datosr [$ini];} else {  $rowc->d6 ="";} break;
								case 7: if($datosd [$ini]==$ini){ $rowc->d7 = $datosr [$ini];} else {  $rowc->d7 ="";} break;
								case 8: if($datosd [$ini]==$ini){ $rowc->d8 = $datosr [$ini];} else {  $rowc->d8 ="";} break;
								case 9: if($datosd [$ini]==$ini){ $rowc->d9 = $datosr [$ini];} else {  $rowc->d9 ="";} break;
								case 10: if($datosd [$ini]==$ini){ $rowc->d10 = $datosr [$ini];} else {  $rowc->d10 ="";} break;
								case 11: if($datosd [$ini]==$ini){ $rowc->d11 = $datosr [$ini];} else {  $rowc->d11 ="";} break;
								case 12: if($datosd [$ini]==$ini){ $rowc->d12 = $datosr [$ini];} else {  $rowc->d12 ="";} break;
								case 13: if($datosd [$ini]==$ini){ $rowc->d13 = $datosr [$ini];} else {  $rowc->d13 ="";} break;
								case 14: if($datosd [$ini]==$ini){ $rowc->d14 = $datosr [$ini];} else {  $rowc->d14 ="";} break;
								case 15: if($datosd [$ini]==$ini){ $rowc->d15 = $datosr [$ini];} else {  $rowc->d15 ="";} break;
								case 16: if($datosd [$ini]==$ini){ $rowc->d16 = $datosr [$ini];} else {  $rowc->d16 ="";} break;
								case 17: if($datosd [$ini]==$ini){ $rowc->d17 = $datosr [$ini];} else {  $rowc->d17 ="";} break;
								case 18: if($datosd [$ini]==$ini){ $rowc->d18 = $datosr [$ini];} else {  $rowc->d18 ="";} break;
								case 19: if($datosd [$ini]==$ini){ $rowc->d19 = $datosr [$ini];} else {  $rowc->d19 ="";} break;
								case 20: if($datosd [$ini]==$ini){ $rowc->d20 = $datosr [$ini];} else {  $rowc->d20 ="";} break;
								case 21: if($datosd [$ini]==$ini){ $rowc->d21 = $datosr [$ini];} else {  $rowc->d21 ="";} break;
								case 22: if($datosd [$ini]==$ini){ $rowc->d22 = $datosr [$ini];} else {  $rowc->d22 ="";} break;
								case 23: if($datosd [$ini]==$ini){ $rowc->d23 = $datosr [$ini];} else {  $rowc->d23 ="";} break;
								case 24: if($datosd [$ini]==$ini){ $rowc->d24 = $datosr [$ini];} else {  $rowc->d24 ="";} break;
								case 25: if($datosd [$ini]==$ini){ $rowc->d25 = $datosr [$ini];} else {  $rowc->d25 ="";} break;
								case 26: if($datosd [$ini]==$ini){ $rowc->d26 = $datosr [$ini];} else {  $rowc->d26 ="";} break;
								case 27: if($datosd [$ini]==$ini){ $rowc->d27 = $datosr [$ini];} else {  $rowc->d27 ="";} break;
								case 28: if($datosd [$ini]==$ini){ $rowc->d28 = $datosr [$ini];} else {  $rowc->d28 ="";} break;
								case 29: if($datosd [$ini]==$ini){ $rowc->d29 = $datosr [$ini];} else {  $rowc->d29 ="";} break;
								case 30: if($datosd [$ini]==$ini){ $rowc->d30 = $datosr [$ini];} else {  $rowc->d30 ="";} break;
								case 31: if($datosd [$ini]==$ini){ $rowc->d31 = $datosr [$ini];} else {  $rowc->d31 ="";} break;
							}
							$ini+=1;
						}
					$data[] = $rowc;
	 				endforeach;
					$veces+=1;
				}	
			endforeach;
			return $data;
		}
    }
?>