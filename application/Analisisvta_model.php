<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Analisisvta_model extends CI_Model {
        
		public $tablae="entradas";	public $tablamp="maqpro_";  
		
		public $idents="idents";
		public $tablas="salidas";
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getfinal($filter,$cic,$alm,$gra){
			//select idt,nomt,sum(kgsp)as procesados,(select sum(kgssal) from salidas where talsal=idtp and estsal=1)as ventas 
			//from tallas inner join maqpro_19 on idtp=idt where lotp!='' group by idt order by idt
			
			//sacar el total de lo que queda para obtener el promedio por talla
			if($gra>0) $this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal='.$gra.' and ciclo=20'.$cic.')as ventas ');
			else $this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and ciclo=20'.$cic.')as ventas ');
			$this->db->join('tallas', 'idt=idtp','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where('lotp !=','');
			$this->db->group_by('idt');
			$this->db->order_by('gpogrl');
			$this->db->order_by('nomt');
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$procesado=0;$ventas=0;$existencia=0;$procesadoa=0;$ventasa=0;$existenciaa=0;$procesadok=0;$ventask=0;$existenciak=0;
			$totpro=0;$peg='';$usg='';$dag='';$cosalm=0;$bormn=0;$totda=0;$pda=0;$totcostos=0;
			$feci=new Libreria();
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				$exi='';$pe='';$us=0;$differenceFormat='';
				$existencia+=$row->procesado-$row->ventas;
				//$exi=$row->procesado-$row->ventas;
				if($row->procesado-$row->ventas>0.9) $exi=$row->procesado-$row->ventas;
				$totpro+=$row->procesado;
				//buscamos la primera fecha de entrada 
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp limit 1
				$this->db->select('fecp');
				$this->db->where('idtp',$row->idt);
				if($gra>0) $this->db->where('idgp',$gra);
				//$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp');	
				$resultpe = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultpe->result() as $rowpe):	
					//$row->pe=$feci->fecha21($rowpe->fecp);
					$pe=$rowpe->fecp;
				endforeach;	
				
				//fecha última salida
				$this->db->select('fecsal');	
				//$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				$this->db->where('talsal',$row->idt);
				if($gra>0)	$this->db->where('grasal',$gra);  //$idgranja
				//$this->db->where('almsal',$idalmacen);
				$this->db->order_by('fecsal','DESC');
				$resultus = $this->db->get($this->tablas,1);
				foreach ($resultus->result() as $rowus):	
					if($exi==''){ $us=$rowus->fecsal;}
					else {$us=$hoy=date('Y-m-d');}
				endforeach;	
				//dias en almacen
				if($pe!=''){
					$datetime1 = date_create($pe);
					if($us!=0){$datetime2 = date_create($us);	
				 	$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$totda+= $contador->format($differenceFormat);}
				}
			 endforeach;
			 
			 //buscamos la primera fecha de la primera maquila
			//SELECT fecp from maqpro_19 where estp=1 order by fecp limit 
				$this->db->select('fecp');
				if($gra>0) $this->db->where('idgp',$gra);
				//$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp');	
				$resultpeg = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultpeg->result() as $rowpeg):	
					$peg=$rowpeg->fecp;
				endforeach;
				//ultima venta general	
				$this->db->select('fecsal');	
				//$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->where('talsal',$row->idt);
				if($gra>0)	$this->db->where('grasal',$gra);  //$idgranja
				//$this->db->where('almsal',$idalmacen);
				$this->db->order_by('fecsal','DESC');
				$resultus = $this->db->get($this->tablas,1);
				foreach ($resultus->result() as $rowus):	
					if($existencia>0){$usg=$hoy=date('Y-m-d'); }
					else {$usg=$rowus->fecsal;}
				endforeach;	
				//dias en almacen general
				$datetime1 = date_create($peg);
				$datetime2 = date_create($usg);
				$contador = date_diff($datetime1, $datetime2);
				$differenceFormat = '%a';
				$dag= $contador->format($differenceFormat);	
				// costo total de almacenaje
				//SELECT sum(impfn) as cosalm from facnot where tippc=3 and cicfn=2019
				$this->db->select('sum(impfn) as cosalm');
				$this->db->where('tippc',3);
				$this->db->where('cicfn','20'.$cic);
				$resultalm = $this->db->get('facnot');
				foreach ($resultalm->result() as $rowalm):	
					$cosalm=$rowalm->cosalm;
				endforeach;
				if($totda>0) $pda=$cosalm/$totda;
				//costo toal del Bordo que se fue a maquila
				//select sum(kgscos)as bordo,sum(kgscos*(grscos+prebas)) as bormn from bordo_19  where tipcos=1
				$this->db->select('sum(kgscos)as bordo,sum(kgscos*(grscos+prebas)) as bormn');
				$this->db->where('tipcos',1);
				$resultabor = $this->db->get('bordo_'.$cic);
				foreach ($resultabor->result() as $rowbor):	
					$bordo=$rowbor->bordo;$bormn=$rowbor->bormn;
				endforeach;
				//costo toal de fletes, seguros y traslados
				//select sum(kgscos)as bordo,sum(kgscos*(grscos+prebas)) as bormn from bordo_19  where tipcos=1
				$this->db->select('sum(impfle*tcfle) as fst');
				$this->db->where('cicfle','20'.$cic);
				$resultafst = $this->db->get('maqflesegtra');
				foreach ($resultafst->result() as $rowfst):	
					$fst=$rowfst->fst;
				endforeach;
			}
			
			
			$this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and ciclo='.$cic.')as ventas ');
			$this->db->join('tallas', 'idt=idtp','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where('lotp !=','');
			$this->db->group_by('idt');
			$this->db->order_by('gpogrl');
			$this->db->order_by('nomt');
			$result = $this->db->get($this->tablamp.$cic);
			$data = array();$procesado=0;$ventas=0;
			$totkgsmn=0;$totimpmn=0;$totkgsus=0;$totimpus=0;
			$tvkmn=0;$tvimn=0;$tvkus=0;$tvius=0; $venutig=0;$cosutig=0;
			if($result->num_rows()>0){
			 foreach($result->result() as $row):
				 //buscar los datos de la granja de Ahome de acuerdo a la talla
				$venuti=0;$cosuti=0;$ca=0;$cb=0;$cm=0;$cfst=0;
				$row->procesado='';$row->porpro='';$row->ventas='';$row->existencia='';$row->por='';$row->pe='';$row->us='';$row->fst='';
				$row->mqkmn='';$row->mqimn='';$row->mqkus='';$row->mqius='';$row->cosalm='';$row->bordo='';$row->bormn='';
				$row->vkmn='';$row->vimn='';$row->vpmn='';$row->vkus='';$row->vius='';$row->vpus='';$row->utilidad='';$row->porutil='';
				if($gra>0) $this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and grasal='.$gra.' and ciclo=20'.$cic.')as ventas ');
				else $this->db->select('idt,nomt,sum(kgsp)as procesado,(select sum(kgssal) from salidas where talsal=idtp and estsal=1 and ciclo=20'.$cic.')as ventas ');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				if($filter['where']!='') $this->db->where($filter['where']);
				//$this->db->where('idgp =',4);
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('idt');
				$this->db->order_by('gpogrl');
				$this->db->order_by('nomt');
				$resulta = $this->db->get($this->tablamp.$cic);
				$por=0;$ventast=0;
				foreach($resulta->result() as $rowa):
					$procesado+=$rowa->procesado;$ventas+=$rowa->ventas;$ventast=$rowa->ventas;
					$por=number_format(($rowa->procesado/$totpro)*100, 2, '.', ',');
					if($rowa->procesado-$rowa->ventas>0.9) $row->existencia=number_format(($rowa->procesado-$rowa->ventas), 2, '.', ','); //else $row->existencia='';
					if($rowa->procesado>0){ $row->procesado=number_format(($rowa->procesado), 2, '.', ',');$row->porpro=number_format(($rowa->procesado/$totpro)*100, 2, '.', ','); }
					if($rowa->ventas) $row->ventas=number_format(($rowa->ventas), 2, '.', ','); // else $row->ventas='';
					if($row->existencia!='') {$row->por=number_format((($rowa->procesado-$rowa->ventas)/$existencia)*100, 2, '.', ',');} else $row->por='';
					
				endforeach;
				$row->fst='$'.number_format(($fst*($por/100)), 2, '.', ',');$cosuti+=$fst*($por/100);$cosutig+=$fst*($por/100);	
				$cfst=$fst*($por/100);
				$row->bordo=number_format(($bordo*($por/100)), 2, '.', ',');
				$row->bormn='$'.number_format(($bormn*($por/100)), 2, '.', ',');$cb=$bormn*($por/100);
				$cosuti+=$bormn*($por/100);$cosutig+=$bormn*($por/100);
				$row->mqimn='';
				//separar por moneda	
				$this->db->select('idt,nomt,sum(kgsp)as procesado,moneda,sum(masp*kgmp*prep*1.16) as importe,tcm');
				$this->db->join('tallas', 'idt=idtp','inner');
				$this->db->where('lotp !=','');
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->where('idt =',$row->idt);
				$this->db->group_by('moneda');
				$resultm = $this->db->get($this->tablamp.$cic);
				$impmnm=0;$impmnv=0;
				foreach($resultm->result() as $rowm):
					switch ($rowm->moneda) {
						case 1: $row->mqkmn=number_format(($rowm->procesado), 2, '.', ',');	$totkgsmn+=$rowm->procesado;
								//$row->mqimn='$'.number_format(($rowm->importe), 2, '.', ',');
								$impmnm=$rowm->importe;
								$totimpmn+=$rowm->importe;$cosuti+=$rowm->importe;$cosutig+=$rowm->importe;
							break;
						case 2: $row->mqkus=number_format(($rowm->procesado), 2, '.', ','); $totkgsus+=$rowm->procesado;
								//$row->mqius='$'.number_format(($rowm->importe*2.20462), 2, '.', ','); $totimpus+=$rowm->importe*2.20462;
								if($rowm->tcm > 1){
									$impmnm+=($rowm->importe*2.20462*$rowm->tcm);
									$totimpmn+=$rowm->importe*2.20462*$rowm->tcm;
									$cosuti+=$rowm->importe*2.20462*$rowm->tcm;$cosutig+=$rowm->importe*2.20462*$rowm->tcm;
									//$row->mqius='$'.number_format(($rowm->importe*2.20462*$rowm->tcm), 2, '.', ',');	
								}else{
									//SELECT avg(tcm) from maqpro_19 where moneda=2 and tcm > 1
									$this->db->select('avg(tcm) as tcm');
									$this->db->where('moneda',2);
									$this->db->where('tcm >',1);
									$resultmp = $this->db->get($this->tablamp.$cic);
									foreach($resultmp->result() as $rowmp):
										$impmnm+=($rowm->importe*2.20462*$rowmp->tcm);
										$totimpmn+=$rowm->importe*2.20462*$rowmp->tcm;
										$cosuti+=$rowm->importe*2.20462*$rowmp->tcm;$cosutig+=$rowm->importe*2.20462*$rowmp->tcm;
									endforeach;
								}
								 
								$row->mqius='$'.number_format(($rowm->importe*2.20462), 2, '.', ','); 								
							break;							
					}		
								
				endforeach;	
				if($impmnm>0) {$row->mqimn='$'.number_format(($impmnm), 2, '.', ',');$cm=$impmnm;
				}
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				//$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($gra==0){if($filter['where']!=''){$this->db->where($filter['where']); }}
				else $this->db->where('grasal',$gra);
				$this->db->where('talsal',$row->idt);
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tvkmn+=$rowm->kgs;$tvimn+=$rowm->imp;$venuti+=$rowm->imp;$venutig+=$rowm->imp;
					$impmnv+=$rowm->imp;
					//$mn=number_format(($rowm->imp), 2, '.', ',');$kmnd=$rowm->kgs;$tkmnd+=$rowm->kgs;$imnd=$rowm->imp;$timnd+=$rowm->imp;
					if($rowm->kgs>0)$row->vkmn=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->vimn='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0 and ($rowm->imp/$rowm->kgs)>0){ $row->vpmn='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');//$pvmn=$rowm->imp/$rowm->kgs;
					}
				endforeach;
				//usd
				//SELECT sum(kgssal*usdkgs*tcv) from salidas where talsal=2 and usdkgs>0
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs*tcv)as imp');
				//$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($gra==0){if($filter['where']!=''){$this->db->where($filter['where']); }}
				else $this->db->where('grasal',$gra);
				$this->db->where('talsal',$row->idt);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tvkus+=$rowm->kgs;$tvius+=$rowm->imp;$venuti+=$rowm->imp;$venutig+=$rowm->imp;
					$impmnv+=$rowm->imp;$tvimn+=$rowm->imp;
					if($rowm->kgs>0)$row->vkus=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->vius='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0 and ($rowm->imp/$rowm->kgs)>0){ $row->vpus='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');}
				endforeach;
				if($ventast>0 and ($impmnv/$ventast)>0){ $row->vpmn='$'.number_format(($impmnv/$ventast), 2, '.', ',');}
				if($impmnv>0)$row->vimn='$'.number_format(($impmnv), 2, '.', ',');
				
				//buscamos la primera fecha de entrada 
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp limit 1
				$this->db->select('fecp');
				$this->db->where('idtp',$row->idt);
				if($gra>0) $this->db->where('idgp',$gra);
				//$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp');	
				$resultpe = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultpe->result() as $rowpe):	
					$row->pe=$feci->fecha21($rowpe->fecp);	$pe=$rowpe->fecp;
				endforeach;	
				
				//fecha última salida
				$this->db->select('fecsal');	
				//$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				$this->db->where('talsal',$row->idt);
				if($gra>0)	$this->db->where('grasal',$gra);   //$idgranja
				//$this->db->where('almsal',$idalmacen);
				$this->db->order_by('fecsal','DESC');
				$resultus = $this->db->get($this->tablas,1);
				foreach ($resultus->result() as $rowus):
					$row->us=$feci->fecha23($rowus->fecsal);	
					if($row->existencia==''){ $us=$rowus->fecsal;}
					else {$us=$hoy=date('Y-m-d');}
				endforeach;	
				//dias en almacen
				if($pe!='' and $us!=''){
					$datetime1 = date_create($pe);
					$datetime2 = date_create($us);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->da= $contador->format($differenceFormat);
					$cosuti+=$row->da*$pda;$cosutig+=$row->da*$pda;
				}else{$row->da=0;}
				$row->utilidad='$'.number_format(($venuti-$cosuti), 2, '.', ',');
				if($venuti-$cosuti>0) $row->porutil=3; else if($venuti-$cosuti<0) $row->porutil=1; else $row->porutil=2;  
				//costo almacenaje
				$row->cosalm='$'.number_format(($row->da*$pda), 2, '.', ',');$ca=$row->da*$pda;
				$row->costot='$'.number_format(($ca+$cb+$cm+$cfst), 2, '.', ',');$totcostos+=$ca+$cb+$cm+$cfst;
				$data[] = $row;
				$tot=1;
			 endforeach;
			
			//Total
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->procesado='';$rowt->porpro='';$rowt->pe='';$rowt->ventas='';$rowt->us='';$rowt->existencia='';
				$rowt->da='';$rowt->cosalm='';$rowt->bordo='';$rowt->bormn='';$rowt->utilidad='';$rowt->porutil='';
				$rowt->mqkmn='';$rowt->mqimn='';$rowt->mqkus='';$rowt->mqius='';$rowt->fst='';
				$rowt->vkmn='';$rowt->vimn='';$rowt->vpmn='';$rowt->vkus='';$rowt->vius='';$rowt->vpus='';
				if($fst>0) $rowt->fst='$'.number_format(($fst), 2, '.', ',');
				if($bordo>0) $rowt->bordo=number_format(($bordo), 2, '.', ',');
				if($bormn>0) $rowt->bormn='$'.number_format(($bormn), 2, '.', ',');
				if($procesado>0) $rowt->procesado=number_format(($procesado), 2, '.', ','); 
				if($peg!='') $rowt->pe=$feci->fecha21($peg);
				if($totkgsmn>0) $rowt->mqkmn=number_format(($totkgsmn), 2, '.', ',');
				if($totimpmn>0) $rowt->mqimn='$'.number_format(($totimpmn), 2, '.', ',');
				if($totkgsus>0) $rowt->mqkus=number_format(($totkgsus), 2, '.', ',');
				if($totimpus>0) $rowt->mqius='$'.number_format(($totimpus), 2, '.', ',');
				if($ventas>0) $rowt->ventas=number_format(($ventas), 2, '.', ',');
				if($usg!='') $rowt->us=$feci->fecha23($usg);
				if($dag!='') $rowt->da=$dag;
				//$rowt->da=$totda;
				if($cosalm>0) $rowt->cosalm='$'.number_format(($cosalm), 2, '.', ',');
				if($tvkmn>0) $rowt->vkmn=number_format(($tvkmn), 2, '.', ',');
				if($tvimn>0) $rowt->vimn='$'.number_format(($tvimn), 2, '.', ',');
				if($tvkmn>0 && $ventas>0) $rowt->vpmn='$'.number_format(($tvimn/$ventas), 2, '.', ',');
				if($tvkus>0) $rowt->vkus=number_format(($tvkus), 2, '.', ',');
				if($tvius>0) $rowt->vius='$'.number_format(($tvius), 2, '.', ',');
				if($procesado>0) $rowt->existencia=number_format(($procesado-$ventas), 2, '.', ',');  
				if(($venutig-$cosutig)!=0) $rowt->utilidad='$'.number_format(($venutig-$cosutig), 2, '.', ',');
				if($venutig-$cosutig>0) $rowt->porutil=3; else if($venutig-$cosutig<0) $rowt->porutil=1; else $rowt->porutil=2;  
				$rowt->costot='$'.number_format($totcostos, 2, '.', ',');;
				$rowt->nomt='Total:';$rowt->por='';
				$data[] = $rowt;	
			 endforeach;
			 
			 //Porcentajes
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->porpro='';$rowt->pe='';$rowt->ventas='';$rowt->us='';$rowt->existencia='';
				$rowt->da='';$rowt->cosalm='';$rowt->bordo='';$rowt->bormn='';$rowt->utilidad='';$rowt->porutil='';
				$rowt->mqkmn='';$rowt->mqimn='';$rowt->mqkus='';$rowt->mqius='';$rowt->fst='';
				$rowt->vkmn='';$rowt->vimn='';$rowt->vpmn='';$rowt->vkus='';$rowt->vius='';$rowt->vpus='';
				$rowt->nomt='';$rowt->por='';$rowt->costot='';
				if($procesado>0){
					$rowt->procesado=number_format(($procesado/$bordo)*100, 2, '.', ',').' %';;	
					$rowt->ventas=number_format((($ventas)/$procesado)*100, 2, '.', ',').' %';
					$rowt->existencia=number_format((($procesado-$ventas)/$procesado)*100, 2, '.', ',').' %';
				} 
				if($cosalm+$bormn+$totimpmn+$fst>0){
					if($cosalm>0) $rowt->cosalm=number_format((($cosalm/($cosalm+$bormn+$totimpmn+$fst))*100), 2, '.', ',').' %';
					if($bormn>0) $rowt->bormn=number_format((($bormn/($cosalm+$bormn+$totimpmn+$fst))*100), 2, '.', ',').' %';
					if($totimpmn>0) $rowt->mqimn=number_format((($totimpmn/($cosalm+$bormn+$totimpmn+$fst))*100), 2, '.', ',').' %';
					if($fst>0) $rowt->fst=number_format((($fst/($cosalm+$bormn+$totimpmn+$fst))*100), 2, '.', ',').' %';
					
				} 
				$data[] = $rowt;	
			 endforeach;
			 

			//costo por kg
			 $this->db->select('max(ident)');
			 $resulta = $this->db->get($this->tablae);
			 foreach ($resulta->result() as $rowt):	
				$rowt->procesado='';$rowt->porpro='';$rowt->pe='';$rowt->ventas='';$rowt->us='';$rowt->existencia='';
				$rowt->da='';$rowt->cosalm='';$rowt->bordo='';$rowt->bormn='';$rowt->utilidad='';$rowt->porutil='';
				$rowt->mqkmn='';$rowt->mqimn='';$rowt->mqkus='';$rowt->mqius='';$rowt->fst='';
				$rowt->vkmn='';$rowt->vimn='';$rowt->vpmn='';$rowt->vkus='';$rowt->vius='';$rowt->vpus='';
				$rowt->nomt='Costo/Kg';$rowt->por='';$rowt->costot='';
				$rowt->existencia='';$rowt->ventas='';
				if($procesado>0){
					$rowt->vimn='$ '.number_format($tvimn/$procesado, 2, '.', ',');
					
				} 
				if($cosalm+$bormn+$totimpmn+$fst>0){
					if($cosalm>0) $rowt->cosalm='$ '.number_format($cosalm/$procesado, 2, '.', ',');
					if($bormn>0) $rowt->bormn='$ '.number_format($bormn/$procesado, 2, '.', ',');
					if($totimpmn>0) $rowt->mqimn='$ '.number_format($totimpmn/$procesado, 2, '.', ',');
					if($fst>0) $rowt->fst='$ '.number_format($fst/$procesado, 2, '.', ',');
					$rowt->costot='$ '.number_format(($cosalm/$procesado)+($bormn/$procesado)+($totimpmn/$procesado)+($fst/$procesado), 2, '.', ',');
				} 
				$data[] = $rowt;	
			 endforeach;
			 }
			return $data;
		}
		
		function getinventarios($filter,$cic,$alma){
			//DIA ACTUAL
			$hoy=date('Y-m-d');
			//SELECT `noma`, `nomg`, `nomt`, `cicent`, `sigg`, `alment`, `graent`, `talent` FROM (`entradas`) INNER JOIN `almacenes` ON `ida`=`alment` INNER JOIN `granjas` ON `idg`=`graent` INNER JOIN `tallas` ON `idt`=`talent` WHERE `cicent` = '19' AND `kgsent` > 0 GROUP BY `alment`, `graent`, `talent` ORDER BY `ida`, `nomg`, `idt`
			$this->db->select('noma,nomg,nomt,cicent,sigg,alment,graent,talent,ident');
			$this->db->join('almacenes', 'ida=alment','inner');
			$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->where('kgsent >',0);
			$this->db->group_by('alment');
			$this->db->group_by('graent');
			$this->db->group_by('talent');
			$this->db->order_by('ida');
			$this->db->order_by('nomg');
			$this->db->order_by('idt');					
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;$exit=0;$totg=0;
			if($result->num_rows()>0){
				 $feci=new Libreria();
			 foreach($result->result() as $row):
				$idtalla=$row->talent;$idgranja=$row->graent;$idalmacen=$row->alment;
				 $idSalida=$row->ident;$e=0;$s=0;$exi=0;
				 $row->due='';$row->ps='';$row->us='';$row->dps='';$row->dus='';
				 /* $data1 = array();
				 $row->pre=1;*/
				if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				//$e=$row->kgsent;
				//$row->kgsent=number_format(($row->kgsent), 2, '.', ',');
				// debo sumar todas las entradas....
				//SELECT idgp,idtp,nomt,sum(kgsp) as totkg from tallas inner join maqpro_19 on idtp=idt where idtp=2 and idgp=4 and idap=1
				$this->db->select('sum(kgsp) as totkg');	
				if($filter['where']!=''){
				$this->db->where('idtp',$idtalla);
				$this->db->where('idgp',$idgranja);
				$this->db->where('idap',$idalmacen);
				}
				$this->db->join($this->tablamp.$cic, 'idtp=idt','inner');
				//if($filter['where']!='') $this->db->where($filter['where']);
				$resultes = $this->db->get('tallas'); 
				foreach ($resultes->result() as $rowes):
					$e=$rowes->totkg;$tote+=$rowes->totkg;
					$row->kgsent=number_format(($rowes->totkg), 2, '.', ',');
				endforeach;
				//suma todas las salidas de esta entrada
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				//$this->db->group_by($idSalida);
				$result = $this->db->get($this->tablas);
				foreach ($result->result() as $rows):	
					$s=$rows->kgssal;
					$tots+=$rows->kgssal;			
					if($rows->kgssal > 0) $row->kgssal=number_format($rows->kgssal, 2, '.', ','); else $row->kgssal='';					
				endforeach;
				//buscamos la primera fecha de entrada 
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp limit 1
				$this->db->select('fecp');
				$this->db->where('idtp',$idtalla);
				$this->db->where('idgp',$idgranja);
				$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp');	
				$resultpe = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultpe->result() as $rowpe):	
					$row->pe=$feci->fecha23($rowpe->fecp);	
					$datetime1 = date_create($rowpe->fecp);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->dpe= $contador->format($differenceFormat);
				endforeach;
				//buscamos la ultima fecha de entrada 
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp limit 1
				$this->db->select('fecp');
				$this->db->where('idtp',$idtalla);
				$this->db->where('idgp',$idgranja);
				$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp','DESC');	
				$resultue = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultue->result() as $rowue):	
					$row->ue=$feci->fecha23($rowue->fecp);	
					$datetime1 = date_create($rowue->fecp);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->due= $contador->format($differenceFormat);				
				endforeach;
				$row->dus=$row->due;
				//fecha primera salida
				//select fecsal from salidas where talsal=7 and almsal=1 and grasal=4 order by fecsal limit 1
				$this->db->select('fecsal');	
				$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				$this->db->where('talsal',$idtalla);
				$this->db->where('grasal',$idgranja);
				$this->db->where('almsal',$idalmacen);
				$resultps = $this->db->get($this->tablas,1);
				foreach ($resultps->result() as $rowps):	
					$row->ps=$feci->fecha23($rowps->fecsal);	
					$datetime1 = date_create($rowps->fecsal);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->dps= $contador->format($differenceFormat);					
				endforeach;
				//fecha última salida
				$this->db->select('fecsal');	
				$this->db->where($this->idents,$idSalida);
				$this->db->where('ciclo','20'.$cic);
				$this->db->where('talsal',$idtalla);
				$this->db->where('grasal',$idgranja);
				$this->db->where('almsal',$idalmacen);
				$this->db->order_by('fecsal','DESC');
				$resultus = $this->db->get($this->tablas,1);
				foreach ($resultus->result() as $rowus):	
					$row->us=$feci->fecha23($rowus->fecsal);	
					$datetime1 = date_create($rowus->fecsal);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->dus= $contador->format($differenceFormat);					
				endforeach;
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp desc limit 1
				if($e-$s>1){ $row->kgsexi=number_format(($e-$s), 2, '.', ',');$exi=$e-$s; }
				if($exi>1){
				if($alm!=$row->noma){
					if($tot>0){
						$this->db->select('max(ident)');
			 			$resulta = $this->db->get($this->tablae);
			 			foreach ($resulta->result() as $rowt):	
							$rowt->cic='';$rowt->noma1 = "Total:"; $rowt->sigg='';$rowt->nomt='';$rowt->kgsent='';$rowt->kgssal='';
							$rowt->pe='';$rowt->ue='';$rowt->dpe='';$rowt->due='';$rowt->ps='';$rowt->us='';$rowt->dps='';$rowt->dus='';
							$rowt->kgsexi=number_format(($exit), 2, '.', ',');
							$data[] = $rowt;	
			 			endforeach;
					   $tot=0;$tote=0;$tots=0;$gra='';$exit=0;
					}		
					$alm=$row->noma;$row->noma1=$row->noma;
				}else{ $row->noma1="";}
				if($gra!=$row->sigg){$gra=$row->sigg;}else{ $row->sigg="";}
				$exit+=$e-$s;$totg+=$e-$s;
				$tot=1;
				}
				if($exi>1)
			 	 $data[] = $row;
			 endforeach;
			 if($exit>0){
						$this->db->select('max(ident)');
			 			$resulta = $this->db->get($this->tablae);
			 			foreach ($resulta->result() as $rowt):	
							$rowt->cic='';$rowt->noma1 = "Total:"; $rowt->sigg='';$rowt->nomt='';$rowt->kgsent='';$rowt->kgssal='';
							$rowt->pe='';$rowt->ue='';$rowt->dpe='';$rowt->due='';$rowt->ps='';$rowt->us='';$rowt->dps='';$rowt->dus='';
							$rowt->kgsexi=number_format(($exit), 2, '.', ',');
							$data[] = $rowt;	
			 			endforeach;
					   $tot=0;$tote=0;$tots=0;$gra='';
					}
			 if($totg>0 and $alma==0){
						$this->db->select('max(ident)');
			 			$resulta = $this->db->get($this->tablae);
			 			foreach ($resulta->result() as $rowt):	
							$rowt->cic='';$rowt->noma1 = "Total General:"; $rowt->sigg='';$rowt->nomt='';$rowt->kgsent='';$rowt->kgssal='';
							$rowt->pe='';$rowt->ue='';$rowt->dpe='';$rowt->due='';$rowt->ps='';$rowt->us='';$rowt->dps='';$rowt->dus='';
							$rowt->kgsexi=number_format(($totg), 2, '.', ',');
							$data[] = $rowt;	
			 			endforeach;
					   $tot=0;$tote=0;$tots=0;$gra='';
					}	
			}
			
			return $data;
		}
		
		function getinventariosg($filter,$cic,$alma,$gran){
			//DIA ACTUAL
			$hoy=date('Y-m-d');
			$this->db->select('noma,nomg,nomt,cicent,sigg,alment,graent,talent,ident,siga');
			$this->db->join('almacenes', 'ida=alment','inner');
			$this->db->join('granjas', 'idg=graent','inner');
			$this->db->join('tallas', 'idt=talent','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->group_by('alment');$this->db->group_by('graent');$this->db->group_by('talent');
			$this->db->order_by('ida');$this->db->order_by('nomg');$this->db->order_by('idt');					
			$result = $this->db->get($this->tablae);
			$data = array();$cic='';$alm='';$gra='';$tote=0;$tot=0;$tots=0;$idtalla=0;$idgranja=0;$exit=0;$totg=0;
			if($result->num_rows()>0){
				 $feci=new Libreria();
			 foreach($result->result() as $row):
				$idtalla=$row->talent;$idgranja=$row->graent;$idalmacen=$row->alment;
				 $idSalida=$row->ident;$e=0;$s=0;$exi=0;
				 $row->due='';$row->ps='';$row->us='';$row->dps='';$row->dus='';
				if($cic!=$row->cicent){$cic=$row->cicent;$row->cic='20'.$row->cicent;}else{ $row->cic="";}
				// debo sumar todas las entradas....
				//SELECT idgp,idtp,nomt,sum(kgsp) as totkg from tallas inner join maqpro_19 on idtp=idt where idtp=2 and idgp=4 and idap=1
				$this->db->select('sum(kgsp) as totkg');	
				if($filter['where']!=''){
				$this->db->where('idtp',$idtalla);$this->db->where('idgp',$idgranja);$this->db->where('idap',$idalmacen);
				}
				$this->db->join($this->tablamp.$cic, 'idtp=idt','inner');
				//if($filter['where']!='') $this->db->where($filter['where']);
				$resultes = $this->db->get('tallas'); 
				foreach ($resultes->result() as $rowes):
					$e=$rowes->totkg;$tote+=$rowes->totkg;
					$row->kgsent=number_format(($rowes->totkg), 2, '.', ',');
				endforeach;
				//suma todas las salidas de esta entrada
				$this->db->select('sum(kgssal) as kgssal');	
				$this->db->where($this->idents,$idSalida);$this->db->where('ciclo','20'.$cic);
				$result = $this->db->get($this->tablas);
				foreach ($result->result() as $rows):	
					$s=$rows->kgssal;$tots+=$rows->kgssal;			
					if($rows->kgssal > 0) $row->kgssal=number_format($rows->kgssal, 2, '.', ','); else $row->kgssal='';					
				endforeach;
				//buscamos la ultima fecha de entrada 
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp limit 1
				$this->db->select('fecp');
				$this->db->where('idtp',$idtalla);$this->db->where('idgp',$idgranja);$this->db->where('idap',$idalmacen);
				$this->db->order_by('fecp','DESC');	
				$resultue = $this->db->get($this->tablamp.$cic,1);
				foreach ($resultue->result() as $rowue):	
					$row->ue=$feci->fecha23($rowue->fecp);	
					$datetime1 = date_create($rowue->fecp);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->due= $contador->format($differenceFormat);				
				endforeach;
				$row->dus=$row->due;
				//fecha última salida
				$this->db->select('fecsal');	
				$this->db->where($this->idents,$idSalida);$this->db->where('ciclo','20'.$cic);$this->db->where('talsal',$idtalla);
				$this->db->where('grasal',$idgranja);$this->db->where('almsal',$idalmacen);
				$this->db->order_by('fecsal','DESC');
				$resultus = $this->db->get($this->tablas,1);
				foreach ($resultus->result() as $rowus):	
					$row->us=$feci->fecha23($rowus->fecsal);	
					$datetime1 = date_create($rowus->fecsal);
					$datetime2 = date_create($hoy);
					$contador = date_diff($datetime1, $datetime2);
					$differenceFormat = '%a';
					$row->dus= $contador->format($differenceFormat);					
				endforeach;
				//select fecp from maqpro_19 where idtp=7 and idap=1 and estp=1 order by fecp desc limit 1
				if($e-$s>1){ $row->kgsexi=number_format(($e-$s), 2, '.', '');$exi=$e-$s; }
				if($exi>1){
					if($alm!=$row->noma){
						$alm=$row->noma;$row->noma1=$row->siga;$gra='';
					}else{ $row->noma1='';}
					if($gra!=$row->sigg){$gra=$row->sigg;}else{ $row->sigg='';}
					$exit+=$e-$s;$totg+=$e-$s;
					if($alma==0 and $gran==0) $row->nomt=$row->noma1.'<br>'.$row->sigg.'<br>'.$row->nomt;
						elseif($alma!=0 and $gran==0) $row->nomt=$row->nomt.'<br>'.$row->sigg;
							elseif($alma==0 and $gran!=0) $row->nomt=$row->noma1.'<br>'.$row->nomt;
								elseif($alma!=0 and $gran!=0) $row->nomt=$row->nomt;
				}
				if($exi>1)	 $data[] = $row;
			 endforeach;
			}
			return $data;
		}
		
		function getvtaMR($filter,$tal,$cli,$cic){
			//DIA ACTUAL
			$hoy=date('Y-m-d');$mesact=date('m');$anoact=date('Y');
			$this->db->select('year(fecsal) as ano,month(fecsal) as mes, fecsal');	
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			//$this->db->group_by('grupo'); 
			$this->db->group_by('year(fecsal)');
			$this->db->group_by('month(fecsal)');
			$query = $this->db->get('salidas',1);
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
				$mesini='';
			foreach($query->result() as $row):
				//$diasmes = cal_days_in_month(CAL_GREGORIAN, $row->mes, $row->ano);
				$fechainicial = new DateTime($row->fecsal);
				$fechafinal = new DateTime($hoy);
				$diferencia = $fechainicial->diff($fechafinal);
				$meses = ( $diferencia->y * 12 ) + $diferencia->m;
				$mesini=$row->fecsal;
			endforeach;		
			$mesT='';
			$mes1=0;$imna=0;$depmna=0;$ao=0;$kgsa=0;$cont=0;
			while ($mes1 <= ($meses+1)) {
				if($mes1==0){$mes=date("m",strtotime($mesini));$ano=date("Y",strtotime($mesini));} 
				else{$mes=date("m",strtotime($mesini."+ ".$mes1." month"));$ano=date("Y",strtotime($mesini."+ ".$mes1." month"));}
				if(($ano.$mes)<=($anoact.$mesact)){
				$this->db->select('max(numero)');			
				$resultZ = $this->db->get('clientes');
				foreach ($resultZ->result() as $row):
					$row->porinc='';$ti=0;$tv=0;$tp=0;
					$row->kgs='';$row->imvm='';$row->imna='';
					$row->depmn='';$row->depmna='';
				//ventas	
					$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
					$this->db->join('tallas', 'idt=talsal', 'inner');
					if($filter['where']!=''){$this->db->where($filter['where']); }
					$this->db->where('estsal',1);$this->db->where('usdkgs',0);
					if($tal>0) $this->db->where('gpoid',$tal);
					$this->db->where('month(fecsal)',$mes);
					$this->db->where('year(fecsal)',$ano);
					$querym = $this->db->get('salidas');	
					foreach($querym->result() as $rowm):
						$imna+=$rowm->imp;$kgsa+=$rowm->kgs;$ti+=$rowm->imp;$tv+=$rowm->kgs;
						/*if($rowm->kgs>0) $row->kgs=number_format(($rowm->kgs), 2, '.', ','); else $row->kgs='';
						$row->kgsa=number_format(($kgsa), 2, '.', ',');
						if($rowm->imp>0) $row->imvm='$'.number_format(($rowm->imp), 2, '.', ','); else $row->imvm='';
						if($imna>0) $row->imna='$'.number_format(($imna), 2, '.', ','); else $row->imna='';*/ 
					endforeach;
					//cargos
					$this->db->select('sum(Pesos) as pesos');
					if($cli>0) $this->db->where('NRC',$cli);
					$this->db->where('bormaq',2);$this->db->where('Des',0);$this->db->where('Car',-1);$this->db->where('ImporteD',0);
					$this->db->where('month(Fecha)',$mes);
					$this->db->where('year(Fecha)',$ano);
					//$this->db->where('Aplicar',2019);
					$querycar = $this->db->get('depositos');	
					foreach($querycar->result() as $rowcar):
						$imna+=$rowcar->pesos;$ti+=$rowcar->pesos;
					endforeach;	
					$this->db->select('sum(kgssal) as kgs,sum(kgssal*(usdkgs*tcv))as imp');
					$this->db->join('tallas', 'idt=talsal', 'inner');
					if($filter['where']!=''){$this->db->where($filter['where']); }
					$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
					if($tal>0) $this->db->where('gpoid',$tal);
					$this->db->where('month(fecsal)',$mes);
					$this->db->where('year(fecsal)',$ano);
					$querym = $this->db->get('salidas');	
					foreach($querym->result() as $rowm):
						//$imna+=$rowm->imp;$kgsa+=$rowm->kgs;$ti+=$rowm->imp;$tv+=$rowm->kgs;
					endforeach;	
					if($tv>0) $row->kgs=number_format(($tv), 2, '.', ','); 
					if($kgsa>0)	$row->kgsa=number_format(($kgsa), 2, '.', ',');
					if($ti>0) $row->imvm='$'.number_format(($ti), 2, '.', ','); 
					if($imna>0) $row->imna='$'.number_format(($imna), 2, '.', ',');  
				//Pagos		
					$this->db->select('sum(Pesos) as pesos');
					if($cli>0) $this->db->where('NRC',$cli);
					$this->db->where('bormaq',2);$this->db->where('Des',0);$this->db->where('Car',0);$this->db->where('ImporteD',0);
					$this->db->where('month(Fecha)',$mes);
					$this->db->where('year(Fecha)',$ano);
					//$this->db->where('Aplicar',2019);
					$querymd = $this->db->get('depositos');	
					foreach($querymd->result() as $rowmd):
						$depmna+=$rowmd->pesos;$tp+=$rowmd->pesos;
					endforeach;
					//descuentos
					$this->db->select('sum(Pesos) as pesos');
					if($cli>0) $this->db->where('NRC',$cli);
					$this->db->where('bormaq',2);$this->db->where('Des',-1);$this->db->where('Car',0);$this->db->where('ImporteD',0);
					$this->db->where('month(Fecha)',$mes);
					$this->db->where('year(Fecha)',$ano);
					//$this->db->where('Aplicar',2019);
					$querymd = $this->db->get('depositos');	
					foreach($querymd->result() as $rowmd):
						$depmna+=$rowmd->pesos;$tp+=$rowmd->pesos;
					endforeach;
					$this->db->select('sum(ImporteD*TC) as pesos');
					if($cli>0) $this->db->where('NRC',$cli);
					$this->db->where('bormaq',2);$this->db->where('Des',0);$this->db->where('Car',0);$this->db->where('ImporteD >',0);
					$this->db->where('month(Fecha)',$mes);
					$this->db->where('year(Fecha)',$ano);
					//$this->db->where('Aplicar',2019);
					$querymd = $this->db->get('depositos');	
					foreach($querymd->result() as $rowmd):
						//$depmna+=$rowmd->pesos;$tp+=$rowmd->pesos;
					endforeach;
					
					if($tp>0) $row->depmn='$'.number_format(($tp), 2, '.', ','); 
					if($depmna>0) $row->depmna='$'.number_format(($depmna), 2, '.', ',');  
					
					$row->salmn='$'.number_format(($imna-$depmna), 2, '.', ',');
					$row->salmni=number_format(($imna-$depmna), 2, '.', '');
					switch ($mes) {
						case '1':$row->mes='Ene';break;	case '2':$row->mes='Feb';break;	case '3':$row->mes='Mar';break;
						case '4':$row->mes='Abr';break;	case '5':$row->mes=' May';break;	case '6':$row->mes=' Jun';break;
						case '7':$row->mes=' Jul';break;	case '8':$row->mes=' Ago';break;	case '9':$row->mes=' Sep';break;
						case '10':$row->mes=' Oct';break; case '11':$row->mes=' Nov';break; case '12':$row->mes=' Dic';break;
					}
					//$row->mes=$row->mes;
					if($ao!=$ano){ $row->ano=$ano; $ao=$ano;}else $row->ano='' ;
					if($imna>0){
						$row->pormn=number_format((($imna-$depmna)/$imna)*100, 2, '.', '');
						
					} else {$row->pormn='';}
					if($cont==0){ $san=$imna-$depmna; $row->porinc=''; $cont=1;}
						else { if($imna-$depmna>0 ){$row->porinc=number_format(((($imna-$depmna)-$san)/($imna-$depmna))*100, 2, '.', ''); ;$san=$imna-$depmna;} }
					
					//$row->imna='$'.number_format($imna, 2, '.', ',');
					//$row->depmn='$'.number_format($depmn, 2, '.', ',');
					/*switch ($mes) {
						case '1':$row->mes=$row->mes.'<br>'.$ano;break;
					}*/
				endforeach;	 
				$data[] = $row;
				}
				$mes1+=1;
			}
			
			
			}
			return $data;
		}
		
		function getvtaMRG($filter,$tal,$cli,$cic){
			//DIA ACTUAL
			$hoy=date('Y-m-d');$mesact=date('m');$anoact=date('Y');
			$this->db->select('year(fecsal) as ano,month(fecsal) as mes, fecsal');	
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			//$this->db->group_by('grupo'); 
			$this->db->group_by('year(fecsal)');
			$this->db->group_by('month(fecsal)');
			$query = $this->db->get('salidas',1);
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
				$mesini='';
			foreach($query->result() as $row):
				//$diasmes = cal_days_in_month(CAL_GREGORIAN, $row->mes, $row->ano);
				$fechainicial = new DateTime($row->fecsal);
				$fechafinal = new DateTime($hoy);
				$diferencia = $fechainicial->diff($fechafinal);
				$meses = ( $diferencia->y * 12 ) + $diferencia->m;
				$mesini=$row->fecsal;
			endforeach;		
			$mesT='';
			$mes1=0;$imna=0;$depmn=0;
			while ($mes1 <= ($meses+1)) {
				if($mes1==0){$mes=date("m",strtotime($mesini));$ano=date("Y",strtotime($mesini));} 
				else{$mes=date("m",strtotime($mesini."+ ".$mes1." month"));$ano=date("Y",strtotime($mesini."+ ".$mes1." month"));}
				if(($ano.$mes)<=($anoact.$mesact)){
				$this->db->select('max(numero)');			
				$resultZ = $this->db->get('clientes');
				foreach ($resultZ->result() as $row):
					
					$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
					$this->db->join('tallas', 'idt=talsal', 'inner');
					if($filter['where']!=''){$this->db->where($filter['where']); }
					$this->db->where('estsal',1);$this->db->where('usdkgs',0);
					if($tal>0) $this->db->where('gpoid',$tal);
					$this->db->where('month(fecsal)',$mes);
					$this->db->where('year(fecsal)',$ano);
					$querym = $this->db->get('salidas');	
					foreach($querym->result() as $rowm):
						$imna+=$rowm->imp;
						$row->imna=number_format(($imna), 2, '.', '');
					endforeach;
					$this->db->select('sum(Pesos) as pesos');
					if($cli>0) $this->db->where('NRC',$cli);
					$this->db->where('bormaq',2);$this->db->where('Des',0);$this->db->where('Car',0);$this->db->where('Pesos >',0);
					$this->db->where('month(Fecha)',$mes);
					$this->db->where('year(Fecha)',$ano);
					$querymd = $this->db->get('depositos');	
					foreach($querymd->result() as $rowmd):
						$depmn+=$rowmd->pesos;
						$row->depmn=number_format(($depmn), 2, '.', '');
					endforeach;
					$row->salmn=number_format(($imna-$depmn), 2, '.', '');
					
					switch ($mes) {
						case '1':$row->mes='Ene';break;	case '2':$row->mes='Feb';break;	case '3':$row->mes='Mar';break;
						case '4':$row->mes='Abr';break;	case '5':$row->mes=' May';break;	case '6':$row->mes=' Jun';break;
						case '7':$row->mes=' Jul';break;	case '8':$row->mes=' Ago';break;	case '9':$row->mes=' Sep';break;
						case '10':$row->mes=' Oct';break; case '11':$row->mes=' Nov';break; case '12':$row->mes=' Dic';break;
					}
					$row->mes=$row->mes.' <br>'.number_format((($imna-$depmn)/$imna)*100, 2, '.', '').' %';
					switch ($mes) {
						case '1':$row->mes=$row->mes.'<br>'.$ano;break;
					}
				endforeach;	 
				$data[] = $row;
				}
				$mes1+=1;
			}
			
			
			}
			return $data;
		}
		function getvtaMR1($filter,$tal,$cli){
			//SELECT year(fecsal) as ao,month(fecsal) as mes, sum(kgssal) as kgs from salidas group by month(fecsal) order by year(fecsal),month(fecsal)
			$this->db->select('year(fecsal) as ano,month(fecsal) as mes, sum(kgssal) as kgs');	
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			//$this->db->group_by('grupo'); 
			$this->db->group_by('year(fecsal)');
			$this->db->group_by('month(fecsal)');
			$query = $this->db->get('salidas');
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
			
			 $kgsa=0;$imna=0;$depmn=0;$ano='';$tkmn=0;$timn=0;$tkus=0;$tius=0;$gra=1;$mn=0;$usd=0;
			foreach($query->result() as $row):
				$mes=$row->mes;$ano=$row->ano;
				//$row->ano=substr($row->ano, 2);
				switch ($row->mes) {
					case '1':$row->mes='Ene <br>'.$row->ano;break;	case '2':$row->mes='Feb';break;	case '3':$row->mes='Mar';break;
					case '4':$row->mes='Abr';break;	case '5':$row->mes=' May';break;	case '6':$row->mes=' Jun';break;
					case '7':$row->mes=' Jul';break;	case '8':$row->mes=' Ago';break;	case '9':$row->mes=' Sep';break;
					case '10':$row->mes=' Oct';break; case '11':$row->mes=' Nov';break; case '12':$row->mes=' Dic';break;
				}
				$kgsa+=$row->kgs;
				$row->kgsa=(float)number_format(($kgsa), 2, '.', '');
				
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('month(fecsal)',$mes);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$imna+=$rowm->imp;
					$row->imna=number_format(($imna), 2, '.', '');
				endforeach;
				
				$this->db->select('sum(Pesos) as pesos');
				//if($filter['where']!=''){$this->db->where($filter['where']); }
				if($cli>0) $this->db->where('NRC',$cli);
				$this->db->where('bormaq',2);$this->db->where('Des',0);$this->db->where('Car',0);$this->db->where('Pesos >',0);
				$this->db->where('month(Fecha)',$mes);
				$this->db->where('year(Fecha)',$ano);
				$querymd = $this->db->get('depositos');	
				foreach($querymd->result() as $rowmd):
					$depmn+=$rowmd->pesos;
					$row->depmn=number_format(($depmn), 2, '.', '');
				endforeach;
				
				$row->salmn=number_format(($imna-$depmn), 2, '.', '');
				//usd
				/*
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->group_by('grupo');
				$this->db->where('month(fecsal)',$mes);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus+=$rowm->kgs;$tius+=$rowm->imp;$usd=number_format(($rowm->imp), 2, '.', '');
					if($rowm->kgs>0)$row->kus=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus=$rowm->imp/$rowm->kgs;}
				endforeach;
				*/
				/*	
				$datos[$gra] =array($gra,$row->mes,(float)$kilos,$row->por,$row->ano,$row->imn,$row->ius,$pvmn,$pvus);$gra+=1;
				*/ 
				$data[] = $row;	
			endforeach;
			
			}
			return $data;
		}
		
		function getvtaCT($filter,$cli1,$cli2){
			//SELECT year(fecsal) as ao,month(fecsal) as mes, sum(kgssal) as kgs from salidas group by month(fecsal) order by year(fecsal),month(fecsal)
			$this->db->select('grupo,talsal,nomt,sum(kgssal) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');	
			if($filter['where']!=''){$this->db->where($filter['where']); }
			//if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('clisal',$cli1);
			$this->db->where('estsal',1);
			$this->db->group_by('grupo'); //$this->db->group_by('talsal');$this->db->group_by('nomt');
			$this->db->order_by('sum(kgssal)','DESC');
			$query = $this->db->get('salidas');
			//$totuni=$query->num_rows();
			$data = array();$datosmes = array();
			if($query->num_rows()>0){
			//Se obtiene el total de lo vendido
			//SELECT FORMAT(sum(kgssal),2) as kgs from salidas where estsal=1
			$this->db->select('sum(kgssal) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			//if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('clisal',$cli1);
			$this->db->where('estsal',1);
			$querym = $this->db->get('salidas');	
			$totkgs=0;
			foreach($querym->result() as $rowm):
				$totkgs=$rowm->kgs;
			endforeach;
			$contx=1;$kilos=0;
			while ($contx <= 15) {
			  //$datos[$contx] =array('','','','','','',''); 
			  $datoscom[$contx] =array('','','','','','','','','','','','','','','','','');
			  $contx+=1;
			}
			$gra=1;$ano='';$tkmn1=0;$timn1=0;$tkus1=0;$tius1=0;$mn1=0;$usd1=0;$tkmn2=0;$timn2=0;$tkus2=0;$tius2=0;$mn2=0;$usd2=0;
			$tkmnd1=0;$timnd1=0;$tkmnd2=0;$timnd2=0;$tkusdd1=0;$tiusdd1=0;$tkusdd2=0;$tiusdd2=0;
			foreach($query->result() as $row):
				$kmnd1=0;$imnd1=0;$kmnd2=0;$imnd2=0;$kusdd1=0;$iusdd1=0;$kusdd2=0;$iusdd2=0;$pvmn1=0;$pvus1=0;$pvmn2=0;$pvus2=0;
				$row->por='';
				$row->kmn1='';$row->imn1='';$row->pvmn1='';
				$row->kmn2='';$row->imn2='';$row->pvmn2='';
				$row->kmnd='';$row->imnd='';$row->pvmnd='';
				
				$row->kus1='';$row->ius1='';$row->pvus1='';
				$row->kus2='';$row->ius2='';$row->pvus2='';
				$row->kusd='';$row->iusd='';$row->pvusd='';
				
				$kilos=number_format(($row->kgs), 2,'.','');
				if($totkgs>0)$row->por=number_format(($row->kgs/$totkgs)*100, 2, '.', ',');
				$row->kgs=number_format(($row->kgs), 2, '.', ',');
				//cliente 1
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('clisal',$cli1);
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkmn1+=$rowm->kgs;$timn1+=$rowm->imp;$mn1=number_format(($rowm->imp), 2, '.', ',');
					$kmnd1=$rowm->kgs;$tkmnd1+=$rowm->kgs;$imnd1=$rowm->imp;$timnd1+=$rowm->imp;
					if($rowm->kgs>0)$row->kmn1=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->imn1='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvmn1='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvmn1=$rowm->imp/$rowm->kgs;}
				endforeach;
				//usd
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('clisal',$cli1);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus1+=$rowm->kgs;$tius1+=$rowm->imp;$usd1=number_format(($rowm->imp), 2, '.', '');
					$kusdd1=$rowm->kgs;$tkusdd1+=$rowm->kgs;$iusdd1=$rowm->imp;$tiusdd1+=$rowm->imp;
					if($rowm->kgs>0)$row->kus1=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius1='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus1='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus1=$rowm->imp/$rowm->kgs;}
				endforeach;
				//cliente 2
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('clisal',$cli2);
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkmn2+=$rowm->kgs;$timn2+=$rowm->imp;$mn2=number_format(($rowm->imp), 2, '.', ',');
					$kmnd2=$rowm->kgs;$tkmnd2+=$rowm->kgs;$imnd2=$rowm->imp;$timnd2+=$rowm->imp;
					if($rowm->kgs>0)$row->kmn2=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->imn2='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvmn2='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvmn2=$rowm->imp/$rowm->kgs;}
				endforeach;
				//usd
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('clisal',$cli2);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus2+=$rowm->kgs;$tius2+=$rowm->imp;$usd2=number_format(($rowm->imp), 2, '.', '');
					$kusdd2=$rowm->kgs;$tkusdd2+=$rowm->kgs;$iusdd2=$rowm->imp;$tiusdd2+=$rowm->imp;
					if($rowm->kgs>0)$row->kus2=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius2='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus2='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus2=$rowm->imp/$rowm->kgs;}
				endforeach;	
				//diferencias
				if($row->kmn1!='' or $row->kmn2!='') $row->kmnd=number_format(($kmnd1-$kmnd2), 2, '.', ',');	
				if($row->imn1!='' or $row->imn2!='') $row->imnd=number_format(($imnd1-$imnd2), 2, '.', ',');
				if($row->pvmn1!='' or $row->pvmn2!='') $row->pvmnd=number_format(($pvmn1-$pvmn2), 2, '.', '');
				if($row->kus1!='' or $row->kus2!='') $row->kusd=number_format(($kusdd1-$kusdd2), 2, '.', ',');
				if($row->ius1!='' or $row->ius2!='') $row->iusd=number_format(($iusdd1-$iusdd2), 2, '.', ',');
				if($row->pvus1!='' or $row->pvus2!='') $row->pvusd=number_format(($pvus1-$pvus2), 2, '.', '');
				$datoscom[$gra] =array($gra,$row->grupo,number_format(($kmnd1+$kmnd2), 2, '.', ',').' kgs','$ '.number_format(($imnd1+$imnd2), 2, '.', ','),'$ '.number_format(((($pvmn1+$pvmn2)/2)), 2, '.', ','),
										$kmnd1,$imnd1,$pvmn1,$kmnd2,$imnd2,$pvmn2,'[+/-] $ '.number_format(($pvmn1-$pvmn2), 2, '.', ','));$gra+=1;
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->mes='';$rowZ->por='';$rowZ->kmn1='';$rowZ->imn1='';$rowZ->kus1='';$rowZ->ius1=''; $rowZ->pvmn1=''; $rowZ->pvus1='';
				$rowZ->kmn2='';$rowZ->imn2='';$rowZ->kus2='';$rowZ->ius2=''; $rowZ->pvmn2=''; $rowZ->pvus2='';
				$rowZ->kmnd='';$rowZ->imnd='';$rowZ->kusd='';$rowZ->iusd=''; $rowZ->pvmnd=''; $rowZ->pvusd='';
				$rowZ->grupo='Total';
				if($totkgs>0) $rowZ->kgs=number_format(($totkgs), 2, '.', ',');
				if($tkmn1>0) $rowZ->kmn1=number_format(($tkmn1), 2, '.', ',');
				if($timn1>0) $rowZ->imn1='$'.number_format(($timn1), 2, '.', ',');
				if($tkmn1>0 && $timn1>0) $rowZ->pvmn1='$'.number_format(($timn1/$tkmn1), 2, '.', ',');
				if($tkus1>0) $rowZ->kus1=number_format(($tkus1), 2, '.', ',');
				if($tius1>0) $rowZ->ius1='$'.number_format(($tius1), 2, '.', ',');
				if($tkus1>0 && $tius1>0) $rowZ->pvus1='$'.number_format(($tius1/$tkus1), 2, '.', ',');
				if($tkmn2>0) $rowZ->kmn2=number_format(($tkmn2), 2, '.', ',');
				if($timn2>0) $rowZ->imn2='$'.number_format(($timn2), 2, '.', ',');
				if($tkmn2>0 && $timn2>0) $rowZ->pvmn2='$'.number_format(($timn2/$tkmn2), 2, '.', ',');
				if($tkus2>0) $rowZ->kus2=number_format(($tkus2), 2, '.', ',');
				if($tius2>0) $rowZ->ius2='$'.number_format(($tius2), 2, '.', ',');
				if($tkus2>0 && $tius2>0) $rowZ->pvus2='$'.number_format(($tius2/$tkus2), 2, '.', ',');
				if($rowZ->kmn1!='' || $rowZ->kmn2!='') $rowZ->kmnd=number_format(($tkmnd1-$tkmnd2), 2, '.', ',');
				if($rowZ->imn1!='' || $rowZ->imn2!='') $rowZ->imnd='$'.number_format(($timnd1-$timnd2), 2, '.', ',');
				if($tkmn1>0 and $tkmn2>0) $rowZ->pvmnd=number_format((($timn1/$tkmn1)-($timn2/$tkmn2)), 2, '.', ',');
				if($rowZ->kus1!='' || $rowZ->kus2!='') $rowZ->kusd=number_format(($tkusdd1-$tkusdd2), 2, '.', ',');
				if($rowZ->ius1!='' || $rowZ->ius2!='') $rowZ->iusd='$'.number_format(($tiusdd1-$tiusdd2), 2, '.', ',');
				if($tkusdd1>0 and $tkusdd2>0) $rowZ->pvusd=number_format((($tiusdd1/$tkusdd1)-($tiusdd2/$tkusdd2)), 2, '.', ',');
				
				$ini=1;
				while($ini<=$gra-1){
				 switch($datoscom [$ini][0]){
					case '1': $rowZ->t1 =$datoscom [$ini][1];
								$rowZ->t1vt =$datoscom [$ini][2];$rowZ->t1vi =$datoscom [$ini][3];$rowZ->t1vp =$datoscom [$ini][4];
								$rowZ->t1vt1 =$datoscom [$ini][5];$rowZ->t1vi1 =$datoscom [$ini][6];$rowZ->t1vp1 =$datoscom [$ini][7];
								$rowZ->t1vt2 =$datoscom [$ini][8];$rowZ->t1vi2 =$datoscom [$ini][8];$rowZ->t1vp2 =$datoscom [$ini][10];
								$rowZ->t1gp =$datoscom [$ini][11];
							 break;
				
					case '2': $rowZ->t2 =$datoscom [$ini][1];
								$rowZ->t2vt =$datoscom [$ini][2];$rowZ->t2vi =$datoscom [$ini][3];$rowZ->t2vp =$datoscom [$ini][4];
								$rowZ->t2vt1 =$datoscom [$ini][5];$rowZ->t2vi1 =$datoscom [$ini][6];$rowZ->t2vp1 =$datoscom [$ini][7];
								$rowZ->t2vt2 =$datoscom [$ini][8];$rowZ->t2vi2 =$datoscom [$ini][8];$rowZ->t2vp2 =$datoscom [$ini][10];
								$rowZ->t2gp =$datoscom [$ini][11];
							 break;
					case '3': $rowZ->t3 =$datoscom [$ini][1];
								$rowZ->t3vt =$datoscom [$ini][2];$rowZ->t3vi =$datoscom [$ini][3];$rowZ->t3vp =$datoscom [$ini][4];
								$rowZ->t3vt1 =$datoscom [$ini][5];$rowZ->t3vi1 =$datoscom [$ini][6];$rowZ->t3vp1 =$datoscom [$ini][7];
								$rowZ->t3vt2 =$datoscom [$ini][8];$rowZ->t3vi2 =$datoscom [$ini][8];$rowZ->t3vp2 =$datoscom [$ini][10];
								$rowZ->t3gp =$datoscom [$ini][11];
							break;
					case '4': $rowZ->t4 =$datoscom [$ini][1];
								$rowZ->t4vt =$datoscom [$ini][2];$rowZ->t4vi =$datoscom [$ini][3];$rowZ->t4vp =$datoscom [$ini][4];
								$rowZ->t4vt1 =$datoscom [$ini][5];$rowZ->t4vi1 =$datoscom [$ini][6];$rowZ->t4vp1 =$datoscom [$ini][7];
								$rowZ->t4vt2 =$datoscom [$ini][8];$rowZ->t4vi2 =$datoscom [$ini][8];$rowZ->t4vp2 =$datoscom [$ini][10];	
								$rowZ->t4gp =$datoscom [$ini][11];
							break;
					case '5': $rowZ->t5 =$datoscom [$ini][1];
								$rowZ->t5vt =$datoscom [$ini][2];$rowZ->t5vi =$datoscom [$ini][3];$rowZ->t5vp =$datoscom [$ini][4];
								$rowZ->t5vt1 =$datoscom [$ini][5];$rowZ->t5vi1 =$datoscom [$ini][6];$rowZ->t5vp1 =$datoscom [$ini][7];
								$rowZ->t5vt2 =$datoscom [$ini][8];$rowZ->t5vi2 =$datoscom [$ini][8];$rowZ->t5vp2 =$datoscom [$ini][10];
								$rowZ->t5gp =$datoscom [$ini][11];
							break;
					case '6': $rowZ->t6 =$datoscom [$ini][1];
								$rowZ->t6vt =$datoscom [$ini][2];$rowZ->t6vi =$datoscom [$ini][3];$rowZ->t6vp =$datoscom [$ini][4];
								$rowZ->t6vt1 =$datoscom [$ini][5];$rowZ->t6vi1 =$datoscom [$ini][6];$rowZ->t6vp1 =$datoscom [$ini][7];
								$rowZ->t6vt2 =$datoscom [$ini][8];$rowZ->t6vi2 =$datoscom [$ini][8];$rowZ->t6vp2 =$datoscom [$ini][10];	
								$rowZ->t6gp =$datoscom [$ini][11];
							break;
				 }
				 $ini+=1;
			    }
				
				$data[] = $rowZ;	
			endforeach;	
			
			}
			return $data;
		}
		
		
		function getvtaMT($filter,$tal,$rep){
			//SELECT year(fecsal) as ao,month(fecsal) as mes, sum(kgssal) as kgs from salidas group by month(fecsal) order by year(fecsal),month(fecsal)
			if($rep==0) $this->db->select('grupo,talsal,nomt,sum(kgssal) as kgs');
			elseif($rep==1) $this->db->select('grupo,talsal,nomt,sum(kgssal*prekgs)as kgs');
				elseif($rep==2) $this->db->select('grupo,talsal,nomt,sum(kgssal*usdkgs)as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');	
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			/*if($rep==0) $this->db->select('grupo,talsal,nomt,sum(kgssal) as kgs');
			elseif($rep==1) $this->db->where('prekgs >',0);
				else
			 */
			 if($rep==1) $this->db->where('usdkgs =',0);
			 elseif($rep==2) $this->db->where('usdkgs >',0);
			$this->db->group_by('grupo'); //$this->db->group_by('talsal');$this->db->group_by('nomt');
			if($rep==0) $this->db->order_by('sum(kgssal)','DESC');
			elseif($rep==1) $this->db->order_by('sum(kgssal*prekgs)','DESC');
				elseif($rep==2) $this->db->order_by('sum(kgssal*usdkgs)','DESC');
			$query = $this->db->get('salidas');
			//$totuni=$query->num_rows();
			$data = array();$datosmes = array();
			//if($query->num_rows()>0){
			//Se obtiene el total de lo vendido
			//SELECT FORMAT(sum(kgssal),2) as kgs from salidas where estsal=1
			if($rep==0) $this->db->select('sum(kgssal) as kgs');
			elseif($rep==1) $this->db->select('sum(kgssal*prekgs) as kgs');
				elseif($rep==2) $this->db->select('sum(kgssal*usdkgs) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$querym = $this->db->get('salidas');	
			foreach($querym->result() as $rowm):
				$totkgs=$rowm->kgs;
			endforeach;
			//Saco los meses de entregas
			$contx=1;$gra=1;$graf=1;$anom=0;
			while ($contx <= 16) {  $datosmes[$contx] =array('','','','','',''); $contx+=1;}
			$this->db->select('year(fecsal) as ano,month(fecsal) as mes');	
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$this->db->group_by('year(fecsal)');
			$this->db->group_by('month(fecsal)');
			$this->db->order_by('year(fecsal)');$this->db->order_by('month(fecsal)');
			$querym = $this->db->get('salidas');	
			foreach($querym->result() as $rowm):
				switch ($rowm->mes) {
					case '1':$mes='Ene';break;	case '2':$mes='Feb';break;	case '3':$mes='Mar';break;
					case '4':$mes='Abr';break;	case '5':$mes='May';break;	case '6':$mes='Jun';break;
					case '7':$mes='Jul';break;	case '8':$mes='Ago';break;	case '9':$mes='Sep';break;
					case '10':$mes='Oct';break; case '11':$mes='Nov';break; case '12':$mes='Dic';break;
				}
				if($anom!=$rowm->ano){
					$anom=$rowm->ano;	
					$datosmes[$graf] =array($graf,$rowm->ano.'<br>'.$mes,$rowm->ano,$rowm->mes,$rowm->ano,$mes);
				}else{ $datosmes[$graf] =array($graf,'<br>'.$mes,$rowm->ano,$rowm->mes,'',$mes);}
				
				$graf+=1;
			endforeach;
			
			$contx=1;$kilos=0;
			while ($contx <= 16) {
			  $datos[$contx] =array('','','','','','',''); 
			  $datostm[$contx] =array('','','','','','','','','','','','','','','','','');
			  $contx+=1;
			}
			$gra=1;$ano='';$mn=0;$usd=0;$entro=1;
			$tm1=0;$tm2=0;$tm3=0;$tm4=0;$tm5=0;$tm6=0;$tm7=0;$tm8=0;$tm9=0;$tm10=0;$tm11=0;$tm12=0;$tm13=0;$tm14=0;$tm15=0;
			foreach($query->result() as $row):
				//encabezados de meses
				if($entro==1){
					$cuenta=1;
					$this->db->select('max(numero)');			
					$resultZ = $this->db->get('clientes');
					foreach ($resultZ->result() as $rowZ):
						if($rep==0) $rowZ->kgs='Kgs';
						elseif($rep==1) $rowZ->kgs='M.N.';
							elseif($rep==2) $rowZ->kgs='U.S.D.';
						$rowZ->por='';$rowZ->grupo='';
						$rowZ->m1='';$rowZ->m2='';$rowZ->m3='';$rowZ->m4='';$rowZ->m5='';$rowZ->m6='';$rowZ->m7='';$rowZ->m8='';$rowZ->m9='';
						$rowZ->m10='';$rowZ->m11='';$rowZ->m12='';$rowZ->m13='';$rowZ->m14='';$rowZ->m15='';
						while($cuenta<=$graf-1){
							switch ($datosmes [$cuenta][0]) {
								case '1': $rowZ->m1=$datosmes [$cuenta][1]; break;
								case '2': $rowZ->m2=$datosmes [$cuenta][1]; break;
								case '3': $rowZ->m3=$datosmes [$cuenta][1]; break;
								case '4': $rowZ->m4=$datosmes [$cuenta][1]; break;
								case '5': $rowZ->m5=$datosmes [$cuenta][1]; break;
								case '6': $rowZ->m6=$datosmes [$cuenta][1]; break;
								case '7': $rowZ->m7=$datosmes [$cuenta][1]; break;
								case '8': $rowZ->m8=$datosmes [$cuenta][1]; break;
								case '9': $rowZ->m9=$datosmes [$cuenta][1]; break;
								case '10': $rowZ->m10=$datosmes [$cuenta][1]; break;
								case '11': $rowZ->m11=$datosmes [$cuenta][1]; break;
								case '12': $rowZ->m12=$datosmes [$cuenta][1]; break;
								case '13': $rowZ->m13=$datosmes [$cuenta][1]; break;
								case '14': $rowZ->m14=$datosmes [$cuenta][1]; break;
								case '15': $rowZ->m15=$datosmes [$cuenta][1]; break;
							}
							$cuenta+=1;
						}
						$data[] = $rowZ;
					endforeach;	
				}
				$entro=2;
				$row->por='';$row->m1='';$row->m2='';$row->m3='';$row->m4='';$row->m5='';$row->m6='';$row->m7='';$row->m8='';$row->m9='';
				$row->m10='';$row->m11='';$row->m12='';$row->m13='';$row->m14='';$row->m15='';
				$kilos=number_format(($row->kgs), 2,'.','');
				if($totkgs>0)$row->por=number_format(($row->kgs/$totkgs)*100, 2, '.', ',');
				$row->kgs=number_format(($row->kgs), 2, '.', ',');
				// SACO DE ACUERDO A LA TALLA LO DEL MES
				if($rep==0) $this->db->select('year(fecsal) as ano,month(fecsal) as mes,month(fecsal)as mesi,sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				elseif($rep==1) $this->db->select('year(fecsal) as ano,month(fecsal) as mes,month(fecsal)as mesi,sum(kgssal*prekgs) as kgs,sum(kgssal*prekgs)as imp');
					elseif($rep==2) $this->db->select('year(fecsal) as ano,month(fecsal) as mes,month(fecsal)as mesi,sum(kgssal*usdkgs) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('estsal',1);
				$this->db->where('grupo',$row->grupo);
				$this->db->group_by('year(fecsal)');
				$this->db->group_by('month(fecsal)');
				$querymes = $this->db->get('salidas');	
				$cuenta=1;$m1=0;$m2=0;$m3=0;$m4=0;$m5=0;$m6=0;$m7=0;$m8=0;$m9=0;$m10=0;$m11=0;$m12=0;$m13=0;$m14=0;$m15=0;
				foreach($querymes->result() as $rowmes):
					switch ($rowmes->ano.$rowmes->mes) {
						case $datosmes [$cuenta][2].$datosmes [$cuenta][3]: $row->m1=number_format(($rowmes->kgs), 2, '.', ','); $tm1+=$rowmes->kgs;$m1=$rowmes->kgs;break;
						case $datosmes [$cuenta+1][2].$datosmes [$cuenta+1][3]: $row->m2=number_format(($rowmes->kgs), 2, '.', ','); $tm2+=$rowmes->kgs;$m2=$rowmes->kgs;break;
						case $datosmes [$cuenta+2][2].$datosmes [$cuenta+2][3]: $row->m3=number_format(($rowmes->kgs), 2, '.', ','); $tm3+=$rowmes->kgs;$m3=$rowmes->kgs;break;
						case $datosmes [$cuenta+3][2].$datosmes [$cuenta+3][3]: $row->m4=number_format(($rowmes->kgs), 2, '.', ','); $tm4+=$rowmes->kgs;$m4=$rowmes->kgs;break;
						case $datosmes [$cuenta+4][2].$datosmes [$cuenta+4][3]: $row->m5=number_format(($rowmes->kgs), 2, '.', ','); $tm5+=$rowmes->kgs;$m5=$rowmes->kgs;break;
						case $datosmes [$cuenta+5][2].$datosmes [$cuenta+5][3]: $row->m6=number_format(($rowmes->kgs), 2, '.', ','); $tm6+=$rowmes->kgs;$m6=$rowmes->kgs;break;
						case $datosmes [$cuenta+6][2].$datosmes [$cuenta+6][3]: $row->m7=number_format(($rowmes->kgs), 2, '.', ','); $tm7+=$rowmes->kgs;$m7=$rowmes->kgs;break;
						case $datosmes [$cuenta+7][2].$datosmes [$cuenta+7][3]: $row->m8=number_format(($rowmes->kgs), 2, '.', ','); $tm8+=$rowmes->kgs;$m8=$rowmes->kgs;break;
						case $datosmes [$cuenta+8][2].$datosmes [$cuenta+8][3]: $row->m9=number_format(($rowmes->kgs), 2, '.', ','); $tm9+=$rowmes->kgs;$m9=$rowmes->kgs;break;
						case $datosmes [$cuenta+9][2].$datosmes [$cuenta+9][3]: $row->m10=number_format(($rowmes->kgs), 2, '.', ','); $tm10+=$rowmes->kgs;$m10=$rowmes->kgs;break;
						case $datosmes [$cuenta+10][2].$datosmes [$cuenta+10][3]: $row->m11=number_format(($rowmes->kgs), 2, '.', ','); $tm11+=$rowmes->kgs;$m11=$rowmes->kgs;break;
						case $datosmes [$cuenta+11][2].$datosmes [$cuenta+11][3]: $row->m12=number_format(($rowmes->kgs), 2, '.', ','); $tm12+=$rowmes->kgs;$m12=$rowmes->kgs;break;
						case $datosmes [$cuenta+12][2].$datosmes [$cuenta+12][3]: $row->m13=number_format(($rowmes->kgs), 2, '.', ','); $tm13+=$rowmes->kgs;$m13=$rowmes->kgs;break;
						case $datosmes [$cuenta+13][2].$datosmes [$cuenta+13][3]: $row->m14=number_format(($rowmes->kgs), 2, '.', ','); $tm14+=$rowmes->kgs;$m14=$rowmes->kgs;break;
						case $datosmes [$cuenta+14][2].$datosmes [$cuenta+14][3]: $row->m15=number_format(($rowmes->kgs), 2, '.', ','); $tm15+=$rowmes->kgs;$m15=$rowmes->kgs;break;
					}
				endforeach;
				$datos[$gra] =array($gra,$rowmes->mes,(float)$kilos,$row->por,$rowmes->ano);
				$datostm[$gra] =array($gra,$row->grupo,$m1,$m2,$m3,$m4,$m5,$m6,$m7,$m8,$m9,$m10,$m11,$m12,$m13,$m14,$m15);$gra+=1;
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->mes='';$rowZ->por='';$rowZ->kmn='';$rowZ->imn='';$rowZ->kus='';$rowZ->ius='';$rowZ->kgs='';
				$rowZ->m1 ='';$rowZ->m2 ='';$rowZ->m3 ='';$rowZ->m4 ='';$rowZ->m5 ='';$rowZ->m6 ='';
				$rowZ->m7 ='';$rowZ->m8 ='';$rowZ->m9 ='';$rowZ->m10 ='';$rowZ->m11 ='';$rowZ->m12 ='';$rowZ->m13 ='';$rowZ->m14 ='';$rowZ->m15 ='';
				$rowZ->mm1 ='';$rowZ->mm2 ='';$rowZ->mm3 ='';$rowZ->mm4 ='';$rowZ->mm5 ='';$rowZ->mm6 ='';
				$rowZ->mm7 ='';$rowZ->mm8 ='';$rowZ->mm9 ='';$rowZ->mm10 ='';$rowZ->mm11 ='';$rowZ->mm12 ='';$rowZ->mm13='';$rowZ->mm14='';$rowZ->mm15='';
				$rowZ->grupo='Total';
				if($totkgs>0) $rowZ->kgs=number_format(($totkgs), 2, '.', ',');
				if($tm1>0) $rowZ->m1=number_format(($tm1), 2, '.', ',');
				if($tm2>0) $rowZ->m2=number_format(($tm2), 2, '.', ',');
				if($tm3>0) $rowZ->m3=number_format(($tm3), 2, '.', ',');
				if($tm4>0) $rowZ->m4=number_format(($tm4), 2, '.', ',');
				if($tm5>0) $rowZ->m5=number_format(($tm5), 2, '.', ',');
				if($tm6>0) $rowZ->m6=number_format(($tm6), 2, '.', ',');
				if($tm7>0) $rowZ->m7=number_format(($tm7), 2, '.', ',');
				if($tm8>0) $rowZ->m8=number_format(($tm8), 2, '.', ',');
				if($tm9>0) $rowZ->m9=number_format(($tm9), 2, '.', ',');
				if($tm10>0) $rowZ->m10=number_format(($tm10), 2, '.', ',');
				if($tm11>0) $rowZ->m11=number_format(($tm11), 2, '.', ',');
				if($tm12>0) $rowZ->m12=number_format(($tm12), 2, '.', ',');
				if($tm13>0) $rowZ->m13=number_format(($tm13), 2, '.', ',');
				if($tm14>0) $rowZ->m14=number_format(($tm14), 2, '.', ',');
				if($tm15>0) $rowZ->m15=number_format(($tm15), 2, '.', ',');
				
				$ini=1;
				while($ini<=$gra-1){
				 switch($datos [$ini][0]){
					case '1': $rowZ->mm1 =$datosmes [$ini][5];$rowZ->v1 =$datos [$ini][2];$rowZ->p1 =$datos [$ini][3];$rowZ->a1 =$datosmes [$ini][4];
					 		  $rowZ->t1 =$datostm [$ini][1]; $rowZ->t1m1 =$datostm [$ini][2];$rowZ->t1m2 =$datostm [$ini][3];$rowZ->t1m3 =$datostm [$ini][4];	
							  $rowZ->t1m4 =$datostm [$ini][5];$rowZ->t1m5 =$datostm [$ini][6];$rowZ->t1m6 =$datostm [$ini][7];$rowZ->t1m7 =$datostm [$ini][8];
							  $rowZ->t1m8 =$datostm [$ini][9];$rowZ->t1m9 =$datostm [$ini][10];$rowZ->t1m10 =$datostm [$ini][11];$rowZ->t1m11 =$datostm [$ini][12];
							  $rowZ->t1m12 =$datostm [$ini][13];$rowZ->t1m13 =$datostm [$ini][14];$rowZ->t1m14 =$datostm [$ini][15];
							 break;
					case '2': $rowZ->mm2 =$datosmes [$ini][5];$rowZ->v2 =$datos [$ini][2];$rowZ->p2 =$datos [$ini][3];$rowZ->a2 =$datosmes [$ini][4];
							  $rowZ->t2 =$datostm [$ini][1]; $rowZ->t2m1 =$datostm [$ini][2];$rowZ->t2m2 =$datostm [$ini][3];$rowZ->t2m3 =$datostm [$ini][4];	
							  $rowZ->t2m4 =$datostm [$ini][5];$rowZ->t2m5 =$datostm [$ini][6];$rowZ->t2m6 =$datostm [$ini][7];$rowZ->t2m7 =$datostm [$ini][8];
							  $rowZ->t2m8 =$datostm [$ini][9];$rowZ->t2m9 =$datostm [$ini][10];$rowZ->t2m10 =$datostm [$ini][11];$rowZ->t2m11 =$datostm [$ini][12];
							  $rowZ->t2m12 =$datostm [$ini][13];$rowZ->t2m13 =$datostm [$ini][14];$rowZ->t2m14 =$datostm [$ini][15];
							 break;
					case '3': $rowZ->mm3 =$datosmes [$ini][5];$rowZ->v3 =$datos [$ini][2];$rowZ->p3 =$datos [$ini][3];$rowZ->a3 =$datosmes [$ini][4];
							  $rowZ->t3 =$datostm [$ini][1]; $rowZ->t3m1 =$datostm [$ini][2];$rowZ->t3m2 =$datostm [$ini][3];$rowZ->t3m3 =$datostm [$ini][4];	
							  $rowZ->t3m4 =$datostm [$ini][5];$rowZ->t3m5 =$datostm [$ini][6];$rowZ->t3m6 =$datostm [$ini][7];$rowZ->t3m7 =$datostm [$ini][8];
							  $rowZ->t3m8 =$datostm [$ini][9];$rowZ->t3m9 =$datostm [$ini][10];$rowZ->t3m10 =$datostm [$ini][11];$rowZ->t3m11 =$datostm [$ini][12];
							  $rowZ->t3m12 =$datostm [$ini][13];$rowZ->t3m13 =$datostm [$ini][14];$rowZ->t3m14 =$datostm [$ini][15];
							break;
					case '4': $rowZ->mm4 =$datosmes [$ini][5];$rowZ->v4 =$datos [$ini][2];$rowZ->p4 =$datos [$ini][3];$rowZ->a4 =$datosmes [$ini][4];
							  $rowZ->t4 =$datostm [$ini][1]; $rowZ->t4m1 =$datostm [$ini][2];$rowZ->t4m2 =$datostm [$ini][3];$rowZ->t4m3 =$datostm [$ini][4];	
							  $rowZ->t4m4 =$datostm [$ini][5];$rowZ->t4m5 =$datostm [$ini][6];$rowZ->t4m6 =$datostm [$ini][7];$rowZ->t4m7 =$datostm [$ini][8];
							  $rowZ->t4m8 =$datostm [$ini][9];$rowZ->t4m9 =$datostm [$ini][10];$rowZ->t4m10 =$datostm [$ini][11];$rowZ->t4m11 =$datostm [$ini][12];
							  $rowZ->t4m12 =$datostm [$ini][13];$rowZ->t4m13 =$datostm [$ini][14];$rowZ->t4m14 =$datostm [$ini][15];
							break;
					case '5': $rowZ->mm5 =$datosmes [$ini][5];$rowZ->v5 =$datos [$ini][2];$rowZ->p5 =$datos [$ini][3];$rowZ->a5 =$datosmes [$ini][4];
							  $rowZ->t5 =$datostm [$ini][1]; $rowZ->t5m1 =$datostm [$ini][2];$rowZ->t5m2 =$datostm [$ini][3];$rowZ->t5m3 =$datostm [$ini][4];	
							  $rowZ->t5m4 =$datostm [$ini][5];$rowZ->t5m5 =$datostm [$ini][6];$rowZ->t5m6 =$datostm [$ini][7];$rowZ->t5m7 =$datostm [$ini][8];
							  $rowZ->t5m8 =$datostm [$ini][9];$rowZ->t5m9 =$datostm [$ini][10];$rowZ->t5m10 =$datostm [$ini][11];$rowZ->t5m11 =$datostm [$ini][12];
							  $rowZ->t5m12 =$datostm [$ini][13];$rowZ->t5m13 =$datostm [$ini][14];$rowZ->t5m14 =$datostm [$ini][15];
							break;
					case '6': $rowZ->mm6 =$datosmes [$ini][5];$rowZ->v6 =$datos [$ini][2];$rowZ->p6 =$datos [$ini][3];$rowZ->a6 =$datosmes [$ini][4];
							  $rowZ->t6 =$datostm [$ini][1]; $rowZ->t6m1 =$datostm [$ini][2];$rowZ->t6m2 =$datostm [$ini][3];$rowZ->t6m3 =$datostm [$ini][4];	
							  $rowZ->t6m4 =$datostm [$ini][5];$rowZ->t6m5 =$datostm [$ini][6];$rowZ->t6m6 =$datostm [$ini][7];$rowZ->t6m7 =$datostm [$ini][8];
							  $rowZ->t6m8 =$datostm [$ini][9];$rowZ->t6m9 =$datostm [$ini][10];$rowZ->t6m10 =$datostm [$ini][11];$rowZ->t6m11 =$datostm [$ini][12];
							  $rowZ->t6m12 =$datostm [$ini][13];$rowZ->t6m13 =$datostm [$ini][14];$rowZ->t6m14 =$datostm [$ini][15];	
							break;
					case '7': $rowZ->mm7 =$datosmes [$ini][5];$rowZ->v7 =$datos [$ini][2];$rowZ->p7 =$datos [$ini][3];$rowZ->a7 =$datosmes [$ini][4];
							  $rowZ->t7 =$datostm [$ini][1]; $rowZ->t7m1 =$datostm [$ini][2];$rowZ->t7m2 =$datostm [$ini][3];$rowZ->t7m3 =$datostm [$ini][4];	
							  $rowZ->t7m4 =$datostm [$ini][5];$rowZ->t7m5 =$datostm [$ini][6];$rowZ->t7m6 =$datostm [$ini][7];$rowZ->t7m7 =$datostm [$ini][8];
							  $rowZ->t7m8 =$datostm [$ini][9];$rowZ->t7m9 =$datostm [$ini][10];$rowZ->t7m10 =$datostm [$ini][11];$rowZ->t7m11 =$datostm [$ini][12];
							  $rowZ->t7m12 =$datostm [$ini][13];$rowZ->t7m13 =$datostm [$ini][14];$rowZ->t7m14 =$datostm [$ini][15];
							break;
					case '8': $rowZ->mm8 =$datosmes [$ini][5];$rowZ->v8 =$datos [$ini][2];$rowZ->p8 =$datos [$ini][3];$rowZ->a8 =$datosmes [$ini][4];
							  $rowZ->t8 =$datostm [$ini][1]; $rowZ->t8m1 =$datostm [$ini][2];$rowZ->t8m2 =$datostm [$ini][3];$rowZ->t8m3 =$datostm [$ini][4];	
							  $rowZ->t8m4 =$datostm [$ini][5];$rowZ->t8m5 =$datostm [$ini][6];$rowZ->t8m6 =$datostm [$ini][7];$rowZ->t8m7 =$datostm [$ini][8];
							  $rowZ->t8m8 =$datostm [$ini][9];$rowZ->t8m9 =$datostm [$ini][10];$rowZ->t8m10 =$datostm [$ini][11];$rowZ->t8m11 =$datostm [$ini][12];
							  $rowZ->t8m12 =$datostm [$ini][13];$rowZ->t8m13 =$datostm [$ini][14];$rowZ->t8m14 =$datostm [$ini][15];
							break;
					case '9': $rowZ->mm9 =$datosmes [$ini][5];$rowZ->v9 =$datos [$ini][2];$rowZ->p9 =$datos [$ini][3];$rowZ->a9 =$datosmes [$ini][4];
							  $rowZ->t9 =$datostm [$ini][1]; $rowZ->t9m1 =$datostm [$ini][2];$rowZ->t9m2 =$datostm [$ini][3];$rowZ->t9m3 =$datostm [$ini][4];	
							  $rowZ->t9m4 =$datostm [$ini][5];$rowZ->t9m5 =$datostm [$ini][6];$rowZ->t9m6 =$datostm [$ini][7];$rowZ->t9m7 =$datostm [$ini][8];
							  $rowZ->t9m8 =$datostm [$ini][9];$rowZ->t9m9 =$datostm [$ini][10];$rowZ->t9m10 =$datostm [$ini][11];$rowZ->t9m11 =$datostm [$ini][12];
							  $rowZ->t9m12 =$datostm [$ini][13];$rowZ->t9m13 =$datostm [$ini][14];$rowZ->t9m14 =$datostm [$ini][15];	
							break;
					case '10': $rowZ->mm10 =$datosmes [$ini][5];$rowZ->v10 =$datos [$ini][2];$rowZ->p10 =$datos [$ini][3];$rowZ->a10 =$datosmes [$ini][4];
							  $rowZ->t10 =$datostm [$ini][1]; $rowZ->t10m1 =$datostm [$ini][2];$rowZ->t10m2 =$datostm [$ini][3];$rowZ->t10m3 =$datostm [$ini][4];	
							  $rowZ->t10m4 =$datostm [$ini][5];$rowZ->t10m5 =$datostm [$ini][6];$rowZ->t10m6 =$datostm [$ini][7];$rowZ->t10m7 =$datostm [$ini][8];
							  $rowZ->t10m8 =$datostm [$ini][9];$rowZ->t10m9 =$datostm [$ini][10];$rowZ->t10m10 =$datostm [$ini][11];$rowZ->t10m11 =$datostm [$ini][12];
							  $rowZ->t10m12 =$datostm [$ini][13];$rowZ->t10m13 =$datostm [$ini][14];$rowZ->t10m14 =$datostm [$ini][15];	
							break;
					
				 }
				 $ini+=1;
			    }
				$data[] = $rowZ;	
			endforeach;	
			
			//}
			return $data;
		}
		
		function getvtaT($filter,$tal){
			//select grupo,talsal,nomt,sum(kgssal) as kgs from tallas inner join salidas on talsal=idt where estsal=1 group by nomt order by sum(kgssal) Desc
			//select grupo,talsal,nomt,sum(kgssal) as kgs from tallas inner join salidas on talsal=idt where estsal=1 group by grupo order by sum(kgssal) Desc
			$this->db->select('grupo,talsal,nomt,sum(kgssal) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');	
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$this->db->group_by('grupo'); //$this->db->group_by('talsal');$this->db->group_by('nomt');
			$this->db->order_by('sum(kgssal)','DESC');
			$query = $this->db->get('salidas');
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
			//Se obtiene el total de lo vendido
			//SELECT FORMAT(sum(kgssal),2) as kgs from salidas where estsal=1
			$this->db->select('sum(kgssal) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$querym = $this->db->get('salidas');	
			foreach($querym->result() as $rowm):
				$totkgs=$rowm->kgs;
			endforeach;
				$contx=1;$kilos=0;
				while ($contx <= 10) {  $datos[$contx] =array('','','','','','','','',''); $contx+=1;}
				$tkmn=0;$timn=0;$tkus=0;$tius=0;$gra=1;$ano='';$mn=0;$usd=0;
			foreach($query->result() as $row):
				$row->por='';$row->kmn='';$row->imn='';$row->kus='';$row->ius=''; $row->pvmn=''; $row->pvus='';$pvmn=0;$pvus=0;
				$kilos=number_format(($row->kgs), 2,'.','');
				
				if($totkgs>0)$row->por=number_format(($row->kgs/$totkgs)*100, 2, '.', ',');
				$row->kgs=number_format(($row->kgs), 2, '.', ',');
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkmn+=$rowm->kgs;$timn+=$rowm->imp;$mn=number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0)$row->kmn=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->imn='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvmn='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvmn=$rowm->imp/$rowm->kgs;}
				endforeach;
				//usd
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');	
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->where('grupo',$row->grupo);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus+=$rowm->kgs;$tius+=$rowm->imp;$usd=number_format(($rowm->imp), 2, '.', '');
					if($rowm->kgs>0)$row->kus=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus=$rowm->imp/$rowm->kgs;}
				endforeach;
				$datos[$gra] =array($gra,$row->grupo,(float)$kilos,$row->por,$row->imn,$row->ius,$pvmn,$pvus);$gra+=1;
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->mes='';$rowZ->por='';$rowZ->kmn='';$rowZ->imn='';$rowZ->kus='';$rowZ->ius='';$rowZ->kgs='';$rowZ->pvmn='';$rowZ->pvus='';
				$rowZ->a1='';$rowZ->a2='';$rowZ->a3='';$rowZ->a4='';$rowZ->a5='';$rowZ->a6='';$rowZ->a7='';$rowZ->a8='';$rowZ->a9='';$rowZ->a10=''; 
				$rowZ->grupo='Total';
				if($totkgs>0) $rowZ->kgs=number_format(($totkgs), 2, '.', ',');
				if($tkmn>0) $rowZ->kmn=number_format(($tkmn), 2, '.', ',');
				if($timn>0) $rowZ->imn='$'.number_format(($timn), 2, '.', ',');
				if($tkmn>0 && $timn>0) $rowZ->pvmn='$'.number_format(($timn/$tkmn), 2, '.', ',');
				if($tkus>0) $rowZ->kus=number_format(($tkus), 2, '.', ',');
				if($tius>0) $rowZ->ius='$'.number_format(($tius), 2, '.', ',');
				if($tkus>0 && $tius>0) $rowZ->pvus='$'.number_format(($tius/$tkus), 2, '.', ',');
				$ini=1;
				while($ini<=$gra-1){
				 switch($datos [$ini][0]){
					case '1': $rowZ->a1 =$datos [$ini][1];$rowZ->v1 =$datos [$ini][2];$rowZ->p1 =$datos [$ini][3];$rowZ->mn1 =$datos [$ini][4];$rowZ->us1 =$datos [$ini][5];$rowZ->pmn1 =$datos [$ini][6];$rowZ->pus1 =$datos [$ini][7];break;
					case '2': $rowZ->a2 =$datos [$ini][1];$rowZ->v2 =$datos [$ini][2];$rowZ->p2 =$datos [$ini][3];$rowZ->mn2 =$datos [$ini][4];$rowZ->us2 =$datos [$ini][5];$rowZ->pmn2 =$datos [$ini][6];$rowZ->pus2 =$datos [$ini][7];break;
					case '3': $rowZ->a3 =$datos [$ini][1];$rowZ->v3 =$datos [$ini][2];$rowZ->p3 =$datos [$ini][3];$rowZ->mn3 =$datos [$ini][4];$rowZ->us3 =$datos [$ini][5];$rowZ->pmn3 =$datos [$ini][6];$rowZ->pus3 =$datos [$ini][7];break;
					case '4': $rowZ->a4 =$datos [$ini][1];$rowZ->v4 =$datos [$ini][2];$rowZ->p4 =$datos [$ini][3];$rowZ->mn4 =$datos [$ini][4];$rowZ->us4 =$datos [$ini][5];$rowZ->pmn4 =$datos [$ini][6];$rowZ->pus4 =$datos [$ini][7];break;
					case '5': $rowZ->a5 =$datos [$ini][1];$rowZ->v5 =$datos [$ini][2];$rowZ->p5 =$datos [$ini][3];$rowZ->mn5 =$datos [$ini][4];$rowZ->us5 =$datos [$ini][5];$rowZ->pmn5 =$datos [$ini][6];$rowZ->pus5 =$datos [$ini][7];break;
					case '6': $rowZ->a6 =$datos [$ini][1];$rowZ->v6 =$datos [$ini][2];$rowZ->p6 =$datos [$ini][3];$rowZ->mn6 =$datos [$ini][4];$rowZ->us6 =$datos [$ini][5];$rowZ->pmn6 =$datos [$ini][6];$rowZ->pus6 =$datos [$ini][7];break;
					case '7': $rowZ->a7 =$datos [$ini][1];$rowZ->v7 =$datos [$ini][2];$rowZ->p7 =$datos [$ini][3];$rowZ->mn7 =$datos [$ini][4];$rowZ->us7 =$datos [$ini][5];$rowZ->pmn7 =$datos [$ini][6];$rowZ->pus7 =$datos [$ini][7];break;
					case '8': $rowZ->a8 =$datos [$ini][1];$rowZ->v8 =$datos [$ini][2];$rowZ->p8 =$datos [$ini][3];$rowZ->mn8 =$datos [$ini][4];$rowZ->us8 =$datos [$ini][5];$rowZ->pmn8 =$datos [$ini][6];$rowZ->pus8 =$datos [$ini][7];break;
					case '9': $rowZ->a9 =$datos [$ini][1];$rowZ->v9 =$datos [$ini][2];$rowZ->p9 =$datos [$ini][3];$rowZ->mn9 =$datos [$ini][4];$rowZ->us9 =$datos [$ini][5];$rowZ->pmn9 =$datos [$ini][6];$rowZ->pus9 =$datos [$ini][7];break;
					case '10': $rowZ->a10 =$datos [$ini][1];$rowZ->v10 =$datos [$ini][2];$rowZ->p10 =$datos [$ini][3];$rowZ->mn10 =$datos [$ini][4];$rowZ->us10 =$datos [$ini][5];$rowZ->pmn10 =$datos [$ini][6];$rowZ->pus10 =$datos [$ini][7];break;
				 }
				 $ini+=1;
			    }
				$data[] = $rowZ;	
			endforeach;	
			
			}
			return $data;
		}
		
		function getvtaM($filter,$tal){
			//SELECT year(fecsal) as ao,month(fecsal) as mes, sum(kgssal) as kgs from salidas group by month(fecsal) order by year(fecsal),month(fecsal)
			$this->db->select('year(fecsal) as ano,month(fecsal) as mes, sum(kgssal) as kgs');	
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			//$this->db->group_by('grupo'); 
			$this->db->group_by('year(fecsal)');
			$this->db->group_by('month(fecsal)');
			$query = $this->db->get('salidas');
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
			//Se obtiene el total de lo vendido
			//SELECT FORMAT(sum(kgssal),2) as kgs from salidas where estsal=1
			$this->db->select('sum(kgssal) as kgs');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$querym = $this->db->get('salidas');	
			foreach($querym->result() as $rowm):
				$totkgs=$rowm->kgs;
			endforeach;
				$contx=1;$kilos=0;
				while ($contx <= 15) {  $datos[$contx] =array('','','','','','','','',''); $contx+=1;}
				$tkmn=0;$timn=0;$tkus=0;$tius=0;$gra=1;$ano='';$mn=0;$usd=0;
			foreach($query->result() as $row):
				$row->por='';$row->kmn='';$row->imn='';$row->kus='';$row->ius='';$row->pvmn=''; $row->pvus='';$pvmn=0;$pvus=0;
				$kilos=number_format(($row->kgs), 2,'.','');$mes=$row->mes;
				if($ano!=$row->ano){
					$ano=$row->ano;	
				}else{ $row->ano="";}
				switch ($row->mes) {
					case '1':$row->mes='Ene';break;	case '2':$row->mes='Feb';break;	case '3':$row->mes='Mar';break;
					case '4':$row->mes='Abr';break;	case '5':$row->mes='May';break;	case '6':$row->mes='Jun';break;
					case '7':$row->mes='Jul';break;	case '8':$row->mes='Ago';break;	case '9':$row->mes='Sep';break;
					case '10':$row->mes='Oct';break; case '11':$row->mes='Nov';break; case '12':$row->mes='Dic';break;
				}
				if($totkgs>0)$row->por=number_format(($row->kgs/$totkgs)*100, 2, '.', ',');
				$row->kgs=number_format(($row->kgs), 2, '.', ',');
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('month(fecsal)',$mes);
				$this->db->where('year(fecsal)',$ano);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkmn+=$rowm->kgs;$timn+=$rowm->imp;$mn=number_format(($rowm->imp), 2, '.', ',');
					$row->kmn=number_format(($rowm->kgs), 2, '.', ',');
					$row->imn='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvmn='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvmn=$rowm->imp/$rowm->kgs;}
				endforeach;
				//usd
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				//$this->db->group_by('grupo');
				$this->db->where('month(fecsal)',$mes);
				$this->db->where('year(fecsal)',$ano);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus+=$rowm->kgs;$tius+=$rowm->imp;$usd=number_format(($rowm->imp), 2, '.', '');
					if($rowm->kgs>0)$row->kus=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus=$rowm->imp/$rowm->kgs;}
				endforeach;
				$datos[$gra] =array($gra,$row->mes,(float)$kilos,$row->por,$row->ano,$row->imn,$row->ius,$pvmn,$pvus);$gra+=1;
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->mes='';$rowZ->por='';$rowZ->kmn='';$rowZ->imn='';$rowZ->kus='';$rowZ->ius='';$rowZ->kgs='';$rowZ->pvmn='';$rowZ->pvus='';
				$rowZ->ano='Total';
				if($totkgs>0) $rowZ->kgs=number_format(($totkgs), 2, '.', ',');
				if($tkmn>0) $rowZ->kmn=number_format(($tkmn), 2, '.', ',');
				if($timn>0) $rowZ->imn='$'.number_format(($timn), 2, '.', ',');
				if($tkmn>0 && $timn>0) $rowZ->pvmn='$'.number_format(($timn/$tkmn), 2, '.', ',');
				if($tkus>0) $rowZ->kus=number_format(($tkus), 2, '.', ',');
				if($tius>0) $rowZ->ius='$'.number_format(($tius), 2, '.', ',');
				if($tkus>0 && $tius>0) $rowZ->pvus='$'.number_format(($tius/$tkus), 2, '.', ',');
				$rowZ->m1 ='';$rowZ->m2 ='';$rowZ->m3 ='';$rowZ->m4 ='';$rowZ->m5 ='';$rowZ->m6 ='';
				$rowZ->m7 ='';$rowZ->m8 ='';$rowZ->m9 ='';$rowZ->m10 ='';$rowZ->m11 ='';$rowZ->m12 ='';
				$ini=1;
				while($ini<=$gra-1){
				 switch($datos [$ini][0]){
					case '1': $rowZ->m1 =$datos [$ini][1];$rowZ->v1 =$datos [$ini][2];$rowZ->p1 =$datos [$ini][3];$rowZ->a1 =$datos [$ini][4];$rowZ->mn1 =$datos [$ini][5];$rowZ->us1 =$datos [$ini][6];$rowZ->pmn1 =$datos [$ini][7];$rowZ->pus1 =$datos [$ini][8];break;
					case '2': $rowZ->m2 =$datos [$ini][1];$rowZ->v2 =$datos [$ini][2];$rowZ->p2 =$datos [$ini][3];$rowZ->a2 =$datos [$ini][4];$rowZ->mn2 =$datos [$ini][5];$rowZ->us2 =$datos [$ini][6];$rowZ->pmn2 =$datos [$ini][7];$rowZ->pus2 =$datos [$ini][8];break;
					case '3': $rowZ->m3 =$datos [$ini][1];$rowZ->v3 =$datos [$ini][2];$rowZ->p3 =$datos [$ini][3];$rowZ->a3 =$datos [$ini][4];$rowZ->mn3 =$datos [$ini][5];$rowZ->us3 =$datos [$ini][6];$rowZ->pmn3 =$datos [$ini][7];$rowZ->pus3 =$datos [$ini][8];break;
					case '4': $rowZ->m4 =$datos [$ini][1];$rowZ->v4 =$datos [$ini][2];$rowZ->p4 =$datos [$ini][3];$rowZ->a4 =$datos [$ini][4];$rowZ->mn4 =$datos [$ini][5];$rowZ->us4 =$datos [$ini][6];$rowZ->pmn4 =$datos [$ini][7];$rowZ->pus4 =$datos [$ini][8];break;
					case '5': $rowZ->m5 =$datos [$ini][1];$rowZ->v5 =$datos [$ini][2];$rowZ->p5 =$datos [$ini][3];$rowZ->a5 =$datos [$ini][4];$rowZ->mn5 =$datos [$ini][5];$rowZ->us5 =$datos [$ini][6];$rowZ->pmn5 =$datos [$ini][7];$rowZ->pus5 =$datos [$ini][8];break;
					case '6': $rowZ->m6 =$datos [$ini][1];$rowZ->v6 =$datos [$ini][2];$rowZ->p6 =$datos [$ini][3];$rowZ->a6 =$datos [$ini][4];$rowZ->mn6 =$datos [$ini][5];$rowZ->us6 =$datos [$ini][6];$rowZ->pmn6 =$datos [$ini][7];$rowZ->pus6 =$datos [$ini][8];break;
					case '7': $rowZ->m7 =$datos [$ini][1];$rowZ->v7 =$datos [$ini][2];$rowZ->p7 =$datos [$ini][3];$rowZ->a7 =$datos [$ini][4];$rowZ->mn7 =$datos [$ini][5];$rowZ->us7 =$datos [$ini][6];$rowZ->pmn7 =$datos [$ini][7];$rowZ->pus7 =$datos [$ini][8];break;
					case '8': $rowZ->m8 =$datos [$ini][1];$rowZ->v8 =$datos [$ini][2];$rowZ->p8 =$datos [$ini][3];$rowZ->a8 =$datos [$ini][4];$rowZ->mn8 =$datos [$ini][5];$rowZ->us8 =$datos [$ini][6];$rowZ->pmn8 =$datos [$ini][7];$rowZ->pus8 =$datos [$ini][8];break;
					case '9': $rowZ->m9 =$datos [$ini][1];$rowZ->v9 =$datos [$ini][2];$rowZ->p9 =$datos [$ini][3];$rowZ->a9 =$datos [$ini][4];$rowZ->mn9 =$datos [$ini][5];$rowZ->us9 =$datos [$ini][6];$rowZ->pmn9 =$datos [$ini][7];$rowZ->pus9 =$datos [$ini][8];break;
					case '10': $rowZ->m10 =$datos [$ini][1];$rowZ->v10 =$datos [$ini][2];$rowZ->p10 =$datos [$ini][3];$rowZ->a10 =$datos [$ini][4];$rowZ->mn10 =$datos [$ini][5];$rowZ->us10 =$datos [$ini][6];$rowZ->pmn10 =$datos [$ini][7];$rowZ->pus10 =$datos [$ini][8];break;
					case '11': $rowZ->m11 =$datos [$ini][1];$rowZ->v11 =$datos [$ini][2];$rowZ->p11 =$datos [$ini][3];$rowZ->a11 =$datos [$ini][4];$rowZ->mn11 =$datos [$ini][5];$rowZ->us11 =$datos [$ini][6];$rowZ->pmn11 =$datos [$ini][7];$rowZ->pus11 =$datos [$ini][8];break;
					case '12': $rowZ->m12 =$datos [$ini][1];$rowZ->v12 =$datos [$ini][2];$rowZ->p12 =$datos [$ini][3];$rowZ->a12 =$datos [$ini][4];$rowZ->mn12 =$datos [$ini][5];$rowZ->us12 =$datos [$ini][6];$rowZ->pmn12 =$datos [$ini][7];$rowZ->pus12 =$datos [$ini][8];break;
					case '13': $rowZ->m13 =$datos [$ini][1];$rowZ->v13 =$datos [$ini][2];$rowZ->p13 =$datos [$ini][3];$rowZ->a13 =$datos [$ini][4];$rowZ->mn13 =$datos [$ini][5];$rowZ->us13 =$datos [$ini][6];$rowZ->pmn13 =$datos [$ini][7];$rowZ->pus13 =$datos [$ini][8];break;
					case '14': $rowZ->m14 =$datos [$ini][1];$rowZ->v14 =$datos [$ini][2];$rowZ->p14 =$datos [$ini][3];$rowZ->a14 =$datos [$ini][4];$rowZ->mn14 =$datos [$ini][5];$rowZ->us14 =$datos [$ini][6];$rowZ->pmn14 =$datos [$ini][7];$rowZ->pus14 =$datos [$ini][8];break;
					case '15': $rowZ->m15 =$datos [$ini][1];$rowZ->v15 =$datos [$ini][2];$rowZ->p15 =$datos [$ini][3];$rowZ->a15 =$datos [$ini][4];$rowZ->mn15 =$datos [$ini][5];$rowZ->us15 =$datos [$ini][6];$rowZ->pmn15 =$datos [$ini][7];$rowZ->pus15 =$datos [$ini][8];break;
				 }
				 $ini+=1;
			    }
				$data[] = $rowZ;	
			endforeach;	
			
			}
			return $data;
		}
		
		function getvtaG($filter,$tal){
			//select almsal,noma,FORMAT(sum(kgssal),2) as kgs from almacenes inner join salidas on almsal=ida where estsal=1 group by almsal order by sum(kgssal) Desc
			$this->db->select('almsal,noma,sum(kgssal) as kgs,siga');	
			$this->db->join('salidas', 'almsal=ida', 'inner');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$this->db->group_by('almsal');
			$this->db->order_by('sum(kgssal)','DESC');
			$query = $this->db->get('almacenes');
			//$totuni=$query->num_rows();
			$data = array();
			if($query->num_rows()>0){
			//Se obtiene el total de lo vendido
			//SELECT FORMAT(sum(kgssal),2) as kgs from salidas where estsal=1
			$this->db->select('sum(kgssal) as kgs');
			//$this->db->join('salidas', 'almsal=ida', 'inner');
			$this->db->join('tallas', 'idt=talsal', 'inner');
			if($filter['where']!=''){$this->db->where($filter['where']); }
			if($tal>0) $this->db->where('gpoid',$tal);
			$this->db->where('estsal',1);
			$querym = $this->db->get('salidas');	
			foreach($querym->result() as $rowm):
				$totkgs=$rowm->kgs;
			endforeach;
				$contx=1;$kilos=0;
				while ($contx <= 6) {  $datos[$contx] =array('','','','','','','',''); $contx+=1;}
				$tkmn=0;$timn=0;$tkus=0;$tius=0;$gra=1;
			foreach($query->result() as $row):
				$row->por='';$row->kmn='';$row->imn='';$row->pvmn='';$row->kus='';$row->ius='';$row->pvus='';$kilos=$row->kgs;$pvmn=0;$pvus=0;
				if($totkgs>0)$row->por=number_format(($row->kgs/$totkgs)*100, 2, '.', ',');
				$row->kgs=number_format(($row->kgs), 2, '.', ',');
				//mn
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*prekgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('estsal',1);$this->db->where('usdkgs',0);
				$this->db->where('almsal',$row->almsal);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkmn+=$rowm->kgs;$timn+=$rowm->imp;
					if($rowm->kgs>0)$row->kmn=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->imn='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvmn='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvmn=$rowm->imp/$rowm->kgs;}
				endforeach;
				//usd
				$this->db->select('sum(kgssal) as kgs,sum(kgssal*usdkgs)as imp');
				$this->db->join('tallas', 'idt=talsal', 'inner');
				if($filter['where']!=''){$this->db->where($filter['where']); }
				if($tal>0) $this->db->where('gpoid',$tal);
				$this->db->where('estsal',1);$this->db->where('usdkgs >',0);
				$this->db->where('almsal',$row->almsal);
				$querym = $this->db->get('salidas');	
				foreach($querym->result() as $rowm):
					$tkus+=$rowm->kgs;$tius+=$rowm->imp;
					if($rowm->kgs>0)$row->kus=number_format(($rowm->kgs), 2, '.', ',');
					if($rowm->imp>0)$row->ius='$'.number_format(($rowm->imp), 2, '.', ',');
					if($rowm->kgs>0){ $row->pvus='$'.number_format(($rowm->imp/$rowm->kgs), 2, '.', ',');$pvus=$rowm->imp/$rowm->kgs;}
				endforeach;
				$datos[$gra] =array($gra,$row->siga,(float)$kilos,$row->por,$row->imn,$row->ius,$pvmn,$pvus);$gra+=1;
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->por='';$rowZ->kmn='';$rowZ->imn='';$rowZ->kus='';$rowZ->ius='';$rowZ->kgs='';$rowZ->pvmn='';$rowZ->pvus='';
				$rowZ->a1='';$rowZ->a2='';$rowZ->a3='';$rowZ->a4='';$rowZ->a5='';$rowZ->a6='';
				$rowZ->v1='';$rowZ->v2='';$rowZ->v3='';$rowZ->v4='';$rowZ->v5='';$rowZ->v6='';
				$rowZ->noma='Total';
				if($totkgs>0) $rowZ->kgs=number_format(($totkgs), 2, '.', ',');
				if($tkmn>0) $rowZ->kmn=number_format(($tkmn), 2, '.', ',');
				if($timn>0) $rowZ->imn='$'.number_format(($timn), 2, '.', ',');
				if($tkmn>0 && $timn>0) $rowZ->pvmn='$'.number_format(($timn/$tkmn), 2, '.', ',');
				if($tkus>0) $rowZ->kus=number_format(($tkus), 2, '.', ',');
				if($tius>0) $rowZ->ius='$'.number_format(($tius), 2, '.', ',');
				if($tkus>0 && $tius>0) $rowZ->pvus='$'.number_format(($tius/$tkus), 2, '.', ',');
				$ini=1;
				while($ini<=$gra-1){
				 switch($datos [$ini][0]){
					case '1': $rowZ->a1 =$datos [$ini][1];$rowZ->v1 =$datos [$ini][2];$rowZ->p1 =$datos [$ini][3];$rowZ->mn1 =$datos [$ini][4];$rowZ->us1 =$datos [$ini][5];$rowZ->pmn1 =$datos [$ini][6];$rowZ->pus1 =$datos [$ini][7];break;
					case '2': $rowZ->a2 =$datos [$ini][1];$rowZ->v2 =$datos [$ini][2];$rowZ->p2 =$datos [$ini][3];$rowZ->mn2 =$datos [$ini][4];$rowZ->us2 =$datos [$ini][5];$rowZ->pmn2 =$datos [$ini][6];$rowZ->pus2 =$datos [$ini][7];break;
					case '3': $rowZ->a3 =$datos [$ini][1];$rowZ->v3 =$datos [$ini][2];$rowZ->p3 =$datos [$ini][3];$rowZ->mn3 =$datos [$ini][4];$rowZ->us3 =$datos [$ini][5];$rowZ->pmn3 =$datos [$ini][6];$rowZ->pus3 =$datos [$ini][7];break;
					case '4': $rowZ->a4 =$datos [$ini][1];$rowZ->v4 =$datos [$ini][2];$rowZ->p4 =$datos [$ini][3];$rowZ->mn4 =$datos [$ini][4];$rowZ->us4 =$datos [$ini][5];$rowZ->pmn4 =$datos [$ini][6];$rowZ->pus4 =$datos [$ini][7];break;
					case '5': $rowZ->a5 =$datos [$ini][1];$rowZ->v5 =$datos [$ini][2];$rowZ->p5 =$datos [$ini][3];$rowZ->mn5 =$datos [$ini][4];$rowZ->us5 =$datos [$ini][5];$rowZ->pmn5 =$datos [$ini][6];$rowZ->pus5 =$datos [$ini][7];break;
					case '6': $rowZ->a6 =$datos [$ini][1];$rowZ->v6 =$datos [$ini][2];$rowZ->p6 =$datos [$ini][3];$rowZ->mn6 =$datos [$ini][4];$rowZ->us6 =$datos [$ini][5];$rowZ->pmn6 =$datos [$ini][6];$rowZ->pus6 =$datos [$ini][7];break;
				 }
				 $ini+=1;
			    }
				$data[] = $rowZ;	
			endforeach;	
			
			}
			return $data;
		}
    
   	
		function getAlmacenes(){
			$this->db->select('ida,noma');
			//$this->db->join('salidas', 'clisal=ida', 'inner');
			//$this->db->where('almsal',1);
			//$this->db->group_by('ida');
			//$this->db->group_by('noma');
			$this->db->order_by('noma');
			$result = $this->db->get('almacenes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
		}
		function historycli($cli){
			$this->db->select('Razon');
			$this->db->from('clientes');
			$this->db->where('Numero =', $cli);
			$query=$this->db->get();
			return $query->row();
		}
		function historycon($con){
			$this->db->select('noma');
			$this->db->from('almacenes');
			$this->db->where('ida =', $con);
			$query=$this->db->get();
			return $query->row();
		}
		function historygpo($tal){
			$this->db->select('gpoid');
			$this->db->from('tallas');
			$this->db->where('idt =', $tal);
			$query=$this->db->get();
			return $query->row();
		}
		function getElementsa($filter){        
        	$this->db->select("ida,noma as val");
			$this->db->join('almacenes', 'ida=almsal', 'inner');	
			$this->db->where('ciclo','20'.$filter['ciclo']);
			$this->db->where('estsal',1);
			$this->db->group_by('ida');
			//$this->db->order_by('sum(kgssal)','DESC');
	        $result = $this->db->get('salidas');       
        	$dataa = array();
			if($result->num_rows()>0){        
        	foreach($result->result() as $rowc):
            	$dataa[] = $rowc;
        	endforeach;
			}        
        	return $dataa;
    	}
		function getElementsb($filter){        
        	$this->db->select("idt,grupo as val");
			$this->db->join('tallas', 'idt=talsal', 'inner');	
			$this->db->where('ciclo','20'.$filter['ciclo']);
			$this->db->where('estsal',1);
			$this->db->group_by('grupo');
			$this->db->order_by('sum(kgssal)','DESC');
	        $result = $this->db->get('salidas');       
        	$datab = array();
			if($result->num_rows()>0){        
        	foreach($result->result() as $rowc):
            	$datab[] = $rowc;
        	endforeach;
			}        
        	return $datab;
    	}
		function getElementsc($filter){        
        	$this->db->select("Numero,Razon as val");
			$this->db->join('salidas', 'clisal=Numero', 'inner');	
			$this->db->where('almsal',$filter['ida']);
			$this->db->group_by('Numero');
			$this->db->group_by('Razon');
			$this->db->order_by('Razon');
			$result = $this->db->get('clientes');   
        	$datac = array();
			if($result->num_rows()>0){        
        	foreach($result->result() as $rowc):
            	$datac[] = $rowc;
        	endforeach;
			}
			return $datac;
			} 
			
			function getClientes(){
			$this->db->select('Numero,Razon');
			$this->db->join('salidas', 'clisal=Numero', 'inner');
			$this->db->where('estsal',1);
			$this->db->group_by('Numero');
			$this->db->group_by('Razon');
			$this->db->order_by('Razon');
			$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$data[] = $row;	
			endforeach;
			return $data;
			}       
        	
    	
    }
?>