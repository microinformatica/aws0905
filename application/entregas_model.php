<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Entregas_model extends CI_Model {        
        public $nbs="NumBioS";
        public $fol="Folio";
        public $act="Act";
        public $pre="Pre";
        public $efi="Efi";
        public $ser="Ser";
        public $adm="Adm";
        public $obs="Obs";
        public $foc="Foco";
		public $veri="Verificacion";
		public $nr="NumRemS";
		public $nc="NumCliS";
        public $id="NumSol";        
		public $tabla="r17";
		public $tabla2="serbio";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		function agregar($nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$nr,$nc){
			if($act=='') $act=0;if($pre=='') $pre=0;if($efi=='') $efi=0;if($adm=='') $adm=0;if($ser=='') $ser=0;if($foc=='') $foc=0;
			$data=array($this->nbs=>$nbs,$this->fol=>$fol,$this->act=>$act,$this->pre=>$pre,$this->efi=>$efi,$this->ser=>$ser,$this->adm=>$adm,$this->obs=>$obs,$this->foc=>$foc,$this->nr=>$nr,$this->nc=>$nc);			
			$this->db->insert($this->tabla2,$data);
			return $this->db->insert_id();
		}
		function actualizar($id,$nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$veri){
			if($act=='') $act=0;if($pre=='') $pre=0;if($efi=='') $efi=0;if($adm=='') $adm=0;if($ser=='') $ser=0;if($foc=='') $foc=0;if($veri=='') $veri=0;
			//$sqlConsulta ="update serbio set NumBioS='$nbio',Folio='$fol',Act='$act',Pre='$pre',Efi='$efi',Ser='$ser',Adm='$adm',Obs='$obs',Foco='$foco' where NumSol='".$claveR."'";			
			$data=array($this->nbs=>$nbs,$this->fol=>$fol,$this->act=>$act,$this->pre=>$pre,$this->efi=>$efi,$this->ser=>$ser,$this->adm=>$adm,$this->obs=>$obs,$this->foc=>$foc,$this->veri=>$veri);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla2,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function getCliente($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('NumRegR',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		public function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('biologo');
			return $query->result();			
		}		
		public function verDestino(){
			$this->db->select('NomDes');
			$this->db->where('numdes >',0);
			$this->db->group_by('NomDes'); 
			$query=$this->db->get('viaticos');
			return $query->result();			
		}
		public function verChofer(){			
			$this->db->where('activo =',0);
			$this->db->order_by('numcho');			
			$query=$this->db->get('chofer');
			return $query->result();			
		}
		public function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		public function verRemision(){
			$this->db->where('RemisionR >',0);			
			$this->db->order_by('NumRegR','DESC');
			$query=$this->db->get('r14');
			return $query->result();			
		}
		function getEntregas($filter){
			//select Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus 
			//from clientes inner join r13 on NumCliR=Numero
			// where FechaR >= '$fi' AND FechaR <= '$ff' AND Estatus<2 
			// order by FechaR,Zona,Razon,RemisionR
			//select Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion from serbio where NumCliS='$ncli' and NumRemS='$nrem'"
			//$this->db->select('Numero,Zona,Razon,sum(CantidadRR) as CantidadRR,(select count(*) from siembras where cliente=numero) as est');
			//$this->db->select('Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus,(select Folio from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Folio,(select Verificacion from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Verificacion');
			//$this->db->select('*,(select sum(CantidadRR) from r13 where FechaR=FechaR ) as dia');
			$this->db->select('Zona,Razon,NumRegR,FechaR,RemisionR,Folio,CantidadRR,Obs,sum(cantidadrr) as total,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion,numclir,NumRemS');
			//$this->db->select('Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion');
			$this->db->join('clientes', 'numero=numclir', 'inner');						
			$this->db->join('serbio', 'numrems=numregr', 'left');
			$this->db->where('estatus <',2);
			$this->db->group_by('fechar');
			$this->db->group_by('zona');	
			$this->db->group_by('razon');
			$this->db->group_by('remisionr');
			$this->db->group_by('folio');
			$this->db->group_by('cantidadrr');
			$this->db->group_by('obs');
			$this->db->group_by(array("foco", "numsol", "act", "pre", "efi", "ser", "adm", "numbios", "verificacion","numclir","NumRegR","NumRemS"));
			$this->db->order_by('fechar','ASC');
			$this->db->order_by('zona');	
			$this->db->order_by('razon');
			$this->db->order_by('remisionr');
			if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['num']!=10){
				if($filter['num']==3){
					$this->db->where('Foco',2);
				}else{	
				if($filter['num']==2){
					$this->db->where('Foco',-1);
				}else{
					if($filter['num']==0 || $filter['num']==-1) {
						$this->db->where('Verificacion',$filter['num']);}
					else{
						$this->db->where('Folio');	
					}
				}
				}				
			}
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $toti=0; $fec=new Libreria();
			 $zona="";$fecha1=date("y-m-d");$sub1=0;$cli="";
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($fecha1!=$row->FechaR){
					$row->FechaR = $fec->fecha($row->FechaR);
					$fecha1=$row->FechaR; //$zona="";
					//$row->totd=$row->dia;					
				} else{ 
		  			//if($encargado!=$row->NomBio){
		  			//} else {
		  			$row->FechaR="";	
					//$row->totd='';
		  		} 
				//}			
				if($zona!=$row->Zona){
					$zona=$row->Zona;
				}
				else{
					$row->Zona="";
				}
				if($cli!=$row->Razon){
					$cli=$row->Razon;
					$row->Razon1=$row->Razon;
				}
				else{
					$row->Razon1="";
				}
				/*if($row->NomDesS == "Improvistos" or $row->NomDesS == "Complemento Gasto"){ $row->NomUni="";	}
				$row->Viatico = '$ '.number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$sub1=($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit);
				$row->sub =number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit), 2, '.', ',');
				//$row->sub = number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$row->Cargo = '$ '.number_format(($row->Cargo), 2, '.', ',');
				if($row->Ali>0){ $row->Ali = number_format(($row->Ali), 2, '.', ',');} else {$row->Ali ="";}
				if($row->Com>0){ $row->Com = number_format(($row->Com), 2, '.', ',');} else {$row->Com ="";}
				if($row->Cas>0){ $row->Cas = number_format(($row->Cas), 2, '.', ',');} else {$row->Cas ="";}
				if($row->Hos>0){ $row->Hos = number_format(($row->Hos), 2, '.', ',');} else {$row->Hos ="";}*/
				if($row->Folio>0){}else{$row->Folio="";}
				if($row->Obs!=''){}else{$row->Obs="";}
				$row->total+=$row->CantidadRR; $toti+=$row->CantidadRR;					
				$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ',');				
				$data[] = $row;
			endforeach;
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row1):
				$row1->NumCliR =0;
			$row1->FechaR = "Total";$row1->Zona = "";$row1->Razon1 = "";$row1->Folio = "";$row1->RemisionR = "";$row1->Obs = "";
			$row1->CantidadRR = number_format(($toti), 3, '.', ',');
			$data[] = $row1;
			endforeach;
			
			return $data;
		}
		
		function getNumRowsE($filter){
			//$this->db->select('Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus,(select Folio from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Folio');
			$this->db->select('Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion');
			$this->db->join('clientes', 'numero=numclir', 'inner');						
			$this->db->join('serbio', 'numrems=numregr', 'left');
			/*$this->db->join('clientes', 'numero=numclir', 'inner');	
			$this->db->join('serbio', 'numrems=numregr', 'left');					
			$this->db->where('estatus <',2);	
			$this->db->group_by('fechar');
			$this->db->group_by('zona');	
			$this->db->group_by('razon');
			$this->db->group_by('remisionr');*/
			$this->db->group_by(array("fechar", "zona", "razon", "remisionr", "obs", "folio", "foco", "numsol", "act", "pre", "efi", "ser", "adm", "numbios", "verificacion"));		
			$this->db->order_by('fechar','ASC');
			$this->db->order_by('zona');	
			$this->db->order_by('razon');
			$this->db->order_by('remisionr');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=10){
				if($filter['num']==2){
					$this->db->where('Foco',-1);
				}else{
					if($filter['num']==0 || $filter['num']==-1) {
						$this->db->where('Verificacion',$filter['num']);}
					else{
						$this->db->where('Folio');	
					}
				}				
			}
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla2);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	public function imprimirdetalle(){
			//$ncli=335;
			$this->db->select('NumSol,NomDesS,NomUni,NomBio,FechaS,Ali,Com,Cas,Hos,Fit,Rep,NumRemS,NumCliS,Cargo');
			//$this->db->from('r12');
			$this->db->join('biologo', 'NumBioS=NumBio', 'inner');
			$this->db->join('unidad', 'NumUniS=NumUni', 'inner');
			$this->db->where('NumSol >',0);			
			$this->db->where('CarAbo =',1);
			//$this->db->where('Numero',220);
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Nombio');
			$this->db->order_by('NomDesS');
			$query = $this->db->get('solicitud');			
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
	function getElements($where){
		if($where['NomDes']!='Todos'){			
		$this->db->select('NumUni,NomUni as val');
		$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
		//$this->db->where('NumDes >',0);
		$this->db->where('Activo =',0);	
		$this->db->group_by('NumUni');        
        $this->db->where($where);
        $result=$this->db->get('viaticos');
        $data = array();        
        foreach($result->result() as $row):
            $data[] = $row;
        endforeach;        
        return $data;
		}
    }
	
	function getEntregastecres($filter){
			//select NumBio,NomBio, (count(*))as entregas,(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1 from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio order by NumBio
			$query=$this->db->query("select NumBio,NomBio,(count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1, 
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio order by gra,ser,act,pre,efi,adm");
			//Se forma el arreglo que sera retornado
			$numbio=0;
			foreach ($query->result() as $row):
				//obtener las entregas registradas de las remisiones
				//select NumBio,NomBio,count(*) from r17 inner join biologo on NumBio=NumBioR where NumBio=29
				$numbio=$row->NumBio;
				$querye=$this->db->query("select count(*) as rement from r17 inner join biologo on NumBio=NumBioR where NumBio=$numbio");
				foreach ($querye->result() as $rowt):
					$row->entregas=$row->entregas." / ".$rowt->rement." = ".($rowt->rement-$row->entregas);
				endforeach;	
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;
			//$query=$this->db->query("select NumBio,NomBio, (count(*))as entregas,
			$query=$this->db->query("select (count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1,
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2");
			foreach ($query->result() as $row):				
				$row->NomBio = "Total General:";$row->NumBio = 0;
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;	
			return $data;
		}
		function getEntregastecresdet($bio){
			//select NumBio,NomBio,Folio,Ser,Act,Pre,Efi,Adm,Obs from biologo inner join serbio on NumBioS=NumBio where NumBio>2 and NumBio=18
			$query=$this->db->query("select NumBio,NomBio,Razon,Folio,Ser,Act,Pre,Efi,Adm,Obs from biologo inner join(clientes inner join serbio on NumCliS=Numero )on NumBioS=NumBio where NumBio>2 and NumBio=$bio order by Folio");
			//if($filter['where']!=''){$this->db->where($filter['where']);}
			//Se forma el arreglo que sera retornado
			foreach ($query->result() as $row):
				if($row->Ser==0){$row->ce = "";$row->cr = "";$row->cm="";}else{
					if($row->Ser==2){$row->ce = "*";$row->cr = "";$row->cm="";}
						elseif ($row->Ser==-2) { $row->ce = "";$row->cr = "";$row->cm="*";
						}else{$row->ce = "";$row->cr = "*";$row->cm="";}
				}
				if($row->Act==0){$row->ae = "";$row->ar = "";$row->am="";}else{
					if($row->Act==2){$row->ae = "*";$row->ar = "";$row->am="";}
						elseif ($row->Act==-2) { $row->ae = "";$row->ar = "";$row->am="*";
						}else{$row->ae = "";$row->ar = "*";$row->am="";}
				}
				if($row->Pre==0){$row->pe = "";$row->pr = "";$row->pm="";}else{
					if($row->Pre==2){$row->pe = "*";$row->pr = "";$row->pm="";}
						elseif ($row->Pre==-2) { $row->pe = "";$row->pr = "";$row->pm="*";
						}else{$row->pe = "";$row->pr = "*";$row->pm="";}
				}
				if($row->Efi==0){$row->ee = "";$row->er = "";$row->em="";}else{
					if($row->Efi==2){$row->ee = "*";$row->er = "";$row->em="";}
						elseif ($row->Efi==-2) { $row->ee = "";$row->er = "";$row->em="*";
						}else{$row->ee = "";$row->er = "*";$row->em="";}
				}
				if($row->Adm==0){$row->oe = "";$row->ro = "";$row->om="";}else{
					if($row->Adm==2){$row->oe = "*";$row->ro = "";$row->om="";}
						elseif ($row->Adm==-2) { $row->oe = "";$row->ro = "";$row->om="*";
						}else{$row->oe = "";$row->ro = "*";$row->om="";}
				}
				$data[] = $row;	
			endforeach;
			/*$query=$this->db->query("select NumBio,NomBio, (count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1,
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2");
			foreach ($query->result() as $row):				
				$row->NomBio = "Total General:";$row->NumBio = 0;
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;	*/
			return $data;
		}
		function verTecnicoD(){
			$query=$this->db->query("select NumBio,NomBio from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio");
			return $query->result();			
		}
		function verTecnicoDN(){
			$query=$this->db->query("select NumBio,NomBio from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio");
			return $query->result();			
		}

    }
    
?>