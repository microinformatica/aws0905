<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Logistica_model extends CI_Model {        
        public $nbs="NumBioS";public $fol="Folio";public $act="Act";public $pre="Pre";public $efi="Efi";public $ser="Ser";
        public $adm="Adm";public $obs="Obs";public $foc="Foco";public $veri="Verificacion";public $nr="NumRemS";
		public $nc="NumCliS";public $id="NumSol";        
		public $tabla="r22";
		public $tabla2="serbio";
        public $ciclo="r22";
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		function agregar($nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$nr,$nc){
			if($act=='') $act=0;if($pre=='') $pre=0;if($efi=='') $efi=0;if($adm=='') $adm=0;if($ser=='') $ser=0;if($foc=='') $foc=0;
			$data=array($this->nbs=>$nbs,$this->fol=>$fol,$this->act=>$act,$this->pre=>$pre,$this->efi=>$efi,$this->ser=>$ser,$this->adm=>$adm,$this->obs=>$obs,$this->foc=>$foc,$this->nr=>$nr,$this->nc=>$nc);			
			$this->db->insert($this->tabla2,$data);
			return $this->db->insert_id();
		}
		function actualizar($id,$nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$veri){
			if($act=='') $act=0;if($pre=='') $pre=0;if($efi=='') $efi=0;if($adm=='') $adm=0;if($ser=='') $ser=0;if($foc=='') $foc=0;if($veri=='') $veri=0;
			//$sqlConsulta ="update serbio set NumBioS='$nbio',Folio='$fol',Act='$act',Pre='$pre',Efi='$efi',Ser='$ser',Adm='$adm',Obs='$obs',Foco='$foco' where NumSol='".$claveR."'";			
			$data=array($this->nbs=>$nbs,$this->fol=>$fol,$this->act=>$act,$this->pre=>$pre,$this->efi=>$efi,$this->ser=>$ser,$this->adm=>$adm,$this->obs=>$obs,$this->foc=>$foc,$this->veri=>$veri);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla2,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function getCliente($id){
			/*$query=$this->db->get($tabla);*/
			
			$this->db->where('NumRegR',$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}
		function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('numbio');
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		
		function verTecnico2(){
			$this->db->where('numbio >',2);
			$this->db->where('activo =',0);
			$this->db->order_by('numbio');
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		public function verDestino(){
			$this->db->select('NomDes');
			$this->db->where('numdes >',0);
			$this->db->group_by('NomDes'); 
			$query=$this->db->get('viaticos');
			return $query->result();			
		}
					
		/*function verDestino(){
			$this->db->where('numdes >',0);
			$this->db->group_by('nomdes');
			$query=$this->db->get('viaticos');
			return $query->result();			
		}*/
		function verChofer(){			
			$this->db->where('activo =',0);
			$this->db->order_by('numcho');			
			$query=$this->db->get('chofer');
			return $query->result();			
		}
		function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		function verRemision(){
			$this->db->where('RemisionR >',0);			
			$this->db->order_by('NumRegR','DESC');
			$query=$this->db->get('r18');
			return $query->result();			
		}
		function getEntregas($filter){
			//select Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus 
			//from clientes inner join r13 on NumCliR=Numero
			// where FechaR >= '$fi' AND FechaR <= '$ff' AND Estatus<2 
			// order by FechaR,Zona,Razon,RemisionR
			//select Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion from serbio where NumCliS='$ncli' and NumRemS='$nrem'"
			//$this->db->select('Numero,Zona,Razon,sum(CantidadRR) as CantidadRR,(select count(*) from siembras where cliente=numero) as est');
			//$this->db->select('Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus,(select Folio from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Folio,(select Verificacion from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Verificacion');
			//$this->db->select('*,(select sum(CantidadRR) from r13 where FechaR=FechaR ) as dia');
			$this->db->select('Zona,Razon,NumRegR,FechaR,RemisionR,Folio,CantidadRR,Obs,sum(cantidadrr) as total,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion,numclir,NumRemS');
			//$this->db->select('Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion');
			$this->db->join('clientes', 'numero=numclir', 'inner');						
			$this->db->join('serbio', 'numrems=numregr', 'left');
			$this->db->where('estatus <',2);
			$this->db->group_by('fechar');
			$this->db->group_by('zona');	
			$this->db->group_by('razon');
			$this->db->group_by('remisionr');
			$this->db->group_by('folio');
			$this->db->group_by('cantidadrr');
			$this->db->group_by('obs');
			$this->db->group_by(array("foco", "numsol", "act", "pre", "efi", "ser", "adm", "numbios", "verificacion","numclir","NumRegR","NumRemS"));
			$this->db->order_by('fechar','ASC');
			$this->db->order_by('zona');	
			$this->db->order_by('razon');
			$this->db->order_by('remisionr');
			if($filter['where']!='') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['num']!=10){
				if($filter['num']==3){
					$this->db->where('Foco',2);
				}else{	
				if($filter['num']==2){
					$this->db->where('Foco',-1);
				}else{
					if($filter['num']==0 || $filter['num']==-1) {
						$this->db->where('Verificacion',$filter['num']);}
					else{
						$this->db->where('Folio');	
					}
				}
				}				
			}
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $toti=0; $fec=new Libreria();
			 $zona="";$fecha1=date("y-m-d");$sub1=0;$cli="";
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($fecha1!=$row->FechaR){
					$row->FechaR = $fec->fecha($row->FechaR);
					$fecha1=$row->FechaR; //$zona="";
					//$row->totd=$row->dia;					
				} else{ 
		  			//if($encargado!=$row->NomBio){
		  			//} else {
		  			$row->FechaR="";	
					//$row->totd='';
		  		} 
				//}			
				if($zona!=$row->Zona){
					$zona=$row->Zona;
				}
				else{
					$row->Zona="";
				}
				if($cli!=$row->Razon){
					$cli=$row->Razon;
					$row->Razon1=$row->Razon;
				}
				else{
					$row->Razon1="";
				}
				/*if($row->NomDesS == "Improvistos" or $row->NomDesS == "Complemento Gasto"){ $row->NomUni="";	}
				$row->Viatico = '$ '.number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$sub1=($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit);
				$row->sub =number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit), 2, '.', ',');
				//$row->sub = number_format(($row->Ali+$row->Com+$row->Cas+$row->Hos+$row->Fit+$row->Rep), 2, '.', ',');
				$row->Cargo = '$ '.number_format(($row->Cargo), 2, '.', ',');
				if($row->Ali>0){ $row->Ali = number_format(($row->Ali), 2, '.', ',');} else {$row->Ali ="";}
				if($row->Com>0){ $row->Com = number_format(($row->Com), 2, '.', ',');} else {$row->Com ="";}
				if($row->Cas>0){ $row->Cas = number_format(($row->Cas), 2, '.', ',');} else {$row->Cas ="";}
				if($row->Hos>0){ $row->Hos = number_format(($row->Hos), 2, '.', ',');} else {$row->Hos ="";}*/
				if($row->Folio>0){}else{$row->Folio="";}
				if($row->Obs!=''){}else{$row->Obs="";}
				$row->total+=$row->CantidadRR; $toti+=$row->CantidadRR;					
				$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ',');				
				$data[] = $row;
			endforeach;
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row1):
				$row1->NumCliR =0;
			$row1->FechaR = "Total";$row1->Zona = "";$row1->Razon1 = "";$row1->Folio = "";$row1->RemisionR = "";$row1->Obs = "";
			$row1->CantidadRR = number_format(($toti), 3, '.', ',');
			$data[] = $row1;
			endforeach;
			
			return $data;
		}
		
		function getNumRowsE($filter){
			//$this->db->select('Zona,Razon,FechaR,RemisionR,CantidadRR,NumRegR,NumCliR,Estatus,(select Folio from serbio where NumCliS=NumCliR and NumRemS=NumRegR group by Folio) as Folio');
			$this->db->select('Obs,Folio,Foco,NumSol,Act,Pre,Efi,Ser,Adm,NumBioS,Verificacion');
			$this->db->join('clientes', 'numero=numclir', 'inner');						
			$this->db->join('serbio', 'numrems=numregr', 'left');
			/*$this->db->join('clientes', 'numero=numclir', 'inner');	
			$this->db->join('serbio', 'numrems=numregr', 'left');					
			$this->db->where('estatus <',2);	
			$this->db->group_by('fechar');
			$this->db->group_by('zona');	
			$this->db->group_by('razon');
			$this->db->group_by('remisionr');*/
			$this->db->group_by(array("fechar", "zona", "razon", "remisionr", "obs", "folio", "foco", "numsol", "act", "pre", "efi", "ser", "adm", "numbios", "verificacion"));		
			$this->db->order_by('fechar','ASC');
			$this->db->order_by('zona');	
			$this->db->order_by('razon');
			$this->db->order_by('remisionr');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=10){
				if($filter['num']==2){
					$this->db->where('Foco',-1);
				}else{
					if($filter['num']==0 || $filter['num']==-1) {
						$this->db->where('Verificacion',$filter['num']);}
					else{
						$this->db->where('Folio');	
					}
				}				
			}
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla2);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	public function imprimirdetalle(){
			//$ncli=335;
			$this->db->select('NumSol,NomDesS,NomUni,NomBio,FechaS,Ali,Com,Cas,Hos,Fit,Rep,NumRemS,NumCliS,Cargo');
			//$this->db->from('r12');
			$this->db->join('biologo', 'NumBioS=NumBio', 'inner');
			$this->db->join('unidad', 'NumUniS=NumUni', 'inner');
			$this->db->where('NumSol >',0);			
			$this->db->where('CarAbo =',1);
			//$this->db->where('Numero',220);
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Nombio');
			$this->db->order_by('NomDesS');
			$query = $this->db->get('solicitud');			
			if($query->num_rows()>0){
				return $query->result();	
			}else{
				return 0;	
			}
		}
	
	function getElements($where){
		if($where['NomDes']!='Todos'){			
		$this->db->select('NumUni,NomUni as val');
		$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
		//$this->db->where('NumDes >',0);
		$this->db->where('Activo =',0);	
		$this->db->group_by('NumUni');        
        $this->db->where($where);
        $result=$this->db->get('viaticos');
        $data = array();        
        foreach($result->result() as $row):
            $data[] = $row;
        endforeach;        
        return $data;
		}
    }
	
	
	//tecnicos
	
	/*function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$query=$this->db->get('biologo');
			return $query->result();			
		}*/
		function verTecnico1($nt){
			$this->db->where('numbio =',$nt);
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		function getTecnicos($filter,$rz){
			$veces=10;//$veces=6;$contador=1;	
			$mex=1;	$primera=1;
			$data = array();
	 		while($mex<=$veces){
		 		//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
				if($primera==1) $mex=12;
				$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR,Zona');
				$this->db->join('clientes', 'numero=numclir', 'inner');	
				$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('month(FechaR) =',$mex);
				$this->db->where('FechaR >',0);
				//$this->db->where('NomDesS !=','Improvistos');
				$this->db->where('NomDesS !=','Complemento Gasto');
				$this->db->group_by(array("FechaR", "RemisionR","Zona"));
				
				$result = $this->db->get($this->ciclo);	
				$cantidadre=$result->num_rows();
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$mes=$row->mes;$datosd [$dia]=$row->dia;
						//$datosr [$dia]="*";
						if($rz==1) $datosr [$dia]=$row->RemisionR; else $datosr [$dia]=substr($row->Zona, 0, 4);
						$tot+=1;
					endforeach;
					switch($mes){
						case 1: $row->mes = "Ene"; break;	case 2: $row->mes = "Feb"; break;	case 3: $row->mes = "Mar"; break;
						case 4: $row->mes = "Abr"; break;	case 5: $row->mes = "May"; break;	case 6: $row->mes = "Jun"; break;
						case 7: $row->mes = "Jul"; break;	case 8: $row->mes = "Ago"; break;	case 9: $row->mes = "Sep"; break;	
						case 10: $row->mes = "Oct"; break;  case 12: $row->mes = "Dic"; break;
						
					}	
					if($cantidadre>=1 && $mex==12){$primera=2;$mex=0;}
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tot = $tot;
					$data[] = $row;
				}
				if($primera==1){$primera=2;$mex=0;}
				//if ($primera==2){$mex=1;} else {$mex+=1;}
				$mex+=1;
			}
			return $data;
		}
		function getNumRowsT($filter){
			return 10;
		}
		function getTotet($filter){
			$this->db->select('NumBio,NomBio');
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$result = $this->db->get('biologo');
			$data = array();
			foreach ($result->result() as $row):
				//select count(*) from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=52 and FechaR>0 and CarAbo=1 and NomDesS!='Improvistos' and NomDesS!='Complemento Gasto' group by NumBioS,NumRemS,RemisionR
				$bio=$row->NumBio;
				$veces=12;//$veces=6;$contador=1;	
				$mex=1; $cont=0;		
				while($mex<=$veces){
		 			//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
					$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR');
					$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
					$this->db->where('NumBioS',$bio);
					$this->db->where('month(FechaR) =',$mex);
					$this->db->where('FechaR >',0);
					//$this->db->where('NomDesS !=','Improvistos');
					$this->db->where('NomDesS !=','Complemento Gasto');
					$this->db->group_by(array("FechaR", "RemisionR"));
					$result1 = $this->db->get($this->ciclo);	
					$cantidadre=$result1->num_rows();
					if($cantidadre>=1){
						foreach ($result1->result() as $row1):
							$cont+=1;
						endforeach;
					}
					$mex+=1;
				}
				$row->viajes=$cont;
				$data[] = $row;
			endforeach;	
			return $data;
		}	
		function getNumRowsTotet($filter){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$result = $this->db->get('biologo');	
			return $result->num_rows();
		}
		function getTecnicosdet($filter){
				//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
				$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR,FechaR,Razon,Zona');
				$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
				$this->db->join('clientes', 'Numero=NumCliR','inner');
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('FechaR >',0);
				//$this->db->where('NomDesS !=','Improvistos');
				$this->db->where('NomDesS !=','Complemento Gasto');
				$this->db->group_by(array("FechaR", "RemisionR", "Razon", "Zona"));
				$result = $this->db->get($this->ciclo);	
				$cantidadre=$result->num_rows();
				$data = array(); $fec=new Libreria();
				if($cantidadre>=1){
					$mex=0; $cont=0;
					foreach ($result->result() as $row):
						$mes=$row->mes;$act=$row->FechaR;
						$row->FechaR1 = $fec->fecha($row->FechaR);
						switch($mes){
							case 1: $row->mesi = "Ene"; break;	case 2: $row->mesi = "Feb"; break;	case 3: $row->mesi = "Mar"; break;
							case 4: $row->mesi = "Abr"; break;	case 5: $row->mesi = "May"; break;	case 6: $row->mesi = "Jun"; break;
							case 7: $row->mesi = "Jul"; break;	case 8: $row->mesi = "Ago"; break;	case 9: $row->mesi = "Sep"; break;	
							case 10: $row->mesi = "Oct"; break; case 12: $row->mesi = "Dic"; break;
						}
						if($mex!=$mes){	$mex=$row->mes;	}else{ $row->mesi="";}
						if($cont==0){$row->diasi=''; $ant=$row->FechaR;}
						else{
							$act = strtotime($act);  //strtotime("2011-02-03 00:00:00");
							$ant = strtotime($ant); //strtotime("1964-10-30 00:00:00");
							$row->diasi= round(abs($act-$ant)/60/60/24);
							$ant=$row->FechaR;
						}
						$cont+=1;
						$data[] = $row;	
					endforeach;
				}
			return $data;
		}
		function getNumRowsTdet($filter){
			$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
			if($filter['where']!=''){$this->db->where($filter['where']);}
			$this->db->where('FechaR >',0);
			//$this->db->where('NomDesS !=','Improvistos');
			$this->db->where('NomDesS !=','Complemento Gasto');
			//$this->db->group_by(array("FechaR", "RemisionR"));
			//$this->db->group_by(array("FechaR", "RemisionR", "Razon", "Zona"));
			$result = $this->db->get($this->ciclo);	
			return $result->num_rows();
		}
		//Gral Mes
		function getTecnicosGral($filter,$rz){
			$ini=1;
			$data = array();
			//agarrar los numeros de biologos activos
			$this->db->select('NumBio'); $this->db->where('numbio >',2); $this->db->where('activo =',0); $this->db->order_by('numbio');
			$query=$this->db->get('biologo');
			$bio=1; $cont=0;
			foreach ($query->result() as $row):
				$datosBi [$bio]=$row->NumBio; $datoslug [$bio]=$bio;
				$bio+=1;$cont+=1;
			endforeach;
			$tt=1;
			while($tt<=18){ $t[$tt]=0;	$tt+=1;	}
			while($ini<=31){
				//buscar las remisiones del biologo
				//$bio=0;$bio+=1;$tec=0;$tec+=1;
				//while($tec<=$cont){
				//select FechaR,RemisionR,NomBio,NumBio 
				//from r13 
				//inner join( biologo inner join solicitud on NumBio=NumBioS) on NumRegR=NumRemS 
				//where day(FechaR)=10 and month(FechaR)=1 
				//group by NumBio
					$this->db->select('day(FechaR) as dia,RemisionR,NumBioS,Zona');
					$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
					$this->db->join('clientes', 'Numero=NumCliR','inner');
					$this->db->join('biologo', 'NumBio=NumBioS','inner');
					if($filter['where']!=''){$this->db->where($filter['where']);}
					//$this->db->where('NumBioS =',$datosB [$bio]);
					$this->db->where('day(FechaR) =',$ini);
					$this->db->where('activo =',0);
					$this->db->where('FechaR >',0);
					//$this->db->where('NomDesS !=','Improvistos');
					//$this->db->where('NomDesS !=','Complemento Gasto');
					$this->db->group_by(array("day(FechaR),NumBio,RemisionR,NumBioS,Zona"));
					$result = $this->db->get($this->ciclo);	
					$cantidadre=$result->num_rows(); $conta=1; $entro=0; $ultimo=""; $contax=0; $resp1=1; 
					//if($cantidadre>1){
					foreach ($result->result() as $row):
						$resp=$row->NumBioS;
						while($resp1<=$cont){
							if($resp==$datosBi[$resp1]){ $datosres[$conta]=$datoslug [$resp1];	$ultimo=$resp1;	$resp1=$cont;}
							$resp1+=1;
						}
						$resp1=$ultimo;
						$datosb [$conta]=$row->NumBioS;	
						if($rz==1) $datosr [$conta]=$row->RemisionR; else $datosr [$conta]=substr($row->Zona, 0, 4);
						
						$conta+=1; $entro=1;
					endforeach;
					if($entro==1){
						$inic=1; $conta1=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d1 = $datosr [$conta1];$t[1]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d1 ="";}}else{$row->d1 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d2 = $datosr [$conta1];$t[2]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d2 ="";}}else{$row->d2 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d3 = $datosr [$conta1];$t[3]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d3 ="";}}else{$row->d3 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d4 = $datosr [$conta1];$t[4]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d4 ="";}}else{$row->d4 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d5 = $datosr [$conta1];$t[5]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d5 ="";}}else{$row->d5 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d6 = $datosr [$conta1];$t[6]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d6 ="";}}else{$row->d6 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d7 = $datosr [$conta1];$t[7]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d7 ="";}}else{$row->d7 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d8 = $datosr [$conta1];$t[8]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d8 ="";}}else{$row->d8 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d9 = $datosr [$conta1];$t[9]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d9 ="";}}else{$row->d9 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d10 = $datosr [$conta1];$t[10]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d10 ="";}}else{$row->d10 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d11 = $datosr [$conta1];$t[11]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d11 ="";}}else{$row->d11 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d12 = $datosr [$conta1];$t[12]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d12 ="";}}else{$row->d12 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d13 = $datosr [$conta1];$t[13]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d13 ="";}}else{$row->d13 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d14 = $datosr [$conta1];$t[14]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d14 ="";}}else{$row->d14 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d15 = $datosr [$conta1];$t[15]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d15 ="";}}else{$row->d15 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d16 = $datosr [$conta1];$t[16]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d16 ="";}}else{$row->d16 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d17 = $datosr [$conta1];$t[17]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d17 ="";}}else{$row->d17 ="";}$inic+=1;
						if($inic<=$cont && $conta1<$conta){ if($datoslug [$inic] == $datosres [$conta1]){ $row->d18 = $datosr [$conta1];$t[18]+=1;if($conta1<$conta) $conta1+=1; }else{$row->d18 ="";}}else{$row->d18 ="";}$inic+=1;
						$data[] = $row;$resp1=1;
						while($resp1<=$cont){ $datosres[$resp1]="";	$resp1+=1;	}
					}
					else{
						$this->db->select('max(RemisionR)');
						$result = $this->db->get($this->tabla);
						foreach($result->result() as $row1):
							$row1->dia =$ini;
							$row1->d1 ="";$row1->d2 ="";$row1->d3 ="";$row1->d4 ="";$row1->d5 ="";$row1->d6 ="";$row1->d7 ="";$row1->d8 ="";$row1->d9 ="";$row1->d10 ="";
							$row1->d11 ="";$row1->d12 ="";$row1->d13 ="";$row1->d14 ="";$row1->d15 ="";$row1->d16 ="";$row1->d17 ="";$row1->d18 =""; //$row1->d19 ="";$row1->d20 ="";
							$data[] = $row1;
						endforeach;
					}
					$ini+=1;
			}
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row1):
				$row1->dia ="Tot:";
				if($t[1]>0) $row1->d1 =$t[1]; else $row1->d1 ="";	if($t[2]>0) $row1->d2 =$t[2]; else $row1->d2 ="";
				if($t[3]>0) $row1->d3 =$t[3]; else $row1->d3 ="";	if($t[4]>0) $row1->d4 =$t[4]; else $row1->d4 ="";
				if($t[5]>0) $row1->d5 =$t[5]; else $row1->d5 ="";	if($t[6]>0) $row1->d6 =$t[6]; else $row1->d6 ="";
				if($t[7]>0) $row1->d7 =$t[7]; else $row1->d7 ="";	if($t[8]>0) $row1->d8 =$t[8]; else $row1->d8 ="";
				if($t[9]>0) $row1->d9 =$t[9]; else $row1->d9 ="";	if($t[10]>0) $row1->d10 =$t[10]; else $row1->d10 ="";
				if($t[11]>0) $row1->d11 =$t[11]; else $row1->d11 ="";	if($t[12]>0) $row1->d12 =$t[12]; else $row1->d12 ="";
				if($t[13]>0) $row1->d13 =$t[13]; else $row1->d13 ="";	if($t[14]>0) $row1->d14 =$t[14]; else $row1->d14 ="";
				if($t[15]>0) $row1->d15 =$t[15]; else $row1->d15 ="";	if($t[16]>0) $row1->d16 =$t[16]; else $row1->d16 ="";
				if($t[17]>0) $row1->d17 =$t[17]; else $row1->d17 ="";	if($t[18]>0) $row1->d18 =$t[18]; else $row1->d18 ="";
				$data[] = $row1;
			endforeach;
			
			return $data;	
		}
		function getNumRowsTGral($filter){
			return 31; // $result->num_rows();
		}	
		
		function getEntregastecres($filter){
			//select NumBio,NomBio, (count(*))as entregas,(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1 from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio order by NumBio
			$query=$this->db->query("select NumBio,NomBio,(count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1, 
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio order by gra,ser,act,pre,efi,adm");
			//Se forma el arreglo que sera retornado
			$numbio=0;
			foreach ($query->result() as $row):
				//obtener las entregas registradas de las remisiones
				//select NumBio,NomBio,count(*) from r18 inner join biologo on NumBio=NumBioR where NumBio=29
				$numbio=$row->NumBio;
				$querye=$this->db->query("select count(*) as rement from r18 inner join biologo on NumBio=NumBioR where NumBio=$numbio");
				foreach ($querye->result() as $rowt):
					$row->entregas=$row->entregas." / ".$rowt->rement." = ".($rowt->rement-$row->entregas);
				endforeach;	
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;
			//$query=$this->db->query("select NumBio,NomBio, (count(*))as entregas,
			$query=$this->db->query("select (count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1,
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2");
			foreach ($query->result() as $row):				
				$row->NomBio = "Total General:";$row->NumBio = 0;
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;	
			return $data;
		}
		function getEntregastecresdet($bio){
			//select NumBio,NomBio,Folio,Ser,Act,Pre,Efi,Adm,Obs from biologo inner join serbio on NumBioS=NumBio where NumBio>2 and NumBio=18
			$query=$this->db->query("select NumBio,NomBio,Razon,Folio,Ser,Act,Pre,Efi,Adm,Obs from biologo inner join(clientes inner join serbio on NumCliS=Numero )on NumBioS=NumBio where NumBio>2 and NumBio=$bio order by Folio");
			//if($filter['where']!=''){$this->db->where($filter['where']);}
			//Se forma el arreglo que sera retornado
			foreach ($query->result() as $row):
				if($row->Ser==0){$row->ce = "";$row->cr = "";$row->cm="";}else{
					if($row->Ser==2){$row->ce = "*";$row->cr = "";$row->cm="";}
						elseif ($row->Ser==-2) { $row->ce = "";$row->cr = "";$row->cm="*";
						}else{$row->ce = "";$row->cr = "*";$row->cm="";}
				}
				if($row->Act==0){$row->ae = "";$row->ar = "";$row->am="";}else{
					if($row->Act==2){$row->ae = "*";$row->ar = "";$row->am="";}
						elseif ($row->Act==-2) { $row->ae = "";$row->ar = "";$row->am="*";
						}else{$row->ae = "";$row->ar = "*";$row->am="";}
				}
				if($row->Pre==0){$row->pe = "";$row->pr = "";$row->pm="";}else{
					if($row->Pre==2){$row->pe = "*";$row->pr = "";$row->pm="";}
						elseif ($row->Pre==-2) { $row->pe = "";$row->pr = "";$row->pm="*";
						}else{$row->pe = "";$row->pr = "*";$row->pm="";}
				}
				if($row->Efi==0){$row->ee = "";$row->er = "";$row->em="";}else{
					if($row->Efi==2){$row->ee = "*";$row->er = "";$row->em="";}
						elseif ($row->Efi==-2) { $row->ee = "";$row->er = "";$row->em="*";
						}else{$row->ee = "";$row->er = "*";$row->em="";}
				}
				if($row->Adm==0){$row->oe = "";$row->ro = "";$row->om="";}else{
					if($row->Adm==2){$row->oe = "*";$row->ro = "";$row->om="";}
						elseif ($row->Adm==-2) { $row->oe = "";$row->ro = "";$row->om="*";
						}else{$row->oe = "";$row->ro = "*";$row->om="";}
				}
				$data[] = $row;	
			endforeach;
			/*$query=$this->db->query("select NumBio,NomBio, (count(*))as entregas,
									(sum(Ser)/(count(*)*2)*100)as ser,(sum(Ser))as ser1,
									(sum(Act)/(count(*)*2)*100)as act,(sum(Act))as act1,
									(sum(Pre)/(count(*)*2)*100)as pre,(sum(Pre))as pre1,
									(sum(Efi)/(count(*)*2)*100)as efi,(sum(Efi))as efi1,
									(sum(Adm)/(count(*)*2)*100)as adm,(sum(Adm))as adm1,
									(sum(Ser+Act+Pre+Efi+Adm)/((count(*)*2)*5)*100)as gra,(sum(Ser+Act+Pre+Efi+Adm))as gra1
									from biologo inner join serbio on NumBioS=NumBio where NumBio>2");
			foreach ($query->result() as $row):				
				$row->NomBio = "Total General:";$row->NumBio = 0;
				if($row->ser>0){$row->ser = number_format(($row->ser), 2, '.', ',')."%";$row->ser1 =$row->ser1." / ".($row->entregas*2);}else{$row->ser ="";$row->ser1 ="";}
				if($row->act>0){$row->act = number_format(($row->act), 2, '.', ',')."%";$row->act1 =$row->act1." / ".($row->entregas*2);}else{$row->act ="";$row->act1 ="";}				
				if($row->pre>0){$row->pre = number_format(($row->pre), 2, '.', ',')."%";$row->pre1 =$row->pre1." / ".($row->entregas*2);}else{$row->pre ="";$row->pre1 ="";}
				if($row->efi>0){$row->efi = number_format(($row->efi), 2, '.', ',')."%";$row->efi1 =$row->efi1." / ".($row->entregas*2);}else{$row->efi ="";$row->efi1 ="";}
				if($row->adm>0){$row->adm = number_format(($row->adm), 2, '.', ',')."%";$row->adm1 =$row->adm1." / ".($row->entregas*2);}else{$row->adm ="";$row->adm1 ="";}
				if($row->gra>0){$row->gra = number_format(($row->gra), 2, '.', ',')."%";$row->gra1 =$row->gra1." / ".(($row->entregas*2)*5);}else{$row->gra ="";$row->gra1 ="";}
				$data[] = $row;	
			endforeach;	*/
			return $data;
		}
		function verTecnicoD(){
			$query=$this->db->query("select NumBio,NomBio from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio");
			return $query->result();			
		}
		function verTecnicoDN(){
			$query=$this->db->query("select NumBio,NomBio from biologo inner join serbio on NumBioS=NumBio where NumBio>2 group by NomBio,NumBio");
			return $query->result();			
		}	
    }
    
?>