<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class General_model extends CI_Model {
        public $estcos="estcos";public $numgrab="numgrab";public $ncicos="ncicos";public $tipcos="tipcos";
        public $feccos="feccos";
        public $tabla="bordo";

        public $idpis="idpis";public $numgra="numgra";
        public $tablaest="siegra";
        public $Fecha="Fecha";public $ngra="ngra";
        public $tabladep="depositos";
        public $tablacli="clientes";

        public $idbio="idbio";public $idpisb="idpisb";public $pesb="pesb";public $fecb="fecb";public $cicb="cicb";
        public $obsb="obsb";public $grab="numgrab";public $incest="incest";
		public $tablabiogra="biogra_21";

		public $tabladie="fac_oad";
		public $tablasue="sueldos";

        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
       
       	function verFechaT(){
			$querycuenta=$this->db->query("select count(*) as sem from biogra_21 group by fecb");
			$tot=0;  
			foreach($querycuenta->result() as $rowcta):
				$tot+=1;
			endforeach;
			return $tot;			
		}
		function verFecha(){
			$querycuenta=$this->db->query("select fecb from biogra_21 group by fecb");
			$tot=0;  //en el ciclo 2014-1 tenia el valor de -1
			foreach($querycuenta->result() as $rowcta):
				$tot+=1;
			endforeach;
			$this->db->select('fecb');
			$this->db->group_by($this->fecb);	
			$this->db->order_by($this->fecb,'DESC');
			$query=$this->db->get($this->tablabiogra, $tot);
			
			return $query->result();			
		}
        function vermeses($cic){
			$this->db->select('month(fecsal) as mes,year(fecsal) as ano');
			$this->db->where('ciclo',$cic);
			$this->db->group_by('month(fecsal)');$this->db->group_by('year(fecsal)');
			$this->db->order_by('month(fecsal)');$this->db->order_by('year(fecsal)');
			$result = $this->db->get('salidas');
			$data = array();
			foreach($result->result() as $row):
				$row->ano=substr($row->ano, 2);
				switch ($row->mes) {
					case '1': $row->nom='Enero '.$row->ano;break;case '2': $row->nom='Febrero '.$row->ano;break;
					case '3': $row->nom='Marzo '.$row->ano;break; case '4': $row->nom='Abril '.$row->ano;break;
					case '5': $row->nom='Mayo '.$row->ano;break; case '6': $row->nom='Junio '.$row->ano;break;
					case '7': $row->nom='Julio '.$row->ano;break; case '8': $row->nom='Agosto '.$row->ano;break;
					case '9': $row->nom='Septiembre '.$row->ano;break; case '10': $row->nom='Octubre '.$row->ano;break;
					case '11': $row->nom='Noviembre '.$row->ano;break; case '12': $row->nom='Diciembre '.$row->ano;break;
				}
				$data[] = $row;	
			endforeach;
			return $data;
		}

        function generalvtatm($cic,$gra,$dia,$g){
        	$cic1='20'.$cic;
        	if($dia==0){
        		$queryvg=$this->db->query("SELECT fecsal,sum(kgssal) as kgs FROM salidas WHERE estsal='1' AND ciclo='$cic1' AND grasal='$gra' GROUP BY fecsal");
        	}else{
        		$queryvg=$this->db->query("SELECT fecsal ,sum(kgssal) as kgs FROM salidas WHERE estsal='1' AND ciclo='$cic1' AND grasal='$gra' AND month(fecsal)='$dia'  GROUP BY fecsal ");
        	}
			$data = array();$fec=new Libreria();
			if($queryvg->num_rows()>0){
			 foreach($queryvg->result() as $row):
			 	if($dia>0) $row->fecsal=$fec->fecha22($row->fecsal);
				$data[] = $row;
			 endforeach;
			}
			return $data;	
		}

        function generalvtat($cic,$gra,$dia,$g){
        	$cic1='20'.$cic;
        	$dia1=date ( 'Y-m-d' , ((strtotime ( '-6 day' , strtotime ( $dia ) ))));
        	$dia2=date ( 'Y-m-d' , ((strtotime ( '-5 day' , strtotime ( $dia ) ))));
        	$dia3=date ( 'Y-m-d' , ((strtotime ( '-4 day' , strtotime ( $dia ) ))));
        	$dia4=date ( 'Y-m-d' , ((strtotime ( '-3 day' , strtotime ( $dia ) ))));
        	$dia5=date ( 'Y-m-d' , ((strtotime ( '-2 day' , strtotime ( $dia ) ))));
        	$dia6=date ( 'Y-m-d' , ((strtotime ( '-1 day' , strtotime ( $dia ) ))));
        	$dia7=$dia;
        	//suma el total de lo vendido
        	$vtatot=0;
        	$queryvt=$this->db->query("SELECT sum(kgssal) as kgssal FROM salidas WHERE estsal='1' AND  fecsal>='$dia1' AND fecsal<='$dia' AND ciclo='$cic1' AND grasal='$gra' ");
        	foreach($queryvt->result() as $rowt):
        		$vtatot=$rowt->kgssal;
        	endforeach;	
			$queryvg=$this->db->query("SELECT idt,nomt FROM salidas INNER JOIN tallas ON talsal=idt WHERE estsal='1' AND fecsal>='$dia1' AND fecsal<='$dia' AND ciclo='$cic1' AND grasal='$gra' GROUP BY idt,nomt ORDER BY sum(kgssal) DESC, gpogrl");
			$data = array();$fec=new Libreria();
			if($queryvg->num_rows()>0){
			 $kgscos=0;$totd1=0;$totd2=0;$totd3=0;$totd4=0;$totd5=0;$totd6=0;$totd7=0;$gtot=0;
			 foreach($queryvg->result() as $row):
			 	$row->d1='';$row->d2='';$row->d3='';$row->d4='';$row->d5='';$row->d6='';$row->d7='';$row->tott='';
			 	$tott=0;$row->tott='';
			 	//buscas los kilos del dia 1
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia1' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d1=number_format($rowd->vta, 0, '.', ',');$totd1+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 2
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia2' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d2=number_format($rowd->vta, 0, '.', ',');$totd2+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 3
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia3' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d3=number_format($rowd->vta, 0, '.', ',');$totd3+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 4
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia4' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d4=number_format($rowd->vta, 0, '.', ',');$totd4+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 5
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia5' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d5=number_format($rowd->vta, 0, '.', ',');$totd5+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 6
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia6' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d6=number_format($rowd->vta, 0, '.', ',');$totd6+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	//buscas los kilos del dia 7
			 	$queryd=$this->db->query("SELECT sum(kgssal) as vta FROM salidas WHERE estsal='1' AND fecsal='$dia7' AND ciclo='$cic1' AND grasal='$gra' and talsal='$row->idt'");
			 	foreach($queryd->result() as $rowd):
			 		if($rowd->vta>0){$row->d7=number_format($rowd->vta, 0, '.', ',');$totd7+=$rowd->vta;$tott+=$rowd->vta;}
			 	endforeach;	
			 	if($tott>0){$row->tott=number_format($tott, 0, '.', ',');
			 		if($g==1){$row->port=number_format(($tott/$vtatot)*100, 2);}
					else{$row->port=($tott/$vtatot)*100;}
			 	}
			 	$gtot+=$tott;
				$data[] = $row;
			 endforeach;
			 if($g==1){
			 $queryt=$this->db->query("SELECT count(*) FROM salidas");
			 foreach($queryt->result() as $rowt):
			 	$rowt->nomt='Total';
			 	$rowt->d1='';$rowt->d2='';$rowt->d3='';$rowt->d4='';$rowt->d5='';$rowt->d6='';$rowt->d7='';$rowt->tott='';
			 	$rowt->port='';
			 	if($totd1>0){$rowt->d1=number_format($totd1, 0, '.', ',');}
			 	if($totd2>0){$rowt->d2=number_format($totd2, 0, '.', ',');}
			 	if($totd3>0){$rowt->d3=number_format($totd3, 0, '.', ',');}
			 	if($totd4>0){$rowt->d4=number_format($totd4, 0, '.', ',');}
			 	if($totd5>0){$rowt->d5=number_format($totd5, 0, '.', ',');}
			 	if($totd6>0){$rowt->d6=number_format($totd6, 0, '.', ',');}
			 	if($totd7>0){$rowt->d7=number_format($totd7, 0, '.', ',');}
			 	if($gtot>0){$rowt->tott=number_format($gtot, 0, '.', ',');}
			 endforeach;	
			}
			$rowt->dt1=$fec->fecha($dia1);$rowt->dt2=$fec->fecha($dia2);$rowt->dt3=$fec->fecha($dia3);
			$rowt->dt4=$fec->fecha($dia4);$rowt->dt5=$fec->fecha($dia5);$rowt->dt6=$fec->fecha($dia6);
			$rowt->dt7=$fec->fecha($dia7);
			$data[] = $rowt;
			} 
			return $data;	
		}
        function generalbord($cic,$gra,$dia){
			$queryvg=$this->db->query("SELECT (nomg) as grad,sum(kgscos) as kgsd from granjas inner join bordo_$cic on numgrab=idg where numgrab='$gra' and feccos='$dia' group by  numgrab");
			$data = array();$orsg=0;
			if($queryvg->num_rows()>0){
			 $kgscos=0;	
			 foreach($queryvg->result() as $row):
			 	if($row->kgsd>0){$kgscos=$row->kgsd;$row->kgsd =number_format($row->kgsd, 0, '.', ',');}
				else{$row->kgsd ='';}
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim");
				$this->db->where($this->tipcos,2); // venta
				$this->db->where($this->numgrab,$gra); //$this->db->where($this->ncicos,$row->cicg); 
				$this->db->where($this->feccos,$dia);
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapvd =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtakgd =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtakgd ='';$row->vtapvd ='';}
					//if($rowf->vtaim>0){$row->vtaiv ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaiv ='';}
				endforeach;
				
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim,sum(prebas)as pb");
				$this->db->where($this->tipcos,1); // maquila
				$this->db->where($this->numgrab,$gra); //$this->db->where($this->ncicos,$row->cicg); 
				$this->db->where($this->feccos,$dia);
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapmd =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtamqd =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtamqd ='';$row->vtapmd ='';}
					//if($rowf->pb>0){$row->vtaim ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaim ='';}
				endforeach;
				
				$this->db->select("sum(kgscos) as vtakg");
				$this->db->where($this->tipcos,3); // donaciones
				$this->db->where($this->numgrab,$gra); //$this->db->where($this->ncicos,$row->cicg);
				$this->db->where($this->feccos,$dia);
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){$row->vtamdd =number_format($rowf->vtakg, 0, '.', ',');}else{$row->vtamdd ='';}
				endforeach;
								
				$data[] = $row;
			 endforeach;
			} 
			return $data;	
		}
		function generalborvd($cic,$gra,$dia){
			$queryvg=$this->db->query("SELECT (nomg) as grabvd,(Razon) as clibvd,sum(kgscos) as kgsbvd, sum(kgscos*(prebas+grscos)) as impbvd from granjas inner join( clientes inner join bordo_$cic on clicos=Numero) on numgrab=idg where tipcos=2 and numgrab='$gra' and feccos='$dia' group by clicos");
			$data = array();
			if($queryvg->num_rows()>0){
			 $kgscos=0;	
			 foreach($queryvg->result() as $row):
			 	if($row->kgsbvd>0){$row->kgsbvd=number_format($row->kgsbvd, 0, '.', ',');}else{$row->kgsbvd='';}	
			 	if($row->impbvd>0){$row->impbvd='$'.number_format($row->impbvd, 2, '.', ',');}else{$row->impbvd='';}	
				$data[] = $row;
			 endforeach;
			} 
			return $data;	
		}
		function generalmaqd($cic,$gra,$dia){
			$cic1='20'.$cic;
			$queryvg=$this->db->query("SELECT kgsp FROM maqpro_$cic WHERE idgp='$gra' and fecr='$dia'");
			$data = array();
			if($queryvg->num_rows()>0){
				$queryvgm=$this->db->query("SELECT (nomg) as grad,(noma) as almd,ide,idae, SUM(kgsp) AS maqpd, avg(prep) as maqcd FROM maqpro_$cic inner join (almacenes INNER JOIN (granjas inner join  maquilas_$cic on idge=idg) ON ida=idae) on ide=idpe WHERE idge='$gra' and fecr='$dia'");
				foreach($queryvgm->result() as $row):
					$row->vta ='20'.$cic;
					$alm=$row->idae;
					$row->maqdm ='';$row->maqfd ='';$row->maqrd ='';$row->maqdd='';
					//$row->maqfd =number_format($row->maqfd, 0, '.', ',');
					$imp=0;$dep=0;$vtap=0;$fco=0;
					if($row->maqpd>0 and $row->maqcd>0){$row->maqid='$'.number_format(($row->maqpd*$row->maqcd)*1.16, 2, '.', ',');}
					else $row->maqid='';
					if($row->maqpd>0){$maq=$row->maqpd;$row->maqpd=number_format(($row->maqpd), 0, '.', ',');}
					else $row->maqpd='';
					if($row->maqcd>0){$row->maqcd='$'.number_format(($row->maqcd), 2, '.', ',');}
					else $row->maqcd='';
					$diapro=0;$cont=0;
					//filtrar todas idpe con las idfe de maquilas con la fecha seleccionada
					//SELECT idpe,(SELECT SUM(ntpf*kgtf) FROM maqfco_21 WHERE idpe=idfe)AS fco,(SELECT fece FROM maquilas_21 WHERE idpe=ide)AS dia FROM maqpro_21 WHERE idgp='4' AND fecr='2021-08-09' GROUP BY idpe
					$queryfco=$this->db->query("SELECT idpe,(SELECT SUM(ntpf*kgtf) FROM maqfco_21 WHERE idpe=idfe)AS fco FROM maqpro_21 WHERE idgp='$gra' AND fecr='$dia' GROUP BY idpe");
					foreach($queryfco->result() as $rowf):		
						$fco+=$rowf->fco;
					endforeach;
					if($rowf->fco>0){$row->maqfd=number_format(($fco), 0, '.', ',');}
					if($fco>0 and $maq>0) $row->maqrd=number_format(($maq/$fco)*100, 2); else $row->maqrd='';
					$data[] = $row;
				endforeach; 
			}
			return $data;	
		}
        function generalmaqd1($cic,$gra,$dia){
			$cic1='20'.$cic;
			$queryvg=$this->db->query("SELECT kgscos FROM maquilas_$cic WHERE idge='$gra' and fece='$dia'");
			$data = array();
			if($queryvg->num_rows()>0){
				$queryvg=$this->db->query("SELECT (nomg) as grad,(noma) as almd,idae, SUM(ntpf*kgtf) AS fco FROM maqfco_$cic inner join (almacenes INNER JOIN (granjas inner join  maquilas_$cic on idge=idg) ON ida=idae) on ide=idfe WHERE idge='$gra' and fece='$dia'");
				foreach($queryvg->result() as $row):
					$row->vta ='20'.$cic;
					$alm=$row->idae;
					$row->maqfd =number_format($row->fco, 0, '.', ',');$imp=0;$dep=0;$vtap=0;$maq=0;
					
					//SELECT SUM(kgsp) AS pro FROM maqpro_21 INNER JOIN maquilas_21 ON ide=idpe WHERE idgp=4 AND estp=1
					$querypro=$this->db->query("SELECT SUM(kgsp) AS maqpd, avg(prep) as maqcd FROM maqpro_$cic INNER JOIN maquilas_$cic ON ide=idpe WHERE idgp='$gra' AND estp=1 and idap='$alm' and fecp='$dia'");
					foreach($querypro->result() as $rowp):			
						if($rowp->maqpd>0){$maq=$rowp->maqpd;$row->maqpd=number_format(($rowp->maqpd), 0, '.', ',');}
						else $row->maqpd='';
						if($rowp->maqcd>0){$row->maqcd='$'.number_format(($rowp->maqcd), 2, '.', ',');}
						else $row->maqcd='';
						if($rowp->maqpd>0 and $rowp->maqcd>0){$row->maqid='$'.number_format(($rowp->maqpd*$rowp->maqcd)*1.16, 2, '.', ',');}
						else $row->maqid='';
					endforeach;
					if($row->fco>0 and $maq>0) $row->maqrd=number_format(($maq/$row->fco)*100, 2); else $row->maqrd='';		
					$data[] = $row;
				endforeach; 
			}
			return $data;	
		}
        
        function generalprod($cic,$gra,$dia){
			$data = array();
			//SELECT (ida) AS ida,(noma) AS porad,(Razon)AS clipd,SUM(kgssal) AS prokd,SUM(kgssal*prekgs) AS proid FROM almacenes INNER JOIN (clientes INNER JOIN salidas ON clisal=Numero) ON ida=almsal WHERE grasal='4' AND  fecsal='2021-08-05' AND estsal=1 GROUP BY noma,Razon ORDER BY noma,Razon
			$querypro=$this->db->query("SELECT (ida) AS ida,(noma) AS proad,(Numero)as cliente,(Razon)AS clipd,SUM(kgssal) AS prokd,SUM(kgssal*prekgs) AS proid FROM almacenes INNER JOIN (clientes INNER JOIN salidas ON clisal=Numero) ON ida=almsal WHERE grasal='$gra' AND  fecsal='$dia' AND estsal=1 GROUP BY noma,Razon ORDER BY noma,Razon");
			$alm=0;$kg=0;$mn=0;$tk=0;$tp=0;$tkg=0;$tpg=0;$entro=0;$cont=0;
			foreach($querypro->result() as $row):
				$kg=$row->prokd;$tkg+=$row->prokd;
				$mn=$row->proid;$tpg+=$row->proid;
				$row->prosg='';$row->propr='';
				if($entro==1 and $alm!=$row->ida){
					$this->db->select('count(*)');
					$resultt = $this->db->get($this->tabladep);
					foreach($resultt->result() as $rowt):
						$rowt->proad='';$rowt->clipd='Total';$rowt->prokd='';$rowt->proid= '';
						$rowt->prosg='';$rowt->propr='';
						if($tk>0){$rowt->prokd = number_format($tk, 0, '.', ',');}
						if($tp>0){$rowt->proid = "$ ".number_format($tp, 2, '.', ',');}			 
						$data[] = $rowt;	
					endforeach;
					$tk=0;$tp=0;$entro=1;
				}
				if($alm!=$row->ida){$alm=$row->ida;$cont+=1;
				}else{$row->proad='';}
				if($row->proid>0){$row->proid="$ ".number_format($row->proid, 2, '.', ',');} else{$row->proid='';} 	
				if($row->prokd>0){$row->prokd = number_format($row->prokd, 0, '.', ',');}
				$entro=1;$tk+=$kg;$tp+=$mn;$cliente=$row->cliente;
				//el saldo del cliente
				$queryvg=$this->db->query('SELECT Numero,idg,sigg,nomg,Razon,sum(kgssal) as kgsvta, sum(kgssal*prekgs) as impvta, (select sum(Pesos) from depositos where ngra=idg and NRC=clisal and Des=0 and Car=0 and bormaq=2 and Aplicar=20'.$cic.') as depvta,sum(kgssal*usdkgs) as impvtau,(select sum(ImporteD) from depositos where ngra=idg and NRC='.$cliente.' and Des=0 and Aplicar=20'.$cic.') as depvtau,(select sum(ImporteD) from depositos where ngra=idg and NRC='.$cliente.' and Des=-1 and Aplicar=20'.$cic.') as desvtau,(select sum(Pesos) from depositos where ngra=idg and  NRC='.$cliente.' and bormaq=2 and Des=-1 and Aplicar=20'.$cic.') as desvta,(select sum(Pesos) from depositos where ngra=idg and  NRC='.$cliente.' and bormaq=2 and Car=-1 and Aplicar=20'.$cic.') as carvta from granjas inner join( clientes inner join salidas on clisal=Numero) on grasal=idg where ciclo=20'.$cic.' and estsal=1 and grasal='.$gra.' and clisal='.$cliente);
				foreach($queryvg->result() as $rows):
					if(($rows->impvta+$rows->carvta)>0) $row->propr=number_format(((($rows->impvta+$rows->carvta)-($rows->depvta+$rows->desvta))/($rows->impvta+$rows->carvta)*100), 2, '.', ','); 
					if(($rows->impvta)-($rows->depvta+$rows->desvta)!=0) $row->prosg='$'.number_format((($rows->impvta+$rows->carvta)-($rows->depvta+$rows->desvta)), 2, '.', ','); else $row->prosg='';

				endforeach;	

				$data[] = $row;
			endforeach; 
			if($entro==1 && $cont>1){
				$this->db->select('count(*)');
				$resultt = $this->db->get($this->tabladep);
				foreach($resultt->result() as $rowt):
					$rowt->proad='';$rowt->clipd='Total';$rowt->prokd='';$rowt->proid= '';$rowt->prosg='';$rowt->propr='';
					if($tk>0){$rowt->prokd = number_format($tk, 0, '.', ',');}
					if($tp>0){$rowt->proid = "$ ".number_format($tp, 2, '.', ',');}	 	
					$data[] = $rowt;	
				endforeach;
			}
			if($tkg>0 || $tpg>0){
				$this->db->select('count(*)');
				$resultt = $this->db->get($this->tabladep);
				foreach($resultt->result() as $rowt):
					$rowt->proad='Total';$rowt->clipd='';$rowt->prokd='';$rowt->proid= '';$rowt->prosg='';$rowt->propr='';
					if($tkg>0){$rowt->prokd = number_format($tkg, 0, '.', ',');}			 
					if($tpg>0){$rowt->proid = "$ ".number_format($tpg, 2, '.', ',');} 	
					$data[] = $rowt;	
				endforeach;
			}
			return $data;	
		}
        function generaldepd($gra,$dia){
			$data = array();
			$this->db->select('sum(ImporteD) as ImporteD,sum(Pesos) as Pesos,Razon,bormaq');
			$this->db->join('clientes', 'clientes.Numero=depositos.NRC');			
			$this->db->where('Des',0);$this->db->where('Car',0);
			$this->db->where($this->ngra,$gra);$this->db->where($this->Fecha,$dia);
			$this->db->group_by('NRC');$this->db->group_by('Razon');$this->db->group_by('bormaq');
			$this->db->order_by('bormaq');$this->db->order_by('Razon'); 
			$result = $this->db->get($this->tabladep);
			$bormaq='';$tp=0;$td=0;$tpg=0;$tdg=0;$entro=0;$cont=0;
			foreach($result->result() as $row):
				$dolar=$row->ImporteD;$tdg+=$row->ImporteD;
				$mn=$row->Pesos;$tpg+=$row->Pesos;
				if($entro==1 and $bormaq!=$row->bormaq){
					$this->db->select('count(*)');
					$resultt = $this->db->get($this->tabladep);
					foreach($resultt->result() as $rowt):
						$rowt->destd='';$rowt->Razon='Total';$rowt->Pesos='';$rowt->Dolar= '';
						if($tp>0){$rowt->Pesos = "$ ".number_format($tp, 2, '.', ',');}			 
						if($td>0){$rowt->Dolar = "$ ".number_format($td, 2, '.', ',');} 	
						$data[] = $rowt;	
					endforeach;
					$tp=0;$td=0;$entro=1;
				}
				if($bormaq!=$row->bormaq){
					if($row->bormaq==1){$row->destd='Bordo';$cont+=1;} else {$row->destd='Maquila';$cont+=1;}
					$bormaq=$row->bormaq;
				}else{$row->destd='';}
				if($row->Pesos>0){$row->Pesos =  "$ ".number_format($mn, 2, '.', ',');}else{$row->Pesos ="";}				 
				if($row->ImporteD>0){$row->Dolar = "$ ".number_format($dolar, 2, '.', ',');}else{$row->Dolar ="";} 	
				$entro=1;$tp+=$mn;$td+=$dolar;
				$data[] = $row;
			endforeach; 
			if($entro==1 && $cont==2){
				$this->db->select('count(*)');
				$resultt = $this->db->get($this->tabladep);
				foreach($resultt->result() as $rowt):
					$rowt->destd='';$rowt->Razon='Total';$rowt->Pesos='';$rowt->Dolar= '';
					if($tp>0){$rowt->Pesos = "$ ".number_format($tp, 2, '.', ',');}			 
					if($td>0){$rowt->Dolar = "$ ".number_format($td, 2, '.', ',');} 	
					$data[] = $rowt;	
				endforeach;
			}
			if($tpg>0 || $tdg>0){
				$this->db->select('count(*)');
				$resultt = $this->db->get($this->tabladep);
				foreach($resultt->result() as $rowt):
					$rowt->destd='Total';$rowt->Razon='';$rowt->Pesos='';$rowt->Dolar= '';
					if($tpg>0){$rowt->Pesos = "$ ".number_format($tpg, 2, '.', ',');}			 
					if($tdg>0){$rowt->Dolar = "$ ".number_format($tdg, 2, '.', ',');} 	
					$data[] = $rowt;	
				endforeach;
			}
			return $data;	
		}
		function generalg($gra,$cic){
			$data = array();$cic1='20'.$cic;
			$this->db->select("count(*)");
			$this->db->where($this->numgra,$gra); 
			$result = $this->db->get($this->tablaest.'_'.$cic);
			foreach($result->result() as $row):
				$row->gto ='20'.$cic;$impali=0;$row->porali='';$impmaq=0;$row->impmaq='';$row->pormaq='';
				$impcom=0;$row->ltscom='';
				$impsue=0;$row->impsue='';$row->porsue='';$impotr=0;$row->impotr='';$row->porotr='';
				$impcom=0;$row->impcom='';$row->porcom='';
				$totalgto=0;
				//Alimento
				//SELECT SUM(tonag) AS kgsali,SUM(imppag) AS impali FROM fac_oa_21 WHERE idgag=4
				$queryali=$this->db->query("SELECT SUM(tonag) AS kgsali,SUM(imppag) AS impali,sum((subfle*1.16)-(subfle*.04))as flete FROM fac_oa_$cic WHERE idgag=$gra");
				foreach($queryali->result() as $rowa):			
					if($rowa->kgsali>0){$row->kgsali=number_format(($rowa->kgsali), 3, '.', ',').' ton';} else $row->kgsali='';
					if($rowa->impali>0){$totalgto+=$rowa->impali+$rowa->flete;$impali=$rowa->impali+$rowa->flete;
						$row->aligrafica=number_format(($rowa->impali+$rowa->flete), 2);$row->aligrafica=str_replace(',','',$row->aligrafica);
						$row->impali='$ '.number_format(($rowa->impali+$rowa->flete), 2, '.', ',');}
					else{ $row->impali='';$row->aligrafica=0;}
				endforeach;
				//Maquila
				$querymaq=$this->db->query("SELECT (SUM(kgsp*prep)/SUM(kgsp)) as premaq,(SUM(kgsp*prep)*1.16) AS impmaq FROM maqpro_$cic INNER JOIN maquilas_$cic ON ide=idpe WHERE idgp=$gra AND estp=1");
				foreach($querymaq->result() as $rowm):			
					if($rowm->impmaq>0){$totalgto+=$rowm->impmaq;$impmaq=$rowm->impmaq;
						$row->premaq='$ '.number_format(($rowm->premaq), 2, '.', ',');
						$row->maqgrafica=number_format(($rowm->impmaq), 2);$row->maqgrafica=str_replace(',','',$row->maqgrafica);
						$row->impmaq='$ '.number_format(($rowm->impmaq), 2, '.', ',');}
					else{$row->premaq='';$row->impmaq='';$row->maqgrafica=0;}
				endforeach;
				//Combustible
				$totalidie=0;
				$this->db->where('cicdie',$cic);	
				$result1 = $this->db->get($this->tabladie);
				$cosdie=0;$totalidie=0;$totaliton=0;
				foreach($result1->result() as $row1):
					if($row1->tipdie==1){$impcom+=$row1->candie*$row1->predie;$totalidie+=$row1->candie;}
					else {$impcom-=$row1->candie*$row1->predie;}
					$row->comgrafica=number_format(($impcom), 2);$row->comgrafica=str_replace(',','',$row->comgrafica);
				endforeach;
				$totalgto+=$impcom;
				if($impcom>0){ $row->impcom='$ '.number_format(($impcom), 2, '.', ',');
				$row->ltscom=number_format(($totalidie), 0, '.', ',').' lts';}
				else{$row->comgrafica=0;}
				//Sueldos
				$impsue=0;
				$this->db->select('sum(impsem) as impsem');
				$this->db->where('cicsem',$cic);	
				$resulta = $this->db->get($this->tablasue);
				foreach ($resulta->result() as $rowsue):				
					$impsue=$rowsue->impsem;
				endforeach;
				if($impsue>0){ $row->impsue='$ '.number_format(($impsue), 2, '.', ',');
					$row->suegrafica=number_format(($impsue), 2);$row->suegrafica=str_replace(',','',$row->suegrafica);
				}else{$row->suegrafica=0;}
				$totalgto+=$impsue;

				if($totalgto>0){
					if($impali>0) $row->porali=number_format(($impali/$totalgto)*100, 1).' %';
					if($impmaq>0) $row->pormaq=number_format(($impmaq/$totalgto)*100, 1).' %';
					if($impcom>0) $row->porcom=number_format(($impcom/$totalgto)*100, 1).' %';
					if($impsue>0) $row->porsue=number_format(($impsue/$totalgto)*100, 1).' %';
					$row->tot='$ '.number_format(($totalgto), 2, '.', ',');
				} else {$row->tot='';}
				$data[] = $row;
			endforeach; 
			return $data;	
		}
		function generalv($gra,$cic,$tipo,$dia){
			$data = array();$cic1='20'.$cic;
			//$time = time();$dia=date("Y-m-d ", $time);
			if($tipo==2){$bormaq=1;
				$queryvg=$this->db->query("SELECT sum(kgscos) as kgs, sum(kgscos*(prebas+grscos)) as imp,(select sum(Pesos) from depositos where ngra=$gra and bormaq=$bormaq and aplicar=$cic1) as dep from bordo_$cic where numgrab='$gra' and tipcos='$tipo'");
			}else{ $bormaq=2;
				$queryvg=$this->db->query("SELECT SUM(ntpf*kgtf) AS fco FROM maqfco_$cic INNER JOIN maquilas_$cic ON ide=idfe WHERE idge='$gra'");
			}
			foreach($queryvg->result() as $row):
				$row->vta ='20'.$cic;
				if($tipo==2){
					if($row->kgs>0){
						if($row->imp>0)$row->pre ='$ '.number_format($row->imp/$row->kgs, 2, '.', ',');
						$row->kgs =number_format($row->kgs, 0, '.', ',');
					}else{$row->kgs ='';$row->pre ='';}
					if($row->imp-$row->dep!=0){$row->sal='$ '.number_format(($row->imp-$row->dep), 2, '.', ',');
					if ($row->imp>0) $row->por=number_format((($row->imp-$row->dep)/$row->imp)*100, 2); else $row->por='';
					} else {$row->sal='';$row->por='';}
					if($row->imp>0){$row->impgrafica=number_format($row->imp, 2);$row->impgrafica=str_replace(',','',$row->impgrafica);$row->imp ='$ '.number_format($row->imp, 2, '.', ',');}else{$row->imp ='';$row->impgrafica=0;}
					if($row->dep>0) $row->dep='$ '.number_format(($row->dep), 2, '.', ','); else $row->dep=''; 
				}
				if($tipo==1){
					$row->kgs =number_format($row->fco, 0, '.', ',');$imp=0;$dep=0;$vtap=0;$maq=0;
					//SELECT SUM(kgsp) AS pro FROM maqpro_21 INNER JOIN maquilas_21 ON ide=idpe WHERE idgp=4 AND estp=1
					$querypro=$this->db->query("SELECT SUM(kgsp) AS pro FROM maqpro_$cic INNER JOIN maquilas_$cic ON ide=idpe WHERE idgp=$gra AND estp=1");
					foreach($querypro->result() as $rowp):			
						if($rowp->pro>0){$maq=$rowp->pro;$row->maq=number_format(($rowp->pro), 0, '.', ',');}
						else $row->maq='';
					endforeach;
					if($row->fco>0) $row->ren=number_format(($maq/$row->fco)*100, 2); else $row->ren='';
					//SELECT SUM(kgssal) AS vta,SUM(kgssal*prekgs) AS imp FROM salidas WHERE grasal=4 AND ciclo=2021 AND estsal=1
					$row->idv ='';$row->isv ='';
					$queryvta=$this->db->query("SELECT SUM(kgssal) AS vta,SUM(kgssal*prekgs) AS imp FROM salidas WHERE grasal=$gra AND ciclo=$cic1 AND estsal=1");
					foreach($queryvta->result() as $rowv):			
						if($rowv->vta>0){$vtap=$rowv->vta;$row->vtap=number_format(($rowv->vta), 0, '.', ',');} 
						else $row->vtap='';
						if($rowv->imp>0){ $row->pre ='$ '.number_format($rowv->imp/$rowv->vta, 2, '.', ',');
							$row->impgrafica=number_format($rowv->imp, 2);$row->impgrafica=str_replace(',','',$row->impgrafica);
							$imp=$rowv->imp;$row->imp='$ '.number_format(($rowv->imp), 2, '.', ',');}
						else{$row->imp='';$row->pre='';$row->impgrafica=0;}
					endforeach;
					if($maq>0) $row->porv=number_format(($vtap/$maq)*100, 2); else $row->porv='';
					//ventas del dia
					//SELECT SUM(kgssal) AS kgsvta FROM salidas WHERE ciclo=2021 AND grasal=4 AND estsal=1 AND fecsal='2021-08-03'
					$queryvtad=$this->db->query("SELECT SUM(kgssal) AS vtadia FROM salidas WHERE grasal=$gra AND ciclo=$cic1 AND estsal=1 AND fecsal='$dia'");
					foreach($queryvtad->result() as $rowvd):			
						if($rowvd->vtadia>0 and $vtap>0){$row->idv=number_format(($rowvd->vtadia/$vtap)*100, 2);
					} 
					endforeach;
					//depositos
					$querydep=$this->db->query("select sum(Pesos) as dep from depositos where ngra=$gra and bormaq=$bormaq and aplicar=$cic1");
					foreach($querydep->result() as $rowd):			
						if($rowd->dep>0){$dep=$rowd->dep;$row->dep='$ '.number_format(($rowd->dep), 2, '.', ',');} else $row->dep='';
					endforeach;
					if(($imp-$dep)!=0) $row->sal='$ '.number_format(($imp-$dep), 2, '.', ','); else $row->sal='';
					if($imp>0) $row->por=number_format((($imp-$dep)/$imp)*100, 2); else $row->por='';
				}
				$data[] = $row;
			endforeach; 
			return $data;	
		}	
		function generalb($gra,$cic){
			$queryvg=$this->db->query("SELECT cicg,sum(hasg) as hasg,sum(orgg) as orsg from siegra_$cic where numgra=$gra group by cicg");
			$data = array();$orsg=0;
			if($queryvg->num_rows()>0){
			 $kgscos=0;	
			 foreach($queryvg->result() as $row):
			 	$orsg=$row->orsg;
			 	$row->hasg =number_format($row->hasg,3, '.', ',');
			 	$row->orsg =number_format($row->orsg,3, '.', ',');
			 	$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta,tipcos");
				$this->db->join($this->tablaest.'_'.$cic, $this->idpis.'='.$this->estcos,'inner'); 
				$this->db->where($this->numgrab,$gra); $this->db->where($this->ncicos,$row->cicg); 
				$result1 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result1->result() as $row1):
					if($row1->kgs>0){$row->kgsg =number_format($row1->kgs, 0, '.', ',');$kgscos=$row1->kgs;}
					else{$row->kgsg ='';}
					if($row1->pp>0){$row->ppg=number_format($row1->pp, 2, '.', ',');}else{$row->ppg='';}	
					if($row1->pp>0) $row->orcg=number_format(($row1->kgs*1000)/$row1->pp, 0, '.', ','); else $row->orcg='';
					if($orsg>0 and $row1->orgspre>0) $row->sobg=number_format(($row1->orgspre/$orsg)*100, 2, '.', ','); else $row->sobg='';
				endforeach;
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim");
				$this->db->where($this->tipcos,2); // venta
				$this->db->where($this->numgrab,$gra); $this->db->where($this->ncicos,$row->cicg); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapv =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtakg =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtakg ='';$row->vtapv ='';}
					if($rowf->vtaim>0){$row->vtaiv ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaiv ='';}
				endforeach;
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim,sum(prebas)as pb");
				$this->db->where($this->tipcos,1); // maquila
				$this->db->where($this->numgrab,$gra); $this->db->where($this->ncicos,$row->cicg); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapm =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtamq =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtamq ='';$row->vtapm ='';}
					if($rowf->pb>0){$row->vtaim ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaim ='';}
				endforeach;
				$this->db->select("sum(kgscos) as vtakg");
				$this->db->where($this->tipcos,3); // donaciones
				$this->db->where($this->numgrab,$gra); $this->db->where($this->ncicos,$row->cicg);
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){$row->vtamd =number_format($rowf->vtakg, 0, '.', ',');}else{$row->vtamd ='';}
				endforeach;
				$data[] = $row;
			 endforeach;
			 //total
			 $kgscos=0;
			 $queryvg=$this->db->query("SELECT sum(hasg) as hasg,sum(orgg) as orsg from siegra_$cic where numgra=$gra");
			 foreach($queryvg->result() as $row):
			 	$row->cicg ='Total';	$orsg=$row->orsg;
				$row->hasg =number_format($row->hasg,3, '.', ',');
			 	$row->orsg =number_format($row->orsg,3, '.', ',');
			 	$this->db->select("(sum(kgscos)/avg(grscos) )as orgspre,sum(kgscos) as kgs, avg(grscos) as pp,(grscos+prebas) as pre,sum(kgscos*(grscos+prebas)) as vta");
				$this->db->where($this->numgrab,$gra);  
				$result1 = $this->db->get($this->tabla.'_'.$cic);
				foreach($result1->result() as $row1):
					if($row1->kgs>0){$row->kgsg =number_format($row1->kgs, 0, '.', ',');$kgscos=$row1->kgs;}else{$row->kgsg ='';}
					if($row1->pp>0){$row->ppg =number_format($row1->pp, 2, '.', ',');}else{$row->ppg ='';}	
					$row->orcg =number_format(($row1->kgs*1000)/$row1->pp, 0, '.', ',');
					if($orsg>0 and $row1->orgspre>0) $row->sobg =number_format(($row1->orgspre/$orsg)*100, 2, '.', ','); else $row->sobg ='';
				endforeach;
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim");
				$this->db->where($this->tipcos,2); // venta
				$this->db->where($this->numgrab,$gra); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapv =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtakg =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtakg ='';$row->vtapv ='';}
					if($rowf->vtaim>0){$row->vtaiv ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaiv ='';}
				endforeach;
				$this->db->select("sum(kgscos) as vtakg,sum(kgscos*(grscos+prebas)) as vtaim,sum(prebas)as pb");
				$this->db->where($this->tipcos,1); // maquila
				$this->db->where($this->numgrab,$gra); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){
						if($kgscos>0) $row->vtapm =number_format(($rowf->vtakg/$kgscos)*100, 2); 
						$row->vtamq =number_format($rowf->vtakg, 0, '.', ',');
					}else{$row->vtamq ='';$row->vtapm ='';}
					if($rowf->pb>0){$row->vtaim ='$'.number_format($rowf->vtaim, 2, '.', ',');}else{$row->vtaim ='';}
				endforeach;
				$this->db->select("sum(kgscos) as vtakg");
				$this->db->where($this->tipcos,3); // donaciones
				$this->db->where($this->numgrab,$gra); 
				$resultf = $this->db->get($this->tabla.'_'.$cic);
				foreach($resultf->result() as $rowf):
					if($rowf->vtakg>0){$row->vtamd =number_format($rowf->vtakg, 0, '.', ',');}else{$row->vtamd ='';}
				endforeach;	
			 	$data[] = $row;
			 endforeach;
			} 
			return $data;
		}	
    }
?>