<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Kpiadm_model extends CI_Model {
       	
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
			function kpiadm($filter){
				//select nad,nom from analisisdepto inner join analisisresdiadepto on nadr=nad where month(fech)=3 and ndep=17 group by nad,nom
				$this->db->select('nad,nom');
				$this->db->join('analisisresdiadepto', 'nadr=nad', 'inner');	
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->group_by('nad');$this->db->group_by('nom');
				$result = $this->db->get('analisisdepto');
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				$data = array();$datose=array();$datosd=array();$datosr=array();$datosv=array();$datosvt=array();$nadr=0;
				if($cantidadre>0){
				$ini=1; while($ini<=31){$datosv [$ini]='';$datosvt [$ini]='';$ini+=1;}	
				foreach ($result->result() as $row):
					$nadr=$row->nad;
					//select day(fech) as dia, estr from analisisdepto inner join analisisresdiadepto on nadr=nad where nadr=6 and month(fech)=3 and ndep=17 group by dia
					$this->db->select('day(fech) as dia, estr,nadr');
					$this->db->join('analisisresdiadepto', 'nadr=nad', 'inner');	
					$this->db->where('nadr =',$nadr);
					if($filter['where']!='') {$this->db->where($filter['where']);}
					$this->db->group_by('day(fech)');$this->db->group_by('estr');
					$resulta = $this->db->get('analisisdepto');
					//$cantidadre=$resulta->num_rows();
					$ini=1; $cont=0;
					while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					foreach ($resulta->result() as $rowa):
						$val=0;
						$datosd [$rowa->dia]=$rowa->dia; 
						$datosr [$rowa->dia]=$rowa->estr; 
						if($rowa->estr>0) $datosvt [$rowa->dia]+=1;
						if($rowa->estr<0 and $rowa->nadr==20) $datosvt [$rowa->dia]+=1;
						$val=$rowa->estr;	
						if($rowa->nadr==20 ){
							if($rowa->estr<=5.0) $val=100;
							elseif(($rowa->estr>=5.01 and $rowa->estr<=20.00) ) $val=60;
								elseif($rowa->estr>=20.01) $val=20;
						}						
						$datosv [$rowa->dia]+=$val;
						
					endforeach;
					
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$data[] = $row;	
				endforeach;
				//kpi general departamento
				$this->db->select('max(Numero)');
				$result = $this->db->get('clientes');
				foreach ($result->result() as $row):
					$row->nom ="KPI";
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ','); } else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 =number_format(($datosv [$ini]/$datosvt [$ini]), 1, '.', ',');} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
				endforeach;
				$data[] = $row;	
				}
				return $data;
			}
				
			
		function verDepto(){			
			$this->db->order_by('NomDep');			
			$query=$this->db->get('departamento');
			return $query->result();			
		}
		
	//Gral Mes combustible
		function getcom($filter,$cic,$mes){
			$ini=1;
			$data = array();
			//mes
			
			$fecha='2017-'.$mes.'-01';
			while($ini<=31){
				$fr1 [$ini]=$fecha;
				$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$fecha = date ( 'Y-m-d' , $fecha ); 
				$ini+=1;				
			}
			$ini=1;
			// agarrar el promedio mensual actaul del chofer
			//SELECT NumCho,NomCho,avg(descom) as tot from chofer inner join r18 on NumCho=NumChoR where month(FechaR) =04 and descom !=0 and Estatus <=1 group by NomCho
			$this->db->select('NumCho,NomCho,avg(descom) as tot');
			$this->db->join('chofer', 'NumCho=NumChoR', 'inner');
			$this->db->where('month(FechaR) =',$mes);
			$this->db->where('descom !=',0);
			$this->db->where('descom !=',-100);
			$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
			$this->db->group_by('NomCho');$this->db->group_by('NumCho');
			$queryc=$this->db->get('r18');
			$cho=1;$cont=0;
			foreach ($queryc->result() as $row):
				$cho=$row->NumCho;
				$datosChp [$cho]=$row->NumCho; $datoslugc [$cho]=$cho; $datoslugp [$cho]=number_format($row->tot, 1, '.', ',');
				//$cho+=1;
				$cont+=1;
			endforeach;
			//agarrar los numeros de choferes activos
			$this->db->select('NumCho,NomCho,Alias'); $this->db->where('alias !=',''); $this->db->where('activo =',0); $this->db->where('NumCho !=',1);
			//$this->db->where('numbio !=',63);
			$this->db->order_by('nomcho');$this->db->order_by('NomCho');
			$query=$this->db->get('chofer');
			$bio=1; $cont=0;
			foreach ($query->result() as $row):
				$datosBi [$bio]=$row->NumCho; $datoslug [$bio]=$row->Alias;
				$bio+=1;$cont+=1;
			endforeach;
			$nbio=$bio;//$nb=29;$nbio=29;
			$nb=1;			
			while($nb<$nbio){
			    $cho=$datosBi [$nb];$ini=1;$entro2=0;
				$this->db->select('Alias,day(FechaR) as dia,RemisionR,Loc,Edo,NumRegR,descom');
				$this->db->join('chofer', 'NumCho=NumChoR', 'inner');	$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				//$this->db->join('serbio', 'NumRemS = NumRegR','left');
				//$this->db->group_by('NumRemS');
				$this->db->where('descom !=',0); $this->db->where('descom !=',-100);
				$this->db->where('Estatus <',2); $this->db->where('NumCho =',$cho);
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->order_by('NomCho');$this->db->order_by('dia','ASC');	$this->db->order_by('RemisionR', 'ASC');
				$result = $this->db->get($cic);
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				if($cantidadre>=1){
					
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;$diac=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$datosd [$dia]=$row->dia;$rem=$row->NumRegR;
						$datosr [$dia]=$row->descom;
						if($cho==$datosChp [$cho]){$row->por=$datoslugp [$cho];}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tec=$datoslug [$nb];$data[] = $row;
				}
				else{
					$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec=$datoslug [$nb];
					$row->d1 ="";$row->d2 ="";$row->d3 ="";$row->d4 ="";$row->d5 ="";$row->d6 ="";$row->d7 ="";$row->d8 ="";$row->d9 ="";$row->d10 ="";
					$row->d11 ="";$row->d12 ="";$row->d13 ="";$row->d14 ="";$row->d15 ="";$row->d16 ="";$row->d17 ="";$row->d18 ="";$row->d19 ="";$row->d20 ="";
					$row->d21 ="";$row->d22 ="";$row->d23 ="";$row->d24 ="";$row->d25 ="";$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";
					$row->d31 ="";
					$data[] = $row;
					endforeach;	
				}
			$nb+=1;
			}
			//general porcentaje dia
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($cic);
			foreach ($result->result() as $row):
				$row->tec ="KPI";$row->por ="";
				// agarrar el promedio mensual actaul
				//SELECT NumCho,NomCho,avg(descom) as tot from chofer inner join r18 on NumCho=NumChoR where month(FechaR) =04 and descom !=0 and Estatus <=1 group by NomCho
				$this->db->select('avg(descom) as totc');
				$this->db->join('chofer', 'NumCho=NumChoR', 'inner');
				$this->db->where('month(FechaR) =',$mes);
				$this->db->where('descom !=',0);
				$this->db->where('descom !=',-100);
				$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
				//$this->db->group_by('NomCho');$this->db->group_by('NumCho');
				$querytc=$this->db->get('r18');
				foreach ($querytc->result() as $rowt):
					$row->por =number_format($rowt->totc, 1, '.', ',');
				endforeach;
				$ini=1;$tc=0;
				while($ini<=31){
					//SELECT avg(descom) from r18 where month(FechaR)=4 and day(FechaR)=3 and descom!=0
					$this->db->select('avg(descom) as tot');
					$this->db->where('month(FechaR) =',$mes);
					$this->db->where('day(FechaR) =',$ini);
					$this->db->where('descom !=',0);
					$this->db->where('descom !=',-100);
					$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
					$queryt=$this->db->get('r18');
					foreach($queryt->result() as $rowcs):
						$d1=$rowcs->tot; 
						 switch($ini){
						 	case 1: if($d1!=0){$row->d1 =number_format((($d1)), 1, '.', ',');} else $row->d1=''; break;
							case 2: if($d1!=0){$row->d2 =number_format((($d1)), 1, '.', ',');} else $row->d2=''; break;
							case 3: if($d1!=0){$row->d3 =number_format((($d1)), 1, '.', ',');} else $row->d3=''; break;
							case 4: if($d1!=0){$row->d4 =number_format((($d1)), 1, '.', ',');} else $row->d4=''; break;
							case 5: if($d1!=0){$row->d5 =number_format((($d1)), 1, '.', ',');} else $row->d5=''; break;
							case 6: if($d1!=0){$row->d6 =number_format((($d1)), 1, '.', ',');} else $row->d6=''; break;
							case 7: if($d1!=0){$row->d7 =number_format((($d1)), 1, '.', ',');} else $row->d7=''; break;
							case 8: if($d1!=0){$row->d8 =number_format((($d1)), 1, '.', ',');} else $row->d8=''; break;
							case 9: if($d1!=0){$row->d9 =number_format((($d1)), 1, '.', ',');} else $row->d9=''; break;
							case 10: if($d1!=0){$row->d10 =number_format((($d1)), 1, '.', ',');} else $row->d10=''; break;
							case 11: if($d1!=0){$row->d11 =number_format((($d1)), 1, '.', ',');} else $row->d11=''; break;
							case 12: if($d1!=0){$row->d12 =number_format((($d1)), 1, '.', ',');} else $row->d12=''; break;
							case 13: if($d1!=0){$row->d13 =number_format((($d1)), 1, '.', ',');} else $row->d13=''; break;
							case 14: if($d1!=0){$row->d14 =number_format((($d1)), 1, '.', ',');} else $row->d14=''; break;
							case 15: if($d1!=0){$row->d15 =number_format((($d1)), 1, '.', ',');} else $row->d15=''; break;
							case 16: if($d1!=0){$row->d16 =number_format((($d1)), 1, '.', ',');} else $row->d16=''; break;
							case 17: if($d1!=0){$row->d17 =number_format((($d1)), 1, '.', ',');} else $row->d17=''; break;
							case 18: if($d1!=0){$row->d18 =number_format((($d1)), 1, '.', ',');} else $row->d18=''; break;
							case 19: if($d1!=0){$row->d19 =number_format((($d1)), 1, '.', ',');} else $row->d19=''; break;
							case 20: if($d1!=0){$row->d20 =number_format((($d1)), 1, '.', ',');} else $row->d20=''; break;
							case 21: if($d1!=0){$row->d21 =number_format((($d1)), 1, '.', ',');} else $row->d21=''; break;
							case 22: if($d1!=0){$row->d22 =number_format((($d1)), 1, '.', ',');} else $row->d22=''; break;
							case 23: if($d1!=0){$row->d23 =number_format((($d1)), 1, '.', ',');} else $row->d23=''; break;
							case 24: if($d1!=0){$row->d24 =number_format((($d1)), 1, '.', ',');} else $row->d24=''; break;
							case 25: if($d1!=0){$row->d25 =number_format((($d1)), 1, '.', ',');} else $row->d25=''; break;
							case 26: if($d1!=0){$row->d26 =number_format((($d1)), 1, '.', ',');} else $row->d26=''; break;
							case 27: if($d1!=0){$row->d27 =number_format((($d1)), 1, '.', ',');} else $row->d27=''; break;
							case 28: if($d1!=0){$row->d28 =number_format((($d1)), 1, '.', ',');} else $row->d28=''; break;
							case 29: if($d1!=0){$row->d29 =number_format((($d1)), 1, '.', ',');} else $row->d29=''; break;
							case 30: if($d1!=0){$row->d30 =number_format((($d1)), 1, '.', ',');} else $row->d30=''; break;
							case 31: if($d1!=0){$row->d31 =number_format((($d1)), 1, '.', ',');} else $row->d31=''; break;
						 }	 
						 //busco para actualizar o agregar kpi general del dia
						 
						 if($d1!=''){
							$this->db->select('nrdad');
						 	$this->db->where('fech =',$fr1 [$ini]);$this->db->where('nadr =',20);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	if($d1!='') $por1=number_format($d1, 1, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr1 [$ini],'nadr'=>20);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }		
					endforeach;
					//}
				  $ini+=1;	
				}							
				$data[] = $row;
				endforeach;
			return $data;	
		}
	//Gral Mes viaticos
		function gettec($filter,$cic,$mes){
			$ini=1;
			$data = array();
			//mes
			
			$fecha='2017-'.$mes.'-01';
			while($ini<=31){
				$fr1 [$ini]=$fecha;
				$fecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;$fecha = date ( 'Y-m-d' , $fecha ); 
				$ini+=1;				
			}
			$ini=1;
			
			//agarrar los numeros de biologos activos
			$this->db->select('NumBio,NomBio,Alias'); $this->db->where('alias !=',''); $this->db->where('activo =',0); 
			$this->db->where('numbio !=',63);
			$this->db->order_by('nombio');$this->db->order_by('NomBio');
			$query=$this->db->get('biologo');
			$bio=1; $cont=0;
			foreach ($query->result() as $row):
				$datosBi [$bio]=$row->NumBio; $datoslug [$bio]=$row->Alias;
				$bio+=1;$cont+=1;
			endforeach;
			$nbio=$bio;//$nb=29;$nbio=29;
			$nb=1;			
			while($nb<$nbio){
			    $biolog=$datosBi [$nb];$ini=1;$entro2=0;
				$this->db->select('Alias,day(FechaR) as dia,RemisionR,Loc,Edo,NumRegR');
				$this->db->join('biologo', 'NumBio=NumBioR', 'inner');	$this->db->join('clientes', 'NumCliR = Numero', 'inner');
				//$this->db->join('serbio', 'NumRemS = NumRegR','left');
				//$this->db->group_by('NumRemS');
				$this->db->where('Estatus <',2); $this->db->where('NumBio =',$biolog);
				if($filter['where']!='') {$this->db->where($filter['where']);}
				$this->db->order_by('NomBio');$this->db->order_by('dia','ASC');	$this->db->order_by('RemisionR', 'ASC');
				$result = $this->db->get($cic);
				$cantidadre=$result->num_rows(); $conta=1; $entro=0;  $contax=0; $resp1=1; 
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;$diac=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$datosd [$dia]=$row->dia;$rem=$row->NumRegR;
						$ser='';$adm='';
						//$datosr [$dia]=$row->RemisionR.'-'.$row->Loc.' '.substr($row->Edo, 0, 3);
						//if($row->Ser==0 and $row->Folio!='') $ser='[SC]';
							//else if($row->Ser!=0) $ser='[*]';
						$this->db->select('Ser,Folio,Adm');	
						$this->db->where('NumRemS =',$rem);
						$resulta = $this->db->get('serbio');
						$cantidadser=$resulta->num_rows(); 
						if($cantidadser>=1){
							foreach ($resulta->result() as $rowser):
								$adm=$rowser->Adm;
								if($adm!=0){
									if($adm==2){$adm='.';}
									if($adm==-2){$adm='-';}
									if($adm==1){$adm=' ';}
								}else{$adm='';}
								if($rowser->Ser==0 and $rowser->Folio!=''){$ser='[SC]';}
							endforeach;	
						}else{
							$ser='[F]';$adm='';
						}	
						if($diac!=$dia){
							//$datosr [$dia]=$adm.'['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser; $diac=$dia;
							$datosr [$dia]=$adm.$row->RemisionR; $diac=$dia;
						}
						else{
							//$datosr [$dia]=$adm.$datosr [$dia].'<br>['.$row->RemisionR.']<br>'.substr($row->Edo, 0, 3).$ser;
							$datosr [$dia]=$adm.$datosr [$dia].$row->RemisionR;
						}
						$tot+=1;
					endforeach;
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tec=$datoslug [$nb];$data[] = $row;
				}
				else{
					$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec=$datoslug [$nb];
					$row->d1 ="";$row->d2 ="";$row->d3 ="";$row->d4 ="";$row->d5 ="";$row->d6 ="";$row->d7 ="";$row->d8 ="";$row->d9 ="";$row->d10 ="";
					$row->d11 ="";$row->d12 ="";$row->d13 ="";$row->d14 ="";$row->d15 ="";$row->d16 ="";$row->d17 ="";$row->d18 ="";$row->d19 ="";$row->d20 ="";
					$row->d21 ="";$row->d22 ="";$row->d23 ="";$row->d24 ="";$row->d25 ="";$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";
					$row->d31 ="";
					$data[] = $row;
					endforeach;	
				}
			$nb+=1;
			}
					/*$this->db->select('max(RemisionR)');
					$result = $this->db->get($cic);
					foreach ($result->result() as $row):
					$row->tec='dia';
					$row->d1 =$fr1 [1];$row->d2 =$fr1 [2];$row->d3 =$fr1 [3];$row->d4 =$fr1 [4];$row->d5 =$fr1 [5];
					$row->d6 =$fr1 [6];$row->d7 =$fr1 [7];$row->d8 =$fr1 [8];$row->d9 =$fr1 [9];$row->d10 =$fr1 [10];
					$row->d11 =$fr1 [11];$row->d12 =$fr1 [12];$row->d13 =$fr1 [13];$row->d14 =$fr1 [14];$row->d15 =$fr1 [15];
					$row->d16 =$fr1 [16];$row->d17 =$fr1 [17];$row->d18 =$fr1 [18];$row->d19 =$fr1 [19];$row->d20 =$fr1 [20];
					$row->d21 =$fr1 [21];$row->d22 =$fr1 [22];$row->d23 =$fr1 [23];$row->d24 =$fr1 [24];$row->d25 =$fr1 [25];
					$row->d26 ="";$row->d27 ="";$row->d28 ="";$row->d29 ="";$row->d30 ="";$row->d31 ="";
					$data[] = $row;
					endforeach;	*/
			//general porcentaje dia
			$this->db->select('max(RemisionR)');
			$result = $this->db->get($cic);
			foreach ($result->result() as $row):
				$row->tec ="KPI";$ini=1;$tc=0;
				while($ini<=31){
					$this->db->select('count(RemisionR) as tot');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('NumBioR !=',63);
					$queryt=$this->db->get('r18');
					//if($queryt->num_rows()>0){
					foreach($queryt->result() as $rowcs):
						$tt=$rowcs->tot;
					endforeach;	
					$this->db->select('count(RemisionR),count(Folio),count(Adm) as Adm');
					$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
					$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Adm =',2);$this->db->where('NumBioR !=',63);
					$query=$this->db->get('r18');
					
					
					/*$fecis=$fr1 [$ini];
					$query=$this->db->query("select count(RemisionR),(count(Adm))as Adm from r18 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=2 and NumbioR!=63");*/
					foreach($query->result() as $rowcs):
						$adm1=0;
						$d1=$rowcs->Adm; 
						//$querys=$this->db->query("select count(Adm)as Adm1 from r18 left join serbio on NumRemS=NumRegR where FechaR=$fecis and Estatus<=1 and Adm=1 and NumbioR!=63");
						$this->db->select('count(RemisionR),count(Folio),count(Adm) as Adm1');
						$this->db->join('serbio', 'NumRemS=NumRegR', 'left'); 
						$this->db->where('FechaR =',$fr1 [$ini]);$this->db->where('Estatus <=',1); $this->db->where('Adm =',1);$this->db->where('NumBioR !=',63);
						$querys=$this->db->get('r18');
						 foreach($querys->result() as $rowcss):
							$adm1=$rowcss->Adm1;
						endforeach;	
						if($adm1 > 0) $d1=($d1)+($adm1*.75); 
						 switch($ini){
						 	case 1: if($d1>0 and $tt>0){$row->d1 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d1=''; break;
							case 2: if($d1>0 and $tt>0){$row->d2 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d2=''; break;
							case 3: if($d1>0 and $tt>0){$row->d3 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d3=''; break;
							case 4: if($d1>0 and $tt>0){$row->d4 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d4=''; break;
							case 5: if($d1>0 and $tt>0){$row->d5 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d5=''; break;
							case 6: if($d1>0 and $tt>0){$row->d6 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d6=''; break;
							case 7: if($d1>0 and $tt>0){$row->d7 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d7=''; break;
							case 8: if($d1>0 and $tt>0){$row->d8 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d8=''; break;
							case 9: if($d1>0 and $tt>0){$row->d9 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d9=''; break;
							case 10: if($d1>0 and $tt>0){$row->d10 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d10=''; break;
							case 11: if($d1>0 and $tt>0){$row->d11 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d11=''; break;
							case 12: if($d1>0 and $tt>0){$row->d12 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d12=''; break;
							case 13: if($d1>0 and $tt>0){$row->d13 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d13=''; break;
							case 14: if($d1>0 and $tt>0){$row->d14 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d14=''; break;
							case 15: if($d1>0 and $tt>0){$row->d15 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d15=''; break;
							case 16: if($d1>0 and $tt>0){$row->d16 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d16=''; break;
							case 17: if($d1>0 and $tt>0){$row->d17 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d17=''; break;
							case 18: if($d1>0 and $tt>0){$row->d18 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d18=''; break;
							case 19: if($d1>0 and $tt>0){$row->d19 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d19=''; break;
							case 20: if($d1>0 and $tt>0){$row->d20 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d20=''; break;
							case 21: if($d1>0 and $tt>0){$row->d21 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d21=''; break;
							case 22: if($d1>0 and $tt>0){$row->d22 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d22=''; break;
							case 23: if($d1>0 and $tt>0){$row->d23 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d23=''; break;
							case 24: if($d1>0 and $tt>0){$row->d24 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d24=''; break;
							case 25: if($d1>0 and $tt>0){$row->d25 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d25=''; break;
							case 26: if($d1>0 and $tt>0){$row->d26 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d26=''; break;
							case 27: if($d1>0 and $tt>0){$row->d27 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d27=''; break;
							case 28: if($d1>0 and $tt>0){$row->d28 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d28=''; break;
							case 29: if($d1>0 and $tt>0){$row->d29 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d29=''; break;
							case 30: if($d1>0 and $tt>0){$row->d30 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d30=''; break;
							case 31: if($d1>0 and $tt>0){$row->d31 =number_format((($d1/$tt)*100), 1, '.', ',');} else $row->d31=''; break;
						 }	 
						 //busco para actualizar o agregar kpi general del dia
						 
						 if($d1>0){
							$this->db->select('nrdad');
						 	$this->db->where('fech =',$fr1 [$ini]);$this->db->where('nadr =',7);
						 	$queryres=$this->db->get('analisisresdiadepto');$por1=0;
						 	if($tt>0) $por1=number_format((($d1/$tt)*100), 1, '.', ',');
						 	if($queryres->num_rows()>0){
						 		foreach($queryres->result() as $rowres):
						 			$actualiza=$rowres->nrdad;
									$datas=array('estr'=>$por1); 
									$this->db->where('nrdad',$actualiza);
									$this->db->update('analisisresdiadepto',$datas); $por1=0;
						 		endforeach;
						 	}else{
						 		$datas=array('estr'=>$por1,'fech'=>$fr1 [$ini],'nadr'=>7);			
								$this->db->insert('analisisresdiadepto',$datas);$por1=0;
						 	}
						 }		 
					endforeach;
					//}
				  $ini+=1;	
				}//if($tc>0)$row->tc =number_format((($tc/$por)*100), 1, '.', ',').'%'; else $row->tc='';							
				$data[] = $row;
				endforeach;
			return $data;	
		}


   
   }
	function sd_square($x, $mean) { return pow($x - $mean,2); } 
	function sd($array,$cont) { 
    	// square root of sum of squares devided by N-1 
		return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)) ); 
		//return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,$cont, (array_sum($array) / $cont) ) ) ) / $cont );
	} 
	
	

    	
?>