<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Combustible_model extends CI_Model {
        
		
		public $idc="idc";
		public $cfec="cfec";
		public $clts="clts";
		public $cdg="cdg";
		public $uide="uide";
		public $cvale="cvale";
		public $cobs="cobs";
		public $cimp="cimporte";
		public $tablaCom="com_";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		public function cagregar($cfec,$clts,$cvale,$cdg,$cobs,$uide,$ciclo,$cimp){
			//$ciclo=14;
			$cimp = str_replace(",", "", $cimp);$clts = str_replace(",", "", $clts);
			$data=array($this->cfec=>$cfec,$this->clts=>$clts,$this->cvale=>$cvale,$this->cdg=>$cdg,$this->cobs=>$cobs,$this->uide=>$uide,$this->cimp=>$cimp);			
			$this->db->insert($this->tablaCom.$ciclo,$data);
			$this->db->insert_id();
			return 0;	
		}
		public function cactualizar($id_post,$cfec,$clts,$cvale,$cdg,$cobs,$uide,$ciclo,$cimp){
			//$ciclo=14;
			$cimp = str_replace(",", "", $cimp);$clts = str_replace(",", "", $clts);
			$data=array($this->cfec=>$cfec,$this->clts=>$clts,$this->cvale=>$cvale,$this->cdg=>$cdg,$this->cobs=>$cobs,$this->uide=>$uide,$this->cimp=>$cimp);
			$this->db->where($this->idc,$id_post);
			$this->db->update($this->tablaCom.$ciclo,$data);
			if($this->db->affected_rows()>0){
				return 1;
			} else {
				return 0;
			}
		}
		function cborrar($id,$ciclo){
			//$ciclo=14;
			$this->db->where($this->idc,$id);
			$this->db->delete($this->tablaCom.$ciclo);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		/*function quitarDet($id){
			$this->db->where($this->nmue,$id);
			$this->db->delete($this->tablacv);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}*/
		
		function getCombustible($filter,$ciclo){
			//$ciclo=14;
			$this->db->select('idc,cfec,clts,cvale,cobs,NomUni,uide,cdg,cimporte');
			$this->db->join('unidad', 'NumUni ='.$this->uide, 'inner');
			$this->db->order_by($this->cvale,'DESC');
			//$this->db->order_by($this->pfec);$this->db->order_by($this->pide);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaCom.$ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaCom.$ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$cli='';$totgranja=0;
			foreach($result->result() as $row):
				$row->clts=number_format($row->clts, 0, '.', ',');
				$row->cimporte=number_format($row->cimporte, 2, '.', ',');
				if($row->cdg==1){$row->cltsd=$row->clts;$row->cltsg='';}else{$row->cltsg=$row->clts;$row->cltsd='';}
				$data[] = $row;		
			endforeach;
			return $data;
		}
		function getNumRowsC($filter,$ciclo){
			//$ciclo=14;
			//$this->db->select('idc,cfec,clts,cvale,cobs,NomUni');
			$this->db->join('unidad', 'NumUni ='.$this->uide, 'inner');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			$result = $this->db->get($this->tablaCom.$ciclo);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('NomUni');
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		
		function history($pil){
			$this->db->select('dg');
			$this->db->from('unidad');
			$this->db->where('NumUni =', $pil);
			$query=$this->db->get();
			return $query->row();
		}
		
		function getCombustibleG($filter,$ciclo,$mes){
			//select NomUni,(select sum(clts) from com_14 where uide=NumUni and cdg=1) as diesel,(select sum(clts) from com_14 where uide=NumUni and cdg=2) as gasolina from unidad inner join com_14 on uide=NumUni where clts>0 group by NumUni order by NomUni
			//$query=$this->db->query("select NomUni,uide,
			//								(select sum(clts) from com_$ciclo where uide=NumUni and cdg=1) as diesel,
			//								(select sum(clts) from com_$ciclo where uide=NumUni and cdg=2) as gasolina 
			//								from unidad inner join com_$ciclo on uide=NumUni where clts>0 group by NumUni order by NomUni");
			if($mes==0){
			$this->db->select('NomUni,uide,(select sum(clts) from com_'.$ciclo.' where uide=NumUni and cdg=1) as diesel,(select sum(clts) from com_'.$ciclo.' where uide=NumUni and cdg=2) as gasolina ');
			}else{
			$this->db->select('NomUni,uide,(select sum(clts) from com_'.$ciclo.' where uide=NumUni and cdg=1 and month(cfec)='.$mes.') as diesel,(select sum(clts) from com_'.$ciclo.' where uide=NumUni and cdg=2 and month(cfec)='.$mes.') as gasolina ');	
			}
			$this->db->join('unidad', 'NumUni ='.$this->uide, 'inner');
			$this->db->where('clts >',0);
			$this->db->group_by('NumUni');
			$this->db->order_by('NomUni');
			if($filter['where']!='')
				$this->db->where($filter['where']);
			$query = $this->db->get('com_'.$ciclo);
			$data = array();
			//Se forma el arreglo que sera retornado
			$td=0;$tg=0;
			foreach($query->result() as $row):
				if($row->uide!=77){$td+=$row->diesel;$tg+=$row->gasolina;}
				if($row->diesel>0){$row->diesel=number_format($row->diesel, 0, '.', ',');}else{$row->diesel='';}
				if($row->gasolina>0){$row->gasolina=number_format($row->gasolina, 0, '.', ',');}else{$row->gasolina='';}
				$data[] = $row;	
			endforeach;
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):				
				$rowZ->NomUni = "Total:";
				if($td>0){$rowZ->diesel =number_format($td, 0, '.', ',');}else{$rowZ->diesel='';}
				if($tg>0){$rowZ->gasolina =number_format($tg, 0, '.', ',');}else{$rowZ->gasolina='';}
				$data[] = $rowZ;	
			endforeach;	
			return $data;
		}
		
		function getCombustibleGD($filter,$ciclo){
			//select cfec,cvale,clts,cobs,cdg from com_14 where uide=64
			$this->db->select('cfec,cvale,clts,cobs,cdg,cimporte');
			$this->db->order_by($this->cfec);
			//$this->db->order_by($this->pide);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0)
			//	$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaCom.$ciclo,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaCom.$ciclo);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			$td=0;$tg=0;$ti=0;$fec=new Libreria();$fech='';
			foreach($result->result() as $row):
				$row->cfec = $fec->fecha($row->cfec);
				if($row->cimporte>0){$ti+=$row->cimporte;$row->cimporte='$'.number_format($row->cimporte, 2, '.', ',');}else{$row->cimporte='';}
				if($row->cdg==1){$row->diesel=number_format($row->clts, 0, '.', ',');$td+=$row->clts;}else{$row->diesel='';}
				if($row->cdg==2){$row->gasolina=number_format($row->clts, 0, '.', ',');$tg+=$row->clts;}else{$row->gasolina='';}
				$data[] = $row;	
			endforeach;
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):				
				$rowZ->cfec = "Total:";$rowZ->cobs = "";$rowZ->cvale = "";
				if($td>0){$rowZ->diesel =number_format($td, 0, '.', ',');}else{$rowZ->diesel='';}
				if($tg>0){$rowZ->gasolina =number_format($tg, 0, '.', ',');}else{$rowZ->gasolina='';}
				if($ti>0){$rowZ->cimporte ='$'.number_format($ti, 2, '.', ',');}else{$rowZ->cimporte='';}
				$data[] = $rowZ;	
			endforeach;	
			return $data;
		}

		function getVentas($filter){
			// se determina el ciclo con el que iniciara
			$tbl=16;
			$this->db->select('MAX(idc) as ultimo');
			$result = $this->db->get('com_'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="com_".$tbl; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); //$tbl=2016;
				 $tbl=explode("0",$tbl,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="com_0".$tbl;}		 
		 	 	else { $ciclo="com_".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	$veces=($tbl-13)+1;
			//$veces=1;
			$contador=1;			
			$data = array();
	 		while($contador<=$veces){
		 		$mes=1; 
				//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR) = 12 AND Estatus=0 and Tipo=1 AND NumRegR>0 GROUP BY Tipo,month(FechaR)";
				//aqui imprime diciembre
				$this->db->select('sum(cimporte) as canti');
				$this->db->where('uide =',77);
				$this->db->group_by(array("month(cfec)"));
				$resulta = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				foreach ($resulta->result() as $row):	
				endforeach;
				$cantidadt=0;	
				if($tbl<10){ $row->cic = "200".$tbl; } else {$row->cic = "20".$tbl; }
				
				 //aqui se obtiene de enero a diciembre
				while($mes<=12){
					//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR)  = ".$mes." AND Estatus=0 and Tipo=1 AND NumRegR>0  GROUP BY Tipo,month(FechaR)";
					$this->db->select('sum(cimporte) as canti');
					$this->db->where('month(cfec) =', $mes);
					//if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('uide =',77);
					$this->db->group_by(array("month(cfec)"));
					$result = $this->db->get($ciclo);
					$cantidadre=$result->num_rows();
					if($cantidadre>=1){
					foreach ($result->result() as $row2):	
						if($mes==1){$row->ene = '$'.number_format($row2->canti, 2, '.', ',');$row->ene1 = $row2->canti;}
						if($mes==2){$row->feb = '$'.number_format($row2->canti, 2, '.', ',');$row->feb1 = $row2->canti;}
						if($mes==3){$row->mar = '$'.number_format($row2->canti, 2, '.', ',');$row->mar1 = $row2->canti;}
						if($mes==4){$row->abr = '$'.number_format($row2->canti, 2, '.', ',');$row->abr1 = $row2->canti;}
						if($mes==5){$row->may = '$'.number_format($row2->canti, 2, '.', ',');$row->may1 = $row2->canti;}
						if($mes==6){$row->jun = '$'.number_format($row2->canti, 2, '.', ',');$row->jun1 = $row2->canti;}
						if($mes==7){$row->jul = '$'.number_format($row2->canti, 2, '.', ',');$row->jul1 = $row2->canti;}
						if($mes==8){$row->ago = '$'.number_format($row2->canti, 2, '.', ',');$row->ago1 = $row2->canti;}
						if($mes==9){$row->sep = '$'.number_format($row2->canti, 2, '.', ',');$row->sep1 = $row2->canti;}
						if($mes==10){$row->oct = '$'.number_format($row2->canti, 2, '.', ',');$row->oct1 = $row2->canti;}
						if($mes==11){$row->nov = '$'.number_format($row2->canti, 2, '.', ',');$row->nov1 = $row2->canti;}
						if($mes==12){$row->dic = '$'.number_format($row2->canti, 2, '.', ',');$row->dic1 = $row2->canti;}
						$cantidadt+=$row2->canti;
						//$data[] = $row;	
					endforeach;
					}else{
						if($mes==1){$row->ene = "";$row->ene1 = 0;}
						if($mes==2){$row->feb = "";$row->feb1 = 0;}
						if($mes==3){$row->mar = "";$row->mar1 = 0;}
						if($mes==4){$row->abr = "";$row->abr1 = 0;}
						if($mes==5){$row->may = "";$row->may1 = 0;}
						if($mes==6){$row->jun = "";$row->jun1 = 0;}
						if($mes==7){$row->jul = "";$row->jul1 = 0;}
						if($mes==8){$row->ago = "";$row->ago1 = 0;}
						if($mes==9){$row->sep = "";$row->sep1 = 0;}
						if($mes==10){$row->oct = "";$row->oct1 = 0;}
						if($mes==11){$row->nov = "";$row->nov1 = 0;}
						if($mes==12){$row->dic = "";$row->dic1 = 0;}
						$cantidadt+=0;
					}	
					$mes=$mes+1;
				}
				//aqui imprime el total por ciclo
				$row->tot = '$'.number_format($cantidadt, 2, '.', ',');
				//if($tbl<10){ $row->cic = "200".$tbl; } else {$row->cic = "20".$tbl; }
				$data[] = $row;	
				//aqui incrementa el contador y toma el siguiente ciclo
				$contador=$contador+1;$tbl-=1;
				if($tbl<10){ $ciclo="com_0".$tbl;} else { $ciclo="com_".$tbl; }
			}
			return $data;
		}
		function getNumRowsV($filter){
			$tbl=16;
			$this->db->select('MAX(idc) as ultimo');
			$result = $this->db->get('com_'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="com_".$tbl; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); $tbl=2014; $tbl=explode("0",$tbl,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="com_0".$tbl;}		 
		 	 	else { $ciclo="com_".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	$veces=($tbl-13)+1;			
			return $veces;
		}
		function getVentasC($filter){
			
			// se determina el ciclo con el que iniciara
			//$tbl=13;
			$this->db->select('MAX(idc) as ultimo');
			//If($filter['num']!=0){
				if($filter['num']==2013){$result = $this->db->get('com_13'); $tbl1="13"; $tbl=13;}
				if($filter['num']==2014){$result = $this->db->get('com_14'); $tbl1="14"; $tbl=14;}
				if($filter['num']==2015){$result = $this->db->get('com_15'); $tbl1="15"; $tbl=15;}
				if($filter['num']==2016){$result = $this->db->get('com_16'); $tbl1="16"; $tbl=16;}
				if($filter['num']==2017){$result = $this->db->get('com_17'); $tbl1="17"; $tbl=17;}
				if($filter['num']==2018){$result = $this->db->get('com_17'); $tbl1="18"; $tbl=18;}
			//}
			//$result = $this->db->get('r'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="com_".$tbl1; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); //$tbl=2014; 
				 $tbl=explode("0",$tbl1,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="com_0".$tbl1;}		 
		 	 	else { $ciclo="com_".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	//$veces=($tbl-8)+1;
			//$ciclo="r13";
			$veces=2; 
			$contador=1;			
			$data = array(); $c=0;$datos[$c]=0;
	 		while($contador<=$veces){
		 		$mes=1; 
				//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR) = 12 AND Estatus=0 and Tipo=1 AND NumRegR>0 GROUP BY Tipo,month(FechaR)";
				//aqui imprime diciembre
				$this->db->select('sum(cimporte) as canti');
				//$this->db->where('month(FechaR) =',12);
				if($filter['where']!=''){$this->db->where($filter['where']);}
				//$this->db->where('Estatus =',0);
				$this->db->where('uide =',77);
				$this->db->group_by(array("month(cfec)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				
				//if($cantidadre>=1){
				foreach ($result->result() as $row):	
						
				endforeach;
				if($tbl<10){ $row->cic = "200".$tbl; } else {$row->cic = "20".$tbl; }
				if($filter['num']==2013 && $veces==2) { $row->cic = "2013"; }
				$cantidadt=0;
				//$data[] = $row;
				/*
				$this->db->select('sum(cimporte) as canti');
				$this->db->where('month(cfec) =',12);
				//$this->db->where('Estatus =',0);
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('uide =',77);
				
				$this->db->group_by(array( "month(cfec)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				$cantidadt=0;
				if($cantidadre>=1){
				foreach ($result->result() as $row1):	
					$row->dic = number_format($row1->canti, 3, '.', ',');
					$cantidadt=$row1->canti;
					$datos [$c]=$row1->canti;$c=$c+1;
					//$data[] = $row;	
				endforeach;	
				} else {
					$row->dic = "";$datos [$c]=0;$c=$c+1;
				}*/
				 //aqui se obtiene de enero a septiembre
				while($mes<=12){
					//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR)  = ".$mes." AND Estatus=0 and Tipo=1 AND NumRegR>0  GROUP BY Tipo,month(FechaR)";
					$this->db->select('sum(cimporte) as canti');
					$this->db->where('month(cfec) =', $mes);
					//$this->db->where('Estatus =',0);
					if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('uide =',77);
					$this->db->group_by(array("month(cfec)"));
					$result = $this->db->get($ciclo);
					$cantidadre=$result->num_rows();
					if($cantidadre>=1){					
					foreach ($result->result() as $row2):	
						if($mes==1){$row->ene = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->ene = "";}
						if($mes==2){$row->feb = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->feb = "";}
						if($mes==3){$row->mar = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->mar = "";}
						if($mes==4){$row->abr = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->abr = "";}
						if($mes==5){$row->may = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->may = "";}
						if($mes==6){$row->jun = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->jun = "";}
						if($mes==7){$row->jul = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->jul = "";}
						if($mes==8){$row->ago = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->ago = "";}
						if($mes==9){$row->sep = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->sep = "";}
						if($mes==10){$row->oct = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->oct = "";}
						if($mes==11){$row->nov = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->nov = "";}
						if($mes==12){$row->dic = '$'.number_format($row2->canti, 2, '.', ',');if($mes>date('m')) $row->dic = "";}
						
						$datos [$c]=$row2->canti;$c=$c+1;
						//$data[] = $row;	
					endforeach;
					}else{
						if($mes==1){$row->ene = "";}
						if($mes==2){$row->feb = "";}
						if($mes==3){$row->mar = "";}
						if($mes==4){$row->abr = "";}
						if($mes==5){$row->may = "";}
						if($mes==6){$row->jun = "";}
						if($mes==7){$row->jul = "";}
						if($mes==8){$row->ago = "";}
						if($mes==9){$row->sep = "";}
						if($mes==10){$row->oct = "";}
						if($mes==11){$row->nov = "";}
						if($mes==12){$row->dic = "";}
						$datos [$c]=0;$c=$c+1;
					}	
					if($mes<=date('m')){$cantidadt+=$row2->canti;}
					$mes=$mes+1;
					
				}
				//aqui incrementa el contador y toma el siguiente ciclo
				$contador=$contador+1;$tbl-=1;
				if($tbl<10){ $ciclo="com_0".$tbl;} else { $ciclo="com_".$tbl; }
				if($tbl<=13){ $ciclo="com_13";}
				//aqui imprime el total por ciclo
				$row->tot = '$'.number_format($cantidadt, 2, '.', ',');
				$data[] = $row;	
				$datos [$c]=$cantidadt;$c=$c+1;
			}
			//se hace esta consulta solo para poder agregar la diferencia de postlarvas
			$this->db->select('MAX(idc) as ultimo');$resultz = $this->db->get($ciclo);
			foreach ($resultz->result() as $rowz):
		 		$rowz->cic = "Imp";
				$rowz->ene =number_format(($datos[0]-$datos[13]), 2, '.', ','); if($rowz->ene==0.00) {$rowz->ene="";}if(1>date('m')){ $rowz->ene = "";}
				$rowz->feb =number_format($datos[1]-$datos[14], 2, '.', ','); if($rowz->feb==0.00) {$rowz->feb="";}if(2>date('m')){ $rowz->feb = "";}
				$rowz->mar =number_format($datos[2]-$datos[15], 2, '.', ','); if($rowz->mar==0.00) {$rowz->mar="";}if(3>date('m')){ $rowz->mar = "";}
				$rowz->abr =number_format($datos[3]-$datos[16], 2, '.', ','); if($rowz->abr==0.00) {$rowz->abr="";}if(4>date('m')){ $rowz->abr = "";}
				$rowz->may =number_format($datos[4]-$datos[17], 2, '.', ','); if($rowz->may==0.00) {$rowz->may="";}if(5>date('m')){ $rowz->may = "";}
				$rowz->jun =number_format($datos[5]-$datos[18], 2, '.', ','); if($rowz->jun==0.00) {$rowz->jun="";}if(6>date('m')){ $rowz->jun = "";}
				$rowz->jul =number_format($datos[6]-$datos[19], 2, '.', ','); if($rowz->jul==0.00) {$rowz->jul="";}if(7>date('m')){ $rowz->jul = "";}
				$rowz->ago =number_format($datos[7]-$datos[20], 2, '.', ','); if($rowz->ago==0.00) {$rowz->ago="";}if(8>date('m')){ $rowz->ago = "";}
				$rowz->sep =number_format($datos[8]-$datos[21], 2, '.', ','); if($rowz->sep==0.00) {$rowz->sep="";}if(9>date('m')){ $rowz->sep = "";}
				$rowz->oct =number_format($datos[9]-$datos[22], 2, '.', ','); if($rowz->oct==0.00) {$rowz->oct="";}if(10>date('m')){ $rowz->oct = "";}
				$rowz->nov =number_format($datos[10]-$datos[23], 2, '.', ','); if($rowz->nov==0.00) {$rowz->nov="";}if(11>date('m')){ $rowz->nov = "";}
				$rowz->dic =number_format($datos[11]-$datos[24], 2, '.', ','); if($rowz->dic==0.00) {$rowz->dic="";}if(12>date('m')){ $rowz->dic = "";}
				$rowz->tot =number_format($datos[12]-$datos[25], 2, '.', ','); if($rowz->tot==0.00) {$rowz->tot="";}
				$data[] = $rowz;
				
			endforeach;
			//se hace esta consulta solo para poder agregar el porcentaje
			$this->db->select('MAX(idc) as ultimo');$resultp = $this->db->get($ciclo);
			foreach ($resultp->result() as $rowp):
				$rowp->cic = "%";
				if($datos[13]>0){ $rowp->ene=number_format((($datos[0]-$datos[13])/$datos[13]*100), 2, '.', ',')."%";}else{$rowp->ene="";}if(1>date('m')){ $rowp->ene = "";}
				if($datos[14]>0){ $rowp->feb=number_format((($datos[1]-$datos[14])/$datos[14]*100), 2, '.', ',')."%";}else{$rowp->feb="";}if(2>date('m')){ $rowp->feb = "";}
				if($datos[15]>0){ $rowp->mar=number_format((($datos[2]-$datos[15])/$datos[15]*100), 2, '.', ',')."%";}else{$rowp->mar="";}if(3>date('m')){ $rowp->mar = "";}
				if($datos[16]>0){ $rowp->abr=number_format((($datos[3]-$datos[16])/$datos[16]*100), 2, '.', ',')."%";}else{$rowp->abr="";}if(4>date('m')){ $rowp->abr = "";}
				if($datos[17]>0){ $rowp->may=number_format((($datos[4]-$datos[17])/$datos[17]*100), 2, '.', ',')."%";}else{$rowp->may="";}if(5>date('m')){ $rowp->may = "";}
				if($datos[18]>0){ $rowp->jun=number_format((($datos[5]-$datos[18])/$datos[18]*100), 2, '.', ',')."%";}else{$rowp->jun="";}if(6>date('m')){ $rowp->jun = "";}
				if($datos[19]>0){ $rowp->jul=number_format((($datos[6]-$datos[19])/$datos[19]*100), 2, '.', ',')."%";}else{$rowp->jul="";}if(7>date('m')){ $rowp->jul = "";}
				if($datos[20]>0){ $rowp->ago=number_format((($datos[7]-$datos[20])/$datos[20]*100), 2, '.', ',')."%";}else{$rowp->ago="";}if(8>date('m')){ $rowp->ago = "";}
				if($datos[21]>0){ $rowp->sep=number_format((($datos[8]-$datos[21])/$datos[21]*100), 2, '.', ',')."%";}else{$rowp->sep="";}if(9>date('m')){ $rowp->sep = "";}
				if($datos[22]>0){ $rowp->oct=number_format((($datos[9]-$datos[22])/$datos[22]*100), 2, '.', ',')."%";}else{$rowp->oct="";}if(10>date('m')){ $rowp->oct = "";}
				if($datos[23]>0){ $rowp->nov=number_format((($datos[10]-$datos[23])/$datos[23]*100), 2, '.', ',')."%";}else{$rowp->nov="";}if(11>date('m')){ $rowp->nom = "";}
				if($datos[24]>0){ $rowp->dic=number_format((($datos[11]-$datos[24])/$datos[24]*100), 2, '.', ',')."%";}else{$rowp->dic="";}if(12>date('m')){ $rowp->dic = "";}
				if($datos[25]>0){ $rowp->tot=number_format((($datos[12]-$datos[25])/$datos[25]*100), 2, '.', ',')."%";}else{$rowp->tot="";}
				$data[] = $rowp;
				endforeach;
			return $data;
		}
		function getNumRowsVC($filter){
			$veces=2;			
			return $veces;
		}
	
    }
    
?>