<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Diesel_model extends CI_Model {
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getdieselG($filter,$ciclo,$por){
			//SELECT NDepS,NomDepA from almsal_20 inner JOIN departamentoa on NDepA=NDepS where NDepS>0 and CBS LIKE 'diesel%' group by NDepS order by NomDepA
			$this->db->select('NDepS,NomDepA,sum(CanS) as litros');	
			$this->db->join('departamentoa', 'NDepA = NDepS', 'inner');
			$this->db->where('year(FecS)','20'.$ciclo);
			$this->db->where('NDepS >',0);
			$this->db->where('CBS LIKE','diesel%');
			//$this->db->or_where('CBS LIKE','disesel%');
			$this->db->group_by('NDepS');
			$this->db->order_by('litros','DESC');
			$this->db->order_by('NomDepA');
			$query = $this->db->get('almsal_'.$ciclo);
			//$totuni=$query->num_rows();
			$data = array();
			//Se obtiene el total de lo consumido de diesel
			$this->db->select('sum(CanS) as lts');
			$this->db->where('year(FecS)','20'.$ciclo);
			$this->db->where('CBS LIKE','diesel%');
			//$this->db->or_where('CBS LIKE','disesel%');
			$this->db->order_by('month(FecS)');
			$querym = $this->db->get('almsal_'.$ciclo);	
			foreach($querym->result() as $rowm):
				$totdie=$rowm->lts;;
			endforeach;
			$tene=0;$tfeb=0;$tmar=0;$tabr=0;$tmay=0;$tjun=0;$tjul=0;$tago=0;$tsep=0;$toct=0;$tnov=0;$tdic=0;$tttt=0;$acum=0;
			foreach($query->result() as $row):
				$contx=1;$tuni=0;
				while ($contx <= 12) {  $datos[$contx] =array('',''); $contx+=1;}
				$row->ene='';$row->feb='';$row->mar='';$row->abr='';$row->may='';$row->jun='';
				$row->jul='';$row->ago='';$row->sep='';$row->oct='';$row->nov='';$row->dic='';$row->tot='';$row->por='';
				//SELECT month(FecS),sum(CanS) lts from almsal_20 where NDepS='104' and CBS LIKE 'diesel%' group by month(FecS)
				$this->db->select('month(FecS) as mes,sum(CanS) as lts');
				$this->db->where('year(FecS)','20'.$ciclo);
				$this->db->where('NDepS', $row->NDepS);
				$this->db->where('CBS LIKE','diesel%');
				//$this->db->or_where('CBS LIKE','disesel%');
				$this->db->group_by('month(FecS)');
				$this->db->order_by('month(FecS)');
				$querym = $this->db->get('almsal_'.$ciclo);	
				$contx=1;
				foreach($querym->result() as $rowm):
					$tuni+=$rowm->lts;$tttt+=$rowm->lts;
					$datos[$contx]=array($rowm->mes,$rowm->lts);
					$contx+=1;
				endforeach;
				$cont=1;	
				while($cont <= $contx-1){
					switch ($datos [$cont][0]) {
						case '1': $row->ene=number_format($datos [$cont][1], 1, '.', ','); $tene+=$datos [$cont][1];	break;
						case '2': $row->feb=number_format($datos [$cont][1], 1, '.', ','); $tfeb+=$datos [$cont][1];	break;
						case '3': $row->mar=number_format($datos [$cont][1], 1, '.', ','); $tmar+=$datos [$cont][1];	break;
						case '4': $row->abr=number_format($datos [$cont][1], 1, '.', ','); $tabr+=$datos [$cont][1];	break;
						case '5': $row->may=number_format($datos [$cont][1], 1, '.', ','); $tmay+=$datos [$cont][1];	break;
						case '6': $row->jun=number_format($datos [$cont][1], 1, '.', ','); $tjun+=$datos [$cont][1];	break;
						case '7': $row->jul=number_format($datos [$cont][1], 1, '.', ','); $tjul+=$datos [$cont][1];	break;
						case '8': $row->ago=number_format($datos [$cont][1], 1, '.', ','); $tago+=$datos [$cont][1];	break;
						case '9': $row->sep=number_format($datos [$cont][1], 1, '.', ','); $tsep+=$datos [$cont][1];	break;
						case '10': $row->oct=number_format($datos [$cont][1], 1, '.', ','); $toct+=$datos [$cont][1];	break;
						case '11': $row->nov=number_format($datos [$cont][1], 1, '.', ','); $tnov+=$datos [$cont][1];	break;
						case '12': $row->dic=number_format($datos [$cont][1], 1, '.', ','); $tdic+=$datos [$cont][1];	break;
					}
					$cont+=1;
				}	
				$row->tot=number_format($tuni, 1, '.', ',');
				
				if($totdie>0){
						$acum+=($tuni/$totdie)*100;
						if($acum<=$por) $row->color=1; else $row->color=0;
						$row->acum=number_format($acum, 2, '.', ',');
						$row->por=number_format(($tuni/$totdie)*100, 2, '.', ',');
						
				} else  {$row->por='';}
				$data[] = $row;	
			endforeach;
			
			$this->db->select('max(numero)');			
			$resultZ = $this->db->get('clientes');
			foreach ($resultZ->result() as $rowZ):		
				$rowZ->ene='';$rowZ->feb='';$rowZ->mar='';$rowZ->abr='';$rowZ->may='';$rowZ->jun='';
				$rowZ->jul='';$rowZ->ago='';$rowZ->sep='';$rowZ->oct='';$rowZ->nov='';$rowZ->dic='';$rowZ->tot='';$rowZ->por='';	
				$rowZ->NomDepA = "Total:";
				if($tene>0){$rowZ->ene =number_format($tene, 1, '.', ',');}
				if($tfeb>0){$rowZ->feb =number_format($tfeb, 1, '.', ',');}
				if($tmar>0){$rowZ->mar =number_format($tmar, 1, '.', ',');}
				if($tabr>0){$rowZ->abr =number_format($tabr, 1, '.', ',');}
				if($tmay>0){$rowZ->may =number_format($tmay, 1, '.', ',');}
				if($tjun>0){$rowZ->jun =number_format($tjun, 1, '.', ',');}
				if($tjul>0){$rowZ->jul =number_format($tjul, 1, '.', ',');}
				if($tago>0){$rowZ->ago =number_format($tago, 1, '.', ',');}
				if($tsep>0){$rowZ->sep =number_format($tsep, 1, '.', ',');}
				if($toct>0){$rowZ->oct =number_format($toct, 1, '.', ',');}
				if($tnov>0){$rowZ->nov =number_format($tnov, 1, '.', ',');}
				if($tdic>0){$rowZ->dic =number_format($tdic, 1, '.', ',');}
				if($tttt>0){$rowZ->tot =number_format($tttt, 1, '.', ',');}
				$data[] = $rowZ;	
			endforeach;	
			return $data;
		}
    
    
	function getdieselGc($filter,$mesf){
			$c1=21;$c2=22; $mes=1;
			if($mesf==0) $mesf=12;$acu1g=0;$acu2g=0;
			while($mes<=$mesf){
				$this->db->select('max(numero)');			
				$result = $this->db->get('clientes');
				foreach($result->result() as $row):
					switch ($mes) {
						case '1': $row->mes='Enero';break;
						case '2': $row->mes='Febrero';break;
						case '3': $row->mes='Marzo';break;
						case '4': $row->mes='Abril';break;
						case '5': $row->mes='Mayo';break;
						case '6': $row->mes='Junio';break;
						case '7': $row->mes='Julio';break;
						case '8': $row->mes='Agosto';break;
						case '9': $row->mes='Septiembre';break;
						case '10': $row->mes='Octubre';break;
						case '11': $row->mes='Noviembre';break;
						case '12': $row->mes='Diciembre';break;
					}
					$row->sem='';$row->c1lts='';$row->c1acu='';$row->c1gacu='';$row->c2lts='';$row->c2acu='';$row->c2gacu='';
					$row->diflts='';$row->porlts='';$row->difaslts='';$row->poraslts='';$row->difaglts='';$row->poraglts='';
					$acu1=0;$acu2=0;
					$data[] = $row;
					$sem=1;
					while($sem<=4){
						$this->db->select('max(numero)');			
						$results = $this->db->get('clientes');
						foreach($results->result() as $rows):
							$rows->mes='';$rows->porlts=''; 
							switch ($sem) {
								case '1': $rows->sem='1';
										  //obtener los lts por ciclo del 1 al 7
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c1.'-'.$mes.'-1');
										  $this->db->where('FecS <= ','20'.$c1.'-'.$mes.'-7');
										  $query1 = $this->db->get('almsal_'.$c1);	
										  foreach($query1->result() as $row1):
										  	 $rows->c1lts=number_format($row1->lts, 1, '.', ',');
											 $acu1+=$row1->lts;$acu1g+=$row1->lts;
										  endforeach;	
										  $rows->c1acu=number_format($acu1, 1, '.', ',');
										  $rows->c1gacu=number_format($acu1g, 1, '.', ',');
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c2.'-'.$mes.'-1');
										  $this->db->where('FecS <= ','20'.$c2.'-'.$mes.'-7');
										  $query2 = $this->db->get('almsal_'.$c2);	
										  foreach($query2->result() as $row2):
										  	 if($row2->lts>0)$rows->c2lts=number_format($row2->lts, 1, '.', ','); else $rows->c2lts='';
											 $acu2+=$row2->lts;$acu2g+=$row2->lts;
										  endforeach;	
										  if($acu2>0) $rows->c2acu=number_format($acu2, 1, '.', ','); else $rows->c2acu='';
										  $rows->c2gacu=number_format($acu2g, 1, '.', ',');
										  $rows->diflts=number_format($row2->lts-$row1->lts, 1, '.', ',');
										  $rows->difaslts=number_format($acu2-$acu1, 1, '.', ',');
										  $rows->difaglts=number_format($acu2g-$acu1g, 1, '.', ',');
										  if($row2->lts>0) $rows->porlts=number_format((($row2->lts-$row1->lts)/$row2->lts)*100, 2, '.', ','); else $rows->porlts='';
										  if($acu2>0) $rows->poraslts=number_format((($acu2-$acu1)/$acu2)*100, 2, '.', ','); else $rows->poraslts='';
										  if($acu2g>0) $rows->poraglts=number_format((($acu2g-$acu1g)/$acu2g)*100, 2, '.', ','); else $rows->poraglts='';
										  break;
								case '2': $rows->sem='2';
										  //obtener los lts por ciclo del 8 al 14
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c1.'-'.$mes.'-8');
										  $this->db->where('FecS <= ','20'.$c1.'-'.$mes.'-14');
										  $query1 = $this->db->get('almsal_'.$c1);	
										  foreach($query1->result() as $row1):
										  	 $rows->c1lts=number_format($row1->lts, 1, '.', ',');
											 $acu1+=$row1->lts;$acu1g+=$row1->lts;
										  endforeach;	
										  $rows->c1acu=number_format($acu1, 1, '.', ',');
										  $rows->c1gacu=number_format($acu1g, 1, '.', ',');
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c2.'-'.$mes.'-8');
										  $this->db->where('FecS <= ','20'.$c2.'-'.$mes.'-14');
										  $query2 = $this->db->get('almsal_'.$c2);	
										  foreach($query2->result() as $row2):
										  	 if($row2->lts>0)$rows->c2lts=number_format($row2->lts, 1, '.', ','); else $rows->c2lts='';
											 $acu2+=$row2->lts;$acu2g+=$row2->lts;
										  endforeach;	
										  if($acu2>0) $rows->c2acu=number_format($acu2, 1, '.', ','); else $rows->c2acu='';
										  $rows->c2gacu=number_format($acu2g, 1, '.', ',');
										  $rows->diflts=number_format($row2->lts-$row1->lts, 1, '.', ',');
										  $rows->difaslts=number_format($acu2-$acu1, 1, '.', ',');
										  $rows->difaglts=number_format($acu2g-$acu1g, 1, '.', ',');
										  if($row2->lts>0) $rows->porlts=number_format((($row2->lts-$row1->lts)/$row2->lts)*100, 2, '.', ','); else $rows->porlts='';
										  if($acu2>0) $rows->poraslts=number_format((($acu2-$acu1)/$acu2)*100, 2, '.', ','); else $rows->poraslts='';
										  if($acu2g>0) $rows->poraglts=number_format((($acu2g-$acu1g)/$acu2g)*100, 2, '.', ','); else $rows->poraglts='';
										  break;
								case '3': $rows->sem='3';
										  //obtener los lts por ciclo del 15 al 21
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c1.'-'.$mes.'-15');
										  $this->db->where('FecS <= ','20'.$c1.'-'.$mes.'-21');
										  $query1 = $this->db->get('almsal_'.$c1);	
										  foreach($query1->result() as $row1):
										  	 $rows->c1lts=number_format($row1->lts, 1, '.', ',');
											 $acu1+=$row1->lts;$acu1g+=$row1->lts;
										  endforeach;	
										  $rows->c1acu=number_format($acu1, 1, '.', ',');
										  $rows->c1gacu=number_format($acu1g, 1, '.', ',');
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c2.'-'.$mes.'-15');
										  $this->db->where('FecS <= ','20'.$c2.'-'.$mes.'-21');
										  $query2 = $this->db->get('almsal_'.$c2);	
										  foreach($query2->result() as $row2):
										  	 if($row2->lts>0)$rows->c2lts=number_format($row2->lts, 1, '.', ','); else $rows->c2lts='';
											 $acu2+=$row2->lts;$acu2g+=$row2->lts;
										  endforeach;	
										  if($acu2>0) $rows->c2acu=number_format($acu2, 1, '.', ','); else $rows->c2acu='';
										  $rows->c2gacu=number_format($acu2g, 1, '.', ',');
										  $rows->diflts=number_format($row2->lts-$row1->lts, 1, '.', ',');
										  $rows->difaslts=number_format($acu2-$acu1, 1, '.', ',');
										  $rows->difaglts=number_format($acu2g-$acu1g, 1, '.', ',');
										  if($row2->lts>0) $rows->porlts=number_format((($row2->lts-$row1->lts)/$row2->lts)*100, 2, '.', ','); else $rows->porlts='';
										  if($acu2>0) $rows->poraslts=number_format((($acu2-$acu1)/$acu2)*100, 2, '.', ','); else $rows->poraslts='';
										  if($acu2g>0) $rows->poraglts=number_format((($acu2g-$acu1g)/$acu2g)*100, 2, '.', ','); else $rows->poraglts='';
										  break;
								case '4': $rows->sem='4';
										  //obtener los lts por ciclo del 21 al final del mes
										  $dia1 = cal_days_in_month(CAL_GREGORIAN, $mes, '20'.$c1);
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c1.'-'.$mes.'-22');
										  $this->db->where('FecS <= ','20'.$c1.'-'.$mes.'-'.$dia1);
										  $query1 = $this->db->get('almsal_'.$c1);	
										  foreach($query1->result() as $row1):
										  	 $rows->c1lts=number_format($row1->lts, 1, '.', ',');
											 $acu1+=$row1->lts;$acu1g+=$row1->lts;
										  endforeach;	
										  $rows->c1acu=number_format($acu1, 1, '.', ',');
										  $rows->c1gacu=number_format($acu1g, 1, '.', ',');
										  $dia2 = cal_days_in_month(CAL_GREGORIAN, $mes, '20'.$c2);
										  $this->db->select('sum(CanS) as lts');
										  $this->db->where('CBS LIKE','diesel%');
										  $this->db->where('FecS >= ','20'.$c2.'-'.$mes.'-22');
										  $this->db->where('FecS <= ','20'.$c2.'-'.$mes.'-'.$dia2);
										  $query2 = $this->db->get('almsal_'.$c2);	
										  foreach($query2->result() as $row2):
										  	 if($row2->lts>0)$rows->c2lts=number_format($row2->lts, 1, '.', ','); else $rows->c2lts='';
											 $acu2+=$row2->lts;$acu2g+=$row2->lts;
										  endforeach;	
										  if($acu2>0) $rows->c2acu=number_format($acu2, 1, '.', ','); else $rows->c2acu='';
										  $rows->c2gacu=number_format($acu2g, 1, '.', ',');
										  $rows->diflts=number_format($row2->lts-$row1->lts, 1, '.', ',');
										  $rows->difaslts=number_format($acu2-$acu1, 1, '.', ',');
										  $rows->difaglts=number_format($acu2g-$acu1g, 1, '.', ',');
										  if($row2->lts>0) $rows->porlts=number_format((($row2->lts-$row1->lts)/$row2->lts)*100, 2, '.', ','); else $rows->porlts='';
										  if($acu2>0) $rows->poraslts=number_format((($acu2-$acu1)/$acu2)*100, 2, '.', ','); else $rows->poraslts='';
										  if($acu2g>0) $rows->poraglts=number_format((($acu2g-$acu1g)/$acu2g)*100, 2, '.', ','); else $rows->poraglts='';
										  break;
							}
							$data[] = $rows;
							
						endforeach;
						$sem+=1;
					}	
						
				endforeach;
				$mes+=1;
			}
			return $data;
		}
    }
?>