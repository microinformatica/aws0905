<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Vigilancia_model extends CI_Model {        
        public $id="nrb"; public $lab="lab";  public $dia="dia";  public $hora="hora"; public $mins="mins";	
        public $desc="desc"; public $vigtur="vigtur"; public $ap="ap";
      	public $tabla="bitvigilancia";
		
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		
		
		public function agregar($fec,$hr,$min,$des,$vig,$lab,$ap){
			$data=array($this->dia=>$fec,$this->hora=>$hr,$this->mins=>$min,$this->desc=>$des,$this->vigtur=>$vig,$this->lab=>$lab,$this->ap=>$ap);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$fec,$hr,$min,$des,$vig,$lab,$ap){
			$data=array($this->dia=>$fec,$this->hora=>$hr,$this->mins=>$min,$this->desc=>$des,$this->vigtur=>$vig,$this->lab=>$lab,$this->ap=>$ap);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
		function getbitacora($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']);
			$this->db->order_by('dia');
			$this->db->order_by('ap');
			$this->db->order_by('hora');
			$this->db->order_by('mins');
			$result = $this->db->get($this->tabla);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
				$fec=new Libreria();
			foreach($result->result() as $row):
				$row->FechaD = $fec->fecha($row->dia);
				$row->Tiempo = $row->hora.':'.$row->mins.' '.$row->ap;
				/*if($row->NumReg>0){
					$row->FinVig = date("d-m-Y",strtotime($row->FinVig));
					$row->pos = number_format(($row->CanPos), 3, '.', ',');
				}else{ $row->IniVig = "";$row->FinVig = "";$row->pos = "";	}
				if($row->entregados>0){$row->ent = number_format(($row->entregados), 3, '.', ',');}else{$row->ent="";}
				$row->dif = number_format(($row->CanPos-$row->entregados), 3, '.', ',');
				 * */
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}		
		
		//SELECT nombre from empleados where depto='Vigilancia' and activo=0
		function getVigilantes(){
			$this->db->select('nombre');
			$this->db->where('depto','Vigilancia');
			$this->db->where('activo',0);
			$query = $this->db->get('empleados');
			return $query->result();
			
		}
		
		
		
		
		function borrarc($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	
		
		
		
    }
    
?>