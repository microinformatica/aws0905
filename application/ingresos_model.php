<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Ingresos_model extends CI_Model {
       
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getingresos($filter){
			$mes=1;$ac=date("Y"); $data = array(); $tota=0;$totb=0;$totc=0;$totd=0;$tote=0;$totf=0;$totg=0;$toth=0;$toti=0;
  			while($mes<=12){
	 			if($mes==1){ $nommes="Enero"; } if($mes==2){ $nommes="Febrero"; } if($mes==3){ $nommes="Marzo"; } if($mes==4){ $nommes="Abril"; }
	 			if($mes==5){ $nommes="Mayo"; } if($mes==6){ $nommes="Junio"; } if($mes==7){ $nommes="Julio"; } if($mes==8){ $nommes="Agosto"; }
	 			if($mes==9){ $nommes="Septiembre"; } if($mes==10){ $nommes="Octubre"; } if($mes==11){ $nommes="Noviembre"; } if($mes==12){ $nommes="Diciembre";} 
				$this->db->select('max(RemisionR)');
				$resultZ = $this->db->get('r16');	
				foreach ($resultZ->result() as $rowT):
						$rowT->mes=$nommes;
						//$contador=1; 
						$ao=date("Y"); 
						//while($contador<=7){
		  					$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->a='';
							foreach ($query->result() as $rowa):
								$var=number_format($rowa->canti, 2, '.', ','); 
								if($rowa->canti>0 && $ao='2016'){$rowT->a= $var;$tota+=$rowa->canti;}else{$rowT->a='';}
							endforeach;	
							$ao=$ao-1;
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->b='';
							foreach ($query->result() as $rowb):
								$var= number_format($rowb->canti, 2, '.', ',');
								if($rowb->canti>0 && $ao='2015'){$rowT->b= $var;$totb+=$rowb->canti;}else{$rowT->b='';}
							endforeach;	
							$ao=$ao-1;
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->c='';
							foreach ($query->result() as $rowc):
								$var= number_format($rowc->canti, 2, '.', ',');
								if($rowc->canti>0 && $ao='2014'){$rowT->c= $var;$totc+=$rowc->canti;}else{$rowT->c='';}
							endforeach;	
							$ao=$ao-1;	
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->d='';
							foreach ($query->result() as $rowd):
								$var= number_format($rowd->canti, 2, '.', ',');
								if($rowd->canti>0 && $ao='2013'){$rowT->d= $var;$totd+=$rowd->canti;}else{$rowT->d='';}
							endforeach;	
							$ao=$ao-1;
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->e='';
							foreach ($query->result() as $rowe):
								$var= number_format($rowe->canti, 2, '.', ',');
								if($rowe->canti>0 && $ao='2012'){$rowT->e= $var;$tote+=$rowe->canti;}else{$rowT->e='';}
							endforeach;	
							$ao=$ao-1;	
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->f='';
							foreach ($query->result() as $rowf):
								$var= number_format($rowf->canti, 2, '.', ',');
								if($rowf->canti>0 && $ao='2011'){$rowT->f= $var;$totf+=$rowf->canti;}else{$rowT->f='';}
							endforeach;	
							$ao=$ao-1;	
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->g='';
							foreach ($query->result() as $rowg):
								$var= number_format($rowg->canti, 2, '.', ',');
								if($rowg->canti>0 && $ao='2010'){$rowT->g= $var;$totg+=$rowg->canti;}else{$rowT->g='';}
							endforeach;	
							$ao=$ao-1;						
						
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->h='';
							foreach ($query->result() as $rowh):
								$var= number_format($rowh->canti, 2, '.', ',');
								if($rowh->canti>0 && $ao='2009'){$rowT->h= $var;$toth+=$rowh->canti;}else{$rowT->h='';}
							endforeach;	
							$ao=$ao-1;
							
							$query=$this->db->query("SELECT sum(ImporteD) as canti 
		  												From depositos 
		  												WHERE (depositos.Des=0 AND depositos.Car=0) and year(Fecha)=$ao AND month(Fecha)=$mes 
		  												GROUP BY month(Fecha)");
							$rowT->i='';
							foreach ($query->result() as $rowi):
								$var= number_format($rowi->canti, 2, '.', ',');
								if($rowi->canti>0 && $ao='2008'){$rowT->i= $var;$toti+=$rowi->canti;}else{$rowT->i='';}
							endforeach;	
							$ao=$ao-1;
							
						$data[] = $rowT;	
				endforeach;
				$mes=$mes+=1;
			}
			$this->db->select('max(RemisionR)');
			$resultZ = $this->db->get('r16');	
			foreach ($resultZ->result() as $rowT):
			$rowT->mes='Total:';
			$rowT->a='$'.number_format($tota, 2, '.', ',');
			$rowT->b='$'.number_format($totb, 2, '.', ',');
			$rowT->c='$'.number_format($totc, 2, '.', ',');
			$rowT->d='$'.number_format($totd, 2, '.', ',');
			$rowT->e='$'.number_format($tote, 2, '.', ',');
			$rowT->f='$'.number_format($totf, 2, '.', ',');
			$rowT->g='$'.number_format($totg, 2, '.', ',');
			$rowT->h='$'.number_format($toth, 2, '.', ',');
			$rowT->i='$'.number_format($toti, 2, '.', ',');
			$data[] = $rowT;
			endforeach;
			
			return $data;
		}
		
    }
    
?>