<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Viaticos_model extends CI_Model {
        public $id="NumDes";
		public $des="NomDes";
		public $uni="NumUniD";
		public $ali="Alimentacion";
		public $com="Combustible";
		public $cas="Casetas";
		public $hos="Hospedaje";
		public $fit="Fitosanitaria";
		public $obs="ObsDes";
		public $tabla="viaticos";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		public function agregar($des,$uni,$ali,$com,$cas,$hos,$fit,$obs){
			//$ali = str_replace(",", "", $ali);$com = str_replace(",", "", $com);$cas = str_replace(",", "", $cas);
			//$hos = str_replace(",", "", $hos);$fit = str_replace(",", "", $fit);$cg = str_replace(",", "", $cg);
			if($ali>0) $ali = str_replace(",", "", $ali); else $ali=0;
			if($com>0) $com = str_replace(",", "", $com); else $com=0;
			if($cas>0) $cas = str_replace(",", "", $cas); else $cas=0;
			if($hos>0) $hos = str_replace(",", "", $hos); else $hos=0;
			if($fit>0) $fit = str_replace(",", "", $fit); else $fit=0;					
			if($cg>0) $cg = str_replace(",", "", $cg); else $cg=0;
			$data=array($this->des=>$des,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->obs=>$obs);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$des,$uni,$ali,$com,$cas,$hos,$fit,$obs){
			if($ali>0) $ali = str_replace(",", "", $ali); else $ali=0;
			if($com>0) $com = str_replace(",", "", $com); else $com=0;
			if($cas>0) $cas = str_replace(",", "", $cas); else $cas=0;
			if($hos>0) $hos = str_replace(",", "", $hos); else $hos=0;
			if($fit>0) $fit = str_replace(",", "", $fit); else $fit=0;	
			/*$ali = str_replace(",", "", $ali);$com = str_replace(",", "", $com);$cas = str_replace(",", "", $cas);
			$hos = str_replace(",", "", $hos);$fit = str_replace(",", "", $fit);*/			
			$data=array($this->des=>$des,$this->uni=>$uni,$this->ali=>$ali,$this->com=>$com,$this->cas=>$cas,$this->hos=>$hos,$this->fit=>$fit,$this->obs=>$obs);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function borrar($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}	
		function getElements($where){
			if($where['NomDes']!='Todos'){			
				$this->db->select('NumUni,NomUni as val');
				$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
				//$this->db->where('NumDes >',0);
				$this->db->where('Activo =',0);	
				$this->db->group_by('NumUni');        
        		$this->db->where($where);
        		$result=$this->db->get('viaticos');
        		$data = array();        
        		foreach($result->result() as $row):
            		$data[] = $row;
        		endforeach;        
        		return $data;	
    		}
		}
		function getCargosVia($filter){			
			//select NumDes,NomDes,NomUni,Alimentacion,Combustible,Casetas,Hospedaje,Fitosanitaria from viaticos inner join unidad on NumUni=NumUniD where NumDes>0 order by NomDes,NomUn
			$this->db->join('unidad', 'numunid=numuni', 'inner');
			$this->db->where('NumDes >',0);
			$this->db->order_by('NomDes');
			$this->db->order_by('NomUni');
			if($filter['where']!='Todos') $this->db->where($filter['where']);			
				//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			/*if($filter['num']!=0){				
					$this->db->where($this->estatus,$filter['num']);				
			}*/
			//Se realiza la consulta con una limitación, en caso de que sea valida
			
			
			If($filter['limit']!=0)
				$result = $this->db->get('viaticos',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('viaticos');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $destino="";$uni="";
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$row->NomDesA=$row->NomDes;
				if($destino!=$row->NomDes){	$destino=$row->NomDes;}else{$row->NomDes="";}
				if($uni!=$row->NomUni){	$uni=$row->NomUni;}else{$row->NomUni="";}
				$row->totals=number_format(($row->Alimentacion+$row->Combustible+$row->Casetas+$row->Hospedaje+$row->Fitosanitaria), 2, '.', ',');
				if($row->Alimentacion>0){ $row->Alimentacion = number_format(($row->Alimentacion), 2, '.', ',');} else {$row->Alimentacion ="";}
				if($row->Combustible>0){ $row->Combustible = number_format(($row->Combustible), 2, '.', ',');} else {$row->Combustible ="";}
				if($row->Casetas>0){ $row->Casetas = number_format(($row->Casetas), 2, '.', ',');} else {$row->Casetas ="";}
				if($row->Hospedaje>0){ $row->Hospedaje = number_format(($row->Hospedaje), 2, '.', ',');} else {$row->Hospedaje ="";}
				if($row->Fitosanitaria>0){ $row->Fitosanitaria = number_format(($row->Fitosanitaria), 2, '.', ',');} else {$row->Fitosanitaria ="";}
					
				$data[] = $row;				
			endforeach;
			return $data;
		}
		
		function getNumRowsVia($filter){
			$this->db->join('unidad', 'numunid=numuni', 'inner');
			$this->db->where('NumDes >',0);
			$this->db->order_by('NomDes');
			$this->db->order_by('NomUni');
			if($filter['where']!='Todos')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			/*if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);*/
			$result = $this->db->get('viaticos');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function verDestino(){
			$this->db->select('NomDes');
			$this->db->where('numdes >',0);
			$this->db->group_by('NomDes'); 
			$query=$this->db->get('viaticos');
			return $query->result();			
		}	
		public function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		public function getViatico($id){
			$this->db->where($this->id,$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}			
    }    
?>