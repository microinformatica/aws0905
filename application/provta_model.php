<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Provta_model extends CI_Model {        
        public $idcos="idcos";public $feccos="feccos";public $estcos="estcos";public $grscos="grscos";public $kgscos="kgscos";
        public $numcos="numcos";public $clicos="clicos";public $tipcos="tipcos";public $prebas="prebas";
        public $tabla="bordo";
		
		public $tablasal="salidas";
		
		/*public $idcos="idcos";public $feccos="feccos";public $estcos="estcos";public $grscos="grscos";public $kgscos="kgscos";
        public $numcos="numcos";public $clicos="clicos";public $tipcos="tipcos";public $prebas="prebas";public $numgrab="numgrab";public $folio="folio";
        public $tablab="bordo";
		*/
		public $idpis="idpis";public $pisg="pisg";
		public $tablaest="siegra";		
		
		public $idpischa="idpischa";public $ciccha="ciccha";
		public $tablacha="aligracha_";
		
		public $tablafac="fac_oa_";public $tablaalm="almsal_";public $tablagrs="gramos";
		
		public $idpre="idpre";public $fecpre="fecpre";public $idgrsp="idgrsp";public $pcc="pcc";
		public $tablagrspre="gramospre";
		
		public $iddie="iddie";public $fecdie="fecdie";public $facdie="facdie";public $candie="candie";public $predie="predie";
		public $impdie="impdie";public $tipdie="tipdie";public $cicdie="cicdie";
		public $tabladie="fac_oad";
		
		
		public $fecb="fecb";
		public $tablabio="biogra_"; public $tablasob="sobgra_";
		
		public $idsem="idsem";public $numsem="numsem";public $fecsem="fecsem";public $impsem="impsem";public $obssem="obssem";
		public $cicsem="cicsem";
		public $tablasue="sueldos";
		
		public $idotc="idotc";public $fecotc="fecotc";public $impotc="impotc";public $obsotc="obsotc";public $cicotc="cicotc";
		public $tablaotc="otroscostos";
				
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getElementsb($filter){        
        	$this->db->select("idt,nomt as val");
			$this->db->join('tallas', 'idt=talsal', 'inner');	
			$this->db->where('ciclo','20'.$filter['ciclo']);
			$this->db->where('estsal',1);
			$this->db->group_by('idt');
			$this->db->order_by('sum(kgssal)','DESC');
	        $result = $this->db->get('salidas');       
        	$datab = array();
			if($result->num_rows()>0){        
        	foreach($result->result() as $rowc):
            	$datab[] = $rowc;
        	endforeach;
			}        
        	return $datab;
    	}
		function prevtar($filter){
		//SELECT nomt from salidas inner join tallas on idt=talsal where estsal=1 group by talsal
			$this->db->select('talsal,nomt');
			$this->db->join('tallas', 'idt=talsal','inner');
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->where('estsal',1);
			$this->db->group_by('talsal');$this->db->group_by('nomt');
			$this->db->order_by('grupo');$this->db->order_by('nomt');
			$result = $this->db->get($this->tablasal);
			$data = array();
			if($result->num_rows()>0){
			    $totalm=6;$cont=1;
			foreach ($result->result() as $row):
				$row->min1='';$row->max1='';$row->pro1='';$row->min2='';$row->max2='';$row->pro2='';$row->min3='';$row->max3='';$row->pro3='';
				$row->min4='';$row->max4='';$row->pro4='';$row->min5='';$row->max5='';$row->pro5='';$row->min6='';$row->max6='';$row->pro6='';
				while($cont<=$totalm){
					switch ($cont) {
						case '1': $alm=1;break;
						case '2': $alm=2;break;
						case '3': $alm=4;break;
						case '4': $alm=6;break;
						case '5': $alm=7;break;
						case '6': $alm=8;break;
						
					}
					//SELECT almsal,nomt,min(prekgs*tcv) as minp, max(prekgs*tcv) as maxp, avg(prekgs*tcv) as prop 
					//from salidas inner join tallas on idt=talsal where talsal=5 and estsal=1 and prekgs>0  group by almsal
					$this->db->select('almsal,min(prekgs*tcv) as minp, max(prekgs*tcv) as maxp, avg(prekgs*tcv) as prop');
					$this->db->where('estsal',1);$this->db->where('prekgs >',0);
					$this->db->where('talsal',$row->talsal);	
					$this->db->where('almsal',$alm);
					$resultt = $this->db->get($this->tablasal);
					foreach ($resultt->result() as $rowt):
						if($rowt->minp>0){
						switch ($alm) {
							case '1': $row->min1='$'.number_format($rowt->minp, 2, '.', ',');$row->max1='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro1='$'.number_format($rowt->prop, 2, '.', ',');break;
							case '2': $row->min2='$'.number_format($rowt->minp, 2, '.', ',');$row->max2='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro2='$'.number_format($rowt->prop, 2, '.', ',');break;
							case '4': $row->min3='$'.number_format($rowt->minp, 2, '.', ',');$row->max3='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro3='$'.number_format($rowt->prop, 2, '.', ',');break;
							case '6': $row->min4='$'.number_format($rowt->minp, 2, '.', ',');$row->max4='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro4='$'.number_format($rowt->prop, 2, '.', ',');break;
							case '7': $row->min5='$'.number_format($rowt->minp, 2, '.', ',');$row->max5='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro5='$'.number_format($rowt->prop, 2, '.', ',');break;
							case '8': $row->min6='$'.number_format($rowt->minp, 2, '.', ',');$row->max6='$'.number_format($rowt->maxp, 2, '.', ',');$row->pro6='$'.number_format($rowt->prop, 2, '.', ',');break;
						
						}
						}
					endforeach;	
					$cont+=1;
				}	
				$data[] = $row;	
				$cont=1;
			endforeach;
			
			}     
        	return $data;
    	}
        function verGramos(){
        	$this->db->select('idgrsp');
			$this->db->group_by('idgrsp');
			$query=$this->db->get($this->tablagrspre);
			return $query->result();			
		}
		
		function grsmes($filter){
			//obtner los gramos que tienen precio
			$datos=array();
			$datos1=array();
			$this->db->select('idgrsp');
			$this->db->group_by('idgrsp');
			$result = $this->db->get($this->tablagrspre);$cont=1;$contt=0;
			foreach ($result->result() as $row):	
				$datos1[$cont] =array($cont,$row->idgrsp); $cont+=1;$contt+=1;
			endforeach;
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->group_by('fecpre');
			$this->db->order_by('fecpre','DESC');
			$result = $this->db->get($this->tablagrspre);
			$data = array();
			if($result->num_rows()>0){
			    $fec=new Libreria();	
				foreach ($result->result() as $row):	
				$row->dia = $fec->fecha($row->fecpre);$dias=$row->fecpre;
				$cont=1;while ($cont <= 33) {  $datos[$cont] =array('','',''); $cont+=1;	} 	
				//busca todos precios de las tallas de acuerdo al dia
				$cont2=1;
				$this->db->where($this->fecpre,$dias);
				$resultd = $this->db->get($this->tablagrspre);
				foreach ($resultd->result() as $rowd):
					$datos[$cont2] =array($cont2,$rowd->idgrsp,$rowd->pcc);	$cont2+=1;
				endforeach;
				$ini=1;$ini2=1;
					while($ini<=$contt){
						switch($ini){
							case 1: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d1 =$datos [$ini2][2];$ini2+=1;} else {$row->d1 ='';} break;
							case 2: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d2 =$datos [$ini2][2];$ini2+=1;} else {$row->d2 ='';} break;
							case 3: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d3 =$datos [$ini2][2];$ini2+=1;} else {$row->d3 ='';} break;
							case 4: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d4 =$datos [$ini2][2];$ini2+=1;} else {$row->d4 ='';} break;
							case 5: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d5 =$datos [$ini2][2];$ini2+=1;} else {$row->d5 ='';} break;
							case 6: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d6 =$datos [$ini2][2];$ini2+=1;} else {$row->d6 ='';} break;
							case 7: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d7 =$datos [$ini2][2];$ini2+=1;} else {$row->d7 ='';} break;
							case 8: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d8 =$datos [$ini2][2];$ini2+=1;} else {$row->d8 ='';} break;
							case 9: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d9 =$datos [$ini2][2];$ini2+=1;} else {$row->d9 ='';} break;
							case 10: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d10 =$datos [$ini2][2];$ini2+=1;} else {$row->d10 ='';} break;
							case 11: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d11 =$datos [$ini2][2];$ini2+=1;} else {$row->d11 ='';} break;
							case 12: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d12 =$datos [$ini2][2];$ini2+=1;} else {$row->d12 ='';} break;
							case 13: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d13 =$datos [$ini2][2];$ini2+=1;} else {$row->d13 ='';} break;
							case 14: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d14 =$datos [$ini2][2];$ini2+=1;} else {$row->d14 ='';} break;
							case 15: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d15 =$datos [$ini2][2];$ini2+=1;} else {$row->d15 ='';} break;
							case 16: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d16 =$datos [$ini2][2];$ini2+=1;} else {$row->d16 ='';} break;
							case 17: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d17 =$datos [$ini2][2];$ini2+=1;} else {$row->d17 ='';} break;
							case 18: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d18 =$datos [$ini2][2];$ini2+=1;} else {$row->d18 ='';} break;
							case 19: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d19 =$datos [$ini2][2];$ini2+=1;} else {$row->d19 ='';} break;
							case 20: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d20 =$datos [$ini2][2];$ini2+=1;} else {$row->d20 ='';} break;
							case 21: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d21 =$datos [$ini2][2];$ini2+=1;} else {$row->d21 ='';} break;
							case 22: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d22 =$datos [$ini2][2];$ini2+=1;} else {$row->d22 ='';} break;
							case 23: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d23 =$datos [$ini2][2];$ini2+=1;} else {$row->d23 ='';} break;
							case 24: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d24 =$datos [$ini2][2];$ini2+=1;} else {$row->d24 ='';} break;
							case 25: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d25 =$datos [$ini2][2];$ini2+=1;} else {$row->d25 ='';} break;
							case 26: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d26 =$datos [$ini2][2];$ini2+=1;} else {$row->d26 ='';} break;
							case 27: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d27 =$datos [$ini2][2];$ini2+=1;} else {$row->d27 ='';} break;
							case 28: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d28 =$datos [$ini2][2];$ini2+=1;} else {$row->d28 ='';} break;
							case 29: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d29 =$datos [$ini2][2];$ini2+=1;} else {$row->d29 ='';} break;
							case 30: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d30 =$datos [$ini2][2];$ini2+=1;} else {$row->d30 ='';} break;
							case 31: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d31 =$datos [$ini2][2];$ini2+=1;} else {$row->d31 ='';} break;
							case 32: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d32 =$datos [$ini2][2];$ini2+=1;} else {$row->d32 ='';} break;
							case 33: if($datos [$ini2][1]==$datos1 [$ini][1]){ $row->d33 =$datos [$ini2][2];$ini2+=1;} else {$row->d33 ='';} break;
						}
						$ini+=1;
					}	
				$data[] = $row;	
				endforeach;
			}     
        	return $data;
    	}
		public function actualizarotc($id,$cic,$fec,$imp,$obs){
			$imp = str_replace(",", "", $imp);
			$data=array($this->cicotc=>$cic,$this->fecotc=>$fec,$this->impotc=>$imp,$this->obsotc=>$obs);
			$this->db->where($this->idotc,$id);
			$this->db->update($this->tablaotc,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarotc($cic,$fec,$imp,$obs){
			$imp = str_replace(",", "", $imp);
			$data=array($this->cicotc=>$cic,$this->fecotc=>$fec,$this->impotc=>$imp,$this->obsotc=>$obs);		
			$this->db->insert($this->tablaotc,$data);
			return $this->db->insert_id();
		}
		
		
		
		function otroscostos($filter){        
        	$this->db->select('sum(impotc) as impotc');	
			if($filter['where']!='') $this->db->where($filter['where']);
			$resulto = $this->db->get($this->tablaotc);
			$data = array();
			if($resulto->num_rows()>0){
			    $fec=new Libreria();	
			foreach ($resulto->result() as $row):				
				$row->fecotc1='Total';$row->obsotc = "";
				if($row->impotc>0)$row->impotc ='$ '. number_format($row->impotc, 2, '.', ','); else $row->impotc ='';
				$data[] = $row;	
			endforeach;
			
			$this->db->order_by('fecotc','DESC');
			if($filter['where']!='') $this->db->where($filter['where']);  
			$result = $this->db->get($this->tablaotc);
			foreach($result->result() as $row):
				$row->fecotc1 = $fec->fecha($row->fecotc);
        		$row->impotc= number_format($row->impotc, 2, '.', ',');
				if($row->obsotc=='')$row->obsotc='';
            	$data[] = $row;
        	endforeach;  
			}     
        	return $data;
    	}
		
		
		public function actualizarpre($id,$fec,$grs,$pcc){
			$data=array($this->fecpre=>$fec,$this->idgrsp=>$grs,$this->pcc=>$pcc);
			$this->db->where($this->idpre,$id);
			$this->db->update($this->tablagrspre,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarpre($fec,$grs,$pcc){
			$data=array($this->fecpre=>$fec,$this->idgrsp=>$grs,$this->pcc=>$pcc);		
			$this->db->insert($this->tablagrspre,$data);
			return $this->db->insert_id();
		}
		
		function prevta($filter){
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablagrspre);
			$data = array();
			if($result->num_rows()>0){
			    $fec=new Libreria();	
			foreach ($result->result() as $row):	
				$row->fecpre1 = $fec->fecha($row->fecpre);	
				$row->pcc1 ='$ '.number_format($row->pcc, 2, '.', ',');		
				$row->grspre ='$ '.number_format($row->pcc+$row->idgrsp, 2, '.', ',');
				$data[] = $row;	
			endforeach;
			
			}     
        	return $data;
    	}
		function prevtah($filter){
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecpre','DESC');
			$result = $this->db->get($this->tablagrspre);
			$data = array();
			if($result->num_rows()>0){
			    $fec=new Libreria();	
			foreach ($result->result() as $row):	
				$row->fecpre1h = $fec->fecha($row->fecpre);	
				$row->pcc1h ='$ '.number_format($row->pcc, 2, '.', ',');		
				$row->grspreh ='$ '.number_format($row->pcc+$row->idgrsp, 2, '.', ',');
				$data[] = $row;	
			endforeach;
			
			}     
        	return $data;
    	}
		public function quitar($id,$tab,$cam,$ciclo){
			$this->db->where($cam,$id);
			if($tab!='gramospre' && $tab!='otroscostos') $this->db->delete($tab.'_'.$ciclo); else $this->db->delete($tab);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		public function actualizardie($id,$fec,$fac,$lts,$pre,$tip,$cic){
			$lts = str_replace(",", "", $lts);
			$data=array($this->fecdie=>$fec,$this->facdie=>$fac,$this->candie=>$lts,$this->predie=>$pre,$this->tipdie=>$tip,$this->cicdie=>$cic);
			$this->db->where($this->iddie,$id);
			$this->db->update($this->tabladie,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregardie($fec,$fac,$lts,$pre,$tip,$cic){
			$lts = str_replace(",", "", $lts);
			$data=array($this->fecdie=>$fec,$this->facdie=>$fac,$this->candie=>$lts,$this->predie=>$pre,$this->tipdie=>$tip,$this->cicdie=>$cic);		
			$this->db->insert($this->tabladie,$data);
			return $this->db->insert_id();
		}
		
		function combustible($filter,$cic){
			$query=$this->db->query("SELECT (select sum(candie) from $this->tabladie where tipdie=1 and cicdie=$cic)as lts, (select sum(candie*predie) from $this->tabladie where tipdie=1 and cicdie=$cic)as fac, (SELECT sum(candie*predie) from $this->tabladie  where tipdie=2 and cicdie=$cic)as nc from $this->tabladie  limit 1");
			//if($filter['where']!='') $query=$query.$this->db->where($filter['where']);
			//$this->db->select('sum(candie*predie) as impdie');	
			//$result = $this->db->get($this->tabladie);
			if($query->num_rows()>0){
			$data = array();    $fec=new Libreria();	
			foreach ($query->result() as $row):				
				$row->tipdie1 = "";$row->facdie = "";$row->fecdie1='Total';
				if($row->lts>0) $row->predie1 ='$ '.number_format(($row->fac-$row->nc)/$row->lts, 6, '.', ','); else $row->predie1 ='';
				$row->candie =number_format($row->lts, 3, '.', ',');
				if($row->fac>0 || $row->nc>0)$row->impdie ='$ '. number_format($row->fac-$row->nc, 2, '.', ','); else $row->impdie ='';
				$data[] = $row;	
			endforeach;
			if($filter['where']!='') $this->db->where($filter['where']);
			$this->db->order_by('fecdie','DESC');
			$this->db->order_by('tipdie','DESC');  
			$result = $this->db->get($this->tabladie);
			foreach($result->result() as $row):
				$row->fecdie1 = $fec->fecha($row->fecdie);
				
				if($row->tipdie==1){
					$row->tipdie1='Factura';
					$row->impdie='$ '. number_format(($row->candie*$row->predie), 2, '.', ',');
					$row->predie1='$'. number_format($row->predie, 6, '.', ',');
					$row->candie =number_format($row->candie, 3, '.', ',');
				 }else {
				 	$row->tipdie1='Nota de Crédito';
				 	$row->impdie='$-'. number_format(($row->candie*$row->predie), 2, '.', ',');
					$row->predie1='';
				 	$row->candie ='';
				 }
        		$data[] = $row;
        	endforeach;  
			}     
        	return $data;
    	}
		
		public function actualizarsue($id,$num,$fec,$imp,$obs,$cic){
			$imp = str_replace(",", "", $imp);
			$data=array($this->numsem=>$num,$this->fecsem=>$fec,$this->impsem=>$imp,$this->obssem=>$obs,$this->cicsem=>$cic);
			$this->db->where($this->idsem,$id);
			$this->db->update($this->tablasue,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarsue($num,$fec,$imp,$obs,$cic){
			$imp = str_replace(",", "", $imp);
			$data=array($this->numsem=>$num,$this->fecsem=>$fec,$this->impsem=>$imp,$this->obssem=>$obs,$this->cicsem=>$cic);		
			$this->db->insert($this->tablasue,$data);
			return $this->db->insert_id();
		}
		
		
		
		function sueldos($filter){        
        	
			$this->db->select('sum(impsem) as impsem');	
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablasue);
			if($result->num_rows()>0){
			$data = array();    $fec=new Libreria();	
			foreach ($result->result() as $row):				
				$row->numsem = "";$row->fecsem1='Total';$row->obssem = "";
				if($row->impsem>0)$row->impsem ='$ '. number_format($row->impsem, 2, '.', ','); else $row->impsem ='';
				$data[] = $row;	
			endforeach;
			$this->db->order_by('numsem','DESC');  
			if($filter['where']!='') $this->db->where($filter['where']);
			$result = $this->db->get($this->tablasue);
			
        	foreach($result->result() as $row):
				$row->fecsem1 = $fec->fecha($row->fecsem);
        		$row->impsem= number_format($row->impsem, 2, '.', ',');
				if($row->obssem=='')$row->obssem='';
            	$data[] = $row;
        	endforeach;  
			}     
        	return $data;
    	}
		
		function secciones1($where){        
        	$this->db->select("secc,secc as val");     
			if($where['numgra']!=0){$this->db->where('numgra',$where['numgra']);}
	        $this->db->where('secc >',0);
			$this->db->group_by('secc');$this->db->group_by('val');
			$result = $this->db->get($this->tablaest.'_'.$cic);
        	$data = array();        
        	foreach($result->result() as $row):
        		switch($row->secc){
					case 21: $row->val='1'; break;	case 22: $row->val='2'; break;
					case 41: $row->val='1'; break;	case 42: $row->val='2'; break;	
					case 43: $row->val='3'; break;	case 44: $row->val='4'; break;
				}
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		
		function secciones($filest){        
			$ano=substr($filest, 2,2);
			$ciclo=substr($filest, 5,1);
			$granja=substr($filest, 6,1);
        	$this->db->select("secc,secc as val");     
			$this->db->where('numgra',$granja);
			if($ciclo!=0){$this->db->where('cicg','20'.$ano.'-'.$ciclo);}
	        $this->db->where('secc >',0);$this->db->group_by('secc');
	        //$this->db->order_by('cicg');$this->db->order_by('pisg');
			$result = $this->db->get($this->tablaest.'_'.$ano);
        	$data = array();        
        	foreach($result->result() as $row):
				switch($row->secc){
					case 21: $row->val='1'; break;	case 22: $row->val='2'; break;
					case 41: $row->val='1'; break;	case 42: $row->val='2'; break;	
					case 43: $row->val='3'; break;	case 44: $row->val='4'; break;
				}
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		
		function gral($filter,$cic,$cica,$tc,$gra){
			$cicloact=substr($cica, -1);
			//SELECT	pisg,orgg,hasg,sum(kgscos) as kgs, avg(grscos) as pp from siegra_18 inner join bordo on estcos=idpis where numcos=1 group by estcos
			$this->db->select("idpis,pisg,orgg,hasg,fecg,fecgc");
			//$this->db->join($this->tablaest, $this->idpis.'='.$this->estcos,'inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->group_by('pisg');      
			//$this->db->group_by('pisg');		
			$result = $this->db->get($this->tablaest.'_'.$cic);
			//$result = $this->db->get($this->tablaest);
			$data = array(); $totorg=0;$tothas=0;$totali=0;$totpri=0;$totseg=0;$totter=0;$totcua=0;$totfin=0;$tottot=0;$totpro=0;$cont=0;$totorgs=0;$dsi=0;
			$totvta=0;$totorgst=0;$totdep=0;$totalidiec=0;
			$totppls=0;$totaliali=0;$totaliimpa=0;$totalidie=0;$totaliimpd=0;$totalisue=0;$totalikgs=0;$totalicos=0;$totalivta=0;$totaliotc=0;
			$tokvtar=0;$tokimpr=0;$costousd=0;$hastot=0;$sueimp=0;$esttot=0;
			if($result->num_rows()>0){
				$hoy=date("Y-m-d");$rowhasg=0;
				
				//todo las hectareas sembradas
				$this->db->select('sum(hasg) as tothas,count(*) as totest');
				//if($filter['where']!='') $this->db->where($filter['where']);		
				$this->db->where('numgra',$gra);
				$resulthas = $this->db->get($this->tablaest.'_'.$cic);
				foreach ($resulthas->result() as $rowhas):				
					$hastot=$rowhas->tothas; $esttot=$rowhas->totest; 
				endforeach;
				//saco los dias que van del año
				$diahoy= new DateTime($hoy);
				$diaini = new DateTime('20'.$cic.'-01-01'); 
				$resultadoc = $diahoy->diff($diaini);
				$diasalhoy=$resultadoc->format('%a');$diasalhoy1=$resultadoc->format('%a');
				$diasalhoy=$diasalhoy*$esttot;
				//todo lo pagado en sueldo
				$this->db->select('max(fecsem) as fecsem,sum(impsem) as impsem');
				$this->db->where('cicsem',$cic);	
				$resulta = $this->db->get($this->tablasue);
				foreach ($resulta->result() as $rowsue):				
					$sueimp=$rowsue->impsem; $fecsue=$rowsue->fecsem;
				endforeach;
				//sacar todo lo que se ha pagdo de alimento
				$this->db->select("tipag,imppag,tcpag,tonag");
				$this->db->where('idgag',4);
				$result1 = $this->db->get($this->tablafac.$cic);
				$cosali=0;$totalicosali=0;$totaliton=0;
				foreach($result1->result() as $row1):
					$cosali=0;
					if($row1->tipag==2) $cosali+=$row1->imppag*$row1->tcpag; else $cosali+=$row1->imppag;
					$totalicosali+=$cosali;$totaliton+=$row1->tonag;
				endforeach;
				//el precio por kilo promedio seria entonces
				$ppkgs= number_format((($totalicosali/$totaliton)/1000), 2, '.', ',');
				
				//sacar todo lo que se ha pagdo de diesel
				//$this->db->select("tipag,imppag,tcpag,tonag");
				//$this->db->where('idgag',4);
				$this->db->where('cicdie',$cic);	
				$result1 = $this->db->get($this->tabladie);
				$cosdie=0;$totalidie=0;$totaliton=0;
				foreach($result1->result() as $row1):
					$cosali=0;
					if($row1->tipdie==1){$cosdie+=$row1->candie*$row1->predie;$totalidie+=$row1->candie;} else {$cosdie-=$row1->candie*$row1->predie;}
					//$totalicosali+=$cosali;$totaliton+=$row1->tonag;
				endforeach;
				//el precio por lts promedio seria entonces
				if($totalidie>0) $ppdie= number_format((($cosdie/$totalidie)), 6, '.', ','); else $ppdie=0;	
				$totalidie=0;	

				//OTROS
				$impotc=0;
				$this->db->select("sum(impotc) as impotc");
				$this->db->where('cicotc',$cic);	
				$resulti = $this->db->get($this->tablaotc);
				foreach($resulti->result() as $rowi):
					//$totali+=$row1->kgsali;
					if($rowi->impotc>0){
						$impotc=$rowi->impotc;
					}	
				endforeach;			
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$est=$row->pisg;$orgg=$row->orgg;$totorg+=$row->orgg;$tothas+=$row->hasg;$fecsalida=$row->fecgc;
				$estanque=$row->pisg;
				
				$dsi=$row->fecg;
				$org1=0;$org2=0;$org3=0;$org4=0;$orgf=0;$costo=0;
				//if($row->idpis>=1 && $row->idpis<=28) $row->pisg='K-I-'.$row->pisg;
				//  elseif($row->idpis>=29 && $row->idpis<=56) $row->pisg='K-II-'.$row->pisg;
					//elseif($row->idpis>=73 && $row->idpis<=99) $row->pisg='20-30-'.$row->pisg;
					 // elseif($row->idpis>=112 && $row->idpis<=128) $row->pisg='21-25-'.$row->pisg;
					//	elseif(($row->idpis>=100 && $row->idpis<=111) || ($row->idpis>=129 && $row->idpis<=149)) $row->pisg='DC-'.$row->pisg;
				$row->hasg = number_format($row->hasg, 3, '.', ',');$rowhasg = $row->hasg;
				//dias de cultivos actuales
				if(is_null($row->fecgc)) $diaact=date("Y-m-d"); else $diaact=$row->fecgc;
				$fecha1 = new DateTime($dsi);
    			$fecha2 = new DateTime($diaact);
    			$resultado = $fecha1->diff($fecha2);
    			$row->dc=$resultado->format('%a');
				$diasdecultivo=$resultado->format('%a');
				//Postlarvas
				//$pre=88.36; //precio proporcionado por Salvador el día 23-may-19.
				$pre=80.00; //precio proporcionado por Salvador el día 13-sep-21. $4 usd el millar. tc $20 mn
				$totppls+=$row->orgg*$pre;$totalicos+=$row->orgg*$pre;$costo=$row->orgg*$pre;
				$row->prepls ='$'. number_format($pre, 2, '.', ',');
				$row->imppls ='$'. number_format(($row->orgg*$pre), 2, '.', ',');
				$row->orgg = number_format($row->orgg, 3, '.', ',');
				
				//Sueldos
				$diastotales=0;$diacostod=0;$diasciclo=0;$diaactc=0;
				if($sueimp>0){
					//dias actuales del ciclo ciclo 1
					
					//$diaactc= new DateTime(date("Y-m-d"));
					$diaactc= new DateTime($fecsue);
					$fecha1c = new DateTime('20'.$cic.'-01-01'); 
					$resultadoc = $diaactc->diff($fecha1c);
					$diastotales=$resultadoc->format('%a');
					//$diacosto=number_format((($sueimp/415.850)*$row->hasg)/$diastotales, 2, '.', ',');
					//$diacosto=number_format((($sueimp/$hastot)*$row->hasg)/$diastotales, 2, '.', ',');
					
					
					
					if($cica=='20'.$cic.'-2'){$fecha1c = new DateTime($row->fecg); }
					if($cica=='20'.$cic.'-1' && $row->fecgc!=''){
						if($row->fecgc>$fecsue) {$fecha1c = new DateTime('20'.$cic.'-01-01');$fecha2c = new DateTime($fecsue);} 
						else{ $fecha1c = new DateTime($row->fecg);$fecha2c = new DateTime($row->fecgc);}

					} else {$fecha2c = new DateTime($hoy);} 
    				$resultadoc= $fecha2c->diff($fecha1c);
					$diasciclo=$resultadoc->format('%a');	//$diasciclo+=1;
					
					$pordias=($diasdecultivo/$diasalhoy);
					
					//$row->impsue =$diasciclo;
					$totalisue+=$sueimp*$pordias;
					$totalicos+=$sueimp*$pordias; //$totalicos+=($sueimp/415.850)*$row->hasg;
					$costo+=$sueimp*$pordias; //$costo+=($sueimp/415.850)*$row->hasg;
					//$row->impsue ='$'. number_format($diacosto, 2, '.', ',');

					$row->impsue ='$'.number_format(($sueimp*$pordias),2,'.',',');
					//$totalisue+=($sueimp/415.850)*$row->hasg;
					
				} else {$row->impsue ='';}
				
				//ALIMENTO
				//sumar todos los kg actuales por est
				$this->db->select("sum(kgd) as  kgsali");
				$this->db->where($this->idpischa,$row->pisg);
				$this->db->where($this->ciccha,$cica);
				$result1 = $this->db->get($this->tablacha.$cic);
				foreach($result1->result() as $row1):
					$totali+=$row1->kgsali;
					if($row1->kgsali>0){
						$totaliali+=$row1->kgsali;$totaliimpa+=$row1->kgsali*$ppkgs;$totalicos+=$row1->kgsali*$ppkgs;$costo+=$row1->kgsali*$ppkgs;
						$row->impali ='$'. number_format($row1->kgsali*$ppkgs, 2, '.', ',');	
						$row->kgsali =number_format($row1->kgsali, 0, '.', ',');}else{$row->kgsali ='';$row->impali ='';}
				endforeach;
				//Precio
				$row->preali ='$'. number_format($ppkgs, 2, '.', ',');
				
				if($fecsalida=='') $fecsalida=date("Y-m-d");
				//DIESEL
				$row->ltscom ='';
				//SELECT sum(CanS) FROM almsal_19 WHERE CBS LIKE 'diesel%';
				$this->db->select("sum(CanS) as ltscom");
				$this->db->where('CBS LIKE','diesel%');
				//$this->db->where('NDepS=',$estanque);
				/*if($cicloact=='1'){
					$this->db->where('FecS <=',$fecsalida);
				}else{
					$this->db->where('FecS >=',$fecsalida);
				}*/
				$result1 = $this->db->get($this->tablaalm.$cic);
				foreach($result1->result() as $row1):
					//$totali+=$row1->kgsali;
					if($row1->ltscom>0){
						//total de diesel	
						if($diastotales>0) $diacostod=(($row1->ltscom/415.850)*$row->hasg)/$diastotales; 
						//$totalidie+=$diasciclo*$diacostod;			
						$totalidiec+=$diasciclo*$diacostod;
						$totalidie+=($row1->ltscom/415.850)*$row->hasg;

						$totaliimpd+=($row1->ltscom*$pordias)*$ppdie; //$totaliimpd+=($row1->ltscom/415.850)*$row->hasg*$ppdie;
						$totalicos+=(($row1->ltscom*$pordias)*$ppdie); //$totalicos+=($row1->ltscom/415.850)*$row->hasg*$ppdie;
						$costo+=($row1->ltscom*$pordias)*$ppdie;//$costo+=($row1->ltscom/415.850)*$row->hasg*$ppdie;
						//$row->pisg=$row->pisg.$diacostod;
						if(($diasciclo*$diacostod)*$ppdie>0)
						$row->impcom ='$'. number_format(($row1->ltscom*$pordias)*$ppdie, 2, '.', ',');	
						else $row->impcom ='';
						//$row->impcom ='$'. number_format(($row1->ltscom/415.850)*$row->hasg*$ppdie, 2, '.', ',');
						//$row->impcom =$ppdie;
						if($diasciclo*$diacostod>0) $row->ltscom =number_format((($row1->ltscom*$pordias)*$ppdie)/$ppdie, 0, '.', ',');}
						//$row->ltscom =number_format(($row1->ltscom/415.850)*$row->hasg, 0, '.', ',');}
					else{$row->ltscom ='';}
				endforeach;
				//OTROS
				$row->impotc ='';$otrdiaest=0;
				if($impotc>0){
					$otrdiaest=(($impotc/415.850)*$row->hasg)/$diastotales;
					$row->impotc ='$'. number_format($otrdiaest*$diasciclo, 2, '.', ',');
					//$row->impotc ='$'. number_format(($row1->impotc/415.850)*$row->hasg, 2, '.', ',');
					$totaliotc+=$otrdiaest*$diasciclo; //$totaliotc+=($row1->impotc/415.850)*$row->hasg;
					$costo+=$otrdiaest*$diasciclo; //$costo+=($row1->impotc/415.850)*$row->hasg;
					$totalicos+=$otrdiaest*$diasciclo; //$totalicos+=($row1->impotc/415.850)*$row->hasg;
				}	
				//DEPRECIACION
				$tcdep=19.30;
				//$tcdep=$tc;
				
				$depdiaest=0;
				if($diastotales>0) //$depdiaest=number_format(($row->hasg*(1000*$tcdep))/$diastotales, 2, '.', ',');
				$depdiaest=($row->hasg*(1000*$tcdep))/$diasalhoy1;
				if(($depdiaest*$diasciclo)>0) $row->impdep ='$'. number_format($depdiaest*$row->dc, 2, '.', ','); 
				else $row->impdep ='';
				//$row->impdep =$diasciclo;
				$totdep+=$depdiaest*$row->dc;
				$costo+=$depdiaest*$row->dc;
				$totalicos+=$depdiaest*$row->dc;
				/*
				$tcdep=19.30;$depdiaest=0;
				if($diastotales>0){ $depdiaest=($row->hasg*(1000*$tcdep));}
				if($depdiaest*$diasdecultivo>0) $row->impdep ='$'. number_format($depdiaest/$diasdecultivo, 2, '.', ','); else $row->impdep ='';
				$totdep+=$depdiaest/$diasdecultivo;
				*/
				//$row->impdep ='$'. number_format($row->hasg*(1000*$tcdep), 2, '.', ',');
				//$totdep+=$row->hasg*(1000*$tcdep);
				//$costo+=$row->hasg*(1000*$tcdep);
				//$totalicos+=$row->hasg*(1000*$tcdep);
				//Precio
				$row->precom ='$'. number_format($ppdie, 6, '.', ',');
				//costo
				$row->costousd ='';
				if($costo>0){$row->costo ='$'.number_format($costo, 2, '.', ',');
					if($tc>0){$row->costousd ='$'.number_format($costo/$tc, 2, '.', ',');$costousd+=($costo/$tc);}
				}else{ $row->costo ='';}
				//Peso promedio mas reciente
				$row->ppkgs ='';$peso=0;
				$query=$this->db->query("select pesb from ".$this->tablabio.$cic." where fecb<= '$hoy' and idpisb= '$est' and cicb='$cica' order by fecb Desc limit 1");
				foreach($query->result() as $row1):
					$peso=$row1->pesb;
					$row->ppkgs =number_format($row1->pesb, 2, '.', ',');	
				endforeach;
				//Sobrevivencia mas reciente
				$row->kgskgs ='';$ventakgs=0;
				$query=$this->db->query("select sobp from ".$this->tablasob.$cic." where fecs<= '$hoy' and idpiss='$est' order by fecs Desc limit 1");
				foreach($query->result() as $row2):
						$totalikgs+=($orgg*$peso*$row2->sobp)/100;$ventakgs=number_format(($orgg*$peso*$row2->sobp)/100, 0, '.', '');	
						if(($orgg*$peso*$row2->sobp)/100>0)$row->kgskgs =number_format(($orgg*$peso*$row2->sobp)/100, 0, '.', ',');	
				endforeach;
				//Precio de venta actual en el mercado y su talla de acuerdo a los gramos actuales del estanque.
				//buscar la talla
				$row->tsc ='';$row->tcc ='';
				$queryt=$this->db->query("select tsc,tcc from $this->tablagrs where '$peso'>=grs");
				foreach($queryt->result() as $rowt):
						$row->tsc =$rowt->tsc;	
						$row->tcc =$rowt->tcc;
				endforeach;
				//busca los registros de venta en bordo
				//SELECT estcos,sum(kgscos) as kgsvta, sum(kgscos*(grscos+prebas))as impvta from bordo_19 where numgrab=4 and estcos=73 
				$row->kgsbor='';$row->vtabor='';$vtabor=0;
					//$querybor=$this->db->query("select sum(kgscos) as kgsbor, sum(kgscos*(grscos+prebas))as vtabor from bordo_$cic where numgrab= '4' and estcos='$est'");
				$querybor=$this->db->query("SELECT sum(kgscos) as kgsbor,sum((SELECT (idgrsp+pcc)as prevta from gramospre where idgrsp>=truncate(grscos,0) and idgrsp<(truncate(grscos,0)+1) and fecpre<=feccos order by fecpre DESC limit 1))as prevta,sum((kgscos*(SELECT (idgrsp+pcc)as prevta from gramospre where idgrsp>=truncate(grscos,0) and idgrsp<(truncate(grscos,0)+1) and fecpre<=feccos order by fecpre DESC limit 1)))as vtabor from cosechas_$cic where estcos='$est' and ciccos='$cica'");	
				foreach($querybor->result() as $rowbor):
					$vtabor=$rowbor->vtabor;
					if($rowbor->kgsbor>0) $row->kgsbor =number_format($rowbor->kgsbor, 0, '.', ',');	
					if($rowbor->vtabor>0)  $row->vtabor ='$'.number_format($rowbor->vtabor, 2, '.', ',');
					$tokvtar+=$rowbor->kgsbor;$tokimpr+=$rowbor->vtabor;
				endforeach;
				//buscar la talla
				//$row->prekgssc =''$row->impkgssc ='';
				//$row->prevtasc ='';
				$row->prekgscc ='';$row->prevtacc ='';$row->venta ='';$venta=0;$row->renta ='';$row->imprenta ='';
				$peso1=intval($peso); $peso2=$peso1+0.99;
				$queryp=$this->db->query("select psc,pcc from $this->tablagrspre where idgrsp>='$peso1' and idgrsp<='$peso2'  order by fecpre Desc limit 1");
				foreach($queryp->result() as $rowp):
						//if($rowp->psc>0){$row->prekgssc =$rowp->psc;$row->prevtasc =number_format($peso+$rowp->psc, 2, '.', ',');} else {$row->prekgssc ='';}	
						if($rowp->pcc>0){
							$totalivta+=($ventakgs)*($peso+$rowp->pcc)+$vtabor;$venta=($ventakgs)*($peso+$rowp->pcc)+$vtabor;
							if(($ventakgs)*($peso+$rowp->pcc)>0){
								$row->renta =number_format(((($venta)/$costo)-1)*100, 2, '.', ',');
								$row->imprenta ='$'.number_format(($venta)-$costo, 2, '.', ',');
							} 
							$row->venta ='$'.number_format((($ventakgs)*($peso+$rowp->pcc))+$vtabor, 2, '.', ',');
							$row->prekgscc =$rowp->pcc;$row->prevtacc ='$'.number_format($peso+$rowp->pcc, 2, '.', ',');
										
						} //else {$row->prekgscc ='';}
				endforeach;  
				
				$data[] = $row;
			endforeach;
			//total
			$this->db->select('max(idcos)');	
			$result = $this->db->get($this->tabla.'_'.$cic);
			foreach ($result->result() as $row):				
				$row->pisg = "Total";$row->dc='';
				if($totorg>0)$row->orgg = number_format($totorg, 3, '.', ','); else $row->orgg ='';
				if($tothas>0)$row->hasg =number_format($tothas,3, '.', ','); else $row->hasg ='';
				if($totppls>0)$row->prepls ='$'.number_format($totppls/$totorg, 2, '.', ','); else $row->prepls ='';
				if($totppls>0)$row->imppls ='$'.number_format($totppls, 2, '.', ','); else $row->imppls ='';
				if($totalisue>0)$row->impsue ='$'.number_format($totalisue, 2, '.', ','); else $row->impsue ='';
				if($totaliali>0)$row->kgsali =number_format($totaliali, 0, '.', ','); else $row->kgsali ='';
				if($totaliimpa>0)$row->preali ='$'.number_format($totaliimpa/$totaliali, 2, '.', ','); else $row->preali ='';
				if($totaliimpa>0)$row->impali ='$'.number_format($totaliimpa, 2, '.', ','); else $row->impali ='';
				if($totalidie>0)$row->ltscom =number_format($totaliimpd/$ppdie, 0, '.', ','); else $row->ltscom ='';
				//if($totalidie>0)$row->precom ='$'.number_format($totaliimpd/$totalidie, 6, '.', ','); else $row->precom ='';
				if($totalidie>0)$row->precom ='$'.number_format($ppdie, 6, '.', ','); else $row->precom ='';
				if($totaliimpd>0)$row->impcom ='$'.number_format($totaliimpd, 2, '.', ','); else $row->impcom ='';	
				if($totalikgs>0)$row->kgskgs =number_format($totalikgs, 0, '.', ','); else $row->kgskgs ='';
				if($totalicos>0)$row->costo ='$'.number_format($totalicos, 2, '.', ','); else $row->costo ='';
				if($costousd>0)$row->costousd ='$'.number_format($costousd, 2, '.', ','); else $row->costousd ='';
				if($totalivta>0)$row->venta ='$'.number_format($totalivta, 2, '.', ','); else $row->venta ='';
				if($totalivta>0)$row->renta =number_format((($totalivta/$totalicos)-1)*100, 2, '.', ','); else $row->renta ='';
				if($totalivta>0)$row->imprenta ='$'.number_format($totalivta-$totalicos, 2, '.', ','); else $row->imprenta ='';
				$row->ppkgs ='';
				if($totaliotc>0)$row->impotc ='$'. number_format($totaliotc, 2, '.', ',');  else $row->impotc ='';
				if($totdep>0)$row->impdep ='$'. number_format($totdep, 2, '.', ',');  else $row->impdep ='';
				if($tokvtar>0)$row->kgsbor = number_format($tokvtar, 0, '.', ',');  else $row->kgsbor ='';
				if($tokimpr>0)$row->vtabor ='$'. number_format($tokimpr, 2, '.', ',');  else $row->vtabor ='';
				//$row->tsc ='';$row->prekgssc ='';$row->impkgssc ='';$row->prevtasc ='';			
				$row->tcc ='';$row->prekgscc ='';$row->prevtacc ='';$row->impkgscc ='';
				$data[] = $row;	
			endforeach;
			}
			return $data;			
		}

	
	

    }
	
?>