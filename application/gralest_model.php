<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Gralest_model extends CI_Model {        
        public $tabla="siegra_22";
		public $tablabio="biogra_22";		
		public $tablap="proygral_22";
				
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function ali($filter,$cic,$est,$dia,$sec){
			//select idpis,pisg,fecg from siegra_22 where pisg='70' and cicg='2020-1'
			$this->db->select("idpis,pisg,fecg,hasg,DATEDIFF( fecb,fecg ) AS dias,(select pors from sobgra where idsob=dias) as por,orgg,fecb,pesb");
			$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->where('fecb <=',$dia);      
			$result = $this->db->get($this->tabla);
			$data = array(); 
			$count=0;$ante=0;$cont=1;$fecant='';
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$fecha1 = new DateTime($row->fecg);
    			$fecha2 = new DateTime($row->fecb);
    			$resultado = $fecha1->diff($fecha2);
    			$dc=$resultado->format('%a');
				$row->diasc=$dc;
				$fecbus=$row->fecb;$row->ahp='';$row->ahq ='';
				
				//$sem=0;
				//obtengo los datos a comparar de la proyeccion general
				//select * from proygral_22 where diasp<=22 order by pesbp DESC limit 1
				$this->db->select("ahp");
				$this->db->where('ciclop =',substr($cic,5,1));
				$this->db->where('seccp =',$sec);
				$this->db->where('diasp <=',$dc);
				$this->db->order_by('diasp','DESC');
				$resultp = $this->db->get($this->tablap,1);
				foreach($resultp->result() as $rowp):
					if($rowp->ahp>0) $row->ahp=number_format(($rowp->ahp), 2, '.','');
				endforeach;	
				//aqui obtengo el pesoa real de acuerdo a la semana
				//busco el alimento
				if($cont==0) $queryali=$this->db->query("SELECT sum(kgd) as alic from chagra_22 where idpischa='$row->idpis' and feccha<='$fecbus' and ciccha='$cic'");
				else $queryali=$this->db->query("SELECT sum(kgd) as alic from chagra_22 where idpischa='$row->idpis' and feccha>'$fecant' and  feccha<='$fecbus' and ciccha='$cic'");
					foreach($queryali->result() as $rowali):
						//$row->ahq =$rowali->alic;
						$row->ahq=number_format(($rowali->alic/$row->hasg/7), 2, '.', ''); 
				endforeach;	
				$fecant=$row->fecb;
				$data[] = $row;
			endforeach;
			}
			return $data;			
		}
		function inc($filter,$cic,$est,$dia,$sec){
			//select idpis,pisg,fecg from siegra_22 where pisg='70' and cicg='2020-1'
			$this->db->select("idpis,pisg,fecg,DATEDIFF( fecb,fecg ) AS dias,(select pors from sobgra where idsob=dias) as por,orgg,fecb,pesb");
			$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->where('fecb <=',$dia);      
			$result = $this->db->get($this->tabla);
			$data = array(); 
			$count=0;$ante=0;$cont=1;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$fecha1 = new DateTime($row->fecg);
    			$fecha2 = new DateTime($row->fecb);
    			$resultado = $fecha1->diff($fecha2);
    			$dc=$resultado->format('%a');
				$row->diasc=$dc;
				$fecbus=$row->fecb;$row->incpp='';$row->incq ='';
				//$sem=0;
				//obtengo los datos a comparar de la proyeccion general
				//select * from proygral_22 where diasp<=22 order by pesbp DESC limit 1
				$this->db->select("incpp");
				$this->db->where('ciclop =',substr($cic,5,1));
				$this->db->where('seccp =',$sec);
				$this->db->where('diasp <=',$dc);
				$this->db->order_by('diasp','DESC');
				$resultp = $this->db->get($this->tablap,1);
				foreach($resultp->result() as $rowp):
					if($rowp->incpp>0) $row->incpp=number_format(($rowp->incpp), 2, '.','');
				endforeach;	
				//aqui obtengo el pesoa real de acuerdo a la semana
				//busco el primer el peso anterior a la primera fecha a buscar
				
				$query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=idpis where idpisb='$row->idpis' and fecb = '$fecbus' and cicb = '$cic' group by fecb");
				foreach($query->result() as $rowbio):
					if($cont==1){
					$rowbio->pesb=0;
					$cont=0;
				} 	
					if($rowbio->pesb>0){$row->incq =number_format(($rowbio->pesb-$ante), 2, '.','');$ante=$rowbio->pesb;} 
				endforeach;
				$data[] = $row;
			endforeach;
			}
			return $data;			
		}
		
		function tal($filter,$cic,$est,$dia,$sec){
			//select idpis,pisg,fecg from siegra_22 where pisg='70' and cicg='2020-1'
			$this->db->select("idpis,pisg,fecg,DATEDIFF( fecb,fecg ) AS dias,(select pors from sobgra where idsob=dias) as por,orgg,fecb,pesb");
			$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->where('fecb <=',$dia);      
			$result = $this->db->get($this->tabla);
			$data = array(); 
			$count=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$fecha1 = new DateTime($row->fecg);
    			$fecha2 = new DateTime($row->fecb);
    			$resultado = $fecha1->diff($fecha2);
    			$dc=$resultado->format('%a');
				$row->diasc=$dc;
				$fecbus=$row->fecb;$row->pesp='';$row->pesq ='';
				//$sem=0;
				//obtengo los datos a comparar de la proyeccion general
				//select * from proygral_22 where diasp<=22 order by pesbp DESC limit 1
				$this->db->select("pesbp");
				$this->db->where('ciclop =',substr($cic,5,1));
				$this->db->where('seccp =',$sec);
				$this->db->where('diasp <=',$dc);
				$this->db->order_by('diasp','DESC');
				$resultp = $this->db->get($this->tablap,1);
				foreach($resultp->result() as $rowp):
					if($rowp->pesbp>0) $row->pesp=number_format(($rowp->pesbp), 2, '.','');
				endforeach;	
				//aqui obtengo el pesoa real de acuerdo a la semana 	
				$query=$this->db->query("select pesb from siegra_22 inner join biogra_22 on idpisb=idpis where idpisb='$row->idpis' and fecb = '$fecbus' and cicb = '$cic' group by fecb");
				foreach($query->result() as $rowbio):
					if($rowbio->pesb>0) $row->pesq =number_format(($rowbio->pesb), 2, '.',''); 
				endforeach;
				$data[] = $row;
			endforeach;
			}
			return $data;			
		}
		
		function fca($filter,$cic,$est,$dia,$sec){
			//select idpis,pisg,fecg from siegra_22 where pisg='70' and cicg='2020-1'
			$this->db->select("idpis,pisg,hasg,fecg,DATEDIFF( fecb,fecg ) AS dias,(select pors from sobgra where idsob=dias) as por,orgg,fecb,pesb");
			$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->where('fecb <=',$dia);      
			$resultfca = $this->db->get($this->tabla);
			$data = array(); 
			$count=0;
			if($resultfca->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($resultfca->result() as $row):
				$fecha1 = new DateTime($row->fecg);
    			$fecha2 = new DateTime($row->fecb);
    			$resultado = $fecha1->diff($fecha2);
    			$dc=$resultado->format('%a');
				$row->diasc=$dc;
				$fecbus=$row->fecb;
				//$sem=0;
				//obtengo los datos a comparar de la proyeccion general
				//select * from proygral_22 where diasp<=22 order by pesbp DESC limit 1
				$this->db->select("fcap,porp,pesbp");
				$this->db->where('ciclop =',substr($cic,5,1));
				$this->db->where('seccp =',$sec);
				$this->db->where('diasp <=',$dc);
				$this->db->order_by('diasp','DESC');
				$resultp = $this->db->get($this->tablap,1);
				foreach($resultp->result() as $rowp):
					$row->fcap=$rowp->fcap;
					$row->porp=$rowp->porp;
					$row->pesbp=$rowp->pesbp;
				endforeach;	
				//aqui obtengo el fca real de acuerdo a la semana 	
				//buscar el porcentaje de sobrevivencia la fecha de busqueda
				$queryali=$this->db->query("SELECT sum(kgd) as alic from chagra_22 where idpischa='$row->idpis' and (feccha<='$fecbus')  and ciccha='$cic'");
				foreach($queryali->result() as $rowali):
					$row->alic =$rowali->alic; 
				endforeach;	
				$queryinc=$this->db->query("select sobp from siegra_22 inner join sobgra_22 on idpiss=idpis where idpiss='$row->idpis' and fecs = '$fecbus' and cics = '$cic' group by fecs");
				foreach($queryinc->result() as $rowsob):
					$row->por =number_format(($rowsob->sobp), 2, '.', ','); 
				endforeach;			 
				$orgsbio= ($row->orgg*$row->por)/100;
				$orgsbio2= ($row->orgg*$row->porp)/100;
				$row->bio= number_format(($row->pesb*$orgsbio), 0, '.', ',');
				$row->bio2= number_format(($row->pesbp*$orgsbio2), 0, '.', ',');
				$row->bioha= number_format(($row->pesb*$orgsbio)/$row->hasg, 0, '.', '');$row->biohag= number_format(($row->pesb*$orgsbio)/$row->hasg, 0, '.', ',');
				$row->bioha2= number_format(($row->pesbp*$orgsbio2)/$row->hasg, 0, '.', '');
				$row->porbio=number_format(((100-(($row->bioha2-$row->bioha)/$row->bioha2)*100)),'2','.','');
				$biomasa = str_replace(",", "", $row->bio);
				if($biomasa>0){
						$row->fcaq=number_format(($row->alic/$biomasa), 2, '.', ',');
					}
				else {$row->fcaq='';}
				$data[] = $row;
			endforeach;
			}
			return $data;			
		}
		
		function gral($filter,$cicb,$secc,$base){
			//SELECT fecb from siegra_22 inner join biogra_22 on idpisb=idpis where numgrab='4' and cicb='2020-1' group by fecb
			$this->db->select("fecb");
			$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
			if($filter['where']!='') $this->db->where($filter['where']);	
			$this->db->group_by('fecb');      
			//$this->db->group_by('pisg');		
			$result = $this->db->get($this->tabla);
			$data = array(); 
			$count=0;
			if($result->num_rows()>0){
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				$count+=1;
				$row->sem=$count;
				//select idpisb,pesb from biogra_22 where numgrab='4' and cicb='2020-1' and fecb='2020-04-10'
				$this->db->select("idpisb,pesb,pisg,fecg,cicg,secc");
				$this->db->join($this->tablabio, 'idpisb=idpis','inner'); 
				if($filter['where']!='') $this->db->where($filter['where']);
				$this->db->where('fecb',$row->fecb);
				$this->db->order_by('pisg');
				$resultb = $this->db->get($this->tabla);
				$row->e1='';$row->e2='';$row->e3='';$row->e4='';$row->e5='';$row->e6='';$row->e7='';$row->e8='';$row->e9='';$row->e10='';
				$row->e11='';$row->e12='';$row->e13='';$row->e14='';$row->e15='';$row->e16='';$row->e17='';$row->e18='';$row->e19='';$row->e20='';
				$row->e21='';$row->e22='';$row->e23='';$row->e24='';$row->e25='';$row->e26='';$row->e27='';$row->e28='';$row->e29='';$row->e30='';
				$row->e31='';$row->e32='';$row->e33='';$row->e34='';$row->e35='';$row->e36='';$row->e37='';$row->e38='';$row->e39='';$row->e40='';
				$row->e41='';$row->e42='';$row->e43='';$row->e44='';$row->e45='';$row->e46='';$row->e47='';$row->e48='';$row->e49='';$row->e50='';
				$row->e51='';$row->e52='';$row->e53='';$row->e54='';$row->e55='';$row->e56='';$row->e57='';$row->e58='';$row->e59='';$row->e60='';
				$row->e61='';$row->e62='';$row->e63='';$row->e64='';$row->e65='';$row->e66='';$row->e67='';$row->e68='';$row->e69='';$row->e70='';
				$row->e71='';$row->e72='';$row->e73='';$row->e74='';$row->e75='';$row->e76='';$row->e77='';
				foreach($resultb->result() as $rowb):
					 //calcular los dias de la fecha de siembra a la fecha de busqueda de la biometria.
					$dc=0;$pc=0;$peso=0;
					$cicp=substr($rowb->cicg,5,1);
					$secci=$rowb->secc;
					$diaact=date("Y-m-d");
					$fecha1 = new DateTime($rowb->fecg);
    				$fecha2 = new DateTime($row->fecb);
    				$resultado = $fecha1->diff($fecha2);
    				$dc=$resultado->format('%a');
					//obtengo los datos a comparar de la proyeccion general
					//select * from proygral_22 where diasp<=22 order by pesbp DESC limit 1
					$this->db->where('diasp <=',$dc);
					$this->db->where('ciclop =',$cicp);
					$this->db->where('seccp =',$secci);
					$this->db->order_by('pesbp','DESC');
					$resultp = $this->db->get($this->tablap,1);
					foreach($resultp->result() as $rowp):
						$pc=$rowp->pesbp;
					endforeach;	
					$peso=number_format($rowb->pesb-$pc,2, '.', '');
					if($peso>0) $colorf='lime';
					elseif($peso<-2) $colorf='red'; else $colorf='orange';
					$liga='<a href='.$base.'index.php/gralest/graldetest/'.$rowb->cicg.'-'.$row->fecb.'-'.$secci.'-'.$rowb->pisg.' style=color:'.$colorf.';background-color:'.$colorf.'; >'.$peso.'</a>';
					switch ($rowb->pisg) {
						case '1': $row->e1=$liga;break;
						case '2': $row->e2=$liga;break;
						case '3': $row->e3=$liga;break;
						case '4': $row->e4=$liga;break;
						case '5': $row->e5=$liga;break;
						case '6': $row->e6=$liga;break;
						case '7': $row->e7=$liga;break;
						case '8': $row->e8=$liga;break;
						case '9': $row->e9=$liga;break;
						case '10': $row->e10=$liga;break;
						case '11': $row->e11=$liga;break;
						case '12': $row->e12=$liga;break;
						case '13': $row->e13=$liga;break;
						case '14': $row->e14=$liga;break;
						case '15': $row->e15=$liga;break;
						case '16': $row->e16=$liga;break;
						case '17': $row->e17=$liga;break;
						case '18': $row->e18=$liga;break;
						case '19': $row->e19=$liga;break;
						case '20': $row->e20=$liga;break;
						case '21': $row->e21=$liga;break;
						case '22': $row->e22=$liga;break;
						case '23': $row->e23=$liga;break;
						case '24': $row->e24=$liga;break;
						case '25': $row->e25=$liga;break;
						case '26': $row->e26=$liga;break;
						case '27': $row->e27=$liga;break;
						case '28': $row->e28=$liga;break;
						case '29': $row->e29=$liga;break;
						case '30': $row->e30=$liga;break;
						case '31': $row->e31=$liga;break;
						case '32': $row->e32=$liga;break;
						case '33': $row->e33=$liga;break;
						case '34': $row->e34=$liga;break;
						case '35': $row->e35=$liga;break;
						case '36': $row->e36=$liga;break;
						case '37': $row->e37=$liga;break;
						case '38': $row->e38=$liga;break;
						case '39': $row->e39=$liga;break;
						case '40': $row->e40=$liga;break;
						case '41': $row->e41=$liga;break;
						case '42': $row->e42=$liga;break;
						case '43': $row->e43=$liga;break;
						case '44': $row->e44=$liga;break;
						case '45': $row->e45=$liga;break;
						case '46': $row->e46=$liga;break;
						case '47': $row->e47=$liga;break;
						case '48': $row->e48=$liga;break;
						case '49': $row->e49=$liga;break;
						case '50': $row->e50=$liga;break;
						case '51': $row->e51=$liga;break;
						case '52': $row->e52=$liga;break;
						case '53': $row->e53=$liga;break;
						case '54': $row->e54=$liga;break;
						case '55': $row->e55=$liga;break;
						case '56': $row->e56=$liga;break;
						case '57': $row->e57=$liga;break;
						case '58': $row->e58=$liga;break;
						case '59': $row->e59=$liga;break;
						case '60': $row->e60=$liga;break;
						case '61': $row->e61=$liga;break;
						case '62': $row->e62=$liga;break;
						case '63': $row->e63=$liga;break;
						case '64': $row->e64=$liga;break;
						case '65': $row->e65=$liga;break;
						case '66': $row->e66=$liga;break;
						case '67': $row->e67=$liga;break;
						case '68': $row->e68=$liga;break;
						case '69': $row->e69=$liga;break;
						case '70': $row->e70=$liga;break;
						case '71': $row->e71=$liga;break;
						case '72': $row->e72=$liga;break;
						case '73': $row->e73=$liga;break;
						case '74': $row->e74=$liga;break;
						case '75': $row->e75=$liga;break;
						case '76': $row->e76=$liga;break;
						case '77': $row->e77=$liga;break;
												
					}
				endforeach;	
				$data[] = $row;
			endforeach;
			
			}
			return $data;			
		}


	
			
    }
    
?>