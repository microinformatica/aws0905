<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Bcv_model extends CI_Model {
        public $id="";				
		public $tabla="";
		public $nom="";
		public $activo="Activo";
		public $tablab="biologo";
		public $idb="NumBio";
		public $nb="NomBio";
		public $tablac="chofer";
		public $idc="NumCho";
		public $nc="NomCho";
		public $tablau="unidad";
		public $idu="NumUni";
		public $nu="NomUni";
		public $Pla="Placas";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		public function agregar($nom,$act,$tablas){
			$this->id=$this->idb; $this->nom=$this->nb;	$this->tabla=$this->tablab;				
			if($tablas=='chofer'){				
				$this->id=$this->idc; $this->nom=$this->nc;	$this->tabla=$this->tablac;
			}
			if($tablas=='unidad'){				
				$this->id=$this->idu; $this->nom=$this->nu;	$this->tabla=$this->tablau;
			}			
			$data=array($this->nom=>$nom,$this->activo=>$act);
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$nom,$act,$tablas){
			$this->id=$this->idb; $this->nom=$this->nb;	$this->tabla=$this->tablab;				
			if($tablas=='chofer'){				
				$this->id=$this->idc; $this->nom=$this->nc;	$this->tabla=$this->tablac;
			}
			if($tablas=='unidad'){				
				$this->id=$this->idu; $this->nom=$this->nu;	$this->tabla=$this->tablau;
			}			
			$data=array($this->nom=>$nom,$this->activo=>$act);
			$this->db->where($this->id,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function borrar($id,$tablas){
			$this->id=$this->idb; $this->tabla=$this->tablab;				
			if($tablas=='chofer'){				
				$this->id=$this->idc; $this->tabla=$this->tablac;
			}
			if($tablas=='unidad'){				
				$this->id=$this->idu; $this->tabla=$this->tablau;
			}
			$this->db->where($this->id,$id);
			$this->db->delete($this->tabla);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}	
		function getElements($where){
			if($where['NomDes']!='Todos'){			
				$this->db->select('NumUni,NomUni as val');
				$this->db->join('unidad', 'NumUniD=NumUni', 'inner');
				//$this->db->where('NumDes >',0);
				$this->db->where('Activo =',0);	
				$this->db->group_by('NumUni');        
        		$this->db->where($where);
        		$result=$this->db->get('viaticos');
        		$data = array();        
        		foreach($result->result() as $row):
            		$data[] = $row;
        		endforeach;        
        		return $data;	
    		}
		}
		function getBCV($filter){						
			//select NumBio,NomBio,Activo from biologo where NumBio>1 order by Activo DESC,NumBio			
			if($filter['where']=='biologo'){
				$this->db->select('NumBio as Numero,NomBio as Nombre,Activo');
				$this->id=$this->idb;	$this->tabla=$this->tablab;
			}
			if($filter['where']=='chofer'){
				$this->db->select('NumCho as Numero,NomCho as Nombre,Activo');
				$this->id=$this->idc;	$this->tabla=$this->tablac;
			}
			if($filter['where']=='unidad'){
				$this->db->select('NumUni as Numero,NomUni as Nombre,Activo');
				$this->id=$this->idu;	$this->tabla=$this->tablau;
			}
			$this->db->where($this->id.' >',1);							
			$this->db->order_by($this->activo,'DESC');
			$this->db->order_by($this->id);
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $destino="";$uni="";
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				//$row->Numero=$row->id;
				$data[] = $row;				
			endforeach;
			return $data;
		}
		
		function getNumRowsBCV($filter){
			if($filter['where']=='biologo'){
				$this->id=$this->idb;	$this->tabla=$this->tablab;
			}						
			if($filter['where']=='chofer'){
				$this->id=$this->idc;	$this->tabla=$this->tablac;
			}
			if($filter['where']=='unidad'){
				$this->id=$this->idu;	$this->tabla=$this->tablau;
			}
			$this->db->order_by($this->activo,'DESC');
			$this->db->order_by($this->id);
			$result = $this->db->get($this->tabla);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function verDestino(){
			$this->db->where('numdes >',0);
			$this->db->group_by('nomdes');
			$query=$this->db->get('viaticos');
			return $query->result();			
		}	
		public function verUnidad(){
			$this->db->where('numuni >',1);
			$this->db->where('activo =',0);
			$query=$this->db->get('unidad');
			return $query->result();			
		}
		public function getViatico($id){
			$this->db->where($this->id,$id);
			$query=$this->db->get($this->tabla);
			if($query->num_rows()>0){
				return $query->row();	
			}else{
				return "-1";	
			}
		}			
    }    
?>