<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Solicitud_model extends CI_Model {        
        public $id="idsolmov";
        public $fsol="fecsol";
        public $nsa="nomsa";
        public $nap="nomap";
        public $cp="cansol";
        public $ins="inspcr";
		public $fol="folpcr";
		public $ref="refpag";
		public $fpag="fecpag";
        public $tablasoli="solmov";
		
		public $idcm="idclimov";
		public $ncli="numcli";
		public $nsol="numsolmov";
        public $tablaclisol="solmovcli";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
				
		public function agregar($fso,$nsa,$nap,$can,$ins,$fol,$ref,$fpa){
			$can = str_replace(",", "", $can);
			$data=array($this->fsol=>$fso,$this->nsa=>$nsa,$this->nap=>$nap,$this->cp=>$can,$this->ins=>$ins,$this->fol=>$fol,$this->ref=>$ref,$this->fpag=>$fpa);			
			$this->db->insert($this->tablasoli,$data);
			return $this->db->insert_id();
		}
		public function actualizar($id,$fso,$nsa,$nap,$can,$ins,$fol,$ref,$fpa){
			$can = str_replace(",", "", $can);
			$data=array($this->fsol=>$fso,$this->nsa=>$nsa,$this->nap=>$nap,$this->cp=>$can,$this->ins=>$ins,$this->fol=>$fol,$this->ref=>$ref,$this->fpag=>$fpa);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasoli,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		function getSolicitudes($filter){
			$this->db->order_by($this->id,'DESC');
			if($filter['where']!='') $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablasoli,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablasoli);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			foreach($result->result() as $row):
				$row->cansol = number_format(($row->cansol), 3, '.', ','); if($row->cansol==0.000){$row->cansol="";}
				$data[] = $row;
			endforeach;
			return $data;
		}
		
		function getNumRowsSM($filter){
			//$this->db->where($this->id.' >',0);
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablasoli);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function solicitudescsm($filter,$extra){
			$this->db->select('idclimov,numcli,numsolmov,Numero,Razon,ubigra,Edo');
			$this->db->join('clientes', 'Numero ='.$this->ncli, 'inner');
			$this->db->where('numsolmov',$extra);
			//if($filter['where']!=0) $this->db->where($filter['where']);			
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='') $this->db->order_by($filter['order']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablaclisol,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablaclisol);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $clix=''; $cont=1; $cuenta=0;
			$tot=$result->num_rows();
			foreach($result->result() as $row):
				$row->Razon = $row->Razon;$cuenta+=1;
				if($cuenta<$tot){
					if($cuenta<$tot-1){	
					$row->cliente = $row->Razon.' Dom. '.$row->ubigra.', '.$row->Edo.'./ ';
					}else{
						$row->cliente = $row->Razon.' Dom. '.$row->ubigra.', '.$row->Edo;
					};
				} 
				else {$row->cliente =' y '. $row->Razon.' Dom. '.$row->ubigra.', '.$row->Edo.'.';}
				if($cont==1){ $row->cli=$row->cliente;$cont=2;}else{$row->cli=$clix.$row->cliente;}
				$clix=$row->cli;
				$data[] = $row;
			endforeach;
			return $data;
		}
		
		function getNumRowsCSM($filter){
			$this->db->join('clientes', 'Numero ='.$this->ncli, 'inner');
			if($filter['where']!='') $this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablaclisol);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		public function quitar($id){
			$this->db->where($this->idcm,$id);
			$this->db->delete($this->tablaclisol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		public function agrega($nc,$sol){
			$data=array($this->ncli=>$nc,$this->nsol=>$sol);			
			$this->db->insert($this->tablaclisol,$data);
			return $this->db->insert_id();
		}
    }
  		
?>